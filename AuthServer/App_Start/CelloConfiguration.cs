//-----------------------------------------------------------------------
// <copyright file="CelloConfiguration.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using CelloSaaS.Configuration;

namespace CelloSaaS.AuthorizationServer.App_Start
{
    /// <summary>
    /// CelloSaaS Related Metadata configuration loading during application start
    /// </summary>
    public static class CelloConfiguration
    {
        /// <summary>
        /// Fluent API configurations of module, features, entities, dataviews, rules
        /// </summary>
        public static void Configure()
        {
            // Register CelloSaaS modules - do not edit
            CelloConfigurator.RegisterModule<CelloSaaS.Notification.NotificationModuleConfigurator>();
            CelloConfigurator.RegisterModule<DBCelloModuleConfigurator>();
            CelloConfigurator.RegisterEntity<DBCelloEntityConfigurator>();
            CelloConfigurator.RegisterDataView<DBCelloDataViewConfigurator>();
            CelloConfigurator.Configure();
        }
    }
}
