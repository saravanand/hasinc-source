//-----------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using CelloSaaS.AuthServer.Core.Extensions;
using CelloSaaS.AuthServer.Core.Performance;
using System.Threading.Tasks;
using CelloSaaS.Configuration;

namespace CelloSaaS.AuthorizationServer.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpActionInvoker), new CelloApiControllerActionInvoker());
            Configure();
            ConfigureWebApi(config);
            config.EnsureInitialized();
        }

        public static void Configure()
        {
            // Register CelloSaaS modules - do not edit
            CelloConfigurator.RegisterModule<CelloSaaS.Notification.NotificationModuleConfigurator>();
            CelloConfigurator.RegisterModule<CelloSaaS.WorkFlow.WorkflowModuleConfigurator>();
            CelloConfigurator.RegisterModule<CelloSaaS.DataBackup.DataBackupModuleConfigurator>();
            CelloConfigurator.RegisterModule<CelloSaaS.Integration.IntegrationModuleConfigurator>();
            CelloConfigurator.RegisterEntity<CelloSaaS.WorkFlow.WorkflowEntityConfigurator>();
            CelloConfigurator.RegisterModule<DBCelloModuleConfigurator>();
            CelloConfigurator.RegisterEntity<DBCelloEntityConfigurator>();
            CelloConfigurator.RegisterDataView<DBCelloDataViewConfigurator>();
            CelloConfigurator.Configure();
            //DalExtensionsConfiguration.Register<CelloSaaS.Services.DataEventHandlers.CURDEventRasieEventHandler>();
            //MeteringPreAndPostProcessConfiguration.AddPostProcess<CelloSaaS.Billing.Services.GenerateAdjustmentInvoiceMeteringPostProcessor>();
        }

        private static void ConfigureWebApi(HttpConfiguration config)
        {
            // Web API configuration and services
            // TODO :Not used as of now due to the exceptions raised for built in service mappings
            //config.DependencyResolver = UnityDependencyRegistry.GetUnityDependencyResolver();

            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            //Register the default filters here
            config.Filters.AddRange(new List<IFilter> { new CelloApiActionTimers(), new ExceptionHandlingAttribute() });
        }

        public static HttpConfiguration Configure(HttpConfiguration config)
        {
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpActionInvoker), new CelloApiControllerActionInvoker());
            ConfigureWebApi(config);
            CelloConfiguration.Configure();
            return config;
        }
    }
}
