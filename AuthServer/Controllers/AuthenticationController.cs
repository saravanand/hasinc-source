﻿//-----------------------------------------------------------------------
// <copyright file="AuthenticationController.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Http;

#region apprelated
using CelloSaaS.AuthServer.Core.Security;
using CelloSaaS.AuthServer.Core.Models;
using CelloSaaS.AuthServer.Core.ServiceContracts;
using System.Security.Claims;
using CelloSaaS.AuthServer.Core.Models.Authorization;
using System.Web.Http;
using CelloSaaS.AuthServer.Core.Models.Identity;
using CelloSaaS.AuthServer.Core;
using CelloSaaS.AuthServer.Core.Ancilliary;
using CelloSaaS.AuthServer.Core.Results.Logout;
using CelloSaaS.AuthServer.Core.Results;
using CelloSaaS.AuthServer.Core.Results.Login;
using CelloSaaS.AuthServer.Core.Models.Consents;
using Microsoft.Owin.Security.Cookies;
using System.Web;
using System.Globalization;
using CelloSaaS.Library;
using CelloSaaS.AuthServer.Core.Contracts;
using CelloSaaS.AuthServer.Core.Models.Status;
using System.Text;
using CelloSaaS.AuthServer.Core.SettingsManagement.Contracts;
using CelloSaaS.AuthServer.Core.Models.Authentication;
using CelloSaaS.AuthServer.Core.Extensions;
using CelloSaaS.AuthorizationServer.Models;
using Contoso.AuthServer.Models;

#endregion

namespace CelloSaaS.AuthorizationServer.Controllers
{
    [RoutePrefix(Routes.AuthenticationPrefix)]
    public class AuthenticationController : CelloApiController
    {
        IUserService _userService;
        IExternalClaimsFilter _externalClaimsFilter;
        IClientSettingsService _tenantSettingsService;
        IClientService _clientService;
        IConsentService _consentService;
        IStateTokenGenerator _stateTokenGenerator;
        CelloSaaS.ServiceContracts.TenantManagement.ITenantService _tenantService;
        const string _activationCookieName = "__uia__";
        const char _keyDelimiter = '~';

        public AuthenticationController()
        {
            _userService = ServiceLocator.Resolve<IUserService>();
            _externalClaimsFilter = ServiceLocator.Resolve<IExternalClaimsFilter>();
            _tenantSettingsService = ServiceLocator.Resolve<IClientSettingsService>();
            _clientService = ServiceLocator.Resolve<IClientService>();
            _consentService = ServiceLocator.Resolve<IConsentService>();
            _stateTokenGenerator = ServiceLocator.Resolve<IStateTokenGenerator>();
            _tenantService = ServiceLocator.Resolve<CelloSaaS.ServiceContracts.TenantManagement.ITenantService>();
        }

        #region Authentication Metadata
        [Route(Routes.GetAuthenticationProvidersRoute, Name = Routes.GetAuthenticationProvidersRouteName)]
        public async Task<IHttpActionResult> GetAuthenticationProviders()
        {
            try
            {
                var authenticationTypes = await _consentService.GetAuthenticationProviders();
                if (authenticationTypes == null || authenticationTypes.Count < 1)
                {
                    return new ResponseContentResult<string>("No valid authentication types found");
                }

                return new ResponseContentResult<List<AuthenticationProvider>>(authenticationTypes.Values.ToList());
            }
            catch (SubjectDetailException)
            {
                return new ResponseContentResult<string>("Exception while fetching the authentication types for the given provider");
            }
        }

        [Route(Routes.GetAuthenticationTypesRoute, Name = Routes.GetAuthenticationTypesRouteName)]
        public async Task<IHttpActionResult> GetAuthenticationTypesForProvider(string providerId = null)
        {
            try
            {
                Dictionary<string, AuthenticationType> authTypes = null;

                authTypes = (string.IsNullOrEmpty(providerId))
                            ? await _consentService.GetAuthenticationTypesAsync(null)
                            : await _consentService.GetAuthenticationTypesAsync(new string[] { providerId });

                Dictionary<string, Dictionary<string, string>> authTypesResult = new Dictionary<string, Dictionary<string, string>>(StringComparer.OrdinalIgnoreCase);

                foreach (var authType in authTypes)
                {
                    if (!authTypesResult.ContainsKey(authType.Value.ProviderId.ToString()))
                        authTypesResult.Add(authType.Value.ProviderId.ToString().ToLowerInvariant(), new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase));

                    if (!authTypesResult[authType.Value.ProviderId.ToString()].ContainsKey(authType.Value.Id.ToString()))
                        authTypesResult[authType.Value.ProviderId.ToString()].Add(authType.Value.Id.ToString().ToLowerInvariant(), authType.Value.Name);
                }
                return new ResponseContentResult<Dictionary<string, Dictionary<string, string>>>(authTypesResult);
                //return new ResponseContentResult<Dictionary<string, string>>(authTypes.ToDictionary(at => at.Key, at => at.Value.Name));
            }
            catch (SubjectDetailException)
            {
                return new ResponseContentResult<string>("Exception while fetching the authentication types for the given provider");
            }
        }
        #endregion

        [HttpGet]
        public async Task<IHttpActionResult> ForgotPassword(string accessKey, string clientId, string companyCode)
        {
            var loginModel = new LoginModel
            {
                Status = UserStatus.ForgotPassword,
                State = _stateTokenGenerator.GenerateToken(),
                AccessKey = accessKey,
                PostbackUrl = this.Request.GetUrlHelper().Route(Routes.ForgotPasswordRouteName, new Dictionary<string, object>()),
                Provider = "local",
                ProviderId = AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId),
                ClientId = clientId,
                CompanyCode = companyCode
            };

            if (string.IsNullOrEmpty(accessKey))
            {
                loginModel.AccessKey = _stateTokenGenerator.GenerateToken();
                loginModel.Status = UserStatus.ForgotPassword;
                loginModel.Message = "Access key cannot be empty";
                return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
            }

            if (!_stateTokenGenerator.IsValid(accessKey))
            {
                loginModel.AccessKey = _stateTokenGenerator.GenerateToken();
                loginModel.Status = UserStatus.ForgotPassword;
                loginModel.Message = "Request state is stale";
                return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
            }

            return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
        }

        [HttpPost]
        [Route(Routes.ForgotPasswordRoute, Name = Routes.ForgotPasswordRouteName)]
        public async Task<IHttpActionResult> ForgotPassword(LoginViewModel loginViewModel)
        {
            InputSanitizer.SanitizeInputs(loginViewModel);

            // to facilitate the view page identification
            if (loginViewModel == null)
                loginViewModel = new LoginViewModel
                {
                    Message = UserStatus.ForgotPassword.ToString("F"),
                    AccessKey = _stateTokenGenerator.GenerateToken(),
                    Status = UserStatus.ForgotPassword
                };

            IHttpActionResult result = null;

            // regular password validation is not required here
            result = await ValidateLoginInputParameters(loginViewModel, true, false, false, UserStatus.ForgotPassword);
            if (result != null) return result;

            result = await ValidatePasswordChangeInputs(loginViewModel, false, UserStatus.ForgotPassword);
            if (result != null) return result;

            var ctx = await _userService.ChangePasswordWithSecurityCheck(loginViewModel);

            if (ctx != null && ctx.IsError)
            {
                var loginmodel = new LoginModel
                {
                    CompanyCode = loginViewModel.CompanyCode,
                    UserName = loginViewModel.UserName,
                    Status = UserStatus.ForgotPassword,
                    State = _stateTokenGenerator.GenerateToken(),
                    PostbackUrl = this.Request.GetUrlHelper().Route(Routes.ForgotPasswordRouteName, new Dictionary<string, object>()),
                    Provider = "local",
                    Message = ctx.ErrorMessage,
                };
                loginmodel.AccessKey = loginmodel.State;
                return new LoginViewResult<LoginViewModel>(loginmodel);
            }
            loginViewModel.Password = loginViewModel.NewPassword;

            // update is successful
            return await AuthenticateAndSignIn(loginViewModel);
        }

        [HttpGet]
        [Route(Routes.InvitationRoute, Name = Routes.InvitationRouteName)]
        public async Task<IHttpActionResult> Invite(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                string status = "Key is missing in the invitation";
                return Redirect(AppSettingHelpers.GetValue<string>(AppSettingConstants.ClientAppErrorReportingUri) + "?msg=" + status);
            }

            // set the cookie
            if (!SetInviteLogOnCookie(key))
            {
                string status = "The application could not accept the user invitaion right now. Please try back after sometime";
                return Redirect(AppSettingHelpers.GetValue<string>(AppSettingConstants.ClientAppErrorReportingUri) + "?msg=" + status);
            }

            UserRegistrationInfo regInfo = GetUserRegistrationInfoFromActivationKey(key);

            if (regInfo == null || string.IsNullOrEmpty(regInfo.CompanyCode) || string.IsNullOrEmpty(regInfo.Identifier) || string.IsNullOrEmpty(regInfo.TenantId))
            {
                string status = "Invalid Activation Key. Please contact your administrator";
                return Redirect(AppSettingHelpers.GetValue<string>(AppSettingConstants.ClientAppErrorReportingUri) + "?msg=" + status);
            }

            var clientDetails = await _clientService.GetClientsAsync(new AuthServer.Core.Models.Clients.ClientSearchCondition
            {
                TenantId = regInfo.TenantId.ToGuid(),
                ApplicationTypes = (CelloSaaS.AuthServer.Core.Models.ApplicationTypes)Enum.Parse(typeof(CelloSaaS.AuthServer.Core.Models.ApplicationTypes), regInfo.ApplicationType)
            });

            if (clientDetails == null || clientDetails.Count < 1)
            {
                string status = "Invalid Client Details";
                return Redirect(AppSettingHelpers.GetValue<string>(AppSettingConstants.ClientAppErrorReportingUri) + "?msg=" + status);
            }

            var clientDetail = clientDetails.Values.FirstOrDefault();

            string tenancyMode = AppSettingHelpers.GetValue<string>(AppSettingConstants.TenancyMode);

            TenancyModes authServerMode = TenancyModes.MultiTenancy;

            Enum.TryParse<TenancyModes>(tenancyMode, true, out authServerMode);

            LoginModel loginModel = null;

            //if (IsCommonLoginEnabled)
            //{
            // get all the regd providers, except the ldap provider
            loginModel = await InitiateUniversalLogin(clientDetail.Id.ToString(), TenancyModes.MultiTenancy);

            if (loginModel != null)
                loginModel.AccessKey = loginModel.State = _stateTokenGenerator.GenerateToken();

            InitLocalLogin(loginModel);

            loginModel.ClientId = clientDetail.Id.ToString();
            loginModel.CompanyCode = regInfo.CompanyCode;
            loginModel.TenantId = regInfo.TenantId;

            // redirect to the commonlogin.html page
            // end the response.
            return new LoginViewResult<LoginModel>(loginModel) { EnableRegistration = true };
        }

        [HttpGet]
        public async Task<IHttpActionResult> Login(string message)
        {
            string companyCode = null, clientId = null;

            if (string.IsNullOrEmpty(message))
            {
                // no jwt is received here
                companyCode = VerifyLoginRequestMessage(out clientId);
            }
            else
            {
                //set the cookie to indicate the login process
                companyCode = SaveLoginRequestMessage(message, out clientId);
            }

            // if no msg & no cookie, what to do...
            if ((!IsCommonLoginEnabled && string.IsNullOrEmpty(companyCode)) || string.IsNullOrEmpty(clientId))
            {
                Request.GetOwinContext().Authentication.SignOut(new string[] 
                { 
                    AuthServerConstants.ExternalAuthenticationType, AuthServerConstants.PrimaryAuthenticationType 
                });

                ClearLoginCookie();
                ClearLoginRequestMessage();

                string status = "ssn_exp";
                return Redirect(AppSettingHelpers.GetValue<string>(AppSettingConstants.ClientApplicationUri) + "?msg=" + status);
            }

            string tenancyMode = AppSettingHelpers.GetValue<string>(AppSettingConstants.TenancyMode);

            TenancyModes authServerMode = TenancyModes.MultiTenancy;

            Enum.TryParse<TenancyModes>(tenancyMode, true, out authServerMode);

            LoginModel loginModel = null;

            if (IsCommonLoginEnabled)
            {
                // get all the regd providers, except the ldap provider
                loginModel = await InitiateUniversalLogin(clientId, TenancyModes.MultiTenancy);
                if (loginModel != null)
                    loginModel.State = _stateTokenGenerator.GenerateToken();

                InitLocalLogin(loginModel);
                // redirect to the commonlogin.html page
                // end the response.
                return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
            }

            loginModel = await SetClientSpecificLogin(companyCode, clientId, authServerMode);

            if (loginModel != null)
                loginModel.State = _stateTokenGenerator.GenerateToken();

            string idProvider = GetClientRegisteredProviders(clientId, companyCode);

            switch (authServerMode)
            {
                default:
                case TenancyModes.MultiTenancy:
                    // if the login is Social Logins [FB, TWT, MS, etc...]
                    if (loginModel.Providers != null && loginModel.Providers.Count == 1 && (idProvider.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.SocialLoginsProviderId)) || idProvider.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.LDAPAuthProviderId))))
                    {
                        var provider = loginModel.Providers[0];
                        loginModel.Provider = provider.Name;
                        //re-direct without showing options page
                        return RedirectToRoute(Routes.ExternalLoginRoute, new
                        {
                            providerid = provider.ProviderId,
                            provider = provider.Name,
                            companyCode = loginModel.CompanyCode,
                            client_Id = loginModel.ClientId
                        });
                    }

                    if ((loginModel.Providers != null && loginModel.Providers.Count == 1 && idProvider.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId))) || (loginModel.Providers == null || loginModel.Providers.Count < 1))
                    {
                        loginModel.AuthenticationTypeId = AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId);
                        InitLocalLogin(loginModel);
                    }
                    break;
                case TenancyModes.SingleTenant:
                    InitLocalLogin(loginModel);
                    break;
            }

            return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
        }

        [Route(Routes.ExternalLoginRoute, Name = Routes.ExternalLoginRouteName)]
        [HttpGet]
        public async Task<IHttpActionResult> LoginExternal(string provider, string client_Id, string providerid, string companyCode = null)
        {
            Request.GetOwinContext().Environment.Add("provider", provider);
            Request.GetOwinContext().Environment.Add("providerid", providerid);

            string _clientId = null, companycode = VerifyLoginRequestMessage(out _clientId);

            if (string.IsNullOrEmpty(_clientId) || string.IsNullOrEmpty(companycode))
            {
                var client = await _clientService.GetAsync(client_Id);
                if (client != null && !string.IsNullOrEmpty(client.Uri))
                    return Redirect(client.Uri);
                else
                    _clientId = client.Id.ToString();
            }

            if (!_clientId.Equals(client_Id))
                return Unauthorized();

            if (!IsCommonLoginEnabled && !string.IsNullOrEmpty(companyCode) && !companyCode.EqualsIgnoreCase(companyCode))
            {
                return Unauthorized();
            }

            var ctx = Request.GetOwinContext();
            var authProp = new Microsoft.Owin.Security.AuthenticationProperties
            {
                RedirectUri = new UriBuilder
                {
                    Scheme = Request.RequestUri.Scheme,
                    Host = Request.RequestUri.Host,
                    Port = Request.RequestUri.Port,
                    Path = "/api/authentication/LoginExternalCallback",
                    Query = new Dictionary<string, string>
                    {
                        {"companyCode",companyCode},
                        {AuthServerConstants.AuthorizeRequest.ClientId,client_Id},
                        {"providerName",provider},
                        {"providerid",providerid},
                        {"prompt","login"}
                    }.AsQueryParameters()
                }.ToString()
            };
            Request.GetOwinContext().Authentication.Challenge(authProp, provider);

            return Unauthorized();
        }

        [Route(Routes.LocalLoginRoute, Name = Routes.LocalLoginRouteName)]
        [HttpPost]
        public async Task<IHttpActionResult> LocalLogin(LoginViewModel loginViewModel)
        {
            InputSanitizer.SanitizeInputs(loginViewModel);

            IHttpActionResult result = await ValidateLoginInputParameters(loginViewModel);
            if (result != null) return result;

            if (!_stateTokenGenerator.IsValid(loginViewModel.State))
            {
                var loginModel = new LoginModel
                {
                    UserName = loginViewModel.UserName,
                    Message = "Request is Stale",
                    State = _stateTokenGenerator.GenerateToken(),
                    PostbackUrl = new UriBuilder
                    {
                        Host = Request.RequestUri.Host,
                        Port = Request.RequestUri.Port,
                        Scheme = Request.RequestUri.Scheme,
                        Path = "/api/authentication/locallogin"
                    }.ToString(),
                    Provider = "local"
                };
                loginModel.AccessKey = loginModel.State;
                return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
            }

            return await AuthenticateAndSignIn(loginViewModel);
        }

        [HttpPost]
        [Route(Routes.UpdateFirsttimeUserRoute, Name = Routes.UpdateFirsttimeUserRouteName)]
        public async Task<IHttpActionResult> UpdateFirsttimeUser(LoginViewModel loginViewModel)
        {
            InputSanitizer.SanitizeInputs(loginViewModel);

            // to facilitate the view page identification
            if (loginViewModel == null)
                loginViewModel = new LoginViewModel { Message = UserStatus.FirstTimeUser.ToString("F") };

            IHttpActionResult result = null;

            // regular password validation is not required here
            result = await ValidateLoginInputParameters(loginViewModel, true, true, false, UserStatus.FirstTimeUser);
            if (result != null) return result;

            // first time user specific validations
            result = await ValidateFirsttimeInputs(loginViewModel);
            if (result != null) return result;

            var ctx = await _userService.UpdateFirsttimeUser(loginViewModel);

            if (ctx != null && ctx.IsError)
            {
                loginViewModel.Message = ctx.ErrorMessage;
                loginViewModel.State = _stateTokenGenerator.GenerateToken();
                string targetUri = GetStatusBasedTargetUri(UserStatus.FirstTimeUser, string.Empty);
                return new LoginViewResult<LoginModel>(GetLoginModelFromViewModel(loginViewModel, ctx.ErrorMessage, UserStatus.FirstTimeUser, targetUri), IsCommonLoginEnabled);
            }
            loginViewModel.Password = loginViewModel.NewPassword;

            // update is successful
            return await AuthenticateAndSignIn(loginViewModel);
        }

        [HttpPost]
        [Route(Routes.UpdatePasswordRoute, Name = Routes.UpdatePasswordRouteName)]
        public async Task<IHttpActionResult> UpdatePassword(LoginViewModel loginViewModel)
        {
            InputSanitizer.SanitizeInputs(loginViewModel);

            // to facilitate the view page identification
            if (loginViewModel == null)
                loginViewModel = new LoginViewModel
                {
                    Status = UserStatus.PasswordChangeForced,
                    Message = UserStatus.PasswordChangeForced.ToString("F")
                };

            IHttpActionResult result;
            // regular password validation is not required here
            result = await ValidateLoginInputParameters(loginViewModel, true, false, true, UserStatus.PasswordChangeForced);
            if (result != null) return result;
            // first time user specific validations
            result = await ValidatePasswordChangeInputs(loginViewModel, true, UserStatus.PasswordChangeForced);
            if (result != null) return result;

            var ctx = await _userService.UpdateUserPassword(loginViewModel);

            if (ctx != null && ctx.IsError)
            {
                return new LoginViewResult<LoginModel>(new LoginModel
                {
                    CompanyCode = loginViewModel.CompanyCode,
                    UserName = loginViewModel.UserName,
                    ProviderId = loginViewModel.ProviderId,
                    Message = ctx.ErrorMessage,
                    Status = UserStatus.PasswordChangeForced,
                    State = _stateTokenGenerator.GenerateToken(),
                    PostbackUrl = this.Request.GetUrlHelper().Route(Routes.UpdatePasswordRouteName, new Dictionary<string, object>())
                }, IsCommonLoginEnabled);
            }

            loginViewModel.Password = loginViewModel.ConfirmPassword;

            // update is successful
            return await AuthenticateAndSignIn(loginViewModel);
        }

        [HttpGet]
        public async Task<IHttpActionResult> LoginExternalCallback(string companyCode, string client_Id, string providerName, string providerid)
        {
            var ctx = Request.GetOwinContext();

            var requestParameters = Request.RequestUri.ParseQueryString();

            string currentSubject = null;
            var currentAuth = await ctx.Authentication.AuthenticateAsync(AuthServerConstants.AuthenticationMethods.CelloAuthentication);

            if (currentAuth != null && currentAuth.Identity != null && currentAuth.Identity.IsAuthenticated)
            {
                var subjClaim = currentAuth.Identity.Claims.Where(c => c.Type.Equals(AuthServerConstants.ClaimTypes.Subject)).FirstOrDefault();

                if (subjClaim != null && subjClaim != default(Claim))
                {
                    currentSubject = subjClaim.Value;
                }
            }

            var externalAuthResult = await ctx.Authentication.AuthenticateAsync(AuthServerConstants.ExternalAuthenticationType);

            var cookies = ctx.Request.Cookies != null && ctx.Request.Cookies.Count() > 0 && ctx.Request.Cookies[AuthServerConstants.ExternalAuthenticationType] != null ? ctx.Request.Cookies[AuthServerConstants.ExternalAuthenticationType] : null;

            if (externalAuthResult == null || externalAuthResult.Identity == null || !externalAuthResult.Identity.Claims.Any())
            {
                string errorMessage = "External Authentication Failed";
                var idProviderErrors = AppSettingHelpers.IdProviderErrorKeys;

                if (idProviderErrors != null && idProviderErrors.ContainsKey(providerName))
                {
                    StringBuilder errorMsg = new StringBuilder();

                    idProviderErrors[providerName].ForEach(v =>
                    {
                        if (!string.IsNullOrEmpty(requestParameters[v]))
                            errorMsg.AppendFormat("Error Code : {0} and Message : {1}", v, requestParameters[v]);
                    });
                    errorMessage = errorMsg.ToString();
                }

                return await ErrorLoginViewResult(companyCode, client_Id, errorMessage);
            }

            var claims = TransformExternalClaims(externalAuthResult);

#if DEBUG || TEST || STAGING
            if (AppSettingHelpers.GetValue<bool>(AppSettingConstants.LogExceptions))
                Task.Run(() =>
                {
                    LogService.Write(string.Format(CultureInfo.InvariantCulture, "claims:{0} & values: {1}", string.Join(",", claims.Select(x => x.Type).ToList()), string.Join(",", claims.Select(x => x.Value).ToList())));
                });
#endif

            #region auth type identification
            string authTypeId = null;

            var authTypes = await _consentService.GetAuthenticationTypesAsync(new string[] { providerid });

            if (authTypes.IsNullOrEmpty())
            {
                //LoginModel loginModel = await SetClientSpecificLogin(companyCode, client_Id, "No Matching Authentication types found");
                //loginModel.State = _stateTokenGenerator.Get();
                //return new LoginViewResult<LoginModel>(loginModel);
                return await ErrorLoginViewResult(companyCode, client_Id, "No Matching Authentication types found");
            }

            foreach (var at in authTypes)
            {
                if (at.Value.Name.EqualsIgnoreCase(providerName) || at.Value.Description.EqualsIgnoreCase(providerName))
                {
                    authTypeId = at.Key;
                    break;
                }
            }

            #endregion

            var externalIdentity = GetExternalIdentity(claims.ToList(), providerid, authTypeId);
            if (externalIdentity == null)
            {
                return await ErrorLoginViewResult(companyCode, client_Id, "No Matching External Account");
            }

            var authResult = await _userService.AuthenticateExternalAsync(currentSubject ?? externalIdentity.ProviderId, companyCode, client_Id, externalIdentity);

            if (authResult == null)
            {
                return await ErrorLoginViewResult(companyCode, client_Id, "No Matching External Account");
            }

            // If a user needs to be registered
            if (authResult != null && authResult.UserAuthenticationStatus == UserStatus.RequiresRegistration)
            {
#if DEBUG || TEST || STAGING
                if (AppSettingHelpers.GetValue<bool>(AppSettingConstants.LogExceptions))
                    Task.Run(() =>
                    {
                        LogService.Write(string.Join(Environment.NewLine, claims.Select(c => string.Format("Claim Name: {0} & claim Value: {1}", c.Type, c.Value))));
                    });
#endif

                return await DoRegistration(companyCode, client_Id, providerName, providerid, claims, authTypeId, authTypes);
            }

            if (authResult.IsError)
            {
                return await ErrorLoginViewResult(companyCode, client_Id, authResult.ErrorMessage);
            }

            return await SignInAndRedirect(authResult, AuthServerConstants.AuthenticationMethods.External, authResult.Provider, providerid, client_Id);
        }

        [HttpPost]
        [Route(Routes.RegisterRoute, Name = Routes.RegisterRouteName)]
        public async Task<IHttpActionResult> Register(UserRegistrationInfo registrationInfo)
        {
            InputSanitizer.SanitizeInputs(registrationInfo);

            // to facilitate the view page identification
            if (registrationInfo == null)
            {
                var message = Request.GetOwinContext().Request.Cookies[LoginRequestCookie];
                return RedirectToRoute(Routes.LoginRoute, new Dictionary<string, object>()
                {
                    {"message", message}
                });
            }

            var regInfo = TryParseInvitationCookie();

            if (regInfo != null)
            {
                registrationInfo.Identifier = regInfo.Identifier;
                registrationInfo.CompanyCode = regInfo.CompanyCode;
                registrationInfo.TenantId = regInfo.TenantId;
            }

            registrationInfo.PostbackUrl = this.Request.GetUrlHelper().Route(Routes.RegisterRoute, new { });

            IHttpActionResult result = null;

            if (registrationInfo.ProviderId.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId))
                || registrationInfo.ProviderId.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.LDAPAuthProviderId)))
            {
                result = ValidateRegistrationParameters(registrationInfo, true);
            }
            else
            {
                result = ValidateRegistrationParameters(registrationInfo);
            }

            if (result != null) return result;

            if (string.IsNullOrEmpty(registrationInfo.TenantId) && !string.IsNullOrEmpty(registrationInfo.CompanyCode))
            {
                registrationInfo.TenantId = _tenantService.GetTenantIdFromTenantCode(registrationInfo.CompanyCode);
            }

            CelloAuthenticationContext ctx = null;

            // Register the user
            ctx = await _userService.Register(registrationInfo);

            if (ctx != null && ctx.IsError)
            {
                registrationInfo.Message = ctx.ErrorMessage;
                registrationInfo.State = _stateTokenGenerator.GenerateToken();
                return new RegistrationResult(registrationInfo);
            }

            //Set a default password to facilitate the automatic user login after successful registration
            // update is successful
            // This never runs in the context of first time user or is never password updatable
            return await AuthenticateAndSignIn(new LoginViewModel
            {
                CompanyCode = registrationInfo.CompanyCode,
                UserName = registrationInfo.UserName,
                EmailId = registrationInfo.EmailId,
                Password = AppSettingHelpers.GetValue<string>(AppSettingConstants.DefaultUserPassword),
                ClientId = registrationInfo.ClientId
            }, UserStatus.None, true);
        }

        [HttpGet]
        [Route(Routes.RegisterUserRoute, Name = Routes.RegisterUserRouteName)]
        public async Task<IHttpActionResult> CelloRegistration(string client_id, string company_code, string access_key)
        {
            UserRegistrationInfo registrationInfo = null;

            registrationInfo = TryParseInvitationCookie();

            if (registrationInfo == null)
            {
                registrationInfo = new UserRegistrationInfo
                {
                    ClientId = null,
                    ClientName = null,
                    TenantId = null,
                    CompanyCode = null,
                    ProviderId = null,
                    ProviderName = null,
                    AuthenticationTypeId = null,
                    AuthenticationType = null
                };
            }
            else
            {
                var userDetail = ServiceLocator.Resolve<CelloSaaS.ServiceContracts.UserManagement.IUserDetailsService>().GetUserDetailsByUserId(registrationInfo.Identifier, registrationInfo.TenantId);

                if (userDetail != null && userDetail.MembershipDetails != null)
                {
                    registrationInfo.UserName = userDetail.MembershipDetails.UserName;
                    registrationInfo.EmailId = userDetail.MembershipDetails.EmailId;
                    registrationInfo.FirstName = userDetail.User.FirstName;
                    registrationInfo.LastName = userDetail.User.LastName;
                }
            }

            registrationInfo.State = _stateTokenGenerator.GenerateToken();
            registrationInfo.PostbackUrl = this.Request.GetUrlHelper().Route(Routes.RegisterRoute, new { });

            if (!_stateTokenGenerator.IsValid(access_key))
            {
                registrationInfo.Message = "invalid request ";
                return new RegistrationResult(registrationInfo);
            }

            if (string.IsNullOrEmpty(client_id))
            {
                registrationInfo.Message = "Client Identity or is null or empty";
                return new RegistrationResult(registrationInfo);
            }

            if (!IsCommonLoginEnabled && string.IsNullOrEmpty(company_code))
            {
                registrationInfo.Message = "Client Identity or company information is null or empty";
                return new RegistrationResult(registrationInfo);
            }

            var clientDetails = await _clientService.GetAsync(client_id);

            if (clientDetails == null || clientDetails.TenantId.IsEmpty())
            {
                registrationInfo.Message = "Client information is null or empty";
                return new RegistrationResult(registrationInfo);
            }

            string tenantId = null;

            if (!IsCommonLoginEnabled)
            {
                tenantId = _tenantService.GetTenantIdFromTenantCode(company_code);

                if (string.IsNullOrEmpty(tenantId))
                {
                    registrationInfo.Message = "Unable to infer the tenant information from the company code";
                    return new RegistrationResult(registrationInfo);
                }
            }

            registrationInfo.ProviderId = AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId);
            var authProviders = await _consentService.GetAuthenticationProviders();

            if (authProviders == null || authProviders.Count < 1 || !authProviders.ContainsKey(registrationInfo.ProviderId))
            {
                registrationInfo.Message = "Unable to infer the authentication providers information ";
                return new RegistrationResult(registrationInfo);
            }

            var authTypes = await _consentService.GetAuthenticationTypesAsync(new string[] { registrationInfo.ProviderId });
            var authType = authTypes.Values.Where(at => at.ProviderId.Equals(registrationInfo.ProviderId.ToGuid())).FirstOrDefault();

            if (authTypes == null || authTypes.Count < 1 || authType == null)
            {
                registrationInfo.Message = "Unable to infer the authentication types information ";
                return new RegistrationResult(registrationInfo);
            }

            registrationInfo.ClientId = clientDetails.Id.ToString();
            registrationInfo.ClientName = clientDetails.Name;
            registrationInfo.TenantId = tenantId;
            registrationInfo.CompanyCode = company_code;
            registrationInfo.ProviderName = authProviders[registrationInfo.ProviderId].Name;
            registrationInfo.AuthenticationTypeId = authType.Id.ToString();
            registrationInfo.AuthenticationType = authType.Name;
            return new RegistrationResult(registrationInfo);
        }

        //[Route(Routes.LogOffRoute, Name = Routes.LogOffRouteName)]
        [HttpGet]
        public IHttpActionResult Logout(string client_id, string redirecturi)
        {
            try
            {
                Request.GetOwinContext().Authentication.SignOut(new string[] 
                { 
                    AuthServerConstants.ExternalAuthenticationType, AuthServerConstants.PrimaryAuthenticationType 
                });

                ClearLoginCookie();
                ClearLoginRequestMessage();

            }
            catch (Exception ex)
            {
                return new ResponseContentResult<string>("Not able to logout from the Authentication Provider" + ex.Message);
            }

            if (AppSettingHelpers.GetValue<bool>(AppSettingConstants.LogOffAutoRedirectionKey))
            {
                redirecturi = string.Format(CultureInfo.InvariantCulture, "{0}&status=logoutsuccess", redirecturi);
                return Redirect(redirecturi);
            }

            return new LogoutResult(redirecturi);
        }

        #region Private Members

        private async Task<IHttpActionResult> DoRegistration(string companyCode, string client_Id, string providerName, string providerid, IEnumerable<Claim> claims, string authTypeId, Dictionary<string, AuthenticationType> authTypes)
        {
            var clientDetails = await _clientService.GetAsync(client_Id);

            if (clientDetails == null || clientDetails.TenantId.IsEmpty())
            {
                //throw error
                return await ErrorLoginViewResult(companyCode, client_Id, "Invalid Client Information");
            }

            string tenantId = _tenantService.GetTenantIdFromTenantCode(companyCode);

            if (string.IsNullOrEmpty(tenantId))
            {
                // do not allow user self registration
                return await ErrorLoginViewResult(companyCode, client_Id, "Not able to identify the tenant details from the given company code");
            }

            var settings = _tenantSettingsService.Get(tenantId);

            if (string.IsNullOrEmpty(settings.GetValue(SettingAttributeConstants.AllowUserRegistration))
                || !Convert.ToBoolean(settings.GetValue(SettingAttributeConstants.AllowUserRegistration)))
            {
                // do not allow user self registration
                return await ErrorLoginViewResult(companyCode, client_Id, "User details not found / Admin does not allow user registration");
            }

            // note down the provider id,  provider name, auth provider id [userid] , name , email etc...
            var registrationInfo = new UserRegistrationInfo
            {
                State = _stateTokenGenerator.GenerateToken(),
                PostbackUrl = this.Request.GetUrlHelper().Route(Routes.RegisterRoute, new { }),
                ClientId = clientDetails.Id.ToString(),
                ClientName = clientDetails.Name,
                TenantId = tenantId,
                CompanyCode = companyCode,
                ProviderId = providerid,
                ProviderName = providerName,
                AuthenticationTypeId = authTypeId,
                AuthenticationType = authTypes[authTypeId].Name
            };
            var eMailClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.Email || x.Type == AuthServerConstants.ClaimTypes.Email);
            if (eMailClaim != null && !string.IsNullOrEmpty(eMailClaim.Value))
                registrationInfo.EmailId = eMailClaim.Value;

            var userNameClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.Name || x.Type == AuthServerConstants.ClaimTypes.Name);
            if (userNameClaim != null && !string.IsNullOrEmpty(userNameClaim.Value))
                registrationInfo.UserName = userNameClaim.Value;

            var firstNameClaim = claims.FirstOrDefault(c => c.Type.EqualsIgnoreCase(ClaimTypes.GivenName));
            if (firstNameClaim != null && !string.IsNullOrEmpty(firstNameClaim.Value))
                registrationInfo.FirstName = firstNameClaim.Value;

            var lastNameClaim = claims.FirstOrDefault(c => c.Type.EqualsIgnoreCase(ClaimTypes.Surname));
            if (lastNameClaim != null && !string.IsNullOrEmpty(lastNameClaim.Value))
                registrationInfo.LastName = lastNameClaim.Value;

            var idClaim = claims.FirstOrDefault(c => c.Type.EqualsIgnoreCase(ClaimTypes.NameIdentifier));
            if (idClaim != null && !string.IsNullOrEmpty(idClaim.Value))
                registrationInfo.ProviderUserId = idClaim.Value;

            return new RegistrationResult(registrationInfo);
        }

        private async Task<IHttpActionResult> AuthenticateAndSignIn(LoginViewModel loginViewModel, UserStatus status = UserStatus.None, bool isRegistered = false)
        {
#if DEBUG || TEST || STAGING

            if (AppSettingHelpers.GetValue<bool>(AppSettingConstants.LogExceptions))
            {
                Task.Run(() =>
        {
            LogService.Write("logging in user details : {0}-{1}-{2}", loginViewModel.CompanyCode ?? string.Empty, loginViewModel.UserName, loginViewModel.Password);
        });
            }
#endif
            var authContext = await _userService.AuthenticateUserAsync(loginViewModel.CompanyCode, loginViewModel.UserName, loginViewModel.Password);

            // No error, sign in and continue
            if (!authContext.IsError)
            {
                return await SignInAndRedirect(authContext, AuthServerConstants.AuthenticationMethods.CelloAuthentication, AuthServerConstants.AuthenticationMethods.CelloAuthentication, loginViewModel.AuthenticationTypeId, loginViewModel.ClientId);
            }

            var targetUri = GetStatusBasedTargetUri(status, authContext.ErrorMessage);

            // User status based page will be displayed
            if (authContext.UserAuthenticationStatus != UserStatus.None)
            {
                loginViewModel.Status = authContext.UserAuthenticationStatus;
                loginViewModel.Message = authContext.UserAuthenticationStatus.ToString("F");
                loginViewModel.State = _stateTokenGenerator.GenerateToken();
                LoginModel model = new LoginModel
                {
                    Message = loginViewModel.Message,
                    ClientId = loginViewModel.ClientId,
                    CompanyCode = loginViewModel.CompanyCode,
                    ProviderId = loginViewModel.ProviderId ?? AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId),
                    State = _stateTokenGenerator.GenerateToken(),
                    UserName = loginViewModel.UserName,
                    PostbackUrl = targetUri,
                    AuthenticationTypeId = loginViewModel.AuthenticationTypeId
                };
                model.Status = loginViewModel.Status;
                model.AccessKey = model.State;
                return new LoginViewResult<LoginViewModel>(model, IsCommonLoginEnabled);
            }

            if (isRegistered)
            {
                var clientDetail = await _clientService.GetAsync(loginViewModel.ClientId);

                if (clientDetail != null && !string.IsNullOrEmpty(clientDetail.RedirectUris))
                {
                    var redirectUri = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(clientDetail.RedirectUris);
                    return new CelloSaaS.AuthServer.Core.Results.Login.RegistrationSuccessResult(redirectUri.FirstOrDefault());
                }
            }

            LoginModel loginModel = null;

            if (IsCommonLoginEnabled)
            {
                // get all the regd providers, except the ldap provider
                loginModel = await InitiateUniversalLogin(loginViewModel.ClientId, TenancyModes.MultiTenancy);
                if (loginModel != null)
                    loginModel.State = _stateTokenGenerator.GenerateToken();
                loginModel.AccessKey = loginModel.State;
                loginModel.Message = authContext.ErrorMessage;

                InitLocalLogin(loginModel);
                // redirect to the commonlogin.html page
                // end the response.
                return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
            }
            else
            {
                loginModel = new LoginModel
                                {
                                    Status = authContext.UserAuthenticationStatus,
                                    Message = authContext.ErrorMessage,
                                    CompanyCode = loginViewModel.CompanyCode,
                                    UserName = loginViewModel.UserName,
                                    State = _stateTokenGenerator.GenerateToken(),
                                    ProviderId = loginViewModel.ProviderId ?? AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId),
                                    AuthenticationTypeId = loginViewModel.AuthenticationTypeId,
                                    PostbackUrl = targetUri
                                };
            }
            loginModel.AccessKey = loginModel.State;
            return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
        }

        private string GetStatusBasedTargetUri(UserStatus status, string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage) && errorMessage.EqualsIgnoreCase(UserStatus.FirstTimeUser.ToString("F")))
            {
                return this.Request.GetUrlHelper().Route(Routes.UpdateFirsttimeUserRouteName, new Dictionary<string, object>());
            }
            else if (!string.IsNullOrEmpty(errorMessage) && errorMessage.EqualsIgnoreCase(UserStatus.PasswordChangeForced.ToString("F")))
            {
                return this.Request.GetUrlHelper().Route(Routes.UpdatePasswordRouteName, new Dictionary<string, object>());
            }

            if (status != UserStatus.None && status.Equals(UserStatus.FirstTimeUser))
            {
                return this.Request.GetUrlHelper().Route(Routes.UpdateFirsttimeUserRouteName, new Dictionary<string, object>());
            }

            if (status != UserStatus.None && status.Equals(UserStatus.PasswordChangeForced))
            {
                return this.Request.GetUrlHelper().Route(Routes.UpdatePasswordRouteName, new Dictionary<string, object>());
            }

            return null;
        }

        private async Task<IHttpActionResult> ErrorLoginViewResult(string companyCode, string clientId, string errorMessage)
        {
            LoginModel loginModel = await SetClientSpecificLogin(companyCode, clientId, TenancyModes.MultiTenancy, errorMessage);
            loginModel.AccessKey = loginModel.AccessKey ?? _stateTokenGenerator.GenerateToken();
            return new LoginViewResult<LoginModel>(loginModel, IsCommonLoginEnabled);
        }

        private IEnumerable<Claim> TransformExternalClaims(Microsoft.Owin.Security.AuthenticateResult externalAuthResult)
        {
            var claims = externalAuthResult.Identity.Claims.ToList();
            return claims;
        }

        private async Task<LoginModel> InitiateUniversalLogin(string clientId, TenancyModes tenancyMode, string message = null)
        {
            var loginModel = new LoginModel
            {
                CompanyCode = null,
                ClientId = clientId
            };

            var ctx = Request.GetOwinContext();

            var authenticationProviders = await _consentService.GetAuthenticationProviders();

            if (authenticationProviders == null) return null;

            if (tenancyMode == TenancyModes.SingleTenant)
                return null;

            loginModel.Providers = null;

            if (!string.IsNullOrEmpty(message)) loginModel.Message = message;

            var authTypes = await _consentService.GetAuthenticationTypesAsync(null);

            foreach (var authProvider in ctx.Authentication.GetAuthenticationTypes(at => !string.IsNullOrEmpty(at.Caption)))
            {
                var providerId = authTypes.Where(atyp => atyp.Value.Name.Equals(authProvider.Caption, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().Value.ProviderId.ToString();

                if (loginModel.Providers == null) loginModel.Providers = new List<ExternalLoginProviders>();

                //if (providerId.Equals(AppSettingHelpers.GetValue<string>(AppSettingConstants.SocialLoginsProviderId).ToGuid()))
                //{
                loginModel.Providers.Add(new ExternalLoginProviders
                {
                    Name = authProvider.Caption,
                    ProviderId = providerId,
                    Url = Url.Route(Routes.ExternalLoginRoute, new
                    {
                        provider = authProvider.AuthenticationType,
                        companyCode = string.Empty,
                        client_id = clientId,
                        //providerid = authenticationProviders.Where(atp => atp.Value.Equals(at.Caption)).FirstOrDefault().Key
                        providerid = providerId
                    })
                });
                //}
                //else if (providerId.Equals(AppSettingHelpers.GetValue<string>(AppSettingConstants.LDAPAuthProviderId).ToGuid()))
                //{
                //    continue;
                //}
            }
            return loginModel;

        }

        private async Task<LoginModel> SetClientSpecificLogin(string companyCode, string clientId, TenancyModes tenancyMode, string message = null)
        {
            var loginModel = new LoginModel
            {
                CompanyCode = companyCode,
                ClientId = clientId
            };

            var ctx = Request.GetOwinContext();

            var authenticationProviders = await _consentService.GetAuthenticationProviders();

            if (authenticationProviders == null) return null;

            string configuredProviderId = GetClientRegisteredProviders(clientId, companyCode);

            if (tenancyMode == TenancyModes.SingleTenant || string.IsNullOrEmpty(configuredProviderId))
                configuredProviderId = AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId);

            loginModel.Providers = null;

            if (!string.IsNullOrEmpty(message)) loginModel.Message = message;

            AuthenticationProvider configuredProvider = null;

            foreach (var provider in authenticationProviders)
            {
                if (provider.Key.Equals(configuredProviderId, StringComparison.OrdinalIgnoreCase))
                {
                    configuredProvider = provider.Value;
                    break;
                }
            }

            // No valid providers found
            if (configuredProvider == null)
                return null;

            if (!configuredProvider.Id.Equals(AppSettingHelpers.GetValue<string>(AppSettingConstants.SocialLoginsProviderId).ToGuid())
                && !configuredProvider.Id.Equals(AppSettingHelpers.GetValue<string>(AppSettingConstants.LDAPAuthProviderId).ToGuid()))
                return loginModel;

            var authTypes = await _consentService.GetAuthenticationTypesAsync(null);

            foreach (var authProvider in ctx.Authentication.GetAuthenticationTypes(at => !string.IsNullOrEmpty(at.Caption)))
            {
                var providerId = authTypes.Where(atyp => atyp.Value.Name.Equals(authProvider.Caption, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().Value.ProviderId.ToString();

                if (loginModel.Providers == null) loginModel.Providers = new List<ExternalLoginProviders>();

                if (configuredProvider.Id.Equals(AppSettingHelpers.GetValue<string>(AppSettingConstants.SocialLoginsProviderId).ToGuid()))
                {
                    loginModel.Providers.Add(new ExternalLoginProviders
                    {
                        Name = authProvider.Caption,
                        ProviderId = providerId,
                        Url = Url.Route(Routes.ExternalLoginRoute, new
                        {
                            provider = authProvider.AuthenticationType,
                            companyCode = companyCode,
                            client_id = clientId,
                            //providerid = authenticationProviders.Where(atp => atp.Value.Equals(at.Caption)).FirstOrDefault().Key
                            providerid = providerId
                        })
                    });
                }
                else if (configuredProvider.Id.Equals(AppSettingHelpers.GetValue<string>(AppSettingConstants.LDAPAuthProviderId).ToGuid()))
                {
                    loginModel.Providers = new List<ExternalLoginProviders>();
                    loginModel.Providers.Add(new ExternalLoginProviders
                    {
                        Name = authProvider.Caption,
                        ProviderId = providerId,
                        Url = Url.Route(Routes.ExternalLoginRoute, new
                        {
                            provider = authProvider.AuthenticationType,
                            companyCode = companyCode,
                            client_id = clientId,
                            //providerid = authenticationProviders.Where(atp => atp.Value.Equals(at.Caption)).FirstOrDefault().Key
                            providerid = providerId
                        })
                    });
                }
            }
            return loginModel;
        }

        private void ClearLoginCookie()
        {
            #region For SystemWeb
            var extCookie = new HttpCookie(CookieAuthenticationDefaults.CookiePrefix + AuthServerConstants.ExternalAuthenticationType, ".")
            {
                Expires = DateTime.UtcNow.AddYears(-1),
                HttpOnly = true,
                Secure = Request.RequestUri.Scheme == Uri.UriSchemeHttps
            };

            var primaryAuthCookie = new HttpCookie(CookieAuthenticationDefaults.CookiePrefix + AuthServerConstants.PrimaryAuthenticationType,
                ".")
            {
                Expires = DateTime.UtcNow.AddYears(-1),
                HttpOnly = true,
                Secure = Request.RequestUri.Scheme == Uri.UriSchemeHttps
            };

            HttpContext.Current.Response.Cookies.Add(extCookie);
            HttpContext.Current.Response.Cookies.Add(primaryAuthCookie);
            #endregion

            #region For OwinPipeline
            var ctx = Request.GetOwinContext();
            var options = new Microsoft.Owin.CookieOptions
                {
                    Expires = DateTime.UtcNow.AddYears(-1),
                    HttpOnly = true,
                    Secure = Request.RequestUri.Scheme == Uri.UriSchemeHttps
                };
            ctx.Response.Cookies.Append(
                CookieAuthenticationDefaults.CookiePrefix + AuthServerConstants.ExternalAuthenticationType,
                ".",
                options);

            ctx.Response.Cookies.Append(
                CookieAuthenticationDefaults.CookiePrefix + AuthServerConstants.PrimaryAuthenticationType,
                ".",
                options);
            #endregion
        }

        private async Task<IHttpActionResult> SignInAndRedirect(CelloAuthenticationContext authNCtx, string authenticationMethod, string idProvider, string providerId, string clientId)
        {
            var signInMessage = LoadLoginRequestMessage();
            string issuer = AuthServerConstants.PrimaryAuthenticationType;

            var claims = new List<Claim>
            {
                new Claim(AuthServerConstants.ClaimTypes.Subject, authNCtx.Subject),
                new Claim(AuthServerConstants.ClaimTypes.Name, authNCtx.Name),
                new Claim(AuthServerConstants.ClaimTypes.AuthenticationMethod, authenticationMethod),
                new Claim(AuthServerConstants.ClaimTypes.IdentityProvider, issuer),
                new Claim(AuthServerConstants.ClaimTypes.Issuer, idProvider),
                new Claim(AuthServerConstants.ClaimTypes.AuthenticationTime, DateTime.Now.Ticks.ToString(), ClaimValueTypes.Integer)
            };

            if (authNCtx.RedirectClaims != null && authNCtx.RedirectClaims.Count > 0)
            {
                claims.AddRange(authNCtx.RedirectClaims);
            }

            return await DoSignIn(authNCtx, providerId, clientId, signInMessage, issuer, claims);
        }

        private async Task<IHttpActionResult> DoSignIn(CelloAuthenticationContext authNCtx, string providerId, string clientId, CelloAuthorizationContext signInMessage, string issuer, List<Claim> claims)
        {
            if (!string.IsNullOrEmpty(authNCtx.ProviderId))
                claims.Add(new Claim(AuthServerConstants.ClaimTypes.ProviderId, authNCtx.ProviderId));

            if (!string.IsNullOrEmpty(providerId))
            {
                claims.Add(new Claim(AuthServerConstants.ClaimTypes.ProviderId, providerId));
                claims.Add(new Claim(AuthServerConstants.ClaimTypes.ExtProviderId, providerId));
                claims.Add(new Claim(AuthServerConstants.ClaimTypes.AuthenticationTypeId, providerId));
            }

            var id = new ClaimsIdentity(claims, issuer);
            var principal = new ClaimsPrincipal(id);

            id = principal.Identities.First();

            Request.GetOwinContext().Authentication.SignOut(issuer);

            if (!string.IsNullOrEmpty(clientId))
            {
                var clientDetails = await _clientService.GetAsync(clientId);

                if (clientDetails != null)
                {
                    var authenticationProperties = new Microsoft.Owin.Security.AuthenticationProperties
                    {
                        ExpiresUtc = new DateTimeOffset(DateTime.UtcNow.AddSeconds(clientDetails.AccessTokenLifetime))
                    };
                    Request.GetOwinContext().Authentication.SignIn(authenticationProperties, id);
                }
            }
            else
            {
                Request.GetOwinContext().Authentication.SignIn(id);
            }
            // TODO -- manage this state better if we're doing redirect to custom page
            // would rather the redirect URL from request message put into cookie
            // and named with a nonce, then the resume url + nonce set as claim
            // in principal above so page being redirected to can know what url to return to
            ClearLoginRequestMessage();

            return new RedirectResponse(signInMessage.ReturnUrl);
        }

        private ExternalIdentity GetExternalIdentity(IEnumerable<Claim> claims, string providerid, string authTypeId)
        {
            if (claims == null || !claims.Any())
            {
                return null;
            }

            var subClaim = claims.FirstOrDefault(x => x.Type == AuthServerConstants.ClaimTypes.Subject);
            if (subClaim == null)
            {
                subClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

                if (subClaim == null)
                {
                    return null;
                }
            }

            claims = claims.Except<Claim>(new Claim[] { subClaim }).ToList();

            if (_externalClaimsFilter != null)
            {
                claims = _externalClaimsFilter.Filter(subClaim.Issuer, claims);
            }

            claims = claims ?? Enumerable.Empty<Claim>();
            claims = claims.Concat<Claim>(new Claim[] 
            {
                new Claim(AuthServerConstants.ClaimTypes.ExtProviderId, providerid), 
                new Claim(AuthServerConstants.ClaimTypes.AuthenticationTypeId, authTypeId) 
            });

            var extId = new ExternalIdentity
            {
                Provider = subClaim.Issuer,
                ProviderId = subClaim.Value,
                Claims = claims
            };

            var authServerSubClaim = claims.FirstOrDefault(c => c.Type.EqualsIgnoreCase(AuthServerConstants.ClaimTypes.Subject));
            if (authServerSubClaim != null && !string.IsNullOrEmpty(authServerSubClaim.Value))
            {
                extId.ProviderId = authServerSubClaim.Value;
            }

            return extId;
        }

        private string GetClientRegisteredProviders(string clientId, string companyCode)
        {
            if (string.IsNullOrEmpty(clientId)) return null;

            if (string.IsNullOrEmpty(companyCode)) return null;

            var tenantId = _tenantService.GetTenantIdFromTenantCode(companyCode);

            if (string.IsNullOrEmpty(tenantId)) return null;

            var clientDetail = _clientService.GetAsync(clientId).Result;

            if (clientDetail == null || clientDetail.TenantId.IsEmpty()) return null;

            var tenantSettings = _tenantSettingsService.Get(tenantId);

            // incase of a parent tenant not having an id provider and the child also does not have one.
            if (tenantSettings == null || tenantSettings.Count < 1 || !tenantSettings.ContainsKey(SettingAttributeConstants.IdentityProviders))
                return AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId);

            return tenantSettings.GetValue(SettingAttributeConstants.IdentityProviders);
        }

        /// <summary>
        /// Returns the company code from the authorization request
        /// </summary>
        /// <param name="message">jwt string</param>
        /// <returns>Company Code</returns>
        private string SaveLoginRequestMessage(string message, out string clientId)
        {
            clientId = null;

            // get the context from the jwt string 
            CelloAuthorizationContext authContext = message.FromJwt();

            if (!string.IsNullOrEmpty(authContext.ReturnUrl))
            {
                // set the client id from the redirect_uri
                authContext.ReturnUrl.TryGetQueryParameter(AuthServerConstants.AuthorizeRequest.ClientId, out clientId);
            }

            double timeOutSeconds = 0;

            if (!string.IsNullOrEmpty(clientId))
            {
                var client = _clientService.GetAsync(clientId).Result;

                if (client != null)
                {
                    timeOutSeconds = client.AccessTokenLifetime;
                }
            }

            var ctx = this.Request.GetOwinContext();

            var cookieOptions = new Microsoft.Owin.CookieOptions
                {
                    HttpOnly = true,
                    Secure = this.Request.RequestUri.Scheme == Uri.UriSchemeHttps,
                    // Domain = AppSettingHelpers.GetValue<string>(AppSettingConstants.CookieDomain)
                };

            if (timeOutSeconds > 0)
            {
                cookieOptions.Expires = DateTime.UtcNow.AddSeconds(timeOutSeconds);
            }

            ctx.Response.Cookies.Append(LoginRequestCookie, message, cookieOptions);
            return authContext.CompanyCode;
        }

        /// <summary>
        /// Returns the company code from the authorization request
        /// </summary>
        /// <returns>Company Code</returns>
        private string VerifyLoginRequestMessage(out string clientId)
        {
            clientId = null;
            var ctx = Request.GetOwinContext();
            if (ctx.Request.Cookies.Count() < 1 || !ctx.Request.Cookies.Any(cook => cook.Key.EqualsIgnoreCase(LoginRequestCookie)))
                return null;

            var message = ctx.Request.Cookies[LoginRequestCookie];

            if (string.IsNullOrEmpty(message))
                return null;

            CelloAuthorizationContext authNContext = null;

            try
            {
                authNContext = message.FromJwt();
            }
            catch (System.IdentityModel.Tokens.SecurityTokenValidationException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }

            if (!string.IsNullOrEmpty(authNContext.ReturnUrl))
                authNContext.ReturnUrl.TryGetQueryParameter(AuthServerConstants.AuthorizeRequest.ClientId, out clientId);

            return authNContext.CompanyCode;
        }

        /// <summary>
        /// Gets the authorization context form the cooke
        /// </summary>
        /// <returns>
        /// The Cello Authorization Context
        /// <paramref name="CelloAuthorizationContext"/>
        /// </returns>
        private CelloAuthorizationContext LoadLoginRequestMessage()
        {
            var ctx = Request.GetOwinContext();
            var message = ctx.Request.Cookies[LoginRequestCookie];

            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentException("Login Request cookie is empty.");
            }
            return message.FromJwt();
        }

        /// <summary>
        /// Clears the login request cookie
        /// </summary>
        private void ClearLoginRequestMessage()
        {
            var ctx = Request.GetOwinContext();
            ctx.Response.Cookies.Append(
                LoginRequestCookie,
                ".",
                new Microsoft.Owin.CookieOptions
                {
                    Expires = DateTime.UtcNow.AddYears(-1),
                    HttpOnly = true,
                    Secure = Request.RequestUri.Scheme == Uri.UriSchemeHttps
                });
        }

        private IHttpActionResult ValidateRegistrationParameters(UserRegistrationInfo registrationInfo, bool SkipProviderUserId = false)
        {
            if (registrationInfo == null)
            {
                var ctx = Request.GetOwinContext();
                var message = ctx.Request.Cookies[LoginRequestCookie];
                return RedirectToRoute(Routes.LoginRoute, new Dictionary<string, object>()
                {
                    {"message", message}
                });
            }

            if (string.IsNullOrEmpty(registrationInfo.State) || !_stateTokenGenerator.IsValid(registrationInfo.State))
            {
                return RegistrationErrorResult(registrationInfo, "The Request is Stale");
            }

            if (string.IsNullOrEmpty(registrationInfo.CompanyCode) || string.IsNullOrEmpty(registrationInfo.UserName))
            {
                return RegistrationErrorResult(registrationInfo, "The Login parameters are invalid or are empty / null");
            }

            if (string.IsNullOrEmpty(registrationInfo.ClientId))
            {
                return RegistrationErrorResult(registrationInfo, "Client Identification failed");
            }

            if (string.IsNullOrEmpty(registrationInfo.FirstName) || string.IsNullOrEmpty(registrationInfo.LastName))
            {
                return RegistrationErrorResult(registrationInfo, "User First / Last name is missing");
            }

            if (!string.IsNullOrEmpty(registrationInfo.Password) && !string.IsNullOrEmpty(registrationInfo.ConfirmPassword)
                && !registrationInfo.Password.EqualsIgnoreCase(registrationInfo.ConfirmPassword))
            {
                return RegistrationErrorResult(registrationInfo, "The password and confirmation password does not match.");
            }

            var passwordValidationService = ServiceLocator.Resolve<CelloSaaS.ServiceContracts.UserManagement.IPasswordValidationService>();


            if (!string.IsNullOrEmpty(registrationInfo.ProviderId)
              && !registrationInfo.ProviderId.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId)))
            {
                registrationInfo.Password = registrationInfo.ConfirmPassword = AppSettingHelpers.GetValue<string>(AppSettingConstants.DefaultUserPassword);
            }

            // if Cello OpenId , then mandate the password
            if (((!string.IsNullOrEmpty(registrationInfo.ProviderId)
                && !registrationInfo.ProviderId.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId)))
                && (string.IsNullOrEmpty(registrationInfo.Password) || !passwordValidationService.ValidatePassword(registrationInfo.Password).IsPasswordValid)))
            {
                return RegistrationErrorResult(registrationInfo, "User Password is either empty or does not meet the security requirements");
            }

            if (string.IsNullOrEmpty(registrationInfo.ProviderId))
            {
                return RegistrationErrorResult(registrationInfo, "Authentication provider inference failed");
            }
            if (!SkipProviderUserId && string.IsNullOrEmpty(registrationInfo.ProviderUserId))
            {
                return RegistrationErrorResult(registrationInfo, "Authentication Provider supplied User Id cannot be empty");
            }
            return null;
        }

        private IHttpActionResult RegistrationErrorResult(UserRegistrationInfo registrationInfo, string errorMessage)
        {
            registrationInfo.State = _stateTokenGenerator.GenerateToken();
            registrationInfo.Message = errorMessage;
            return new RegistrationResult(registrationInfo);
        }

        /// <summary>
        /// Validates the following paramters
        /// <para>State</para>
        /// <para>Company Code</para>
        /// <para>UserName</para>
        /// <para>Password</para>
        /// </summary>
        /// <param name="loginViewModel">The login model</param>
        /// <returns></returns>
        private async Task<IHttpActionResult> ValidateLoginInputParameters(LoginViewModel loginViewModel, bool skipPasswords = false, bool isFirsttime = false, bool isChangePassword = false, UserStatus status = UserStatus.None)
        {
            if (loginViewModel == null)
            {
                return new LoginViewResult<LoginViewModel>(new LoginViewModel
                {
                    Message = "The Login information is not received"
                });
            }

            if (string.IsNullOrEmpty(loginViewModel.State))
            {
                return ErrorLoginViewResult(loginViewModel, "The Request is Stale", status);
            }

            if (!IsCommonLoginEnabled && (string.IsNullOrEmpty(loginViewModel.CompanyCode) || string.IsNullOrEmpty(loginViewModel.UserName)))
            {
                return ErrorLoginViewResult(loginViewModel, "The Login parameters are invalid or are empty", status);
            }

            if (!skipPasswords && string.IsNullOrEmpty(loginViewModel.Password))
            {
                return ErrorLoginViewResult(loginViewModel, "The password is invalid", status);
            }

            if (isFirsttime && isChangePassword && !string.IsNullOrEmpty(loginViewModel.ProviderId)
                && (!loginViewModel.ProviderId.EqualsIgnoreCase(AppSettingHelpers.GetValue<string>(AppSettingConstants.CelloAuthProviderId))))
            {
                return ErrorLoginViewResult(loginViewModel, "Cannot change the user passwords for this login type", status);
            }

            return null;
        }

        private IHttpActionResult ErrorLoginViewResult(LoginViewModel loginViewModel, string errorMessage, UserStatus status = UserStatus.None)
        {
            var targetUri = GetStatusBasedTargetUri(status, string.Empty);
            return new LoginViewResult<LoginModel>(GetLoginModelFromViewModel(loginViewModel, errorMessage, status, targetUri), IsCommonLoginEnabled);
        }

        /// <summary>
        /// Validates the following parameters
        /// <para>Security Question</para>
        /// <para>Security Answer</para>
        /// <para>New Password</para>
        /// <para>Confirm Password</para>
        /// </summary>
        /// <param name="loginViewModel">The login view model</param>
        /// <returns></returns>
        private async Task<IHttpActionResult> ValidateFirsttimeInputs(LoginViewModel loginViewModel)
        {
            var targetUri = GetStatusBasedTargetUri(UserStatus.FirstTimeUser, string.Empty);

            if (string.IsNullOrEmpty(loginViewModel.SecurityQuestion))
            {
                return new LoginViewResult<LoginModel>(GetLoginModelFromViewModel(loginViewModel, "The Security Question chosen is Invalid", UserStatus.FirstTimeUser, targetUri), IsCommonLoginEnabled);
            }

            if (string.IsNullOrEmpty(loginViewModel.SecurityAnswer))
            {
                return new LoginViewResult<LoginModel>(GetLoginModelFromViewModel(loginViewModel, "The Security Answer chosen is Invalid", UserStatus.FirstTimeUser, targetUri), IsCommonLoginEnabled);
            }

            if (string.IsNullOrEmpty(loginViewModel.NewPassword) || string.IsNullOrEmpty(loginViewModel.ConfirmPassword)
                || !loginViewModel.NewPassword.EqualsIgnoreCase(loginViewModel.ConfirmPassword))
            {
                return new LoginViewResult<LoginModel>(GetLoginModelFromViewModel(loginViewModel, "The Password does not meet the security requirements", UserStatus.FirstTimeUser, targetUri), IsCommonLoginEnabled);
            }
            return null;
        }

        private async Task<IHttpActionResult> ValidatePasswordChangeInputs(LoginViewModel loginViewModel, bool comparePasswords = true, UserStatus status = UserStatus.None)
        {
            var targetUri = GetStatusBasedTargetUri(status, string.Empty);

            if (string.IsNullOrEmpty(loginViewModel.NewPassword) || string.IsNullOrEmpty(loginViewModel.ConfirmPassword)
                || !loginViewModel.NewPassword.EqualsIgnoreCase(loginViewModel.ConfirmPassword))
                return new LoginViewResult<LoginModel>(GetLoginModelFromViewModel(loginViewModel, "The Password does not meet the security requirements", status, targetUri), IsCommonLoginEnabled);

            if (comparePasswords && loginViewModel.Password.EqualsIgnoreCase(loginViewModel.NewPassword))
                return new LoginViewResult<LoginModel>(GetLoginModelFromViewModel(loginViewModel, "The Password does not meet the security requirements", status, targetUri), IsCommonLoginEnabled);

            return null;
        }

        private LoginModel GetLoginModelFromViewModel(LoginViewModel loginViewModel, string errorMessage, UserStatus status, string targetUri)
        {
            return new LoginModel
            {
                Message = errorMessage,
                State = _stateTokenGenerator.GenerateToken(),
                Status = status,
                AccessKey = loginViewModel.AccessKey ?? _stateTokenGenerator.GenerateToken(),
                AuthenticationTypeId = loginViewModel.AuthenticationTypeId,
                ClientId = loginViewModel.ClientId,
                CompanyCode = loginViewModel.CompanyCode,
                EmailId = loginViewModel.EmailId,
                FirstName = loginViewModel.FirstName,
                Identifier = loginViewModel.Identifier,
                LastName = loginViewModel.LastName,
                MiddleName = loginViewModel.MiddleName,
                PostbackUrl = targetUri,
                Provider = loginViewModel.Provider,
                ProviderId = loginViewModel.ProviderId,
                SecurityQuestion = loginViewModel.SecurityQuestion,
                TenantId = loginViewModel.TenantId,
                UserName = loginViewModel.UserName
            };
        }

        private void InitLocalLogin(LoginModel loginModel)
        {
            loginModel.AccessKey = _stateTokenGenerator.GenerateToken();
            loginModel.PostbackUrl = Request.GetUrlHelper().Route(Routes.LocalLoginRoute, null);
            loginModel.Provider = "local";
        }

        private UserRegistrationInfo TryParseInvitationCookie()
        {
            var ctx = this.Request.GetOwinContext();
            if (ctx.Request.Cookies.Count() < 1 || !ctx.Request.Cookies.Any(cook => cook.Key.EqualsIgnoreCase(_activationCookieName)))
                return null;

            var message = ctx.Request.Cookies[_activationCookieName];

            if (string.IsNullOrEmpty(message)) return null;

            return GetUserRegistrationInfoFromActivationKey(message);
        }

        private bool SetInviteLogOnCookie(string activationKey)
        {
            try
            {
                var activationDetails = ParseActivationKey(activationKey);

                if (activationDetails.IsNullOrEmpty()) return false;

                var ctx = this.Request.GetOwinContext();

                var cookieOptions = new Microsoft.Owin.CookieOptions
                {
                    HttpOnly = true,
                    Secure = this.Request.RequestUri.Scheme == Uri.UriSchemeHttps,
                };

                var timeout = AppSettingHelpers.GetValue<long>(AppSettingConstants.ActivationCookieExpiryTimeinSeconds);

                if (timeout > 0)
                {
                    cookieOptions.Expires = DateTime.UtcNow.AddSeconds(timeout);
                }

                ctx.Response.Cookies.Append(_activationCookieName, activationKey, cookieOptions);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static List<string> ParseActivationKey(string activationKey)
        {
            var passwordEncrptionService = ServiceLocator.Resolve<CelloSaaS.Library.Encryption.IPasswordEncrptionService>();

            string key = passwordEncrptionService.DecryptPassword(CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId, HttpUtility.UrlDecode(activationKey));

            if (string.IsNullOrEmpty(key) || !key.Contains(_keyDelimiter)) return null;

            string[] activationDetails = key.Split(_keyDelimiter);

            if (activationDetails.Length < 3) return null;

            return activationDetails.ToList();
        }

        private static UserRegistrationInfo GetUserRegistrationInfoFromActivationKey(string activationKey)
        {
            var activationDetails = ParseActivationKey(activationKey);

            if (activationDetails.IsNullOrEmpty()) return null;

            if (activationDetails.Count == 3)
            {
                activationDetails.Add(CelloSaaS.AuthServer.Core.Models.ApplicationTypes.MultiTenantAccess.ToString("F"));
            }
            else
            {
                if (activationDetails[3].EqualsIgnoreCase(CelloSaaS.Library.ApplicationTypes.Mobile.ToString("F")))
                    activationDetails[3] = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.InternalMobile.ToString();
                else if (activationDetails[3].EqualsIgnoreCase(CelloSaaS.Library.ApplicationTypes.Native.ToString("F")))
                    activationDetails[3] = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.InternalMobile.ToString();
                else if (activationDetails[3].EqualsIgnoreCase(CelloSaaS.Library.ApplicationTypes.Others.ToString("F")))
                    activationDetails[3] = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.ExternalTenant.ToString();
                else if (activationDetails[3].EqualsIgnoreCase(CelloSaaS.Library.ApplicationTypes.Services.ToString("F")))
                    activationDetails[3] = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.ExternalTenant.ToString();
                else if (activationDetails[3].EqualsIgnoreCase(CelloSaaS.Library.ApplicationTypes.Web.ToString("F")))
                    activationDetails[3] = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.InternalWeb.ToString();
            }

            return new UserRegistrationInfo
            {
                Identifier = activationDetails[0],
                CompanyCode = activationDetails[1],
                TenantId = activationDetails[2],
                ApplicationType = activationDetails[3]
            };
        }

        #endregion

        #region constants
        const string LoginRequestCookie = "cellologinrequest";
        #endregion
    }
}

