//-----------------------------------------------------------------------
// <copyright file="AuthorizeController.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using CelloSaaS.AuthorizationServer.Models;
using CelloSaaS.AuthServer.Core;
using CelloSaaS.AuthServer.Core.Ancilliary;
using CelloSaaS.AuthServer.Core.Authorization;
using CelloSaaS.AuthServer.Core.Extensions;
using CelloSaaS.AuthServer.Core.Models;
using CelloSaaS.AuthServer.Core.Models.Authentication;
using CelloSaaS.AuthServer.Core.Models.Authorization;
using CelloSaaS.AuthServer.Core.Models.Consents;
using CelloSaaS.AuthServer.Core.Models.Response;
using CelloSaaS.AuthServer.Core.Response.Authorization;
using CelloSaaS.AuthServer.Core.Results;
using CelloSaaS.AuthServer.Core.Results.Errors;
using CelloSaaS.AuthServer.Core.Results.Login;
using CelloSaaS.AuthServer.Core.ServiceContracts;
using CelloSaaS.AuthServer.Core.ServiceContracts.Validators;
using CelloSaaS.Library;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace CelloSaaS.AuthorizationServer.Controllers
{
    public class AuthorizeController : CelloApiController
    {
        IAuthorizeRequestValidator _authReqValidator;
        AuthorizeInteractionProcessor _interactionResponseProcessor;
        IAuthorizationCodeService _authCodeService;
        IClientService _clientService;

        const string CompanyCodeRequestCookie = "cid";

        public AuthorizeController()
        {
            _authReqValidator = ServiceLocator.Resolve<IAuthorizeRequestValidator>();
            _interactionResponseProcessor = ServiceLocator.Resolve<AuthorizeInteractionProcessor>();
            _authCodeService = ServiceLocator.Resolve<IAuthorizationCodeService>();
            _clientService = ServiceLocator.Resolve<IClientService>();
        }

        [HttpGet]
        [Route(Routes.AuthorizeRoute, Name = Routes.AuthorizeRouteName)]
        public async Task<IHttpActionResult> Get(HttpRequestMessage request)
        {
            return await ProcessRequestAsync(request);
        }

        [Route(Routes.PostConsentRoute, Name = Routes.PostConsentRouteName)]
        [HttpPost]
        public async Task<IHttpActionResult> PostConsent(HttpRequestMessage request, ConsentViewModel consent)
        {
            var parameters = request.RequestUri.ParseQueryString();

            //TODO: Form this kind of values from the request body if the POST method is supported
            if (parameters == null || parameters.Count < 1) throw new ArgumentException("Parameters cannot be empty");

            // check state
            var state = parameters.Get(AuthServerConstants.AuthorizeRequest.State);
            var redirectUri = parameters.Get(AuthServerConstants.AuthorizeRequest.RedirectUri);
            string companyCode = parameters.Get(AuthServerConstants.AuthorizeRequest.CompanyCode);

            if (!string.IsNullOrEmpty(consent.CompanyCode)) companyCode = consent.CompanyCode;

            if (string.IsNullOrEmpty(companyCode))
            {
                return new AuthorizeErrorResult(new AuthorizeError
                {
                    Error = "Not able to identify the tenant company code",
                    ErrorType = ErrorTypes.Client,
                    State = state,
                    ErrorUri = new Uri(redirectUri)
                });
            }

            string tenantId = ServiceLocator.Resolve<CelloSaaS.ServiceContracts.TenantManagement.ITenantService>().GetTenantIdFromTenantCode(companyCode);
            if (string.IsNullOrEmpty(tenantId))
            {
                return new AuthorizeErrorResult(new AuthorizeError
                {
                    Error = "Not able to identify the tenant identifier from the company code",
                    ErrorType = ErrorTypes.Client,
                    State = state,
                    ErrorUri = new Uri(redirectUri)
                });
            }

            if (string.IsNullOrEmpty(consent.IdScopes) && string.IsNullOrEmpty(consent.AppScopes))
                return new AuthorizeErrorResult(new AuthorizeError
                {
                    Error = "No consent received",
                    ErrorType = ErrorTypes.User,
                    State = state,
                    ErrorUri = new Uri(redirectUri)
                });

            if (!string.IsNullOrEmpty(consent.ClientId))
            {
                var client = await _clientService.GetAsync(consent.ClientId);
                if (client == null || client.Id.IsEmpty() || client.TenantId.IsEmpty())
                {
                    return Unauthorized();
                }
                consent.TenantId = tenantId.ToGuid();
            }

            //TODO : do the mapping from view model to the main model
            var userConsent = consent.Transform();

            return await ProcessRequestAsync(request, userConsent);
        }

        [Route(Routes.PostCompanyIdRoute, Name = Routes.PostCompanyIdRouteName)]
        [HttpPost]
        public async Task<IHttpActionResult> PostCompanyCodeInfo(CompanyCodeResultModel companyCodeResult)
        {
            if (companyCodeResult == null || string.IsNullOrEmpty(companyCodeResult.CompanyCode) || !CelloSaaS.Library.Util.ValidateAlphaNumeric(companyCodeResult.CompanyCode))
            {
                return new CompanyCodeViewResult(null, "Company Code information cannot be null", Request.GetUrlHelper().Route(Routes.PostCompanyIdRouteName, null));
            }

            string clientId = null, query = null;

            var tenantValidator = ServiceLocator.Resolve<ITenantValidator>();
            if (!await tenantValidator.IsCompanyCodeValid(companyCodeResult.CompanyCode))
            {
                return new CompanyCodeViewResult(companyCodeResult.CompanyCode, "Company Code information is invalid", Request.GetUrlHelper().Route(Routes.PostCompanyIdRouteName, null));
            }

            // if the tenant is valid
            GetCidCookie(out query, out clientId);

            //ClearCidCookie();

            var uri = new UriBuilder
            {
                Scheme = Request.RequestUri.Scheme,
                Host = Request.RequestUri.Host,
                Port = Request.RequestUri.Port,
                Path = Request.GetUrlHelper().Route(Routes.AuthorizeRouteName, null),
                Query = query.TrimStart('?') + "&company_code=" + companyCodeResult.CompanyCode
            };

            return Redirect(uri.ToString());
        }

        #region private members
        private async Task<IHttpActionResult> ProcessRequestAsync(HttpRequestMessage request, UserConsent consent = null)
        {
            var requestParameters = request.RequestUri.ParseQueryString();

            // validate the request parameters
            var validatedResult = await _authReqValidator.ValidateRequest(requestParameters);
            var authRequest = _authReqValidator.authorizeRequest;

            // incase of a public client, show the company code page
            if (validatedResult.ErrorType.Equals(ErrorTypes.PublicClient))
            {
                SetCidCookie(request.RequestUri.Query, authRequest.ClientId, authRequest.Client.AccessTokenLifetime);
                return new CompanyCodeViewResult(null, null, Request.GetUrlHelper().Route(Routes.PostCompanyIdRouteName, null));
            }

            // if invalid, return authorize error
            if (validatedResult.IsError)
            {
                return Invalid(error: validatedResult.Error, errorType: validatedResult.ErrorType, authRequest: authRequest);
            }

            // do the login check and process if required
            var interactionResponse = _interactionResponseProcessor.ProcessLogin(authRequest, User as ClaimsPrincipal);

            if (interactionResponse != null && interactionResponse.IsError)
            {
                return Invalid(authRequest, interactionResponse.Error.Error, interactionResponse.Error.ErrorType);
            }

            if (interactionResponse != null && interactionResponse.IsLogin)
            {
                return LoginResult(request, authRequest, interactionResponse);
            }

            // user must be authenticated at this point
            if (!User.Identity.IsAuthenticated)
            {
                throw new System.Security.SecurityException("User is not authenticated");
            }

            authRequest.Subject = User as ClaimsPrincipal;

            if (IsCommonLoginEnabled)
            {
                var userDetails = await ServiceLocator.Resolve<IUserService>().Get(authRequest.Subject.FindFirst("sub").Value);
                if (userDetails != null && userDetails.Item1 != null && userDetails.Item1.MembershipDetails != null && !string.IsNullOrEmpty(userDetails.Item1.MembershipDetails.TenantCode))
                {
                    authRequest.TenantId = userDetails.Item1.MembershipDetails.TenantCode;
                    var tenantdetails = ServiceLocator.Resolve<CelloSaaS.ServiceContracts.TenantManagement.ITenantService>().GetTenantDetailsByTenantId(authRequest.TenantId);

                    if (tenantdetails != null && tenantdetails.TenantDetails != null && !string.IsNullOrEmpty(tenantdetails.TenantDetails.TenantCodeString))
                        authRequest.CompanyCode = tenantdetails.TenantDetails.TenantCodeString;
                }
            }
            //validate client, initialize the client scopes based on the client's features in the license package
            validatedResult = await _authReqValidator.ValidateClientAsync(authRequest);

            if (validatedResult.IsError)
            {
                return Invalid(authRequest, validatedResult.Error, validatedResult.ErrorType);
            }

            // process the user consent, like storing the consent, validating the consents w.r.to the clients
            interactionResponse = await _interactionResponseProcessor.ProcessConsentAsync(authRequest, consent);

            if (interactionResponse != null && interactionResponse.IsError)
            {
                return Invalid(authRequest, interactionResponse.Error.Error, interactionResponse.Error.ErrorType);
            }

            // if this user under the context of this client, requires consent, show the consent page
            if (interactionResponse.IsConsent)
            {
                var consentModel = new ConsentViewModel(authRequest, User.Identity.Name);
                // set the post url here
                UriBuilder builder = new UriBuilder
                {
                    Host = request.RequestUri.Host,
                    Port = request.RequestUri.Port,
                    Scheme = request.RequestUri.Scheme,
                    Path = "authorize/postconsent",
                    Query = request.RequestUri.Query.TrimStart('?')
                };

                consentModel.PostUrl = builder.ToString();

                return new ConsentResult(consentModel);
            }

            // form the authorization code
            AuthorizeResponse authResponse = null;

            // store the authorization code in the temp store
            if (authRequest.Flow == Flows.Code)
            {
                authResponse = await new AuthorizationResponse(_authCodeService).CreateCodeFlowResponseAsync(authRequest, User as ClaimsPrincipal, authRequest.CompanyCode, null);
            }
            // return back the code to the caller
            return new AuthorizeCodeResult(authResponse, authRequest.CompanyCode);
        }

        private void ClearCidCookie()
        {
            var cookieOptions = new Microsoft.Owin.CookieOptions
            {
                HttpOnly = true,
                Secure = this.Request.RequestUri.Scheme == Uri.UriSchemeHttps,
                Expires = DateTime.UtcNow.AddYears(-1)
            };
            Request.GetOwinContext().Response.Cookies.Append(CompanyCodeRequestCookie, null, cookieOptions);
        }

        private void GetCidCookie(out string query, out string clientId)
        {
            clientId = null;
            query = null;

            var ctx = Request.GetOwinContext();
            if (ctx.Request.Cookies.Count() < 1 || !ctx.Request.Cookies.Any(cook => cook.Key.EqualsIgnoreCase(CompanyCodeRequestCookie)))
                return;

            var message = ctx.Request.Cookies[CompanyCodeRequestCookie];

            CompanyCodeResultModel cookieData = JsonConvert.DeserializeObject<CompanyCodeResultModel>(message);
            clientId = cookieData.ClientId;
            query = cookieData.QueryParameters;
        }

        private void SetCidCookie(string authRequestQuery, string clientId, long timeOutSeconds)
        {
            var ctx = this.Request.GetOwinContext();

            var cookieOptions = new Microsoft.Owin.CookieOptions
            {
                HttpOnly = true,
                Secure = this.Request.RequestUri.Scheme == Uri.UriSchemeHttps,
            };

            if (timeOutSeconds > 0)
            {
                cookieOptions.Expires = DateTime.UtcNow.AddSeconds(timeOutSeconds);
            }

            var data = new CompanyCodeResultModel
            {
                QueryParameters = authRequestQuery,
                ClientId = clientId
            };

            var cookieData = JsonConvert.SerializeObject(data);

            ctx.Response.Cookies.Append(CompanyCodeRequestCookie, cookieData, cookieOptions);
        }

        private static IHttpActionResult Invalid(ValidatedAuthorizeRequest authRequest, string error, ErrorTypes errorType)
        {
            return new AuthorizeErrorResult(new AuthorizeError
            {
                Error = error,
                ErrorType = errorType,
                ResponseMode = authRequest.ResponseMode,
                State = authRequest.State,
                ErrorUri = authRequest.RedirectUri
            });
        }

        private IHttpActionResult LoginResult(HttpRequestMessage request, ValidatedAuthorizeRequest authRequest, InteractionResponse interactionResponse)
        {
            CelloAuthorizationContext ctx = interactionResponse.AuthContext;
            return new LoginResult(ctx, interactionResponse, authRequest, request);
        }
        #endregion
    }
}
