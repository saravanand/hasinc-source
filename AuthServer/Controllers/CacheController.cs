﻿using CelloSaaS.AuthServer.Core;
using CelloSaaS.AuthServer.Core.Ancilliary;
using CelloSaaS.AuthServer.Core.Services.CacheServices;
using CelloSaaS.AuthServer.Core.Services.Clients;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CelloSaaS.AuthorizationServer.Controllers
{
    [RoutePrefix("cache")]
    public class CacheController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Clear()
        {
            FlushObjectCache();
            FlushResourcesCache();
            FlushAppSettings();
            ClearUserCache();
            return Ok<string>("Cache Cleared Successfully");
        }

        [Route("clearusercache")]
        [HttpGet]
        public IHttpActionResult ClearUserCache()
        {
            try
            {
                CelloUserCacheService.Flush();
            }
            catch (Exception exception)
            {
                System.Threading.Tasks.Task.Run(() => CelloSaaS.Library.ExceptionService.HandleException(exception, LoggingConstants.AuthServerExceptionPolicy));
                return new ErrorResult();
            }
            return Ok<string>("User Cache Cleared Successfully");
        }

        [Route("clearobjectcache")]
        [HttpGet]
        public IHttpActionResult FlushObjectCache()
        {
            try
            {
                AuthenticationProvidersCacheService.Flush();
                AuthenticationTypesCacheService.Flush();
                ClientCacheService.Flush();
            }
            catch (Exception exception)
            {
                System.Threading.Tasks.Task.Run(() => CelloSaaS.Library.ExceptionService.HandleException(exception, LoggingConstants.AuthServerExceptionPolicy));
                return new ErrorResult();
            }
            return Ok<string>("Object Cache Cleared Successfully");
        }

        [Route("clearresourcecache")]
        [HttpGet]
        public IHttpActionResult FlushResourcesCache()
        {
            try
            {
                StaticResourceHelpers.Flush();
                StaticResourceHelpers.Init();
            }
            catch (Exception exception)
            {
                System.Threading.Tasks.Task.Run(() => CelloSaaS.Library.ExceptionService.HandleException(exception, LoggingConstants.AuthServerExceptionPolicy));
                return new ErrorResult();
            }
            return Ok<string>("Resource Cache Cleared Successfully");
        }

        [Route("clearapplicationsettings")]
        [HttpGet]
        public IHttpActionResult FlushAppSettings()
        {
            try
            {
                AppSettingHelpers.Flush();
            }
            catch (Exception exception)
            {
                System.Threading.Tasks.Task.Run(() => CelloSaaS.Library.ExceptionService.HandleException(exception, LoggingConstants.AuthServerExceptionPolicy));
                return new ErrorResult();
            }
            return Ok<string>("Application Cache Cleared Successfully");
        }
    }

    public class ErrorResult : IHttpActionResult
    {
        public async System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            return new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                ReasonPhrase = "Exception occured while clearing the cache",
                Content = new StringContent("Exception occured while clearing the cache")
            };
        }
    }
}
