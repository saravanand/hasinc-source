//-----------------------------------------------------------------------
// <copyright file="ClientsController.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using CelloSaaS.AuthServer.Core.Extensions;
using CelloSaaS.AuthServer.Core.Models.Clients;
using CelloSaaS.AuthServer.Core.Results;
using CelloSaaS.AuthServer.Core.ServiceContracts;
using CelloSaaS.AuthorizationServer.Models;
using CelloSaaS.Library;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using CelloSaaS.AuthServer.Core.Ancilliary;
using CelloSaaS.AuthServer.Core.Extensions;
using CelloSaaS.AuthServer.Core.Models.Consents;

namespace CelloSaaS.AuthorizationServer.Controllers
{
    [RoutePrefix(Routes.ClientsPrefix)]
    public class ClientsController : CelloApiController
    {
        IClientService _clientService;
        IConsentService _consentService;

        public ClientsController()//IClientService clientService, IConsentService consentService)
        {
            _clientService = ServiceLocator.Resolve<IClientService>();
            _consentService = ServiceLocator.Resolve<IConsentService>();
        }

        /// <summary>
        /// Gets the clients by their id. 
        /// Will be called by the tenant context only
        /// </summary>
        /// <param name="Id">The client id</param>
        /// <returns></returns>
        [Route(Routes.GetClientRoute, Name = Routes.GetClientRouteName)]
        public async Task<IHttpActionResult> Get(string Id)
        {
            if (string.IsNullOrEmpty(Id))
            {
                return new ResponseContentResult<ClientDetail>("Client Id cannot be null or empty");
            }

            try
            {
                var clientInfo = await _clientService.GetAsync(Id);
                return new ResponseContentResult<ClientDetail>(clientInfo);
            }
            catch (ClientDetailException ex)
            {
                return new ResponseContentResult<int>(ex.Message);
            }
        }

        /// <summary>
        /// This API is always invoked in the context of the Product / ISV Admin
        /// </summary>
        /// <param name="searchCondition">The ClientSearchCondition</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [Route(Routes.GetClientsRoute, Name = Routes.GetClientsRouteName)]
        public async Task<IHttpActionResult> GetClients([FromBody]ClientSearchCondition searchCondition)
        {
            if (searchCondition == null)
            {
                return new ResponseContentResult<List<ClientDetail>>("Invalid or empty search condition");
            }

            try
            {
                var clientDetails = await _clientService.GetClientsAsync(searchCondition);

                if (clientDetails == null || clientDetails.Count < 1)
                    return new ResponseContentResult<string>("Unable to find the clients matching the given search condition parameters");

                return new ResponseContentResult<List<ClientDetail>>(clientDetails.Values.ToList());
            }
            catch (ClientDetailException ex)
            {
                return new ResponseContentResult<int>(ex.Message);
            }
        }

        [HttpPost]
        [Route(Routes.UpdateClientRoute, Name = Routes.UpdateClientRouteName)]
        public async Task<IHttpActionResult> Post([FromBody]ClientDetail clientDetail)
        {
            if (clientDetail == null) return new ResponseContentResult<ClientDetail>("Client Detials cannot be null");

            try
            {
                var recordsAffected = await _clientService.UpdateAsync(clientDetail);
                return new ResponseContentResult<int>(recordsAffected);
            }
            catch (ArgumentNullException argNullEx)
            {
                return new ResponseContentResult<int>(argNullEx.Message);
            }
            catch (ArgumentException argEx)
            {
                return new ResponseContentResult<int>(argEx.Message);
            }
            catch (ClientDetailException ex)
            {
                return new ResponseContentResult<int>(ex.Message);
            }
        }

        [HttpPut]
        [Route(Routes.AddClientRoute, Name = Routes.AddClientRouteName)]
        public async Task<IHttpActionResult> Put([FromBody]ClientDetail clientDetail)
        {
            if (clientDetail == null) return new ResponseContentResult<ClientDetail>("ClientDetail cannot be null");

            try
            {
                string clientId = await _clientService.AddAsync(clientDetail);
                return new ResponseContentResult<string>("Client Added Successfully, Client Id is: ", clientId);
            }
            catch (DuplicateClientDetailsException ex)
            {
                return new ResponseContentResult<string>(ex.Message);
            }
            catch (ClientDetailException ex)
            {
                return new ResponseContentResult<string>(ex.Message);
            }
        }

        [Route(Routes.DeleteClientRoute, Name = Routes.DeleteClientRouteName)]
        public async Task<IHttpActionResult> Delete(string Id)
        {
            if (string.IsNullOrEmpty(Id)) return new ResponseContentResult<string>("Client Id cannot be null or empty");

            try
            {
                int recordsAffected = await _clientService.SoftDeleteAsync(Id);
                return new ResponseContentResult<int>("ClientDetails Deactivated successfully. Records affected: ", recordsAffected.ToString());
            }
            catch (ClientDetailException ex)
            {
                return new ResponseContentResult<int>(ex.Message);
            }
        }

        [Route(Routes.AddSubjectsRoute, Name = Routes.AddSubjectsRouteName)]
        public async Task<IHttpActionResult> AddSubjects(List<SubjectDetail> subjects)
        {
            if (subjects == null || subjects.Count < 1) return new ResponseContentResult<string>("Client Id cannot be null or empty");

            try
            {
                await _consentService.AddSubjects(subjects);
                return new ResponseContentResult<string>("SubjectDetails added successfully : ", subjects.Count.ToString());
            }
            catch (SubjectDetailException ex)
            {
                return new ResponseContentResult<int>(ex.Message);
            }
        }
    }
}
