//-----------------------------------------------------------------------
// <copyright file="TokenController.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using CelloSaaS.AuthServer.Core;
using CelloSaaS.AuthServer.Core.Extensions;
using CelloSaaS.AuthServer.Core.Response.Token;
using CelloSaaS.AuthServer.Core.Results.Errors;
using CelloSaaS.AuthServer.Core.Results.Tokens;
using CelloSaaS.AuthServer.Core.ServiceContracts;
using CelloSaaS.Library;
using System.Threading.Tasks;
using System.Web.Http;

namespace CelloSaaS.AuthorizationServer.Controllers
{
    public class TokenRequest
    {
        public string grant_type;
        public string code;
        public string redirect_uri;
        public string client_id;
    }

    public class TokenController : CelloApiController
    {
        IClientValidator _clientValidator;
        ITokenRequestValidator _requestValidator;
        TokenResponseGenerator _generator;
        IClientService clientService = null;

        public TokenController()
        {
            _clientValidator = ServiceLocator.Resolve<IClientValidator>();
            _requestValidator = ServiceLocator.Resolve<ITokenRequestValidator>();
            _generator = new TokenResponseGenerator(ServiceLocator.Resolve<ITokenService>());
            clientService = ServiceLocator.Resolve<IClientService>();
        }

        public async Task<IHttpActionResult> Post(TokenRequest tokenRequest)
        {
            if (string.IsNullOrEmpty(tokenRequest.client_id))
            {
                return new TokenErrorResult(AuthServerConstants.TokenErrors.InvalidClient);
            }

            var client = await clientService.GetAsync(tokenRequest.client_id);

            if (client == null) return new TokenErrorResult(AuthServerConstants.TokenErrors.InvalidClient);

            var result = await _requestValidator.ValidateTokenRequestAsync(tokenRequest.grant_type, tokenRequest.code, tokenRequest.redirect_uri, client);

            if (result.IsError)
            {
                return new TokenErrorResult(result.Error);
            }

            // send back the access_token, token_type, expires_in, refresh_token,additional parameters etc...
            // return response
            var response = await _generator.ProcessAsync(_requestValidator._validatedRequest);
            return new TokenResponseResult(response);
        }
    }
}
