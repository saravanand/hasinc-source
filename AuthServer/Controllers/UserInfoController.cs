﻿//-----------------------------------------------------------------------
// <copyright file="UserInfoController.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:46:28 PM</date>
// </copyright>
//---------------------------------------------
using System.Threading.Tasks;
using System.Web.Http;
using CelloSaaS.AuthServer.Core;
using CelloSaaS.AuthServer.Core.Ancilliary;
using CelloSaaS.AuthServer.Core.Extensions;
using CelloSaaS.AuthServer.Core.Results;
using CelloSaaS.AuthServer.Core.Security;
using CelloSaaS.AuthServer.Core.ServiceContracts;
using CelloSaaS.Library;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.ServiceModel.Security;

namespace CelloSaaS.AuthorizationServer.Controllers
{
    [RoutePrefix("userinfo")]
    public class UserInfoController : CelloApiController
    {
        private IUserService _userService;
        private JwtSecurityTokenHandler _jwtTokenHandler;
        private IClientService _clientService;
        private IRoleService _roleService;
        private IClientCertificateStore certStore;
        public UserInfoController()
        {
            _userService = ServiceLocator.Resolve<IUserService>();
            _clientService = ServiceLocator.Resolve<IClientService>();
            _roleService = ServiceLocator.Resolve<IRoleService>();

            _jwtTokenHandler = new JwtSecurityTokenHandler();
#if DEBUG || TEST || STAGING
            DisableCertificateCheck();
#endif
            if (AppSettingHelpers.GetValue<bool>(AppSettingConstants.DisableSSLCertificateCheck))
            {
                DisableCertificateCheck();
            }
            certStore = ServiceLocator.Resolve<IClientCertificateStore>();
        }

        private void DisableCertificateCheck()
        {
            _jwtTokenHandler.Configuration = new SecurityTokenHandlerConfiguration();
            _jwtTokenHandler.Configuration.CertificateValidationMode = X509CertificateValidationMode.None;
            _jwtTokenHandler.Configuration.CertificateValidator = X509CertificateValidator.None;
        }

        /// <summary>
        /// Returns the User Profile information 
        /// </summary>
        /// <param name="Id">The User Identifier</param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            if (Request.Headers == null || Request.Headers.Authorization == null || string.IsNullOrEmpty(Request.Headers.Authorization.Parameter))
                return null;

            // deconstruct the access token,
            var accessToken = Request.Headers.Authorization.Parameter;
            //LogService.Write("accessToken : " + Newtonsoft.Json.JsonConvert.SerializeObject(accessToken));

            ProtectionSettings protection = new DataProtectionSettings().GetAuthorizationServerProtectionSettings();
            var claimsPrincipal = _jwtTokenHandler.ValidateToken(accessToken, new TokenValidationParameters
            {
                AllowedAudience = protection.Audience,
                ValidIssuer = protection.Issuer,
                SigningToken = new X509SecurityToken(await certStore.GetAuthServerCertificate())
            });

            //LogService.Write("formed the claims principal");

            // identify the client id and subject
            var clientClaim = claimsPrincipal.FindFirst(AuthServerConstants.ClaimTypes.ClientId);
            var userIdClaim = claimsPrincipal.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
            var scopeClaim = claimsPrincipal.FindFirst(AuthServerConstants.ClaimTypes.Scope);

            if (clientClaim == null || string.IsNullOrEmpty(clientClaim.Value))
                return Unauthorized(AuthenticationHeaderHelper.AuthenticateHeader);

            if (userIdClaim == null || string.IsNullOrEmpty(userIdClaim.Value))
                userIdClaim = claimsPrincipal.FindFirst(AuthServerConstants.ClaimTypes.Subject);

            if (userIdClaim == null || string.IsNullOrEmpty(userIdClaim.Value))
                return Unauthorized(AuthenticationHeaderHelper.AuthenticateHeader);

            // get the client and get its tenantid
            var client = await _clientService.GetAsync(clientClaim.Value);

            if (client == null)
                return Unauthorized(AuthenticationHeaderHelper.AuthenticateHeader);

            //var tenantId = userTenantId[userIdClaim.Value];

            // get the userdetails by userid and tenantid
            var userProfile = await _userService.Get(userIdClaim.Value);

            CelloSaaS.Model.UserManagement.UserDetails profileInfo = null;

            if (userProfile != null && userProfile.Item1 != null)
                profileInfo = userProfile.Item1;

            if (profileInfo == null || profileInfo.MembershipDetails == null || string.IsNullOrEmpty(profileInfo.MembershipDetails.TenantCode))
                return Unauthorized(AuthenticationHeaderHelper.AuthenticateHeader);

            // get the user roles
            string[] userroles = null;

            if (userProfile.Item2 != null && userProfile.Item2.Count > 0)
                userroles = userProfile.Item2.ToArray();
            else
                userroles = _roleService.GetUserRoles(userIdClaim.Value, profileInfo.MembershipDetails.TenantCode);

            string scopes = scopeClaim != null && !string.IsNullOrEmpty(scopeClaim.Value) ? scopeClaim.Value : string.Empty;

            // return the user details as a profile object
            if (userroles != null && userroles.Length > 0)
            {
                return new ProfileResult(
                    new
                    {
                        id = profileInfo.User.Identifier,
                        displayName = profileInfo.MembershipDetails.UserName,
                        givenName = profileInfo.User.FirstName,
                        familyName = profileInfo.User.LastName,
                        email = profileInfo.MembershipDetails.EmailId,
                        tenantId = profileInfo.MembershipDetails.TenantCode,
                        rights = string.Join(",", userroles),
                        client_id = client.Id.ToString(),
                        scope = scopes
                    });
            }
            else
            {
                return new ProfileResult(
                    new
                    {
                        id = profileInfo.User.Identifier,
                        displayName = profileInfo.MembershipDetails.UserName,
                        givenName = profileInfo.User.FirstName,
                        familyName = profileInfo.User.LastName,
                        email = profileInfo.MembershipDetails.EmailId,
                        tenantId = profileInfo.MembershipDetails.TenantCode,
                        client_id = client.Id.ToString(),
                        scope = scopes
                    });
            }
        }

        /// <summary>
        /// Performs new user Registration with the Cello Membership Provider
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> PutAsync()
        {
            return null;
        }

        /// <summary>
        /// Update the user details with the Cello Membership Provider
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> PostAsync()
        {
            return null;
        }
    }
}
