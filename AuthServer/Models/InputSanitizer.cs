﻿using CelloSaaS.AuthorizationServer.Models;
using CelloSaaS.AuthServer.Core.Models.Authentication;
using System.Web.Security.AntiXss;

namespace Contoso.AuthServer.Models
{
    public static class InputSanitizer
    {
        public static void SanitizeInputs(LoginViewModel input)
        {
            if (input == null) return;

            if (!string.IsNullOrEmpty(input.CompanyCode)) input.CompanyCode = AntiXssEncoder.HtmlEncode(input.CompanyCode, true);
            if (!string.IsNullOrEmpty(input.UserName)) input.UserName = AntiXssEncoder.HtmlEncode(input.UserName, true);
            if (!string.IsNullOrEmpty(input.Provider)) input.Provider = AntiXssEncoder.HtmlEncode(input.Provider, true);
            if (!string.IsNullOrEmpty(input.ProviderId)) input.ProviderId = AntiXssEncoder.HtmlEncode(input.ProviderId, true);
            if (!string.IsNullOrEmpty(input.AuthenticationTypeId)) input.AuthenticationTypeId = AntiXssEncoder.HtmlEncode(input.AuthenticationTypeId, true);
        }

        public static void SanitizeInputs(UserRegistrationInfo registrationInfo)
        {
            if (registrationInfo == null) return;

            if (!string.IsNullOrEmpty(registrationInfo.CompanyCode)) registrationInfo.CompanyCode = AntiXssEncoder.HtmlEncode(registrationInfo.CompanyCode, true);
            if (!string.IsNullOrEmpty(registrationInfo.UserName)) registrationInfo.UserName = AntiXssEncoder.HtmlEncode(registrationInfo.UserName, true);
            if (!string.IsNullOrEmpty(registrationInfo.ProviderId)) registrationInfo.ProviderId = AntiXssEncoder.HtmlEncode(registrationInfo.ProviderId, true);
            if (!string.IsNullOrEmpty(registrationInfo.AuthenticationTypeId)) registrationInfo.AuthenticationTypeId = AntiXssEncoder.HtmlEncode(registrationInfo.AuthenticationTypeId, true);
            if (!string.IsNullOrEmpty(registrationInfo.EmailId)) registrationInfo.EmailId = AntiXssEncoder.HtmlEncode(registrationInfo.EmailId, true);

        }
    }
}