﻿namespace CelloSaaS.AuthorizationServer.Models
{
    public enum LoginMode : int
    {
        External,
        CelloIdp,
        ADFS
    }
}