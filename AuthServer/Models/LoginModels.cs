//-----------------------------------------------------------------------
// <copyright file="LoginModels.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using CelloSaaS.AuthServer.Core.Models.Authentication;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CelloSaaS.AuthorizationServer.Models
{
    [Serializable]
    [DataContract]
    public class LoginViewModel : AuthenticationInfo
    {
        [DataMember]
        public string Provider { get; set; }
        [DataMember]
        public string ClientId { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string AccessKey { get; set; }
        [DataMember]
        public string AuthenticationTypeId { get; set; }
    }

    [Serializable]
    [DataContract]
    public class LoginModel : LoginViewModel
    {
        [DataMember]
        public List<ExternalLoginProviders> Providers { get; set; }
        [DataMember]
        public string PostbackUrl { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ExternalLoginProviders
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string ProviderId { get; set; }
    }
}
