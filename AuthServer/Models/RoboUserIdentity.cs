//-----------------------------------------------------------------------
// <copyright file="RoboUserIdentity.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using CelloSaaS.Library;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CelloSaaS.AuthorizationServer.Models
{
    public class RoboUserIdentity : IUserIdentityProvider
    {
        #region IUserIdentityProvider Members

        private const string ProductAdminUserId = "3398f837-b988-4708-999d-d3dfe11875b3";
        private const string ProductAdminTenantId = "b590cd25-3093-df11-8deb-001ec9dab123";


        List<string> roles = new List<string>();
        List<string> priviligesList = new List<string>();
        string[] privilige = null;
        string[] rolesArray = null;
        private string userId;
        private string tenantId;
        private string name;

        public string UserId
        {
            get
            {
                userId = ProductAdminUserId;
                return userId;
            }
            set { userId = value; }
        }

        public string TenantId
        {
            get
            {
                tenantId = ProductAdminTenantId;
                return tenantId;
            }
            set { tenantId = value; }
        }

        public string Name
        {
            get
            {
                return "CompanyAdmin";
            }
            set { name = value; }
        }

        public string[] Roles
        {
            get
            {
                return new string[]
                {
                    "GR$Tenant_Admin",
                    "GR$Product_Admin"
                };
            }
            set
            {
                rolesArray = value;
            }
        }

        public string[] Privileges
        {
            get
            {
                Dictionary<string, RolePrivilege> rolePrivilegeDetails = new Dictionary<string, RolePrivilege>();

                rolePrivilegeDetails = PrivilegeProxy.GetPrivilegesForRoles(TenantId, UserIdentity.Roles);

                if (rolePrivilegeDetails != null && rolePrivilegeDetails.Count > 0)
                {
                    priviligesList = new List<string>();
                    foreach (RolePrivilege rolePrivilege in rolePrivilegeDetails.Values)
                    {
                        priviligesList.Add(rolePrivilege.PrivilegeId);
                    }
                }

                if (priviligesList != null && priviligesList.Count > 0)
                {
                    privilige = priviligesList.ToArray();
                }
                return privilige;
            }
            set
            {
                privilige = value;
            }
        }

        public Dictionary<string, string> AdditionalProperties
        {
            get;
            set;
        }

        public System.Security.Principal.IPrincipal Ticket
        {
            get;
            set;
        }

        #endregion IUserIdentityProvider Members

        #region IUserIdentityProvider Members


        public bool OnBehalfOfUser
        {
            get;
            set;
        }

        public string LoggedInUserId
        {
            get;
            set;
        }

        public string LoggedInUserTenantId
        {
            get;
            set;
        }

        public string LoggedInUserName
        {
            get;
            set;
        }

        public string[] LoggedInUserRoles
        {
            get;
            set;
        }

        public string[] LoggedInUserPrivileges
        {
            get;
            set;
        }

        #endregion


        public string WCFSharedKey
        {
            get
            {
                return "UBd2DX9xIPprqtB7D0yb0w==";
            }
            set
            {

            }
        }

        private static void Configure()
        {
            // Register the modules
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.Notification.NotificationModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.WorkFlow.WorkflowModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.DataBackup.DataBackupModuleConfigurator>();

            // Register the entities
            CelloSaaS.Configuration.CelloConfigurator.RegisterEntity<CelloSaaS.WorkFlow.WorkflowEntityConfigurator>();

            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.Configuration.DBCelloModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterEntity<CelloSaaS.Configuration.DBCelloEntityConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterDataView<CelloSaaS.Configuration.DBCelloDataViewConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.Configure();
        }


        public string SessionTenantId
        {
            get { return this.tenantId; }
        }

        public string[] SessionRoles
        {
            get { return this.Roles; }
        }


        public Dictionary<string, string> TenantSettings
        {
            get;
            set;
        }
    }
}
