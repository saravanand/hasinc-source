//-----------------------------------------------------------------------
// <copyright file="Routes.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CelloSaaS.AuthorizationServer.Models
{
    public static class Routes
    {
        #region Client Routes
        public const string ClientsPrefix = "clients";
        public const string GetClientRouteName = "GetClient";
        public const string GetClientRoute = "get/{Id}";
        public const string GetClientsRouteName = "getclients";
        public const string GetClientsRoute = "clients";
        public const string UpdateClientRouteName = "updateclient";
        public const string UpdateClientRoute = "update";

        public const string AddClientRouteName = "addclient";
        public const string AddClientRoute = "add";

        public const string DeleteClientRouteName = "deleteclient";
        public const string DeleteClientRoute = "delete/{Id}";


        public const string AddSubjectsRouteName = "addsubjects";
        public const string AddSubjectsRoute = "addsubjects";
        #endregion

        #region Authentication Routes
        public const string AuthenticationPrefix = "Authentication";

        public const string GetAuthenticationProvidersRouteName = "GetAuthenticationProviders";
        public const string GetAuthenticationProvidersRoute = "getproviders";

        public const string GetAuthenticationTypesRouteName = "GetAuthenticationTypesForProvider";
        public const string GetAuthenticationTypesRoute = "gettypes/{providerId?}";

        public const string ExternalLoginRoute = "loginexternal";
        public const string ExternalLoginRouteName = "loginexternal";

        public const string LocalLoginRoute = "locallogin";
        public const string LocalLoginRouteName = "locallogin";

        public const string UpdatePasswordRoute = "UpdatePassword";
        public const string UpdatePasswordRouteName = "UpdatePassword";

        public const string RegisterRoute = "Register";
        public const string RegisterRouteName = "Register";

        public const string RegisterUserRoute = "RegisterUser";
        public const string RegisterUserRouteName = "RegisterUser";

        public const string LoginRoute = "Login";
        public const string LoginRouteName = "Login";

        public const string UpdateFirsttimeUserRoute = "UpdateFirsttimeUser";
        public const string UpdateFirsttimeUserRouteName = "UpdateFirsttimeUser";

        public const string ForgotPasswordRoute = "ForgotPassword";
        public const string ForgotPasswordRouteName = "ForgotPassword";

        public const string CompanyIdentificationRoute = "CompanyIdentification";
        public const string CompanyIdentificationRouteName = "CompanyIdentification";

        public const string ActiveDirectoryLoginRoute = "ActiveDirectoryLogin";
        public const string ActiveDirectoryLoginRouteName = "ActiveDirectoryLogin";

        public const string LogOffRoute = "logoff";
        public const string LogOffRouteName = "logout";

        public const string InvitationRoute = "Invite";
        public const string InvitationRouteName = "Invite";
        #endregion

        #region Authorize Routes
        public const string AuthorizePrefix = "Authorize";
        public const string AuthorizeRouteName = "authorize";
        public const string AuthorizeRoute = "authorize";

        public const string PostConsentRouteName = "postconsent";
        public const string PostConsentRoute = "authorize/postconsent";

        public const string PostCompanyIdRouteName = "setcompanycontext";
        public const string PostCompanyIdRoute = "authorize/setcompanycontext";
        #endregion
    }
}
