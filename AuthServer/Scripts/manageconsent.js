﻿$(document).ready(function () {

    var idScopes = [], appScopes = [], consolidatedScopes = [];

    $("#btnAccept").on("click", function () {

        $(".IdentityScopes").each(function (index, element) {
            var idscope = {
                IsSelected: true,
                Name: $(this).val(),
                DisplayName: $(this).data("displayname"),
                Description: $(this).data("description")
            };
            idScopes.push($(this).val());
            consolidatedScopes.push($(this).val());
        });

        $(".ApplicationScopes").each(function (index, element) {
            var appscope = {
                IsSelected: true,
                Name: $(this).val(),
                DisplayName: $(this).data("displayname"),
                Description: $(this).data("description")
            };
            appScopes.push($(this).val());
            consolidatedScopes.push($(this).val());
        });


        submitForm();
    });

    $("#btnCancel").on("click", function () {
        $("#GrantConsent").val(false);
        submitForm();
    });

    function submitForm() {

        $("#AppScopes").val(appScopes);
        $("#IdScopes").val(idScopes);

        $("#GrantConsent").val(true);

        $("#AllowRememberConsent").val($("#RememberConsent").is(":checked"));
        $("#consentForm").submit();
    }
});