//-----------------------------------------------------------------------
// <copyright file="Startup.cs" company="TechCello">
// Copyright (c) TechCello. All rights reserved.
// <author> TechCello </author>
// <date>8/24/2014 3:40:54 PM</date>
// </copyright>
//-----------------------------------------------------------------------

using CelloSaaS.AuthorizationServer.App_Start;
using CelloSaaS.AuthServer.Core;
using CelloSaaS.AuthServer.Core.Ancilliary;
using CelloSaaS.AuthServer.Core.Contracts.Clients;
using CelloSaaS.AuthServer.Core.Middlewares;
using CelloSaaS.Library;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication.Context;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication.Provider;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.MicrosoftAccount;
using Microsoft.Owin.Security.Twitter;
using Owin;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Http;
using CelloSaaS.AuthServer.Core.Contracts.Claims;

namespace CelloSaaS.AuthorizationServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            EventHandlingMiddlewares(app);

            ClientsSecurityMiddlewares(app);

            AuthenticationMiddlewares(app);

            app.UseWebApi(WebApiConfig.Configure(new HttpConfiguration()));
        }

        private static void EventHandlingMiddlewares(IAppBuilder app)
        {
            app.Map(new PathString("/Authentication/Login"), loginApp =>
            {
                loginApp.UseEventsMiddleware(new EventsMiddlewareOptions { EventType = AuthServerEvents.Login });
            }).Map(new PathString("/Authentication/LogOff"), loginApp =>
            {
                loginApp.UseEventsMiddleware(new EventsMiddlewareOptions { EventType = AuthServerEvents.Logout });
            });
        }

        private static void ClientsSecurityMiddlewares(IAppBuilder app)
        {
            return;
            app.Map(new PathString("/clients/get"), clientsApp =>
            {
                clientsApp.UseAdminAuthenticationMiddleware(new AdminAuthenticationOptions());
            }).Map(new PathString("/clients/add"), clientsListApp =>
            {
                clientsListApp.UseAdminAuthenticationMiddleware(new AdminAuthenticationOptions());
            }).Map(new PathString("/clients/update"), clientsListApp =>
            {
                clientsListApp.UseAdminAuthenticationMiddleware(new AdminAuthenticationOptions());
            }).Map(new PathString("/clients/addsubjects"), clientsListApp =>
            {
                clientsListApp.UseAdminAuthenticationMiddleware(new AdminAuthenticationOptions());
            }).Map(new PathString("/clients/delete"), clientsListApp =>
            {
                clientsListApp.UseAdminAuthenticationMiddleware(new AdminAuthenticationOptions { IsISVAdminAuthenticationEnabled = true });
            });

            //app.Map(new PathString("/clients/clients"), clientsListApp =>
            //{
            //    clientsListApp.UseAdminAuthenticationMiddleware(new AdminAuthenticationOptions { IsISVAdminAuthenticationEnabled = true });
            //});
        }

        private static void AuthenticationMiddlewares(IAppBuilder app)
        {
            var cookieAuthOptions = new CookieAuthenticationOptions
            {
                AuthenticationType = AuthServerConstants.PrimaryAuthenticationType,
            };

            app.UseCookieAuthentication(cookieAuthOptions);

            app.UseExternalSignInCookie(AuthServerConstants.ExternalAuthenticationType);

            ConfigureExternalIdentityProviders(app, AuthServerConstants.ExternalAuthenticationType);
        }

        public static void ConfigureExternalIdentityProviders(IAppBuilder app, string signInAsType)
        {
            var claimTransformer = ServiceLocator.Resolve<IClaimsTransformer>();

            #region LDAP Auth
            //{"AuthorizationEndPointUri":"https://identity.cello.com:44330/Authorize"
            //,"ProfileEndPointUri":"https://identity.cello.com:44320/Authentication/Login"}
            var ldapAuthOptions = new GenericOAuth2AuthenticationOptions("LDAP")
            {
                Caption = "LDAP",
                SignInAsAuthenticationType = signInAsType,
                AccessType = "LDAP",
                Scope = new List<string> { "all" },
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive
            };
            ldapAuthOptions.Provider = new GenericOAuth2AuthenticationProvider
            {
                InitRequestFromAuthenticationProperties = EndpointUriTypes => true,
                OnDemandEndpoints = async (clientid, EndpointUriTypes) =>
                {
                    var endpointResolver = ServiceLocator.Resolve<IClientEndpointResolver>();
                    return await endpointResolver.ResolveEndpointUri(EndpointUriTypes, clientid);
                },
                OnMapContextToClaims = context =>
                {
                    //LogService.Write(Newtonsoft.Json.JsonConvert.SerializeObject(context));
                    return System.Threading.Tasks.Task.FromResult<List<Claim>>(claimTransformer.GetAdditionalClaimsFromContext(context));
                },
                OnAuthenticated = async context =>
                {
                    //LogService.Write("on authenticated : " + Newtonsoft.Json.JsonConvert.SerializeObject(context));
                    claimTransformer.TransformClaims<GenericOAuth2AuthenticatedContext, GenericOAuth2AuthenticationOptions>(context, ldapAuthOptions);
                    //LogService.Write("on authenticated : " + Newtonsoft.Json.JsonConvert.SerializeObject(context));
                },
                OnReturnEndpoint = async context =>
                {
                    //LogService.Write("inside return end point " + Newtonsoft.Json.JsonConvert.SerializeObject(context));
                }
            };

            //  app.Map(new PathString("/api/account/login"), loginApp =>
            //{
            app.UseGenericOAuth2Authentication(ldapAuthOptions);
            //});
            #endregion

            #region Google Auth
            var google = new GoogleOAuth2AuthenticationOptions
               {
                   ClientId = AppSettingHelpers.GetValue<string>(AppSettingConstants.GoogleAppId),
                   ClientSecret = AppSettingHelpers.GetValue<string>(AppSettingConstants.GoogleAppSecret),
                   SignInAsAuthenticationType = AuthServerConstants.ExternalAuthenticationType,
                   AuthenticationMode = AuthenticationMode.Passive
               };
            google.Provider = new GoogleOAuth2AuthenticationProvider
            {
                OnAuthenticated = async ctx =>
                {
                    claimTransformer.TransformClaims<GoogleOAuth2AuthenticatedContext, GoogleOAuth2AuthenticationOptions>(ctx, google);
                }
            };
            app.UseGoogleAuthentication(google);
            #endregion

            #region Facebook Auth
            var fbOptions = new FacebookAuthenticationOptions
            {
                AppId = AppSettingHelpers.GetValue<string>(AppSettingConstants.FacebookAppId),
                AppSecret = AppSettingHelpers.GetValue<string>(AppSettingConstants.FacebookAppSecret),
                SignInAsAuthenticationType = AuthServerConstants.ExternalAuthenticationType,
                AuthenticationMode = AuthenticationMode.Passive
            };

            fbOptions.Scope.Add("email");
            fbOptions.Provider = new FacebookAuthenticationProvider
            {
                OnAuthenticated = async ctx =>
                {
                    claimTransformer.TransformClaims<FacebookAuthenticatedContext, FacebookAuthenticationOptions>(ctx, fbOptions);
                }
            };

            app.UseFacebookAuthentication(fbOptions);
            #endregion

            #region Twitter Auth
            var twitterOptions = new TwitterAuthenticationOptions
               {
                   ConsumerKey = AppSettingHelpers.GetValue<string>(AppSettingConstants.TwitterAppId),
                   ConsumerSecret = AppSettingHelpers.GetValue<string>(AppSettingConstants.TwitterAppSecret),
                   SignInAsAuthenticationType = AuthServerConstants.ExternalAuthenticationType,
                   AuthenticationMode = AuthenticationMode.Passive
               };
            twitterOptions.Provider = new TwitterAuthenticationProvider
            {
                OnAuthenticated = async ctx =>
                {
                    claimTransformer.TransformClaims<TwitterAuthenticatedContext, TwitterAuthenticationOptions>(ctx, twitterOptions);
                }
            };

            app.UseTwitterAuthentication(twitterOptions);
            #endregion

            #region Microsoft Authentication
            //var msftOptions = new MicrosoftAccountAuthenticationOptions
            //    {
            //        ClientId = AppSettingHelpers.GetValue<string>(AppSettingConstants.MSFTAppId),
            //        ClientSecret = AppSettingHelpers.GetValue<string>(AppSettingConstants.MSFTAppSecret),
            //        SignInAsAuthenticationType = AuthServerConstants.ExternalAuthenticationType,
            //        AuthenticationMode = AuthenticationMode.Passive
            //    };
            //msftOptions.Provider = new MicrosoftAccountAuthenticationProvider
            //{
            //    OnAuthenticated = async ctx =>
            //    {
            //        claimTransformer.TransformClaims<MicrosoftAccountAuthenticatedContext, MicrosoftAccountAuthenticationOptions>(ctx, msftOptions);
            //    }
            //};

            //app.UseMicrosoftAccountAuthentication(msftOptions);
            #endregion
        }
    }
}