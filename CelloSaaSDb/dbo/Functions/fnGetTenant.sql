﻿
CREATE FUNCTION [dbo].[fnGetTenant]()
RETURNS varchar(36) AS 
BEGIN
    DECLARE @retval varchar(36) 
    DECLARE @inputstring varchar(255) = APP_NAME()
    DECLARE @end INT = CHARINDEX('_', @inputstring) 
    DECLARE @ctxTenantId varchar(36) = null;
    SELECT @ctxTenantId =  CONTEXT_INFO();
    
	   IF @inputstring = NULL
		  IF @ctxTenantId IS NOT NULL 
			 BEGIN
       			RETURN @ctxTenantId
			 END
	   ELSE
		  return  SUBSTRING(@inputstring, @end+1, LEN(@inputstring))
	   RETURN NULL
END
