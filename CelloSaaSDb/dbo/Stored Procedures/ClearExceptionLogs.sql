﻿CREATE PROCEDURE ClearExceptionLogs
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM ExceptionCategoryLog
	DELETE FROM [ExceptionLog]
    DELETE FROM ExceptionCategory
END
