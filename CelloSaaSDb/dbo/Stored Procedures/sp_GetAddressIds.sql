﻿--Procedure to Get Address Ids
CREATE PROCEDURE [dbo].[sp_GetAddressIds]
@dtAddressUniqueId UniqueTable readonly
as
Begin
    Select Address_ID, Address_UniqueID from Address
    where Address_UniqueID in (select UniqueId from @dtAddressUniqueId)
End

