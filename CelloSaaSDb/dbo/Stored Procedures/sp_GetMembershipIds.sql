﻿--Procedure to Get Membership Ids
CREATE PROCEDURE [dbo].[sp_GetMembershipIds]
@dtUserName StringTable readonly,
@tenantId varchar(50)
as
Begin
    Select Membership_ID, Membership_UserName from Membership 
    where Membership_UserName in (select StringId from @dtUserName) and Membership_TenantID = @tenantId
End

