﻿CREATE TABLE [dbo].[Activities] (
    [ActivityId]   UNIQUEIDENTIFIER CONSTRAINT [DF_Activities_ActivityId] DEFAULT (newsequentialid()) NOT NULL,
    [Name]         NVARCHAR (200)   NOT NULL,
    [Type]         VARCHAR (50)     NOT NULL,
    [Description]  NVARCHAR (MAX)   NOT NULL,
    [WorkflowId]   UNIQUEIDENTIFIER NULL,
    [TypeName]     VARCHAR (255)    NOT NULL,
    [AssemblyName] VARCHAR (255)    NOT NULL,
    [CreatedOn]    DATETIME         NOT NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]    DATETIME         NULL,
    [UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [Status]       BIT              NOT NULL,
    [TenantId]     UNIQUEIDENTIFIER NULL,
    [Outcomes]     NVARCHAR (MAX)   NULL,
    [KeyValues]    VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_Activities] PRIMARY KEY CLUSTERED ([ActivityId] ASC)
);

