﻿CREATE TABLE [dbo].[ActivityContentType] (
    [ActivityContentType_ID]          SMALLINT      NOT NULL,
    [ActivityContentType_Description] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_star_ActivityContentType] PRIMARY KEY CLUSTERED ([ActivityContentType_ID] ASC)
);

