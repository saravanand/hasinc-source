﻿CREATE TABLE [dbo].[ActivityEvents] (
    [ActivityEvent_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_ActivityEvents_ActivityEvent_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ActivityEvent_EventId]     UNIQUEIDENTIFIER NOT NULL,
    [ActivityEvent_Description] VARCHAR (MAX)    NULL,
    [ActivityEvent_Status]      BIT              CONSTRAINT [DF_ActivityEvents_ActivityEvent_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ActivityEvents] PRIMARY KEY CLUSTERED ([ActivityEvent_Id] ASC),
    CONSTRAINT [FK_ActivityEvents_Events] FOREIGN KEY ([ActivityEvent_EventId]) REFERENCES [dbo].[Events] ([Event_Id]) ON DELETE CASCADE,
    CONSTRAINT [IX_ActivityEvents] UNIQUE NONCLUSTERED ([ActivityEvent_EventId] ASC)
);

