﻿CREATE TABLE [dbo].[ActivityLogs] (
    [ActivityLog_Id]       UNIQUEIDENTIFIER CONSTRAINT [DF_ActivityLogs_ActivityLog_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ActivityLog_EventId]  UNIQUEIDENTIFIER NOT NULL,
    [ActivityLog_LogTime]  DATETIME         NOT NULL,
    [ActivityLog_Comments] VARCHAR (MAX)    NULL,
    [ActivityLog_UserId]   UNIQUEIDENTIFIER NULL,
    [ActivityLog_Status]   BIT              CONSTRAINT [DF_ActivityLogs_ActivityLog_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ActivityLogs] PRIMARY KEY CLUSTERED ([ActivityLog_Id] ASC),
    CONSTRAINT [FK_ActivityLogs_ActivityEvents] FOREIGN KEY ([ActivityLog_EventId]) REFERENCES [dbo].[ActivityEvents] ([ActivityEvent_EventId]) ON DELETE CASCADE
);

