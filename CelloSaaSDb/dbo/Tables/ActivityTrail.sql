﻿CREATE TABLE [dbo].[ActivityTrail] (
    [ActivityTrail_ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [ActivityTrail_ActionTargetUserId] UNIQUEIDENTIFIER NULL,
    [ActivityTrail_ActionTakingUserID] UNIQUEIDENTIFIER NOT NULL,
    [ActivityTrail_ActionID]           INT              NOT NULL,
    [ActivityTrail_ActionDate]         DATETIME         NOT NULL,
    [ActivityTrail_Notes]              VARCHAR (500)    NULL,
    [ActivityTrail_ContentTypeID]      SMALLINT         NULL,
    [ActivityTrail_Content]            NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ActivityTrial] PRIMARY KEY CLUSTERED ([ActivityTrail_ID] ASC)
);

