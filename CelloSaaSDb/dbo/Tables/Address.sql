﻿CREATE TABLE [dbo].[Address] (
    [Address_ID]         UNIQUEIDENTIFIER CONSTRAINT [DF_Address_Address_ID] DEFAULT (newsequentialid()) NOT NULL,
    [Address_Address1]   NVARCHAR (2000)  NULL,
    [Address_Address2]   NVARCHAR (2000)  NULL,
    [Address_City]       NVARCHAR (200)   NOT NULL,
    [Address_State]      NVARCHAR (200)   NOT NULL,
    [Address_CountryId]  UNIQUEIDENTIFIER NOT NULL,
    [Address_PostalCode] VARCHAR (15)     NULL,
    [Address_CreatedBy]  UNIQUEIDENTIFIER NOT NULL,
    [Address_CreatedOn]  DATETIME         NOT NULL,
    [Address_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [Address_UpdatedOn]  DATETIME         NULL,
    [Address_Status]     BIT              NOT NULL,
    [Address_UniqueId]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([Address_ID] ASC),
    CONSTRAINT [FK_Address_Country] FOREIGN KEY ([Address_CountryId]) REFERENCES [dbo].[Country] ([Country_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Address_CountryId]
    ON [dbo].[Address]([Address_CountryId] ASC);

