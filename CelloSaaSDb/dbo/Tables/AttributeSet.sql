﻿CREATE TABLE [dbo].[AttributeSet] (
    [AttributeSet_ID]        UNIQUEIDENTIFIER NOT NULL,
    [AttributeSet_Name]      VARCHAR (100)    NOT NULL,
    [AttributeSet_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [AttributeSet_CreatedOn] DATETIME         NOT NULL,
    [AttributeSet_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [AttributeSet_UpdatedOn] DATETIME         NULL,
    [AttributeSet_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_AttributeSet] PRIMARY KEY CLUSTERED ([AttributeSet_ID] ASC)
);

