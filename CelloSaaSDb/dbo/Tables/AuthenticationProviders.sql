﻿CREATE TABLE [dbo].[AuthenticationProviders] (
    [Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_AuthenticationProviders_Id] DEFAULT (newsequentialid()) NOT NULL,
    [Name]        VARCHAR (50)     NOT NULL,
    [Description] VARCHAR (255)    NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    [CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]   DATETIME         NULL,
    [UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Status]      BIT              NOT NULL,
    CONSTRAINT [PK_AuthenticationProviders] PRIMARY KEY CLUSTERED ([Id] ASC)
);

