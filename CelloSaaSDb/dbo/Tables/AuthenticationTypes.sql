﻿CREATE TABLE [dbo].[AuthenticationTypes] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [Name]        VARCHAR (255)    NOT NULL,
    [Description] VARCHAR (255)    NULL,
    [Links]       VARCHAR (512)    NULL,
    [Mode]        VARCHAR (255)    NOT NULL,
    [ProviderId]  UNIQUEIDENTIFIER NOT NULL,
    [CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    [UpdatedOn]   DATETIME         NULL,
    [UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Status]      BIT              NOT NULL,
    CONSTRAINT [PK_AuthorizationTypes] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AuthenticationTypes_AuthenticationProviders] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[AuthenticationProviders] ([Id])
);

