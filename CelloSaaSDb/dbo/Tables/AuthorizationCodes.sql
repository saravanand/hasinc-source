﻿CREATE TABLE [dbo].[AuthorizationCodes] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [CodeString]        VARCHAR (MAX)    NOT NULL,
    [CompanyCode]       VARCHAR (255)    NOT NULL,
    [SubjectId]         UNIQUEIDENTIFIER NOT NULL,
    [ClientId]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]         DATETIME         NOT NULL,
    [UpdatedOn]         DATETIME         NULL,
    [UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [Status]            BIT              NOT NULL,
    [ScopeRestrictions] VARCHAR (MAX)    NULL,
    [AccessTokenType]   VARCHAR (25)     NULL,
    CONSTRAINT [PK_AuthorizationCodes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

