﻿CREATE TABLE [dbo].[BackupRequest] (
    [RequestId]          UNIQUEIDENTIFIER CONSTRAINT [DF_BackupRequest_RequestId] DEFAULT (newsequentialid()) NOT NULL,
    [Source]             TEXT             NOT NULL,
    [DestinationType]    VARCHAR (10)     NOT NULL,
    [DestinationDetails] TEXT             NOT NULL,
    [Description]        TEXT             NULL,
    [PrimaryTenantId]    UNIQUEIDENTIFIER NOT NULL,
    [TenantIds]          TEXT             NOT NULL,
    [BackupMode]         VARCHAR (10)     NOT NULL,
    [RequestDate]        DATETIME         NOT NULL,
    [RequestBy]          NVARCHAR (255)   NULL,
    [StartDate]          DATETIME         NULL,
    [CompletedDate]      DATETIME         NULL,
    [BackupSize]         FLOAT (53)       NULL,
    [BackupStatus]       VARCHAR (15)     NOT NULL,
    [ExceptionDetails]   TEXT             NULL,
    CONSTRAINT [PK_BackupRequest] PRIMARY KEY CLUSTERED ([RequestId] ASC)
);

