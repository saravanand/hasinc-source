﻿CREATE TABLE [dbo].[BatchFtpDetails] (
    [BatchFtpDetails_BatchId]       UNIQUEIDENTIFIER NOT NULL,
    [BatchFtpDetails_FtpAddress]    VARCHAR (100)    NOT NULL,
    [BatchFtpDetails_FtpUserName]   NVARCHAR (255)   NOT NULL,
    [BatchFtpDetails_FtpPassword]   NVARCHAR (255)   NOT NULL,
    [BatchFtpDetails_FilePath]      NVARCHAR (255)   NULL,
    [BatchFtpDetails_FileName]      NVARCHAR (255)   NULL,
    [BatchFtpDetails_FtpStatus]     VARCHAR (50)     NOT NULL,
    [BatchFtpDetails_StatusMessage] VARCHAR (MAX)    NULL,
    [BatchFtpDetails_CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [BatchFtpDetails_CreatedOn]     DATETIME         NOT NULL,
    [BatchFtpDetails_UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [BatchFtpDetails_UpdatedOn]     DATETIME         NULL,
    [BatchFtpDetails_Status]        BIT              NOT NULL,
    [BatchFtpDetails_IsSecured]     BIT              NOT NULL,
    CONSTRAINT [PK_BatchFtpDetails] PRIMARY KEY CLUSTERED ([BatchFtpDetails_BatchId] ASC),
    CONSTRAINT [FK_BatchFtpDetails_NotificationBatch] FOREIGN KEY ([BatchFtpDetails_BatchId]) REFERENCES [dbo].[NotificationBatch] ([NotificationBatch_Id])
);

