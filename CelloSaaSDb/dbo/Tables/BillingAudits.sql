﻿CREATE TABLE [dbo].[BillingAudits] (
    [Id]                 UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [TenantId]           UNIQUEIDENTIFIER NULL,
    [ParentTenantId]     UNIQUEIDENTIFIER NULL,
    [PackageId]          UNIQUEIDENTIFIER NULL,
    [PricePlanId]        UNIQUEIDENTIFIER NULL,
    [InvoiceId]          UNIQUEIDENTIFIER NULL,
    [InvoiceNo]          NVARCHAR (200)   NULL,
    [BillingFrequency]   INT              NULL,
    [InvoiceDate]        DATETIME         NOT NULL,
    [Duration]           FLOAT (53)       NULL,
    [ExceptionDetails]   VARCHAR (MAX)    NULL,
    [TimeStamp]          DATETIME         NOT NULL,
    [BillingAuditStatus] INT              NOT NULL,
    CONSTRAINT [PK_BillingAudits] PRIMARY KEY CLUSTERED ([Id] ASC)
);

