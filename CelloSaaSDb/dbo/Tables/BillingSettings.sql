﻿CREATE TABLE [dbo].[BillingSettings] (
    [Id]            UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [TenantId]      UNIQUEIDENTIFIER NULL,
    [StartDay]      INT              NOT NULL,
    [BillFrequency] INT              NOT NULL,
    [ChargeDay]     INT              NOT NULL,
    [Mode]          INT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_BillingSettings] PRIMARY KEY CLUSTERED ([Id] ASC)
);

