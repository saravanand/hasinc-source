﻿CREATE TABLE [dbo].[CelloVersion] (
    [VersionNo]   VARCHAR (10) NULL,
    [PatchNo]     INT          NULL,
    [Description] TEXT         NULL,
    [PatchDate]   DATETIME     NULL,
    [RunDate]     DATETIME     NULL,
    [Status]      BIT          NULL
);

