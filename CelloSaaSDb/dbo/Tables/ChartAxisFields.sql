﻿CREATE TABLE [dbo].[ChartAxisFields] (
    [ChartAxisFields_Id]            UNIQUEIDENTIFIER CONSTRAINT [DF_ChartAxisFields_ChartAxisFields_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ChartAxisFields_ChartId]       UNIQUEIDENTIFIER NOT NULL,
    [ChartAxisFields_AxisName]      VARCHAR (50)     NOT NULL,
    [ChartAxisFields_AxisFieldName] VARCHAR (50)     NOT NULL,
    [ChartAxisFields_AxisTitle]     VARCHAR (50)     NOT NULL,
    [ChartAxisFields_Interval]      FLOAT (53)       NULL,
    [ChartAxisFields_IsDependant]   BIT              NULL,
    [ChartAxisFields_AddedBy]       UNIQUEIDENTIFIER NOT NULL,
    [ChartAxisFields_AddedOn]       DATETIME         NOT NULL,
    [ChartAxisFields_UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [ChartAxisFields_UpdatedOn]     DATETIME         NULL,
    [ChartAxisFields_Status]        BIT              NOT NULL,
    CONSTRAINT [PK__ChartAxi__621C04167132C993] PRIMARY KEY NONCLUSTERED ([ChartAxisFields_Id] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_dbo_ChartAxisFields_ChartDetails] FOREIGN KEY ([ChartAxisFields_ChartId]) REFERENCES [dbo].[ChartDetails] ([ChartDetails_ChartId])
);


GO
CREATE NONCLUSTERED INDEX [IX_ChartAxisFields_ChartId]
    ON [dbo].[ChartAxisFields]([ChartAxisFields_ChartId] ASC) WITH (FILLFACTOR = 100);

