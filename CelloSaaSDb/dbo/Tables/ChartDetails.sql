﻿CREATE TABLE [dbo].[ChartDetails] (
    [ChartDetails_ChartId]      UNIQUEIDENTIFIER CONSTRAINT [DF_ChartDetails_ChartDetails_ChartId] DEFAULT (newsequentialid()) NOT NULL,
    [ChartDetails_DataSourceId] UNIQUEIDENTIFIER NOT NULL,
    [ChartDetails_TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [ChartDetails_Name]         NVARCHAR (255)   NOT NULL,
    [ChartDetails_Type]         NVARCHAR (150)   NULL,
    [ChartDetails_Options]      NVARCHAR (4000)  NULL,
    [ChartDetails_AddedBy]      UNIQUEIDENTIFIER NOT NULL,
    [ChartDetails_AddedOn]      DATETIME         NOT NULL,
    [ChartDetails_UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [ChartDetails_UpdatedOn]    DATETIME         NULL,
    [ChartDetails_Status]       BIT              NOT NULL,
    CONSTRAINT [PK_ChartDetails] PRIMARY KEY CLUSTERED ([ChartDetails_ChartId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ChartDetails_TenantID]
    ON [dbo].[ChartDetails]([ChartDetails_TenantId] ASC) WITH (FILLFACTOR = 100);

