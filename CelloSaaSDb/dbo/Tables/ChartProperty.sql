﻿CREATE TABLE [dbo].[ChartProperty] (
    [ChartProperty_Id]           UNIQUEIDENTIFIER CONSTRAINT [DF_ChartProperty_ChartProperty_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ChartProperty_PropertyName] NVARCHAR (255)   NOT NULL,
    [ChartProperty_AddedBy]      UNIQUEIDENTIFIER NOT NULL,
    [ChartProperty_AddedOn]      DATETIME         NOT NULL,
    [ChartProperty_UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [ChartProperty_UpdatedOn]    DATETIME         NULL,
    [ChartProperty_Status]       BIT              NOT NULL,
    CONSTRAINT [PK_ChartProperty] PRIMARY KEY CLUSTERED ([ChartProperty_Id] ASC),
    CONSTRAINT [U_dbo_ChartProperty_1] UNIQUE NONCLUSTERED ([ChartProperty_PropertyName] ASC) WITH (FILLFACTOR = 100)
);

