﻿CREATE TABLE [dbo].[ChartPropertyValues] (
    [ChartPropertyValues_Id]                UNIQUEIDENTIFIER CONSTRAINT [DF_ChartPropertyValues_ChartPropertyValues_Id] DEFAULT (newsequentialid()) NULL,
    [ChartPropertyValues_ChartId]           UNIQUEIDENTIFIER NULL,
    [ChartPropertyValues_SectionPropertyId] UNIQUEIDENTIFIER NOT NULL,
    [ChartPropertyValues_Value]             VARCHAR (255)    NOT NULL,
    [ChartPropertyValues_TenantId]          UNIQUEIDENTIFIER NULL,
    [ChartPropertyValues_AddedBy]           UNIQUEIDENTIFIER NOT NULL,
    [ChartPropertyValues_AddedOn]           DATETIME         NOT NULL,
    [ChartPropertyValues_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [ChartPropertyValues_UpdatedOn]         DATETIME         NULL,
    [ChartPropertyValues_Status]            BIT              NOT NULL,
    CONSTRAINT [FK_ChartPropertyValues_ChartDetails] FOREIGN KEY ([ChartPropertyValues_ChartId]) REFERENCES [dbo].[ChartDetails] ([ChartDetails_ChartId]),
    CONSTRAINT [FK_ChartPropertyValues_SectionProperties] FOREIGN KEY ([ChartPropertyValues_SectionPropertyId]) REFERENCES [dbo].[ChartSectionProperties] ([ChartSectionProperties_Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ChartPropertyValues_ChartId]
    ON [dbo].[ChartPropertyValues]([ChartPropertyValues_ChartId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ChartPropertyValues_PrimaryKey]
    ON [dbo].[ChartPropertyValues]([ChartPropertyValues_Id] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ChartPropertyValues_SectionPropId]
    ON [dbo].[ChartPropertyValues]([ChartPropertyValues_SectionPropertyId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ChartPropertyValues_TenantId]
    ON [dbo].[ChartPropertyValues]([ChartPropertyValues_TenantId] ASC) WITH (FILLFACTOR = 100);

