﻿CREATE TABLE [dbo].[ChartSectionProperties] (
    [ChartSectionProperties_Id]         UNIQUEIDENTIFIER CONSTRAINT [DF_ChartSectionProperties_ChartSectionProperties_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ChartSectionProperties_PropertyId] UNIQUEIDENTIFIER NOT NULL,
    [ChartSectionProperties_SectionId]  UNIQUEIDENTIFIER NOT NULL,
    [ChartSectionProperties_TenantId]   UNIQUEIDENTIFIER NULL,
    [ChartSectionProperties_AddedBy]    UNIQUEIDENTIFIER NOT NULL,
    [ChartSectionProperties_AddedOn]    DATETIME         NOT NULL,
    [ChartSectionProperties_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [ChartSectionProperties_UpdatedOn]  DATETIME         NULL,
    [ChartSectionProperties_Status]     BIT              NOT NULL,
    CONSTRAINT [PK_ChartSectionProperties] PRIMARY KEY CLUSTERED ([ChartSectionProperties_Id] ASC),
    CONSTRAINT [FK_ChartSectionProperties_Property] FOREIGN KEY ([ChartSectionProperties_PropertyId]) REFERENCES [dbo].[ChartProperty] ([ChartProperty_Id]),
    CONSTRAINT [FK_ChartSectionProperties_Sections] FOREIGN KEY ([ChartSectionProperties_SectionId]) REFERENCES [dbo].[ChartSections] ([ChartSections_Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_ChartSectionProperties_PropertyId]
    ON [dbo].[ChartSectionProperties]([ChartSectionProperties_PropertyId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ChartSectionProperties_SectionId]
    ON [dbo].[ChartSectionProperties]([ChartSectionProperties_SectionId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ChartSectionProperties_TenantId]
    ON [dbo].[ChartSectionProperties]([ChartSectionProperties_TenantId] ASC) WITH (FILLFACTOR = 100);

