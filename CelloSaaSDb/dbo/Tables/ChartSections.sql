﻿CREATE TABLE [dbo].[ChartSections] (
    [ChartSections_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_ChartSections_ChartSections_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ChartSections_SectionName] VARCHAR (100)    NOT NULL,
    [ChartSections_AddedBy]     UNIQUEIDENTIFIER NOT NULL,
    [ChartSections_AddedOn]     DATETIME         NOT NULL,
    [ChartSections_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [ChartSections_UpdatedOn]   DATETIME         NULL,
    [ChartSections_Status]      BIT              NOT NULL,
    PRIMARY KEY NONCLUSTERED ([ChartSections_Id] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [U_dbo_ChartSections_1] UNIQUE NONCLUSTERED ([ChartSections_SectionName] ASC) WITH (FILLFACTOR = 100)
);

