﻿CREATE TABLE [dbo].[Contact] (
    [Contact_ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_Contact_Contact_ID] DEFAULT (newsequentialid()) NOT NULL,
    [Contact_FirstName] NVARCHAR (255)   NOT NULL,
    [Contact_LastName]  NVARCHAR (255)   NOT NULL,
    [Contact_Email]     VARCHAR (255)    NOT NULL,
    [Contact_Phone]     VARCHAR (50)     NOT NULL,
    [Contact_Fax]       VARCHAR (50)     NULL,
    [Contact_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [Contact_CreatedOn] DATETIME         NOT NULL,
    [Contact_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [Contact_UpdatedOn] DATETIME         NULL,
    [Contact_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([Contact_ID] ASC)
);

