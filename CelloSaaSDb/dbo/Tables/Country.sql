﻿CREATE TABLE [dbo].[Country] (
    [Country_ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_Country_Country_ID] DEFAULT (newsequentialid()) NOT NULL,
    [Country_Name]      NVARCHAR (255)   NOT NULL,
    [Country_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [Country_CreatedOn] DATETIME         NOT NULL,
    [Country_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [Country_UpdatedOn] DATETIME         NULL,
    [Country_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([Country_ID] ASC)
);

