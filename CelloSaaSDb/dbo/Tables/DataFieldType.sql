﻿CREATE TABLE [dbo].[DataFieldType] (
    [DataFieldType_ID]        UNIQUEIDENTIFIER NOT NULL,
    [DataFieldType_Name]      VARCHAR (100)    NOT NULL,
    [DataFieldType_CreatedOn] DATETIME         NOT NULL,
    [DataFieldType_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [DataFieldType_UpdatedOn] DATETIME         NULL,
    [DataFieldType_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [DataFieldType_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_DataFieldType] PRIMARY KEY CLUSTERED ([DataFieldType_ID] ASC)
);

