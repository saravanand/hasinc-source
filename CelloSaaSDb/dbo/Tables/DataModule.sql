﻿CREATE TABLE [dbo].[DataModule] (
    [DataModuleID]   UNIQUEIDENTIFIER CONSTRAINT [DF_DataModule_DataModuleID] DEFAULT (newsequentialid()) NOT NULL,
    [DataModuleName] NVARCHAR (200)   NOT NULL,
    [DataModuleCode] VARCHAR (200)    NOT NULL,
    [Description]    NVARCHAR (MAX)   NOT NULL,
    [CreateScript]   NVARCHAR (MAX)   NULL,
    [UpdateScript]   NVARCHAR (MAX)   NULL,
    [CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]      DATETIME         NOT NULL,
    [UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [UpdatedOn]      DATETIME         NULL,
    [Status]         BIT              NOT NULL,
    CONSTRAINT [PK_DataModule] PRIMARY KEY CLUSTERED ([DataModuleID] ASC)
);

