﻿CREATE TABLE [dbo].[DataPartition] (
    [DataPartitionID]   UNIQUEIDENTIFIER NOT NULL,
    [DataPartitionName] NVARCHAR (200)   NOT NULL,
    [Description]       NVARCHAR (1000)  NOT NULL,
    [IsDefault]         BIT              NOT NULL,
    [Status]            BIT              NOT NULL,
    [CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]         DATETIME         NOT NULL,
    [Updatedby]         UNIQUEIDENTIFIER NULL,
    [UpdatedOn]         DATETIME         NULL,
    CONSTRAINT [PK_DataPartition] PRIMARY KEY CLUSTERED ([DataPartitionID] ASC)
);

