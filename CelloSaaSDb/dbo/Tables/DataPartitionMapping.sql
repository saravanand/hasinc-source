﻿CREATE TABLE [dbo].[DataPartitionMapping] (
    [DataPartitionMappingID] UNIQUEIDENTIFIER CONSTRAINT [DF_DataPartitionMapping_DataPartitionMappingID] DEFAULT (newsequentialid()) NOT NULL,
    [DataModuleID]           UNIQUEIDENTIFIER NOT NULL,
    [DataServerID]           UNIQUEIDENTIFIER NOT NULL,
    [DataPartitionID]        UNIQUEIDENTIFIER NOT NULL,
    [Status]                 BIT              NOT NULL,
    [CreatedBy]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]              DATETIME         NOT NULL,
    [UpdatedBy]              UNIQUEIDENTIFIER NULL,
    [UpdatedOn]              DATETIME         NULL,
    CONSTRAINT [PK_DataPartitionSetMapping] PRIMARY KEY CLUSTERED ([DataPartitionMappingID] ASC),
    CONSTRAINT [FK_DataPartitionMapping_DataModule] FOREIGN KEY ([DataModuleID]) REFERENCES [dbo].[DataModule] ([DataModuleID]),
    CONSTRAINT [FK_DataPartitionMapping_DataPartition] FOREIGN KEY ([DataPartitionID]) REFERENCES [dbo].[DataPartition] ([DataPartitionID]),
    CONSTRAINT [FK_DataPartitionMapping_DataServer] FOREIGN KEY ([DataServerID]) REFERENCES [dbo].[DataServer] ([DataServerID])
);

