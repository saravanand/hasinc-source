﻿CREATE TABLE [dbo].[DataScope] (
    [DataScope_ID]              UNIQUEIDENTIFIER NOT NULL,
    [DataScope_Name]            NVARCHAR (255)   NULL,
    [DataScope_SelectQuery]     NVARCHAR (MAX)   NULL,
    [DataScope_FilteredSubject] NVARCHAR (255)   NULL,
    [DataScope_Condition]       NVARCHAR (255)   NULL,
    [DataScope_AccessLevel]     SMALLINT         NULL,
    [DataScope_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [DataScope_CreatedOn]       DATETIME         NOT NULL,
    [DataScope_UpdatedBy]       UNIQUEIDENTIFIER NULL,
    [DataScope_UpdatedOn]       DATETIME         NULL,
    [DataScope_Status]          BIT              NOT NULL,
    [DataScope_IsDynamic]       BIT              NULL,
    CONSTRAINT [PK_DataScope] PRIMARY KEY CLUSTERED ([DataScope_ID] ASC)
);

