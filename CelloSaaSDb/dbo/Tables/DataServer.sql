﻿CREATE TABLE [dbo].[DataServer] (
    [DataServerID]     UNIQUEIDENTIFIER NOT NULL,
    [Name]             NVARCHAR (200)   NOT NULL,
    [ServerAddress]    NVARCHAR (200)   NOT NULL,
    [DataBaseName]     NVARCHAR (200)   NOT NULL,
    [ConnectionString] NVARCHAR (1000)  NOT NULL,
    [Status]           BIT              NOT NULL,
    [CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME         NOT NULL,
    [UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [UpdatedOn]        DATETIME         NULL,
    CONSTRAINT [PK_DataServer] PRIMARY KEY CLUSTERED ([DataServerID] ASC)
);

