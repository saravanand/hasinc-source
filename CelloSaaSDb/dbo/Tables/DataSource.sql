﻿CREATE TABLE [dbo].[DataSource] (
    [DataSource_Id]         UNIQUEIDENTIFIER CONSTRAINT [DF_DataSource_DataSource_Id] DEFAULT (newsequentialid()) NOT NULL,
    [DataSource_Name]       NVARCHAR (255)   NULL,
    [DataSource_SourceType] VARCHAR (50)     NULL,
    [DataSource_ContentId]  UNIQUEIDENTIFIER NOT NULL,
    [DataSource_TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [DataSource_CreatedBy]  UNIQUEIDENTIFIER NOT NULL,
    [DataSource_CreatedOn]  DATETIME         NOT NULL,
    [DataSource_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [DataSource_UpdatedOn]  DATETIME         NULL,
    [DataSource_Status]     BIT              NOT NULL,
    CONSTRAINT [PK_DataSource] PRIMARY KEY CLUSTERED ([DataSource_Id] ASC),
    CONSTRAINT [FK_DataSource_TenantDetails] FOREIGN KEY ([DataSource_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [IX_DataSource]
    ON [dbo].[DataSource]([DataSource_Id] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_DataSource_TenantId]
    ON [dbo].[DataSource]([DataSource_TenantId] ASC) WITH (FILLFACTOR = 100);

