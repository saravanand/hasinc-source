﻿CREATE TABLE [dbo].[DataView] (
    [DataView_ID]          INT              NOT NULL,
    [DataView_DataViewID]  NVARCHAR (255)   NULL,
    [DataView_Name]        NVARCHAR (250)   NOT NULL,
    [DataView_Application] INT              NOT NULL,
    [DataView_Description] NVARCHAR (MAX)   NULL,
    [DataView_MainEntity]  NVARCHAR (255)   NULL,
    [DataView_TenantId]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Form] PRIMARY KEY CLUSTERED ([DataView_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Ind_DataView_DataViewID]
    ON [dbo].[DataView]([DataView_DataViewID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_DataView_MainEntity]
    ON [dbo].[DataView]([DataView_MainEntity] ASC);

