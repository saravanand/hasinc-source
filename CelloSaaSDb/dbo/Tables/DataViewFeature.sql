﻿CREATE TABLE [dbo].[DataViewFeature] (
    [DataViewFeature_FeatureCode] NVARCHAR (255) NOT NULL,
    [DataViewFeature_DataViewID]  INT            NOT NULL,
    CONSTRAINT [FK_DataViewFeature_DataView] FOREIGN KEY ([DataViewFeature_DataViewID]) REFERENCES [dbo].[DataView] ([DataView_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_DataViewFeature_DataViewID]
    ON [dbo].[DataViewFeature]([DataViewFeature_DataViewID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_DataViewFeature_FeatureCode]
    ON [dbo].[DataViewFeature]([DataViewFeature_FeatureCode] ASC);

