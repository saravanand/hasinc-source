﻿CREATE TABLE [dbo].[DataViewFieldProperties] (
    [FieldProperties_TenantID]       UNIQUEIDENTIFIER NULL,
    [FieldProperties_FieldID]        INT              NOT NULL,
    [FieldProperties_FieldName]      NVARCHAR (255)   NOT NULL,
    [FieldProperties_Description]    NVARCHAR (MAX)   NULL,
    [FieldProperties_Ordinal]        INT              NULL,
    [FieldProperties_IsVisible]      BIT              NOT NULL,
    [FieldProperties_IsEditable]     BIT              NOT NULL,
    [FieldProperties_IsMandatory]    BIT              NULL,
    [FieldProperties_RegExpression]  NVARCHAR (200)   NULL,
    [FieldProperties_MaxLength]      INT              NULL,
    [FieldProperties_PickupListID]   UNIQUEIDENTIFIER NULL,
    [FieldProperties_ViewDataName]   NVARCHAR (100)   NULL,
    [FieldProperties_DefaultValue]   NVARCHAR (150)   NULL,
    [FieldProperties_IsHidden]       BIT              NOT NULL,
    [FieldProperties_CreatedOn]      DATETIME         NULL,
    [FieldProperties_CreatedBy]      UNIQUEIDENTIFIER NULL,
    [FieldProperties_UpdatedOn]      DATETIME         NULL,
    [FieldProperties_UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [FieldProperties_ControlFieldID] VARCHAR (50)     NULL,
    [FieldProperties_IsMultiLine]    BIT              NULL,
    CONSTRAINT [FK_DataViewFieldProperties_DataViewFields] FOREIGN KEY ([FieldProperties_FieldID]) REFERENCES [dbo].[DataViewFields] ([DataViewField_ID]),
    CONSTRAINT [FK_DataViewFieldProperties_PickupList] FOREIGN KEY ([FieldProperties_PickupListID]) REFERENCES [dbo].[PickupList] ([PickupList_ID]),
    CONSTRAINT [FK_DataViewFieldProperties_TenantDetails] FOREIGN KEY ([FieldProperties_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_FieldProperties_FieldID]
    ON [dbo].[DataViewFieldProperties]([FieldProperties_FieldID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_FieldProperties_Ordinal]
    ON [dbo].[DataViewFieldProperties]([FieldProperties_Ordinal] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_FieldProperties_PickupListID]
    ON [dbo].[DataViewFieldProperties]([FieldProperties_PickupListID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_FieldProperties_TenantID]
    ON [dbo].[DataViewFieldProperties]([FieldProperties_TenantID] ASC);

