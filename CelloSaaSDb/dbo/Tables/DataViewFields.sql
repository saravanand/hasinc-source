﻿CREATE TABLE [dbo].[DataViewFields] (
    [DataViewField_ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [DataViewField_FieldID]            NVARCHAR (255)   NOT NULL,
    [DataViewField_DataViewID]         INT              NULL,
    [DataViewField_TypeID]             INT              NOT NULL,
    [DataViewField_ExtendedTenantCode] UNIQUEIDENTIFIER NULL,
    [DataViewField_GlobalId]           INT              NULL,
    [DataViewField_IsGlobalField]      BIT              NULL,
    [DataViewField_IsFixed]            BIT              NULL,
    CONSTRAINT [PK_FormFields] PRIMARY KEY CLUSTERED ([DataViewField_ID] ASC),
    CONSTRAINT [FK_DataViewFields_TenantDetails] FOREIGN KEY ([DataViewField_ExtendedTenantCode]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code]),
    CONSTRAINT [FK_FormFields_FieldType] FOREIGN KEY ([DataViewField_TypeID]) REFERENCES [dbo].[FieldType] ([FieldType_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_DataViewField_DataViewID]
    ON [dbo].[DataViewFields]([DataViewField_DataViewID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_DataViewField_ExtendedTenantCode]
    ON [dbo].[DataViewFields]([DataViewField_ExtendedTenantCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_DataViewField_FieldID]
    ON [dbo].[DataViewFields]([DataViewField_FieldID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_DataViewField_TypeID]
    ON [dbo].[DataViewFields]([DataViewField_TypeID] ASC);

