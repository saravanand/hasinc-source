﻿CREATE TABLE [dbo].[DefaultRoles] (
    [DefaultRole_ID]        VARCHAR (50)     NOT NULL,
    [DefaultRole_Name]      VARCHAR (100)    NOT NULL,
    [DefaultRole_Rule]      VARCHAR (100)    NULL,
    [DefaultRole_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [DefaultRole_CreatedOn] DATETIME         NOT NULL,
    [DefaultRole_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [DefaultRole_UpdatedOn] DATETIME         NULL,
    [DefaultRole_Status]    BIT              NOT NULL,
    CONSTRAINT [PK__DefaultR__F8098907693CA210] PRIMARY KEY CLUSTERED ([DefaultRole_ID] ASC)
);

