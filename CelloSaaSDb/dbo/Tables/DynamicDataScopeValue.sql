﻿CREATE TABLE [dbo].[DynamicDataScopeValue] (
    [DynamicDataScopeValue_Id]                UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [DynamicDataScopeValue_RolePrivilegeId]   UNIQUEIDENTIFIER NOT NULL,
    [DynamicDataScopeValue_DynamicVariableId] UNIQUEIDENTIFIER NOT NULL,
    [DynamicDataScopeValue_Value]             VARCHAR (500)    NOT NULL,
    [DynamicDataScopeValue_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [DynamicDataScopeValue_CreatedOn]         DATETIME         NOT NULL,
    [DynamicDataScopeValue_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [DynamicDataScopeValue_UpdatedOn]         DATETIME         NULL,
    [DynamicDataScopeValue_Status]            BIT              NOT NULL,
    CONSTRAINT [PK__DynamicD__39971D05370627FE] PRIMARY KEY NONCLUSTERED ([DynamicDataScopeValue_Id] ASC) WITH (FILLFACTOR = 100),
    CONSTRAINT [FK_DynamicDataScopeValue_DynamicVariable] FOREIGN KEY ([DynamicDataScopeValue_Id]) REFERENCES [dbo].[DynamicVariable] ([DynamicVariable_Id]),
    CONSTRAINT [FK_DynamicDataScopeValue_RolePrivileges] FOREIGN KEY ([DynamicDataScopeValue_RolePrivilegeId]) REFERENCES [dbo].[RolePrivileges] ([RolePrivilege_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_DynamicDataScopeValue_DynamicVariable_Id]
    ON [dbo].[DynamicDataScopeValue]([DynamicDataScopeValue_DynamicVariableId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [Ind_DynamicDataScopeValue_Privilege_Id]
    ON [dbo].[DynamicDataScopeValue]([DynamicDataScopeValue_RolePrivilegeId] ASC) WITH (FILLFACTOR = 100);

