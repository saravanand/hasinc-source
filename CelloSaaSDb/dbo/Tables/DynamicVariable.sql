﻿CREATE TABLE [dbo].[DynamicVariable] (
    [DynamicVariable_Id]                UNIQUEIDENTIFIER CONSTRAINT [DF_DynamicVariable_DynamicVariable_Id] DEFAULT (newsequentialid()) NOT NULL,
    [DynamicVariable_Name]              NVARCHAR (255)   NOT NULL,
    [DynamicVariable_VariableName]      NVARCHAR (255)   NOT NULL,
    [DynamicVariable_DataTypeId]        UNIQUEIDENTIFIER NOT NULL,
    [DynamicVariable_FieldTypeId]       VARCHAR (255)    NOT NULL,
    [DynamicVariable_DataScopeId]       UNIQUEIDENTIFIER NOT NULL,
    [DynamicVariable_DynamicValueQuery] NVARCHAR (MAX)   NULL,
    [DynamicVariable_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [DynamicVariable_CreatedOn]         DATETIME         NOT NULL,
    [DynamicVariable_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [DynamicVariable_UpdatedOn]         DATETIME         NULL,
    [DynamicVariable_Status]            BIT              NOT NULL,
    CONSTRAINT [PK_DynamicVariable] PRIMARY KEY CLUSTERED ([DynamicVariable_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Ind_DynamicVariable_DynamicVariable_DataScopeId]
    ON [dbo].[DynamicVariable]([DynamicVariable_DataScopeId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [Ind_DynamicVariable_DynamicVariable_DataTypeId]
    ON [dbo].[DynamicVariable]([DynamicVariable_DataTypeId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [Ind_DynamicVariable_DynamicVariable_FieldTypeId]
    ON [dbo].[DynamicVariable]([DynamicVariable_FieldTypeId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [Ind_DynamicVariable_DynamicVariable_Name]
    ON [dbo].[DynamicVariable]([DynamicVariable_Name] ASC) WITH (FILLFACTOR = 100);

