﻿CREATE TABLE [dbo].[EmailContentDetails] (
    [EmailContentDetails_ContentDetailsId]         UNIQUEIDENTIFIER NOT NULL,
    [EmailContentDetails_Content]                  NVARCHAR (MAX)   NULL,
    [EmailContentDetails_AttachmentFileFolderPath] VARCHAR (MAX)    NULL,
    [EmailContentDetails_AttachmentFiles]          VARCHAR (MAX)    NULL,
    [EmailContentDetails_TemplateId]               UNIQUEIDENTIFIER NULL,
    [EmailContentDetails_CreatedBy]                UNIQUEIDENTIFIER NULL,
    [EmailContentDetails_CreatedOn]                DATETIME         DEFAULT (getdate()) NOT NULL,
    [EmailContentDetails_UpdatedBy]                UNIQUEIDENTIFIER NULL,
    [EmailContentDetails_UpdatedOn]                DATETIME         NULL,
    [EmailContentDetails_Status]                   BIT              DEFAULT ((1)) NOT NULL,
    [EmailContentDetails_Subject]                  NVARCHAR (500)   NULL,
    [EmailContentDetails_RuleSetCode]              VARCHAR (50)     NULL,
    CONSTRAINT [PK_EmailContentDetails] PRIMARY KEY CLUSTERED ([EmailContentDetails_ContentDetailsId] ASC),
    CONSTRAINT [FK_EmailContentDetails_NotificationContentDetails] FOREIGN KEY ([EmailContentDetails_ContentDetailsId]) REFERENCES [dbo].[NotificationContentDetails] ([NotificationContentDetails_Id]),
    CONSTRAINT [FK_EmailContentDetails_Template] FOREIGN KEY ([EmailContentDetails_TemplateId]) REFERENCES [dbo].[Template] ([Template_Id])
);

