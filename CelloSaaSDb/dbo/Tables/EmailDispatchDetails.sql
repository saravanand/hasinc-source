﻿CREATE TABLE [dbo].[EmailDispatchDetails] (
    [EmailDispatchDetails_DispatchDetailsId]         UNIQUEIDENTIFIER NOT NULL,
    [EmailDispatchDetails_SenderAddress]             VARCHAR (255)    NOT NULL,
    [EmailDispatchDetails_RecipientAddress]          VARCHAR (255)    NULL,
    [EmailDispatchDetails_SecondaryRecipientAddress] VARCHAR (MAX)    NULL,
    [EmailDispatchDetails_Subject]                   NVARCHAR (500)   NULL,
    [EmailDispatchDetails_SmtpAddress]               VARCHAR (100)    NOT NULL,
    [EmailDispatchDetails_SmtpUserName]              NVARCHAR (255)   NOT NULL,
    [EmailDispatchDetails_SmtpPassword]              NVARCHAR (255)   NOT NULL,
    [EmailDispatchDetails_CreatedBy]                 UNIQUEIDENTIFIER NULL,
    [EmailDispatchDetails_CreatedOn]                 DATETIME         DEFAULT (getdate()) NOT NULL,
    [EmailDispatchDetails_UpdatedBy]                 UNIQUEIDENTIFIER NULL,
    [EmailDispatchDetails_UpdatedOn]                 DATETIME         NULL,
    [EmailDispatchDetails_Status]                    BIT              DEFAULT ((1)) NOT NULL,
    [EmailDispatchDetails_Bcc]                       VARCHAR (MAX)    NULL,
    [EmailDispatchDetails_PortNumber]                INT              NULL,
    [EmailDispatchDetails_EnableSSL]                 BIT              NULL,
    CONSTRAINT [PK_EmailDispatchDetails] PRIMARY KEY CLUSTERED ([EmailDispatchDetails_DispatchDetailsId] ASC),
    CONSTRAINT [FK_EmailDispatchDetails_NotificationDispatchDetails] FOREIGN KEY ([EmailDispatchDetails_DispatchDetailsId]) REFERENCES [dbo].[NotificationDispatchDetails] ([NotificationDispatchDetails_Id])
);

