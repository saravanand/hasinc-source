﻿CREATE TABLE [dbo].[EmailNotificationDispatch] (
    [EmailNotificationDispatch_NotificationDispatchId]    UNIQUEIDENTIFIER NOT NULL,
    [EmailNotificationDispatch_SenderAddress]             VARCHAR (255)    NOT NULL,
    [EmailNotificationDispatch_RecipientAddress]          VARCHAR (255)    NOT NULL,
    [EmailNotificationDispatch_SecondaryRecipientAddress] VARCHAR (MAX)    NULL,
    [EmailNotificationDispatch_SmtpAddress]               VARCHAR (255)    NOT NULL,
    [EmailNotificationDispatch_SmtpUserName]              NVARCHAR (200)   NOT NULL,
    [EmailNotificationDispatch_SmtpPassword]              NVARCHAR (200)   NOT NULL,
    [EmailNotificationDispatch_CreatedOn]                 DATETIME         NOT NULL,
    [EmailNotificationDispatch_CreatedBy]                 UNIQUEIDENTIFIER NOT NULL,
    [EmailNotificationDispatch_UpdatedOn]                 DATETIME         NULL,
    [EmailNotificationDispatch_UpdatedBy]                 UNIQUEIDENTIFIER NULL,
    [EmailNotificationDispatch_Status]                    BIT              NOT NULL,
    [EmailNotificationDispatch_Bcc]                       VARCHAR (MAX)    NULL,
    [EmailNotificationDispatch_PortNumber]                INT              NULL,
    [EmailNotificationDispatch_EnableSSL]                 BIT              NULL,
    CONSTRAINT [PK_EmailNotificationDispatch] PRIMARY KEY CLUSTERED ([EmailNotificationDispatch_NotificationDispatchId] ASC),
    CONSTRAINT [FK_EmailNotificationDispatch_NotificationDispatch] FOREIGN KEY ([EmailNotificationDispatch_NotificationDispatchId]) REFERENCES [dbo].[NotificationDispatch] ([NotificationDispatch_Id])
);

