﻿CREATE TABLE [dbo].[EmailQueueLog] (
    [EmailQueue_ID]               UNIQUEIDENTIFIER NOT NULL,
    [EmailQueue_FromAddress]      VARCHAR (50)     NOT NULL,
    [EmailQueue_ToAddress]        VARCHAR (50)     NOT NULL,
    [EmailQueue_EmailContent]     NVARCHAR (MAX)   NOT NULL,
    [EmailQueue_SenderRole]       VARCHAR (50)     NOT NULL,
    [EmailQueue_SenderContextID]  INT              NOT NULL,
    [EmailQueue_SenderUserID]     UNIQUEIDENTIFIER NOT NULL,
    [EmailQueue_ReceiverUserID]   UNIQUEIDENTIFIER NULL,
    [EmailQueue_ErrorDescription] NVARCHAR (500)   NULL,
    [EmailQueue_InviteDate]       DATETIME         NOT NULL,
    [EmailQueue_Status]           INT              NOT NULL,
    [EmailQueue_ReferenceID]      NVARCHAR (255)   NULL,
    CONSTRAINT [PK_EmailQueueLog] PRIMARY KEY CLUSTERED ([EmailQueue_ID] ASC)
);

