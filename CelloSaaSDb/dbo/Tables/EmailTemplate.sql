﻿CREATE TABLE [dbo].[EmailTemplate] (
    [Emailtemplate_ID]       INT              IDENTITY (1, 1) NOT NULL,
    [Emailtemplate_Name]     NVARCHAR (255)   NOT NULL,
    [Emailtemplate_Type]     SMALLINT         NOT NULL,
    [Emailtemplate_TenantID] UNIQUEIDENTIFIER NOT NULL,
    [Emailtemplate_Content]  NVARCHAR (MAX)   NOT NULL,
    CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED ([Emailtemplate_ID] ASC)
);

