﻿CREATE TABLE [dbo].[Entity] (
    [Entity_ID]                              NVARCHAR (255)   NOT NULL,
    [Entity_SchemaTableName]                 VARCHAR (200)    NULL,
    [Entity_ExtendedSchemaTableName]         VARCHAR (200)    NULL,
    [Entity_PrimaryKeyName]                  NVARCHAR (255)   NULL,
    [Entity_TypeName]                        VARCHAR (100)    NULL,
    [Entity_AssemblyName]                    VARCHAR (100)    NULL,
    [Entity_CreatedOn]                       DATETIME         NULL,
    [Entity_CreatedBy]                       UNIQUEIDENTIFIER NULL,
    [Entity_UpdatedOn]                       DATETIME         NULL,
    [Entity_UpdatedBy]                       UNIQUEIDENTIFIER NULL,
    [Entity_Status]                          BIT              NULL,
    [Entity_IsTenantBasedExtn]               BIT              NULL,
    [Entity_IsExtendable]                    BIT              NULL,
    [Entity_ExtnSchemaTableConnectionName]   VARCHAR (100)    NULL,
    [Entity_SchemaTableConnectionStringName] VARCHAR (100)    NULL,
    [Entity_DisplayColumnName]               NVARCHAR (200)   NULL,
    [Entity_TenantIdColumnName]              NVARCHAR (255)   NULL,
    [Entity_TenantId]                        UNIQUEIDENTIFIER NULL,
    [Entity_Name]                            NVARCHAR (255)   NULL,
    CONSTRAINT [PK_Entity] PRIMARY KEY CLUSTERED ([Entity_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Ind_Entity_IsTenantBasedExtn]
    ON [dbo].[Entity]([Entity_IsTenantBasedExtn] ASC);

