﻿CREATE TABLE [dbo].[EntityBasedTenantScope] (
    [EntityBasedTenantScope_Id]              UNIQUEIDENTIFIER CONSTRAINT [DF_EntityBasedTenantScope_EntityBasedTenantScope_Id] DEFAULT (newsequentialid()) NOT NULL,
    [EntityBasedTenantScope_EntityId]        NVARCHAR (255)   NOT NULL,
    [EntityBasedTenantScope_BridgeCondition] NVARCHAR (MAX)   NOT NULL,
    [EntityBasedTenantScope_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [EntityBasedTenantScope_CreatedOn]       DATETIME         NOT NULL,
    [EntityBasedTenantScope_UpdatedBy]       UNIQUEIDENTIFIER NULL,
    [EntityBasedTenantScope_UpdatedOn]       DATETIME         NULL,
    [EntityBasedTenantScope_Status]          BIT              NOT NULL,
    CONSTRAINT [PK_EntityBasedTenantScope] PRIMARY KEY CLUSTERED ([EntityBasedTenantScope_Id] ASC)
);

