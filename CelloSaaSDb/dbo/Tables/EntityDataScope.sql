﻿CREATE TABLE [dbo].[EntityDataScope] (
    [EntityDataScope_ID]              UNIQUEIDENTIFIER NOT NULL,
    [EntityDataScope_DataScopeID]     UNIQUEIDENTIFIER NOT NULL,
    [EntityDataScope_EntityID]        NVARCHAR (255)   NOT NULL,
    [EntityDataScope_BridgeCondition] NVARCHAR (MAX)   NULL,
    [EntityDataScope_Property]        NVARCHAR (255)   NULL,
    [EntityDataScope_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [EntityDataScope_CreatedOn]       DATETIME         NOT NULL,
    [EntityDataScope_UpdatedBy]       UNIQUEIDENTIFIER NULL,
    [EntityDataScope_UpdatedOn]       DATETIME         NULL,
    [EntityDataScope_Status]          BIT              NOT NULL,
    CONSTRAINT [PK_EntityDataScope] PRIMARY KEY CLUSTERED ([EntityDataScope_ID] ASC),
    CONSTRAINT [FK_EntityDataScope_DataScope] FOREIGN KEY ([EntityDataScope_DataScopeID]) REFERENCES [dbo].[DataScope] ([DataScope_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityDataScope_DataScopeID]
    ON [dbo].[EntityDataScope]([EntityDataScope_DataScopeID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityDataScope_EntityID]
    ON [dbo].[EntityDataScope]([EntityDataScope_EntityID] ASC);

