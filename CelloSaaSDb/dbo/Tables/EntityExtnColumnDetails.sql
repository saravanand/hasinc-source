﻿CREATE TABLE [dbo].[EntityExtnColumnDetails] (
    [EntityExtnColumnDetails_Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_EntityExtnColumnDetails_ID] DEFAULT (newsequentialid()) NOT NULL,
    [EntityExtnColumnDetails_EntityId]         NVARCHAR (255)   NOT NULL,
    [EntityExtnColumnDetails_ColumnMetadataId] NVARCHAR (255)   NOT NULL,
    [EntityExtnColumnDetails_ColumnName]       NVARCHAR (255)   NOT NULL,
    [EntityExtnColumnDetails_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [EntityExtnColumnDetails_CreatedOn]        DATETIME         NOT NULL,
    [EntityExtnColumnDetails_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [EntityExtnColumnDetails_UpdatedOn]        DATETIME         NULL,
    [EntityExtnColumnDetails_Status]           BIT              NULL
);

