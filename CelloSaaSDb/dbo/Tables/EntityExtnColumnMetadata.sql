﻿CREATE TABLE [dbo].[EntityExtnColumnMetadata] (
    [EntityExtnColumnMetadata_Id]         UNIQUEIDENTIFIER CONSTRAINT [DF_EntityExtnColumnMetadata_ID] DEFAULT (newsequentialid()) NOT NULL,
    [EntityExtnColumnMetadata_EntityId]   NVARCHAR (255)   NOT NULL,
    [DataFieldType_ID]                    UNIQUEIDENTIFIER NOT NULL,
    [EntityExtnColumnMetadata_NoOfColumn] INT              NOT NULL,
    [EntityExtnColumnMetadata_Dimension]  VARCHAR (50)     NOT NULL,
    [EntityExtnColumnMetadata_CreatedBy]  UNIQUEIDENTIFIER NOT NULL,
    [EntityExtnColumnMetadata_CreatedOn]  DATETIME         NOT NULL,
    [EntityExtnColumnMetadata_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [EntityExtnColumnMetadata_UpdatedOn]  DATETIME         NULL,
    [EntityExtnColumnMetadata_Status]     BIT              NOT NULL,
    CONSTRAINT [PK_EntityExtnColumnMetadata] PRIMARY KEY CLUSTERED ([EntityExtnColumnMetadata_Id] ASC),
    FOREIGN KEY ([DataFieldType_ID]) REFERENCES [dbo].[DataFieldType] ([DataFieldType_ID])
);

