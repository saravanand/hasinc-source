﻿CREATE TABLE [dbo].[EntityFeaturePrivilege] (
    [EntityFeaturePrivilege_Code]        UNIQUEIDENTIFIER CONSTRAINT [DF_EntityFeaturePrivilege_EntityFeaturePrivilege_Code] DEFAULT (newsequentialid()) NOT NULL,
    [EntityFeaturePrivilege_FeatureCode] NVARCHAR (255)   NOT NULL,
    [EntityFeaturePrivilege_EntityId]    NVARCHAR (255)   NOT NULL,
    [EntityFeaturePrivilege_CreatedOn]   DATETIME         NOT NULL,
    [EntityFeaturePrivilege_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [EntityFeaturePrivilege_UpdatedOn]   DATETIME         NULL,
    [EntityFeaturePrivilege_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [EntityFeaturePrivilege_Status]      BIT              NOT NULL,
    [EntityFeaturePrivilege_TenantId]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EntityFeaturePrivilege] PRIMARY KEY CLUSTERED ([EntityFeaturePrivilege_Code] ASC),
    CONSTRAINT [FK_FeatureEntity_Entity] FOREIGN KEY ([EntityFeaturePrivilege_EntityId]) REFERENCES [dbo].[Entity] ([Entity_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityFeaturePrivilege_EntityId]
    ON [dbo].[EntityFeaturePrivilege]([EntityFeaturePrivilege_EntityId] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityFeaturePrivilege_FeatureCode]
    ON [dbo].[EntityFeaturePrivilege]([EntityFeaturePrivilege_FeatureCode] ASC);

