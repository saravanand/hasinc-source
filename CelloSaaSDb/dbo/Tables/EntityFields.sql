﻿CREATE TABLE [dbo].[EntityFields] (
    [EntityFields_TenantId]              UNIQUEIDENTIFIER NULL,
    [EntityFields_EntityId]              NVARCHAR (255)   NOT NULL,
    [EntityFields_DataFieldTypeID]       UNIQUEIDENTIFIER NOT NULL,
    [EntityFields_Id]                    NVARCHAR (255)   NOT NULL,
    [EntityFields_Name]                  NVARCHAR (1000)  NOT NULL,
    [EntityFields_IsUnique]              BIT              NULL,
    [EntityFields_AvailableInMainSchema] BIT              NULL,
    [EntityFields_PickUpListId]          UNIQUEIDENTIFIER NULL,
    [EntityFields_CreatedOn]             DATETIME         NULL,
    [EntityFields_CreatedBy]             UNIQUEIDENTIFIER NULL,
    [EntityFields_UpdatedOn]             DATETIME         NULL,
    [EntityFields_UpdatedBy]             UNIQUEIDENTIFIER NULL,
    [EntityFields_Status]                BIT              NULL,
    [EntityFields_Length]                INT              NULL,
    [EntityFields_validationRegEx]       NVARCHAR (200)   NULL,
    [EntityFields_MappedColumnName]      NVARCHAR (255)   NULL,
    [EntityFields_ExtnColumnDetailsId]   VARCHAR (MAX)    NULL,
    [EntityFields_ActivatedDate]         DATETIME         NULL,
    [EntityFields_DeactivatedDate]       DATETIME         NULL,
    [EntityFields_ReferenceEntityId]     NVARCHAR (255)   NULL,
    CONSTRAINT [FK_EntityFields_DataFieldType] FOREIGN KEY ([EntityFields_DataFieldTypeID]) REFERENCES [dbo].[DataFieldType] ([DataFieldType_ID]),
    CONSTRAINT [FK_EntityFields_PickupList] FOREIGN KEY ([EntityFields_PickUpListId]) REFERENCES [dbo].[PickupList] ([PickupList_ID]),
    CONSTRAINT [FK_EntityFields_TenantDetails] FOREIGN KEY ([EntityFields_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityFields_DataFieldTypeID]
    ON [dbo].[EntityFields]([EntityFields_DataFieldTypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityFields_EntityId]
    ON [dbo].[EntityFields]([EntityFields_EntityId] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityFields_PickUpListId]
    ON [dbo].[EntityFields]([EntityFields_PickUpListId] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityFields_TenantId]
    ON [dbo].[EntityFields]([EntityFields_TenantId] ASC);

