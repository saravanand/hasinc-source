﻿CREATE TABLE [dbo].[EntityPrivileges] (
    [EntityPrivilegesID]        UNIQUEIDENTIFIER CONSTRAINT [DF_EntityPrivileges_EntityPrivilegesID] DEFAULT (newsequentialid()) NOT NULL,
    [EntityID]                  NVARCHAR (255)   NOT NULL,
    [PrivilegeID]               NVARCHAR (255)   NOT NULL,
    [AddedOn]                   DATETIME         NULL,
    [AddedBy]                   UNIQUEIDENTIFIER NULL,
    [EditedOn]                  DATETIME         NULL,
    [EditedBy]                  UNIQUEIDENTIFIER NULL,
    [Status]                    BIT              NOT NULL,
    [EntityPrivileges_TenantId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EntityPrivileges] PRIMARY KEY CLUSTERED ([EntityPrivilegesID] ASC),
    CONSTRAINT [FK_EntityID] FOREIGN KEY ([EntityID]) REFERENCES [dbo].[Entity] ([Entity_ID]),
    CONSTRAINT [FK_PrivilegeID] FOREIGN KEY ([PrivilegeID]) REFERENCES [dbo].[Privileges] ([Privilege_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_EntityID]
    ON [dbo].[EntityPrivileges]([EntityID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_PrivilegeID]
    ON [dbo].[EntityPrivileges]([PrivilegeID] ASC);

