﻿CREATE TABLE [dbo].[EntityTenantScope] (
    [EntityTenantScope_Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF_EntityTenantScope_EntityTenantScope_Id] DEFAULT (newsequentialid()) NOT NULL,
    [EntityTenantScope_TenantId]              UNIQUEIDENTIFIER NULL,
    [EntityTenantScope_EntityId]              NVARCHAR (255)   NOT NULL,
    [EntityTenantScope_ViewTenantScopeId]     UNIQUEIDENTIFIER NULL,
    [EntityTenantScope_EditTenantScopeId]     UNIQUEIDENTIFIER NULL,
    [EntityTenantScope_DeleteTenantScopeId]   UNIQUEIDENTIFIER NULL,
    [EntityTenantScope_ViewSelectiveTenant]   VARCHAR (MAX)    NULL,
    [EntityTenantScope_DeleteSelectiveTenant] VARCHAR (MAX)    NULL,
    [EntityTenantScope_EditSelectiveTenant]   VARCHAR (MAX)    NULL,
    [EntityTenantScope_CreatedBy]             UNIQUEIDENTIFIER NOT NULL,
    [EntityTenantScope_CreatedOn]             DATETIME         NOT NULL,
    [EntityTenantScope_UpdatedBy]             UNIQUEIDENTIFIER NULL,
    [EntityTenantScope_UpdatedOn]             DATETIME         NULL,
    [EntityTenantScope_Status]                BIT              NOT NULL,
    CONSTRAINT [PK_EntityTenantScope] PRIMARY KEY CLUSTERED ([EntityTenantScope_Id] ASC)
);

