﻿CREATE TABLE [dbo].[EventAudit] (
    [EventAudit_Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF_ActivityEventLog_ActivityEventLog_Id] DEFAULT (newsequentialid()) NOT NULL,
    [EventAudit_EventId]             UNIQUEIDENTIFIER NULL,
    [EventAudit_TenantId]            UNIQUEIDENTIFIER NULL,
    [EventAudit_UserId]              UNIQUEIDENTIFIER NULL,
    [EventAudit_SubjectId]           NVARCHAR (255)   NULL,
    [EventAudit_SubjectType]         NVARCHAR (150)   NULL,
    [EventAudit_SubjectValue]        XML              NULL,
    [EventAudit_SubjectXml]          XML              NULL,
    [EventAudit_TargetId]            NVARCHAR (255)   NULL,
    [EventAudit_TargetType]          VARCHAR (100)    NULL,
    [EventAudit_TargetValue]         XML              NULL,
    [EventAudit_TargetXml]           XML              NULL,
    [EventAudit_ContextId]           NVARCHAR (255)   NULL,
    [EventAudit_ContextType]         VARCHAR (50)     NULL,
    [EventAudit_ContextValue]        XML              NULL,
    [EventAudit_ContextXml]          XML              NULL,
    [EventAudit_EventStatus]         VARCHAR (50)     NULL,
    [EventAudit_Priority]            VARCHAR (50)     NULL,
    [EventAudit_CreatedOn]           DATETIME         NOT NULL,
    [EventAudit_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [EventAudit_UpdatedOn]           DATETIME         NULL,
    [EventAudit_UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [EventAudit_Status]              BIT              NOT NULL,
    [EventAudit_EventParameterValue] XML              NULL,
    [EventAudit_EventParameterXml]   XML              NULL,
    CONSTRAINT [PK_ActivityEventLog] PRIMARY KEY CLUSTERED ([EventAudit_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EventAudit_TenantId]
    ON [dbo].[EventAudit]([EventAudit_TenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-CreatedOn]
    ON [dbo].[EventAudit]([EventAudit_CreatedOn] ASC);

