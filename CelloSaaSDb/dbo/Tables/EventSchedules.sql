﻿CREATE TABLE [dbo].[EventSchedules] (
    [EventSchedule_Id]         UNIQUEIDENTIFIER CONSTRAINT [DF_EventSchedule_EventSchedule_Id] DEFAULT (newsequentialid()) NOT NULL,
    [EventSchedule_EventId]    UNIQUEIDENTIFIER NOT NULL,
    [EventSchedule_ScheduleId] UNIQUEIDENTIFIER NOT NULL,
    [EventSchedule_CreatedBy]  UNIQUEIDENTIFIER NULL,
    [EventSchedule_CreatedOn]  DATETIME         CONSTRAINT [DF_EventSchedule_EventSchedule_CreatedOn] DEFAULT (getdate()) NULL,
    [EventSchedule_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [EventSchedule_UpdatedOn]  DATETIME         NULL,
    [EventSchedule_Status]     BIT              CONSTRAINT [DF_EventSchedule_EventSchedule_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_EventSchedule] PRIMARY KEY CLUSTERED ([EventSchedule_Id] ASC),
    CONSTRAINT [FK_EventSchedules_Events] FOREIGN KEY ([EventSchedule_EventId]) REFERENCES [dbo].[Events] ([Event_Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_EventSchedules_Schedules] FOREIGN KEY ([EventSchedule_ScheduleId]) REFERENCES [dbo].[Schedules] ([Schedule_Id]) ON DELETE CASCADE
);

