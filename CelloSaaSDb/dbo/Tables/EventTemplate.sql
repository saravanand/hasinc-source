﻿CREATE TABLE [dbo].[EventTemplate] (
    [EventTemplate_Id]         UNIQUEIDENTIFIER CONSTRAINT [DF_EventTemplate_EventTemplateId] DEFAULT (newsequentialid()) NOT NULL,
    [EventTemplate_EventId]    UNIQUEIDENTIFIER NOT NULL,
    [EventTemplate_TenantId]   UNIQUEIDENTIFIER NOT NULL,
    [EventTemplate_TemplateId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_EventTemplate] PRIMARY KEY CLUSTERED ([EventTemplate_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EventTemplate_EventId]
    ON [dbo].[EventTemplate]([EventTemplate_EventId] ASC);

