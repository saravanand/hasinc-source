﻿CREATE TABLE [dbo].[Events] (
    [Event_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_Event_Event_Id] DEFAULT (newsequentialid()) NOT NULL,
    [Event_Name]        NVARCHAR (255)   NOT NULL,
    [Event_Category]    NVARCHAR (255)   NULL,
    [Event_Description] NVARCHAR (MAX)   NULL,
    [Event_TenantId]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED ([Event_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [EventName]
    ON [dbo].[Events]([Event_Name] ASC) WITH (FILLFACTOR = 100);

