﻿CREATE TABLE [dbo].[ExceptionCategoryLog] (
    [CategoryLogID] INT IDENTITY (1, 1) NOT NULL,
    [CategoryID]    INT NOT NULL,
    [LogID]         INT NOT NULL,
    CONSTRAINT [PK_CategoryLog] PRIMARY KEY CLUSTERED ([CategoryLogID] ASC),
    CONSTRAINT [FK_CategoryLog_Category] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[ExceptionCategory] ([CategoryID]),
    CONSTRAINT [FK_CategoryLog_Log] FOREIGN KEY ([LogID]) REFERENCES [dbo].[ExceptionLog] ([LogID])
);


GO
CREATE NONCLUSTERED INDEX [ixCategoryLog]
    ON [dbo].[ExceptionCategoryLog]([LogID] ASC, [CategoryID] ASC);

