﻿CREATE TABLE [dbo].[ExceptionSubscriptions] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [Name]           NVARCHAR (MAX)   NULL,
    [Description]    NVARCHAR (MAX)   NULL,
    [To]             NVARCHAR (MAX)   NULL,
    [Cc]             NVARCHAR (MAX)   NULL,
    [NotificationId] UNIQUEIDENTIFIER NOT NULL,
    [Categories]     NVARCHAR (MAX)   NULL,
    [CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]      DATETIME         NOT NULL,
    [UpdatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]      DATETIME         NULL,
    [Status]         BIT              NOT NULL,
    CONSTRAINT [PK_dbo.ExceptionSubscriptions] PRIMARY KEY CLUSTERED ([Id] ASC)
);

