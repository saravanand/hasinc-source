﻿CREATE TABLE [dbo].[FeaturePrivileges] (
    [FeaturePrivileges_Code]          UNIQUEIDENTIFIER CONSTRAINT [DF_FeaturePrivileges_FeaturePrivileges_Code] DEFAULT (newsequentialid()) NOT NULL,
    [FeaturePrivileges_FeatureCode]   NVARCHAR (255)   NOT NULL,
    [FeaturePrivileges_PrivilegeCode] NVARCHAR (255)   NULL,
    [FeaturePrivileges_CreatedOn]     DATETIME         NOT NULL,
    [FeaturePrivileges_CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [FeaturePrivileges_UpdatedOn]     DATETIME         NULL,
    [FeaturePrivileges_UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [FeaturePrivileges_Status]        BIT              NOT NULL,
    [FeaturePrivileges_TenantId]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_FeaturePrivileges] PRIMARY KEY CLUSTERED ([FeaturePrivileges_Code] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Ind_FeaturePrivileges_FeatureCode]
    ON [dbo].[FeaturePrivileges]([FeaturePrivileges_FeatureCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_FeaturePrivileges_PrivilegeCode]
    ON [dbo].[FeaturePrivileges]([FeaturePrivileges_PrivilegeCode] ASC);

