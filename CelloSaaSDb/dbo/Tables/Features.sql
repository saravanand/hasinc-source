﻿CREATE TABLE [dbo].[Features] (
    [Feature_Code]       NVARCHAR (255)   NOT NULL,
    [Feature_Name]       NVARCHAR (255)   NOT NULL,
    [Feature_ModuleCode] NVARCHAR (255)   NOT NULL,
    [Feature_CreatedOn]  DATETIME         NOT NULL,
    [Feature_CreatedBy]  UNIQUEIDENTIFIER NOT NULL,
    [Feature_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [Feature_UpdatedOn]  DATETIME         NULL,
    [Feature_Status]     BIT              NOT NULL,
    CONSTRAINT [PK__Features__75CE315503317E3D] PRIMARY KEY CLUSTERED ([Feature_Code] ASC),
    CONSTRAINT [FK_Features_Modules] FOREIGN KEY ([Feature_ModuleCode]) REFERENCES [dbo].[Modules] ([Module_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Feature_ModuleCode]
    ON [dbo].[Features]([Feature_ModuleCode] ASC);

