﻿CREATE TABLE [dbo].[FieldType] (
    [FieldType_ID]        INT              NOT NULL,
    [FieldType_Name]      VARCHAR (50)     NOT NULL,
    [FieldType_CreatedOn] DATETIME         NOT NULL,
    [FieldType_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [FieldType_UpdatedOn] DATETIME         NULL,
    [FieldType_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [FieldType_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_FieldType] PRIMARY KEY CLUSTERED ([FieldType_ID] ASC)
);

