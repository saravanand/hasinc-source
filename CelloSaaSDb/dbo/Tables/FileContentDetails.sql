﻿CREATE TABLE [dbo].[FileContentDetails] (
    [FileContentDetails_ContentDetailsId] UNIQUEIDENTIFIER NOT NULL,
    [FileContentDetails_FileTemplateId]   UNIQUEIDENTIFIER NULL,
    [FileContentDetails_FilePath]         NVARCHAR (255)   NULL,
    [FileContentDetails_FileName]         NVARCHAR (255)   NULL,
    [FileContentDetails_CreatedBy]        UNIQUEIDENTIFIER NULL,
    [FileContentDetails_CreatedOn]        DATETIME         DEFAULT (getdate()) NOT NULL,
    [FileContentDetails_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [FileContentDetails_UpdatedOn]        DATETIME         NULL,
    [FileContentDetails_Status]           BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_FileContentDetails] PRIMARY KEY CLUSTERED ([FileContentDetails_ContentDetailsId] ASC),
    CONSTRAINT [FK_FileContentDetails_NotifiactionTemplate] FOREIGN KEY ([FileContentDetails_FileTemplateId]) REFERENCES [dbo].[NotificationTemplate] ([NotificationTemplate_Id]),
    CONSTRAINT [FK_FileContentDetails_NotificationContentDetails1] FOREIGN KEY ([FileContentDetails_ContentDetailsId]) REFERENCES [dbo].[NotificationContentDetails] ([NotificationContentDetails_Id])
);

