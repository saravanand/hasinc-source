﻿CREATE TABLE [dbo].[FileNotificationContent] (
    [FileNotificationContent_NotificationContentId] UNIQUEIDENTIFIER NOT NULL,
    [FileNotificationContent_ArchivedFilePath]      VARCHAR (100)    NULL,
    [FileNotificationContent_FileType]              VARCHAR (50)     NULL,
    [FileNotificationContent_FileName]              VARCHAR (100)    NULL,
    [FileNotificationContent_CreatedOn]             DATETIME         NOT NULL,
    [FileNotificationContent_CreatedBy]             UNIQUEIDENTIFIER NOT NULL,
    [FileNotificationContent_UpdatedOn]             DATETIME         NULL,
    [FileNotificationContent_UpdatedBy]             UNIQUEIDENTIFIER NULL,
    [FileNotificationContent_Status]                BIT              NOT NULL,
    CONSTRAINT [PK_FileNotificationContent] PRIMARY KEY CLUSTERED ([FileNotificationContent_NotificationContentId] ASC),
    CONSTRAINT [FK_FileNotificationContent_NotificationContent] FOREIGN KEY ([FileNotificationContent_NotificationContentId]) REFERENCES [dbo].[NotificationContent] ([NotificationContent_Id])
);

