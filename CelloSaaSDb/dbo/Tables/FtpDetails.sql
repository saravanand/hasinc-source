﻿CREATE TABLE [dbo].[FtpDetails] (
    [FtpDetails_Id]            UNIQUEIDENTIFIER CONSTRAINT [DF_FtpDetails_FtpDetails_Id] DEFAULT (newsequentialid()) NOT NULL,
    [FtpDetails_BatchId]       UNIQUEIDENTIFIER NULL,
    [FtpDetails_FtpAddress]    VARCHAR (100)    NOT NULL,
    [FtpDetails_FtpUserName]   NVARCHAR (255)   NOT NULL,
    [FtpDetails_FtpPassword]   NVARCHAR (255)   NOT NULL,
    [FtpDetails_FilePath]      NVARCHAR (255)   NULL,
    [FtpDetails_FileName]      NVARCHAR (255)   NULL,
    [FtpDetails_FtpStatus]     VARCHAR (50)     NOT NULL,
    [FtpDetails_StatusMessage] NVARCHAR (MAX)   NULL,
    [FtpDetails_CreatedOn]     DATETIME         NOT NULL,
    [FtpDetails_CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [FtpDetails_UpdatedOn]     DATETIME         NULL,
    [FtpDetails_UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [FtpDetails_IsSecured]     BIT              NOT NULL,
    [FtpDetails_TrackId]       NVARCHAR (255)   NULL,
    CONSTRAINT [PK_FtpDetails] PRIMARY KEY CLUSTERED ([FtpDetails_Id] ASC),
    CONSTRAINT [FK_FtpDetails_NotificationBatch] FOREIGN KEY ([FtpDetails_BatchId]) REFERENCES [dbo].[NotificationBatch] ([NotificationBatch_Id])
);

