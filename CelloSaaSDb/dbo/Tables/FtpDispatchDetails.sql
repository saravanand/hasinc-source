﻿CREATE TABLE [dbo].[FtpDispatchDetails] (
    [FtpDispatchDetails_DispatchDetailsId] UNIQUEIDENTIFIER NOT NULL,
    [FtpDispatchDetails_FtpAddress]        VARCHAR (100)    NOT NULL,
    [FtpDispatchDetails_UserName]          NVARCHAR (255)   NOT NULL,
    [FtpDispatchDetails_Password]          NVARCHAR (255)   NOT NULL,
    [FtpDispatchDetails_IsSecured]         BIT              NOT NULL,
    [FtpDispatchDetails_CreatedBy]         UNIQUEIDENTIFIER NULL,
    [FtpDispatchDetails_CreatedOn]         DATETIME         DEFAULT (getdate()) NOT NULL,
    [FtpDispatchDetails_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [FtpDispatchDetails_UpdatedOn]         DATETIME         NULL,
    [FtpDispatchDetails_Status]            BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_FtpDispatchDetails] PRIMARY KEY CLUSTERED ([FtpDispatchDetails_DispatchDetailsId] ASC),
    CONSTRAINT [FK_FtpDispatchDetails_NotificationDispatchDetails] FOREIGN KEY ([FtpDispatchDetails_DispatchDetailsId]) REFERENCES [dbo].[NotificationDispatchDetails] ([NotificationDispatchDetails_Id])
);

