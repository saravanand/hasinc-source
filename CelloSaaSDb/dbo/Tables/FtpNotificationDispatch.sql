﻿CREATE TABLE [dbo].[FtpNotificationDispatch] (
    [FtpNotificationDispatch_NotificationDispatchId] UNIQUEIDENTIFIER NOT NULL,
    [FtpNotificationDispatch_FtpAddress]             VARCHAR (100)    NOT NULL,
    [FtpNotificationDispatch_UserName]               VARCHAR (100)    NOT NULL,
    [FtpNotificationDispatch_Password]               VARCHAR (50)     NOT NULL,
    [FtpNotificationDispatch_IsSecured]              BIT              NOT NULL,
    [FtpNotificationDispatch_CreatedOn]              DATETIME         NOT NULL,
    [FtpNotificationDispatch_CreatedBy]              UNIQUEIDENTIFIER NOT NULL,
    [FtpNotificationDispatch_UpdatedOn]              DATETIME         NULL,
    [FtpNotificationDispatch_UpdatedBy]              UNIQUEIDENTIFIER NULL,
    [FtpNotificationDispatch_Status]                 BIT              NOT NULL,
    CONSTRAINT [PK_FtpNotificationDispatch] PRIMARY KEY CLUSTERED ([FtpNotificationDispatch_NotificationDispatchId] ASC),
    CONSTRAINT [FK_FtpNotificationDispatch_NotificationDispatch] FOREIGN KEY ([FtpNotificationDispatch_NotificationDispatchId]) REFERENCES [dbo].[NotificationDispatch] ([NotificationDispatch_Id])
);

