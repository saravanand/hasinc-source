﻿CREATE TABLE [dbo].[ImageSource] (
    [ImageSource_SourceId]   UNIQUEIDENTIFIER CONSTRAINT [DF_ImageSource_ImageSource_SourceId] DEFAULT (newsequentialid()) NOT NULL,
    [ImageSource_Name]       NVARCHAR (255)   NULL,
    [ImageSource_SourcePath] NVARCHAR (255)   NOT NULL,
    [ImageSource_TenantId]   UNIQUEIDENTIFIER NULL,
    [ImageSource_CreatedBy]  UNIQUEIDENTIFIER NOT NULL,
    [ImageSource_CreatedOn]  DATETIME         NOT NULL,
    [ImageSource_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [ImageSource_UpdatedOn]  DATETIME         NULL,
    [ImageSource_Status]     BIT              NOT NULL,
    CONSTRAINT [PK_ImageSource] PRIMARY KEY CLUSTERED ([ImageSource_SourceId] ASC),
    CONSTRAINT [FK_ImageSource_TenantDetails] FOREIGN KEY ([ImageSource_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

