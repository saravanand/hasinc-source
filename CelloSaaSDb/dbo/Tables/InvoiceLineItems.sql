﻿CREATE TABLE [dbo].[InvoiceLineItems] (
    [Id]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [InvoiceId]      UNIQUEIDENTIFIER NOT NULL,
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [Name]           NVARCHAR (255)   NOT NULL,
    [Description]    NVARCHAR (MAX)   NULL,
    [Amount]         FLOAT (53)       NOT NULL,
    [Type]           NVARCHAR (255)   NULL,
    [Usage]          NVARCHAR (MAX)   NULL,
    [IsUserAdded]    BIT              NOT NULL,
    [ParentTenantId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_InvoiceLineItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_InvoiceInvoiceLineItems] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoices] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_InvoiceInvoiceLineItems]
    ON [dbo].[InvoiceLineItems]([InvoiceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InvoiceLineItems_ParentTenantId]
    ON [dbo].[InvoiceLineItems]([ParentTenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InvoiceLineItems_TenantId]
    ON [dbo].[InvoiceLineItems]([TenantId] ASC);

