﻿CREATE TABLE [dbo].[InvoicePayments] (
    [Id]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [InvoiceId]      UNIQUEIDENTIFIER NOT NULL,
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [PaymentMode]    INT              NOT NULL,
    [PaidAmount]     FLOAT (53)       NOT NULL,
    [PaymentDate]    DATETIME         NOT NULL,
    [CreatedBy]      NVARCHAR (200)   NOT NULL,
    [Notes]          NVARCHAR (MAX)   NULL,
    [ParentTenantId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_InvoicePayments] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_InvoicePayments_Invoices] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoices] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_InvoicePayments_Invoices]
    ON [dbo].[InvoicePayments]([InvoiceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InvoicePayments_ParentTenantId]
    ON [dbo].[InvoicePayments]([ParentTenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_InvoicePayments_TenantId]
    ON [dbo].[InvoicePayments]([TenantId] ASC);

