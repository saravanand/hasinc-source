﻿CREATE TABLE [dbo].[Invoices] (
    [Id]              UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [InvoiceNo]       NVARCHAR (250)   NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [PackageId]       UNIQUEIDENTIFIER NOT NULL,
    [PricePlanId]     UNIQUEIDENTIFIER NOT NULL,
    [InvoiceDate]     DATETIME         NOT NULL,
    [StartDate]       DATETIME         NOT NULL,
    [EndDate]         DATETIME         NOT NULL,
    [NextInvoiceDate] DATETIME         NOT NULL,
    [InvoiceStatus]   INT              NOT NULL,
    [InvoiceType]     INT              DEFAULT ((0)) NOT NULL,
    [Memo]            NVARCHAR (MAX)   NULL,
    [DueDate]         DATETIME         NOT NULL,
    [CreatedOn]       DATETIME         NOT NULL,
    [CreatedBy]       NVARCHAR (200)   NOT NULL,
    [UpdatedOn]       DATETIME         NULL,
    [UpdatedBy]       NVARCHAR (200)   NULL,
    [BillFrequency]   INT              NOT NULL,
    [PendingBalance]  FLOAT (53)       NOT NULL,
    [ParentTenantId]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Invoices_InvoiceDate]
    ON [dbo].[Invoices]([InvoiceDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Invoices_ParentTenatnId]
    ON [dbo].[Invoices]([ParentTenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Invoices_TenantId]
    ON [dbo].[Invoices]([TenantId] ASC);

