﻿CREATE TABLE [dbo].[JobAudits] (
    [JobAudit_Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_JobAudit_JobAudit_Id] DEFAULT (newsequentialid()) NOT NULL,
    [JobAudit_JobId]            UNIQUEIDENTIFIER NOT NULL,
    [JobAudit_StartTime]        DATETIME         NOT NULL,
    [JobAudit_EndTime]          DATETIME         NULL,
    [JobAudit_ExecutionStatus]  BIT              NOT NULL,
    [JobAudit_ExceptionDetails] NVARCHAR (MAX)   NULL,
    [JobAudit_CreatedBy]        UNIQUEIDENTIFIER NULL,
    [JobAudit_CreatedOn]        DATETIME         CONSTRAINT [DF_JobAudit_JobAudit_CreatedOn] DEFAULT (getdate()) NULL,
    [JobAudit_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [JobAudit_UpdatedOn]        DATETIME         NULL,
    [JobAudit_Status]           BIT              CONSTRAINT [DF_JobAudit_JobAudit_Status] DEFAULT ((1)) NOT NULL,
    [JobAudit_TenantId]         UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_JobAudit] PRIMARY KEY CLUSTERED ([JobAudit_Id] ASC),
    CONSTRAINT [FK_JobAudits_Jobs] FOREIGN KEY ([JobAudit_JobId]) REFERENCES [dbo].[Jobs] ([Job_Id])
);

