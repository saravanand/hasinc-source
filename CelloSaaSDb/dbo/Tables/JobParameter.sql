﻿CREATE TABLE [dbo].[JobParameter] (
    [JobParameter_Id]           UNIQUEIDENTIFIER CONSTRAINT [DF_JobParameter_JobParameter_Id] DEFAULT (newsequentialid()) NOT NULL,
    [JobParameter_EventId]      UNIQUEIDENTIFIER NULL,
    [JobParameter_TenantId]     UNIQUEIDENTIFIER NULL,
    [JobParameter_JobId]        UNIQUEIDENTIFIER NULL,
    [JobParameter_ParameterXML] XML              NULL,
    [JobParameter_CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [JobParameter_CreatedOn]    DATETIME         NOT NULL,
    [JobParameter_UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [JobParameter_UpdatedOn]    DATETIME         NULL,
    [JobParameter_Status]       BIT              NOT NULL,
    CONSTRAINT [PK_JobParameter] PRIMARY KEY CLUSTERED ([JobParameter_Id] ASC)
);

