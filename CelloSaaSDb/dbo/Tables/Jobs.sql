﻿CREATE TABLE [dbo].[Jobs] (
    [Job_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_Jobs_Job_Id] DEFAULT (newsequentialid()) NOT NULL,
    [Job_Name]        NVARCHAR (255)   NOT NULL,
    [Job_Type]        NVARCHAR (255)   NOT NULL,
    [Job_Description] NVARCHAR (MAX)   NULL,
    [Job_CreatedBy]   UNIQUEIDENTIFIER NULL,
    [Job_CreatedOn]   DATETIME         NULL,
    [Job_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Job_UpdatedOn]   DATETIME         NULL,
    [Job_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED ([Job_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [jobname]
    ON [dbo].[Jobs]([Job_Name] ASC) WITH (FILLFACTOR = 100);

