﻿CREATE TABLE [dbo].[Language] (
    [Language_Id]        UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Language_Name]      NVARCHAR (255)   NOT NULL,
    [Language_Code]      NVARCHAR (100)   NOT NULL,
    [Language_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [Language_CreatedOn] DATETIME         NOT NULL,
    [Language_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [Language_UpdatedOn] DATETIME         NULL,
    [Language_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([Language_Id] ASC)
);

