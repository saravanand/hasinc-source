﻿CREATE TABLE [dbo].[LicenseFeature] (
    [LicenseFeature_ID]                   UNIQUEIDENTIFIER CONSTRAINT [DF_LicenseFeature_LicenseFeature_ID] DEFAULT (newsequentialid()) NOT NULL,
    [LicenseFeature_TenantLicenseCode]    UNIQUEIDENTIFIER NOT NULL,
    [LicenseFeature_PurchasedFeatureCode] NVARCHAR (255)   NOT NULL,
    [LicenseFeature_ValidityStartDate]    DATETIME         NOT NULL,
    [LicenseFeature_ValidityEndDate]      DATETIME         NULL,
    [LicenseFeature_CreatedOn]            DATETIME         NOT NULL,
    [LicenseFeature_CreatedBy]            UNIQUEIDENTIFIER NOT NULL,
    [LicenseFeature_UpdatedOn]            DATETIME         NULL,
    [LicenseFeature_UpdatedBy]            UNIQUEIDENTIFIER NULL,
    [LicenseFeature_Status]               BIT              NOT NULL,
    CONSTRAINT [PK_LicenseFeature] PRIMARY KEY CLUSTERED ([LicenseFeature_ID] ASC),
    CONSTRAINT [FK_LicenseFeature_Features] FOREIGN KEY ([LicenseFeature_PurchasedFeatureCode]) REFERENCES [dbo].[Features] ([Feature_Code]),
    CONSTRAINT [FK_LicenseFeature_TenantLicense] FOREIGN KEY ([LicenseFeature_TenantLicenseCode]) REFERENCES [dbo].[TenantLicense] ([TenantLicense_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseFeature_PurchasedFeatureCode]
    ON [dbo].[LicenseFeature]([LicenseFeature_PurchasedFeatureCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseFeature_TenantLicenseCode]
    ON [dbo].[LicenseFeature]([LicenseFeature_TenantLicenseCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseFeature_ValidityEndDate]
    ON [dbo].[LicenseFeature]([LicenseFeature_ValidityEndDate] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseFeature_ValidityStartDate]
    ON [dbo].[LicenseFeature]([LicenseFeature_ValidityStartDate] ASC);

