﻿CREATE TABLE [dbo].[LicenseModule] (
    [LicenseModule_TenantLicenseCode]   UNIQUEIDENTIFIER CONSTRAINT [DF_LicenseModule_LicenseModule_TenantLicenseCode] DEFAULT (newsequentialid()) NOT NULL,
    [LicenseModule_PurchasedModuleCode] NVARCHAR (255)   NOT NULL,
    [LicenseModule_NumberOfUsers]       INT              NULL,
    [LicenseModule_ValidityStart]       DATETIME         NOT NULL,
    [LicenseModule_ValidityEnd]         DATETIME         NULL,
    [LicenseModule_CreatedOn]           DATETIME         NOT NULL,
    [LicenseModule_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [LicenseModule_UpdatedOn]           DATETIME         NULL,
    [LicenseModule_UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [LicenseModule_Status]              BIT              NOT NULL,
    [LicenseModule_ID]                  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_LicenseModule] PRIMARY KEY CLUSTERED ([LicenseModule_ID] ASC),
    CONSTRAINT [FK_License_Module_Modules] FOREIGN KEY ([LicenseModule_PurchasedModuleCode]) REFERENCES [dbo].[Modules] ([Module_Code]),
    CONSTRAINT [FK_LicenseModule_TenantLicense] FOREIGN KEY ([LicenseModule_TenantLicenseCode]) REFERENCES [dbo].[TenantLicense] ([TenantLicense_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseModule_PurchasedModuleCode]
    ON [dbo].[LicenseModule]([LicenseModule_PurchasedModuleCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseModule_TenantLicenseCode]
    ON [dbo].[LicenseModule]([LicenseModule_TenantLicenseCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseModule_ValidityEnd]
    ON [dbo].[LicenseModule]([LicenseModule_ValidityEnd] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseModule_ValidityStart]
    ON [dbo].[LicenseModule]([LicenseModule_ValidityStart] ASC);

