﻿CREATE TABLE [dbo].[LicensePackage] (
    [LicensePackage_ID]                  UNIQUEIDENTIFIER CONSTRAINT [DF_LicensePackage_LicensePackage_ID] DEFAULT (newsequentialid()) NOT NULL,
    [LicensePackage_Name]                NVARCHAR (250)   NOT NULL,
    [LicensePackage_Description]         NVARCHAR (MAX)   NULL,
    [LicensePackage_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [LicensePackage_CreatedOn]           DATETIME         NOT NULL,
    [LicensePackage_UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [LicensePackage_UpdatedOn]           DATETIME         NULL,
    [LicensePackage_Status]              BIT              NOT NULL,
    [LicensePackage_TenantId]            UNIQUEIDENTIFIER NULL,
    [LicensePackage_IntegrateForBilling] BIT              CONSTRAINT [DF_LicensePackage_LicensePackage_IntegrateForBilling] DEFAULT ((0)) NOT NULL,
    [LicensePackage_IsEvaluation]        BIT              CONSTRAINT [DF_LicensePackage_LicensePackage_IsEvaluation] DEFAULT ((0)) NOT NULL,
    [LicensePackage_ExpiryDays]          INT              CONSTRAINT [DF_LicensePackage_LicensePackage_ExpiryDays] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LicensePackage] PRIMARY KEY CLUSTERED ([LicensePackage_ID] ASC)
);

