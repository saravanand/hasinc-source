﻿CREATE TABLE [dbo].[LicensePackageFeature] (
    [LicensePackageFeature_ID]               UNIQUEIDENTIFIER CONSTRAINT [DF_LicensePackageFeature_LicensePackageFeature_ID] DEFAULT (newsequentialid()) NOT NULL,
    [LicensePackageFeature_LicensePackageID] UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageFeature_FeatureID]        NVARCHAR (255)   NOT NULL,
    [LicensePackageFeature_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageFeature_CreatedOn]        DATETIME         NOT NULL,
    [LicensePackageFeature_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [LicensePackageFeature_UpdatedOn]        DATETIME         NULL,
    [LicensePackageFeature_Status]           BIT              NOT NULL,
    [LicensePackageFeature_IsAssignable]     BIT              NOT NULL,
    [LicensePackageFeature_IsPossessed]      BIT              NOT NULL,
    CONSTRAINT [PK_LicensePackageFeature] PRIMARY KEY CLUSTERED ([LicensePackageFeature_ID] ASC),
    CONSTRAINT [FK_LicensePackageFeature_LicensePackage] FOREIGN KEY ([LicensePackageFeature_LicensePackageID]) REFERENCES [dbo].[LicensePackage] ([LicensePackage_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_LicensePackageFeature_FeatureID]
    ON [dbo].[LicensePackageFeature]([LicensePackageFeature_FeatureID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicensePackageFeature_LicensePackageID]
    ON [dbo].[LicensePackageFeature]([LicensePackageFeature_LicensePackageID] ASC);

