﻿CREATE TABLE [dbo].[LicensePackageModule] (
    [LicensePackageModule_ID]               UNIQUEIDENTIFIER CONSTRAINT [DF_LicensePackageModule_LicensePackageModule_ID] DEFAULT (newsequentialid()) NOT NULL,
    [LicensePackageModule_ModuleID]         NVARCHAR (255)   NOT NULL,
    [LicensePackageModule_LicensepackageID] UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageModule_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageModule_CreatedOn]        DATETIME         NOT NULL,
    [LicensePackageModule_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [LicensePackageModule_UpdatedOn]        DATETIME         NULL,
    [LicensePackageModule_Status]           BIT              NOT NULL,
    [LicensePackageModule_IsAssignable]     BIT              NOT NULL,
    [LicensePackageModule_IsPossessed]      BIT              NOT NULL,
    CONSTRAINT [PK_LicensePackageModule] PRIMARY KEY CLUSTERED ([LicensePackageModule_ID] ASC),
    CONSTRAINT [FK_LicensePackageModule_LicensePackage] FOREIGN KEY ([LicensePackageModule_LicensepackageID]) REFERENCES [dbo].[LicensePackage] ([LicensePackage_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_LicensePackageModule_LicensepackageID]
    ON [dbo].[LicensePackageModule]([LicensePackageModule_LicensepackageID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicensePackageModule_ModuleID]
    ON [dbo].[LicensePackageModule]([LicensePackageModule_ModuleID] ASC);

