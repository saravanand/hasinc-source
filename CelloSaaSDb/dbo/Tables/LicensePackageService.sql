﻿CREATE TABLE [dbo].[LicensePackageService] (
    [LicensePackageService_ID]               UNIQUEIDENTIFIER CONSTRAINT [DF_LicensePackageService_LicensePackageService_ID] DEFAULT (newsequentialid()) NOT NULL,
    [LicensePackageService_ServiceID]        VARCHAR (50)     NOT NULL,
    [LicensePackageService_LicensepackageID] UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageService_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageService_CreatedOn]        DATETIME         NOT NULL,
    [LicensePackageService_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [LicensePackageService_UpdatedOn]        DATETIME         NULL,
    [LicensePackageService_Status]           BIT              NOT NULL,
    [LicensePackageService_IsAssignable]     BIT              NULL,
    [LicensePackageService_IsPossessed]      BIT              NULL,
    CONSTRAINT [PK_LicensePackageService] PRIMARY KEY CLUSTERED ([LicensePackageService_ID] ASC),
    CONSTRAINT [FK_LicensePackageService_LicensePackage] FOREIGN KEY ([LicensePackageService_LicensepackageID]) REFERENCES [dbo].[LicensePackage] ([LicensePackage_ID])
);

