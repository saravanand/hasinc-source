﻿CREATE TABLE [dbo].[LicensePackageUsage] (
    [LicensePackageUsage_ID]               UNIQUEIDENTIFIER CONSTRAINT [DF_LicensePackageUsage_LicensePackageUsage_ID] DEFAULT (newsequentialid()) NOT NULL,
    [LicensePackageUsage_UsageCode]        NVARCHAR (255)   NOT NULL,
    [LicensePackageUsage_LicensePackageID] UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageUsage_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [LicensePackageUsage_CreatedOn]        DATETIME         NOT NULL,
    [LicensePackageUsage_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [LicensePackageUsage_UpdatedOn]        DATETIME         NULL,
    [LicensePackageUsage_Status]           BIT              NOT NULL,
    [LicensePackageUsage_MinCap]           DECIMAL (18)     NULL,
    [LicensePackageUsage_MaxCap]           DECIMAL (18)     NULL,
    [LicensePackageUsage_NumberPurchased]  INT              NULL,
    [LicensePackageUsage_AllowOverUsage]   BIT              NULL,
    [LicensePackageUsage_IsAssignable]     BIT              NOT NULL,
    [LicensePackageUsage_IsPossessed]      BIT              NOT NULL,
    CONSTRAINT [PK_LicensePackageUsage] PRIMARY KEY CLUSTERED ([LicensePackageUsage_ID] ASC),
    CONSTRAINT [FK_LicensePackageUsage_LicensePackage] FOREIGN KEY ([LicensePackageUsage_LicensePackageID]) REFERENCES [dbo].[LicensePackage] ([LicensePackage_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_LicensePackageUsage_LicensePackageID]
    ON [dbo].[LicensePackageUsage]([LicensePackageUsage_LicensePackageID] ASC);

