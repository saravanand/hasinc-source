﻿CREATE TABLE [dbo].[LicenseUsage] (
    [LicenseUsage_ID]                UNIQUEIDENTIFIER CONSTRAINT [DF_LicenseUsage_LicenseUsage_ID] DEFAULT (newsequentialid()) NOT NULL,
    [LicenseUsage_TenantLicenseCode] UNIQUEIDENTIFIER NOT NULL,
    [LicenseUsage_UsageCode]         NVARCHAR (255)   NOT NULL,
    [LicenseUsage_MinCap]            DECIMAL (18)     NULL,
    [LicenseUsage_MaxCap]            DECIMAL (18)     NULL,
    [LicenseUsage_NumberPurchased]   INT              NULL,
    [LicenseUsage_PaymentTypeCode]   NVARCHAR (255)   NULL,
    [LicenseUsage_AllowOverUsage]    BIT              NOT NULL,
    [LicenseUsage_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [LicenseUsage_CreatedOn]         DATETIME         NOT NULL,
    [LicenseUsage_UpdatedOn]         DATETIME         NULL,
    [LicenseUsage_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [LicenseUsage_Status]            BIT              NOT NULL,
    CONSTRAINT [PK_LicenseUsage] PRIMARY KEY CLUSTERED ([LicenseUsage_ID] ASC),
    CONSTRAINT [FK_Licnese_Usage_PaymentType] FOREIGN KEY ([LicenseUsage_PaymentTypeCode]) REFERENCES [dbo].[PaymentType] ([PaymentType_Code]),
    CONSTRAINT [FK_Licnese_Usage_Usage] FOREIGN KEY ([LicenseUsage_UsageCode]) REFERENCES [dbo].[Usage] ([Usage_Code]),
    CONSTRAINT [FK_LicneseUsage_TenantLicense] FOREIGN KEY ([LicenseUsage_TenantLicenseCode]) REFERENCES [dbo].[TenantLicense] ([TenantLicense_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseUsage_PaymentTypeCode]
    ON [dbo].[LicenseUsage]([LicenseUsage_PaymentTypeCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseUsage_TenantLicenseCode]
    ON [dbo].[LicenseUsage]([LicenseUsage_TenantLicenseCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_LicenseUsage_UsageCode]
    ON [dbo].[LicenseUsage]([LicenseUsage_UsageCode] ASC);

