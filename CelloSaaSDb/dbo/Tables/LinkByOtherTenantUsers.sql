﻿CREATE TABLE [dbo].[LinkByOtherTenantUsers] (
    [LinkByOtherTenantUsers_Id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [LinkByOtherTenantUsers_TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [LinkByOtherTenantUsers_UserId]      UNIQUEIDENTIFIER NOT NULL,
    [LinkByOtherTenantUsers_ReqStatus]   VARCHAR (50)     NOT NULL,
    [LinkByOtherTenantUsers_Comments]    NVARCHAR (MAX)   NULL,
    [LinkByOtherTenantUsers_RequestedBy] VARCHAR (50)     NULL,
    [LinkByOtherTenantUsers_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [LinkByOtherTenantUsers_CreatedOn]   DATETIME         NOT NULL,
    [LinkByOtherTenantUsers_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [LinkByOtherTenantUsers_UpdatedOn]   DATETIME         NULL,
    [LinkByOtherTenantUsers_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_LinkByOtherTenant] PRIMARY KEY CLUSTERED ([LinkByOtherTenantUsers_Id] ASC)
);

