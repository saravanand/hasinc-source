﻿CREATE TABLE [dbo].[LocaleNames] (
    [LocaleNames_Id]          UNIQUEIDENTIFIER NOT NULL,
    [LocaleNames_Name]        NVARCHAR (100)   NOT NULL,
    [LocaleNames_Description] NVARCHAR (500)   NULL,
    [LocaleNames_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [LocaleNames_CreatedOn]   DATETIME         NOT NULL,
    [LocaleNames_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [LocaleNames_UpdatedOn]   DATETIME         NULL,
    [LocaleNames_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_LocaleNames] PRIMARY KEY CLUSTERED ([LocaleNames_Id] ASC)
);

