﻿CREATE TABLE [dbo].[LocaleTemplate] (
    [LocaleTemplate_TemplateId] UNIQUEIDENTIFIER NOT NULL,
    [LocaleTemplate_LocaleId]   UNIQUEIDENTIFIER NULL,
    [LocaleTemplate_Content]    NVARCHAR (MAX)   NOT NULL,
    [LocaleTemplate_Id]         UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [LocaleTemplate_LocaleName] VARCHAR (20)     NULL,
    [LocaleTemplate_CreatedBy]  UNIQUEIDENTIFIER DEFAULT ('3398f837-b988-4708-999d-d3dfe11875b3') NULL,
    [LocaleTemplate_CreatedOn]  DATETIME         DEFAULT (getdate()) NULL,
    [LocaleTemplate_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [LocaleTemplate_UpdatedOn]  DATETIME         NULL,
    [LocaleTemplate_Status]     BIT              DEFAULT ((1)) NULL,
    CONSTRAINT [PK_LocaleTemplate] PRIMARY KEY CLUSTERED ([LocaleTemplate_Id] ASC),
    CONSTRAINT [FK_LocaleTemplate_LocaleId] FOREIGN KEY ([LocaleTemplate_LocaleId]) REFERENCES [dbo].[LocaleNames] ([LocaleNames_Id])
);

