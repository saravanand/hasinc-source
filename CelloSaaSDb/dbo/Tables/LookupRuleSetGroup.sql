﻿CREATE TABLE [dbo].[LookupRuleSetGroup] (
    [RuleSetGroupId]          UNIQUEIDENTIFIER CONSTRAINT [DF_LookupRulesSetGroup_RuleSetGroupId] DEFAULT (newsequentialid()) NOT NULL,
    [RuleSetGroupName]        NVARCHAR (255)   NOT NULL,
    [RuleSetGroupCode]        NVARCHAR (255)   NOT NULL,
    [RuleSetGroupDescription] NVARCHAR (MAX)   NULL,
    [CreatedBy]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]             DATETIME         NOT NULL,
    [UpdatedBy]               UNIQUEIDENTIFIER NULL,
    [UpdatedDate]             DATETIME         NULL,
    [Status]                  BIT              NOT NULL,
    CONSTRAINT [PK_LookupRulesSetGroup] PRIMARY KEY CLUSTERED ([RuleSetGroupId] ASC)
);

