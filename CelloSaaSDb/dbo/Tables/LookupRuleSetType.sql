﻿CREATE TABLE [dbo].[LookupRuleSetType] (
    [RuleSetTypeId]          UNIQUEIDENTIFIER CONSTRAINT [DF_RuleType_RuleTypeId] DEFAULT (newsequentialid()) NOT NULL,
    [RuleSetTypeName]        NVARCHAR (255)   NOT NULL,
    [RuleSetTypeCode]        NVARCHAR (255)   NOT NULL,
    [RuleSetTypeDescription] NVARCHAR (MAX)   NULL,
    [CreatedBy]              UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]            DATETIME         NOT NULL,
    [UpdatedBy]              UNIQUEIDENTIFIER NULL,
    [UpdatedDate]            DATETIME         NULL,
    [Status]                 BIT              NOT NULL,
    [ScreenType]             VARCHAR (50)     NULL,
    CONSTRAINT [PK_LookupRuleSetType] PRIMARY KEY CLUSTERED ([RuleSetTypeId] ASC)
);

