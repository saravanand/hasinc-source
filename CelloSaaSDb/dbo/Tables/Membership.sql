﻿CREATE TABLE [dbo].[Membership] (
    [Membership_ID]                                     UNIQUEIDENTIFIER CONSTRAINT [DF_Membership_Membership_ID] DEFAULT (newsequentialid()) NOT NULL,
    [Membership_TenantID]                               UNIQUEIDENTIFIER NOT NULL,
    [Membership_UserName]                               NVARCHAR (255)   NOT NULL,
    [Membership_EmailID]                                VARCHAR (255)    NULL,
    [Membership_Comment]                                VARCHAR (255)    NULL,
    [Membership_Password]                               NVARCHAR (255)   NOT NULL,
    [Membership_PasswordQuestion]                       NVARCHAR (255)   NULL,
    [Membership_PasswordAnswer]                         NVARCHAR (255)   NULL,
    [Membership_IsApproved]                             BIT              NULL,
    [Membership_LastActivityDate]                       DATETIME         NULL,
    [Membership_LastLoginDate]                          DATETIME         NULL,
    [Membership_LastPasswordChangedDate]                DATETIME         NULL,
    [Membership_CreationDate]                           DATETIME         NULL,
    [Membership_IsLockedOut]                            BIT              NULL,
    [Membership_LastLockedOutDate]                      DATETIME         NULL,
    [Membership_FailedPasswordAttemptCount]             INT              NULL,
    [Membership_FailedPasswordAttemptWindowStart]       DATETIME         NULL,
    [Membership_FailedPasswordAnswerAttemptCount]       INT              NULL,
    [Membership_FailedPasswordAnswerAttemptWindowStart] DATETIME         NULL,
    [Membership_Status]                                 BIT              NOT NULL,
    [Membership_IsFirstTimeUser]                        BIT              DEFAULT ((1)) NOT NULL,
    [Membership_IsUserPasswordChangeForced]             BIT              CONSTRAINT [DF_Membership_Membership_IsPasswordForced] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Membership] PRIMARY KEY CLUSTERED ([Membership_ID] ASC),
    CONSTRAINT [FK_Membership_TenantDetails] FOREIGN KEY ([Membership_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Membership_EmailID]
    ON [dbo].[Membership]([Membership_EmailID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_Membership_TenantID]
    ON [dbo].[Membership]([Membership_TenantID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_Membership_UserName]
    ON [dbo].[Membership]([Membership_UserName] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-Status]
    ON [dbo].[Membership]([Membership_Status] ASC)
    INCLUDE([Membership_TenantID]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-LastActivityDate]
    ON [dbo].[Membership]([Membership_LastActivityDate] ASC);

