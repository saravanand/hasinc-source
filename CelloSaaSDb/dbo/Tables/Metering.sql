﻿CREATE TABLE [dbo].[Metering] (
    [Metering_ID]          UNIQUEIDENTIFIER CONSTRAINT [DF_Metering_Metering_ID] DEFAULT (newsequentialid()) NOT NULL,
    [Metering_TenantID]    UNIQUEIDENTIFIER NOT NULL,
    [Metering_UsageCode]   NVARCHAR (255)   NOT NULL,
    [Metering_UsageAmount] DECIMAL (18)     NOT NULL,
    [Metering_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [Metering_CreatedOn]   DATETIME         NOT NULL,
    [Metering_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Metering_UpdatedOn]   DATETIME         NULL,
    [Metering_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_Metering] PRIMARY KEY CLUSTERED ([Metering_ID] ASC),
    CONSTRAINT [FK_Metering_TenantDetails] FOREIGN KEY ([Metering_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Metering_TenantID]
    ON [dbo].[Metering]([Metering_TenantID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_Metering_UsageCode]
    ON [dbo].[Metering]([Metering_UsageCode] ASC);

