﻿CREATE TABLE [dbo].[MeteringLog] (
    [MeteringLog_ID]         UNIQUEIDENTIFIER CONSTRAINT [DF_MeteringLog_MeteringLog_ID] DEFAULT (newsequentialid()) NOT NULL,
    [MeteringLog_UsageCode]  NVARCHAR (255)   NOT NULL,
    [MeteringLog_Amount]     DECIMAL (18)     NOT NULL,
    [MeteringLog_TenantID]   UNIQUEIDENTIFIER NULL,
    [MeteringLog_LoggedBy]   UNIQUEIDENTIFIER NOT NULL,
    [MeteringLog_LoggedDate] DATETIME         NOT NULL,
    CONSTRAINT [PK_MeteringLog] PRIMARY KEY CLUSTERED ([MeteringLog_ID] ASC),
    CONSTRAINT [FK_MeteringLog_TenantDetails] FOREIGN KEY ([MeteringLog_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_MeteringLog_TenantID]
    ON [dbo].[MeteringLog]([MeteringLog_TenantID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_MeteringLog_UsageCode]
    ON [dbo].[MeteringLog]([MeteringLog_UsageCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MeteringLog_Date]
    ON [dbo].[MeteringLog]([MeteringLog_LoggedDate] ASC);

