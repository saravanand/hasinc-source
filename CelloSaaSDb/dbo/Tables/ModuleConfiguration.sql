﻿CREATE TABLE [dbo].[ModuleConfiguration] (
    [ModuleConfiguration_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_SettingConfiguration_SettingConfiguration_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ModuleConfiguration_Name]        NVARCHAR (150)   NOT NULL,
    [ModuleConfiguration_Description] NVARCHAR (MAX)   NULL,
    [ModuleConfiguration_CreatedOn]   DATETIME         NOT NULL,
    [ModuleConfiguration_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ModuleConfiguration_UpdatedOn]   DATETIME         NULL,
    [ModuleConfiguration_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [ModuleConfiguration_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_SettingConfiguration] PRIMARY KEY CLUSTERED ([ModuleConfiguration_Id] ASC)
);

