﻿CREATE TABLE [dbo].[ModuleConfigurationSetting] (
    [ModuleConfigurationSetting_Id]              UNIQUEIDENTIFIER CONSTRAINT [DF_ModuleConfigurationSetting_ModuleConfigurationSetting_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ModuleConfigurationSetting_TenantId]        UNIQUEIDENTIFIER NULL,
    [ModuleConfigurationSetting_ConfigurationId] UNIQUEIDENTIFIER CONSTRAINT [DF_ModuleConfigurationSetting_ModuleConfigurationSetting_ConfigurationId] DEFAULT (newsequentialid()) NOT NULL,
    [ModuleConfigurationSetting_Value]           NVARCHAR (255)   NOT NULL,
    [ModuleConfigurationSetting_CreatedOn]       DATETIME         NOT NULL,
    [ModuleConfigurationSetting_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [ModuleConfigurationSetting_UpdatedOn]       DATETIME         NULL,
    [ModuleConfigurationSetting_UpdatedBy]       UNIQUEIDENTIFIER NULL,
    [ModuleConfigurationSetting_Status]          BIT              NOT NULL,
    CONSTRAINT [PK_ModuleConfigurationSetting] PRIMARY KEY CLUSTERED ([ModuleConfigurationSetting_Id] ASC),
    CONSTRAINT [FK_ModuleConfigurationSetting_ModuleConfigurationSetting] FOREIGN KEY ([ModuleConfigurationSetting_Id]) REFERENCES [dbo].[ModuleConfigurationSetting] ([ModuleConfigurationSetting_Id])
);

