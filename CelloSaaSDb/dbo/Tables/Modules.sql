﻿CREATE TABLE [dbo].[Modules] (
    [Module_Code]        NVARCHAR (255)   NOT NULL,
    [Module_Name]        NVARCHAR (255)   NOT NULL,
    [Module_ServiceCode] NVARCHAR (255)   NULL,
    [Module_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [Module_CreatedOn]   DATETIME         NOT NULL,
    [Module_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Module_UpdatedOn]   DATETIME         NULL,
    [Module_Status]      BIT              NOT NULL,
    CONSTRAINT [PK__Modules__EB27D4327F60ED59] PRIMARY KEY CLUSTERED ([Module_Code] ASC),
    CONSTRAINT [FK_Modules_Services] FOREIGN KEY ([Module_ServiceCode]) REFERENCES [dbo].[Services] ([Service_Code])
);

