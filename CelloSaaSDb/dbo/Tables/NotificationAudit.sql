﻿CREATE TABLE [dbo].[NotificationAudit] (
    [NotificationAudit_Id]                       UNIQUEIDENTIFIER CONSTRAINT [DF_NotificationAudit_NotificationAudit_Id] DEFAULT (newsequentialid()) NOT NULL,
    [NotificationAudit_NotificationContentType]  VARCHAR (50)     NOT NULL,
    [NotificationAudit_NotificationDispatchType] VARCHAR (50)     NOT NULL,
    [NotificationAudit_NotificationStatus]       VARCHAR (50)     NOT NULL,
    [NotificationAudit_NotificationId]           UNIQUEIDENTIFIER NOT NULL,
    [NotificationAudit_NotificationBatchId]      UNIQUEIDENTIFIER NULL,
    [NotificationAudit_NotificationBatchCount]   INT              NULL,
    [NotificationAudit_CreatedBy]                UNIQUEIDENTIFIER NOT NULL,
    [NotificationAudit_CreatedOn]                DATETIME         NOT NULL,
    [NotificationAudit_UpdatedBy]                UNIQUEIDENTIFIER NULL,
    [NotificationAudit_UpdatedOn]                DATETIME         NULL,
    [NotificationAudit_Status]                   BIT              NOT NULL,
    [NotificationAudit_TenantId]                 UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_NotificationAudit] PRIMARY KEY CLUSTERED ([NotificationAudit_Id] ASC),
    CONSTRAINT [FK_NotificationAudit_NotificationBatch] FOREIGN KEY ([NotificationAudit_NotificationBatchId]) REFERENCES [dbo].[NotificationBatch] ([NotificationBatch_Id]),
    CONSTRAINT [FK_NotificationAudit_NotificationDetails] FOREIGN KEY ([NotificationAudit_NotificationId]) REFERENCES [dbo].[NotificationDetails] ([NotificationDetails_Id])
);

