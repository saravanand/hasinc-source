﻿CREATE TABLE [dbo].[NotificationAuditFailure] (
    [NotificationAuditFailure_Id]                   UNIQUEIDENTIFIER CONSTRAINT [DF_NotificationAuditFailure_NotificationAuditFailure_Id] DEFAULT (newsequentialid()) NOT NULL,
    [NotificationAuditFailure_NotificationAuditId]  UNIQUEIDENTIFIER NOT NULL,
    [NotificationAuditFailure_NotificationContent]  NVARCHAR (MAX)   NULL,
    [NotificationAuditFailure_NotificationDispatch] NVARCHAR (MAX)   NULL,
    [NotificationAuditFailure_CreatedBy]            UNIQUEIDENTIFIER NOT NULL,
    [NotificationAuditFailure_CreatedOn]            DATETIME         NOT NULL,
    [NotificationAuditFailure_UpdatedBy]            UNIQUEIDENTIFIER NOT NULL,
    [NotificationAuditFailure_UpdatedOn]            DATETIME         NOT NULL,
    [NotificationAuditFailure_Status]               BIT              NOT NULL,
    [NotificationAuditFailure_TrackId]              NVARCHAR (255)   NULL,
    CONSTRAINT [PK_NotificationAuditFailure] PRIMARY KEY CLUSTERED ([NotificationAuditFailure_Id] ASC),
    CONSTRAINT [FK_NotificationAuditFailure_NotificationAudit] FOREIGN KEY ([NotificationAuditFailure_NotificationAuditId]) REFERENCES [dbo].[NotificationAudit] ([NotificationAudit_Id])
);

