﻿CREATE TABLE [dbo].[NotificationBatch] (
    [NotificationBatch_Id]                       UNIQUEIDENTIFIER CONSTRAINT [DF_NotificationBatch_NotificationBatch_Id] DEFAULT (newsequentialid()) NOT NULL,
    [NotificationBatch_NotificationContentType]  VARCHAR (50)     NOT NULL,
    [NotificationBatch_NotificationDispatchType] VARCHAR (50)     NOT NULL,
    [NotificationBatch_Comments]                 NVARCHAR (MAX)   NULL,
    [NotificationBatch_Priority]                 VARCHAR (50)     NOT NULL,
    [NotificationBatch_Combine]                  BIT              NULL,
    [NotificationBatch_DispatchedStatus]         VARCHAR (50)     NULL,
    [NotificationBatch_CreatedBy]                UNIQUEIDENTIFIER NOT NULL,
    [NotificationBatch_CreatedOn]                DATETIME         NOT NULL,
    [NotificationBatch_UpdatedBy]                UNIQUEIDENTIFIER NULL,
    [NotificationBatch_UpdatedOn]                DATETIME         NULL,
    [NotificationBatch_Status]                   BIT              NOT NULL,
    CONSTRAINT [PK_NotificationBatch] PRIMARY KEY CLUSTERED ([NotificationBatch_Id] ASC)
);

