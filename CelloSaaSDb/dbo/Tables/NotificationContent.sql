﻿CREATE TABLE [dbo].[NotificationContent] (
    [NotificationContent_Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF_NotificationContent_NotificationContent_Id] DEFAULT (newsequentialid()) NOT NULL,
    [NotificationContent_TemplateId]          UNIQUEIDENTIFIER NULL,
    [NotificationContent_NotificationAuditId] UNIQUEIDENTIFIER NULL,
    [NotificationContent_CreatedOn]           DATETIME         NOT NULL,
    [NotificationContent_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [NotificationContent_UpdatedOn]           DATETIME         NULL,
    [NotificationContent_UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [NotificationContent_Status]              BIT              NOT NULL,
    CONSTRAINT [PK_NotificationContent] PRIMARY KEY CLUSTERED ([NotificationContent_Id] ASC),
    CONSTRAINT [FK_NotificationContent_NotificationAudit] FOREIGN KEY ([NotificationContent_NotificationAuditId]) REFERENCES [dbo].[NotificationAudit] ([NotificationAudit_Id])
);

