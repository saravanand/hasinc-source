﻿CREATE TABLE [dbo].[NotificationContentDetails] (
    [NotificationContentDetails_Id]   UNIQUEIDENTIFIER NOT NULL,
    [NotificationContentDetails_Type] VARCHAR (50)     NOT NULL,
    CONSTRAINT [PK_NotificationContentDetails] PRIMARY KEY CLUSTERED ([NotificationContentDetails_Id] ASC)
);

