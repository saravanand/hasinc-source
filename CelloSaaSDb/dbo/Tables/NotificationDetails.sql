﻿CREATE TABLE [dbo].[NotificationDetails] (
    [NotificationDetails_Id]                UNIQUEIDENTIFIER NOT NULL,
    [NotificationDetails_NotificationId]    UNIQUEIDENTIFIER NOT NULL,
    [NotificationDetails_ContentDetailsId]  UNIQUEIDENTIFIER NULL,
    [NotificationDetails_DispatchDetailsId] UNIQUEIDENTIFIER NULL,
    [NotificationDetails_TenantId]          UNIQUEIDENTIFIER NULL,
    [NotificationDetails_CreatedBy]         UNIQUEIDENTIFIER NULL,
    [NotificationDetails_CreatedOn]         DATETIME         DEFAULT (getdate()) NOT NULL,
    [NotificationDetails_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [NotificationDetails_UpdatedOn]         DATETIME         NULL,
    [NotificationDetails_Status]            BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_NotificationDetails] PRIMARY KEY CLUSTERED ([NotificationDetails_Id] ASC),
    CONSTRAINT [FK_NotificationDetails_NotificationContentDetails] FOREIGN KEY ([NotificationDetails_ContentDetailsId]) REFERENCES [dbo].[NotificationContentDetails] ([NotificationContentDetails_Id]),
    CONSTRAINT [FK_NotificationDetails_NotificationDispatchDetails] FOREIGN KEY ([NotificationDetails_DispatchDetailsId]) REFERENCES [dbo].[NotificationDispatchDetails] ([NotificationDispatchDetails_Id]),
    CONSTRAINT [FK_NotificationDetails_NotificationMaster] FOREIGN KEY ([NotificationDetails_NotificationId]) REFERENCES [dbo].[NotificationMaster] ([NotificationMaster_Id])
);

