﻿CREATE TABLE [dbo].[NotificationDispatch] (
    [NotificationDispatch_Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF_NotificationDispatch_NotificationDispatch_Id] DEFAULT (newsequentialid()) NOT NULL,
    [NotificationDispatch_NotificationAuditId] UNIQUEIDENTIFIER NOT NULL,
    [NotificationDispatch_CreatedOn]           DATETIME         NOT NULL,
    [NotificationDispatch_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [NotificationDispatch_UpdatedOn]           DATETIME         NULL,
    [NotificationDispatch_UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [NotificationDispatch_Status]              BIT              NOT NULL,
    [NotificationDispatch_TrackId]             NVARCHAR (255)   NULL,
    CONSTRAINT [PK_NotificationDispatch] PRIMARY KEY CLUSTERED ([NotificationDispatch_Id] ASC),
    CONSTRAINT [FK_NotificationDispatch_NotificationAudit] FOREIGN KEY ([NotificationDispatch_NotificationAuditId]) REFERENCES [dbo].[NotificationAudit] ([NotificationAudit_Id])
);

