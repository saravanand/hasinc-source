﻿CREATE TABLE [dbo].[NotificationDispatchDetails] (
    [NotificationDispatchDetails_Id]   UNIQUEIDENTIFIER NOT NULL,
    [NotificationDispatchDetails_Type] VARCHAR (50)     NOT NULL,
    CONSTRAINT [PK_NotificationDispatchDetails] PRIMARY KEY CLUSTERED ([NotificationDispatchDetails_Id] ASC)
);

