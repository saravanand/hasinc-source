﻿CREATE TABLE [dbo].[NotificationMaster] (
    [NotificationMaster_Id]          UNIQUEIDENTIFIER NOT NULL,
    [NotificationMaster_Name]        NVARCHAR (255)   NOT NULL,
    [NotificationMaster_Description] NVARCHAR (MAX)   NULL,
    [NotificationMaster_Category]    NVARCHAR (255)   NULL,
    [NotificationMaster_CreatedBy]   UNIQUEIDENTIFIER NULL,
    [NotificationMaster_CreatedOn]   DATETIME         DEFAULT (getdate()) NOT NULL,
    [NotificationMaster_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [NotificationMaster_UpdatedOn]   DATETIME         NULL,
    [NotificationMaster_Status]      BIT              DEFAULT ((1)) NOT NULL,
    [NotificationMaster_TenantId]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_NotificationMaster] PRIMARY KEY CLUSTERED ([NotificationMaster_Id] ASC)
);

