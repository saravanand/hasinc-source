﻿CREATE TABLE [dbo].[NotificationTemplate] (
    [NotificationTemplate_Id]          UNIQUEIDENTIFIER NOT NULL,
    [NotificationTemplate_Template]    NVARCHAR (MAX)   NULL,
    [NotificationTemplate_CreatedBy]   UNIQUEIDENTIFIER NULL,
    [NotificationTemplate_CreatedOn]   DATETIME         DEFAULT (getdate()) NOT NULL,
    [NotificationTemplate_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [NotificationTemplate_UpdatedOn]   DATETIME         NULL,
    [NotificationTemplate_Status]      BIT              DEFAULT ((1)) NOT NULL,
    [NotificationTemplate_Name]        NVARCHAR (255)   NULL,
    [NotificationTemplate_Description] NVARCHAR (500)   NULL,
    CONSTRAINT [PK_NotifiactionTemplate] PRIMARY KEY CLUSTERED ([NotificationTemplate_Id] ASC)
);

