﻿CREATE TABLE [dbo].[PackageSetting] (
    [PackageSetting_PackageID]      UNIQUEIDENTIFIER NOT NULL,
    [PackageSetting_AttributeID]    NVARCHAR (255)   NOT NULL,
    [PackageSetting_AttributeValue] VARCHAR (500)    NULL,
    [PackageSetting_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [PackageSetting_CreatedOn]      DATETIME         NOT NULL,
    [PackageSetting_UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [PackageSetting_UpdatedOn]      DATETIME         NULL,
    [PackageSetting_Status]         BIT              NOT NULL,
    CONSTRAINT [PK_PackageSetting] PRIMARY KEY CLUSTERED ([PackageSetting_PackageID] ASC, [PackageSetting_AttributeID] ASC),
    CONSTRAINT [FK_PackageSetting_LicensePackage] FOREIGN KEY ([PackageSetting_PackageID]) REFERENCES [dbo].[LicensePackage] ([LicensePackage_ID]),
    CONSTRAINT [FK_PackageSetting_SettingsAttribute] FOREIGN KEY ([PackageSetting_AttributeID]) REFERENCES [dbo].[SettingsAttribute] ([SettingsAttribute_ID])
);

