﻿CREATE TABLE [dbo].[PaymentAccountSettings] (
    [TenantId]     UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [AccountType]  INT              NOT NULL,
    [StoreKey]     NVARCHAR (255)   NOT NULL,
    [Email]        NVARCHAR (255)   NULL,
    [CurrencyCode] VARCHAR (10)     NULL,
    [CreatedBy]    NVARCHAR (100)   NOT NULL,
    [CreatedOn]    DATETIME         NOT NULL,
    [UpdatedBy]    NVARCHAR (100)   NULL,
    [UpdatedOn]    DATETIME         NULL,
    [Status]       BIT              NOT NULL,
    CONSTRAINT [PK_PaymentAccountSettings] PRIMARY KEY CLUSTERED ([TenantId] ASC)
);

