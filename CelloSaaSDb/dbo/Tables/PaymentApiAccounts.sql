﻿CREATE TABLE [dbo].[PaymentApiAccounts] (
    [Id]          UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [AccountType] INT              NOT NULL,
    [UserName]    NVARCHAR (MAX)   NOT NULL,
    [Password]    NVARCHAR (MAX)   NOT NULL,
    [Signature]   NVARCHAR (MAX)   NULL,
    [Email]       NVARCHAR (255)   NULL,
    [AppId]       NVARCHAR (MAX)   NULL,
    [Mode]        BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PaymentApiAccounts] PRIMARY KEY CLUSTERED ([Id] ASC)
);

