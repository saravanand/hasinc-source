﻿CREATE TABLE [dbo].[PaymentAudits] (
    [Id]              UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [AccountType]     INT              NOT NULL,
    [InvoiceId]       UNIQUEIDENTIFIER NOT NULL,
    [InvoiceNo]       NVARCHAR (255)   NOT NULL,
    [TransactionId]   NVARCHAR (255)   NULL,
    [TransactionDate] DATETIME         NOT NULL,
    [Details]         NVARCHAR (MAX)   NULL,
    [Amount]          FLOAT (53)       NOT NULL,
    [Attempt]         INT              NOT NULL,
    [Status]          NVARCHAR (255)   NOT NULL,
    CONSTRAINT [PK_PaymentAudits] PRIMARY KEY CLUSTERED ([Id] ASC)
);

