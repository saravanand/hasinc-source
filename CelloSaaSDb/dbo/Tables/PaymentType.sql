﻿CREATE TABLE [dbo].[PaymentType] (
    [PaymentType_Code]      NVARCHAR (255)   NOT NULL,
    [PaymentType_Name]      NVARCHAR (255)   NOT NULL,
    [PaymentType_CreatedOn] DATETIME         NOT NULL,
    [PaymentType_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [PaymentType_UpdatedOn] DATETIME         NULL,
    [PaymentType_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [PaymentType_Status]    BIT              NOT NULL,
    CONSTRAINT [PK__PaymentT__0D43BC3B1B0907CE] PRIMARY KEY CLUSTERED ([PaymentType_Code] ASC)
);

