﻿CREATE TABLE [dbo].[PickupList] (
    [PickupList_ID]         UNIQUEIDENTIFIER CONSTRAINT [DF_PickupList_PickupList_ID] DEFAULT (newsequentialid()) NOT NULL,
    [PickupList_Name]       NVARCHAR (255)   NOT NULL,
    [PickupList_TenantCode] UNIQUEIDENTIFIER NULL,
    [PickupList_CreatedBy]  UNIQUEIDENTIFIER NOT NULL,
    [PickupList_CreatedOn]  DATETIME         NOT NULL,
    [PickupList_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [PickupList_UpdatedOn]  DATETIME         NULL,
    [PickupList_Status]     BIT              NOT NULL,
    [PickupList_TableName]  NVARCHAR (255)   NULL,
    CONSTRAINT [PK_PickupList] PRIMARY KEY CLUSTERED ([PickupList_ID] ASC),
    CONSTRAINT [FK_PickupList_TenantDetails] FOREIGN KEY ([PickupList_TenantCode]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupList_TenantCode]
    ON [dbo].[PickupList]([PickupList_TenantCode] ASC);

