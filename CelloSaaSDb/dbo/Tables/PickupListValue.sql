﻿CREATE TABLE [dbo].[PickupListValue] (
    [PickupListValue_ID]           UNIQUEIDENTIFIER CONSTRAINT [DF_PickupListValue_PickupListValue_ID] DEFAULT (newsequentialid()) NOT NULL,
    [PickupListValue_ItemID]       NVARCHAR (255)   NOT NULL,
    [PickupListValue_ItemName]     NVARCHAR (255)   NOT NULL,
    [PickupListValue_PickupListID] UNIQUEIDENTIFIER NOT NULL,
    [PickupListValue_TenantCode]   UNIQUEIDENTIFIER NULL,
    [PickupListValue_CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [PickupListValue_CreatedOn]    DATETIME         NOT NULL,
    [PickupListValue_UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [PickupListValue_UpdatedOn]    DATETIME         NULL,
    [PickupListValue_Status]       BIT              NOT NULL,
    [PickupListValue_Description]  NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_PickupListValue] PRIMARY KEY CLUSTERED ([PickupListValue_ID] ASC),
    CONSTRAINT [FK_PickupListValue_PickupList] FOREIGN KEY ([PickupListValue_PickupListID]) REFERENCES [dbo].[PickupList] ([PickupList_ID]),
    CONSTRAINT [FK_PickupListValue_TenantDetails] FOREIGN KEY ([PickupListValue_TenantCode]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValue_ItemID]
    ON [dbo].[PickupListValue]([PickupListValue_ItemID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValue_PickupListID]
    ON [dbo].[PickupListValue]([PickupListValue_PickupListID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValue_TenantCode]
    ON [dbo].[PickupListValue]([PickupListValue_TenantCode] ASC);

