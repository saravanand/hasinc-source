﻿CREATE TABLE [dbo].[PickupListValueMapping] (
    [PickupListValueMapping_ID]                     UNIQUEIDENTIFIER CONSTRAINT [DF_PickupListValueMapping_PickupListValueMapping_ID] DEFAULT (newsequentialid()) NOT NULL,
    [PickupListValueMapping_SourceValueID]          NVARCHAR (255)   NOT NULL,
    [PickupListValueMapping_TargetValueID]          NVARCHAR (255)   NOT NULL,
    [PickupListValueMapping_RelationshipMetaDataID] UNIQUEIDENTIFIER NOT NULL,
    [PickupListValueMapping_TenantID]               UNIQUEIDENTIFIER NULL,
    [PickupListValueMapping_CreatedOn]              DATETIME         NOT NULL,
    [PickupListValueMapping_CreatedBy]              UNIQUEIDENTIFIER NOT NULL,
    [PickupListValueMapping_UpdatedOn]              DATETIME         NULL,
    [PickupListValueMapping_UpdatedBy]              UNIQUEIDENTIFIER NULL,
    [PickupListValueMapping_Status]                 BIT              NOT NULL,
    CONSTRAINT [PK_PickupListValueMapping] PRIMARY KEY CLUSTERED ([PickupListValueMapping_ID] ASC),
    CONSTRAINT [FK_PickupListValueMapping_RelationshipMetaData] FOREIGN KEY ([PickupListValueMapping_RelationshipMetaDataID]) REFERENCES [dbo].[RelationshipMetaData] ([RelationshipMetaData_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValueMapping_RelationshipMetaDataID]
    ON [dbo].[PickupListValueMapping]([PickupListValueMapping_RelationshipMetaDataID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValueMapping_SourceValueID]
    ON [dbo].[PickupListValueMapping]([PickupListValueMapping_SourceValueID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValueMapping_TargetValueID]
    ON [dbo].[PickupListValueMapping]([PickupListValueMapping_TargetValueID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValueMapping_TenantID]
    ON [dbo].[PickupListValueMapping]([PickupListValueMapping_TenantID] ASC);

