﻿CREATE TABLE [dbo].[PickupList_Country] (
    [PickupListValue_ID]           UNIQUEIDENTIFIER NOT NULL,
    [PickupListValue_Value]        NVARCHAR (255)   NOT NULL,
    [PickupListValue_Description]  NVARCHAR (MAX)   NULL,
    [PickupListValue_PickupListID] UNIQUEIDENTIFIER NOT NULL,
    [PickupListValue_TenantCode]   UNIQUEIDENTIFIER NULL,
    [PickupListValue_CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [PickupListValue_CreatedOn]    DATETIME         NOT NULL,
    [PickupListValue_UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [PickupListValue_UpdatedOn]    DATETIME         NULL,
    [PickupListValue_Status]       BIT              NOT NULL,
    CONSTRAINT [PK_PickupList_Country] PRIMARY KEY CLUSTERED ([PickupListValue_ID] ASC),
    CONSTRAINT [FK_CountryList_PickupList] FOREIGN KEY ([PickupListValue_PickupListID]) REFERENCES [dbo].[PickupList] ([PickupList_ID]),
    CONSTRAINT [FK_CountryList_TenantDetails] FOREIGN KEY ([PickupListValue_TenantCode]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_PickupListValue_TenantCode]
    ON [dbo].[PickupList_Country]([PickupListValue_TenantCode] ASC);

