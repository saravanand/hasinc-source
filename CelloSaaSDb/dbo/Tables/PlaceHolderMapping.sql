﻿CREATE TABLE [dbo].[PlaceHolderMapping] (
    [PlaceHolderMapping_ID]           INT            NOT NULL,
    [PlaceHolderMapping_Name]         NVARCHAR (255) NOT NULL,
    [PlaceHolderMapping_EntityName]   NVARCHAR (255) NOT NULL,
    [PlaceHolderMapping_Propertyname] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_PlaceHolderMapping] PRIMARY KEY CLUSTERED ([PlaceHolderMapping_ID] ASC)
);

