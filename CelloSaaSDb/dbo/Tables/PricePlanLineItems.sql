﻿CREATE TABLE [dbo].[PricePlanLineItems] (
    [Id]           UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Name]         NVARCHAR (255)   NOT NULL,
    [Description]  NVARCHAR (MAX)   NULL,
    [PriceTableId] UNIQUEIDENTIFIER NOT NULL,
    [PricePlanId]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_PricePlanLineItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PricePlanLineItemPriceTable] FOREIGN KEY ([PriceTableId]) REFERENCES [dbo].[PriceTables] ([Id]),
    CONSTRAINT [FK_PricePlanPricePlanLineItem] FOREIGN KEY ([PricePlanId]) REFERENCES [dbo].[PricePlans] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_PricePlanLineItemPriceTable]
    ON [dbo].[PricePlanLineItems]([PriceTableId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_PricePlanPricePlanLineItem]
    ON [dbo].[PricePlanLineItems]([PricePlanId] ASC);

