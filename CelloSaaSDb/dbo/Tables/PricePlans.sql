﻿CREATE TABLE [dbo].[PricePlans] (
    [Id]              UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [PackageId]       UNIQUEIDENTIFIER NOT NULL,
    [SkipCalculation] BIT              NOT NULL,
    [Price]           FLOAT (53)       NOT NULL,
    [BillFrequency]   INT              NOT NULL,
    [CreatedBy]       NVARCHAR (100)   NOT NULL,
    [CreatedOn]       DATETIME         NOT NULL,
    [UpdatedBy]       NVARCHAR (100)   NULL,
    [UpdatedOn]       DATETIME         NULL,
    [Status]          BIT              NOT NULL,
    [Name]            NVARCHAR (255)   NOT NULL,
    [Description]     NVARCHAR (MAX)   NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_PricePlans] PRIMARY KEY CLUSTERED ([Id] ASC)
);

