﻿CREATE TABLE [dbo].[PriceSlabs] (
    [Id]           UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [PriceTableId] UNIQUEIDENTIFIER NOT NULL,
    [StartValue]   FLOAT (53)       NOT NULL,
    [EndValue]     FLOAT (53)       NULL,
    [Factor]       FLOAT (53)       NOT NULL,
    CONSTRAINT [PK_PriceSlabs] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PriceTablePriceSlab] FOREIGN KEY ([PriceTableId]) REFERENCES [dbo].[PriceTables] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_PriceTablePriceSlab]
    ON [dbo].[PriceSlabs]([PriceTableId] ASC);

