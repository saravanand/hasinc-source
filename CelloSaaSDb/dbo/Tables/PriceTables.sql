﻿CREATE TABLE [dbo].[PriceTables] (
    [Id]                   UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [SlabMeterVariable]    NVARCHAR (255)   NOT NULL,
    [ActedOnMeterVariable] NVARCHAR (255)   NULL,
    [FactorType]           INT              DEFAULT ((0)) NOT NULL,
    [CalculationType]      VARCHAR (255)    NOT NULL,
    CONSTRAINT [PK_PriceTables] PRIMARY KEY CLUSTERED ([Id] ASC)
);

