﻿CREATE TABLE [dbo].[Privileges] (
    [Privilege_ID]            NVARCHAR (255)   NOT NULL,
    [Privilege_Name]          NVARCHAR (255)   NULL,
    [Privilege_Description]   NVARCHAR (MAX)   NULL,
    [Privilege_CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [Privilege_CreatedOn]     DATETIME         NOT NULL,
    [Privilege_UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [Privilege_UpdatedOn]     DATETIME         NULL,
    [Privilege_Status]        BIT              NOT NULL,
    [Privilege_BasePrivilege] NVARCHAR (255)   NULL,
    [Privilege_TenantId]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__Privileg__645E08121D7B6025] PRIMARY KEY CLUSTERED ([Privilege_ID] ASC),
    CONSTRAINT [FK_Privileges_Privileges] FOREIGN KEY ([Privilege_BasePrivilege]) REFERENCES [dbo].[Privileges] ([Privilege_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Privilege_BasePrivilege]
    ON [dbo].[Privileges]([Privilege_BasePrivilege] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_Privilege_Name]
    ON [dbo].[Privileges]([Privilege_Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Privileges_TenantId]
    ON [dbo].[Privileges]([Privilege_TenantId] ASC);

