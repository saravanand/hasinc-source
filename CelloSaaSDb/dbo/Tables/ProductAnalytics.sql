﻿CREATE TABLE [dbo].[ProductAnalytics] (
    [ProductAnalytics_Id]                  UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [ProductAnalytics_UserId]              UNIQUEIDENTIFIER NOT NULL,
    [ProductAnalytics_TenantId]            UNIQUEIDENTIFIER NOT NULL,
    [ProductAnalytics_Controller]          VARCHAR (100)    NOT NULL,
    [ProductAnalytics_Action]              VARCHAR (200)    NOT NULL,
    [ProductAnalytics_TimeStamp]           DATETIME         NOT NULL,
    [ProductAnalytics_BrowserData]         NVARCHAR (MAX)   NULL,
    [ProductAnalytics_MachineName]         VARCHAR (50)     NULL,
    [ProductAnalytics_RequestMethod]       VARCHAR (10)     NULL,
    [ProductAnalytics_RequestMode]         VARCHAR (50)     NULL,
    [ProductAnalytics_Exception]           NVARCHAR (MAX)   NULL,
    [ProductAnalytics_Url]                 NVARCHAR (MAX)   NOT NULL,
    [ProductAnalytics_ResponseCode]        INT              NULL,
    [ProductAnalytics_ResponseDescription] VARCHAR (50)     NULL,
    [ProductAnalytics_CreatedOn]           DATETIME         NOT NULL,
    [ProductAnalytics_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [ProductAnalytics_Status]              BIT              NOT NULL,
    [ProductAnalytics_UserIP]              VARCHAR (30)     NOT NULL,
    [ProductAnalytics_SessionID]           VARCHAR (255)    NULL,
    [ProductAnalytics_ResponseLength]      BIGINT           CONSTRAINT [DF_ProductAnalytics_ProductAnalytics_ResponseLength] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__ProductA__C969771745544755] PRIMARY KEY NONCLUSTERED ([ProductAnalytics_Id] ASC) WITH (FILLFACTOR = 100)
);


GO
CREATE NONCLUSTERED INDEX [Ind_ProductAnalytics_TenantId]
    ON [dbo].[ProductAnalytics]([ProductAnalytics_TenantId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [Ind_ProductAnalytics_UserId]
    ON [dbo].[ProductAnalytics]([ProductAnalytics_UserId] ASC) WITH (FILLFACTOR = 100);

