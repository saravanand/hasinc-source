﻿CREATE TABLE [dbo].[ProductSettings] (
    [Key]   VARCHAR (100)  NOT NULL,
    [Value] NVARCHAR (200) NULL,
    CONSTRAINT [PK_ProductSettings] PRIMARY KEY CLUSTERED ([Key] ASC)
);

