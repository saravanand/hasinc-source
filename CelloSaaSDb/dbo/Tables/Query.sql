﻿CREATE TABLE [dbo].[Query] (
    [Query_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_Query_QueryId] DEFAULT (newsequentialid()) NOT NULL,
    [Query_Name]        NVARCHAR (255)   NOT NULL,
    [Query_Description] NVARCHAR (MAX)   NULL,
    [Query_XmlQuery]    XML              NULL,
    [Query_TenantId]    UNIQUEIDENTIFIER NULL,
    [Query_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [Query_CreatedOn]   DATETIME         NOT NULL,
    [Query_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Query_UpdatedOn]   DATETIME         NULL,
    [Query_Status]      BIT              NOT NULL,
    [Query_Type]        INT              NULL,
    [Query_RenderMode]  VARCHAR (10)     NULL,
    CONSTRAINT [PK_Query] PRIMARY KEY CLUSTERED ([Query_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [FK_Query_QueryId]
    ON [dbo].[Query]([Query_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_Query_QueryName]
    ON [dbo].[Query]([Query_Name] ASC);

