﻿CREATE TABLE [dbo].[QueryRelations] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_QueryRelations_Id] DEFAULT (newsequentialid()) NOT NULL,
    [Name]             NVARCHAR (500)   NOT NULL,
    [Description]      NVARCHAR (MAX)   NULL,
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    [ParentQueryId]    UNIQUEIDENTIFIER NOT NULL,
    [RelatedQueryId]   UNIQUEIDENTIFIER NOT NULL,
    [VariableMappings] XML              NULL,
    [RelationType]     VARCHAR (10)     NOT NULL,
    [RenderMode]       VARCHAR (10)     NOT NULL,
    [RenderArea]       VARCHAR (10)     NOT NULL,
    CONSTRAINT [PK_QueryRelations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

