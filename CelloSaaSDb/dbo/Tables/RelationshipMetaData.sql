﻿CREATE TABLE [dbo].[RelationshipMetaData] (
    [RelationshipMetaData_ID]                UNIQUEIDENTIFIER CONSTRAINT [DF_RelationshipMetaData_RelationshipMetaData_ID] DEFAULT (newsequentialid()) NOT NULL,
    [RelationshipMetaData_SourceID]          VARCHAR (50)     NOT NULL,
    [RelationshipMetaData_TargetID]          VARCHAR (50)     NOT NULL,
    [RelationshipMetaData_TargetKeyName]     VARCHAR (100)    NULL,
    [RelationshipMetaData_TargetDisplayName] VARCHAR (100)    NULL,
    [RelationshipMetaData_RelationType]      VARCHAR (50)     NOT NULL,
    [RelationshipMetaData_TargetType]        VARCHAR (50)     NOT NULL,
    [RelationshipMetaData_TenantID]          UNIQUEIDENTIFIER NULL,
    [RelationshipMetaData_CreatedOn]         DATETIME         NOT NULL,
    [RelationshipMetaData_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [RelationshipMetaData_UpdatedOn]         DATETIME         NULL,
    [RelationshipMetaData_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [RelationshipMetaData_Status]            BIT              NOT NULL,
    CONSTRAINT [PK_RelationshipMetaData] PRIMARY KEY CLUSTERED ([RelationshipMetaData_ID] ASC),
    CONSTRAINT [FK_RelationshipMetaData_TenantDetails] FOREIGN KEY ([RelationshipMetaData_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_RelationshipMetaData_SourceID]
    ON [dbo].[RelationshipMetaData]([RelationshipMetaData_SourceID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_RelationshipMetaData_TargetID]
    ON [dbo].[RelationshipMetaData]([RelationshipMetaData_TargetID] ASC);

