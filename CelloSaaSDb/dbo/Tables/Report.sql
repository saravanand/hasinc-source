﻿CREATE TABLE [dbo].[Report] (
    [Report_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_Report_Report_Id] DEFAULT (newsequentialid()) NOT NULL,
    [Report_TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [Report_Name]        NVARCHAR (255)   NOT NULL,
    [Report_Description] NVARCHAR (MAX)   NULL,
    [Report_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [Report_CreatedOn]   DATETIME         NOT NULL,
    [Report_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Report_UpdatedOn]   DATETIME         NULL,
    [Report_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED ([Report_Id] ASC),
    CONSTRAINT [FK_Report_Report] FOREIGN KEY ([Report_Id]) REFERENCES [dbo].[Report] ([Report_Id]),
    CONSTRAINT [FK_Report_TenantDetails] FOREIGN KEY ([Report_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

