﻿CREATE TABLE [dbo].[ReportNotificationContent] (
    [ReportNotificationContent_NotificationContentId] UNIQUEIDENTIFIER NOT NULL,
    [ReportNotificationContent_ReportId]              UNIQUEIDENTIFIER NULL,
    [ReportNotificationContent_StoredReportPath]      VARCHAR (100)    NULL,
    [ReportNotificationContent_CreatedOn]             DATETIME         NOT NULL,
    [ReportNotificationContent_CreatedBy]             UNIQUEIDENTIFIER NOT NULL,
    [ReportNotificationContent_UpdatedOn]             DATETIME         NULL,
    [ReportNotificationContent_UpdatedBy]             UNIQUEIDENTIFIER NULL,
    [ReportNotificationContent_Status]                BIT              NOT NULL,
    CONSTRAINT [PK_ReportNotificationContent] PRIMARY KEY CLUSTERED ([ReportNotificationContent_NotificationContentId] ASC),
    CONSTRAINT [FK_ReportNotificationContent_NotificationContent] FOREIGN KEY ([ReportNotificationContent_NotificationContentId]) REFERENCES [dbo].[NotificationContent] ([NotificationContent_Id])
);

