﻿CREATE TABLE [dbo].[ReportObject] (
    [ReportObject_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_ReportObject_ReportObject_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ReportObject_TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [ReportObject_ReportId]    UNIQUEIDENTIFIER NOT NULL,
    [ReportObject_SourceId]    UNIQUEIDENTIFIER NULL,
    [ReportObject_Ordinal]     INT              NOT NULL,
    [ReportObject_TypeId]      UNIQUEIDENTIFIER NOT NULL,
    [ReportObject_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [ReportObject_CreatedOn]   DATETIME         NOT NULL,
    [ReportObject_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [ReportObject_UpdatedOn]   DATETIME         NULL,
    [ReportObject_Status]      BIT              NOT NULL,
    [ReportObject_RowsPerPage] INT              NULL,
    [ReportObject_Name]        NVARCHAR (255)   NULL,
    CONSTRAINT [PK_ReportObject] PRIMARY KEY CLUSTERED ([ReportObject_Id] ASC),
    CONSTRAINT [FK_ReportObject_Report] FOREIGN KEY ([ReportObject_TypeId]) REFERENCES [dbo].[ReportObjectType] ([ReportObjectType_Id]),
    CONSTRAINT [FK_ReportObject_TenantDetails] FOREIGN KEY ([ReportObject_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [IX_ReportObject_ReportId]
    ON [dbo].[ReportObject]([ReportObject_Id] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ReportObject_Sources]
    ON [dbo].[ReportObject]([ReportObject_Id] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ReportObject_TenantId]
    ON [dbo].[ReportObject]([ReportObject_Id] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_ReportObject_Types]
    ON [dbo].[ReportObject]([ReportObject_Id] ASC) WITH (FILLFACTOR = 100);

