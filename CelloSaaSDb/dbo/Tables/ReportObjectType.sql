﻿CREATE TABLE [dbo].[ReportObjectType] (
    [ReportObjectType_Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_ReportObjectType_ReportObjectType_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ReportObjectType_Name]      NVARCHAR (255)   NOT NULL,
    [ReportObjectType_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [ReportObjectType_CreatedOn] DATETIME         NOT NULL,
    [ReportObjectType_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [ReportObjectType_UpdatedOn] DATETIME         NULL,
    [ReportObjectType_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_ReportObjectType] PRIMARY KEY CLUSTERED ([ReportObjectType_Id] ASC)
);

