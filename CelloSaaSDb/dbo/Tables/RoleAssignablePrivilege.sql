﻿CREATE TABLE [dbo].[RoleAssignablePrivilege] (
    [RoleAssignablePrivilege_Id]          UNIQUEIDENTIFIER NOT NULL,
    [RoleAssignablePrivilege_RoleId]      NVARCHAR (255)   NOT NULL,
    [RoleAssignablePrivilege_PrivilegeId] NVARCHAR (255)   NOT NULL,
    [RoleAssignablePrivilege_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [RoleAssignablePrivilege_CreatedOn]   DATETIME         NOT NULL,
    [RoleAssignablePrivilege_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [RoleAssignablePrivilege_UpdatedOn]   DATETIME         NULL,
    [RoleAssignablePrivilege_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_RoleAssignablePrivilege] PRIMARY KEY CLUSTERED ([RoleAssignablePrivilege_Id] ASC),
    CONSTRAINT [FK_RoleAssignablePrivilege_Privileges] FOREIGN KEY ([RoleAssignablePrivilege_PrivilegeId]) REFERENCES [dbo].[Privileges] ([Privilege_ID]),
    CONSTRAINT [FK_RoleAssignablePrivilege_Roles] FOREIGN KEY ([RoleAssignablePrivilege_RoleId]) REFERENCES [dbo].[Roles] ([Role_ID])
);

