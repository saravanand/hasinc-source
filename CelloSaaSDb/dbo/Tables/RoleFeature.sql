﻿CREATE TABLE [dbo].[RoleFeature] (
    [RoleFeature_ID]          UNIQUEIDENTIFIER CONSTRAINT [DF_RoleFeature_RoleFeature_ID] DEFAULT (newsequentialid()) NOT NULL,
    [RoleFeature_TenantID]    UNIQUEIDENTIFIER NULL,
    [RoleFeature_RoleID]      NVARCHAR (255)   NOT NULL,
    [RoleFeature_FeatureID]   NVARCHAR (255)   NOT NULL,
    [RoleFeature_DataScopeID] UNIQUEIDENTIFIER NULL,
    [RoleFeature_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [RoleFeature_CreatedOn]   DATETIME         NOT NULL,
    [RoleFeature_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [RoleFeature_UpdatedOn]   DATETIME         NULL,
    [RoleFeature_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_RoleFeature] PRIMARY KEY CLUSTERED ([RoleFeature_ID] ASC),
    CONSTRAINT [FK_RoleFeature_DataScope] FOREIGN KEY ([RoleFeature_DataScopeID]) REFERENCES [dbo].[DataScope] ([DataScope_ID]),
    CONSTRAINT [FK_RoleFeature_Features] FOREIGN KEY ([RoleFeature_FeatureID]) REFERENCES [dbo].[Features] ([Feature_Code]),
    CONSTRAINT [FK_RoleFeature_Roles] FOREIGN KEY ([RoleFeature_RoleID]) REFERENCES [dbo].[Roles] ([Role_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_RoleFeature_DataScopeID]
    ON [dbo].[RoleFeature]([RoleFeature_DataScopeID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_RoleFeature_FeatureID]
    ON [dbo].[RoleFeature]([RoleFeature_FeatureID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_RoleFeature_RoleID]
    ON [dbo].[RoleFeature]([RoleFeature_RoleID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_RoleFeature_TenantID]
    ON [dbo].[RoleFeature]([RoleFeature_TenantID] ASC);

