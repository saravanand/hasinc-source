﻿CREATE TABLE [dbo].[RolePrivileges] (
    [RolePrivilege_ID]          UNIQUEIDENTIFIER CONSTRAINT [DF_RolePrivileges_RolePrivilege_ID] DEFAULT (newsequentialid()) NOT NULL,
    [RolePrivilege_TenantID]    UNIQUEIDENTIFIER NULL,
    [RolePrivilege_RoleID]      NVARCHAR (255)   NOT NULL,
    [RolePrivilege_PrivilegeID] NVARCHAR (200)   NOT NULL,
    [RolePrivilege_DataScopeID] UNIQUEIDENTIFIER NULL,
    [RolePrivilege_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [RolePrivilege_CreatedOn]   DATETIME         NOT NULL,
    [RolePrivilege_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [RolePrivilege_UpdatedOn]   DATETIME         NULL,
    [RolePrivilege_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_RolePrivileges] PRIMARY KEY CLUSTERED ([RolePrivilege_ID] ASC),
    CONSTRAINT [FK_RolePrivileges_DataScope] FOREIGN KEY ([RolePrivilege_DataScopeID]) REFERENCES [dbo].[DataScope] ([DataScope_ID]),
    CONSTRAINT [FK_RolePrivileges_Roles] FOREIGN KEY ([RolePrivilege_RoleID]) REFERENCES [dbo].[Roles] ([Role_ID]),
    CONSTRAINT [FK_RolePrivileges_TenantDetails] FOREIGN KEY ([RolePrivilege_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_RolePrivilege_DataScopeID]
    ON [dbo].[RolePrivileges]([RolePrivilege_DataScopeID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_RolePrivilege_PrivilegeID]
    ON [dbo].[RolePrivileges]([RolePrivilege_PrivilegeID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_RolePrivilege_RoleID]
    ON [dbo].[RolePrivileges]([RolePrivilege_RoleID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_RolePrivilege_TenantID]
    ON [dbo].[RolePrivileges]([RolePrivilege_TenantID] ASC);

