﻿CREATE TABLE [dbo].[RoleService] (
    [RoleService_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_RoleService_RoleService_Id] DEFAULT (newsequentialid()) NOT NULL,
    [RoleService_RoleId]      NVARCHAR (255)   NOT NULL,
    [RoleService_ServiceCode] NVARCHAR (255)   NOT NULL,
    [RoleService_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [RoleService_CreatedOn]   DATETIME         NOT NULL,
    [RoleService_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [RoleService_UpdatedOn]   DATETIME         NULL,
    [RoleService_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_RoleService] PRIMARY KEY CLUSTERED ([RoleService_Id] ASC),
    CONSTRAINT [FK_RoleService_Roles] FOREIGN KEY ([RoleService_RoleId]) REFERENCES [dbo].[Roles] ([Role_ID]),
    CONSTRAINT [FK_RoleService_Services] FOREIGN KEY ([RoleService_ServiceCode]) REFERENCES [dbo].[Services] ([Service_Code])
);

