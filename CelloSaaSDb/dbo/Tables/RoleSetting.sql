﻿CREATE TABLE [dbo].[RoleSetting] (
    [RoleSetting_RoleID]         NVARCHAR (255)   NOT NULL,
    [RoleSetting_AttributeID]    NVARCHAR (255)   NOT NULL,
    [RoleSetting_AttributeValue] NVARCHAR (255)   NULL,
    [RoleSetting_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [RoleSetting_CreatedOn]      DATETIME         NOT NULL,
    [RoleSetting_UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [RoleSetting_UpdatedOn]      DATETIME         NULL,
    [RoleSetting_Status]         BIT              NOT NULL,
    CONSTRAINT [PK_RoleSetting] PRIMARY KEY CLUSTERED ([RoleSetting_RoleID] ASC),
    CONSTRAINT [FK_RoleSetting_Roles] FOREIGN KEY ([RoleSetting_RoleID]) REFERENCES [dbo].[Roles] ([Role_ID]),
    CONSTRAINT [FK_RoleSetting_SettingsAttribute] FOREIGN KEY ([RoleSetting_AttributeID]) REFERENCES [dbo].[SettingsAttribute] ([SettingsAttribute_ID])
);

