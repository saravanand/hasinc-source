﻿CREATE TABLE [dbo].[RoleTenant] (
    [RoleTenant_Id]         UNIQUEIDENTIFIER CONSTRAINT [DF_RoleTenant_RoleTenant_Id] DEFAULT (newsequentialid()) NOT NULL,
    [RoleTenant_RoleId]     NVARCHAR (255)   NOT NULL,
    [RoleTenant_TenantCode] UNIQUEIDENTIFIER NOT NULL,
    [RoleTenant_CreatedBy]  UNIQUEIDENTIFIER NOT NULL,
    [RoleTenant_CreatedOn]  DATETIME         NOT NULL,
    [RoleTenant_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [RoleTenant_UpdatedOn]  DATETIME         NULL,
    [RoleTenant_Status]     BIT              NOT NULL,
    CONSTRAINT [PK_RoleTenant] PRIMARY KEY CLUSTERED ([RoleTenant_Id] ASC),
    CONSTRAINT [FK_RoleTenant_Roles] FOREIGN KEY ([RoleTenant_RoleId]) REFERENCES [dbo].[Roles] ([Role_ID]),
    CONSTRAINT [FK_RoleTenant_TenantDetails] FOREIGN KEY ([RoleTenant_TenantCode]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

