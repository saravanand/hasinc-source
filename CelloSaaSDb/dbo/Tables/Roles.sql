﻿CREATE TABLE [dbo].[Roles] (
    [Role_ID]          NVARCHAR (255)   NOT NULL,
    [Role_Name]        NVARCHAR (150)   NOT NULL,
    [Role_Description] NVARCHAR (MAX)   NOT NULL,
    [Role_TenantCode]  UNIQUEIDENTIFIER NULL,
    [Role_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [Role_CreatedOn]   DATETIME         NOT NULL,
    [Role_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Role_UpdatedOn]   DATETIME         NULL,
    [Role_Status]      BIT              NOT NULL,
    CONSTRAINT [PK__Roles__D80AB49B6D0D32F4] PRIMARY KEY CLUSTERED ([Role_ID] ASC),
    CONSTRAINT [FK_Roles_TenantDetails] FOREIGN KEY ([Role_TenantCode]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Role_Name]
    ON [dbo].[Roles]([Role_Name] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_Role_TenantCode]
    ON [dbo].[Roles]([Role_TenantCode] ASC);

