﻿CREATE TABLE [dbo].[RuleActivities] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF_RuleActivities_Id] DEFAULT (newsequentialid()) NOT NULL,
    [RulesetCode]       NVARCHAR (255)   NOT NULL,
    [TenantId]          UNIQUEIDENTIFIER NULL,
    [ActivityType]      VARCHAR (50)     NOT NULL,
    [ServiceEndpointId] UNIQUEIDENTIFIER NULL,
    [AssemblyTypeName]  VARCHAR (MAX)    NULL,
    [CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]         DATETIME         NOT NULL,
    [Status]            BIT              CONSTRAINT [DF_RuleActivities_Status] DEFAULT ((1)) NOT NULL,
    [ServiceOutputXML]  XML              NULL,
    CONSTRAINT [PK_RuleActivities] PRIMARY KEY CLUSTERED ([Id] ASC)
);

