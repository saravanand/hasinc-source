﻿CREATE TABLE [dbo].[RuleSet] (
    [RuleSetId]       UNIQUEIDENTIFIER CONSTRAINT [DF_RuleSet_RuleSetId] DEFAULT (newsequentialid()) NOT NULL,
    [TenantId]        UNIQUEIDENTIFIER NOT NULL,
    [RuleSetTypeId]   VARCHAR (50)     NULL,
    [RulesSetGroupId] VARCHAR (50)     NULL,
    [RuleContent]     NVARCHAR (MAX)   NOT NULL,
    [CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]     DATETIME         NOT NULL,
    [UpdatedBy]       UNIQUEIDENTIFIER NULL,
    [UpdatedDate]     DATETIME         NULL,
    CONSTRAINT [PK_Rule] PRIMARY KEY CLUSTERED ([RuleSetId] ASC)
);

