﻿CREATE TABLE [dbo].[RuleSetMetadata] (
    [Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_RuleMetadata_Id] DEFAULT (newsequentialid()) NOT NULL,
    [RulesetCode] NVARCHAR (255)   NULL,
    [Name]        NVARCHAR (255)   NOT NULL,
    [Description] NVARCHAR (MAX)   NULL,
    [TenantId]    UNIQUEIDENTIFIER NULL,
    [Category]    NVARCHAR (255)   NULL,
    [ScreenType]  VARCHAR (50)     NOT NULL,
    [CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    [UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [UpdatedOn]   DATETIME         NULL,
    [Status]      BIT              CONSTRAINT [DF_RuleMetadata_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_RuleMetadata] PRIMARY KEY CLUSTERED ([Id] ASC)
);

