﻿CREATE TABLE [dbo].[ScheduleJobs] (
    [ScheduleJobs_Id]         UNIQUEIDENTIFIER CONSTRAINT [DF_ScheduleJobs_ScheduleJobs_Id] DEFAULT (newsequentialid()) NOT NULL,
    [ScheduleJobs_ScheduleId] UNIQUEIDENTIFIER NOT NULL,
    [ScheduleJobs_JobId]      UNIQUEIDENTIFIER NOT NULL,
    [ScheduleJobs_CreatedBy]  UNIQUEIDENTIFIER NULL,
    [ScheduleJobs_CreatedOn]  DATETIME         CONSTRAINT [DF_ScheduleJobs_ScheduleJobs_CreatedOn] DEFAULT (getdate()) NULL,
    [ScheduleJobs_UpdatedBy]  UNIQUEIDENTIFIER NULL,
    [ScheduleJobs_UpdatedOn]  DATETIME         NULL,
    [ScheduleJobs_Status]     BIT              CONSTRAINT [DF_ScheduleJobs_ScheduleJobs_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ScheduleJobs] PRIMARY KEY CLUSTERED ([ScheduleJobs_Id] ASC),
    CONSTRAINT [FK_ScheduleJobs_Jobs] FOREIGN KEY ([ScheduleJobs_JobId]) REFERENCES [dbo].[Jobs] ([Job_Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ScheduleJobs_Schedules] FOREIGN KEY ([ScheduleJobs_ScheduleId]) REFERENCES [dbo].[Schedules] ([Schedule_Id]) ON DELETE CASCADE
);

