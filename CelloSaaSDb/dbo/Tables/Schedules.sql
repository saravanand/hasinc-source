﻿CREATE TABLE [dbo].[Schedules] (
    [Schedule_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_Schedule_Schedule_Id] DEFAULT (newsequentialid()) NOT NULL,
    [Schedule_Name]        NVARCHAR (255)   NOT NULL,
    [Schedule_TenantId]    UNIQUEIDENTIFIER NOT NULL,
    [Schedule_Description] NVARCHAR (MAX)   NULL,
    [Schedule_CreatedBy]   UNIQUEIDENTIFIER NULL,
    [Schedule_CreatedOn]   DATETIME         CONSTRAINT [DF_Schedule_Schedule_CreatedOn] DEFAULT (getdate()) NULL,
    [Schedule_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Schedule_UpdatedOn]   DATETIME         NULL,
    [Schedule_Status]      BIT              CONSTRAINT [DF_Schedule_Schedule_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED ([Schedule_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Schedules_TenantId]
    ON [dbo].[Schedules]([Schedule_TenantId] ASC);

