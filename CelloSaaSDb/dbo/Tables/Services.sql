﻿CREATE TABLE [dbo].[Services] (
    [Service_Code]        NVARCHAR (255)   NOT NULL,
    [Service_Name]        NVARCHAR (255)   NOT NULL,
    [Service_Description] NVARCHAR (MAX)   NOT NULL,
    [Service_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [Service_CreatedOn]   DATETIME         NOT NULL,
    [Service_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Service_UpdatedOn]   DATETIME         NULL,
    [Service_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED ([Service_Code] ASC),
    CONSTRAINT [FK_Services_Services] FOREIGN KEY ([Service_Code]) REFERENCES [dbo].[Services] ([Service_Code])
);

