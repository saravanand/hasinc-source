﻿CREATE TABLE [dbo].[SettingTemplate] (
    [SettingTemplate_Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_SettingTemplate_SettingTemplate_Id] DEFAULT (newsequentialid()) NOT NULL,
    [SettingTemplate_Name]      NVARCHAR (255)   NOT NULL,
    [SettingTemplate_Type]      VARCHAR (50)     NOT NULL,
    [SettingTemplate_TenantId]  UNIQUEIDENTIFIER NULL,
    [SettingTemplate_CreatedOn] DATETIME         NOT NULL,
    [SettingTemplate_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [SettingTemplate_UpdatedOn] DATETIME         NULL,
    [SettingTemplate_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [SettingTemplate_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_SettingTemplate] PRIMARY KEY CLUSTERED ([SettingTemplate_Id] ASC)
);

