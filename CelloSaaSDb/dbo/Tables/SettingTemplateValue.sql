﻿CREATE TABLE [dbo].[SettingTemplateValue] (
    [SettingTemplateValue_Id]                UNIQUEIDENTIFIER CONSTRAINT [DF_SettingTemplateValue_SettingTemplateValue_Id] DEFAULT (newsequentialid()) NOT NULL,
    [SettingTemplateValue_SettingTemplateId] UNIQUEIDENTIFIER NOT NULL,
    [SettingTemplateValue_AttributeId]       NVARCHAR (255)   NOT NULL,
    [SettingTemplateValue_AttributeValue]    NVARCHAR (MAX)   NULL,
    [SettingTemplateValue_CreatedOn]         DATETIME         NOT NULL,
    [SettingTemplateValue_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [SettingTemplateValue_UpdatedOn]         DATETIME         NULL,
    [SettingTemplateValue_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [SettingTemplateValue_Status]            BIT              NOT NULL,
    CONSTRAINT [PK_SettingTemplateValue] PRIMARY KEY CLUSTERED ([SettingTemplateValue_Id] ASC),
    CONSTRAINT [FK_SettingTemplateValue_SettingsAttribute] FOREIGN KEY ([SettingTemplateValue_AttributeId]) REFERENCES [dbo].[SettingsAttribute] ([SettingsAttribute_ID]),
    CONSTRAINT [FK_SettingTemplateValue_SettingTemplate1] FOREIGN KEY ([SettingTemplateValue_SettingTemplateId]) REFERENCES [dbo].[SettingTemplate] ([SettingTemplate_Id])
);

