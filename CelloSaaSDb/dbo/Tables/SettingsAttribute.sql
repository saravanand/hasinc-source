﻿CREATE TABLE [dbo].[SettingsAttribute] (
    [SettingsAttribute_ID]              NVARCHAR (255)   NOT NULL,
    [SettingsAttribute_Name]            NVARCHAR (255)   NOT NULL,
    [SettingsAttribute_DataFieldTypeId] UNIQUEIDENTIFIER NOT NULL,
    [SettingsAttribute_ModuleCode]      NVARCHAR (255)   NULL,
    [SettingsAttribute_FeatureCode]     NVARCHAR (255)   NULL,
    [SettingsAttribute_AttributeSetId]  UNIQUEIDENTIFIER NULL,
    [SettingsAttribute_CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [SettingsAttribute_CreatedOn]       DATETIME         NOT NULL,
    [SettingsAttribute_UpdatedBy]       UNIQUEIDENTIFIER NULL,
    [SettingsAttribute_UpdatedOn]       DATETIME         NULL,
    [SettingsAttribute_Status]          BIT              NOT NULL,
    CONSTRAINT [PK_TenantSettingsAttribute] PRIMARY KEY CLUSTERED ([SettingsAttribute_ID] ASC),
    CONSTRAINT [FK_SettingsAttribute_AttributeSet] FOREIGN KEY ([SettingsAttribute_AttributeSetId]) REFERENCES [dbo].[AttributeSet] ([AttributeSet_ID]),
    CONSTRAINT [FK_SettingsAttribute_DataFieldType] FOREIGN KEY ([SettingsAttribute_DataFieldTypeId]) REFERENCES [dbo].[DataFieldType] ([DataFieldType_ID]),
    CONSTRAINT [FK_TenantSettingsAttribute_Features] FOREIGN KEY ([SettingsAttribute_FeatureCode]) REFERENCES [dbo].[Features] ([Feature_Code]),
    CONSTRAINT [FK_TenantSettingsAttribute_Modules] FOREIGN KEY ([SettingsAttribute_ModuleCode]) REFERENCES [dbo].[Modules] ([Module_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_SettingsAttribute_AttributeSetId]
    ON [dbo].[SettingsAttribute]([SettingsAttribute_AttributeSetId] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_SettingsAttribute_DataFieldTypeId]
    ON [dbo].[SettingsAttribute]([SettingsAttribute_DataFieldTypeId] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_SettingsAttribute_FeatureCode]
    ON [dbo].[SettingsAttribute]([SettingsAttribute_FeatureCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_SettingsAttribute_ModuleCode]
    ON [dbo].[SettingsAttribute]([SettingsAttribute_ModuleCode] ASC);

