﻿CREATE TABLE [dbo].[SourceContent] (
    [SourceContent_Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_SourceContent_SourceContent_Id] DEFAULT (newsequentialid()) NOT NULL,
    [SourceContent_Content]   XML              NOT NULL,
    [SourceContent_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [SourceContent_CreatedOn] DATETIME         NOT NULL,
    [SourceContent_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [SourceContent_UpdatedOn] DATETIME         NULL,
    [SourceContent_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_SourceContent] PRIMARY KEY CLUSTERED ([SourceContent_Id] ASC)
);

