﻿CREATE TABLE [dbo].[SubjectDetails] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [UserId]             UNIQUEIDENTIFIER NOT NULL,
    [AuthenticationType] UNIQUEIDENTIFIER NULL,
    [ClientId]           UNIQUEIDENTIFIER NULL,
    [Name]               VARCHAR (50)     NOT NULL,
    [MembershipId]       UNIQUEIDENTIFIER NOT NULL,
    [TenantId]           UNIQUEIDENTIFIER NOT NULL,
    [ProviderId]         VARCHAR (512)    NULL,
    [CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]          DATETIME         NOT NULL,
    [UpdatedOn]          DATETIME         NULL,
    [UpdatedBy]          UNIQUEIDENTIFIER NULL,
    [Status]             BIT              NOT NULL,
    CONSTRAINT [PK_SubjectDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SubjectDetails_ClientDetails] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientDetails] ([Id]),
    CONSTRAINT [FK_SubjectDetails_TenantDetails] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code]),
    CONSTRAINT [FK_SubjectDetails_UserDetails] FOREIGN KEY ([UserId]) REFERENCES [dbo].[UserDetails] ([User_UserID])
);

