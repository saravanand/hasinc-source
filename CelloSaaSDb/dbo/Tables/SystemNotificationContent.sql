﻿CREATE TABLE [dbo].[SystemNotificationContent] (
    [SystemNotificationContent_NotificationContentId] UNIQUEIDENTIFIER NOT NULL,
    [SystemNotificationContent_Content]               NVARCHAR (MAX)   NULL,
    [SystemNotificationContent_ProcessedContent]      NVARCHAR (MAX)   NOT NULL,
    [SystemNotificationContent_CreatedOn]             DATETIME         NOT NULL,
    [SystemNotificationContent_CreatedBy]             UNIQUEIDENTIFIER NOT NULL,
    [SystemNotificationContent_UpdatedOn]             DATETIME         NULL,
    [SystemNotificationContent_UpdatedBy]             UNIQUEIDENTIFIER NULL,
    [SystemNotificationContent_Status]                BIT              NOT NULL,
    CONSTRAINT [PK_SystemNotificationContent] PRIMARY KEY CLUSTERED ([SystemNotificationContent_NotificationContentId] ASC),
    CONSTRAINT [FK_SystemNotificationContent_NotificationContent] FOREIGN KEY ([SystemNotificationContent_NotificationContentId]) REFERENCES [dbo].[NotificationContent] ([NotificationContent_Id])
);

