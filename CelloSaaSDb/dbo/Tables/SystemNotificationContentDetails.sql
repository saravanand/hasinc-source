﻿CREATE TABLE [dbo].[SystemNotificationContentDetails] (
    [SystemNotificationContentDetails_ContentDetailsId] UNIQUEIDENTIFIER NOT NULL,
    [SystemNotificationContentDetails_Content]          NVARCHAR (MAX)   NULL,
    [SystemNotificationContentDetails_TemplateId]       UNIQUEIDENTIFIER NULL,
    [SystemNotificationContentDetails_CreatedBy]        UNIQUEIDENTIFIER NULL,
    [SystemNotificationContentDetails_CreatedOn]        DATETIME         CONSTRAINT [DF_Table_1_EmailContentDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [SystemNotificationContentDetails_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [SystemNotificationContentDetails_UpdatedOn]        DATETIME         NULL,
    [SystemNotificationContentDetails_Status]           BIT              CONSTRAINT [DF_Table_1_EmailContentDetails_Status] DEFAULT ((1)) NOT NULL,
    [SystemNotificationContentDetails_RuleSetCode]      VARCHAR (50)     NULL,
    CONSTRAINT [PK_SystemNotificationContentDetails] PRIMARY KEY CLUSTERED ([SystemNotificationContentDetails_ContentDetailsId] ASC),
    CONSTRAINT [FK_SystemNotificationContentDetails_NotificationContentDetails] FOREIGN KEY ([SystemNotificationContentDetails_ContentDetailsId]) REFERENCES [dbo].[NotificationContentDetails] ([NotificationContentDetails_Id]),
    CONSTRAINT [FK_SystemNotificationContentDetails_Template] FOREIGN KEY ([SystemNotificationContentDetails_TemplateId]) REFERENCES [dbo].[Template] ([Template_Id])
);

