﻿CREATE TABLE [dbo].[SystemNotificationDetails] (
    [SystemNotificationDetails_Id]                       UNIQUEIDENTIFIER CONSTRAINT [DF_SystemNotificationDetails_SystemNotificationDetails_Id] DEFAULT (newsequentialid()) NOT NULL,
    [SystemNotificationDetails_BatchId]                  UNIQUEIDENTIFIER NULL,
    [SystemNotificationDetails_MapId]                    NVARCHAR (255)   NOT NULL,
    [SystemNotificationDetails_Content]                  NVARCHAR (MAX)   NOT NULL,
    [SystemNotificationDetails_SystemNotificationStatus] VARCHAR (15)     NOT NULL,
    [SystemNotificationDetails_StatusMessage]            VARCHAR (50)     NULL,
    [SystemNotificationDetails_CreatedBy]                UNIQUEIDENTIFIER NOT NULL,
    [SystemNotificationDetails_CreatedOn]                DATETIME         NOT NULL,
    [SystemNotificationDetails_UpdatedBy]                UNIQUEIDENTIFIER NULL,
    [SystemNotificationDetails_UpdatedOn]                DATETIME         NULL,
    [SystemNotificationDetails_TrackId]                  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_SystemNotificationDetails] PRIMARY KEY CLUSTERED ([SystemNotificationDetails_Id] ASC),
    CONSTRAINT [FK_SystemNotificationDetails_NotificationBatch] FOREIGN KEY ([SystemNotificationDetails_BatchId]) REFERENCES [dbo].[NotificationBatch] ([NotificationBatch_Id])
);

