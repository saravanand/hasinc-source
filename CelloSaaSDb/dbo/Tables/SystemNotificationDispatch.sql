﻿CREATE TABLE [dbo].[SystemNotificationDispatch] (
    [SystemNotificationDispatch_NotificationDispatchId] UNIQUEIDENTIFIER NOT NULL,
    [SystemNotificationDispatch_MapId]                  NVARCHAR (255)   NOT NULL,
    [SystemNotificationDispatch_CreatedOn]              DATETIME         NOT NULL,
    [SystemNotificationDispatch_CreatedBy]              UNIQUEIDENTIFIER NOT NULL,
    [SystemNotificationDispatch_UpdatedOn]              DATETIME         NULL,
    [SystemNotificationDispatch_UpdatedBy]              UNIQUEIDENTIFIER NULL,
    [SystemNotificationDispatch_Status]                 BIT              NOT NULL,
    CONSTRAINT [PK_SystemNotificationDispatch] PRIMARY KEY CLUSTERED ([SystemNotificationDispatch_NotificationDispatchId] ASC),
    CONSTRAINT [FK_SystemNotificationDispatch_NotificationDispatch] FOREIGN KEY ([SystemNotificationDispatch_NotificationDispatchId]) REFERENCES [dbo].[NotificationDispatch] ([NotificationDispatch_Id])
);

