﻿CREATE TABLE [dbo].[SystemNotificationDispatchDetails] (
    [SystemNotificationDispatchDetails_DispatchDetailsId] UNIQUEIDENTIFIER NOT NULL,
    [SystemNotificationDispatchDetails_MapId]             NVARCHAR (255)   NULL,
    [SystemNotificationDispatchDetails_CreatedBy]         UNIQUEIDENTIFIER NULL,
    [SystemNotificationDispatchDetails_CreatedOn]         DATETIME         CONSTRAINT [DF_Table_1_EmailDispatchDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [SystemNotificationDispatchDetails_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [SystemNotificationDispatchDetails_UpdatedOn]         DATETIME         NULL,
    [SystemNotificationDispatchDetails_Status]            BIT              CONSTRAINT [DF_Table_1_EmailDispatchDetails_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_SystemNotificationDispatchDetails] PRIMARY KEY CLUSTERED ([SystemNotificationDispatchDetails_DispatchDetailsId] ASC),
    CONSTRAINT [FK_SystemNotificationDispatchDetails_NotificationDispatchDetails] FOREIGN KEY ([SystemNotificationDispatchDetails_DispatchDetailsId]) REFERENCES [dbo].[NotificationDispatchDetails] ([NotificationDispatchDetails_Id])
);

