﻿CREATE TABLE [dbo].[TableResultColumn] (
    [TableResultColumn_Id]             UNIQUEIDENTIFIER CONSTRAINT [DF_TableResultColumn_TableResultColumn_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TableResultColumn_TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [TableResultColumn_TableSourceId]  UNIQUEIDENTIFIER NOT NULL,
    [TableResultColumn_ReportObjectId] UNIQUEIDENTIFIER NOT NULL,
    [TableResultColumn_Column]         NVARCHAR (255)   NOT NULL,
    [TableResultColumn_DisplayName]    NVARCHAR (255)   NOT NULL,
    [TableResultColumn_Ordinal]        INT              NULL,
    [TableResultColumn_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [TableResultColumn_CreatedOn]      DATETIME         NOT NULL,
    [TableResultColumn_UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [TableResultColumn_UpdatedOn]      DATETIME         NULL,
    [TableResultColumn_Status]         BIT              NOT NULL,
    CONSTRAINT [PK_TableResultColumn] PRIMARY KEY CLUSTERED ([TableResultColumn_Id] ASC),
    CONSTRAINT [FK_TableResultColumn_ReportObjects] FOREIGN KEY ([TableResultColumn_ReportObjectId]) REFERENCES [dbo].[ReportObject] ([ReportObject_Id]),
    CONSTRAINT [FK_TableResultColumn_TableSource] FOREIGN KEY ([TableResultColumn_TableSourceId]) REFERENCES [dbo].[TableSource] ([TableSource_Id]),
    CONSTRAINT [FK_TableResultColumn_TenantDetails] FOREIGN KEY ([TableResultColumn_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [IX_TableResultColumn]
    ON [dbo].[TableResultColumn]([TableResultColumn_Id] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_TableResultColumn_ReportObjectId]
    ON [dbo].[TableResultColumn]([TableResultColumn_ReportObjectId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_TableResultColumn_TableSourceId]
    ON [dbo].[TableResultColumn]([TableResultColumn_TableSourceId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_TableResultColumn_TenantId]
    ON [dbo].[TableResultColumn]([TableResultColumn_TableSourceId] ASC) WITH (FILLFACTOR = 100);

