﻿CREATE TABLE [dbo].[TableSource] (
    [TableSource_Id]           UNIQUEIDENTIFIER CONSTRAINT [DF_TableSource_TableSource_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TableSource_Name]         NVARCHAR (255)   NOT NULL,
    [TableSource_Description]  NVARCHAR (MAX)   NULL,
    [TableSource_DataSourceId] UNIQUEIDENTIFIER NULL,
    [TableSource_TenantId]     UNIQUEIDENTIFIER NOT NULL,
    [TableSource_CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [TableSource_CreatedOn]    DATETIME         NOT NULL,
    [TableSource_UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [TableSource_UpdatedOn]    DATETIME         NULL,
    [TableSource_Status]       BIT              NOT NULL,
    CONSTRAINT [PK_TableSource] PRIMARY KEY CLUSTERED ([TableSource_Id] ASC),
    CONSTRAINT [FK_TableSource_DataSource] FOREIGN KEY ([TableSource_DataSourceId]) REFERENCES [dbo].[DataSource] ([DataSource_Id]),
    CONSTRAINT [FK_TableSource_TenantDetails] FOREIGN KEY ([TableSource_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [IX_TableSource]
    ON [dbo].[TableSource]([TableSource_Id] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_TableSource_DataSource]
    ON [dbo].[TableSource]([TableSource_DataSourceId] ASC) WITH (FILLFACTOR = 100);


GO
CREATE NONCLUSTERED INDEX [IX_TableSource_TenantId]
    ON [dbo].[TableSource]([TableSource_TenantId] ASC) WITH (FILLFACTOR = 100);

