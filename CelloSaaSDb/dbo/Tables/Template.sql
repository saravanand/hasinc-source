﻿CREATE TABLE [dbo].[Template] (
    [Template_Id]          UNIQUEIDENTIFIER NOT NULL,
    [Template_TenantId]    UNIQUEIDENTIFIER NULL,
    [Template_Name]        NVARCHAR (255)   NULL,
    [Template_Description] NVARCHAR (MAX)   NULL,
    [Template_Category]    NVARCHAR (255)   NULL,
    [Template_Content]     NVARCHAR (MAX)   NULL,
    [Template_CreatedBy]   UNIQUEIDENTIFIER NULL,
    [Template_CreatedOn]   DATETIME         DEFAULT (getdate()) NOT NULL,
    [Template_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [Template_UpdatedOn]   DATETIME         NULL,
    [Template_Status]      BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED ([Template_Id] ASC)
);

