﻿CREATE TABLE [dbo].[TenantActivationDetails] (
    [TenantId]       UNIQUEIDENTIFIER NOT NULL,
    [ActivationKey]  VARCHAR (MAX)    NOT NULL,
    [ExpirationDate] DATETIME         NOT NULL,
    [CreatedOn]      DATETIME         NOT NULL,
    [CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]      DATETIME         NULL,
    [UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [Status]         BIT              NOT NULL,
    CONSTRAINT [PK_TenantActivationDetails] PRIMARY KEY CLUSTERED ([TenantId] ASC),
    CONSTRAINT [FK_TenantActivationDetails_TenantDetails] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

