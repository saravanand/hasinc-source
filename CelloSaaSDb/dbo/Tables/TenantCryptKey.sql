﻿CREATE TABLE [dbo].[TenantCryptKey] (
    [TenantCryptKey_Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_TenantCryptKey_TenantCryptKey_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TenantCryptKey_TenantId]  UNIQUEIDENTIFIER NULL,
    [TenantCryptKey_Key]       NVARCHAR (255)   NOT NULL,
    [TenantCryptKey_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [TenantCryptKey_CreatedOn] DATETIME         NOT NULL,
    [TenantCryptKey_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [TenantCryptKey_UpdatedOn] DATETIME         NULL,
    [TenantCryptKey_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_TenantCryptKey] PRIMARY KEY CLUSTERED ([TenantCryptKey_Id] ASC),
    CONSTRAINT [FK_TenantCryptKey_TenantDetails] FOREIGN KEY ([TenantCryptKey_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);


GO
CREATE NONCLUSTERED INDEX [tenantid]
    ON [dbo].[TenantCryptKey]([TenantCryptKey_TenantId] ASC) WITH (FILLFACTOR = 100);

