﻿CREATE TABLE [dbo].[TenantDetails] (
    [Tenant_Code]             UNIQUEIDENTIFIER CONSTRAINT [DF_TenantDetails_Tenant_Code] DEFAULT (newsequentialid()) NOT NULL,
    [Tenant_Name]             NVARCHAR (255)   NOT NULL,
    [Tenant_Description]      NVARCHAR (MAX)   NULL,
    [Tenant_AddressID]        UNIQUEIDENTIFIER NULL,
    [Tenant_Website]          VARCHAR (255)    NULL,
    [Tenant_ContactID]        UNIQUEIDENTIFIER NULL,
    [Tenant_BillingType]      VARCHAR (50)     NULL,
    [Tenant_PaymentTypeCode]  NVARCHAR (255)   NULL,
    [Tenant_BillOther]        BIT              NULL,
    [Tenant_BillingContactID] UNIQUEIDENTIFIER NULL,
    [Tenant_BillingAddressID] UNIQUEIDENTIFIER NULL,
    [Tenant_IsProductAdmin]   BIT              NULL,
    [Tenant_URL]              VARCHAR (255)    NULL,
    [Tenant_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [Tenant_CreatedOn]        DATETIME         NOT NULL,
    [Tenant_UpdatedOn]        DATETIME         NULL,
    [Tenant_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [Tenant_Status]           BIT              NOT NULL,
    [Tenant_CodeString]       VARCHAR (100)    NOT NULL,
    [Tenant_ApprovalStatus]   VARCHAR (50)     NOT NULL,
    [Tenant_ParentTenantId]   UNIQUEIDENTIFIER NULL,
    [Tenant_Size]             NVARCHAR (100)   NULL,
    [Tenant_EnableAutoDebit]  BIT              DEFAULT ((0)) NOT NULL,
    [Tenant_IsSelfRegistered] BIT              DEFAULT ((0)) NOT NULL,
    [Tenant_DataPartitionId]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TenantDetails] PRIMARY KEY CLUSTERED ([Tenant_Code] ASC),
    CONSTRAINT [FK_TenantDetails_Address] FOREIGN KEY ([Tenant_AddressID]) REFERENCES [dbo].[Address] ([Address_ID]),
    CONSTRAINT [FK_TenantDetails_Contact] FOREIGN KEY ([Tenant_ContactID]) REFERENCES [dbo].[Contact] ([Contact_ID]),
    CONSTRAINT [FK_TenantDetails_PaymentType] FOREIGN KEY ([Tenant_PaymentTypeCode]) REFERENCES [dbo].[PaymentType] ([PaymentType_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Tenant_Name]
    ON [dbo].[TenantDetails]([Tenant_Name] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_Tenant_URL]
    ON [dbo].[TenantDetails]([Tenant_URL] ASC);

