﻿CREATE TABLE [dbo].[TenantDetailsTenantType] (
    [TenantDetailsTenantType_ID]            UNIQUEIDENTIFIER CONSTRAINT [DF_TenantDetailsTenantType_TenantDetailsTenantType_ID] DEFAULT (newsequentialid()) NOT NULL,
    [TenantDetailsTenantType_Tenant_Code]   UNIQUEIDENTIFIER NOT NULL,
    [TenantDetailsTenantType_TenantType_ID] UNIQUEIDENTIFIER NOT NULL,
    [TenantDetailsTenantType_Priority]      SMALLINT         NULL,
    [TenantDetailsTenantType_CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [TenantDetailsTenantType_CreatedOn]     DATE             CONSTRAINT [DF_TenantDetailsTenantType_TenantDetailsTenantType_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [TenantDetailsTenantType_UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [TenantDetailsTenantType_UpdatedOn]     DATE             NULL,
    [TenantDetailsTenantType_Status]        BIT              CONSTRAINT [DF_TenantDetailsTenantType_TenantDetailsTenantType_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_TenantDetailsTenantType] PRIMARY KEY CLUSTERED ([TenantDetailsTenantType_ID] ASC)
);

