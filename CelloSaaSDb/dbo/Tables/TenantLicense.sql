﻿CREATE TABLE [dbo].[TenantLicense] (
    [TenantLicense_Code]             UNIQUEIDENTIFIER CONSTRAINT [DF_TenantLicense_TenantLicense_Code] DEFAULT (newsequentialid()) NOT NULL,
    [TenantLicense_TenantID]         UNIQUEIDENTIFIER NOT NULL,
    [TenantLicense_ValidityStart]    DATETIME         NOT NULL,
    [TenantLicense_ValidityEnd]      DATETIME         NULL,
    [TenantLicense_NumberOfUser]     INT              NULL,
    [TenantLicense_LicensePackageId] UNIQUEIDENTIFIER NOT NULL,
    [TenantLicense_CreatedOn]        DATETIME         NOT NULL,
    [TenantLicense_CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [TenantLicense_UpdatedOn]        DATETIME         NULL,
    [TenantLicense_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [TenantLicense_Status]           BIT              NOT NULL,
    [TenantLicense_PricePlanId]      UNIQUEIDENTIFIER NULL,
    [TenantLicense_TrialEndDate]     DATETIME         NULL,
    CONSTRAINT [PK_TenantLicense] PRIMARY KEY CLUSTERED ([TenantLicense_Code] ASC),
    CONSTRAINT [CHK_LICENSETRIALCHECK] CHECK ([TenantLicense_TrialEndDate]<>NULL AND [TenantLicense_TrialEndDate]>=[TenantLicense_ValidityStart]),
    CONSTRAINT [CHK_LICENSEVALIDITYENDCHECK] CHECK ([TenantLicense_ValidityEnd]<>NULL AND [TenantLicense_ValidityEnd]>=[TenantLicense_ValidityStart]),
    CONSTRAINT [FK_Tenant_License_TenantDetails] FOREIGN KEY ([TenantLicense_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code]),
    CONSTRAINT [FK_TenantLicense_LicensePackage] FOREIGN KEY ([TenantLicense_LicensePackageId]) REFERENCES [dbo].[LicensePackage] ([LicensePackage_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_TenantLicense_LicensePackageId]
    ON [dbo].[TenantLicense]([TenantLicense_LicensePackageId] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_TenantLicense_TenantID]
    ON [dbo].[TenantLicense]([TenantLicense_TenantID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_TenantLicense_ValidityEnd]
    ON [dbo].[TenantLicense]([TenantLicense_ValidityEnd] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_TenantLicense_ValidityStart]
    ON [dbo].[TenantLicense]([TenantLicense_ValidityStart] ASC);

