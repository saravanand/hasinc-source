﻿CREATE TABLE [dbo].[TenantRelationExpanded] (
    [TenantRelationExpanded_Id]             UNIQUEIDENTIFIER CONSTRAINT [DF_TenantRelationExpanded_TenantRelationExpanded_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TenantRelationExpanded_MappingId]      UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationExpanded_ChildTenantId]  UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationExpanded_ParentTenantId] UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationExpanded_Level]          INT              NOT NULL,
    [TenantRelationExpanded_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationExpanded_CreatedOn]      DATE             NOT NULL,
    [TenantRelationExpanded_UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [TenantRelationExpanded_UpdatedOn]      DATE             NULL,
    [TenantRelationExpanded_Status]         BIT              NOT NULL
);

