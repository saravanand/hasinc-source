﻿CREATE TABLE [dbo].[TenantRelationMappings] (
    [TenantRelationMapping_ID]                   UNIQUEIDENTIFIER CONSTRAINT [DF_TenantRealationMappings_TenantRealationMapping_ID] DEFAULT (newsequentialid()) NOT NULL,
    [TenantRelationMapping_ParentTenantID]       UNIQUEIDENTIFIER NULL,
    [TenantRelationMapping_ChildTenantID]        UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationMapping_TenantRelationTypeID] UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationMapping_CreatedBy]            UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationMapping_CreatedOn]            DATE             CONSTRAINT [DF_TenantRelationMappings_TenantRelationMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [TenantRelationMapping_UpdatedBy]            UNIQUEIDENTIFIER NULL,
    [TenantRelationMapping_UpdatedOn]            DATE             NULL,
    [TenantRelationMapping_Status]               BIT              CONSTRAINT [DF_TenantRealationMappings_TenantRealationMapping_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_TenantRealationMappings] PRIMARY KEY CLUSTERED ([TenantRelationMapping_ID] ASC)
);

