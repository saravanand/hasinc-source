﻿CREATE TABLE [dbo].[TenantRelationTypes] (
    [TenantRelationType_ID]        VARCHAR (50)     CONSTRAINT [DF_TenantRelationTypes_TenantRelationType_ID] DEFAULT (newid()) NOT NULL,
    [TenantRelationType_Name]      VARCHAR (50)     NOT NULL,
    [TenantRelationType_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [TenantRelationType_CreatedOn] DATE             CONSTRAINT [DF_TenantRelationTypes_TenantRelationType_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [TenantRelationType_UpdateBy]  UNIQUEIDENTIFIER NULL,
    [TenantRelationType_UpdateOn]  DATE             NULL,
    [TenantRelationType_Status]    BIT              CONSTRAINT [DF_TenantRelationType_TenantRelationType_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_TenantRelationType] PRIMARY KEY CLUSTERED ([TenantRelationType_ID] ASC)
);

