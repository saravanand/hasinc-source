﻿CREATE TABLE [dbo].[TenantScope] (
    [TenantScope_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_TenantScope_TenantScope_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TenantScope_Name]        NVARCHAR (255)   NOT NULL,
    [TenantScope_Description] NVARCHAR (MAX)   NULL,
    [TenantScope_FilterQuery] NVARCHAR (MAX)   NOT NULL,
    [TenantScope_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [TenantScope_CreatedOn]   DATETIME         NOT NULL,
    [TenantScope_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [TenantScope_UpdatedOn]   DATETIME         NULL,
    [TenantScope_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_TenantScope] PRIMARY KEY CLUSTERED ([TenantScope_Id] ASC)
);

