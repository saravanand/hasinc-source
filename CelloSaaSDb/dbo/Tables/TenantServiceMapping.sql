﻿CREATE TABLE [dbo].[TenantServiceMapping] (
    [TenantServiceMapping_Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_TenantServiceMapping_TenantServiceMapping_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TenantServiceMapping_TenantId]  UNIQUEIDENTIFIER NOT NULL,
    [TenantServiceMapping_ServiceId] NVARCHAR (255)   NOT NULL,
    [TenantServiceMapping_StartDate] DATETIME         NULL,
    [TenantServiceMapping_EndDate]   DATETIME         NULL,
    [TenantServiceMapping_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [TenantServiceMapping_CreatedOn] DATETIME         NOT NULL,
    [TenantServiceMapping_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [TenantServiceMapping_UpdatedOn] DATETIME         NULL,
    [TenantServiceMapping_Status]    BIT              NOT NULL,
    CONSTRAINT [FK_TenantServiceMapping_Services] FOREIGN KEY ([TenantServiceMapping_ServiceId]) REFERENCES [dbo].[Services] ([Service_Code]),
    CONSTRAINT [FK_TenantServiceMapping_TenantDetails] FOREIGN KEY ([TenantServiceMapping_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

