﻿CREATE TABLE [dbo].[TenantSetting] (
    [TenantSetting_TenantID]       UNIQUEIDENTIFIER NOT NULL,
    [TenantSetting_AttributeID]    NVARCHAR (255)   NOT NULL,
    [TenantSetting_AttributeValue] NVARCHAR (4000)  NULL,
    [TenantSetting_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [TenantSetting_CreatedOn]      DATETIME         NOT NULL,
    [TenantSetting_UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [TenantSetting_UpdatedOn]      DATETIME         NULL,
    [TenantSetting_Status]         BIT              NOT NULL,
    CONSTRAINT [PK_TenantSetting] PRIMARY KEY CLUSTERED ([TenantSetting_TenantID] ASC, [TenantSetting_AttributeID] ASC),
    CONSTRAINT [FK_TenantSetting_SettingsAttribute] FOREIGN KEY ([TenantSetting_AttributeID]) REFERENCES [dbo].[SettingsAttribute] ([SettingsAttribute_ID]),
    CONSTRAINT [FK_TenantSetting_TenantDetails] FOREIGN KEY ([TenantSetting_TenantID]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

