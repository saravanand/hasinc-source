﻿CREATE TABLE [dbo].[TenantSettingTemplate] (
    [TenantSettingTemplate_Id]                UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [TenantSettingTemplate_SettingTemplateId] UNIQUEIDENTIFIER NOT NULL,
    [TenantSettingTemplate_TenantId]          UNIQUEIDENTIFIER NOT NULL,
    [TenantSettingTemplate_CreatedOn]         DATETIME         NOT NULL,
    [TenantSettingTemplate_CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [TenantSettingTemplate_UpdatedOn]         DATETIME         NULL,
    [TenantSettingTemplate_UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [TenantSettingTemplate_Status]            BIT              NOT NULL,
    CONSTRAINT [PK_TenantSettingTemplate] PRIMARY KEY CLUSTERED ([TenantSettingTemplate_Id] ASC),
    CONSTRAINT [FK_TenantSettingTemplate_SettingTemplate] FOREIGN KEY ([TenantSettingTemplate_SettingTemplateId]) REFERENCES [dbo].[SettingTemplate] ([SettingTemplate_Id]),
    CONSTRAINT [FK_TenantSettingTemplate_TenantDetails] FOREIGN KEY ([TenantSettingTemplate_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

