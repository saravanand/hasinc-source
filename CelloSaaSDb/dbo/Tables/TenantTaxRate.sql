﻿CREATE TABLE [dbo].[TenantTaxRate] (
    [TenantTaxRateId] UNIQUEIDENTIFIER NOT NULL,
    [TaxRateId]       UNIQUEIDENTIFIER NOT NULL,
    [Name]            NVARCHAR (150)   NOT NULL,
    [Description]     NVARCHAR (300)   NULL,
    [Percentage]      FLOAT (53)       DEFAULT ((0)) NOT NULL,
    [TaxOrder]        SMALLINT         DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TenantTaxRate] PRIMARY KEY CLUSTERED ([TenantTaxRateId] ASC)
);

