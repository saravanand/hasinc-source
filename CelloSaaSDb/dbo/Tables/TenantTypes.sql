﻿CREATE TABLE [dbo].[TenantTypes] (
    [TenantType_ID]          UNIQUEIDENTIFIER CONSTRAINT [DF_TenantType_TenantType_ID] DEFAULT (newsequentialid()) NOT NULL,
    [TenantType_Name]        NVARCHAR (255)   NOT NULL,
    [TenantType_AccessLevel] SMALLINT         NOT NULL,
    [TenantType_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [TenantType_CreatedOn]   DATE             CONSTRAINT [DF_TenantType_TenantType_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [TenantType_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [TenantType_UpdatedOn]   DATE             NULL,
    [TenantType_Status]      BIT              CONSTRAINT [DF_TenantType_TenantType_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_TenantType] PRIMARY KEY CLUSTERED ([TenantType_ID] ASC)
);

