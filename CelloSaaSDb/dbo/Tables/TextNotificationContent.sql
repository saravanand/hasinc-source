﻿CREATE TABLE [dbo].[TextNotificationContent] (
    [TextNotificationContent_NotificationContentId]    UNIQUEIDENTIFIER NOT NULL,
    [TextNotificationContent_Content]                  NVARCHAR (MAX)   NULL,
    [TextNotificationContent_ProcessedContent]         NVARCHAR (MAX)   NOT NULL,
    [TextNotificationContent_AttachmentFileFolderPath] VARCHAR (MAX)    NULL,
    [TextNotificationContent_AttachmentFiles]          VARCHAR (MAX)    NULL,
    [TextNotificationContent_CreatedOn]                DATETIME         NOT NULL,
    [TextNotificationContent_CreatedBy]                UNIQUEIDENTIFIER NOT NULL,
    [TextNotificationContent_UpdatedOn]                DATETIME         NULL,
    [TextNotificationContent_UpdatedBy]                UNIQUEIDENTIFIER NULL,
    [TextNotificationContent_Status]                   BIT              NOT NULL,
    [TextNotificationContent_Subject]                  NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_TextNotificationContent] PRIMARY KEY CLUSTERED ([TextNotificationContent_NotificationContentId] ASC),
    CONSTRAINT [FK_TextNotificationContent_NotificationContent] FOREIGN KEY ([TextNotificationContent_NotificationContentId]) REFERENCES [dbo].[NotificationContent] ([NotificationContent_Id])
);

