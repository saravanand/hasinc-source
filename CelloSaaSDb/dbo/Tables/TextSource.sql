﻿CREATE TABLE [dbo].[TextSource] (
    [TextSource_Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_TextSourceId_TextSource_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TextSource_Name]      NVARCHAR (255)   NULL,
    [TextSource_Content]   NVARCHAR (MAX)   NOT NULL,
    [TextSource_TenantId]  UNIQUEIDENTIFIER NOT NULL,
    [TextSource_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [TextSource_CreatedOn] DATETIME         NOT NULL,
    [TextSource_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [TextSource_UpdatedOn] DATETIME         NULL,
    [TextSource_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_TextSourceId] PRIMARY KEY CLUSTERED ([TextSource_Id] ASC),
    CONSTRAINT [FK_TextSource_TenantDetails] FOREIGN KEY ([TextSource_TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

