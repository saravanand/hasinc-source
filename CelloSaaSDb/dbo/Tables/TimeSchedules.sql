﻿CREATE TABLE [dbo].[TimeSchedules] (
    [TimeSchedule_Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_TimeSchedules_TimeSchedule_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TimeSchedule_ScheduleId]       UNIQUEIDENTIFIER NOT NULL,
    [TimeSchedule_JobId]            UNIQUEIDENTIFIER NOT NULL,
    [TimeSchedule_JobReferenceName] VARCHAR (500)    NULL,
    [TimeSchedule_StartDateTime]    DATETIME         NULL,
    [TimeSchedule_EndDateTime]      DATETIME         NULL,
    [TimeSchedule_CreatedBy]        UNIQUEIDENTIFIER NULL,
    [TimeSchedule_CreatedOn]        DATETIME         CONSTRAINT [DF_TimeSchedules_TimeSchedule_CreatedOn] DEFAULT (getdate()) NULL,
    [TimeSchedule_UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [TimeSchedule_UpdatedOn]        DATETIME         NULL,
    [TimeSchedule_Status]           BIT              CONSTRAINT [DF_TimeSchedules_TimeSchedule_Status] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_TimeSchedules] PRIMARY KEY CLUSTERED ([TimeSchedule_Id] ASC)
);

