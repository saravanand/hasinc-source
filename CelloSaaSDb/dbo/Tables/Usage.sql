﻿CREATE TABLE [dbo].[Usage] (
    [Usage_Code]               NVARCHAR (255)   NOT NULL,
    [Usage_ModuleCode]         NVARCHAR (255)   NOT NULL,
    [Usage_UsageTypeCode]      UNIQUEIDENTIFIER NOT NULL,
    [Usage_UnitOfMeasurement]  NVARCHAR (255)   NOT NULL,
    [Usage_Name]               NVARCHAR (255)   NOT NULL,
    [Usage_CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [Usage_CreatedOn]          DATETIME         NOT NULL,
    [Usage_UpdatedBy]          UNIQUEIDENTIFIER NULL,
    [Usage_UpdatedOn]          DATETIME         NULL,
    [Usage_Status]             BIT              NOT NULL,
    [Usage_Threshold]          FLOAT (53)       CONSTRAINT [DF_Usage_Usage_Threshold] DEFAULT ((10.0)) NOT NULL,
    [Usage_CanGenerateInvoice] BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK__Usage__F4483462145C0A3F] PRIMARY KEY CLUSTERED ([Usage_Code] ASC),
    CONSTRAINT [FK_Usage_UsageType] FOREIGN KEY ([Usage_UsageTypeCode]) REFERENCES [dbo].[UsageType] ([UsageType_Code])
);


GO
CREATE NONCLUSTERED INDEX [Ind_Usage_ModuleCode]
    ON [dbo].[Usage]([Usage_ModuleCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_Usage_UsageTypeCode]
    ON [dbo].[Usage]([Usage_UsageTypeCode] ASC);

