﻿CREATE TABLE [dbo].[UsageType] (
    [UsageType_Code]      UNIQUEIDENTIFIER NOT NULL,
    [UsageType_Name]      NVARCHAR (255)   NOT NULL,
    [UsageType_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [UsageType_CreatedOn] DATETIME         NOT NULL,
    [UsageType_UpdatedOn] DATETIME         NULL,
    [UsageType_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [UsageType_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_UsageType] PRIMARY KEY CLUSTERED ([UsageType_Code] ASC)
);

