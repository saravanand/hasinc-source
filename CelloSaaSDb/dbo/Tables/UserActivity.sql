﻿CREATE TABLE [dbo].[UserActivity] (
    [UserActivity_Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF_UserActivity_UserActivity_Id] DEFAULT (newsequentialid()) NOT NULL,
    [UserActivity_UserId]                UNIQUEIDENTIFIER NOT NULL,
    [UserActivity_LastActivityTimeStamp] DATETIME         NOT NULL,
    CONSTRAINT [PK_UserActivity] PRIMARY KEY CLUSTERED ([UserActivity_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_UserActivity]
    ON [dbo].[UserActivity]([UserActivity_UserId] ASC);

