﻿CREATE TABLE [dbo].[UserConsent] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [ClientId]                UNIQUEIDENTIFIER NOT NULL,
    [SubjectId]               UNIQUEIDENTIFIER NULL,
    [TenantId]                UNIQUEIDENTIFIER NOT NULL,
    [IsConsentStorageAllowed] BIT              NOT NULL,
    [ConsentedScopes]         NVARCHAR (MAX)   NOT NULL,
    [CreatedBy]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]               DATETIME         NOT NULL,
    [UpdatedOn]               DATETIME         NULL,
    [UpdatedBy]               UNIQUEIDENTIFIER NULL,
    [Status]                  BIT              NOT NULL,
    [RememberConsent]         BIT              NOT NULL,
    [WasConsentGranted]       BIT              NOT NULL,
    CONSTRAINT [PK_UserConsent] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserConsent_ClientDetails] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientDetails] ([Id]),
    CONSTRAINT [FK_UserConsent_SubjectDetails] FOREIGN KEY ([SubjectId]) REFERENCES [dbo].[SubjectDetails] ([Id]),
    CONSTRAINT [FK_UserConsent_TenantDetails] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code])
);

