﻿CREATE TABLE [dbo].[UserDetails] (
    [User_UserID]       UNIQUEIDENTIFIER CONSTRAINT [DF_UserDetails_User_UserID] DEFAULT (newsequentialid()) NOT NULL,
    [User_MembershipID] UNIQUEIDENTIFIER NOT NULL,
    [User_FirstName]    NVARCHAR (200)   NULL,
    [User_LastName]     NVARCHAR (200)   NULL,
    [User_AddressID]    UNIQUEIDENTIFIER NULL,
    [User_CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [User_CreatedOn]    DATETIME         NOT NULL,
    [User_UpdatedOn]    DATETIME         NULL,
    [User_UpdatedBY]    UNIQUEIDENTIFIER NULL,
    [User_Status]       BIT              NOT NULL,
    [User_Description]  NVARCHAR (MAX)   NULL,
    [User_TenantId]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED ([User_UserID] ASC),
    CONSTRAINT [FK_UserDetails_Address] FOREIGN KEY ([User_AddressID]) REFERENCES [dbo].[Address] ([Address_ID]),
    CONSTRAINT [FK_UserDetails_Membership] FOREIGN KEY ([User_MembershipID]) REFERENCES [dbo].[Membership] ([Membership_ID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_User_FirstName]
    ON [dbo].[UserDetails]([User_FirstName] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_User_LastName]
    ON [dbo].[UserDetails]([User_LastName] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_User_MembershipID]
    ON [dbo].[UserDetails]([User_MembershipID] ASC);

