﻿CREATE TABLE [dbo].[UserEntityOtherPermission] (
    [OtherPermissionId]      UNIQUEIDENTIFIER CONSTRAINT [DF_UserEntityOtherPermission_OtherPermissionId] DEFAULT (newsequentialid()) NOT NULL,
    [UserEntityPermissionId] UNIQUEIDENTIFIER NOT NULL,
    [Privilege]              VARCHAR (100)    NOT NULL,
    [CreatedBy]              UNIQUEIDENTIFIER NULL,
    [CreatedOn]              DATETIME         NULL,
    [UpdatedBy]              UNIQUEIDENTIFIER NULL,
    [UpdatedOn]              DATETIME         NULL,
    [Status]                 BIT              NOT NULL,
    CONSTRAINT [PK_UserEntityOtherPermission] PRIMARY KEY CLUSTERED ([OtherPermissionId] ASC),
    CONSTRAINT [FK_UserEntityOtherPermission_UserEntityPermission] FOREIGN KEY ([UserEntityPermissionId]) REFERENCES [dbo].[UserEntityPermission] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityOtherPermission_Privilege]
    ON [dbo].[UserEntityOtherPermission]([Privilege] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityOtherPermission_UserEntityPermissionId]
    ON [dbo].[UserEntityOtherPermission]([UserEntityPermissionId] ASC);

