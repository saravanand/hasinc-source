﻿CREATE TABLE [dbo].[UserEntityPermission] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_UserEntityPermission_Id] DEFAULT (newsequentialid()) NOT NULL,
    [EntityId]         VARCHAR (100)    NULL,
    [ReferenceId]      UNIQUEIDENTIFIER NULL,
    [ReferenceName]    NVARCHAR (255)   NULL,
    [FieldIdentifier]  VARCHAR (255)    NULL,
    [UserId]           UNIQUEIDENTIFIER NULL,
    [RoleId]           NVARCHAR (255)   NULL,
    [TenantId]         UNIQUEIDENTIFIER NOT NULL,
    [ViewPermission]   BIT              NULL,
    [AddPermission]    BIT              NULL,
    [EditPermission]   BIT              NULL,
    [DeletePermission] BIT              NULL,
    [CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME         NOT NULL,
    [UpdatedBy]        UNIQUEIDENTIFIER NULL,
    [UpdatedOn]        DATETIME         NULL,
    [Status]           BIT              NOT NULL,
    CONSTRAINT [PK_UserEntityPermission] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityPermission_EntityId]
    ON [dbo].[UserEntityPermission]([EntityId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityPermission_FieldIdentifier]
    ON [dbo].[UserEntityPermission]([FieldIdentifier] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityPermission_ReferenceId]
    ON [dbo].[UserEntityPermission]([ReferenceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityPermission_RoleId]
    ON [dbo].[UserEntityPermission]([RoleId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityPermission_TenantId]
    ON [dbo].[UserEntityPermission]([TenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserEntityPermission_UserId]
    ON [dbo].[UserEntityPermission]([UserId] ASC);

