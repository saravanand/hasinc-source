﻿CREATE TABLE [dbo].[UserPasswordExpiration] (
    [Expiration_Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF_UserPasswordExpiration_Expiration_Id] DEFAULT (newsequentialid()) NOT NULL,
    [Expiration_Membership_Id]       UNIQUEIDENTIFIER NOT NULL,
    [Expiration_ExpirationDate]      DATE             NOT NULL,
    [Expiration_PasswordChangedDate] DATETIME         NULL,
    [Expiration_IsExpired]           BIT              NOT NULL,
    [Expiration_CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [Expiration_CreatedOn]           DATETIME         NOT NULL,
    [Expiration_UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [Expiration_UpdatedOn]           DATETIME         NULL,
    [Expiration_Status]              BIT              NOT NULL,
    CONSTRAINT [PK_UserPwdExpiration] PRIMARY KEY CLUSTERED ([Expiration_Id] ASC),
    CONSTRAINT [FK_UserPasswordExpiration_Membership] FOREIGN KEY ([Expiration_Membership_Id]) REFERENCES [dbo].[Membership] ([Membership_ID])
);

