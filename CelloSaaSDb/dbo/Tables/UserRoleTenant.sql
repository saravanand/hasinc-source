﻿CREATE TABLE [dbo].[UserRoleTenant] (
    [UserRoleTenant_Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF_UserRoleTenant_UserRoleTenant_Id] DEFAULT (newsequentialid()) NOT NULL,
    [UserRoleTenant_UserRoleId]            UNIQUEIDENTIFIER NOT NULL,
    [UserRoleTenant_StratifiedTenantId]    UNIQUEIDENTIFIER NOT NULL,
    [UserRoleTenant_CreatedBy]             UNIQUEIDENTIFIER NOT NULL,
    [UserRoleTenant_CreatedOn]             DATETIME         NOT NULL,
    [UserRoleTenant_UpdatedBy]             UNIQUEIDENTIFIER NULL,
    [UserRoleTenant_UpdatedOn]             DATETIME         NULL,
    [UserRoleTenant_Status]                BIT              NOT NULL,
    [UserRoleTenant_RoleAssigningTenantId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_UserRoleTenant] PRIMARY KEY CLUSTERED ([UserRoleTenant_Id] ASC),
    CONSTRAINT [FK_UserRoleTenant_TenantDetails] FOREIGN KEY ([UserRoleTenant_StratifiedTenantId]) REFERENCES [dbo].[TenantDetails] ([Tenant_Code]),
    CONSTRAINT [FK_UserRoleTenant_UserRoles] FOREIGN KEY ([UserRoleTenant_UserRoleId]) REFERENCES [dbo].[UserRoles] ([UserRole_ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AssignedTenantId]
    ON [dbo].[UserRoleTenant]([UserRoleTenant_RoleAssigningTenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_StratifiedTenantIndex]
    ON [dbo].[UserRoleTenant]([UserRoleTenant_StratifiedTenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_UserRoleIdStatus]
    ON [dbo].[UserRoleTenant]([UserRoleTenant_UserRoleId] ASC, [UserRoleTenant_Status] ASC);

