﻿CREATE TABLE [dbo].[UserRoles] (
    [UserRole_ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_UserRoles_UserRole_ID] DEFAULT (newsequentialid()) NOT NULL,
    [UserRole_UserID]    UNIQUEIDENTIFIER NOT NULL,
    [UserRole_RoleID]    NVARCHAR (255)   NOT NULL,
    [UserRole_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [UserRole_CreatedOn] DATETIME         NOT NULL,
    [UserRole_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [UserRole_UpdatedOn] DATETIME         NULL,
    [UserRole_Status]    BIT              NOT NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED ([UserRole_ID] ASC),
    CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY ([UserRole_RoleID]) REFERENCES [dbo].[Roles] ([Role_ID]),
    CONSTRAINT [FK_UserRoles_UserDetails] FOREIGN KEY ([UserRole_UserID]) REFERENCES [dbo].[UserDetails] ([User_UserID])
);


GO
CREATE NONCLUSTERED INDEX [Ind_UserRole_RoleID]
    ON [dbo].[UserRoles]([UserRole_RoleID] ASC);


GO
CREATE NONCLUSTERED INDEX [Ind_UserRole_UserID]
    ON [dbo].[UserRoles]([UserRole_UserID] ASC);

