﻿CREATE TABLE [dbo].[UserService] (
    [UserService_Id]          UNIQUEIDENTIFIER CONSTRAINT [DF_UserService_UserService_Id] DEFAULT (newsequentialid()) NOT NULL,
    [UserService_UserId]      UNIQUEIDENTIFIER NOT NULL,
    [UserService_ServiceCode] NVARCHAR (255)   NOT NULL,
    [UserService_CreatedBy]   UNIQUEIDENTIFIER NOT NULL,
    [UserService_CreatedOn]   DATETIME         NOT NULL,
    [UserService_UpdatedBy]   UNIQUEIDENTIFIER NULL,
    [UserService_UpdatedOn]   DATETIME         NULL,
    [UserService_Status]      BIT              NOT NULL,
    CONSTRAINT [PK_UserService] PRIMARY KEY CLUSTERED ([UserService_Id] ASC),
    CONSTRAINT [FK_UserService_Services] FOREIGN KEY ([UserService_ServiceCode]) REFERENCES [dbo].[Services] ([Service_Code]),
    CONSTRAINT [FK_UserService_UserDetails] FOREIGN KEY ([UserService_UserId]) REFERENCES [dbo].[UserDetails] ([User_UserID])
);

