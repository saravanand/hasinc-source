﻿CREATE TABLE [dbo].[UserSetting] (
    [UserSetting_UserID]         UNIQUEIDENTIFIER NOT NULL,
    [UserSetting_AttributeID]    NVARCHAR (255)   NOT NULL,
    [UserSetting_AttributeValue] NVARCHAR (255)   NOT NULL,
    [UserSetting_CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [UserSetting_CreatedOn]      DATETIME         NOT NULL,
    [UserSetting_UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [UserSetting_UpdatedOn]      DATETIME         NULL,
    [UserSetting_Status]         BIT              NOT NULL,
    CONSTRAINT [PK_UserSetting] PRIMARY KEY CLUSTERED ([UserSetting_UserID] ASC, [UserSetting_AttributeID] ASC),
    CONSTRAINT [FK_UserSetting_SettingsAttribute] FOREIGN KEY ([UserSetting_AttributeID]) REFERENCES [dbo].[SettingsAttribute] ([SettingsAttribute_ID]),
    CONSTRAINT [FK_UserSetting_UserDetails] FOREIGN KEY ([UserSetting_UserID]) REFERENCES [dbo].[UserDetails] ([User_UserID])
);

