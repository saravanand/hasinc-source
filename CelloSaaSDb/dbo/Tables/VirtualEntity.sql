﻿CREATE TABLE [dbo].[VirtualEntity] (
    [VirtualEntity_Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_VirtualEntity_Reference_Id] DEFAULT (newsequentialid()) NOT NULL,
    [VirtualEntity_EntityId]  NVARCHAR (255)   NOT NULL,
    [VirtualEntity_TenantId]  UNIQUEIDENTIFIER NOT NULL,
    [VirtualEntity_CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [VirtualEntity_CreatedOn] DATETIME         CONSTRAINT [DF_VirtualEntity_Created_On] DEFAULT (getdate()) NOT NULL,
    [VirtualEntity_UpdatedBy] UNIQUEIDENTIFIER NULL,
    [VirtualEntity_UpdatedOn] DATETIME         NULL,
    [VirtualEntity_Status]    BIT              CONSTRAINT [DF_VirtualEntity_Status] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_VirtualEntity] PRIMARY KEY CLUSTERED ([VirtualEntity_Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [EntityIdIndex]
    ON [dbo].[VirtualEntity]([VirtualEntity_EntityId] ASC);


GO
CREATE NONCLUSTERED INDEX [TenantIdIndex]
    ON [dbo].[VirtualEntity]([VirtualEntity_TenantId] ASC);

