﻿CREATE TABLE [dbo].[WFDefinition] (
    [WfDefinitionId]     UNIQUEIDENTIFIER NOT NULL,
    [WfId]               UNIQUEIDENTIFIER NOT NULL,
    [TenantId]           UNIQUEIDENTIFIER NULL,
    [DefinitionXml]      NVARCHAR (MAX)   NOT NULL,
    [Version]            INT              NULL,
    [WfDefinitionStatus] VARCHAR (50)     NOT NULL,
    [CreatedOn]          DATETIME         NOT NULL,
    [CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]          DATETIME         NULL,
    [UpdatedBy]          UNIQUEIDENTIFIER NULL,
    [Status]             BIT              NOT NULL,
    [FilterId]           NVARCHAR (255)   NULL,
    CONSTRAINT [PK_WFDefinition] PRIMARY KEY CLUSTERED ([WfDefinitionId] ASC),
    CONSTRAINT [FK_WFDefinition_WorkFlow] FOREIGN KEY ([WfId]) REFERENCES [dbo].[WorkFlow] ([WfId])
);

