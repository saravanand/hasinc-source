﻿CREATE TABLE [dbo].[WFDefinitionUI] (
    [WFDefinitionId]  UNIQUEIDENTIFIER NULL,
    [UIDefinitionXml] XML              NULL,
    CONSTRAINT [FK_WFDefinitionUI_WFDefinition] FOREIGN KEY ([WFDefinitionId]) REFERENCES [dbo].[WFDefinition] ([WfDefinitionId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_WFDefinitionUI]
    ON [dbo].[WFDefinitionUI]([WFDefinitionId] ASC);

