﻿CREATE TABLE [dbo].[WFDynamicTaskActor] (
    [WFInstanceId] UNIQUEIDENTIFIER NOT NULL,
    [TaskCode]     NVARCHAR (255)   NOT NULL,
    [ActorId]      VARCHAR (100)    NOT NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]    DATETIME         NOT NULL,
    [UpdatedBy]    UNIQUEIDENTIFIER NULL,
    [UpdatedOn]    DATETIME         NULL,
    [Status]       BIT              NOT NULL,
    CONSTRAINT [FK_WFDynamicTaskActor_WFInstance] FOREIGN KEY ([WFInstanceId]) REFERENCES [dbo].[WFInstance] ([WfInstanceId])
);


GO
CREATE NONCLUSTERED INDEX [IX_WFDynamicTaskActor_ACtorId]
    ON [dbo].[WFDynamicTaskActor]([ActorId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFDynamicTaskActor_TaskCode]
    ON [dbo].[WFDynamicTaskActor]([TaskCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFDynamicTaskActor_wfinstanceid]
    ON [dbo].[WFDynamicTaskActor]([WFInstanceId] ASC);

