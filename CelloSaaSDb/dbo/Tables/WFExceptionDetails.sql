﻿CREATE TABLE [dbo].[WFExceptionDetails] (
    [WFExceptionId]    UNIQUEIDENTIFIER NOT NULL,
    [WFInstanceId]     UNIQUEIDENTIFIER NOT NULL,
    [WFTaskInstanceId] UNIQUEIDENTIFIER NOT NULL,
    [ExceptionMessage] VARCHAR (MAX)    NULL,
    [ExceptionDetails] VARCHAR (MAX)    NULL,
    [DateTime]         DATETIME         NULL,
    CONSTRAINT [PK_WFExceptionDetails] PRIMARY KEY CLUSTERED ([WFExceptionId] ASC)
);

