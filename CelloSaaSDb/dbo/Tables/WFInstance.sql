﻿CREATE TABLE [dbo].[WFInstance] (
    [WfInstanceId]   UNIQUEIDENTIFIER NOT NULL,
    [WfDefinitionId] UNIQUEIDENTIFIER NOT NULL,
    [InstanceStatus] VARCHAR (100)    NOT NULL,
    [WfId]           UNIQUEIDENTIFIER NULL,
    [WfName]         NVARCHAR (255)   NOT NULL,
    [MapId]          NVARCHAR (255)   NOT NULL,
    [CreatedOn]      DATETIME         NOT NULL,
    [CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]      DATETIME         NULL,
    [UpdatedBy]      UNIQUEIDENTIFIER NULL,
    [Status]         BIT              NOT NULL,
    [TenantId]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_WFInstance] PRIMARY KEY CLUSTERED ([WfInstanceId] ASC),
    CONSTRAINT [FK_WFInstance_WFDefinition] FOREIGN KEY ([WfDefinitionId]) REFERENCES [dbo].[WFDefinition] ([WfDefinitionId]),
    CONSTRAINT [FK_WFInstance_WorkFlow] FOREIGN KEY ([WfId]) REFERENCES [dbo].[WorkFlow] ([WfId])
);


GO
CREATE NONCLUSTERED INDEX [IX_WFInstance]
    ON [dbo].[WFInstance]([TenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFInstance_WfName]
    ON [dbo].[WFInstance]([WfName] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFinstanceMapId]
    ON [dbo].[WFInstance]([MapId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFInstanceStatus]
    ON [dbo].[WFInstance]([InstanceStatus] ASC);

