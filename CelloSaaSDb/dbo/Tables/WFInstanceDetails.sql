﻿CREATE TABLE [dbo].[WFInstanceDetails] (
    [WFInstanceId] UNIQUEIDENTIFIER NOT NULL,
    [Input]        NVARCHAR (MAX)   NULL,
    [WFState]      VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_WFInstanceDetails] PRIMARY KEY CLUSTERED ([WFInstanceId] ASC),
    CONSTRAINT [FK_WFInstanceDetails_WFInstance] FOREIGN KEY ([WFInstanceId]) REFERENCES [dbo].[WFInstance] ([WfInstanceId])
);

