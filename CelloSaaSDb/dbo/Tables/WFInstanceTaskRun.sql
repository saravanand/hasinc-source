﻿CREATE TABLE [dbo].[WFInstanceTaskRun] (
    [WfInstanceTaskRunId]           UNIQUEIDENTIFIER NOT NULL,
    [WfInstanceId]                  UNIQUEIDENTIFIER NOT NULL,
    [WfTaskDefinitionId]            UNIQUEIDENTIFIER NOT NULL,
    [TaskCode]                      NVARCHAR (255)   NOT NULL,
    [CurrentExecutionStatus]        VARCHAR (50)     NOT NULL,
    [ActorId]                       VARCHAR (50)     NULL,
    [DateTimeUpdated]               DATETIME         NULL,
    [DateTimeStarted]               DATETIME         NULL,
    [DateTimeCompleted]             DATETIME         NULL,
    [DateTimeExpired]               DATETIME         NULL,
    [InvokedTaskDefinitionRouterId] UNIQUEIDENTIFIER NULL,
    [TaskType]                      VARCHAR (50)     NULL,
    [ExecutionOrder]                INT              CONSTRAINT [DF_WFInstanceTaskRun_ExecutionOrder] DEFAULT ((0)) NOT NULL,
    [CreatedOn]                     DATETIME         NOT NULL,
    [CreatedBy]                     UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]                     DATETIME         NULL,
    [UpdatedBy]                     UNIQUEIDENTIFIER NULL,
    [Status]                        BIT              NOT NULL,
    CONSTRAINT [PK_WFInstanceTaskRun] PRIMARY KEY CLUSTERED ([WfInstanceTaskRunId] ASC),
    CONSTRAINT [FK_WFInstanceTaskRun_WFInstance] FOREIGN KEY ([WfInstanceId]) REFERENCES [dbo].[WFInstance] ([WfInstanceId]),
    CONSTRAINT [FK_WFInstanceTaskRun_WFInstanceTaskRun] FOREIGN KEY ([WfInstanceTaskRunId]) REFERENCES [dbo].[WFInstanceTaskRun] ([WfInstanceTaskRunId])
);


GO
CREATE NONCLUSTERED INDEX [IX_WFInstanceId]
    ON [dbo].[WFInstanceTaskRun]([WfInstanceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTaskCode]
    ON [dbo].[WFInstanceTaskRun]([TaskCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTaskCurrentStatus]
    ON [dbo].[WFInstanceTaskRun]([CurrentExecutionStatus] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTaskDefId]
    ON [dbo].[WFInstanceTaskRun]([WfTaskDefinitionId] ASC);

