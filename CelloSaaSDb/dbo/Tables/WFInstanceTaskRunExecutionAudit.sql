﻿CREATE TABLE [dbo].[WFInstanceTaskRunExecutionAudit] (
    [WfInstanceTaskRunId] UNIQUEIDENTIFIER NOT NULL,
    [ExecutionStatus]     VARCHAR (100)    NOT NULL,
    [DateTime]            DATETIME         NOT NULL,
    [CreatedOn]           DATETIME         NOT NULL,
    [CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]           DATETIME         NULL,
    [UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [Status]              BIT              NOT NULL,
    CONSTRAINT [FK_WFInstanceTaskRunExecutionAudit_WFInstanceTaskRun] FOREIGN KEY ([WfInstanceTaskRunId]) REFERENCES [dbo].[WFInstanceTaskRun] ([WfInstanceTaskRunId])
);


GO
CREATE NONCLUSTERED INDEX [IX_WFInstanceTaskRunExecutionAudit]
    ON [dbo].[WFInstanceTaskRunExecutionAudit]([WfInstanceTaskRunId] ASC);

