﻿CREATE TABLE [dbo].[WFInstanceTaskRunOutcomes] (
    [WfInstanceTaskRunId] UNIQUEIDENTIFIER NOT NULL,
    [Outcome]             NVARCHAR (255)   NOT NULL,
    [DateTime]            DATETIME         NOT NULL,
    [CreatedOn]           DATETIME         NOT NULL,
    [CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]           DATETIME         NULL,
    [UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [Status]              BIT              NOT NULL,
    CONSTRAINT [FK_WFInstanceTaskRunOutcomes_WFInstanceTaskRun] FOREIGN KEY ([WfInstanceTaskRunId]) REFERENCES [dbo].[WFInstanceTaskRun] ([WfInstanceTaskRunId])
);


GO
CREATE NONCLUSTERED INDEX [IX_WFInstanceTaskRunOutcomes]
    ON [dbo].[WFInstanceTaskRunOutcomes]([WfInstanceTaskRunId] ASC);

