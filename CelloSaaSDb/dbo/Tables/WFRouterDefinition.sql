﻿CREATE TABLE [dbo].[WFRouterDefinition] (
    [WFRouterDefinitionId] UNIQUEIDENTIFIER NOT NULL,
    [WfDefinitionId]       UNIQUEIDENTIFIER NOT NULL,
    [RouterName]           NVARCHAR (255)   NOT NULL,
    [CreatedOn]            DATETIME         NOT NULL,
    [CreatedBy]            UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]            DATETIME         NULL,
    [UpdatedBy]            UNIQUEIDENTIFIER NULL,
    [Status]               BIT              NOT NULL,
    CONSTRAINT [FK_WFDefinitionRouters_WFDefinition] FOREIGN KEY ([WfDefinitionId]) REFERENCES [dbo].[WFDefinition] ([WfDefinitionId]) ON DELETE CASCADE
);

