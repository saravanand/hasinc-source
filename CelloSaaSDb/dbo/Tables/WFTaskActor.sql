﻿CREATE TABLE [dbo].[WFTaskActor] (
    [WfInstanceTaskRunId] UNIQUEIDENTIFIER NOT NULL,
    [ActorId]             VARCHAR (100)    NOT NULL,
    [CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]           DATETIME         NOT NULL,
    [UpdatedBy]           UNIQUEIDENTIFIER NULL,
    [UpdatedOn]           DATETIME         NULL,
    [Status]              BIT              NOT NULL,
    CONSTRAINT [FK_WFTaskActor_WFInstanceTaskRun] FOREIGN KEY ([WfInstanceTaskRunId]) REFERENCES [dbo].[WFInstanceTaskRun] ([WfInstanceTaskRunId])
);


GO
CREATE NONCLUSTERED INDEX [IX_WFTaskActor]
    ON [dbo].[WFTaskActor]([WfInstanceTaskRunId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTaskActor_1]
    ON [dbo].[WFTaskActor]([ActorId] ASC);

