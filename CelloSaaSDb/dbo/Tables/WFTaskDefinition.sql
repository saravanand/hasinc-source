﻿CREATE TABLE [dbo].[WFTaskDefinition] (
    [WFTaskDefinitionId] UNIQUEIDENTIFIER NOT NULL,
    [WfDefinitionId]     UNIQUEIDENTIFIER NOT NULL,
    [TaskName]           NVARCHAR (255)   NOT NULL,
    [TaskCode]           NVARCHAR (255)   NOT NULL,
    [CreatedOn]          DATETIME         NOT NULL,
    [CreatedBy]          UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]          DATETIME         NULL,
    [UpdatedBy]          UNIQUEIDENTIFIER NULL,
    [Status]             BIT              NOT NULL,
    [TaskType]           VARCHAR (50)     NULL,
    CONSTRAINT [FK_WFDefinitionTasks_WFDefinition] FOREIGN KEY ([WfDefinitionId]) REFERENCES [dbo].[WFDefinition] ([WfDefinitionId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_WFDefinitionId]
    ON [dbo].[WFTaskDefinition]([WfDefinitionId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTaskCode]
    ON [dbo].[WFTaskDefinition]([TaskCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTaskDefinition]
    ON [dbo].[WFTaskDefinition]([WFTaskDefinitionId] ASC);

