﻿CREATE TABLE [dbo].[WFTrackAudit] (
    [Id]                  UNIQUEIDENTIFIER CONSTRAINT [DF_WFTrackAudit_Id] DEFAULT (newsequentialid()) NOT NULL,
    [WFTaskInstanceRunId] UNIQUEIDENTIFIER NOT NULL,
    [PreviousStatus]      VARCHAR (50)     NULL,
    [CurrentStatus]       VARCHAR (50)     NOT NULL,
    [DateTime]            DATETIME         CONSTRAINT [DF_WFTrackAudit_DateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_WFTrackAudit] PRIMARY KEY CLUSTERED ([Id] ASC)
);

