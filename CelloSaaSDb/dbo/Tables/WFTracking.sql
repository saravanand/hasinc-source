﻿CREATE TABLE [dbo].[WFTracking] (
    [WFTaskInstanceRunId] UNIQUEIDENTIFIER NOT NULL,
    [WFInstanceId]        UNIQUEIDENTIFIER NOT NULL,
    [Status]              VARCHAR (50)     NOT NULL,
    [DateTime]            DATETIME         CONSTRAINT [DF_WFTracking_DateTime] DEFAULT (getdate()) NOT NULL,
    [MapId]               NVARCHAR (255)   NOT NULL,
    [TrackingId]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [FK_WFTracking_WFInstance] FOREIGN KEY ([WFInstanceId]) REFERENCES [dbo].[WFInstance] ([WfInstanceId])
);


GO
CREATE NONCLUSTERED INDEX [IX_WFTracking_Status]
    ON [dbo].[WFTracking]([Status] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTracking_WFInstanceId]
    ON [dbo].[WFTracking]([WFInstanceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WFTracking_WFTaskInstanceRunId]
    ON [dbo].[WFTracking]([WFTaskInstanceRunId] ASC);

