﻿CREATE TABLE [dbo].[WorkFlow] (
    [WfId]              UNIQUEIDENTIFIER CONSTRAINT [DF_WorkFlow_WfId] DEFAULT (newsequentialid()) NOT NULL,
    [Name]              NVARCHAR (255)   NOT NULL,
    [Description]       NVARCHAR (MAX)   NOT NULL,
    [CreatedOn]         DATETIME         NOT NULL,
    [CreatedBy]         UNIQUEIDENTIFIER NOT NULL,
    [UpdatedOn]         DATETIME         NULL,
    [UpdatedBy]         UNIQUEIDENTIFIER NULL,
    [Status]            BIT              NOT NULL,
    [WFInputFinderType] VARCHAR (MAX)    NULL,
    [TenantId]          UNIQUEIDENTIFIER NULL,
    [CategoryId]        NVARCHAR (255)   NULL,
    [WFXmlTransform]    NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_WorkFlow] PRIMARY KEY CLUSTERED ([WfId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_WFName]
    ON [dbo].[WorkFlow]([Name] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Workflow Input object finder type which inherits IWorkflowInputFider interface. This will return the IWorkflowInput object for the given mapIds.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WorkFlow', @level2type = N'COLUMN', @level2name = N'WFInputFinderType';

