﻿CREATE TABLE [dbo].[WorkflowRequest] (
    [Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_WorkflowRequest_Id] DEFAULT (newsequentialid()) NOT NULL,
    [TenantId]  UNIQUEIDENTIFIER NOT NULL,
    [WfId]      UNIQUEIDENTIFIER NOT NULL,
    [MapId]     NVARCHAR (255)   NOT NULL,
    [Status]    VARCHAR (50)     NOT NULL,
    [CreatedBy] UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn] DATETIME         NOT NULL,
    [UpdatedBy] UNIQUEIDENTIFIER NULL,
    [UpdatedOn] DATETIME         NULL,
    CONSTRAINT [PK_WRId] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_WRMapId]
    ON [dbo].[WorkflowRequest]([MapId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WRTenantId]
    ON [dbo].[WorkflowRequest]([TenantId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WRWFId]
    ON [dbo].[WorkflowRequest]([WfId] ASC);

