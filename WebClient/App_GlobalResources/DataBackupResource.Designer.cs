//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class DataBackupResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DataBackupResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.DataBackupResource", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request cannot be canceled!.
        /// </summary>
        internal static string e_CancelRequest {
            get {
                return ResourceManager.GetString("e_CancelRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose a Primary Tenant.
        /// </summary>
        internal static string e_ChoosePrimaryTenant {
            get {
                return ResourceManager.GetString("e_ChoosePrimaryTenant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid Request Id!.
        /// </summary>
        internal static string e_RequestId {
            get {
                return ResourceManager.GetString("e_RequestId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request Id cannot be null or empty!.
        /// </summary>
        internal static string e_RequestIdNull {
            get {
                return ResourceManager.GetString("e_RequestIdNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select at-least one tenant!.
        /// </summary>
        internal static string e_SelectOneTenant {
            get {
                return ResourceManager.GetString("e_SelectOneTenant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid server address!.
        /// </summary>
        internal static string e_ServerAddress {
            get {
                return ResourceManager.GetString("e_ServerAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid database name!.
        /// </summary>
        internal static string e_ValidDatabase {
            get {
                return ResourceManager.GetString("e_ValidDatabase", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose a valid source database.
        /// </summary>
        internal static string e_ValidDataSource {
            get {
                return ResourceManager.GetString("e_ValidDataSource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid file path!.
        /// </summary>
        internal static string e_ValidFilePath {
            get {
                return ResourceManager.GetString("e_ValidFilePath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid ftp details!.
        /// </summary>
        internal static string e_ValidFtp {
            get {
                return ResourceManager.GetString("e_ValidFtp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid ftp password!.
        /// </summary>
        internal static string e_ValidFtpPassword {
            get {
                return ResourceManager.GetString("e_ValidFtpPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid ftp path!.
        /// </summary>
        internal static string e_ValidFtpPath {
            get {
                return ResourceManager.GetString("e_ValidFtpPath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid ftp server address!.
        /// </summary>
        internal static string e_ValidFtpServer {
            get {
                return ResourceManager.GetString("e_ValidFtpServer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid ftp user name!.
        /// </summary>
        internal static string e_ValidFtpUserName {
            get {
                return ResourceManager.GetString("e_ValidFtpUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid password!.
        /// </summary>
        internal static string e_ValidPassword {
            get {
                return ResourceManager.GetString("e_ValidPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid ftp port number! Should be between 1 to 65535..
        /// </summary>
        internal static string e_ValidPortNumber {
            get {
                return ResourceManager.GetString("e_ValidPortNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter a valid user name!.
        /// </summary>
        internal static string e_ValidUserName {
            get {
                return ResourceManager.GetString("e_ValidUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request cancelled successfully!.
        /// </summary>
        internal static string s_CancelRequest {
            get {
                return ResourceManager.GetString("s_CancelRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Backup request created successfully!.
        /// </summary>
        internal static string s_CreateBackupRequest {
            get {
                return ResourceManager.GetString("s_CreateBackupRequest", resourceCulture);
            }
        }
    }
}
