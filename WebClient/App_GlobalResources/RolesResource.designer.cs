//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class RolesResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RolesResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.RolesResource", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, you don&apos;t have the required access..
        /// </summary>
        internal static string e_AccessDenied {
            get {
                return ResourceManager.GetString("e_AccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in adding privilege.
        /// </summary>
        internal static string e_AddPrivilege {
            get {
                return ResourceManager.GetString("e_AddPrivilege", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while adding privileges to the role!.
        /// </summary>
        internal static string e_AddPrivilegeToRole {
            get {
                return ResourceManager.GetString("e_AddPrivilegeToRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select the data scope..
        /// </summary>
        internal static string e_DataScope {
            get {
                return ResourceManager.GetString("e_DataScope", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to display the data scope details due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_DataScopeLoad {
            get {
                return ResourceManager.GetString("e_DataScopeLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to update the data scope due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_DataScopeUpdate {
            get {
                return ResourceManager.GetString("e_DataScopeUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while deleting the privileges for the role!.
        /// </summary>
        internal static string e_DeletePrivilege {
            get {
                return ResourceManager.GetString("e_DeletePrivilege", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while deleting privileges!.
        /// </summary>
        internal static string e_DeletePrivileges {
            get {
                return ResourceManager.GetString("e_DeletePrivileges", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter the description..
        /// </summary>
        internal static string e_Description {
            get {
                return ResourceManager.GetString("e_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The role already exists..
        /// </summary>
        internal static string e_DuplicateRoleName {
            get {
                return ResourceManager.GetString("e_DuplicateRoleName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Feature identifier cannot be null or empty..
        /// </summary>
        internal static string e_FeatureIdMandatory {
            get {
                return ResourceManager.GetString("e_FeatureIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching feature details.
        /// </summary>
        internal static string e_FetchFeature {
            get {
                return ResourceManager.GetString("e_FetchFeature", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching role privilege details..
        /// </summary>
        internal static string e_FetchPrivilege {
            get {
                return ResourceManager.GetString("e_FetchPrivilege", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching privileges!.
        /// </summary>
        internal static string e_FetchPrivileges {
            get {
                return ResourceManager.GetString("e_FetchPrivileges", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching roles!.
        /// </summary>
        internal static string e_FetchRoles {
            get {
                return ResourceManager.GetString("e_FetchRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while getting tenant license..
        /// </summary>
        internal static string e_FetchTenantLicense {
            get {
                return ResourceManager.GetString("e_FetchTenantLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid request.
        /// </summary>
        internal static string e_InvalidRequest {
            get {
                return ResourceManager.GetString("e_InvalidRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Module identifier cannot be null or empty..
        /// </summary>
        internal static string e_ModuleIdMandatory {
            get {
                return ResourceManager.GetString("e_ModuleIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Roles Available for the tenant.
        /// </summary>
        internal static string e_NoRolesAvailable {
            get {
                return ResourceManager.GetString("e_NoRolesAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parameter value is null or empty..
        /// </summary>
        internal static string e_ParameterEmptyOrNull {
            get {
                return ResourceManager.GetString("e_ParameterEmptyOrNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to add the privileges due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_PrivilegeAdd {
            get {
                return ResourceManager.GetString("e_PrivilegeAdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to display the privileges and data scope due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_PrivilegeDataScopeLoad {
            get {
                return ResourceManager.GetString("e_PrivilegeDataScopeLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to delete the privileges due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_PrivilegeDelete {
            get {
                return ResourceManager.GetString("e_PrivilegeDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Privilege identifiers cannot be null or empty..
        /// </summary>
        internal static string e_PrivilegeIdMandatory {
            get {
                return ResourceManager.GetString("e_PrivilegeIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter  a privilege search string. .
        /// </summary>
        internal static string e_PrivilegeSearch {
            get {
                return ResourceManager.GetString("e_PrivilegeSearch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select any one of the privileges..
        /// </summary>
        internal static string e_PrivilegeSelect {
            get {
                return ResourceManager.GetString("e_PrivilegeSelect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to activate the role due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_RoleActivate {
            get {
                return ResourceManager.GetString("e_RoleActivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to create the role due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_RoleCreate {
            get {
                return ResourceManager.GetString("e_RoleCreate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to deactivate the role due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_RoleDelete {
            get {
                return ResourceManager.GetString("e_RoleDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This selected role has associated privileges..
        /// </summary>
        internal static string e_RoleDeleteHasPrivilege {
            get {
                return ResourceManager.GetString("e_RoleDeleteHasPrivilege", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role identifier cannot be null or empty..
        /// </summary>
        internal static string e_RoleIdMandatory {
            get {
                return ResourceManager.GetString("e_RoleIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to display the role due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_RoleLoad {
            get {
                return ResourceManager.GetString("e_RoleLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select a valid role name..
        /// </summary>
        internal static string e_RoleName {
            get {
                return ResourceManager.GetString("e_RoleName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RoleName Cannot be Empty.
        /// </summary>
        internal static string e_RoleNameEmpty {
            get {
                return ResourceManager.GetString("e_RoleNameEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter the role name..
        /// </summary>
        internal static string e_RoleNameMandatory {
            get {
                return ResourceManager.GetString("e_RoleNameMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to display the role priviliges due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_RolePrivilegeLoad {
            get {
                return ResourceManager.GetString("e_RolePrivilegeLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to update the role details due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com)  for immediate assistance..
        /// </summary>
        internal static string e_RoleUpdate {
            get {
                return ResourceManager.GetString("e_RoleUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while saving datascope value.
        /// </summary>
        internal static string e_SaveDatascopeValue {
            get {
                return ResourceManager.GetString("e_SaveDatascopeValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Should select at-least one service!.
        /// </summary>
        internal static string e_SelectOneService {
            get {
                return ResourceManager.GetString("e_SelectOneService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Should select at-least one service..
        /// </summary>
        internal static string e_ServiceMandatory {
            get {
                return ResourceManager.GetString("e_ServiceMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} privilege(s) added.
        /// </summary>
        internal static string s_AddPrivilege {
            get {
                return ResourceManager.GetString("s_AddPrivilege", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} roles copied from parent..
        /// </summary>
        internal static string s_CopySuccess {
            get {
                return ResourceManager.GetString("s_CopySuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Saved..
        /// </summary>
        internal static string s_DataSaved {
            get {
                return ResourceManager.GetString("s_DataSaved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The data scope details have been updated successfully..
        /// </summary>
        internal static string s_DataScopeUpdate {
            get {
                return ResourceManager.GetString("s_DataScopeUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data updated..
        /// </summary>
        internal static string s_DataUpdated {
            get {
                return ResourceManager.GetString("s_DataUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} privilege(s) deleted.
        /// </summary>
        internal static string s_DeletePrivilege {
            get {
                return ResourceManager.GetString("s_DeletePrivilege", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The privilege is added successfully..
        /// </summary>
        internal static string s_PrivilegeAdd {
            get {
                return ResourceManager.GetString("s_PrivilegeAdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The privilege is deleted..
        /// </summary>
        internal static string s_PrivilegeDelete {
            get {
                return ResourceManager.GetString("s_PrivilegeDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The role details are activated successfully..
        /// </summary>
        internal static string s_RoleActivate {
            get {
                return ResourceManager.GetString("s_RoleActivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The role is created successfully..
        /// </summary>
        internal static string s_RoleCreate {
            get {
                return ResourceManager.GetString("s_RoleCreate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The role details are deactivated..
        /// </summary>
        internal static string s_RoleDelete {
            get {
                return ResourceManager.GetString("s_RoleDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The role details are updated successfully..
        /// </summary>
        internal static string s_RoleUpdate {
            get {
                return ResourceManager.GetString("s_RoleUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saved successfully.
        /// </summary>
        internal static string s_Saved {
            get {
                return ResourceManager.GetString("s_Saved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Success.
        /// </summary>
        internal static string s_SuccessMessage {
            get {
                return ResourceManager.GetString("s_SuccessMessage", resourceCulture);
            }
        }
    }
}
