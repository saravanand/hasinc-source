//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class TemplateResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TemplateResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.TemplateResource", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access Denied.
        /// </summary>
        internal static string e_AccessDenied {
            get {
                return ResourceManager.GetString("e_AccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide a valid category.
        /// </summary>
        internal static string e_Category {
            get {
                return ResourceManager.GetString("e_Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while deleting the locale template..
        /// </summary>
        internal static string e_DeleteLocaleTemplate {
            get {
                return ResourceManager.GetString("e_DeleteLocaleTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template name already exists.
        /// </summary>
        internal static string e_DuplicateTemplateName {
            get {
                return ResourceManager.GetString("e_DuplicateTemplateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching locale template details..
        /// </summary>
        internal static string e_FetchLocaleTemplate {
            get {
                return ResourceManager.GetString("e_FetchLocaleTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching Template Details.
        /// </summary>
        internal static string e_FetchTemplate {
            get {
                return ResourceManager.GetString("e_FetchTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select locale name.
        /// </summary>
        internal static string e_LocaleIdMandatory {
            get {
                return ResourceManager.GetString("e_LocaleIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Master Data details are not fetched.
        /// </summary>
        internal static string e_MasterDataDetails_Fetch {
            get {
                return ResourceManager.GetString("e_MasterDataDetails_Fetch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parameter null or empty..
        /// </summary>
        internal static string e_ParameterEmptyOrNull {
            get {
                return ResourceManager.GetString("e_ParameterEmptyOrNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while saves the locale template details..
        /// </summary>
        internal static string e_SaveLocaletemplate {
            get {
                return ResourceManager.GetString("e_SaveLocaletemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while saves the template details..
        /// </summary>
        internal static string e_SaveTemplate {
            get {
                return ResourceManager.GetString("e_SaveTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid tempalteId  passed!..
        /// </summary>
        internal static string e_TemplateId {
            get {
                return ResourceManager.GetString("e_TemplateId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template name mandatory..
        /// </summary>
        internal static string e_TemplateName {
            get {
                return ResourceManager.GetString("e_TemplateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide a valid template name.
        /// </summary>
        internal static string e_TemplateNameInvalid {
            get {
                return ResourceManager.GetString("e_TemplateNameInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template is not available for this notification..
        /// </summary>
        internal static string e_TemplateNotAvailable {
            get {
                return ResourceManager.GetString("e_TemplateNotAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter the template content.
        /// </summary>
        internal static string e_TemplateText {
            get {
                return ResourceManager.GetString("e_TemplateText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ---Select---.
        /// </summary>
        internal static string select {
            get {
                return ResourceManager.GetString("select", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Locale template deleted successfully..
        /// </summary>
        internal static string s_DeleteLocaleTemplate {
            get {
                return ResourceManager.GetString("s_DeleteLocaleTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Locale Template saved successfully..
        /// </summary>
        internal static string s_SaveLocaleTemplate {
            get {
                return ResourceManager.GetString("s_SaveLocaleTemplate", resourceCulture);
            }
        }
    }
}
