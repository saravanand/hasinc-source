//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class UserResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.UserResource", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, you don&apos;t have the required access..
        /// </summary>
        internal static string e_AccessDenied {
            get {
                return ResourceManager.GetString("e_AccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are un-authorized to activate user details..
        /// </summary>
        internal static string e_ActivateAccessDenied {
            get {
                return ResourceManager.GetString("e_ActivateAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address is required.
        /// </summary>
        internal static string e_Address1 {
            get {
                return ResourceManager.GetString("e_Address1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while adding the role details..
        /// </summary>
        internal static string e_AddRole {
            get {
                return ResourceManager.GetString("e_AddRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while checking the access privileges..
        /// </summary>
        internal static string e_CheckTenantAccessPrivilege {
            get {
                return ResourceManager.GetString("e_CheckTenantAccessPrivilege", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City is required.
        /// </summary>
        internal static string e_City {
            get {
                return ResourceManager.GetString("e_City", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country is required.
        /// </summary>
        internal static string e_Country {
            get {
                return ResourceManager.GetString("e_Country", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to display the country details due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_CountryLoad {
            get {
                return ResourceManager.GetString("e_CountryLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are un-authorized to de-activate user details..
        /// </summary>
        internal static string e_DeactivateAccessDenied {
            get {
                return ResourceManager.GetString("e_DeactivateAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while deleting user role..
        /// </summary>
        internal static string e_DeleteRole {
            get {
                return ResourceManager.GetString("e_DeleteRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to delete the user details..
        /// </summary>
        internal static string e_DeleteUserDetails {
            get {
                return ResourceManager.GetString("e_DeleteUserDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This email id already exists.
        /// </summary>
        internal static string e_DuplicateEmailId {
            get {
                return ResourceManager.GetString("e_DuplicateEmailId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user is already linked..
        /// </summary>
        internal static string e_DuplicateLinkUser {
            get {
                return ResourceManager.GetString("e_DuplicateLinkUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user already exists..
        /// </summary>
        internal static string e_DuplicateUser {
            get {
                return ResourceManager.GetString("e_DuplicateUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, the user name already exists..
        /// </summary>
        internal static string e_DuplicateUserName {
            get {
                return ResourceManager.GetString("e_DuplicateUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid email id. Please enter your email address in the format someone@example.com.
        /// </summary>
        internal static string e_EmailId {
            get {
                return ResourceManager.GetString("e_EmailId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Empty Email Id.
        /// </summary>
        internal static string e_emptyEmail {
            get {
                return ResourceManager.GetString("e_emptyEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while getting the role details. .
        /// </summary>
        internal static string e_FetchRoleDetails {
            get {
                return ResourceManager.GetString("e_FetchRoleDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching the licence services..
        /// </summary>
        internal static string e_FetchService {
            get {
                return ResourceManager.GetString("e_FetchService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while getting the tenant details..
        /// </summary>
        internal static string e_FetchTenantDetails {
            get {
                return ResourceManager.GetString("e_FetchTenantDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while fetching tenant mappings..
        /// </summary>
        internal static string e_FetchTenantMapping {
            get {
                return ResourceManager.GetString("e_FetchTenantMapping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot fetch user details for this tenant..
        /// </summary>
        internal static string e_FetchUserDetails {
            get {
                return ResourceManager.GetString("e_FetchUserDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First name is required.
        /// </summary>
        internal static string e_FirstName {
            get {
                return ResourceManager.GetString("e_FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid data..
        /// </summary>
        internal static string e_InvalidData {
            get {
                return ResourceManager.GetString("e_InvalidData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last name is required.
        /// </summary>
        internal static string e_LastName {
            get {
                return ResourceManager.GetString("e_LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access denied to lock the user accounts..
        /// </summary>
        internal static string e_LockAccessDenied {
            get {
                return ResourceManager.GetString("e_LockAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access denied to lock all user accounts..
        /// </summary>
        internal static string e_LockAllAccessDenied {
            get {
                return ResourceManager.GetString("e_LockAllAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in locking all user accounts..
        /// </summary>
        internal static string e_LockAllUser {
            get {
                return ResourceManager.GetString("e_LockAllUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in locking user accounts..
        /// </summary>
        internal static string e_LockUser {
            get {
                return ResourceManager.GetString("e_LockUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Mode Email Id Cannot change.
        /// </summary>
        internal static string e_Nochange {
            get {
                return ResourceManager.GetString("e_Nochange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No data available.
        /// </summary>
        internal static string e_NoData {
            get {
                return ResourceManager.GetString("e_NoData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parameter value is null or empty..
        /// </summary>
        internal static string e_ParameterEmptyOrNull {
            get {
                return ResourceManager.GetString("e_ParameterEmptyOrNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access Denied to Force Password Expiry on User Accounts!.
        /// </summary>
        internal static string e_PasswordForceAccessDenied {
            get {
                return ResourceManager.GetString("e_PasswordForceAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in Force Password Expiry on User Accounts!.
        /// </summary>
        internal static string e_PasswordForceFail {
            get {
                return ResourceManager.GetString("e_PasswordForceFail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in resetting PIN for the given user accounts..
        /// </summary>
        internal static string e_ResetPin {
            get {
                return ResourceManager.GetString("e_ResetPin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access denied to reset PIN for the user accounts..
        /// </summary>
        internal static string e_ResetPinAccessDenied {
            get {
                return ResourceManager.GetString("e_ResetPinAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role id cannot be null or empty..
        /// </summary>
        internal static string e_RoleIdMandatory {
            get {
                return ResourceManager.GetString("e_RoleIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to display the role details due to some problems.
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_RoleLoad {
            get {
                return ResourceManager.GetString("e_RoleLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select at-least one role..
        /// </summary>
        internal static string e_RoleMandatory {
            get {
                return ResourceManager.GetString("e_RoleMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role not found..
        /// </summary>
        internal static string e_RoleNotFound {
            get {
                return ResourceManager.GetString("e_RoleNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tenant not found. Cannot save user details..
        /// </summary>
        internal static string e_SaveUser {
            get {
                return ResourceManager.GetString("e_SaveUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we were not able to send the confirmation mail to the new user. .
        /// </summary>
        internal static string e_SendMail {
            get {
                return ResourceManager.GetString("e_SendMail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The tenant to which the user belongs have disabled user sharing.
        /// </summary>
        internal static string e_SharingDisabled {
            get {
                return ResourceManager.GetString("e_SharingDisabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to State is required.
        /// </summary>
        internal static string e_State {
            get {
                return ResourceManager.GetString("e_State", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tenant identifier cannot be null or empty..
        /// </summary>
        internal static string e_TenantIdMandatory {
            get {
                return ResourceManager.GetString("e_TenantIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requested tenant not found..
        /// </summary>
        internal static string e_TenantNotFound {
            get {
                return ResourceManager.GetString("e_TenantNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access denied to un-lock the user accounts..
        /// </summary>
        internal static string e_UnlockAccessDenied {
            get {
                return ResourceManager.GetString("e_UnlockAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access denied to un-lock all user accounts..
        /// </summary>
        internal static string e_UnlockAllAccessDenied {
            get {
                return ResourceManager.GetString("e_UnlockAllAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in un-locking all user accounts..
        /// </summary>
        internal static string e_UnlockAllUser {
            get {
                return ResourceManager.GetString("e_UnlockAllUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in un-locking user accounts..
        /// </summary>
        internal static string e_UnlockUser {
            get {
                return ResourceManager.GetString("e_UnlockUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please unselect the selected tenant to delete the role..
        /// </summary>
        internal static string e_UnselectToDeleteRole {
            get {
                return ResourceManager.GetString("e_UnselectToDeleteRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in updating service mapping..
        /// </summary>
        internal static string e_UpdateServiceMapping {
            get {
                return ResourceManager.GetString("e_UpdateServiceMapping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in updating tenant mapping..
        /// </summary>
        internal static string e_UpdateTenantMapping {
            get {
                return ResourceManager.GetString("e_UpdateTenantMapping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You are un-authorized to update user details..
        /// </summary>
        internal static string e_UpdateUserAccessDenied {
            get {
                return ResourceManager.GetString("e_UpdateUserAccessDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error in approving user accounts..
        /// </summary>
        internal static string e_UserApproval {
            get {
                return ResourceManager.GetString("e_UserApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to create the user due to some problems. 
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_UserCreate {
            get {
                return ResourceManager.GetString("e_UserCreate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry! You cannot add a new user as you have exceeded the licensed limit..
        /// </summary>
        internal static string e_UserCreateLicenseOver {
            get {
                return ResourceManager.GetString("e_UserCreateLicenseOver", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to deactivate the user due to some problems. 
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_UserDelete {
            get {
                return ResourceManager.GetString("e_UserDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user description has more than 255 characters in input..
        /// </summary>
        internal static string e_UserDescription {
            get {
                return ResourceManager.GetString("e_UserDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Details are not available for this tenant.
        /// </summary>
        internal static string e_UserDetails {
            get {
                return ResourceManager.GetString("e_UserDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User identifier cannot be null or empty..
        /// </summary>
        internal static string e_UserIdMandatory {
            get {
                return ResourceManager.GetString("e_UserIdMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Some exception occured while sending out the invites to the users.
        /// </summary>
        internal static string e_UserInviteFailed {
            get {
                return ResourceManager.GetString("e_UserInviteFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to display the user details due to some problems. 
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_UserLoad {
            get {
                return ResourceManager.GetString("e_UserLoad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username is required.
        /// </summary>
        internal static string e_UserName {
            get {
                return ResourceManager.GetString("e_UserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username is requird.
        /// </summary>
        internal static string e_UserNameMandatory {
            get {
                return ResourceManager.GetString("e_UserNameMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User not found..
        /// </summary>
        internal static string e_UserNotFound {
            get {
                return ResourceManager.GetString("e_UserNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to add the role details due to some problems. 
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_UserRoleInsert {
            get {
                return ResourceManager.GetString("e_UserRoleInsert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to update the role details due to some problems. 
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_UserRoleUpdate {
            get {
                return ResourceManager.GetString("e_UserRoleUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, we are unable to update the user details due to some problems. 
        ///Please try again after sometime or contact our production support team (support@techcello.com) for immediate assistance..
        /// </summary>
        internal static string e_UserUpdate {
            get {
                return ResourceManager.GetString("e_UserUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid user name..
        /// </summary>
        internal static string e_ValidUserName {
            get {
                return ResourceManager.GetString("e_ValidUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user was already requested and waiting for your approval..
        /// </summary>
        internal static string e_WaitingForTenant {
            get {
                return ResourceManager.GetString("e_WaitingForTenant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This user was already requested and waiting for user approval..
        /// </summary>
        internal static string e_WaitingForUser {
            get {
                return ResourceManager.GetString("e_WaitingForUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zip code is required.
        /// </summary>
        internal static string e_Zipcode {
            get {
                return ResourceManager.GetString("e_Zipcode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zipcode should not contain any special letters.
        /// </summary>
        internal static string e_Zipcode_SpecialChars {
            get {
                return ResourceManager.GetString("e_Zipcode_SpecialChars", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user with this email already exists, you can link the user by clicking here.
        /// </summary>
        internal static string m_linkableUser {
            get {
                return ResourceManager.GetString("m_linkableUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user is in another tenant. You can link the user..
        /// </summary>
        internal static string m_LinkUser {
            get {
                return ResourceManager.GetString("m_LinkUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User role added..
        /// </summary>
        internal static string s_AddRole {
            get {
                return ResourceManager.GetString("s_AddRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User added successfully..
        /// </summary>
        internal static string s_AddUser {
            get {
                return ResourceManager.GetString("s_AddUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password Change Forced successfully!.
        /// </summary>
        internal static string s_PasswordForceSuccess {
            get {
                return ResourceManager.GetString("s_PasswordForceSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ({0}) - User Accounts PIN Changed!.
        /// </summary>
        internal static string s_PinReset {
            get {
                return ResourceManager.GetString("s_PinReset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User role removed..
        /// </summary>
        internal static string s_RemoveUserRole {
            get {
                return ResourceManager.GetString("s_RemoveUserRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service mapping saved..
        /// </summary>
        internal static string s_SaveServiceMapping {
            get {
                return ResourceManager.GetString("s_SaveServiceMapping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tenant mapping saved..
        /// </summary>
        internal static string s_SaveTenantMapping {
            get {
                return ResourceManager.GetString("s_SaveTenantMapping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User updated successfully..
        /// </summary>
        internal static string s_UpdateUser {
            get {
                return ResourceManager.GetString("s_UpdateUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user is activated successfully..
        /// </summary>
        internal static string s_UserActivate {
            get {
                return ResourceManager.GetString("s_UserActivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user details have been added successfully..
        /// </summary>
        internal static string s_UserCreate {
            get {
                return ResourceManager.GetString("s_UserCreate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user has been deactivated successfully. .
        /// </summary>
        internal static string s_UserDelete {
            get {
                return ResourceManager.GetString("s_UserDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ({0}) - User Accounts Locked!.
        /// </summary>
        internal static string s_UserLock {
            get {
                return ResourceManager.GetString("s_UserLock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ({0}) - User Accounts Approved!.
        /// </summary>
        internal static string s_UsersApproved {
            get {
                return ResourceManager.GetString("s_UsersApproved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The invitations for the users were sent successfully.
        /// </summary>
        internal static string s_UsersInvited {
            get {
                return ResourceManager.GetString("s_UsersInvited", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ({0}) - User Accounts Un-Locked!.
        /// </summary>
        internal static string s_UserUnLock {
            get {
                return ResourceManager.GetString("s_UserUnLock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user details have been updated successfully..
        /// </summary>
        internal static string s_UserUpdate {
            get {
                return ResourceManager.GetString("s_UserUpdate", resourceCulture);
            }
        }
    }
}
