//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18051
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class VirtualEntityResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal VirtualEntityResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.VirtualEntityResource", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to entity&apos;s data.
        /// </summary>
        internal static string EntityData {
            get {
                return ResourceManager.GetString("EntityData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permission to Add {0} data is denied..
        /// </summary>
        internal static string e_AddDenied {
            get {
                return ResourceManager.GetString("e_AddDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permission to Delete {0} data is denied..
        /// </summary>
        internal static string e_DeleteDenied {
            get {
                return ResourceManager.GetString("e_DeleteDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permission to Edit {0} data is denied..
        /// </summary>
        internal static string e_EditDenied {
            get {
                return ResourceManager.GetString("e_EditDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is no data view available for {0}..
        /// </summary>
        internal static string e_NoDataView {
            get {
                return ResourceManager.GetString("e_NoDataView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is no entity available for the tenant..
        /// </summary>
        internal static string e_NoEntity {
            get {
                return ResourceManager.GetString("e_NoEntity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is no entity meta data available for {0}..
        /// </summary>
        internal static string e_NoMetaData {
            get {
                return ResourceManager.GetString("e_NoMetaData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is no virtual entity available for the tenant..
        /// </summary>
        internal static string e_NoVirtualEntity {
            get {
                return ResourceManager.GetString("e_NoVirtualEntity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The data view should not be null or empty..
        /// </summary>
        internal static string e_NullDataView {
            get {
                return ResourceManager.GetString("e_NullDataView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The virtual entity should not be null or empty..
        /// </summary>
        internal static string e_NullEntity {
            get {
                return ResourceManager.GetString("e_NullEntity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select any data view..
        /// </summary>
        internal static string e_SelectDataView {
            get {
                return ResourceManager.GetString("e_SelectDataView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select any entity..
        /// </summary>
        internal static string e_SelectVirtualEntity {
            get {
                return ResourceManager.GetString("e_SelectVirtualEntity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Permission to View {0} data is denied..
        /// </summary>
        internal static string e_ViewDenied {
            get {
                return ResourceManager.GetString("e_ViewDenied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is no extended fields for this entity..
        /// </summary>
        internal static string m_NoExtnData {
            get {
                return ResourceManager.GetString("m_NoExtnData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The {0} data has been deleted successfully..
        /// </summary>
        internal static string s_VirtualEntity_Deleted_Successfully {
            get {
                return ResourceManager.GetString("s_VirtualEntity_Deleted_Successfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The {0} data has been saved successfully..
        /// </summary>
        internal static string s_VirtualEntity_Saved_Successfully {
            get {
                return ResourceManager.GetString("s_VirtualEntity_Saved_Successfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to this entity.
        /// </summary>
        internal static string thisEntity {
            get {
                return ResourceManager.GetString("thisEntity", resourceCulture);
            }
        }
    }
}
