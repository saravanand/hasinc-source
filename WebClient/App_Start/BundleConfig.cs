﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using CelloSaaS.Library.Helpers;

namespace CelloSaaSApplication
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            var bundle = new ScriptBundle("~/bundles/celloscripts")
                        .Include("~/Scripts/jquery.min.js")
                        .Include("~/Scripts/jquery-ui.min.js")
                        .Include("~/Scripts/json2.js")
                        .Include("~/Scripts/jquery.cookie.js")
                        .Include("~/Scripts/jquery.dataTables.js")
                        .Include("~/Scripts/select2.min.js")
                        .Include("~/Scripts/bootstrap.min.js")
                        .Include("~/Scripts/bootstrap.datepicker.js")
                        .Include("~/Scripts/bootstrap.slider.js")
                        .Include("~/Scripts/MicrosoftAjax.js")
                        .Include("~/Scripts/MicrosoftMvcAjax.js")
                        .Include("~/Scripts/CascadePickupList.js")
                        .Include("~/Scripts/helpers.js");
            bundle.Orderer = new AsDefinedBundleOrderer();
            bundles.Add(bundle);
            
            var cssBundle = new StyleBundle("~/bundles/cellostyles")
                         .Include("~/Content/jquery-ui.min.css")
                         .Include("~/Content/select2.css");

            if (ConfigHelper.PageProfiler)
            {
                cssBundle.Include("~/Content/introspection_toolbar.css");
            }

            cssBundle.Orderer = new AsDefinedBundleOrderer();
            bundles.Add(cssBundle);

            var vizBundle = new ScriptBundle("~/bundles/viz")
                        .Include("~/Scripts/celloChart.js")
                        .Include("~/Scripts/rgbcolor.js")
                        .Include("~/Scripts/canvg.js");
            vizBundle.Orderer = new AsDefinedBundleOrderer();
            bundles.Add(vizBundle);

            bundles.Add(new StyleBundle("~/bundles/kendostyles")
                        .Include("~/Content/Kendo/*.css")
                       );

            bundles.Add(new ScriptBundle("~/bundles/kendoscripts")
                        .Include("~/Scripts/kendo.dataviz.min.js")
                       );

            var codeMirrorBundle = new ScriptBundle("~/bundles/codemirrorscripts")
                        .Include("~/Scripts/codemirror.js")
                        .Include("~/Scripts/xml.js")
                        .Include("~/Scripts/util/runmode.js")
                        .Include("~/Scripts/util/formatting.js")
                        .Include("~/Scripts/util/xml-hint.js")
                        .Include("~/Scripts/util/closetag.js")
                        .Include("~/Scripts/util/searchcursor.js")
                        .Include("~/Scripts/util/matchBrackets.js")
                        .Include("~/Scripts/util/match-highlighter.js");
            codeMirrorBundle.Orderer = new AsDefinedBundleOrderer();
            bundles.Add(codeMirrorBundle);

            bundles.Add(new StyleBundle("~/bundles/themes/celloskin").Include("~/App_Themes/CelloSkin/*.css"));
        }
    }

    public class AsDefinedBundleOrderer : IBundleOrderer
    {        
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}