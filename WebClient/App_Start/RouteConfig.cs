﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CelloSaaSApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{*allaspx}", new { allaspx = @".*\.aspx(/.*)?" });
            routes.IgnoreRoute("{*staticfile}", new { staticfile = @".*\.(ico|css|js|gif|jpg|woff|eot|svg|eot|ttf|otf)(/.*)?" });

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.wdgt/{*pathInfo}");
            routes.IgnoreRoute("ChartImg.axd/{*pathInfo}");
            routes.Ignore("{*pathInfo}", new { pathInfo = @"^.*(ChartImg.axd)$" });
            routes.IgnoreRoute("{resource}.svc");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Datascopes",
                url: "AccessControl/DataSecurity/{id}",
                defaults: new { controller = "MasterData", action = "MasterDataList", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}