﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dependencies;
using System.Web.Http.Filters;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.View.WebApi;
using Microsoft.Practices.Unity;

namespace CelloSaaSApplication
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            cors.SupportsCredentials = false;
            config.EnableCors(cors);

            //config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithActionName",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { action = "", controller = "", namespaces = new string[] { "WebApplication.api" } }
            );

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional, namespaces = new string[] { "WebApplication.api" } }
            //);

            config.Filters.Add(new CelloExceptionFilterAttribute());
            config.Filters.Add(new CelloActionFilterAttribute());

            // do not change the order
            config.MessageHandlers.Add(new CelloWebApiUsageHandler()); // 1st
            config.MessageHandlers.Add(new CelloWebApiAuthenticationHandler()); // 2nd

            //var container = CelloSaaS.Library.ServiceLocator.GetContainer();
            //RegisterServices(container);
            //config.DependencyResolver = new UnityResolver(container, config.DependencyResolver);
        }

        //private static void RegisterServices(IUnityContainer container)
        //{
        //    container.RegisterType<IUserDetailsService, UserDetailsService>();
        //    container.RegisterType<ITenantService, TenantService>();
        //}
    }
}
