﻿using CelloSaaS.AuthServer.Client.Extensions;
using CelloSaaS.AuthServer.Core.Models;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.EventScheduler.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.View;
using CelloSaaSApplication.Models;
using CelloSaaSWebClient.Services;
using Microsoft.AspNet.Identity;
using Microsoft.IdentityModel.Claims;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class holds action for account controller
    /// </summary>
    [HandleError]
    public class AccountController : Controller
    {
        private const string DefaultPolicy = "GlobalExceptionLogger";

        /// <summary>
        /// This method is invoked when the tenant tries to activate after being self-registered. This completes the activation process
        /// </summary>
        /// <param name="activationKey">The activation key sent to the self-registered tenant.</param>
        /// <returns></returns>
        public ActionResult InviteLogOn(string activationKey)
        {
            if (string.IsNullOrEmpty(activationKey))
            {
                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey);
            }

            var passwordEncrptionService = ServiceLocator.Resolve<CelloSaaS.Library.Encryption.IPasswordEncrptionService>();
            string key = passwordEncrptionService.DecryptPassword(ProductAdminConstants.ProductAdminTenantId, HttpUtility.UrlDecode(activationKey));
            string[] activationDetails = key.Split('~');

            if (activationDetails.Length != 4)
            {
                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey);
            }

            string tenantId = activationDetails[0];
            string username = activationDetails[2];
            string password = activationDetails[3];

            var tenant = TenantProxy.GetTenantInfo(tenantId);

            if (tenant == null)
            {
                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey + Resources.AccountResource.e_TenantNotFound);
            }

            if (tenant.TenantDetails.ApprovalStatus == TenantApprovalStatus.REJECTED)
            {
                ModelState.AddModelError("ErrorMessage", Resources.AccountResource.e_AccountDeactivated);
                return View();
            }

            var userDetails = UserDetailsProxy.GetUserDetailsByName(username, tenantId);

            if (userDetails == null || string.IsNullOrEmpty(userDetails.MembershipDetails.TenantCode))
            {
                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey + Resources.AccountResource.e_UserNotFound);
            }

            if (userDetails.MembershipDetails.IsFirstTimeUser || userDetails.MembershipDetails.IsUserPasswordChangeForced)
            {
                //return LogOn(tenant.TenantDetails.TenantCodeString, username, password);
                return RedirectToAction("Logon", new { companyCode = tenant.TenantDetails.TenantCodeString });
            }
            else
            {
                ModelState.AddModelError("ErrorMessage", Resources.AccountResource.e_AlreadyActive);
                return View();
            }
        }

        /// <summary>
        /// This method is the entry point to logging in to the application from the client application
        /// </summary>
        /// <param name="companyCode">The company code</param>
        /// <param name="appType">The application type</param>
        /// <returns></returns>
        [ValidateInput(true)]
        public async Task<ActionResult> LogOn(string companyCode = null, string appType = null)
        {
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                return GetHomePageRedirectUrl(UserIdentity.Roles != null ? UserIdentity.Roles.ToList() : new List<string>());

            var commonLoginEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AllowCommonLogin"] ?? "false");

            if (commonLoginEnabled)
            {
                companyCode = System.Configuration.ConfigurationManager.AppSettings["RoboUserTenantCodeString"];
                appType = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.MultiTenantAccess.ToString("F");
            }


            string tenantId = null;
            if (string.IsNullOrEmpty(companyCode) || !Util.ValidateAlphaNumeric(companyCode))
            {
                var tenantUri = Request.Url.GetBaseUrl();

                if (string.IsNullOrEmpty(tenantUri))
                {
                    ModelState.AddModelError("", "Not able to infer the requested Uri");
                    return View();
                }

                var tenantDetails = TenantProxy.GetTenantDetailsByURL(tenantUri);

                if (tenantDetails == null || string.IsNullOrEmpty(tenantDetails.TenantCode))
                {
                    if (!string.IsNullOrEmpty(companyCode))
                        ModelState.AddModelError("", "Company code is Invalid");
                    return View();
                }
                tenantId = tenantDetails.TenantCode;
            }
            else
            {
                tenantId = TenantProxy.GetTenantIdFromTenantCode(companyCode);
                if (string.IsNullOrEmpty(tenantId))
                {
                    ModelState.AddModelError("companyCode", "Invalid Company Code");
                    return View();
                }
            }

            //For Live App
            //var clients = await ClientService.SearchClientsAsync(Guid.Parse(tenantId), ApplicationTypes.InternalWeb.ToString("F"), ClientTypes.Web.ToString("F"));

            CelloSaaS.AuthServer.Core.Models.ApplicationTypes appTypes = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.InternalWeb;

            if (!string.IsNullOrEmpty(appType))
                Enum.TryParse<CelloSaaS.AuthServer.Core.Models.ApplicationTypes>(appType, out appTypes);

            List<ExpandoObject> clients = null;

            try
            {
                clients = await ClientService.SearchClientsAsync(Guid.Parse(tenantId), appTypes.ToString("F"), ClientTypes.Web.ToString("F"));
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Exception occured while processing your request, please try back after few minutes");
                return View();
            }

            if (clients == null || clients.Count < 1)
            {
                ModelState.AddModelError("", "Invalid client details");
                return View();
            }

            var client = clients.FirstOrDefault();

            if (client == null)
            {
                ModelState.AddModelError("", "Not able to find the client matching the given Uri");
                return View();
            }

            Guid clientId = client.TryGetStringValue("Id").ToGuid();

            if (clientId.Equals(Guid.Empty))
            {
                ModelState.AddModelError("", "Could not find a suitable client");
                return View();
            }

            var redirectUris = client.TryGetStringValue("RedirectUris");

            if (string.IsNullOrEmpty(redirectUris))
            {
                ModelState.AddModelError("", "Could not find a suitable client");
                return View();
            }

            var redirectUriList = JsonConvert.DeserializeObject<List<string>>(redirectUris);

            var ctx = Request.GetOwinContext();
            var authProp = new Microsoft.Owin.Security.AuthenticationProperties
            {
                RedirectUri = redirectUriList[0]
            };
            authProp.Dictionary.Add("client_id", clientId.ToString());

            Request.GetOwinContext().Authentication.Challenge(authProp, AuthenticationConstants.CelloAuthentication);

            return new HttpUnauthorizedResult();
        }

        /// <summary>
        /// This method is called by the authorization server to send back the tokens to the client, post authentication
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> AuthorizationCallBack()
        {
            var ctx = Request.GetOwinContext();
            var externalAuthResult = await ctx.Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ExternalCookie);

            System.Security.Claims.ClaimsIdentity id = null;

            if (externalAuthResult != null && externalAuthResult.Identity != null && externalAuthResult.Identity.Claims != null && externalAuthResult.Identity.Claims.Count() > 0)
            {
                ctx.Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                id = new System.Security.Claims.ClaimsIdentity(externalAuthResult.Identity.Claims, DefaultAuthenticationTypes.ApplicationCookie);

                var expiryTimeSeconds = id.FindFirstValue(ClaimTypes.Expiration);

                long SessionTimeOut = CelloSaaS.View.AppSettingHelper.GetCookieTimeout();

                if (!string.IsNullOrEmpty(expiryTimeSeconds))
                {
                    long.TryParse(expiryTimeSeconds, out SessionTimeOut);
                }
                ctx.Authentication.SignIn(new Microsoft.Owin.Security.AuthenticationProperties
                {
                    ExpiresUtc = new DateTimeOffset(DateTime.UtcNow.AddSeconds(SessionTimeOut))
                }, id);
            }

            if (id != null)
            {
                string roles = id.FindFirstValue(ClaimTypes.Role);
                if (!string.IsNullOrEmpty(roles))
                    return RedirectToAction("SessionTenant", "Home");
                //return GetHomePageRedirectUrl(roles.Contains(',') ? roles.Split(',').ToList() : new List<string> { roles });
            }

            return RedirectToAction("SessionTenant", "Home");
            //return GetHomePageRedirectUrl();
        }

        private ActionResult GetHomePageRedirectUrl(List<string> userRoles = null)
        {
            if (userRoles != null)
            {
                if (userRoles.Contains(RoleConstants.ProductAdmin))
                {
                    return RedirectToAction("ProductDashboard", "Dashboard");
                }
                else if (userRoles.Contains(RoleConstants.TenantAdmin))
                {
                    return RedirectToAction("Index", "Dashboard");
                }
            }

            if (string.IsNullOrEmpty(ConfigHelper.AppHomePageUrl))
            {
                return this.RedirectToAction("Index", "Home");
            }
            else
            {
                return Redirect(ConfigHelper.AppHomePageUrl);
            }
        }

        public async Task<ActionResult> Error()
        {
            ViewBag.Message = TempData["Status"];
            TempData["Status"] = null;

            return View();
        }

        /// <summary>
        /// Logs off the user in the client application and the authorization server
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> LogOff()
        {
            if (!string.IsNullOrEmpty(UserIdentity.UserId))
            {
                UserActivityProxy.DeleteUserActivity(UserIdentity.UserId);

                EventRegisterProxy.RegisterEvent(new Event
                {
                    EventId = CelloEventConstant.LogOutEventId,
                    TenantId = UserIdentity.LoggedInUserTenantId,
                    UserId = UserIdentity.UserId,
                    SubjectId = UserIdentity.UserId,
                    SubjectType = "User",
                    SubjectXmlValue = UserDetailsProxy.GetUserDetailsByUserId(UserIdentity.UserId, UserIdentity.LoggedInUserTenantId).SerializeToXml()
                });
                TenantContext.Clear();
            }

            if (!string.IsNullOrEmpty(this.Request.QueryString["msg"]))
            {
                // tenant license validator httpmodule will logoff expired tenants
                try
                {
                    var message = HttpUtility.UrlDecode(this.Request.QueryString["msg"]).Decrypt();
                    TempData["Error"] = message;
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                }
            }

            #region OWIN Auth
            Request.GetOwinContext().Authentication.SignOut(new string[] 
            { 
                DefaultAuthenticationTypes.ExternalCookie,
                DefaultAuthenticationTypes.ApplicationCookie, 
                AuthenticationConstants.CelloAuthentication, 
                "FormAuthentication" 
            });
            #endregion

            var clientRedirectUri = Request.Url.GetBaseUrl();

            var logOffUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
            {
                Path = "api/Authentication/LogOut"
            };

            string queryPart = null;

            if (!string.IsNullOrEmpty(clientRedirectUri))
            {
                queryPart = string.Format(CultureInfo.InvariantCulture, "redirectUri={0}&client_id=", clientRedirectUri);
                if (UserIdentity.AdditionalProperties != null && UserIdentity.AdditionalProperties.ContainsKey(CelloClaimTypes.ClientId))
                {
                    queryPart += UserIdentity.AdditionalProperties[CelloClaimTypes.ClientId];
                }
            }

            logOffUri.Query = queryPart.TrimStart('?');

            return Redirect(logOffUri.Uri.ToString());
        }

        #region ChangePassword

        /// <summary>
        ///  This method is used to Changes the password.
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// This method is to change the password by getting the current password and new password.
        /// </summary>
        /// <param name="currentPassword">The current password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <param name="confirmPassword">The confirm password.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ChangePassword(string currentPassword, string newPassword, string confirmPassword)
        {
            try
            {
                if (ValidateChangePassword(currentPassword: currentPassword, newPassword: newPassword, confirmPassword: confirmPassword))
                {
                    if (currentPassword.Equals(newPassword))
                    {
                        ModelState.AddModelError("Error", Resources.AccountResource.e_SamePassword);
                        return View();
                    }

                    PasswordValidationStatus passwordValidationStatus = PasswordValidationProxy.IsPasswordValid(newPassword, confirmPassword);

                    if (passwordValidationStatus.IsPasswordValid)
                    {
                        if (UserDetailsProxy.ChangePassword(UserIdentity.Name, UserIdentity.TenantID, currentPassword, newPassword))
                        {
                            TempData["Success"] = Resources.AccountResource.s_CurrentPassword;
                        }
                        else
                        {
                            ModelState.AddModelError("Error", Resources.AccountResource.e_CurrentPassword);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Error", passwordValidationStatus.Message);
                    }
                }
            }
            catch (UserDetailException userDetailsException)
            {
                ExceptionService.HandleException(userDetailsException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.AccountResource.e_CurrentPassword);
            }
            catch (InvalidUserNameException invalidUserException)
            {
                ExceptionService.HandleException(invalidUserException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.AccountResource.e_InvalidUser);
            }

            return View();
        }

        /// <summary>
        /// This is to validate the change password fields.
        /// </summary>
        /// <param name="currentPassword">The current password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <param name="confirmPassword">The confirm password.</param>
        /// <returns></returns>
        private bool ValidateChangePassword(string currentPassword, string newPassword, string confirmPassword)
        {
            if (string.IsNullOrEmpty(currentPassword))
            {
                ModelState.AddModelError("currentPassword", Resources.AccountResource.e_currentpasswordMandatory);
            }
            if (string.IsNullOrEmpty(newPassword))
            {
                ModelState.AddModelError("newPassword", Resources.AccountResource.e_newPasswordMandatory);
            }
            if (string.IsNullOrEmpty(confirmPassword))
            {
                ModelState.AddModelError("confirmPassword", Resources.AccountResource.e_confirmPasswordMandatory);
            }

            if (!string.IsNullOrEmpty(newPassword) && !string.IsNullOrEmpty(confirmPassword))
            {
                if (!string.Equals(newPassword, confirmPassword, StringComparison.Ordinal))
                {
                    ModelState.AddModelError("newPassword", string.Empty);
                    ModelState.AddModelError("confirmPassword", string.Empty);
                    ModelState.AddModelError("Error", Resources.AccountResource.e_confirmPassword);
                }
            }

            return ModelState.IsValid;
        }

        #endregion
    }
}