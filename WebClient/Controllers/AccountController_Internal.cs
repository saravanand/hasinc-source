//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Data;
//using System.Globalization;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Web;
//using System.Web.Mvc;
//using System.Web.Security;
//using CelloSaaS.Billing.Model;
//using CelloSaaS.Billing.ServiceProxies;
//using CelloSaaS.EventScheduler.EventPublishingEngine;
//using CelloSaaS.EventScheduler.ServiceProxies;
//using CelloSaaS.Library;
//using CelloSaaS.Library.Encryption;
//using CelloSaaS.Library.Helpers;
//using CelloSaaS.Model;
//using CelloSaaS.Model.AccessControlManagement;
//using CelloSaaS.Model.LicenseManagement;
//using CelloSaaS.Model.TenantManagement;
//using CelloSaaS.Model.UserManagement;
//using CelloSaaS.ServiceContracts.AccessControlManagement;
//using CelloSaaS.ServiceContracts.LicenseManagement;
//using CelloSaaS.ServiceContracts.SettingsManagement;
//using CelloSaaS.ServiceContracts.UserManagement;
//using CelloSaaS.ServiceProxies.AccessControlManagement;
//using CelloSaaS.ServiceProxies.LicenseManagement;
//using CelloSaaS.ServiceProxies.SettingsManagement;
//using CelloSaaS.ServiceProxies.TenantManagement;
//using CelloSaaS.ServiceProxies.UserManagement;
//using CelloSaaS.View;
//using CelloSaaSApplication.Models;
//using Microsoft.IdentityModel.Claims;
//using Microsoft.IdentityModel.Protocols.WSFederation;
//using Microsoft.IdentityModel.Web;

//namespace CelloSaaSApplication.Controllers
//{
//    /// <summary>
//    /// This class holds action for account controller
//    /// </summary>
//    [HandleError]
//    public class AccountController : Controller
//    {
//        private const string DefaultPolicy = "GlobalExceptionLogger";

//        #region resetPassword

//        /// <summary>
//        /// This method will guide
//        /// the user to reset their password/unlock their account
//        /// in step by step manner
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult ResetPassword()
//        {
//            ViewData["step"] = "1";
//            return View();
//        }

//        /// <summary>
//        /// This method will get data step by step
//        /// and validating the data and resets the password if success
//        /// </summary>
//        /// <param name="forms">html form data collection</param>
//        /// <returns>returns the view</returns>
//        [HttpPost]
//        public ActionResult ResetPassword(FormCollection forms)
//        {
//            int step = Convert.ToInt16(forms["step"], CultureInfo.InvariantCulture);
//            string companyCode = forms["companyCode"];
//            string userName = forms["userName"];

//            ViewData["companyCode"] = companyCode;
//            ViewData["userName"] = userName;
//            ViewData["step"] = step;

//            if (string.IsNullOrEmpty(companyCode))
//            {
//                ModelState.AddModelError("resetpassword", Resources.AccountResource.e_CompanyCode);
//            }
//            else
//            {
//                TenantDetails tenantDetails = TenantProxy.GetTenantDetailsForTenantCodeString(companyCode);

//                if (tenantDetails == null)
//                {
//                    this.ModelState.AddModelError("resetpassword", Resources.AccountResource.e_CompanyCode);
//                }
//                else if (tenantDetails.Status.Equals(false) || tenantDetails.ApprovalStatus != TenantApprovalStatus.APPROVED)
//                {
//                    this.ModelState.AddModelError("resetpassword", Resources.AccountResource.e_AccountDeactivated);
//                }
//            }

//            if (string.IsNullOrEmpty(userName))
//            {
//                ModelState.AddModelError("resetpassword", Resources.AccountResource.e_UserName);
//            }

//            if (ModelState.IsValid)
//            {
//                switch (step)
//                {
//                    default:
//                    case 1:
//                        ResetPasswordStep1(companyCode, userName);
//                        break;

//                    case 2:
//                        ResetPasswordStep2(forms, companyCode, userName);
//                        break;

//                    case 3:
//                        ResetPasswordStep3(forms);
//                        break;
//                }
//            }

//            return View();
//        }

//        /// <summary>
//        /// This method is used to Resets the password step1.
//        /// </summary>
//        /// <param name="companyCode">The company code.</param>
//        /// <param name="userName">Name of the user.</param>
//        private void ResetPasswordStep1(string companyCode, string userName)
//        {
//            try
//            {
//                // get the user details
//                UserDetails userDetails = UserDetailsProxy.GetUserDetails(companyCode, userName);

//                if (userDetails != null && userDetails.MembershipDetails != null && !string.IsNullOrEmpty(userDetails.MembershipDetails.TenantCode))
//                {
//                    if (userDetails.MembershipDetails.IsFirstTimeUser || userDetails.MembershipDetails.IsLockedOut)
//                    {
//                        string errorMessage = (userDetails.MembershipDetails.IsFirstTimeUser) ? Resources.AccountResource.e_FirstTimeUserResetAttempt : Resources.AccountResource.e_UserLocked;
//                        // first time user so ask him to login with
//                        // temporary PIN to change his password
//                        ModelState.AddModelError("resetpassword", errorMessage);
//                    }
//                    else
//                    {
//                        if (string.IsNullOrEmpty(userDetails.MembershipDetails.PasswordQuestion))
//                        {
//                            // security question is null something went wrong
//                            ModelState.AddModelError("resetpassword", Resources.AccountResource.e_unableToContinue);
//                        }
//                        else
//                        {
//                            // user found so get his security questions
//                            ViewData["securityQuestion"] = userDetails.MembershipDetails.PasswordQuestion;
//                            ViewData["membershipId"] = userDetails.MembershipDetails.MembershipId;
//                            ViewData["tenantCode"] = userDetails.MembershipDetails.TenantCode;
//                            ViewData["step"] = 2;
//                        }
//                    }
//                }
//                else
//                {
//                    // user not found
//                    ModelState.AddModelError("resetpassword", Resources.AccountResource.e_InvalidUser);
//                }
//            }
//            catch (UserDetailException userExp)
//            {
//                // exception while retrieving user details
//                ExceptionService.HandleException(userExp, DefaultPolicy);
//                ModelState.AddModelError("resetpassword", Resources.AccountResource.e_userDetails);
//            }
//        }

//        /// <summary>
//        /// This method is used to Resets the password step2.
//        /// </summary>
//        /// <param name="forms">The forms.</param>
//        /// <param name="companyCode">The company code.</param>
//        /// <param name="userName">Name of the user.</param>
//        private void ResetPasswordStep2(FormCollection forms, string companyCode, string userName)
//        {
//            try
//            {
//                string securityAnswer = forms["securityAnswer"];
//                ViewData["membershipId"] = forms["membershipId"];
//                ViewData["securityQuestion"] = forms["securityQuestion"];
//                ViewData["tenantCode"] = forms["tenantCode"];

//                if (!string.IsNullOrEmpty(securityAnswer))
//                {
//                    if (UserDetailsProxy.ValidateSecurityAnswer(companyCode, userName, securityAnswer))
//                    {
//                        // success go to next step
//                        ViewData["step"] = 3;
//                    }
//                    else
//                    {
//                        // security answer does not match
//                        ModelState.AddModelError("resetpassword", Resources.AccountResource.e_wrongSecurityAnswer);
//                    }
//                }
//                else
//                {
//                    // empty security answer supplied
//                    ModelState.AddModelError("resetpassword", Resources.AccountResource.e_securityAnswer);
//                }
//            }
//            catch (UserLockedOutException userLockExp)
//            {
//                // due to exceed attempt user has been locked
//                ExceptionService.HandleException(userLockExp, DefaultPolicy);
//                ModelState.AddModelError("resetpassword", Resources.AccountResource.e_UserLocked);
//            }
//        }

//        /// <summary>
//        ///This method is used to Resets the password step3.
//        /// </summary>
//        /// <param name="forms">The forms.</param>
//        private void ResetPasswordStep3(FormCollection forms)
//        {
//            try
//            {
//                string membershipId = forms["membershipId"];
//                string password = forms["newPassword"];
//                string confirmPassword = forms["confirmNewPassword"];
//                string tenantCode = forms["tenantCode"];

//                ViewData["membershipId"] = membershipId;
//                ViewData["tenantCode"] = tenantCode;

//                if (!string.IsNullOrEmpty(password)
//                    && !string.IsNullOrEmpty(confirmPassword))
//                {
//                    if (password.Equals(confirmPassword))
//                    {
//                        PasswordValidationStatus passwordValidationStatus = PasswordValidationProxy.IsPasswordValid(password, confirmPassword);

//                        if (passwordValidationStatus.IsPasswordValid)
//                        {
//                            UserDetailsProxy.UpdateUserPassword(membershipId, password, tenantCode);
//                            // show success message
//                            ViewData["updateSuccess"] = Resources.AccountResource.e_updateSuccess;
//                        }
//                        else
//                        {
//                            ModelState.AddModelError("resetpassword", passwordValidationStatus.Message);
//                        }
//                    }
//                    else
//                    {
//                        // does not match
//                        ModelState.AddModelError("resetpassword", Resources.AccountResource.e_confirmPassword);
//                    }
//                }
//                else
//                {
//                    // empty password supplied
//                    ModelState.AddModelError("resetpassword", Resources.AccountResource.e_Password);
//                }
//            }
//            catch (UserDetailException userDetailException)
//            {
//                // exception in updating password
//                ExceptionService.HandleException(userDetailException, DefaultPolicy);
//                ModelState.AddModelError("resetpassword", Resources.AccountResource.e_unableToContinue);
//            }
//        }

//        #endregion resetpassword

//        #region LogOn

//        /// <summary>
//        /// This method is load the logon page.
//        /// </summary>
//        /// <returns></returns>        
//        public ActionResult LogOn(string companyCode)
//        {
//            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
//            {
//                // user already loged in so redirect him/her to home page
//                string returnUrl = Request.QueryString["ReturnUrl"];
//                if (!string.IsNullOrEmpty(returnUrl) && returnUrl.StartsWith("/", StringComparison.Ordinal)
//                        && !returnUrl.StartsWith("//", StringComparison.Ordinal) && !returnUrl.StartsWith("/\\", StringComparison.Ordinal))
//                {
//                    return Redirect(returnUrl);
//                }

//                return GetHomePageRedirectUrl();
//            }

//            TenantContext.Clear();

//            ViewData["companyCode"] = companyCode;
//            ViewData["username"] = string.Empty;

//            if (TempData["Error"] != null)
//            {
//                ModelState.AddModelError("login", TempData["Error"].ToString());
//            }

//            if (!ConfigHelper.EnableFederation)
//            {
//                return View(); //proceed to normal sign in
//            }

//            if (!string.IsNullOrEmpty(Request.QueryString["werror"]))
//            {
//                return FederatedSignOff();
//            }
//            string tenantId = string.Empty;
//            string whr = ConfigHelper.DefaultHomeRealm;

//            if (string.IsNullOrEmpty(companyCode))
//            {
//                string url = this.Request.Url.AbsoluteUri.Substring(0, this.Request.Url.AbsoluteUri.Length - this.Request.Url.PathAndQuery.Length);

//                TenantDetails tenantDetails = TenantProxy.GetTenantDetailsByURL(url);

//                if (tenantDetails != null && !string.IsNullOrEmpty(tenantDetails.TenantCode))
//                {
//                    tenantId = tenantDetails.TenantCode;
//                }
//                else
//                {
//                    // if tenant details not found return a page to enter the company code 
//                    return this.View("CompanyCode");
//                }
//            }
//            else
//            {
//                // get tenantid from the entered company code
//                tenantId = TenantProxy.GetTenantIdFromTenantCode(companyCode);

//                if (string.IsNullOrEmpty(tenantId))
//                {
//                    this.ModelState.AddModelError("", Resources.AccountResource.e_CompanyCode);
//                    return this.View("CompanyCode");
//                }
//            }

//            whr = GetHomeRealmForTenant(tenantId) ?? whr;

//            var fam = FederatedAuthentication.WSFederationAuthenticationModule;

//            SignInRequestMessage mess = new SignInRequestMessage(
//                new Uri(fam.Issuer),
//                fam.Realm, fam.Reply ?? fam.Realm)
//            {
//                Context = tenantId,
//                HomeRealm = whr
//            };

//            mess.Parameters.Add("TenantId", tenantId);

//            //Response.Write(mess.WriteFormPost());
//            this.Response.Redirect(mess.WriteQueryString());

//            return View();
//        }

//        /// <summary>
//        /// This method is used to Federateds the result.
//        /// </summary>
//        /// <returns>ActionResult.</returns>
//        [ValidateInput(false)]
//        public ActionResult FederatedResult()
//        {
//            var fam = FederatedAuthentication.WSFederationAuthenticationModule;

//            if (fam.CanReadSignInResponse(System.Web.HttpContext.Current.Request, true))
//            {
//                // This is a response from the STS
//                SignInResponseMessage mesg = WSFederationMessage.CreateFromNameValueCollection(
//                   WSFederationMessage.GetBaseUrl(Request.Url), Request.Form) as SignInResponseMessage;

//                string userName = GetUserNameFromClaims();
//                if (string.IsNullOrEmpty(userName))
//                {
//                    ModelState.AddModelError("Request", "User Name is empty.");
//                    return View("FederatedSignInError");
//                }

//                string tenantId = mesg.Context;
//                string companyCode = string.Empty;

//                Tenant tenantInfo = TenantProxy.GetTenantInfo(tenantId);
//                companyCode = tenantInfo.TenantDetails.TenantCodeString;

//                //Get user details based on the user name
//                UserDetails userDetails = UserDetailsProxy.GetUserDetails(companyCode, userName);

//                if (userDetails != null && userDetails.MembershipDetails != null
//                    && !string.IsNullOrEmpty(userDetails.MembershipDetails.TenantCode))
//                {
//                    var userRoles = new List<string>();

//                    // fetch tenant roles, privileges and license and set form auth ticket cookies
//                    // add necessary calims to the principal
//                    if (this.PrepareTenantRolesAndPrivileges(userDetails, userRoles))
//                    {
//                        UserActivityProxy.AddUserActivity(userDetails.User.UserId);

//                        EventRegisterProxy.RegisterEvent(
//                            new Event
//                            {
//                                EventId = CelloEventConstant.LoginEventId,
//                                TenantId = tenantId,
//                                UserId = userDetails.User.UserId,
//                                SubjectId = userDetails.User.UserId,
//                                SubjectType = "UserDetails",
//                                SubjectXmlValue = userDetails.SerializeToXml()
//                            });

//                        // redirect 
//                        return GetHomePageRedirectUrl(userRoles);
//                    }
//                }
//                else
//                {
//                    ModelState.AddModelError("Login", Resources.AccountResource.e_UserDetailsNotAvailable);
//                }
//            }
//            else
//            {
//                ModelState.AddModelError("Request", Resources.AccountResource.e_ValidRequest);
//            }

//            FederatedAuthentication.SessionAuthenticationModule.SignOut();
//            FederatedAuthentication.SessionAuthenticationModule.DeleteSessionTokenCookie();
//            FederatedAuthentication.WSFederationAuthenticationModule.SignOut(false);
//            FormsAuthentication.SignOut();

//            return View("FederatedSignInError");
//        }

//        /// <summary>
//        /// This method is used to gets the user name from claims.
//        /// </summary>
//        /// <returns>System.String.</returns>
//        private static string GetUserNameFromClaims()
//        {
//            var claims = ((IClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identities[0].Claims;
//            string userName = string.Empty;
//            foreach (var item in claims)
//            {
//                if (item.ClaimType == ClaimTypes.Name && !string.IsNullOrEmpty(item.Value))
//                {
//                    userName = item.Value;
//                }
//            }

//            return userName;
//        }

//        /// <summary>
//        /// This method is used to gets the home realm for tenant.
//        /// </summary>
//        /// <param name="tenantId">The tenant id.</param>
//        /// <returns>System.String.</returns>
//        private static string GetHomeRealmForTenant(string tenantId)
//        {
//            string whr = null;
//            var tenantSettings = TenantSettingsProxy.GetTenantSettings(tenantId);

//            if (tenantSettings != null && tenantSettings.Setting != null
//                && tenantSettings.Setting.Attributes != null
//                && tenantSettings.Setting.Attributes.ContainsKey(AttributeConstants.HomeRealm)
//                && !string.IsNullOrEmpty(tenantSettings.Setting.Attributes[AttributeConstants.HomeRealm]))
//            {
//                whr = tenantSettings.Setting.Attributes[AttributeConstants.HomeRealm];
//            }

//            return whr;
//        }

//        /// <summary>
//        /// Gets the username and passwoed.
//        /// Validate the username and password.Then Get the tenantid by passing the username.
//        /// Check the tenant have license.
//        /// Then get the role and privilges for the user.
//        /// Set the user Idetity.
//        /// </summary>
//        /// <param name="companyCode">Company Code</param>
//        /// <param name="userName">Username</param>
//        /// <param name="password">password</param>
//        /// <returns></returns>
//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult LogOn(string companyCode, string userName, string password)
//        {
//            if (!string.IsNullOrEmpty(companyCode))
//                companyCode = companyCode.Trim();
//            if (!string.IsNullOrEmpty(userName))
//                userName = userName.Trim();
//            if (!string.IsNullOrEmpty(password))
//                password = password.Trim();

//            ViewData["companyCode"] = companyCode;
//            ViewData["username"] = userName;

//            if (!this.ValidateLogOn(userName, password, companyCode))
//            {
//                return this.View();
//            }

//            try
//            {
//                TenantDetails tenantDetails = TenantProxy.GetTenantDetailsForTenantCodeString(companyCode);

//                if (tenantDetails == null)
//                {
//                    this.ModelState.AddModelError("companyCode", Resources.AccountResource.e_CompanyCode);
//                    return this.View();
//                }
//                else if (tenantDetails.Status.Equals(false) || tenantDetails.ApprovalStatus == TenantApprovalStatus.REJECTED)
//                {
//                    this.ModelState.AddModelError("login", Resources.AccountResource.e_AccountDeactivated);
//                    return this.View();
//                }

//                if (tenantDetails.ApprovalStatus == TenantApprovalStatus.WAITINGFORAPPROVAL)
//                {
//                    if (!CheckSecretPrePaidLogonCookie(companyCode)) // if pre-paid logon then allow else don't
//                    {
//                        this.ModelState.AddModelError("login", Resources.AccountResource.e_AccountWaitingForApproval);
//                        return this.View();
//                    }
//                }

//                // Get tenant license details and check valid tenant license
//                TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(tenantDetails.TenantCode);

//                // if license expired then do not allow to login to the system
//                if (tenantLicense == null || (tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value < DateTime.Now))
//                {
//                    this.ModelState.AddModelError("login", Resources.AccountResource.e_TenantLicense);
//                }

//                // validate parent tenant license also
//                if (!ValidateRootTenantLicense(tenantDetails.TenantCode))
//                {
//                    this.ModelState.AddModelError("login", Resources.AccountResource.e_TenantLicense);
//                }

//                if (ModelState.IsValid)
//                {
//                    //Check valid user name and password.
//                    bool validUser = UserDetailsProxy.ValidateUser(userName, password, companyCode);

//                    if (!validUser)
//                    {
//                        this.ModelState.AddModelError("login", Resources.AccountResource.e_UserNamePassword);
//                        return this.View();
//                    }

//                    //Get user details based on the user name
//                    UserDetails userDetails = UserDetailsProxy.GetUserDetails(companyCode, userName);

//                    if (userDetails == null || string.IsNullOrEmpty(userDetails.MembershipDetails.TenantCode))
//                    {
//                        this.ModelState.AddModelError("login", Resources.AccountResource.e_unableToContinue);
//                        return this.View();
//                    }

//                    var currentURL = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.PathAndQuery.Length);

//                    var urlTenant = TenantProxy.GetTenantDetailsByURL(currentURL);

//                    // if the login url does not match with the user tenant url then throw error
//                    if (urlTenant != null && !userDetails.MembershipDetails.TenantCode.Equals(urlTenant.TenantCode, StringComparison.OrdinalIgnoreCase))
//                    {
//                        this.ModelState.AddModelError("login", Resources.AccountResource.e_InvalidUser);
//                        return this.View();
//                    }

//                    // check for the forced password setting for this member
//                    if (userDetails.MembershipDetails.IsUserPasswordChangeForced && !userDetails.MembershipDetails.IsFirstTimeUser)
//                    {
//                        var key = string.Format("{0}~{1}~{2}", companyCode, userDetails.MembershipDetails.UserName, userDetails.MembershipDetails.MembershipId);
//                        var encKey = key.Encrypt();
//                        var routeValuesPc = new { key = encKey };

//                        return this.RedirectToAction("PasswordChange", routeValuesPc);
//                    }

//                    // check for new user
//                    if (userDetails.MembershipDetails.IsFirstTimeUser)
//                    {
//                        var key = string.Format("{0}~{1}", companyCode, userDetails.MembershipDetails.UserName);
//                        var encKey = key.Encrypt();

//                        var routeValues = new { key = encKey };

//                        return this.RedirectToAction("FirstTimeUser", routeValues);
//                    }

//                    if (ConfigHelper.CanTraceUserActivity() && UserActivityProxy.IsSessionActive(userDetails.User.UserId))
//                    {
//                        this.ModelState.AddModelError("login", Resources.AccountResource.e_UserActivity_SessionActive);
//                        return this.View();
//                    }

//                    var userRoles = new List<string>();

//                    // check for valid tenant status and license status
//                    if (this.PrepareTenantRolesAndPrivileges(userDetails, userRoles))
//                    {
//                        UserActivityProxy.AddUserActivity(userDetails.User.UserId);
//                        EventRegisterProxy.RegisterEvent(
//                            new Event
//                            {
//                                EventId = CelloEventConstant.LoginEventId,
//                                TenantId = tenantDetails.TenantCode,
//                                UserId = userDetails.User.UserId,
//                                SubjectId = userDetails.User.UserId,
//                                SubjectType = "UserDetails",
//                                SubjectXmlValue = userDetails.SerializeToXml()
//                            });

//                        TenantContext.Clear();

//                        return GetHomePageRedirectUrl(userRoles);
//                    }
//                    else
//                    {
//                        bool isSharedTenantUser = TenantUserAssociationProxy.CanLinkWithOtherTenants(userDetails.MembershipDetails.TenantCode);

//                        if (isSharedTenantUser)
//                        {
//                            Dictionary<string, TenantDetails> stratifiedTenantDetails = TenantProxy.GetStratifiedTenantsByUser(userDetails.User.UserId, userDetails.MembershipDetails.TenantCode);

//                            if (stratifiedTenantDetails != null && stratifiedTenantDetails.Count > 0)
//                            {
//                                // self tenant mapping is removed, but self tenant linking is not allowed, so no need to check that
//                                if (stratifiedTenantDetails.ContainsKey(userDetails.MembershipDetails.TenantCode))
//                                {
//                                    stratifiedTenantDetails.Remove(userDetails.MembershipDetails.TenantCode);
//                                }

//                                if (stratifiedTenantDetails.Count > 0)
//                                {
//                                    UserActivityProxy.AddUserActivity(userDetails.User.UserId);
//                                    EventRegisterProxy.RegisterEvent(
//                                        new Event
//                                        {
//                                            EventId = CelloEventConstant.LoginEventId,
//                                            TenantId = tenantDetails.TenantCode,
//                                            UserId = userDetails.User.UserId,
//                                            SubjectId = userDetails.User.UserId,
//                                            SubjectType = "UserDetails",
//                                            SubjectXmlValue = userDetails.SerializeToXml()
//                                        });

//                                    TenantContext.Clear();

//                                    SetAuthCookie(userDetails, null);

//                                    TempData["RedirectToSessionTenant"] = true;
//                                    return RedirectToAction("SessionTenant", "Home");
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            catch (UserApprovalPendingException approvalException)
//            {
//                ExceptionService.HandleException(approvalException, DefaultPolicy);
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_UserNotApproved);
//            }
//            catch (UserDetailException exception)
//            {
//                ExceptionService.HandleException(exception, DefaultPolicy);
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_userDetails);
//            }
//            catch (LicenseException licenseException)
//            {
//                ExceptionService.HandleException(licenseException, DefaultPolicy);
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_license);
//            }
//            catch (UserPasswordExpirationException userPasswordExpirationException)
//            {
//                ExceptionService.HandleException(userPasswordExpirationException, DefaultPolicy);

//                UserDetails userDetails = UserDetailsProxy.GetUserDetails(companyCode, userName);
//                var key = string.Format("{0}~{1}~{2}~{3}~{4}", companyCode,
//                    userDetails.MembershipDetails.UserName,
//                    userDetails.MembershipDetails.MembershipId,
//                    userDetails.MembershipDetails.TenantCode,
//                    1);
//                var encKey = key.Encrypt();
//                var routeValues = new { key = encKey };

//                return this.RedirectToAction("PasswordChange", routeValues);
//            }
//            catch (UserDeactivateException userDeactivateException)
//            {
//                ExceptionService.HandleException(userDeactivateException, DefaultPolicy);
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_AccountDeactivated);
//            }
//            catch (UserLockedOutException userLockException)
//            {
//                ExceptionService.HandleException(userLockException, DefaultPolicy);
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_UserLocked);
//            }
//            catch (UserPasswordPenultimateFailureAttempt userLockException)
//            {
//                ExceptionService.HandleException(userLockException, DefaultPolicy);
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_UserPreLocked);
//            }

//            return View();
//        }

//        /// <summary>
//        /// If parent tenant is deactivated then do not allow child tenants to access the application
//        /// </summary>
//        /// <param name="tenantId"></param>
//        /// <returns></returns>
//        private static bool ValidateRootTenantLicense(string tenantId)
//        {
//            var parentId = GetParentTenantsTillBoundaryRoot(tenantId);

//            if (!string.IsNullOrEmpty(parentId))
//            {
//                var license = LicenseProxy.GetTenantLicense(parentId);

//                if (license == null || (license.ValidityEnd.HasValue && license.ValidityEnd.Value < DateTime.Now))
//                {
//                    return false;
//                }
//            }

//            return true;
//        }

//        /// <summary>
//        /// Find and get the parent tenant id till the billing root
//        /// </summary>
//        /// <param name="childTenantId"></param>
//        /// <returns></returns>
//        private static string GetParentTenantsTillBoundaryRoot(string tenantId)
//        {
//            var tenantTypes = TenantTypesProxy.GetTenantTypes(tenantId);

//            // do not check for isv or reseller (other than org unit)
//            if (tenantTypes != null && !tenantTypes.Any(x => x.ID == TenantTypeConstants.OrgUnit))
//            {
//                return null;
//            }

//            var parent = TenantRelationProxy.GetParentTenantInHierarchy(tenantId);

//            while (parent != null)
//            {
//                tenantTypes = TenantTypesProxy.GetTenantTypes(parent.TenantCode);

//                // if tenant is other than orgunit then stop
//                if (tenantTypes != null && tenantTypes.Any(x => x.ID == TenantTypeConstants.SMB || x.ID == TenantTypeConstants.Enterprise))
//                {
//                    return parent.TenantCode;
//                }

//                parent = parent.ParentTenant;
//            }

//            return null;
//        }

//        /// <summary>
//        /// Logs the on.
//        /// </summary>
//        /// <param name="activationKey">The invitation key.</param>
//        /// <returns></returns>
//        public ActionResult InviteLogOn(string activationKey)
//        {
//            if (string.IsNullOrEmpty(activationKey))
//            {
//                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey);
//            }

//            var passwordEncrptionService = ServiceLocator.Resolve<IPasswordEncrptionService>();
//            string key = passwordEncrptionService.DecryptPassword(ProductAdminConstants.ProductAdminTenantId, HttpUtility.UrlDecode(activationKey));
//            string[] activationDetails = key.Split('~');

//            if (activationDetails.Length != 4)
//            {
//                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey);
//            }

//            string tenantId = activationDetails[0];
//            string username = activationDetails[2];
//            string password = activationDetails[3];

//            var tenant = TenantProxy.GetTenantInfo(tenantId);

//            if (tenant == null)
//            {
//                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey + Resources.AccountResource.e_TenantNotFound);
//            }

//            if (tenant.TenantDetails.ApprovalStatus == TenantApprovalStatus.REJECTED)
//            {
//                ModelState.AddModelError("ErrorMessage", Resources.AccountResource.e_AccountDeactivated);
//                return View();
//            }

//            var userDetails = UserDetailsProxy.GetUserDetailsByName(username, tenantId);

//            if (userDetails == null || string.IsNullOrEmpty(userDetails.MembershipDetails.TenantCode))
//            {
//                return HttpNotFound(Resources.AccountResource.e_InvalidActivationKey + Resources.AccountResource.e_UserNotFound);
//            }

//            if (userDetails.MembershipDetails.IsFirstTimeUser || userDetails.MembershipDetails.IsUserPasswordChangeForced)
//            {
//                return LogOn(tenant.TenantDetails.TenantCodeString, username, password);
//            }
//            else
//            {
//                ModelState.AddModelError("ErrorMessage", Resources.AccountResource.e_AlreadyActive);
//                return View();
//            }
//        }

//        /// <summary>
//        /// Check whether the given user is login for first time
//        /// if so then get his/her new password and security answer
//        /// and update them
//        /// </summary>
//        /// <param name="key">encrypted key</param>
//        /// <returns>returns the view</returns>
//        public ActionResult FirstTimeUser(string key)
//        {
//            if (!string.IsNullOrEmpty(key))
//            {
//                try
//                {
//                    var tmp = key.Decrypt().Split('~');
//                    string companyCode = tmp[0];
//                    string userName = tmp[1];

//                    UserDetails userDetails = UserDetailsProxy.GetUserDetails(companyCode, userName);

//                    if (userDetails != null && userDetails.MembershipDetails != null
//                        && userDetails.MembershipDetails.IsFirstTimeUser)
//                    {
//                        ViewData["userName"] = userName;
//                        ViewData["companyCode"] = companyCode;
//                        return View();
//                    }
//                }
//                catch (Exception ex)
//                {
//                    ExceptionService.HandleException(ex, DefaultPolicy);
//                }
//            }

//            return RedirectToAction("Index", "Home");
//        }

//        /// <summary>
//        ///This method is used to get the new password and security answer
//        /// and updates them
//        /// </summary>
//        /// <param name="forms">user entered form details</param>
//        /// <returns>returns the view</returns>
//        [HttpPost]
//        public ActionResult FirstTimeUser(FormCollection forms)
//        {
//            string password = forms["password"];
//            string confirmPassword = forms["confirmPassword"];
//            string securityQuestion = forms["securityQuestion"];
//            string securityAnswer = forms["securityAnswer"];

//            string companyCode = forms["companyCode"];
//            string userName = forms["userName"];

//            // check the username and company codes
//            if (string.IsNullOrEmpty(companyCode) || string.IsNullOrEmpty(userName))
//            {
//                return RedirectToAction("LogOn", "Account");
//            }

//            // store the req view data's
//            ViewData["companyCode"] = companyCode;
//            ViewData["userName"] = userName;

//            if (string.IsNullOrEmpty(password))
//            {
//                // user not supplied valid password
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_newPasswordMandatory);
//            }
//            else if (string.IsNullOrEmpty(confirmPassword))
//            {
//                // password does not match
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_confirmPassword);
//            }

//            if (string.IsNullOrEmpty(securityAnswer))
//            {
//                // security answer not supplied
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_securityAnswer);
//            }

//            if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(confirmPassword) && !password.Equals(confirmPassword))
//            {
//                // password does not match
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_confirmPassword);
//            }

//            if (!ModelState.IsValid)
//            {
//                return View(); // display error message in view
//            }

//            try
//            {
//                // Get user details based on the user name and company code
//                UserDetails userDetails = UserDetailsProxy.GetUserDetails(companyCode, userName);

//                if (userDetails != null)
//                {
//                    PasswordValidationStatus passwordValidationStatus = PasswordValidationProxy.IsPasswordValid(password, confirmPassword);

//                    if (passwordValidationStatus.IsPasswordValid)
//                    {
//                        // update the details
//                        UserDetailsProxy.UpdateFirstTimeLogOn(
//                            userDetails.MembershipDetails.MembershipId,
//                            password,
//                            securityQuestion,
//                            securityAnswer,
//                            true,
//                            userDetails.MembershipDetails.TenantCode);

//                        var userRoles = new List<string>();

//                        // fetch tenant roles, privileges and license and set form auth ticket cookies
//                        if (this.PrepareTenantRolesAndPrivileges(userDetails, userRoles))
//                        {
//                            UserActivityProxy.AddUserActivity(userDetails.User.UserId);
//                            EventRegisterProxy.RegisterEvent(
//                                new Event
//                                {
//                                    EventId = CelloEventConstant.LoginEventId,
//                                    TenantId = userDetails.MembershipDetails.TenantCode,
//                                    UserId = userDetails.User.UserId,
//                                    SubjectId = userDetails.User.UserId,
//                                    SubjectType = "UserDetails",
//                                    SubjectXmlValue = userDetails.SerializeToXml()
//                                });
//                            TenantContext.Clear();

//                            return GetHomePageRedirectUrl(userRoles);
//                        }
//                        else
//                        {
//                            TempData["Error"] = Resources.AccountResource.e_UserRoles;
//                            return this.RedirectToAction("LogOn", "Account");
//                        }
//                    }

//                    this.ModelState.AddModelError("updatePassword", passwordValidationStatus.Message);
//                }
//                else
//                {
//                    this.ModelState.AddModelError("updatePassword", Resources.AccountResource.e_InvalidUser);
//                }
//            }
//            catch (ArgumentNullException userDetailsExp)
//            {
//                ExceptionService.HandleException(userDetailsExp, DefaultPolicy);
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_ParameterEmptyOrNull);
//            }
//            catch (ArgumentException userDetailsExp)
//            {
//                ExceptionService.HandleException(userDetailsExp, DefaultPolicy);
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_ParameterEmptyOrNull);
//            }
//            catch (UnauthorizedAccessException userDetailsExp)
//            {
//                ExceptionService.HandleException(userDetailsExp, DefaultPolicy);
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_Access);
//            }
//            catch (UserDetailException userDetailsExp)
//            {
//                ExceptionService.HandleException(userDetailsExp, DefaultPolicy);
//                ModelState.AddModelError("updatePassword", Resources.AccountResource.e_userDetails);
//            }

//            return View(); // something went wrong, so dispaly error message
//        }

//        private ActionResult GetHomePageRedirectUrl(List<string> userRoles = null)
//        {
//            if (userRoles != null)
//            {
//                if (userRoles.Contains(RoleConstants.ProductAdmin))
//                {
//                    return RedirectToAction("ProductDashboard", "Dashboard");
//                }
//                else if (userRoles.Contains(RoleConstants.TenantAdmin))
//                {
//                    return RedirectToAction("Index", "Dashboard");
//                }
//            }

//            if (string.IsNullOrEmpty(ConfigHelper.AppHomePageUrl))
//            {
//                return this.RedirectToAction("Index", "Home");
//            }
//            else
//            {
//                return Redirect(ConfigHelper.AppHomePageUrl);
//            }
//        }

//        /// <summary>
//        /// This method will get the privileges, license, roles from the
//        /// given user details and set the form auth ticket and cookie
//        /// </summary>
//        /// <param name="userDetails">user details</param>
//        /// <returns>returns True/False</returns>
//        private bool PrepareTenantRolesAndPrivileges(UserDetails userDetails, List<string> userRoles)
//        {
//            // Get tenant details and check valid tenant
//            Tenant tenantDetails = TenantProxy.GetTenantInfo(userDetails.MembershipDetails.TenantCode);

//            if (tenantDetails == null || !tenantDetails.TenantDetails.Status.Equals(true))
//            {
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_TenantDeactivated);
//                return false;
//            }

//            // Get user roles
//            string[] roles = GetUserRoles(userDetails, tenantDetails);

//            // Get tenant license details and check valid tenant license
//            TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(userDetails.MembershipDetails.TenantCode);

//            if (tenantLicense == null)
//            {
//                this.ModelState.AddModelError("login", Resources.AccountResource.e_TenantLicense);
//                return false;
//            }

//            // user is not tenant admin and trial ended and not payment details then donot' allow
//            if (tenantLicense.TrialEndDate.HasValue && tenantLicense.TrialEndDate.Value < DateTime.Now
//                && tenantDetails.TenantDetails.EnableAutoDebit && (roles != null && !roles.Contains(RoleConstants.TenantAdmin)))
//            {
//                var payment = PaymentProxy.GetAccountSetting(Guid.Parse(tenantDetails.TenantDetails.TenantCode));

//                if (payment == null || string.IsNullOrEmpty(payment.StoreKey) || payment.StoreKey.Split('/').Length != 2 || !payment.Status)
//                {
//                    this.ModelState.AddModelError("login", Resources.AccountResource.e_TenantLicense);
//                    return false;
//                }
//            }

//            if (roles != null && roles.Length > 0)
//            {
//                userRoles.AddRange(roles);
//                // user has valid role so allow login
//                SetAuthCookie(userDetails, roles);
//                return true;
//            }

//            this.ModelState.AddModelError("login", Resources.AccountResource.e_UserRoles);
//            return false;
//        }

//        /// <summary>
//        /// Consolidate and get user roles
//        /// </summary>
//        /// <param name="userDetails"></param>
//        /// <param name="tenantDetails"></param>
//        /// <returns></returns>
//        private static string[] GetUserRoles(UserDetails userDetails, Tenant tenantDetails)
//        {
//            string[] roles = RoleProxy.GetUserRolesForTenant(userDetails.User.UserId, tenantDetails.TenantDetails.TenantCode, tenantDetails.TenantDetails.TenantCode);

//            // incase the user has no self tenant roles, then get the roles that the user has, atleast by the way of stratification
//            if (roles == null || roles.Length == 0)
//            {
//                roles = RoleProxy.GetUserRoles(userDetails.User.UserId);
//            }

//            // even if there is no role in this case, report the no active roles and disallow login
//            if (roles != null && roles.Length > 0)
//            {
//                // Get tenant roles
//                List<Role> roleList = RoleProxy.SearchRolesInCurrentTenant(string.Empty, userDetails.MembershipDetails.TenantCode);

//                // Check if user has at-least one role in tenant role then set role status is true.
//                if (roleList != null && roleList.Any(x => roles.Contains(x.RoleId)))
//                {
//                    return roles; // user has valid role so allow login
//                }
//            }

//            return null;
//        }

//        /// <summary>
//        /// Set authentication cookie
//        /// </summary>
//        /// <param name="userDetails"></param>
//        /// <param name="roles"></param>
//        private static void SetAuthCookie(UserDetails userDetails, string[] roles)
//        {
//            if (System.Threading.Thread.CurrentPrincipal is IClaimsPrincipal)
//            {
//                AddClaimsToCurrentPrincipal(
//                    userDetails.MembershipDetails.UserName,
//                    userDetails.User.UserId,
//                    roles,
//                    null,
//                    userDetails.MembershipDetails.TenantCode,
//                    userDetails.MembershipDetails.TenantCode);
//            }
//            else
//            {
//                SetAuthenticationTicketInfo(
//                    userDetails.MembershipDetails.UserName,
//                    userDetails.User.UserId,
//                    roles,
//                    userDetails.MembershipDetails.TenantCode,
//                    null);
//            }
//        }

//        private bool CheckSecretPrePaidLogonCookie(string companyCode)
//        {
//            var tplCookie = Request.Cookies.Get("cello_tpl");
//            if (tplCookie == null || string.IsNullOrEmpty(tplCookie.Value)) return false;
//            tplCookie.Expires = DateTime.Now.AddYears(-1);
//            Response.Cookies.Add(tplCookie);
//            var value = tplCookie.Value.Decrypt();
//            return value.Equals(companyCode, StringComparison.OrdinalIgnoreCase);
//        }

//        #endregion

//        #region LogOff

//        /// <summary>
//        /// This method is to LOgOff.
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult LogOff()
//        {
//            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
//            {
//                UserActivityProxy.DeleteUserActivity(UserIdentity.UserId);

//                EventRegisterProxy.RegisterEvent(new Event
//                {
//                    EventId = CelloEventConstant.LogOutEventId,
//                    TenantId = UserIdentity.LoggedInUserTenantId,
//                    UserId = UserIdentity.UserId,
//                    SubjectId = UserIdentity.UserId,
//                    SubjectType = "User",
//                    SubjectXmlValue = UserDetailsProxy.GetUserDetailsByUserId(UserIdentity.UserId, UserIdentity.LoggedInUserTenantId).SerializeToXml()
//                });
//            }
//            if (Thread.CurrentPrincipal is IClaimsPrincipal)
//            {
//                return FederatedSignOff();
//            }
//            else
//            {
//                FormsAuthentication.SignOut();
//            }

//            TenantContext.Clear();

//            if (!string.IsNullOrEmpty(this.Request.QueryString["msg"]))
//            {
//                // tenant license validator httpmodule will logoff expired tenants
//                try
//                {
//                    var message = HttpUtility.UrlDecode(this.Request.QueryString["msg"]).Decrypt();
//                    TempData["Error"] = message;
//                }
//                catch (Exception ex)
//                {
//                    ExceptionService.HandleException(ex, DefaultPolicy);
//                }
//            }

//            return Redirect("/");
//        }

//        /// <summary>
//        /// This method is used to federateds the sign off.
//        /// </summary>
//        /// <returns>RedirectResult.</returns>
//        private RedirectResult FederatedSignOff()
//        {
//            FederatedAuthentication.SessionAuthenticationModule.SignOut();
//            FederatedAuthentication.SessionAuthenticationModule.DeleteSessionTokenCookie();
//            FederatedAuthentication.WSFederationAuthenticationModule.SignOut(false);
//            //FormsAuthentication.SignOut();

//            Response.Cache.SetCacheability(HttpCacheability.NoCache);

//            WSFederationAuthenticationModule fam = FederatedAuthentication.WSFederationAuthenticationModule;
//            SignOutRequestMessage signOutRequest = new SignOutRequestMessage(new Uri(fam.Issuer), fam.Realm);
//            return Redirect(signOutRequest.WriteQueryString());
//            //WSFederationAuthenticationModule authModule = FederatedAuthentication.WSFederationAuthenticationModule;
//            //string signoutUrl = (WSFederationAuthenticationModule.GetFederationPassiveSignOutUrl(authModule.Issuer, authModule.Realm, null));
//            //WSFederationAuthenticationModule.FederatedSignOut(new Uri(authModule.Issuer), new Uri(authModule.Realm));
//        }

//        #endregion

//        #region ChangePassword

//        /// <summary>
//        ///  This method is used to Changes the password.
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult ChangePassword()
//        {
//            return View();
//        }

//        /// <summary>
//        /// This method is to load change password page.
//        /// </summary>
//        /// <param name="companyCode">The company code.</param>
//        /// <param name="userName">Name of the user.</param>
//        /// <param name="membershipId">The membership id.</param>
//        /// <returns></returns>
//        [HttpGet]
//        public ActionResult PasswordChange(string key)
//        {
//            if (string.IsNullOrEmpty(key))
//            {
//                return RedirectToAction("Logon", "Account");
//            }

//            var tmp = key.Decrypt().Split('~');

//            if (tmp.Length < 3)
//                return RedirectToAction("Logon", "Account");

//            string companyCode = tmp[0];
//            string userName = tmp[1];
//            string membershipId = tmp[2];
//            string tc = tmp.Length > 3 ? tmp[3] : null;
//            string ia = tmp.Length > 4 ? tmp[4] : null;

//            // Check whether this membership id is locked for forced password reset
//            // if (TempData.ContainsKey("isAllowed") || TempData.ContainsKey("tenantCode") && (TempData["tenantCode"] != null))
//            if (!string.IsNullOrWhiteSpace(tc))
//            {
//                // if (!UserDetailsProxy.IsUserPasswordForcedForChange(membershipId, TempData["tenantCode"].ToString()))
//                if (!UserDetailsProxy.IsUserPasswordForcedForChange(membershipId, tc))
//                {
//                    // return RedirectToAction("LogOn", "Account");
//                }
//            }

//            ViewData["passExpired"] = false;

//            // Since this view is also used for the Forced Password Change, we are changing the 
//            // view page Title accordingly with the help of this temp variable
//            if (!string.IsNullOrWhiteSpace(ia))
//            {
//                int isAllowed = 0;
//                if (int.TryParse(ia, out isAllowed))
//                {
//                    if (isAllowed >= 1)
//                    {
//                        ViewData["passExpired"] = true;
//                    }
//                }
//            }

//            ViewData["companyCode"] = companyCode;
//            ViewData["userName"] = userName;
//            return View();
//        }

//        /// <summary>
//        /// This method is used to Update the user's new password with the database.
//        /// </summary>
//        /// <param name="companyCode">The company code.</param>
//        /// <param name="userName">Name of the user.</param>
//        /// <param name="currentPassword">The current password.</param>
//        /// <param name="newPassword">The new password.</param>
//        /// <param name="confirmPassword">The confirm password.</param>
//        /// <returns></returns>
//        [HttpPost]
//        public ActionResult PasswordChange(string companyCode, string userName, string currentPassword, string newPassword, string confirmPassword)
//        {
//            ViewData["companyCode"] = companyCode;
//            ViewData["userName"] = userName;
//            ViewData["Status"] = false;

//            if (string.IsNullOrEmpty(currentPassword))
//            {
//                ModelState.AddModelError("CurrentPassword", Resources.AccountResource.e_currentpasswordMandatory);
//            }

//            if (string.IsNullOrEmpty(confirmPassword))
//            {
//                ModelState.AddModelError("confirmPassword", Resources.AccountResource.e_confirmPasswordMandatory);
//            }

//            if (string.IsNullOrEmpty(newPassword))
//            {
//                ModelState.AddModelError("newPassword", Resources.AccountResource.e_newPasswordMandatory);
//            }

//            if (ModelState.IsValid)
//            {
//                if (!newPassword.Equals(confirmPassword))
//                {
//                    ModelState.AddModelError("ConfirmPassword", Resources.AccountResource.e_PasswordMismatch);
//                }
//                else if (currentPassword.Equals(newPassword))
//                {
//                    // If the newly reset and the old passwords are the same, reenter the passwords.
//                    ModelState.AddModelError("NewPassword", Resources.AccountResource.e_SamePassword);
//                }
//                else
//                {
//                    try
//                    {
//                        string tenantId = TenantProxy.GetTenantIdFromTenantCode(companyCode);

//                        if (ValidateChangePassword(newPassword: newPassword, confirmPassword: confirmPassword, currentPassword: currentPassword))
//                        {
//                            PasswordValidationStatus passwordValidationStatus = PasswordValidationProxy.IsPasswordValid(newPassword, confirmPassword);

//                            if (!passwordValidationStatus.IsPasswordValid)
//                            {
//                                ModelState.AddModelError("Password", passwordValidationStatus.Message);
//                            }
//                            else
//                            {
//                                if (UserDetailsProxy.ChangePassword(userName: userName, oldPassword: currentPassword, tenantId: tenantId, newPassword: newPassword))
//                                {
//                                    ModelState.AddModelError("Success", passwordValidationStatus.Message);
//                                    ViewData["Status"] = true;

//                                    var userDetails = UserDetailsProxy.GetUserDetailsByName(userName, tenantId);

//                                    if (ConfigHelper.CanTraceUserActivity() && UserActivityProxy.IsSessionActive(userDetails.User.UserId))
//                                    {
//                                        return RedirectToAction("LogOn", "Account");
//                                    }
//                                    var userRoles = new List<string>();

//                                    // fetch tenant roles, privileges and license and set form auth ticket cookies
//                                    if (this.PrepareTenantRolesAndPrivileges(userDetails, userRoles))
//                                    {
//                                        EventRegisterProxy.RegisterEvent(
//                                            new Event
//                                            {
//                                                EventId = CelloEventConstant.LoginEventId,
//                                                TenantId = userDetails.MembershipDetails.TenantCode,
//                                                UserId = userDetails.User.UserId,
//                                                SubjectId = userDetails.User.UserId,
//                                                SubjectType = "UserDetails",
//                                                SubjectXmlValue = userDetails.SerializeToXml()
//                                            });

//                                        TenantContext.Clear();

//                                        return GetHomePageRedirectUrl();
//                                    }
//                                }
//                                else
//                                {
//                                    ModelState.AddModelError("CurrentPassword", Resources.AccountResource.e_CurrentPassword);
//                                }
//                            }
//                        }
//                    }
//                    catch (UserDetailException userDetailsException)
//                    {
//                        ExceptionService.HandleException(userDetailsException, DefaultPolicy);
//                        ModelState.AddModelError("CurrentPassword", Resources.AccountResource.e_CurrentPassword);
//                    }
//                    catch (InvalidUserNameException invalidUserException)
//                    {
//                        ExceptionService.HandleException(invalidUserException, DefaultPolicy);
//                        ModelState.AddModelError("UserName", Resources.AccountResource.e_InvalidUser);
//                    }
//                    catch (ArgumentException argException)
//                    {
//                        ExceptionService.HandleException(argException, DefaultPolicy);
//                        ModelState.AddModelError("Argument", argException.Message);
//                    }
//                }
//            }

//            ViewData["currentPassword"] = "";

//            return View();
//        }

//        /// <summary>
//        /// This method is to change the password by getting the current password and new password.
//        /// </summary>
//        /// <param name="currentPassword">The current password.</param>
//        /// <param name="newPassword">The new password.</param>
//        /// <param name="confirmPassword">The confirm password.</param>
//        /// <returns></returns>
//        [AcceptVerbs(HttpVerbs.Post)]
//        public ActionResult ChangePassword(string currentPassword, string newPassword, string confirmPassword)
//        {
//            try
//            {
//                if (ValidateChangePassword(currentPassword: currentPassword, newPassword: newPassword, confirmPassword: confirmPassword))
//                {
//                    if (currentPassword.Equals(newPassword))
//                    {
//                        ModelState.AddModelError("Error", Resources.AccountResource.e_SamePassword);
//                        return View();
//                    }

//                    PasswordValidationStatus passwordValidationStatus = PasswordValidationProxy.IsPasswordValid(newPassword, confirmPassword);

//                    if (passwordValidationStatus.IsPasswordValid)
//                    {
//                        if (UserDetailsProxy.ChangePassword(UserIdentity.Name, UserIdentity.TenantID, currentPassword, newPassword))
//                        {
//                            TempData["Success"] = Resources.AccountResource.s_CurrentPassword;
//                        }
//                        else
//                        {
//                            ModelState.AddModelError("Error", Resources.AccountResource.e_CurrentPassword);
//                        }
//                    }
//                    else
//                    {
//                        ModelState.AddModelError("Error", passwordValidationStatus.Message);
//                    }
//                }
//            }
//            catch (UserDetailException userDetailsException)
//            {
//                ExceptionService.HandleException(userDetailsException, DefaultPolicy);
//                ModelState.AddModelError("Error", Resources.AccountResource.e_CurrentPassword);
//            }
//            catch (InvalidUserNameException invalidUserException)
//            {
//                ExceptionService.HandleException(invalidUserException, DefaultPolicy);
//                ModelState.AddModelError("Error", Resources.AccountResource.e_InvalidUser);
//            }

//            return View();
//        }

//        /// <summary>
//        /// This is the page for change password success.
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult ChangePasswordSuccess()
//        {
//            return View();
//        }

//        #endregion

//        #region Validation Methods

//        /// <summary>
//        /// This is to validate the change password fields.
//        /// </summary>
//        /// <param name="currentPassword">The current password.</param>
//        /// <param name="newPassword">The new password.</param>
//        /// <param name="confirmPassword">The confirm password.</param>
//        /// <returns></returns>
//        private bool ValidateChangePassword(string currentPassword, string newPassword, string confirmPassword)
//        {
//            if (string.IsNullOrEmpty(currentPassword))
//            {
//                ModelState.AddModelError("currentPassword", Resources.AccountResource.e_currentpasswordMandatory);
//            }
//            if (string.IsNullOrEmpty(newPassword))
//            {
//                ModelState.AddModelError("newPassword", Resources.AccountResource.e_newPasswordMandatory);
//            }
//            if (string.IsNullOrEmpty(confirmPassword))
//            {
//                ModelState.AddModelError("confirmPassword", Resources.AccountResource.e_confirmPasswordMandatory);
//            }

//            if (!string.IsNullOrEmpty(newPassword) && !string.IsNullOrEmpty(confirmPassword))
//            {
//                if (!string.Equals(newPassword, confirmPassword, StringComparison.Ordinal))
//                {
//                    ModelState.AddModelError("newPassword", string.Empty);
//                    ModelState.AddModelError("confirmPassword", string.Empty);
//                    ModelState.AddModelError("Error", Resources.AccountResource.e_confirmPassword);
//                }
//            }

//            return ModelState.IsValid;
//        }
//        /// <summary>
//        /// This method is to validate the logon fields.
//        /// </summary>
//        /// <param name="userName">Name of the user.</param>
//        /// <param name="password">The password.</param>
//        /// <param name="companyCode">The company code.</param>
//        /// <returns></returns>
//        private bool ValidateLogOn(string userName, string password, string companyCode)
//        {
//            //check for a valid company code
//            if (string.IsNullOrEmpty(companyCode))
//            {
//                ModelState.AddModelError("companyCode", string.Empty);
//                ModelState.AddModelError("login", Resources.AccountResource.e_CompanyCode);
//            }

//            // check for valid username
//            if (string.IsNullOrEmpty(userName))
//            {
//                ModelState.AddModelError("username", "");
//                ModelState.AddModelError("login", Resources.AccountResource.e_EmailID);
//            }

//            // check for valid password
//            if (string.IsNullOrEmpty(password))
//            {
//                ModelState.AddModelError("password", "");
//                ModelState.AddModelError("login", Resources.AccountResource.e_Password);
//            }

//            return ModelState.IsValid;
//        }

//        /// <summary>
//        /// This method is to validate the Forget password page.
//        /// </summary>
//        /// <param name="username">The username.</param>
//        /// <returns></returns>
//        private bool ValidateForgotPassword(string username)
//        {
//            if (string.IsNullOrEmpty(username))
//            {
//                ModelState.AddModelError("emailId", Resources.AccountResource.Mandatory);
//            }

//            return ModelState.IsValid;
//        }

//        #endregion

//        #region IdentitySetting

//        /// <summary>
//        /// This method is to get the role ,privileges ,tenantId,UserName and set to user data and add to Cookies.
//        /// </summary>
//        /// <param name="userName">Name of the user.</param>
//        /// <param name="userId">The user ID.</param>
//        /// <param name="roles">The roles.</param>
//        /// <param name="privileges">The privileges.</param>
//        /// <param name="tenantId">The tenant id.</param>
//        /// <param name="sessionTenantId">The session tenant id.</param>
//        public static void SetAuthenticationTicketInfo(string userName, string userId, string[] roles, string tenantId, string sessionTenantId)
//        {
//            //set user identity details
//            var cookieData = new CookieData
//            {
//                userKey = userId,
//                userName = userName,
//                roles = roles,
//                tenantId = tenantId
//            };

//            cookieData.loggedInUserTenantId = tenantId;

//            if (!string.IsNullOrEmpty(sessionTenantId))
//            {
//                cookieData.sessiontenantid = sessionTenantId;
//            }

//            HttpCookie cookie = AuthenticationCookieHelper.GetAuthenticationCookie(cookieData);

//            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
//        }

//        public static void AddClaimsToCurrentPrincipal(string userName, string userId, string[] roles, string[] privileges, string tenantId, string sessionTenantId)
//        {
//            //set user identity details            
//            var identity = (IClaimsIdentity)((IClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identities[0];

//            identity.Claims.Add(new Claim(ClaimTypes.Name, userName));
//            identity.Claims.Add(new Claim(CelloSaaS.Library.CelloClaimTypes.UserId, userId));
//            identity.Claims.Add(new Claim(CelloSaaS.Library.CelloClaimTypes.TenantId, tenantId));
//            identity.Claims.Add(new Claim(CelloSaaS.Library.CelloClaimTypes.SessionTenantId, string.Empty));
//            identity.Claims.Add(new Claim(CelloSaaS.Library.CelloClaimTypes.LoggedInTenantId, tenantId));
//            identity.Claims.Add(new Claim(ClaimTypes.Role, string.Join(",", roles)));
//        }

//        #endregion

//        #region Privacy Policy
//        /// <summary>
//        /// Privacies the policy.
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult PrivacyPolicy()
//        {
//            return View();
//        }
//        #endregion

//        #region Help
//        /// <summary>
//        /// Helps this instance.
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult Help()
//        {
//            return View();
//        }
//        #endregion
//    }
//}