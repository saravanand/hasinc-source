﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.EventScheduler.Model;
using CelloSaaS.EventScheduler.ServiceContracts;
using CelloSaaS.EventScheduler.ServiceProxies;
using CelloSaaS.EventScheduler.Services;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.ProductAnalytics;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.Rules.Execution;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.ProductAnalytics;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.ProductAnalytics;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.Util.Library;
using CelloSaaS.View;
namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for auditing and reporting.
    /// </summary>
    [HandleError]
    public class AuditController : CelloController
    {
        private string TenantId = TenantContext.GetTenantId("_Audit");
        private const string DefaultPolicy = "GlobalExceptionLogger";

        public ActionResult Index()
        {
            return RedirectToAction("ProductAnalytics");
        }

        #region Product Usage Analytics
        /// <summary>
        /// This method is used to get the analytics dashboard based on the given time frame.
        /// </summary>
        /// <param name="from">from date.</param>
        /// <param name="to">to date.</param>
        /// <returns></returns>
        public ActionResult AnalyticsDashboard(DateTime? from, DateTime? to)
        {
            if (!to.HasValue || to.Value == DateTime.Today) to = DateTime.Now;
            if (!from.HasValue) from = to.Value.AddDays(-to.Value.Day).AddMonths(-2); // last three months

            if ((to.Value - from.Value).TotalDays > 100)
            {
                ModelState.AddModelError("Error", "Date period should be less than 100 days");
            }

            if (from.Value > to.Value)
            {
                ModelState.AddModelError("Error", "From date value should be less than To date value!");
            }

            if (to.Value > DateTime.Now) to = DateTime.Now;

            ViewBag.toDate = to.Value;
            ViewBag.fromDate = from.Value;

            return View();
        }

        /// <summary>
        /// This method is used to get the tenant usage statistics based on the given time frame.
        /// </summary>
        /// <param name="from">from date.</param>
        /// <param name="to">to date.</param>
        /// <returns></returns>
        public PartialViewResult TenantUsageStats(DateTime? from, DateTime? to)
        {
            try
            {
                if (!to.HasValue || to.Value == DateTime.Today) to = DateTime.Now;
                if (!from.HasValue) from = to.Value.AddDays(-to.Value.Day).AddMonths(-2); // last three months
                ViewBag.toDate = to.Value;
                ViewBag.fromDate = from.Value;

                var stats = ProductAnalyticsProxy.GetTenantUsageStatistics(from, to);
                ViewBag.Stats = stats;

                if (stats != null && stats.Count > 0)
                {
                    var tenantIds = stats.Keys.ToArray();
                    var tenantList = TenantProxy.GetTenantDetailsForShareUsers(tenantIds);
                    ViewBag.TenantList = tenantList;
                }
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to get the user usage statistics for the specified tenant based on the given time frame.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="from">from date.</param>
        /// <param name="to">to date.</param>
        /// <returns></returns>
        public PartialViewResult UserUsageStats(string tenantId, DateTime? from, DateTime? to)
        {
            try
            {
                if (!to.HasValue || to.Value == DateTime.Today) to = DateTime.Now;
                if (!from.HasValue) from = to.Value.AddDays(-to.Value.Day).AddMonths(-2); // last three months
                if (string.IsNullOrEmpty(tenantId)) tenantId = UserIdentity.TenantID;
                ViewBag.toDate = to.Value;
                ViewBag.fromDate = from.Value;

                var stats = ProductAnalyticsProxy.GetUserUsageStatictics(tenantId, from, to);
                ViewBag.Stats = stats;

                if (stats != null && stats.Count > 0)
                {
                    var userIds = stats.Keys.ToArray();
                    var userNames = UserDetailsProxy.GetUserNameForAudit(userIds);
                    ViewBag.UserNames = userNames;
                }
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            LoadTenantList(tenantId);

            return PartialView();
        }

        /// <summary>
        /// This method is used to get the module usage statistics for the specified tenant based on the given time frame.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="from">from date.</param>
        /// <param name="to">to date.</param>
        /// <returns></returns>
        public PartialViewResult ModuleUsageStats(string tenantId, DateTime? from, DateTime? to)
        {
            try
            {
                if (!to.HasValue || to.Value == DateTime.Today) to = DateTime.Now;
                if (!from.HasValue) from = to.Value.AddDays(-to.Value.Day).AddMonths(-2); // last three months
                ViewBag.toDate = to.Value;
                ViewBag.fromDate = from.Value;

                if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                {
                    tenantId = UserIdentity.TenantID;
                }

                var stats = ProductAnalyticsProxy.GetControllerUsageStatictics(tenantId, from, to);
                ViewBag.Stats = stats;
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            LoadTenantList(tenantId);

            return PartialView();
        }

        /// <summary>
        /// This method is used to get the browser statistics for the specified tenant based on the given time frame.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="from">from date.</param>
        /// <param name="to">to date.</param>
        /// <returns></returns>
        public ActionResult BrowserStats(string tenantId, DateTime? from, DateTime? to)
        {
            try
            {
                if (!to.HasValue || to.Value == DateTime.Today) to = DateTime.Now;
                if (!from.HasValue) from = to.Value.AddDays(-to.Value.Day).AddMonths(-2); // last three months

                if ((to.Value - from.Value).TotalDays > 100)
                {
                    ModelState.AddModelError("Error", "Date period should be less than 100 days");
                }

                if (from.Value > to.Value)
                {
                    ModelState.AddModelError("Error", "From date value should be less than To date value!");
                }

                ViewBag.toDate = to.Value;
                ViewBag.fromDate = from.Value;

                if (ModelState.IsValid)
                {
                    if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                    {
                        tenantId = UserIdentity.TenantID;
                    }

                    var result = ProductAnalyticsProxy.GetProductAnalyticsLog(new ProductAnalyticsSearchCondition
                    {
                        TenantId = tenantId,
                        FromDate = from,
                        ToDate = to
                    });

                    if (result != null && result.Count > 0)
                    {
                        var stats = result.Values.Where(x => !string.IsNullOrEmpty(x.BrowserDetails))
                            .Select(x =>
                            {
                                return Newtonsoft.Json.Linq.JObject.Parse(x.BrowserDetails);
                            });

                        ViewBag.CountryStats = IpAddressToCountry(result);
                        ViewBag.Result = result;
                        ViewBag.Stats = stats;
                    }
                }
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            LoadTenantList(tenantId);

            return View();
        }

        /// <summary>
        /// This method is used to get country based on the ip address.
        /// </summary>
        /// <param name="result">list of product analytics details.</param>
        /// <returns></returns>
        private static Dictionary<string, Tuple<IpMapResponse, int>> IpAddressToCountry(Dictionary<string, ProductAnalytics> result)
        {
            var data = result.Values.GroupBy(x => x.IPAddress);
            var ipList = new ConcurrentDictionary<string, Tuple<IpMapResponse, int>>();
            var ipMapUtility = new IpMapUtility(".\\Content\\GeoLite2-City.mmdb.gz");

            Parallel.ForEach(data, (item) =>
            {
                if (!ipList.ContainsKey(item.Key))
                {
                    ipList.TryAdd(item.Key, null);
                }
                IpMapResponse resp = GetCountryForIpAddress(ipMapUtility, item.Key);
                ipList[item.Key] = Tuple.Create<IpMapResponse, int>(resp, item.Count());
            });

            return ipList.ToDictionary(x => x.Key, y => y.Value);
        }

        /// <summary>
        /// This method is used to get the country for specified ip address.
        /// </summary>
        /// <param name="ipMapUtility">ip map utility.</param>
        /// <param name="ipAddress">ip address.</param>
        /// <returns></returns>
        private static IpMapResponse GetCountryForIpAddress(IpMapUtility ipMapUtility, string ipAddress)
        {
            try
            {
                var resp = ipMapUtility.GetDetails(ipAddress);

                if (resp != null)
                {
                    return resp;
                }
            }
            catch (Exception ex) { ExceptionService.HandleException(ex, null); }

            return null;
        }

        /// <summary>
        /// This method is used load all active tenant details.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        private void LoadTenantList(string tenantId)
        {
            try
            {
                var activeTenants = TenantProxy.GetAllActiveTenantDetails();

                if (activeTenants != null && activeTenants.Count > 0)
                {
                    var lst = activeTenants.OrderBy(x => x.Value.TenantName).Select(x => new SelectListItem
                    {
                        Text = x.Value.TenantName,
                        Value = x.Key,
                        Selected = x.Key == tenantId
                    }).ToList();

                    lst.Insert(0, new SelectListItem { Text = "All", Value = "", Selected = string.IsNullOrEmpty(tenantId) });

                    ViewBag.TenantSelectList = lst;
                }
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
        }

        /// <summary>
        /// This method is used to get the product analytics log details based on the search condition.
        /// </summary>
        /// <param name="search">search condition.</param>
        /// <param name="page">page number.</param>
        /// <param name="searchField">name of the search field.</param>
        /// <param name="searchValue">value of the search field.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="pageSize">page size.</param>
        /// <returns></returns>
        public ActionResult ProductAnalytics(ProductAnalyticsSearchCondition search, int? page, string searchField, string searchValue, string sortString, string sortDirection, int pageSize = 50)
        {
            Dictionary<string, ProductAnalytics> entries = null;
            int totalCount = 0;
            List<SelectListItem> lstTenantList = new List<SelectListItem>();

            try
            {
                sortString = string.IsNullOrEmpty(sortString) ? "ProductAnalytics_TimeStamp" : sortString;
                sortDirection = string.IsNullOrEmpty(sortDirection) ? SortExpressionConstants.Decending : sortDirection;

                search.TenantId = search.TenantId == null ? "-1" : search.TenantId;
                search.SortString = sortString;
                search.SortDirection = sortDirection;
                search.Offset = page.HasValue ? (page.Value * pageSize) - (pageSize - 1) : 0;
                search.PageSize = pageSize;

                var activeTenant = TenantProxy.GetAllActiveTenantDetails();
                lstTenantList.Add(new SelectListItem { Value = "-1", Text = "All", Selected = search.TenantId == "-1" ? true : false });

                foreach (var item in activeTenant.OrderBy(x => x.Value.TenantName))
                {
                    lstTenantList.Add(new SelectListItem { Value = item.Value.TenantCode, Text = item.Value.TenantName, Selected = search.TenantId == item.Value.TenantCode });
                }

                entries = ProductAnalyticsProxy.GetProductAnalyticsLog(search);
                totalCount = search.TotalCount;
            }
            catch (FormatException fex)
            {
                ExceptionService.HandleException(fex, DefaultPolicy);
                ModelState.AddModelError("Error", fex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            ViewData["TenantList"] = lstTenantList;
            ViewData["PageNumber"] = !page.HasValue ? 1 : (int)page;
            ViewData["SortString"] = sortString;
            ViewData["SortDirection"] = sortDirection;
            ViewData["TotalCount"] = totalCount;
            ViewData["PageSize"] = pageSize;
            ViewData["search"] = search;

            if (Request.IsAjaxRequest())
            {
                return PartialView("PartialProductAnalytics", entries != null ? entries.Values.ToList() : null);
            }

            return View(entries != null ? entries.Values.ToList() : null);
        }

        /// <summary>
        /// This method is used to get the product analytics date based on the tenant identifier and given time frame.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="fromDate">from date.</param>
        /// <param name="toDate">to date.</param>
        /// <param name="interval">time interval.</param>
        /// <returns></returns>
        [DisableTrackUsage]
        public JsonResult ProductUsageData(string tenantId, DateTime? fromDate, DateTime? toDate, int interval = 30)
        {
            try
            {
                if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                    tenantId = UserIdentity.TenantID;

                if (!fromDate.HasValue && !toDate.HasValue)
                {
                    fromDate = toDate = DateTime.Today;
                }
                else if (toDate.Value > DateTime.Now || (toDate.Value == DateTime.Today && toDate.Value != fromDate.Value))
                {
                    toDate = DateTime.Now;
                }

                if (toDate.Value >= fromDate.Value)
                {
                    var stats = ProductAnalyticsProxy.GetProductUsageStats(tenantId, fromDate, toDate, interval);

                    if (stats != null && stats.Count > 0)
                    {
                        string formatString = "MMM-d";

                        if (fromDate.Value == toDate.Value)
                        {
                            formatString = "HH:mm";
                        }

                        var data = new List<dynamic>();

                        foreach (var item in stats.OrderBy(x => x.Key))
                        {
                            int? val = item.Value;

                            if (formatString == "HH:mm" && item.Key > DateTime.Now && val == 0)
                            {
                                if (item.Key > DateTime.Now.AddHours(3))
                                {
                                    continue;
                                }

                                val = null;
                            }

                            data.Add(new { date = item.Key.ToString(formatString), value = val });
                        }

                        return Json(new
                        {
                            data = data,
                            fromDate = fromDate.Value.ToUIDateString(),
                            toDate = toDate.Value.ToUIDateString(),
                            interval = interval
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            return Json(new { data = string.Empty }, JsonRequestBehavior.AllowGet);
        }

        #endregion Product Usage Analytics
        /// <summary>
        /// This method is used to manages the business rules for tenant.
        /// </summary>
        /// <returns></returns>
        public ActionResult BusinessRules()
        {
            List<CelloSaaS.Rules.Utils.LogEntry> entries = null;
            if (RuleAudit.HasEntries)
                entries = RuleAudit.ToList();

            return View(entries);
        }

        /// <summary>
        /// This method is used to clear the rules entities.
        /// </summary>
        /// <returns></returns>
        public ActionResult ClearRulesEntries()
        {
            if (RuleAudit.HasEntries)
                RuleAudit.ClearEntries();
            return RedirectToAction("BusinessRules");
        }

        /// <summary>
        /// This method is used to get the job audits based on given search condition.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <param name="executionStatus">execution status.</param>
        /// <param name="startDate">start date.</param>
        /// <param name="endDate">end date.</param>
        /// <param name="jobName">job name.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="page">page number.</param>
        /// <param name="pageSize">page size.</param>
        /// <returns></returns>
        public ActionResult JobAudits(string eventId,
            bool? executionStatus, DateTime? startDate, DateTime? endDate, string jobName,
            string sortString, string sortDirection,
            int page = 1, int pageSize = 10)
        {
            IEnumerable<JobAudit> jobAudits = null;
            long totalCount = 0;

            try
            {
                LoadEventList(eventId);

                if (!string.IsNullOrEmpty(eventId))
                {
                    var result = JobAuditProxy.SearchJobAudits(new JobAuditSearchCondition
                    {
                        EventId = eventId,
                        TenantId = this.TenantId,
                        JobName = jobName,
                        ExecutionStatus = executionStatus,
                        StartTime = startDate,
                        EndTime = endDate,
                        SortString = sortString,
                        SortDirection = sortDirection,
                        PageNo = page,
                        PageSize = pageSize,
                        SetTotalCount = true
                    });

                    totalCount = result.TotalCount;
                    jobAudits = result.JobAuditsList != null ? result.JobAuditsList.Values : null;
                }
            }
            catch (JobAuditException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            ViewData["PageNumber"] = page;
            ViewData["SortString"] = sortString;
            ViewData["SortDirection"] = sortDirection;
            ViewData["TotalCount"] = totalCount;
            ViewData["PageSize"] = pageSize;
            ViewData["executionStatus"] = executionStatus;
            ViewData["startDate"] = startDate;
            ViewData["endDate"] = endDate;
            ViewData["jobName"] = jobName;
            ViewData["eventId"] = eventId;

            if (Request.IsAjaxRequest())
            {
                return PartialView("JobAuditsGrid", jobAudits);
            }

            return View(jobAudits);
        }

        /// <summary>
        /// This method is used to get the all event metadata based on the given event identifier.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        private void LoadEventList(string eventId)
        {
            var allEvents = EventServiceProxy.GetAllEvents(this.TenantId);

            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Value = string.Empty, Text = "--Select an Event--", Selected = string.IsNullOrEmpty(eventId) });

            if (allEvents != null && allEvents.Count > 0)
            {
                foreach (var item in allEvents)
                {
                    list.Add(new SelectListItem
                    {
                        Value = item.Key,
                        Text = item.Value.EventName,
                        Selected = (!string.IsNullOrEmpty(eventId) && item.Key.Equals(eventId))
                    });
                }
            }

            ViewData["Events"] = list;
        }

        /// <summary>
        /// This method is used to get the usage details based on the search condition.
        /// </summary>
        /// <param name="condition">search condition.</param>
        /// <returns></returns>
        public ActionResult UsageDetails(UsageSearchCondition condition)
        {
            if (condition == null)
                condition = new UsageSearchCondition();

            try
            {
                condition.TenantId = this.TenantId;
                ViewBag.Condition = condition;

                var result = MeteringProxy.SearchUsageDetails(condition);
                TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);
                if (tenantLicense != null && tenantLicense.LicenseUsageDetails != null)
                {
                    var totalUsageList = tenantLicense.LicenseUsageDetails.Values.ToDictionary(x => x.UsageCode, v => v.MaximumCapacityUsage);
                    ViewBag.TotalUsageList = totalUsageList;
                }
                if (result != null)
                {
                    ViewBag.UsageDetailsList = result.Result;
                    ViewBag.TotalCount = result.TotalCount;
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to get the metering log details based on the given search condition.
        /// </summary>
        /// <param name="condition">search condition.</param>
        /// <param name="page">page.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="partial">partial view.</param>
        /// <param name="StartDate">start date.</param>
        /// <param name="EndDate">end date.</param>
        /// <returns></returns>
        public ActionResult MeteringLogDetails(MeteringSearchCondition condition, int page = 1, int pageSize = 10, bool partial = false, string StartDate = null, string EndDate = null)
        {
            if (condition == null)
            {
                condition = new MeteringSearchCondition();
            }
            try
            {
                condition.PageNumber = page;
                condition.PageSize = pageSize;
                ViewBag.Condition = condition;
                ViewBag.PageNumber = page;
                ViewBag.PageSize = pageSize;
                ViewBag.SortString = condition.SortString ?? "MeteringLog_LoggedDate";
                ViewBag.SortDirection = condition.SortExpression ?? CelloSaaS.Model.SortExpressionConstants.Decending;
                ViewBag.Condition = condition;

                if (string.IsNullOrEmpty(condition.TenantId))
                {
                    condition.TenantId = this.TenantId;
                }

                if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
                {
                    condition.StartDate = DateTime.Parse(StartDate);
                    condition.EndDate = DateTime.Parse(EndDate);
                }

                if (!string.IsNullOrEmpty(condition.UsageCode)
                    && condition.StartDate != DateTime.MinValue
                    && condition.EndDate != DateTime.MinValue)
                {
                    ViewData["TotalUsageAmount"] = MeteringProxy.GetUsageAmount(new string[] { condition.UsageCode }, condition.StartDate, condition.EndDate, condition.TenantId);
                }

                var result = MeteringProxy.SearchMeteringLog(condition);

                if (result != null && result.TotalCount > 0)
                {
                    ViewBag.UsageDetailsList = result.Result.Values.ToList();
                    ViewBag.TotalCount = result.TotalCount;
                    LoadUserList(result.Result.Values.Select(c => c.LoggedBy).ToArray());
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            if (this.Request.IsAjaxRequest() || partial)
            {
                return PartialView("MeteringLogGrid");
            }

            ViewData["TenantId"] = condition.TenantId;
            ViewData["UsageCode"] = condition.UsageCode;

            return View(condition);
        }

        /// <summary>
        /// This method is used to get the user (membership name) by user identifier.
        /// </summary>
        /// <param name="userIDs">array of user identifiers.</param>
        public void LoadUserList(string[] userIDs)
        {
            try
            {
                ViewBag.UserList = UserDetailsProxy.GetUserNameForAudit(userIDs);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (TenantUserAssociationException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UserDetailException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
        }

        #region Billing Audits

        /// <summary>
        /// This method is used to search the bill audit based on the given search condition.
        /// </summary>
        /// <param name="searchCondition">search condition.</param>
        /// <returns></returns>
        public ActionResult BillingAudits(BillingAuditSearchCondition searchCondition)
        {
            IEnumerable<BillingAudit> audits = null;

            try
            {
                searchCondition = searchCondition ?? new BillingAuditSearchCondition();

                searchCondition.TenantId = Guid.Parse(this.TenantId);
                audits = BillingAuditProxy.SearchBillAudits(searchCondition);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(audits);
        }

        #endregion
    }
}
