﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Notification.Model;
using CelloSaaS.Notification.Model.Content;
using CelloSaaS.Notification.Model.Dispatch;
using CelloSaaS.Notification.ServiceProxies;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.Template.ServiceProxies;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for managing the bill.
    /// </summary>
    [HandleError]
    public class BillingController : CelloController
    {
        Guid TenantId = Guid.Parse(TenantContext.GetTenantId("_Billing"));

        private const string PolicyName = "BillingExceptionLogger";

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        #region Manage Payment account, api account, history

        /// <summary>
        /// This method is used to customize the bill charge/generation date.
        /// </summary>
        /// <returns></returns>
        public ActionResult CustomizeBillDay()
        {
            try
            {
                if (UserIdentity.IsInRole(RoleConstants.ProductAdmin) && ProductAdminConstants.IsProductAdminTenant)
                {
                    var result = BillingProxy.GetGlobalBillingSettings();
                    ViewData["BillingSettingsList"] = result;
                }
                else
                {
                    var tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId.ToString());

                    if (!string.IsNullOrEmpty(tenantLicense.PricePlanId))
                    {
                        var pricePlanId = Guid.Parse(tenantLicense.PricePlanId);

                        var parentTenantId = Guid.Parse(TenantRelationProxy.GetParentTenant(this.TenantId.ToString()).Identifier);
                        var pricePlan = BillingPlanProxy.GetPricePlan(pricePlanId, parentTenantId);

                        if (pricePlan == null)
                        {
                            ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidPricePlan);
                        }
                        else
                        {
                            ViewData["PricePlan"] = pricePlan;

                            var billingSetting = BillingProxy.GetBillingSetting(pricePlan.BillFrequency, this.TenantId);

                            ViewData["BillingSetting"] = billingSetting;
                        }
                    }
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to manage payment account.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManagePaymentAccount()
        {
            try
            {
                var tenantInfo = TenantProxy.GetTenantInfo(this.TenantId.ToString());

                if (tenantInfo == null) return HttpNotFound();

                if (tenantInfo.Types != null && tenantInfo.Types.Any(x => x.ID != TenantTypeConstants.OrgUnit))
                {
                    ViewBag.TenantInfo = tenantInfo;

                    var parentTenantId = Guid.Parse(tenantInfo.TenantDetails.ParentTenantId);

                    ViewBag.PayPalApiAccount = PaymentProxy.GetApiAccount(parentTenantId, PaymentAccountType.PayPal);
                    ViewBag.AuthorizeNetApiAccount = PaymentProxy.GetApiAccount(parentTenantId, PaymentAccountType.CreditCard);
                }

                if (!string.IsNullOrWhiteSpace(this.Request.QueryString["profileId"]))
                {
                    var profileId = this.Request.QueryString["profileId"].Decrypt();
                    PaymentProxy.SaveAuthorizeNetPaymentProfile(this.TenantId);

                    CheckAndUpdateTenantStatus(tenantInfo);

                    Response.Redirect(Url.Action("ManagePaymentAccount", "Billing"));
                    Response.SuppressContent = true;
                    Response.End();
                }

                PaymentAccount account = PaymentProxy.GetAccountDetails(this.TenantId);

                if (account != null && account is CreditCardAccount)
                {
                    var ccAccount = account as CreditCardAccount;

                    if (ccAccount == null || ccAccount.CreditCardInfo == null
                        || string.IsNullOrEmpty(ccAccount.StoreKey) || ccAccount.StoreKey.Split('/').Length != 2)
                    {
                        // if authorize.net payment entry flow is not successfull
                        // delete the partial account and create again
                        PaymentProxy.DeleteAccount(this.TenantId);
                        account = null;
                    }
                }

                ViewBag.AccountDetails = account;
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (TenantException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (TenantActivationException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to check and update tenant status.
        /// </summary>
        /// <param name="tenantInfo"></param>
        internal void CheckAndUpdateTenantStatus(Tenant tenantInfo)
        {
            // only SMB, Enterprise, Reseller
            if (tenantInfo.TenantDetails.ApprovalStatus == TenantApprovalStatus.WAITINGFORAPPROVAL
                && tenantInfo.Types != null && tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.SMB || x.ID == TenantTypeConstants.Enterprise || x.ID == TenantTypeConstants.Reseller))
            {
                var tenantId = tenantInfo.TenantDetails.TenantCode;
                var license = LicenseProxy.GetTenantLicense(tenantId);

                if (license != null && !string.IsNullOrEmpty(license.PricePlanId) && !license.TrialEndDate.HasValue)
                {
                    // no trial tenant, so ask him to provide Payment info if in pre-paid mode
                    var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(license.PricePlanId), Guid.Parse(tenantInfo.TenantDetails.ParentTenantId));

                    if (pricePlan != null)
                    {
                        var billSetting = BillingProxy.GetBillingSetting(pricePlan.BillFrequency, Guid.Parse(tenantId));

                        if (billSetting.Mode == BillCycleMode.Prepaid)
                        {
                            if (tenantInfo.TenantDetails.IsSelfRegistered)
                            {
                                // get actiavation key and activate
                                var activationDetails = TenantProxy.GetTenantActivationDetails(tenantId);

                                if (activationDetails != null && !string.IsNullOrEmpty(activationDetails.ActivationKey))
                                {
                                    TenantProxy.ActivateTenant(HttpUtility.UrlDecode(activationDetails.ActivationKey));
                                }
                            }
                            else
                            {
                                TenantProxy.UpdateApprovalStatus(tenantId, TenantApprovalStatus.APPROVED);
                            }

                            if (!ConfigHelper.ApplyGlobalBillSettingForPrepaidMode)
                            {
                                billSetting.TenantId = Guid.Parse(tenantId);
                                billSetting.ChargeDay = DateTime.Now.Day + (billSetting.ChargeDay - billSetting.StartDay);
                                billSetting.StartDay = DateTime.Now.Day;
                                BillingProxy.UpdateBillingSetting(billSetting);
                            }

                            // generate bill and debit
                            var invoice = BillingProxy.GenerateInvoice(Guid.Parse(tenantId));
                            ViewData["InvoiceDetails"] = invoice;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method is used to delete the payment account.
        /// </summary>
        /// <returns></returns>
        public ActionResult DeletePaymentAccount()
        {
            try
            {
                PaymentProxy.DeleteAccount(this.TenantId);
                TempData["Success"] = Resources.BillingResource.s_DeletePaymentAccount;
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("ManagePaymentAccount");
        }

        /// <summary>
        /// This method is used to manage credit card account type.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageCreditCardAccount()
        {
            try
            {
                CreditCardAccount account = PaymentProxy.GetAccountDetails(this.TenantId) as CreditCardAccount;

                if (account != null && !string.IsNullOrEmpty(account.StoreKey) && account.StoreKey.Split('/').Length == 2)
                {
                    return Content(Resources.BillingResource.e_CreditCard);
                }

                var profileId = PaymentProxy.CreateAuthorizeNetCustomerProfile(this.TenantId);

                string encProfileId = profileId.Encrypt();

                string returnUrl = Url.Action("ManagePaymentAccount", "Billing", new { profileId = encProfileId, area = "" }, this.HttpContext.Request.Url.Scheme);
                string returnUrlText = "Return";

                var token = PaymentProxy.GetAuthorizeNetHostedFormToken(this.TenantId, returnUrl, returnUrlText);

                ViewData["token"] = token;

                var tenant = TenantProxy.GetTenantDetailsByTenantId(this.TenantId.ToString(), this.TenantId.ToString());
                var parentTenantId = Guid.Parse(tenant.TenantDetails.ParentTenantId);
                var apiAccount = PaymentProxy.GetApiAccount(parentTenantId, PaymentAccountType.CreditCard);

                ViewData["form_action"] = apiAccount.Mode ? "https://secure.authorize.net/profile/manage" : "https://test.authorize.net/profile/manage";
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (InvalidCreditCardException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to validate the credit card account type.
        /// </summary>
        /// <param name="cardAccount">credit card number.</param>
        private void ValidateCreditCardAccount(CreditCardAccount cardAccount)
        {
            if (string.IsNullOrEmpty(cardAccount.Email) || !Util.ValidateEmailId(cardAccount.Email))
            {
                ModelState.AddModelError("CreditCardAccount.Email", Resources.BillingResource.e_EmailAddress);
            }

            var cardInfo = cardAccount.CreditCardInfo;

            if (string.IsNullOrEmpty(cardInfo.CardNumber))
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.CardNumber", Resources.BillingResource.e_CardNumber);
            }

            if (!string.IsNullOrEmpty(cardInfo.CardNumber) && cardInfo.CardNumber.Length > 16)
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.CardNumber", Resources.BillingResource.e_CardDigit);
            }

            if (string.IsNullOrEmpty(cardInfo.CCV))
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.CCV", Resources.BillingResource.e_CCVMandatory);
            }

            if (!string.IsNullOrEmpty(cardInfo.CCV) && cardInfo.CCV.Length > 4)
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.CCV", Resources.BillingResource.e_CCVDigit);
            }

            if (cardInfo.Address == null)
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address", Resources.BillingResource.e_AddressMandatory);
            }

            if (string.IsNullOrEmpty(cardInfo.Address.First))
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address.First", Resources.BillingResource.e_FirstName);
            }

            if (string.IsNullOrEmpty(cardInfo.Address.Last))
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address.Last", Resources.BillingResource.e_LastName);
            }

            if (string.IsNullOrEmpty(cardInfo.Address.Street))
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address.Street", Resources.BillingResource.e_Street);
            }

            if (!string.IsNullOrEmpty(cardInfo.Address.Street) && cardInfo.Address.Street.Length > 255)
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address.Street", Resources.BillingResource.e_StreetLength);
            }

            if (string.IsNullOrEmpty(cardInfo.Address.Zip))
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address.Zip", Resources.BillingResource.e_Zip);
            }

            if (!string.IsNullOrEmpty(cardInfo.Address.Zip) && cardInfo.Address.Zip.Length != 5)
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address.Zip", Resources.BillingResource.e_ZipLength);
            }

            if (!string.IsNullOrEmpty(cardInfo.Address.Phone) && !Util.ValidatePhoneNumber(cardInfo.Address.Phone))
            {
                ModelState.AddModelError("CreditCardAccount.CreditCardInfo.Address.Phone", Resources.BillingResource.e_PhoneNumber);
            }
        }

        /// <summary>
        /// This method is used for Pay pal pre-approval page.
        /// </summary>
        /// <param name="email">email identifier</param>
        /// <returns></returns>
        public PartialViewResult PayPalPreapproval(string email)
        {
            try
            {
                PayPalAccount account = PaymentProxy.GetAccountDetails(TenantId) as PayPalAccount;
                ViewBag.AccountDetails = account;

                if (string.IsNullOrEmpty(email) || !Util.ValidateEmailId(email))
                {
                    ModelState.AddModelError("Error", Resources.BillingResource.e_Email);
                }

                bool canCreatePreapprovalKey = ModelState.IsValid;

                if (account != null && account.PreapprovalDetails != null &&
                    account.PreapprovalDetails.Approved.HasValue && account.PreapprovalDetails.Approved.Value)
                {
                    if (string.IsNullOrEmpty(account.PreapprovalDetails.EndingDate) ||
                        (!string.IsNullOrEmpty(account.PreapprovalDetails.EndingDate) && Convert.ToDateTime(account.PreapprovalDetails.EndingDate) >= DateTime.Now))
                    {
                        canCreatePreapprovalKey = false;
                        ModelState.AddModelError("Error", Resources.BillingResource.e_PreapprovalKey);
                    }
                }

                if (canCreatePreapprovalKey)
                {
                    var preapprovalResponse = PaymentProxy.CreatePreapprovalRequest(new PreapprovalRequest
                    {
                        TenantId = this.TenantId,
                        Email = email,
                        CurrencyCode = ConfigHelper.BillingCurrency,
                        CancelUrl = Url.Action("CancelPreapproval", "Billing", new { tenantId = this.TenantId }, this.Request.Url.Scheme),
                        ReturnUrl = Url.Action("ApprovePreapproval", "Billing", new { tenantId = this.TenantId }, this.Request.Url.Scheme)
                    });

                    ViewBag.PreapprovalResponse = preapprovalResponse;
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used for PayPal redirects to this page when the use clicks Approve button.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult ApprovePreapproval(string tenantId)
        {
            try
            {
                if (!string.IsNullOrEmpty(tenantId) && tenantId.Equals(this.TenantId.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    var tenantInfo = TenantProxy.GetTenantInfo(tenantId);

                    if (tenantInfo == null) return HttpNotFound();

                    PayPalAccount account = PaymentProxy.GetAccountDetails(this.TenantId) as PayPalAccount;
                    ViewBag.AccountDetails = account;

                    if (account != null && account.PreapprovalDetails != null)
                    {
                        if (account.Status && account.PreapprovalDetails.Approved.HasValue && account.PreapprovalDetails.Approved.Value)
                        {
                            ModelState.AddModelError("Error", Resources.BillingResource.e_ActivatePaypal);
                        }
                        else if (!account.Status && account.PreapprovalDetails.Approved.HasValue && account.PreapprovalDetails.Approved.Value)
                        {
                            account.Status = true;
                            PaymentProxy.UpdateAccount(account);
                            ViewBag.Success = true;
                            CheckAndUpdateTenantStatus(tenantInfo);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Error", Resources.BillingResource.e_SetupPaypal);
                    }
                }
                else
                {
                    ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidRequest);
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used for PayPal redirects to this page when the use clicks Cancel button.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult CancelPreapproval(string tenantId)
        {
            try
            {
                if (string.IsNullOrEmpty(tenantId) || !tenantId.Equals(this.TenantId.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidRequest);
                }
                else
                {
                    PayPalAccount account = PaymentProxy.GetAccountDetails(TenantId) as PayPalAccount;
                    ViewBag.AccountDetails = account;

                    if (account != null && account.PreapprovalDetails != null)
                    {
                        if (account.Status && account.PreapprovalDetails.Approved.HasValue && account.PreapprovalDetails.Approved.Value)
                        {
                            ModelState.AddModelError("Error", Resources.BillingResource.e_AlreadyPaypalActivate);
                        }
                        else
                        {
                            PaymentProxy.DeleteAccount(TenantId);
                            ViewBag.Success = true;
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Error", Resources.BillingResource.e_HaveNotSetupPayPal);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to manage payment api account only product admin and reseller tenants.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManagePaymentApiAccount()
        {
            try
            {
                if (this.Request.HttpMethod == "POST")
                {
                    SaveApiAccount();
                }

                if (ViewBag.payPalAccount == null)
                    ViewBag.payPalAccount = PaymentProxy.GetApiAccount(this.TenantId, PaymentAccountType.PayPal);

                if (ViewBag.creditCardAccount == null)
                    ViewBag.creditCardAccount = PaymentProxy.GetApiAccount(this.TenantId, PaymentAccountType.CreditCard);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to validates and saves the api account.
        /// </summary>
        private void SaveApiAccount()
        {
            PaymentApiAccount apiAccount = new PaymentApiAccount();
            string prefix = this.Request["prefix"];
            TryUpdateModel(apiAccount, prefix);
            apiAccount.TenantId = this.TenantId;

            if (prefix == "PayPal")
                ViewBag.payPalAccount = apiAccount;
            else if (prefix == "CreditCard")
                ViewBag.creditCardAccount = apiAccount;

            if (string.IsNullOrEmpty(apiAccount.Email))
            {
                ModelState.AddModelError(prefix + ".Email", Resources.BillingResource.e_EmailMandatory);
            }
            else if (!Util.ValidateEmailId(apiAccount.Email))
            {
                ModelState.AddModelError(prefix + ".Email", Resources.BillingResource.e_Email);
            }

            if (string.IsNullOrEmpty(apiAccount.UserName))
            {
                ModelState.AddModelError(prefix + ".UserName", Resources.BillingResource.e_UserName);
            }

            if (string.IsNullOrEmpty(apiAccount.Password))
            {
                ModelState.AddModelError(prefix + ".Password", Resources.BillingResource.e_Password);
            }

            if (apiAccount.AccountType == PaymentAccountType.PayPal)
            {
                if (string.IsNullOrEmpty(apiAccount.Signature))
                {
                    ModelState.AddModelError(prefix + ".Signature", Resources.BillingResource.e_Signature);
                }

                if (string.IsNullOrEmpty(apiAccount.AppId))
                {
                    ModelState.AddModelError(prefix + ".AppId", Resources.BillingResource.e_ApplicationId);
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (apiAccount.Id == Guid.Empty)
                    {
                        PaymentProxy.CreateApiAccount(apiAccount);
                        ViewBag.Success = apiAccount.AccountType.ToString() + Resources.BillingResource.s_APIAccount;
                    }
                    else
                    {
                        PaymentProxy.UpdateApiAccount(apiAccount);
                        ViewBag.Success = apiAccount.AccountType.ToString() + Resources.BillingResource.s_UpdateAPIAccount;
                    }
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (InvalidPaymentAPIAccountException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (BillingException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }
        }

        /// <summary>
        /// This method is used to view the child payment audit details.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="page">page.</param>
        /// <returns></returns>
        public ActionResult ManageChildPaymentAudits(PaymentAuditSearchCondition condition, int pageSize = 10, int page = 1)
        {
            ViewData["ShowTenantIdColumn"] = true;

            var tenantList = GetChildTenants();
            ViewData["TenantList"] = tenantList;

            if (condition.TenantIds != null)
            {
                var tids = condition.TenantIds.ToList();
                tids.RemoveAll(x => x == Guid.Empty);
                condition.TenantIds = tids.ToArray();
            }

            if (condition.TenantIds == null || condition.TenantIds.Length == 0)
            {
                condition.TenantIds = tenantList.Keys.Select(x => Guid.Parse(x)).ToArray();
            }

            return PaymentAudits(condition, pageSize, page);
        }

        /// <summary>
        /// This method is used to view the self payment audit details.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="page">page.</param>
        /// <returns></returns>
        public ActionResult PaymentAudits(PaymentAuditSearchCondition condition, int pageSize = 10, int page = 1)
        {
            try
            {
                condition = condition ?? new PaymentAuditSearchCondition();

                if (condition.TenantIds != null)
                {
                    var tids = condition.TenantIds.ToList();
                    tids.RemoveAll(x => x == Guid.Empty);
                    condition.TenantIds = tids.ToArray();
                }

                if (condition.TenantIds == null || condition.TenantIds.Length == 0)
                {
                    condition.TenantIds = new Guid[] { this.TenantId };
                }

                condition.PageNo = page;
                condition.PageSize = pageSize;

                ViewBag.PageNumber = condition.PageNo;
                ViewBag.PageSize = condition.PageSize;

                ViewBag.SearchCondition = condition;

                var auditResult = PaymentProxy.SearchPaymentAudits(condition);

                if (auditResult != null)
                {
                    ViewBag.TotalCount = auditResult.TotalCount;
                    ViewBag.PaymentAudits = auditResult.Items;
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("PaymentAuditsGrid");
            }

            return View("PaymentAudits"); // don't change
        }

        /// <summary>
        /// This method is used to manage the bill setting.
        /// </summary>
        /// <param name="setting">settings details.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ManageBillSettings(BillingSetting setting)
        {
            if (setting == null)
            {
                return Json(new { Error = Resources.BillingResource.e_ValidData });
            }

            if (setting.ChargeDay <= 0 || setting.ChargeDay > 31)
            {
                return Json(new { Error = Resources.BillingResource.e_BillChargeDay });
            }

            if (setting.StartDay <= 0 || setting.StartDay > 31)
            {
                return Json(new { Error = Resources.BillingResource.e_StartDay });
            }

            try
            {
                if (UserIdentity.IsInRole(RoleConstants.ProductAdmin) && ProductAdminConstants.IsProductAdminTenant)
                {
                    setting.TenantId = null;
                }
                else
                {
                    setting.TenantId = this.TenantId;
                }

                if (setting.Id == Guid.Empty)
                {
                    BillingProxy.AddBillingSetting(setting);
                    return Json(new { Success = Resources.BillingResource.s_AddBillSetting });
                }
                else
                {
                    BillingProxy.UpdateBillingSetting(setting);
                    return Json(new { Success = Resources.BillingResource.s_UpdateBillSetting });
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
        }

        #endregion

        #region Manage Bills, Manual Line items and payments

        /// <summary>
        /// This method is used to view child tenant bills.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageChildBills()
        {
            Guid tenantId = this.TenantId;
            string viewTenantId = this.Request["viewTenantId"];
            ViewData["viewTenantId"] = viewTenantId;
            ViewData["viewChildBills"] = true;
            Dictionary<string, TenantDetails> tenantList = null;
            Invoice invoice = null;

            if (!string.IsNullOrEmpty(viewTenantId))
            {
                Guid.TryParse(viewTenantId, out tenantId);
            }

            tenantList = GetChildTenants();

            ViewData["TenantList"] = tenantList;

            if (tenantList != null && tenantList.ContainsKey(tenantId.ToString()))
            {
                invoice = LoadBill(tenantId);
            }

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("PartialCurrentInvoice", invoice);
            }

            return View("MyBills", invoice);
        }

        /// <summary>
        /// This method is used to retrieves stratified tenants for the current user
        /// having ViewChildInvoice privilege.
        /// </summary>
        /// <returns>collection of child tenants.</returns>
        private Dictionary<string, TenantDetails> GetChildTenants()
        {
            var childTenants = new Dictionary<string, TenantDetails>();
            var immediateChilds = TenantRelationProxy.GetAllImmediateChildTenants(this.TenantId.ToString());

            if (immediateChilds == null || immediateChilds.Count == 0)
                return childTenants;

            immediateChilds = immediateChilds.Where(x => x.Value.ApprovalStatus == TenantApprovalStatus.APPROVED)
                                             .ToDictionary(x => x.Key, y => y.Value);

            if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
            {
                return immediateChilds;
            }
            else
            {
                var privilegeTenants = AccessControlProxy.GetTenantAccessPrivileges(UserIdentity.UserId, new string[] { PrivilegeConstants.ViewChildInvoice });

                if (privilegeTenants != null && privilegeTenants.ContainsKey(PrivilegeConstants.ViewChildInvoice))
                {
                    var tenantList = privilegeTenants[PrivilegeConstants.ViewChildInvoice];
                    childTenants = tenantList.Where(x => immediateChilds.ContainsKey(x.Key)).ToDictionary(x => x.Key, y => y.Value);
                }
            }

            return childTenants;
        }

        /// <summary>
        /// This method is used to view self bill.
        /// </summary>
        /// <returns></returns>
        public ActionResult MyBills()
        {
            Guid tenantId = this.TenantId;

            Invoice invoice = LoadBill(tenantId);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("PartialCurrentInvoice", invoice);
            }

            return View(invoice);
        }

        /// <summary>
        /// This method used to load the invoice details based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        private Invoice LoadBill(Guid tenantId)
        {
            Invoice invoice = null;

            try
            {
                if (!tenantId.ToString().Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase))
                {
                    var currentTenant = TenantProxy.GetTenantInfo(tenantId.ToString());
                    ViewData["TenantDetails"] = currentTenant;

                    if (currentTenant != null)
                    {
                        LoadParentTenantAndLogo(currentTenant.TenantDetails.ParentTenantId);
                    }

                    var tenantLicense = LoadTenantLicenseAndPricePlan(tenantId);

                    var billStatisticsResult = BillingProxy.GetBillStatistics(new BillStatisticsSearchCondition
                    {
                        TenantIds = new Guid[] { tenantId },
                        OnlyOverdue = false
                    });

                    if (billStatisticsResult != null && billStatisticsResult.ContainsKey(tenantId))
                    {
                        ViewData["BillStatistics"] = billStatisticsResult[tenantId];
                    }

                    var prevInvoices = BillingProxy.SearchInvoices(new InvoiceSearchCondition
                    {
                        TenantIds = new Guid[] { tenantId }
                    });

                    invoice = BillingProxy.GetCurrentInvoice(tenantId);

                    if (invoice != null && prevInvoices != null && prevInvoices.Items != null)
                    {
                        prevInvoices.Items.Remove(invoice.Id);
                    }

                    ViewData["CurrentInvoice"] = invoice;
                    ViewData["InvoiceSearchResult"] = prevInvoices;

                    if (invoice != null)
                    {
                        var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(invoice.PackageId.ToString());
                        ViewData["PackageDetails"] = packageDetails;

                        DateTime startDate, endDate, chargeDate;
                        CelloSaaS.View.Extensions.GetStartAndEndDates(tenantLicense, out startDate, out endDate, out chargeDate);

                        var usageAmounts = MeteringProxy.GetUsageAmount(null, startDate, endDate, tenantId.ToString());
                        ViewData["UsageAmounts"] = usageAmounts;

                        var childUsageAmounts = MeteringProxy.GetChildTenantsUsageAmount(null, startDate, endDate, tenantId.ToString());
                        ViewData["ChildUsageAmounts"] = childUsageAmounts;
                    }
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return invoice;
        }

        /// <summary>
        /// This method is used to load tenant logo based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        private void LoadParentTenantAndLogo(string tenantId)
        {
            ViewData["ParentTenant"] = TenantProxy.GetTenantDetailsByTenantId(tenantId, tenantId);
            var tenantSetting = TenantSettingsProxy.GetTenantSettings(tenantId);
            string logo = string.Empty;

            if (tenantSetting != null
                && tenantSetting.Setting != null
                && tenantSetting.Setting.Attributes != null
                && tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.Logo))
            {
                logo = tenantSetting.Setting.Attributes[AttributeConstants.Logo];
            }

            ViewData["ParentTenantLogo"] = logo;
        }

        /// <summary>
        /// This method is used to loads the tenant license and price plans.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        private TenantLicense LoadTenantLicenseAndPricePlan(Guid tenantId)
        {
            TenantLicense tenantLicense = null;

            try
            {
                var licenseHistory = LicenseProxy.GetTenantLicenseHistory(tenantId.ToString());

                if (licenseHistory != null && licenseHistory.Count > 0)
                    tenantLicense = licenseHistory.First();

                if (tenantLicense != null && string.IsNullOrEmpty(tenantLicense.PricePlanId))
                {
                    ViewData["NoBillingPlan"] = true;
                }
                else if (tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value < DateTime.Now)
                {
                    ViewData["TenantDeactivated"] = true;
                }
                else
                {
                    DateTime startDate, endDate, chargeDate;
                    ViewData["BillCycleMode"] = CelloSaaS.View.Extensions.GetStartAndEndDates(tenantLicense, out startDate, out endDate, out chargeDate);
                    if (startDate == endDate) chargeDate = DateTime.MinValue;
                    ViewData["chargeDate"] = chargeDate;
                }

                ViewData["TenantLicense"] = tenantLicense;
                ViewData["LicenseHistory"] = licenseHistory;

                if (licenseHistory != null)
                {
                    var parentTenantId = Guid.Parse(TenantProxy.GetTenantDetailsByTenantId(tenantId.ToString(), this.TenantId.ToString()).TenantDetails.ParentTenantId);
                    var pricePlans = new Dictionary<Guid, PricePlan>();

                    foreach (var lic in licenseHistory.Where(x => !string.IsNullOrEmpty(x.PricePlanId)))
                    {
                        if (!pricePlans.ContainsKey(Guid.Parse(lic.PricePlanId)))
                        {
                            var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(lic.PricePlanId), parentTenantId);

                            if (pricePlan != null)
                            {
                                pricePlans.Add(pricePlan.Id, pricePlan);
                            }
                        }
                    }

                    ViewData["PricePlans"] = pricePlans;
                }
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return tenantLicense;
        }

        /// <summary>
        /// This method is used to view the previously generated invoices.
        /// </summary>
        /// <param name="searchCondition">search condition.</param>
        /// <returns></returns>
        public PartialViewResult PreviousInvoices(InvoiceSearchCondition searchCondition)
        {
            InvoiceSearchResult result = null;

            try
            {
                searchCondition = searchCondition ?? new InvoiceSearchCondition();

                if (searchCondition.TenantIds == null || searchCondition.TenantIds.Length == 0)
                {
                    searchCondition.TenantIds = new Guid[] { this.TenantId };
                }

                if (searchCondition.PageNo <= 0)
                {
                    searchCondition.PageNo = 1;
                }

                if (searchCondition.PageSize <= 0)
                {
                    searchCondition.PageSize = 6;
                }

                ViewData["SearchCondition"] = searchCondition;

                result = BillingProxy.SearchInvoices(searchCondition);

                ViewData["InvoiceSearchResult"] = result;
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to search the invoices.
        /// </summary>
        /// <param name="searchCondition">search condition.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="page">page.</param>
        /// <returns></returns>
        public ActionResult SearchInvoices(InvoiceSearchCondition searchCondition, int pageSize = 10, int page = 1)
        {
            InvoiceSearchResult result = null;
            List<Guid> tenantIds = new List<Guid>();
            Dictionary<string, TenantDetails> tenantList = null;
            string viewTenantId = this.Request["viewTenantId"];

            try
            {
                tenantList = GetChildTenants();

                var loggedInTenant = TenantProxy.GetTenantInfo(this.TenantId.ToString());

                if (tenantList == null)
                {
                    tenantList = new Dictionary<string, TenantDetails>();
                }

                Guid temp;
                if (string.IsNullOrWhiteSpace(viewTenantId) || !Guid.TryParse(viewTenantId, out temp))
                {
                    tenantIds.AddRange(tenantList.Keys.Select(x => Guid.Parse(x)));
                }
                else
                {
                    tenantIds.Add(Guid.Parse(viewTenantId));
                }

                searchCondition = searchCondition ?? new InvoiceSearchCondition();

                searchCondition.TenantIds = tenantIds.ToArray();

                searchCondition.PageNo = page;
                searchCondition.PageSize = pageSize;

                ViewData["PageNumber"] = searchCondition.PageNo;
                ViewData["PageSize"] = searchCondition.PageSize;

                ViewData["viewTenantId"] = viewTenantId;
                ViewData["TenantList"] = tenantList;
                ViewData["SearchCondition"] = searchCondition;

                if (tenantIds.Count > 0)
                {
                    result = BillingProxy.SearchInvoices(searchCondition);
                }

                if (result != null)
                {
                    ViewData["TotalCount"] = result.TotalCount;
                    ViewData["InvoiceList"] = result.Items;
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            LoadNotificationAndTemplate();

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("InvoiceSearchGrid", result != null ? result.Items : null);
            }

            return View();
        }

        /// <summary>
        /// This method is used to load notifications and template.
        /// </summary>
        private void LoadNotificationAndTemplate()
        {
            try
            {
                ViewBag.NotificationList = NotificationConfigurationProxy.GetNotificationMasterDetailsByTenantId(this.TenantId.ToString());
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
        }

        /// <summary>
        /// This method is used to retrieve the email audits for the given invoice identifier and notification identifier.
        /// </summary>
        /// <param name="notificationId">notification identifier.</param>
        /// <param name="invoiceId">invoice identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public PartialViewResult GetEmailAudits(string notificationId, string invoiceId, string tenantId)
        {
            try
            {
                var audits = NotificationAuditProxy.SearchNotificationAuditDetails(new NotificationAuditSearchCondition
                {
                    NotificationId = notificationId,
                    TrackId = invoiceId,
                    TenantId = tenantId
                });

                ViewBag.NotifyAudits = audits;
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to preview unpaid bills email for the given invoice identifier.
        /// </summary>
        /// <param name="notificationId">notification identifier.</param>
        /// <param name="invoiceIds">array of invoice of invoice identifiers.</param>
        /// <param name="tenantIds">array of tenant identifiers.</param>
        /// <returns></returns>
        public ActionResult PreviewUnPaidBillEmail(string notificationId, Guid[] invoiceIds, Guid[] tenantIds)
        {
            if (string.IsNullOrEmpty(notificationId))
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_SelectNotification);
            }

            if (invoiceIds == null || invoiceIds.Length == 0 || tenantIds == null || tenantIds.Length == 0)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_SelectBill);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string tenantId = this.TenantId.ToString();
                    var invoiceDetails = BillingProxy.GetInvoiceById(invoiceIds[0], tenantIds[0]);
                    var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(tenantIds[0].ToString(), tenantId);
                    var notificationDetails = NotificationServiceProxy.GetNotificationDetails(notificationId, tenantId);
                    var dispatch = notificationDetails.NotificationDestination as EmailDestination;

                    if (dispatch == null)
                    {
                        ModelState.AddModelError("Error", Resources.BillingResource.e_NotificationDispatch);
                    }

                    var notificationContent = notificationDetails.NotificationContent as TextMessageContent;

                    if (notificationContent == null)
                    {
                        ModelState.AddModelError("Error", Resources.BillingResource.e_NotificationContent);
                    }

                    if (ModelState.IsValid)
                    {
                        string subject = notificationContent.Subject;
                        string templateContent = notificationContent.Content != null ? notificationContent.Content.ToString() : string.Empty;

                        if (notificationContent.Template != null && notificationContent.Template.TemplateContentText != null)
                        {
                            templateContent = notificationContent.Template.TemplateContentText.ToString();
                        }

                        notificationContent.Content = new StringBuilder(templateContent);

                        var processedContent = TemplateProxy.GetProcessedContent(templateContent, tenantId, null, null, null, new object[] { invoiceDetails, tenantDetails });
                        var processedSubject = TemplateProxy.GetProcessedContent(subject, tenantId, null, null, null, new object[] { invoiceDetails, tenantDetails });

                        ViewBag.ToAddress = tenantDetails.ContactDetail.Email;
                        ViewBag.Subject = processedSubject;
                        ViewBag.BodyContent = processedContent;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to sends email to the selected invoice identifiers.
        /// </summary>
        /// <param name="notificationId">notification identifier.</param>
        /// <param name="invoiceIds">array of invoice identifiers.</param>
        /// <param name="tenantIds">array of tenant identifiers.</param>
        /// <returns></returns>
        public JsonResult SendUnPaidBillEmail(string notificationId, Guid[] invoiceIds, Guid[] tenantIds)
        {
            if (string.IsNullOrEmpty(notificationId))
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_SelectNotification);
            }

            if (invoiceIds == null || invoiceIds.Length == 0 || tenantIds == null || tenantIds.Length == 0)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_SelectBill);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string tenantId = this.TenantId.ToString();
                    var invTenantIds = new Dictionary<Guid, Guid>();
                    for (int i = 0; i < invoiceIds.Length; ++i) invTenantIds.Add(invoiceIds[i], tenantIds[i]);

                    var formIdentity = Thread.CurrentPrincipal.Identity.Clone<FormsIdentity>();
                    Task.Factory.StartNew(() =>
                    {
                        Parallel.ForEach(invTenantIds, (kvp) =>
                        {
                            Thread.CurrentPrincipal = new GenericPrincipal(formIdentity, null);

                            var notificationDetails = NotificationServiceProxy.GetNotificationDetails(notificationId, tenantId);
                            var dispatch = notificationDetails.NotificationDestination as EmailDestination;
                            var notificationContent = notificationDetails.NotificationContent as TextMessageContent;
                            notificationContent.Placeholder = new NotificationPlaceholder();

                            var invoiceDetails = BillingProxy.GetInvoiceById(kvp.Key, kvp.Value);
                            var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(kvp.Value.ToString(), tenantId);
                            string toEmail = tenantDetails.ContactDetail != null ? tenantDetails.ContactDetail.Email : null;
                            notificationContent.Placeholder.ObjectPlaceholders = new object[] { invoiceDetails, tenantDetails };

                            dispatch.RecipientAddress = toEmail;
                            dispatch.TrackId = kvp.Key.ToString();

                            NotificationServiceProxy.ProcessAndSendNotification(notificationId, tenantId, notificationContent, dispatch);
                        });
                    });

                    return Json(new { Success = true });
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return Json(new { Error = ModelState.ErrorString() });
        }

        /// <summary>
        /// This method is used to preview the given invoice.
        /// </summary>
        /// <param name="invoiceId">invoice identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult PreviewInvoice(Guid? invoiceId, Guid? tenantId)
        {
            if (!invoiceId.HasValue)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidInvoiceId);
            }

            if (!tenantId.HasValue)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidTenantId);
            }

            Invoice invoiceDetails = null;

            if (ModelState.IsValid)
            {
                try
                {
                    var tenantInfo = TenantProxy.GetTenantInfo(tenantId.Value.ToString());
                    ViewData["TenantDetails"] = tenantInfo;

                    LoadParentTenantAndLogo(tenantInfo.TenantDetails.ParentTenantId);

                    invoiceDetails = BillingProxy.GetInvoiceById(invoiceId.Value, tenantId.Value);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (BillingException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return View(invoiceDetails);
        }

        /// <summary>
        /// This method is used to edit the given invoice.
        /// </summary>
        /// <param name="invoiceId">invoice identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult ManageInvoice(Guid? invoiceId, Guid? tenantId)
        {
            if (!invoiceId.HasValue)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidInvoiceId);
            }

            if (!tenantId.HasValue)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidTenantId);
            }

            Invoice invoiceDetails = null;

            if (ModelState.IsValid)
            {
                try
                {
                    var tenantInfo = TenantProxy.GetTenantInfo(tenantId.Value.ToString());
                    ViewData["TenantDetails"] = tenantInfo;

                    LoadParentTenantAndLogo(tenantInfo.TenantDetails.ParentTenantId);

                    invoiceDetails = BillingProxy.GetInvoiceById(invoiceId.Value, tenantId.Value);

                    if (invoiceDetails != null)
                    {
                        if (invoiceDetails.TenantId.ToString().Equals(UserIdentity.TenantID, StringComparison.OrdinalIgnoreCase))
                        {
                            invoiceDetails = null;
                            ModelState.AddModelError("Error", Resources.BillingResource.e_EditChildBill);
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (BillingException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return View(invoiceDetails);
        }

        /// <summary>
        /// This method is used to updates the invoice user added line items and memo.
        /// </summary>
        /// <param name="invoiceId">invoice identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="Memo">memo.</param>
        /// <param name="lineItems">line items.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ManageInvoice(Guid? invoiceId, Guid? tenantId, string Memo, List<InvoiceLineItem> lineItems)
        {
            if (!invoiceId.HasValue)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidInvoiceId);
            }

            if (!tenantId.HasValue)
            {
                ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidTenantId);
            }

            Invoice invoiceDetails = null;

            if (ModelState.IsValid)
            {
                try
                {
                    var tenantInfo = TenantProxy.GetTenantInfo(tenantId.Value.ToString());
                    ViewData["TenantDetails"] = tenantInfo;

                    LoadParentTenantAndLogo(tenantInfo.TenantDetails.ParentTenantId);

                    invoiceDetails = BillingProxy.GetInvoiceById(invoiceId.Value, tenantId.Value);

                    if (invoiceDetails != null && invoiceDetails.InvoiceStatus == InvoiceStatus.Current)
                    {
                        if (invoiceDetails.LineItems == null)
                            invoiceDetails.LineItems = new List<InvoiceLineItem>();

                        if (invoiceDetails.LineItems.IsReadOnly)
                            invoiceDetails.LineItems = new List<InvoiceLineItem>(invoiceDetails.LineItems);

                        // add the new user added line items
                        if (lineItems != null && lineItems.Count > 0)
                        {
                            foreach (var li in lineItems)
                            {
                                if (string.IsNullOrWhiteSpace(li.Name))
                                {
                                    ModelState.AddModelError("Error", Resources.BillingResource.e_LineItemMandatory);
                                }
                                else if (!Util.ValidateIdentifierWithSpace(li.Name))
                                {
                                    ModelState.AddModelError("Error", Resources.BillingResource.e_InvalidLineItem);
                                }
                                else if (li.Name.Length > 255)
                                {
                                    ModelState.AddModelError("Error", Resources.BillingResource.e_LineItemNameLength);
                                }

                                if (li.Id == Guid.Empty)
                                    li.Id = Guid.NewGuid();

                                li.Type = "UserAdded";
                                li.IsUserAdded = true;
                                if (!invoiceDetails.LineItems.Any(x => x.Id == li.Id))
                                {
                                    invoiceDetails.LineItems.Add(li);
                                }
                            }
                        }

                        if (ModelState.IsValid)
                        {
                            BillingProxy.AddInvoiceLineItem(invoiceDetails.Id, invoiceDetails.TenantId, lineItems);

                            if (invoiceDetails.Memo != Memo)
                            {
                                BillingProxy.UpdateInvoiceMemo(invoiceDetails.Id, invoiceDetails.TenantId, Memo);
                            }

                            TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.BillingResource.s_UpdateInvoice, invoiceDetails.InvoiceNo);
                            return RedirectToAction("ManageChildBills", new { viewTenantId = invoiceDetails.TenantId });
                        }
                    }
                    else if (invoiceDetails != null && ModelState.IsValid && invoiceDetails.Memo != Memo)
                    {
                        BillingProxy.UpdateInvoiceMemo(invoiceDetails.Id, invoiceDetails.TenantId, Memo);

                        TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.BillingResource.s_UpdateInvoiceNotes, invoiceDetails.InvoiceNo);
                        return RedirectToAction("ManageChildBills", new { viewTenantId = invoiceDetails.TenantId });
                    }
                    else
                    {
                        ModelState.AddModelError("Error", Resources.BillingResource.e_InvoiceDetailsNotFound);
                    }
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (BillingException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return View(invoiceDetails);
        }

        /// <summary>
        /// This method is used to add payment to the given invoice.
        /// </summary>
        /// <param name="invoiceId">invoice identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="payment">payment details.</param>
        /// <returns></returns>
        public JsonResult AddInvoicePayment(Guid? invoiceId, Guid? tenantId, InvoicePayment payment)
        {
            if (invoiceId == null || tenantId == null || payment == null)
            {
                return Json(new { Error = Resources.BillingResource.e_InvalidInput });
            }

            if (!ModelState.IsValid)
            {
                return Json(new { Error = ModelState.ErrorString() });
            }

            try
            {
                payment.InvoiceId = invoiceId.Value;
                payment.TenantId = tenantId.Value;
                BillingProxy.AddInvoicePayment(payment);
                return Json(new { Success = Resources.BillingResource.s_PaymenyMade });
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to delete the given payment from the given invoice.
        /// </summary>
        /// <param name="invoiceId">invoice identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="paymentId">payment identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteInvoicePayment(Guid? invoiceId, Guid? tenantId, Guid? paymentId)
        {
            if (invoiceId == null || tenantId == null || paymentId == null)
            {
                return Json(new { Error = Resources.BillingResource.e_Input });
            }

            try
            {
                var invoiceDetails = BillingProxy.GetInvoiceById(invoiceId.Value, tenantId.Value);

                if (invoiceDetails == null)
                {
                    return Json(new { Error = Resources.BillingResource.e_InvoiceDetails });
                }

                if (invoiceDetails.Payments != null && invoiceDetails.Payments.Any(x => x.Id == paymentId))
                {
                    if (invoiceDetails.Payments.IsReadOnly)
                    {
                        invoiceDetails.Payments = new List<InvoicePayment>(invoiceDetails.Payments);
                    }

                    var payment = invoiceDetails.Payments.Single(x => x.Id == paymentId);
                    invoiceDetails.Payments.Remove(payment);

                    BillingProxy.UpdateInvoice(invoiceDetails);

                    return Json(new { Success = Resources.BillingResource.s_DeletePayment });
                }
                else
                {
                    return Json(new { Error = Resources.BillingResource.e_PaymentDetails });
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to download the invoice as PDF document.
        /// </summary>
        /// <param name="invoiceId">invoice identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ContentResult DownloadInvoice(Guid? invoiceId, Guid? tenantId)
        {
            if (invoiceId == null || tenantId == null)
            {
                return Content(Resources.BillingResource.e_InputParameter);
            }

            string errorMessage = string.Empty;

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";

            try
            {
                var tenantInfo = TenantProxy.GetTenantInfo(tenantId.Value.ToString());
                ViewData["TenantDetails"] = tenantInfo;

                LoadParentTenantAndLogo(tenantInfo.TenantDetails.ParentTenantId);

                var invoiceDetails = BillingProxy.GetInvoiceById(invoiceId.Value, tenantId.Value);

                if (invoiceDetails != null)
                {
                    Response.AddHeader("content-disposition", "attachment;filename=" + invoiceDetails.InvoiceNo + "_" + DateTime.Now.ToShortDateString() + ".pdf");
                    Response.ContentType = "application/pdf";

                    string htmlContent = RenderViewToString("~/Views/Billing/DownloadInvoiceTemplate.aspx", invoiceDetails, this.ControllerContext);


                    CelloSaaS.Utility.PDF.PdfConverterUtility.ConvertHtmlToPdf(htmlContent, Response.OutputStream);
                }
                else
                {
                    errorMessage = Resources.BillingResource.e_InvoiceDetails;
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            Response.Flush();

            // do not generate any other content
            Response.SuppressContent = true;

            // Directs the thread to finish, bypassing additional processing
            HttpContext.ApplicationInstance.CompleteRequest();

            return Content(errorMessage);
        }

        /// <summary>
        /// This method is used to helper function to render the aspx view to string.
        /// </summary>
        /// <typeparam name="T">model.</typeparam>
        /// <param name="viewPath">view path.</param>
        /// <param name="model">model.</param>
        /// <param name="controllerContext">controller context.</param>
        /// <returns></returns>
        protected string RenderViewToString<T>(string viewPath, T model, ControllerContext controllerContext)
        {
            using (var writer = new StringWriter())
            {
                var view = new WebFormView(controllerContext, viewPath);
                var vdd = new ViewDataDictionary<T>(model);
                foreach (var v in ViewData)
                {
                    vdd.Add(v.Key, v.Value);
                }
                var viewCxt = new ViewContext(controllerContext, view, vdd, new TempDataDictionary(), writer);
                viewCxt.View.Render(viewCxt, writer);
                return writer.ToString();
            }
        }

        #endregion

        #region Dashboard

        /// <summary>
        /// This method is used view the billing dashboard.
        /// </summary>
        /// <returns></returns>
        public ActionResult Dashboard()
        {
            var tenantInfo = TenantTypesProxy.GetTenantTypes(UserIdentity.TenantID);

            if (!tenantInfo.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller))
            {
                ModelState.AddModelError("Error", "Billing dashboard is available only for ISV and Reseller tenants.");
            }
            return View();
        }

        /// <summary>
        /// This method is used to view the total revenues for the tenants.
        /// </summary>
        /// <param name="year">year.</param>
        /// <param name="hideChart">hide chart (true/false).</param>
        /// <returns></returns>
        public PartialViewResult TotalRevenues(int? year, bool hideChart = false)
        {
            ViewBag.hideChart = hideChart;
            DateTime fromDate = DateTime.Today, toDate = DateTime.Today;

            try
            {
                if (!year.HasValue || !(year.Value <= DateTime.Now.Year & year.Value > DateTime.MinValue.Year))
                    year = DateTime.Now.Year;

                fromDate = DateTime.ParseExact("01/01/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact("12/31/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                ViewData["RevenueData"] = BillingProxy.GetRevenues(fromDate, toDate, null);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            ViewBag.year = year;
            ViewBag.fromDate = fromDate;
            ViewBag.toDate = toDate;

            return PartialView();
        }

        /// <summary>
        /// This method is used to view the top revenue tenants.
        /// </summary>
        /// <param name="year">year.</param>
        /// <param name="topCount">top count.</param>
        /// <param name="hideTable">hide chart (true/false).</param>
        /// <returns></returns>
        public PartialViewResult TopRevenueTenants(int? year, int topCount = 5, bool hideTable = false)
        {
            ViewBag.hideTable = hideTable;
            DateTime? fromDate = null, toDate = null;

            try
            {
                if (year.HasValue && year.Value <= DateTime.Now.Year & year.Value > DateTime.MinValue.Year)
                {
                    fromDate = DateTime.ParseExact("01/01/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    toDate = DateTime.ParseExact("12/31/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                }

                if (topCount <= 0 || topCount > 50)
                    topCount = 5;

                ViewData["TotalStats"] = BillingProxy.GetBillStatistics(new BillStatisticsSearchCondition
                {
                    TopTenants = topCount,
                    FromDate = fromDate,
                    ToDate = toDate
                });

                ViewData["TotalRevenue"] = BillingProxy.GetRevenues(fromDate, toDate, null);

                var tenantsRegs = GetActiveTenantsDuring(toDate);
                ViewData["TenantList"] = tenantsRegs;
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            ViewBag.year = year;
            ViewBag.topCount = topCount;
            ViewBag.fromDate = fromDate;
            ViewBag.toDate = toDate;

            return PartialView();
        }

        /// <summary>
        /// This method is used to view the tenant bill statistics as table/chart.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <returns></returns>
        public PartialViewResult RevenuesByTenants(int? year, BillStatisticsSearchCondition condition)
        {
            try
            {
                if (condition == null)
                {
                    condition = new BillStatisticsSearchCondition();
                }

                if (year.HasValue && year.Value <= DateTime.Now.Year & year.Value > DateTime.MinValue.Year)
                {
                    condition.FromDate = DateTime.ParseExact("01/01/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    condition.ToDate = DateTime.ParseExact("12/31/" + year, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                }

                var result = BillingProxy.GetBillStatistics(condition);

                var childTenants = TenantRelationProxy.GetAllImmediateChildTenants(this.TenantId.ToString());
                ViewData["TenantList"] = childTenants;
                ViewData["BillStatistics"] = result;
                ViewData["SearchCondition"] = condition;
                ViewData["year"] = year;
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to view the child tenants revenues by year wise as chart/table.
        /// </summary>
        /// <param name="chartType">type of chart.</param>
        /// <param name="selectedTenantIds">collection of selected tenant identifiers.</param>
        /// <param name="minYear">minimum year.</param>
        /// <param name="maxYear">maximum year.</param>
        /// <returns></returns>
        public PartialViewResult TenantRevenuesTrends(string chartType, string[] selectedTenantIds, int? minYear, int? maxYear)
        {
            if (!minYear.HasValue)
            {
                minYear = DateTime.Today.Year - 4;
            }

            if (!maxYear.HasValue)
            {
                maxYear = DateTime.Today.Year;
            }

            ViewBag.chartType = chartType;
            ViewBag.minYear = minYear;
            ViewBag.maxYear = maxYear;

            try
            {
                if (AccessControlProxy.CheckPermission(PrivilegeConstants.ViewInvoiceDashboard))
                {
                    var childTenants = TenantRelationProxy.GetAllImmediateChildTenants(this.TenantId.ToString());

                    ViewData["TenantList"] = childTenants;

                    if (selectedTenantIds == null && childTenants != null)
                    {
                        selectedTenantIds = childTenants.Keys.ToArray();
                    }

                    ViewBag.selectedTenantIds = selectedTenantIds;

                    var result = new Dictionary<string, Dictionary<Guid, BillStatistics>>();

                    foreach (var year in Enumerable.Range(minYear.Value, (maxYear.Value - minYear.Value) + 1))
                    {
                        var condition = new BillStatisticsSearchCondition();
                        condition.TenantIds = selectedTenantIds != null ? selectedTenantIds.Select(x => Guid.Parse(x)).ToArray() : null;
                        condition.FromDate = DateTime.ParseExact("01/01/" + year, "M/d/yyyy", CultureInfo.InvariantCulture);
                        condition.ToDate = DateTime.ParseExact("12/31/" + year, "M/d/yyyy", CultureInfo.InvariantCulture);

                        var b = BillingProxy.GetBillStatistics(condition);
                        result.Add(year.ToString(), b);
                    }

                    ViewData["BillStatistics"] = result;
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to get active tenants during the given period.
        /// </summary>
        /// <param name="toDate">to date.</param>
        /// <returns></returns>
        private Dictionary<string, TenantDetails> GetActiveTenantsDuring(DateTime? toDate)
        {
            var childTenants = TenantRelationProxy.GetAllImmediateChildTenants(this.TenantId.ToString());

            if (!toDate.HasValue)
                return childTenants;

            var tenantsRegs = new Dictionary<string, TenantDetails>();

            if (childTenants != null && childTenants.Count > 0)
            {
                foreach (var item in childTenants)
                {
                    if (item.Value.CreatedOn <= toDate)
                    {
                        tenantsRegs.Add(item.Key, item.Value);
                    }
                }
            }

            return tenantsRegs;
        }

        #endregion

        #region Tax Report

        /// <summary>
        /// This method is used to get the tax report based on the given month.
        /// </summary>
        /// <param name="month">month.</param>
        /// <returns></returns>
        public ActionResult TaxReport(int month = 0)
        {
            try
            {
                if (month < 1) month = DateTime.Today.Month;
                int year = month > DateTime.Now.Month ? DateTime.Today.Year - 1 : DateTime.Today.Year;

                ViewBag.currMonth = month;
                ViewBag.currYear = year;

                var fromDate = DateTime.ParseExact(month + "/1/" + year, "M/d/yyyy", CultureInfo.InvariantCulture);
                var toDate = fromDate.AddDays(DateTime.DaysInMonth(year, month));

                // get tax report for current month
                var stats = BillingProxy.GetTaxReports(fromDate, toDate);
                ViewBag.Stats = stats;

                var revenues = BillingProxy.GetRevenues(fromDate, toDate, null);
                ViewBag.Revenues = revenues;

                if (stats != null && stats.Count > 0)
                {
                    var tenantIds = stats.Keys.Select(x => x.ToString()).ToArray();
                    var tenantList = TenantProxy.GetTenantDetailsForShareUsers(tenantIds);
                    ViewBag.TenantList = tenantList;
                }
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        #endregion

        #region Test Usage Insert

        public ActionResult ManageUsageData(string usageCode, DateTime? usageDate, double amount = 1.0)
        {
            try
            {
                var tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId.ToString());

                if (string.IsNullOrEmpty(usageCode) && tenantLicense.LicenseUsageDetails != null
                    && tenantLicense.LicenseUsageDetails.Count > 0)
                {
                    usageCode = tenantLicense.LicenseUsageDetails.First().Value.UsageCode;
                }

                if (!usageDate.HasValue)
                    usageDate = DateTime.Now;

                ViewBag.Usages = tenantLicense.LicenseUsageDetails != null ? tenantLicense.LicenseUsageDetails.Values.ToList() : null;
                ViewBag.usageCode = usageCode;
                ViewBag.amount = amount;
                ViewBag.usageDate = usageDate;
                ViewBag.TenantLicense = tenantLicense;

                if (this.Request.HttpMethod == "POST")
                {
                    if (!string.IsNullOrEmpty(usageCode))
                    {
                        InsertLogMeteringDetails(this.TenantId.ToString(), usageCode, amount, usageDate.Value);
                        ModelState.AddModelError("Success", string.Format(CultureInfo.InvariantCulture, Resources.BillingResource.s_UsageInsert, usageCode, amount, usageDate));
                    }
                    else
                    {
                        ModelState.AddModelError("Error", Resources.BillingResource.e_ChooseUsage);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        public ActionResult DeleteMeterLog(string id)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var query = new StringBuilder();
                query.Append("UPDATE Metering SET Metering_UsageAmount = Metering_UsageAmount - (SELECT MeteringLog_Amount FROM MeteringLog WHERE MeteringLog_Id=@id AND MeteringLog_TenantId=@tenantId),")
                     .Append(" Metering_UpdatedOn = @updatedOn, Metering_UpdatedBy = @updatedBy WHERE Metering_UsageCode = (SELECT MeteringLog_UsageCode FROM MeteringLog WHERE MeteringLog_Id=@id AND MeteringLog_TenantId=@tenantId) AND Metering_TenantId = @tenantId; ")
                     .Append("DELETE FROM MeteringLog WHERE MeteringLog_Id=@id AND MeteringLog_TenantId=@tenantId;");

                var db = CelloSaaS.Library.DataAccessLayer.CelloDbFactory.CreateDataBase(CelloSaaS.Library.DAL.Constants.CelloSaaSConnectionString);
                var dbCommand = db.DbProviderFactory.CreateCommand();
                dbCommand.CommandText = query.ToString();

                DbHelper.AddParameter("@id", id, db, dbCommand);
                DbHelper.AddParameter("@tenantId", this.TenantId, db, dbCommand);
                DbHelper.AddParameter("@updatedBy", UserIdentity.UserId, db, dbCommand);
                DbHelper.AddParameter("@updatedOn", DateTime.Now, db, dbCommand);

                db.ExecuteNonQuery(dbCommand);

                scope.Complete();
            }

            return Redirect(this.Request.UrlReferrer.AbsoluteUri);
        }

        private static void InsertLogMeteringDetails(string tenantId, string usageCode, double amount, DateTime date)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var query = new StringBuilder("INSERT INTO MeteringLog(MeteringLog_UsageCode,MeteringLog_Amount,MeteringLog_TenantID,");
                query.Append(" MeteringLog_LoggedBy,MeteringLog_LoggedDate) OUTPUT Inserted.MeteringLog_ID")
                     .Append(" VALUES (@usageCode,@amount,@tenantId,@loggedBy,@date);");

                var db = CelloSaaS.Library.DataAccessLayer.CelloDbFactory.CreateDataBase(CelloSaaS.Library.DAL.Constants.CelloSaaSConnectionString);
                var dbCommand = db.DbProviderFactory.CreateCommand();
                dbCommand.CommandText = query.ToString();
                dbCommand.CommandType = System.Data.CommandType.Text;

                DbHelper.AddParameter("@usageCode", usageCode, db, dbCommand);
                DbHelper.AddParameter("@date", date, db, dbCommand);
                DbHelper.AddParameter("@tenantId", tenantId, db, dbCommand);
                DbHelper.AddParameter("@amount", amount, db, dbCommand);
                DbHelper.AddParameter("@loggedBy", UserIdentity.UserId, db, dbCommand);

                db.ExecuteScalar(dbCommand);

                IncrementMeterUsage(tenantId, usageCode, amount, date);

                scope.Complete();
            }
        }

        private static void IncrementMeterUsage(string tenantIdentifier, string usageCode, double amount, DateTime date)
        {
            var db = CelloSaaS.Library.DataAccessLayer.CelloDbFactory.CreateDataBase(CelloSaaS.Library.DAL.Constants.CelloSaaSConnectionString);

            var query = new StringBuilder("SELECT Metering_ID from Metering");
            query.Append(" WHERE Metering_TenantID = @tenantId AND Metering_UsageCode=@usageCode");

            var dbCommand = db.DbProviderFactory.CreateCommand();
            dbCommand.CommandText = query.ToString();

            DbHelper.AddParameter("@usageCode", usageCode, db, dbCommand);
            DbHelper.AddParameter("@tenantId", tenantIdentifier, db, dbCommand);

            string meteringId = Convert.ToString(db.ExecuteScalar(dbCommand));

            query = new StringBuilder();

            dbCommand = db.DbProviderFactory.CreateCommand();

            if (string.IsNullOrEmpty(meteringId))
            {
                query.Append(" INSERT INTO Metering (Metering_TenantID,Metering_UsageCode,Metering_UsageAmount,Metering_CreatedBy,Metering_CreatedOn,Metering_Status)");
                query.Append(" VALUES (@tenantId, @usageCode,@amount, @createdBy, @createdOn, 1)");

                DbHelper.AddParameter("@usageCode", usageCode, db, dbCommand);
                DbHelper.AddParameter("@tenantId", tenantIdentifier, db, dbCommand);
                DbHelper.AddParameter("@createdBy", UserIdentity.UserId, db, dbCommand);
                DbHelper.AddParameter("@createdOn", date, db, dbCommand);
            }
            else
            {
                query.Append("UPDATE Metering SET Metering_UsageAmount = Metering_UsageAmount + @amount,")
                     .Append(" Metering_UpdatedOn = @updatedOn, Metering_UpdatedBy = @updatedBy WHERE Metering_ID = @meteringId ");

                DbHelper.AddParameter("@meteringId", meteringId, db, dbCommand);
                DbHelper.AddParameter("@updatedBy", UserIdentity.UserId, db, dbCommand);
                DbHelper.AddParameter("@updatedOn", date, db, dbCommand);
            }

            dbCommand.CommandText = query.ToString();
            DbHelper.AddParameter("@amount", amount, db, dbCommand);

            db.ExecuteNonQuery(dbCommand);
        }

        #endregion
    }
}
