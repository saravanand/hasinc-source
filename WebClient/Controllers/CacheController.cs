﻿using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.ServiceContracts;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for cache management.
    /// </summary>
    [HandleError]
    public class CacheController : CelloSaaS.View.CelloController
    {
        //
        // GET: /Cache/

        /// <summary>
        /// This method is used to refreshes the cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        public ActionResult RefreshCache()
        {
            return View();
        }

        /// <summary>
        /// This method is used to refreshes the rule cache.
        /// </summary>
        /// <returns>RefreshRuleCache View</returns>
        public ActionResult RefreshRuleCache()
        {
            CelloSaaS.Rules.Execution.Rules.ReInitialise();
            ViewData["Info"] = Resources.CacheResource.Rulescache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes all cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshAllCache()
        {
            CacheManager.Flush();
            CelloCacheManager.Flush();
            CelloSaaS.Rules.Execution.Rules.ReInitialise();

            ViewData["Info"] = Resources.CacheResource.Allcache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the workflow cache.
        /// </summary>
        /// <returns></returns>
        public ActionResult RefreshWorkflowCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.WorkflowModelCacheKey);
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.WorkflowDefinitionCacheKey);
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.WorkflowDefinitionUICacheKey);

            ViewData["Info"] = Resources.CacheResource.Workflowcache;

            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the data scope cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshDataScopeCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.DataScopeCacheKey);
            ViewData["Info"] = Resources.CacheResource.DataScopecache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the entity based data scope cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshEntityBasedDataScopeCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.EntityBasedDataScopeCacheKey);
            ViewData["Info"] = Resources.CacheResource.Entitydatascope;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the privilege cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshPrivilegeCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.PrivilegeCacheKey);
            ViewData["Info"] = Resources.CacheResource.Privilegecache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the entity meta data cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshEntityMetaDataCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.EntityMetaDataCacheKey);
            ViewData["Info"] = Resources.CacheResource.Entitymetadatacache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the extended fields cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshExtendedFieldsCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.ExtendedFieldsCacheKey);
            ViewData["Info"] = Resources.CacheResource.Extendedfieldscache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the base fields cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshBaseFieldsCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.BaseFieldsCacheKey);
            ViewData["Info"] = Resources.CacheResource.Basefieldscache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is sued to refreshes the tenant license cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshTenantLicenseCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.TenantLicenseCacheKey);
            ViewData["Info"] = Resources.CacheResource.Tenantlicensecache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the package cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshPackageCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.PackageCacheKey);
            ViewData["Info"] = Resources.CacheResource.Packagecache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the tenant settings cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshTenantSettingsCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.TenantSettingsCacheKey);
            ViewData["Info"] = Resources.CacheResource.Tenantsettingscache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the tenant cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshTenantCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.TenantCacheKey);
            ViewData["Info"] = Resources.CacheResource.Tenantcache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the data view cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshDataViewCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.DataViewCacheKey);
            ViewData["Info"] = Resources.CacheResource.Dataviewcache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the global default values cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshGlobalDefaultValuesCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.GlobalDefaultValuesCacheKey);
            ViewData["Info"] = Resources.CacheResource.Globaldefaultvalues;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the global fields cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshGlobalFieldsCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.GlobalFieldsCacheKey);
            ViewData["Info"] = Resources.CacheResource.Globalfieldscache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the data view field default values cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshDataViewFieldDefaultValuesCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.DataViewFieldDefaultValuesCacheKey);
            ViewData["Info"] = Resources.CacheResource.Dataviewfielddefaultvaluescache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the data view meta data cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshDataViewMetaDataCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.DataViewMetaDataCacheKey);
            ViewData["Info"] = Resources.CacheResource.Dataviewmetadatacache;
            return View("RefreshCache");
        }

        /* Added for newly added cache - START */
        /// <summary>
        /// This method is used to refreshes the countries cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [HttpPost]
        public ActionResult RefreshCountriesCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.CountryCacheKey);
            ViewData["Info"] = Resources.CacheResource.Countrycache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the role cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [HttpPost]
        public ActionResult RefreshRoleCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.RoleCacheKey);
            ViewData["Info"] = Resources.CacheResource.Rolescache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the default roles cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [HttpPost]
        public ActionResult RefreshDefaultRolesCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.DefaultRolesCacheKey);
            ViewData["Info"] = Resources.CacheResource.DefaultRolescache;
            return View("RefreshCache");
        }

        /// <summary>
        /// This method is used to refreshes the default roles cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [HttpPost]
        public ActionResult RefreshGlobalRolesCache()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.GlobalRolesCacheKey);
            ViewData["Info"] = Resources.CacheResource.GlobalRolescache;
            return View("RefreshCache");
        }


        /// <summary>
        /// This method is used to refreshes the global roles cache.
        /// </summary>
        /// <returns>RefreshCache View</returns>
        [HttpPost]
        public ActionResult RefreshTenantSettingsCacheWithAttributeId()
        {
            CelloCacheManager.RemoveFromCache(CacheKeyConstants.TenantSettingsCacheKeyWithAttributeId);
            ViewData["Info"] = Resources.CacheResource.Tenantsettingscacherefreshed;
            return View("RefreshCache");
        }

        /* Added for newly added cache - ENDS */
    }
}
