using System.Collections.Generic;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Model.Configuration;
using CelloSaaS.ServiceProxies.Configuration;

namespace CelloSaaSApplication.Controllers
{   
    /// <summary>
    /// This class is responsible for getting the child pickup list details.
    /// </summary>
    [HandleError]
    public class CascadePickupListController : CelloSaaS.View.CelloController
    {
        //private string PickupListTenantId = TenantContext.GetTenantId("parentId");

        /// <summary>
        /// This method is used to get child pickup list value details for parent pickup list identifier.
        /// </summary>
        /// <param name="parentPickupListId">parent pickup list identifier</param>
        /// <param name="parentPickupListValueItemId">parent pickup list value item identifier</param>
        /// <param name="childPickupListId">child pickup list identifier</param>
        /// <param name="parentTenantId">parent tenant identifier</param>
        /// <returns>JsonResult[Child Pickup list Values]</returns>
        public JsonResult GetChildPickupListDetails(string parentPickupListId, string parentPickupListValueItemId, string childPickupListId, string parentTenantId)
        {
            string PickupListTenantId = parentTenantId;
            //Get parent pickup list value details
            PickupListValue parentPickupListValue = PickupListProxy.GetPickupListValue(parentPickupListValueItemId, parentPickupListId, PickupListTenantId);
            //Check if parent pickup list value is not null and check child pickup list id is not null or empty
            if (parentPickupListValue != null && !string.IsNullOrEmpty(childPickupListId))
            {
                //Get Relationship details of parent pickup list id
                List<RelationshipMetaData> relationshipList = PickupListProxy.GetRelationshipMetaData(parentPickupListId, PickupListTenantId, Relation.Parent, string.Empty);
                //Check if parent pickup list has childrens
                if (relationshipList != null && relationshipList.Count > 0)
                {
                    //Create instance for pickup list filter and fill parent pickup list id and value id
                    PickupListFilter filter = new PickupListFilter { PickupListId = parentPickupListId, PickupListValueId = parentPickupListValue.Id };
                    foreach (RelationshipMetaData relation in relationshipList)
                    {
                        //Check if relationship child and passed child id is same then get child values
                        if (!childPickupListId.Trim().Equals(relation.SourceId.Trim()))
                        {
                            continue;
                        }
                        parentPickupListValue = PickupListProxy.FilterParentPickupList(relation.SourceId, relation.TenantId, filter);
                        if (parentPickupListValue != null && parentPickupListValue.Details != null && parentPickupListValue.Details.ItemArray != null)
                        {
                            return this.Json(parentPickupListValue.Details.ItemArray, JsonRequestBehavior.AllowGet);
                        }
                        return null;
                    }
                }
            }
            return null;
        }
    }
}
