﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using CelloSaaS.ChartBuilder.Model;
using CelloSaaS.ChartBuilder.ServiceContracts;
using CelloSaaS.ChartBuilder.ServiceContracts.Constants;
using CelloSaaS.Library;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.QueryBuilderLibrary.ServiceProxies;
using CelloSaaS.ReportBuilder.ServiceProxies;
using CelloSaaS.Reporting.DataSources;
using CelloSaaS.Reporting.DataSources.Model;
using CelloSaaS.Reporting.DataSources.Model.ViewModel;
using CelloSaaS.Reporting.DataSources.ServiceContracts;
using CelloSaaS.Reporting.DataSources.ServiceProxies;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Resources;

namespace CelloSaaSApplication.Controllers
{
    using CelloSaaS.Reporting.DataSources.Helpers;

    using Rectangle = iTextSharp.text.Rectangle;
    using CelloSaaSApplication.Models;
    using System.Globalization;
    using CelloSaaS.ChartBuilder.ServiceProxies;
    using CelloSaaS.View;
    /// <summary>
    /// This class is responsible for managing the chart details.
    /// </summary>
    public class ChartDetailsController : CelloController
    {
        private string TenantId = TenantContext.GetTenantId("_ChartBuilder");

        /// <summary>
        /// This method is used to get the DynamicVariables if any for this source.
        /// </summary>
        /// <param name="sourceId">source identifier.</param>
        /// <returns></returns>
        public JsonResult GetDynamicVariables(string sourceId)
        {
            if (string.IsNullOrEmpty(sourceId))
            {
                return Json(new { Error = Resources.ChartDetailsResource.e_SourceId });
            }

            List<DynamicVariable> dynamicVariables = new List<DynamicVariable>();

            var dynamicVars = QueryDetailsProxy.GetDynamicVariables(sourceId);

            if (dynamicVars == null || dynamicVars.Count < 1)
            {
                return Json(dynamicVariables);
            }

            foreach (var dynamicVar in dynamicVars)
            {
                dynamicVariables.Add(new DynamicVariable
                {
                    VariableId = Guid.NewGuid().ToString(),
                    Name = dynamicVar.Key,
                    Value = dynamicVar.Value
                });
            }

            return Json(dynamicVariables, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to gets the dynamic variables for chart.
        /// </summary>
        /// <param name="chartId">chart identifier.</param>
        /// <returns></returns>
        public JsonResult GetDynamicVariablesForChart(string chartId)
        {
            if (string.IsNullOrEmpty(chartId))
            {
                return Json(new { Error = Resources.ChartDetailsResource.e_ChartId });
            }

            ChartDetails chartDetails = ChartDetailsProxy.GetChartDetails(chartId, this.TenantId);

            if (chartDetails == null || string.IsNullOrEmpty(chartDetails.ChartSourceId))
            {
                return Json(new { Error = Resources.ChartDetailsResource.e_FetchDynamicVariables });
            }

            var dynamicVariables = DataSourceProxy.GetDynamicVariables(chartDetails.ChartSourceId, this.TenantId);

            if (dynamicVariables == null || dynamicVariables.Count < 1)
            {
                return Json(new { Message = Resources.ChartDetailsResource.e_NoDynamicVariables });
            }

            return Json((from dv in dynamicVariables
                         select new DynamicVariable { VariableId = Guid.NewGuid().ToString(), Name = dv.Key, Value = dv.Value }).ToList(), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// This method is used to gets the chart report object identifier.
        /// </summary>
        private static string ChartReportObjectId { get { return "C9B32C08-7A00-4FB7-9340-FF5984466EC1"; } }

        #region Public Methods

        /// <summary>
        /// This method is used to get the manage charts view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<ChartDetails> charts = null;

            try
            {
                var allCharts = new Dictionary<string, string>();

                try
                {
                    allCharts = ChartDetailsProxy.GetAllChartsList(this.TenantId);
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError("Error", exception.Message);
                }

                Dictionary<string, string> chartsList = new Dictionary<string, string>();

                if (allCharts != null && allCharts.Count > 0)
                {
                    chartsList.Add("-1", Resources.ChartDetailsResource.opt_ChooseChart);
                    foreach (var chart in allCharts)
                    {
                        if (!chartsList.ContainsKey(chart.Key))
                        {
                            chartsList.Add(chart.Key, chart.Value);
                        }
                    }
                }
                else
                {
                    chartsList.Add("-1", Resources.ChartDetailsResource.opt_NoChartAvailable);
                }

                //ViewData["ChartList"] = new SelectList(chartsList, "Key", "Value");

                if (TempData.ContainsKey("Success"))
                {
                    ViewData["Success"] = TempData["Success"].ToString();
                }

                if (TempData.ContainsKey("Error"))
                {
                    ViewData["Error"] = TempData["Error"].ToString();
                }

                charts = (from a in allCharts select new ChartDetails() { ChartId = a.Key, ChartName = a.Value }).ToList();

            }
            catch (Exception)
            {
                ModelState.AddModelError("", Resources.ChartDetailsResource.e_GetChartDetails);
            }
            return View(charts);
        }
        
        /// <summary>
        /// This method is used to returns chart details, options and chart data.
        /// </summary>
        /// <param name="chartId">chart identifier.</param>
        /// <returns></returns>
        public JsonResult GetPreviewChartDetails(string chartId)
        {
            if (string.IsNullOrEmpty(chartId))
            {
                return Json(new { Error = Resources.ChartDetailsResource.e_ChartId });
            }

            try
            {
                var chartDetails = ChartDetailsProxy.GetChartDetails(chartId, this.TenantId);

                if (chartDetails == null)
                {
                    return Json(new { Error =Resources.ChartDetailsResource.e_ChartDetailsNotFound });
                }

                var dataTable = DataSourceProxy.ExecuteSource(chartDetails.ChartSourceId, this.TenantId, null);

                var chartdata = GetJsonChartData(dataTable);

                return Json(new { details = chartDetails, data = chartdata });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to get the JSON chart data based on the given data table.
        /// </summary>
        /// <param name="dataTable">data table.</param>
        /// <returns></returns>
        private static string GetJsonChartData(DataTable dataTable)
        {
            var data = new List<Dictionary<string, object>>(dataTable.Columns.Count);

            foreach (System.Data.DataRow item in dataTable.Rows)
            {
                var colVals = new Dictionary<string, object>();

                for (int col = 0; col < item.ItemArray.Length; ++col)
                {
                    colVals.Add("field" + col.ToString(), item[col]);
                }

                data.Add(colVals);
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.None);
        }

        /// <summary>
        /// This method is used to gets the charts list.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetChartsList()
        {
            Dictionary<string, string> allCharts = ChartDetailsProxy.GetAllChartsList(this.TenantId);

            var res = from ac in allCharts
                      select new { ac.Key, ac.Value };

            return Json(res, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to deletes the chart.
        /// </summary>
        /// <param name="chartId">chart identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteChart(string chartId)
        {
            if (string.IsNullOrEmpty(chartId))
            {
                TempData["Error"] = Resources.ChartDetailsResource.e_Chart;
                return RedirectToAction("Index");
            }
            try
            {
                var dependentReports = ReportProxy.GetAllReportsBySourceId(this.TenantId, chartId);

                if (dependentReports != null && dependentReports.Count > 0)
                {
                    IEnumerable<string> mappedReportNames = dependentReports.Select(p => p.Value.Name);

                    TempData["Error"] = string.Format(Resources.ChartDetailsResource.e_ReportReference,
                                string.Join("','", mappedReportNames));

                    return RedirectToAction("Index");
                }

                ChartDetailsProxy.DeactivateChart(chartId);
            }
            catch (Exception exception)
            {
                TempData["Error"] = exception.Message;
                return RedirectToAction("Index");
            }

            TempData["Success"] = Resources.ChartDetailsResource.m_ChartDeleteSuccess;

            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to gets the data source columns.
        /// </summary>
        /// <param name="sourceId">source identifier.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetDataSourceColumns(string sourceId)
        {
            var colsList = new Dictionary<string, string>();
            colsList.Add("-1", Resources.ChartDetailsResource.opt_ChooseColumn);

            if (string.IsNullOrEmpty(sourceId))
            {
                return colsList;
            }

            DataSource ds = (DataSource)DataSourceProxy.GetSource(sourceId, this.TenantId);

            if (ds == null || string.IsNullOrEmpty(ds.SourceTypeId))
            {
                return colsList;
            }

            List<string> cols = GetSourceColumns(ds, this.TenantId);

            if (cols == null)
            {
                return colsList;
            }

            foreach (var col in cols)
            {
                if (colsList.ContainsKey(col))
                {
                    continue;
                }
                colsList.Add(col, col);
            }

            return colsList;
        }

       /// <summary>
        /// This method is used to gets the source columns.
       /// </summary>
       /// <param name="ds">data source</param>
       /// <param name="tenantId">tenant identifier.</param>
       /// <returns></returns>
        private static List<string> GetSourceColumns(DataSource ds, string tenantId)
        {
            List<string> cols = new List<string>();

            SourceContentViewModel scvm;
            Dictionary<string, string> sourceParameters = null;

            switch (ds.SourceTypeId)
            {
                case DataSourceConstants.TextQuerySource:
                    if (ds.DataSourceContent == null || string.IsNullOrEmpty(ds.DataSourceContent.Content))
                    {
                        break;
                    }
                    scvm = (SourceContentViewModel)SerializationHelper.DeSerializeObject<SourceContentViewModel>(ds.DataSourceContent.Content);
                    if (!scvm.parameters.IsNullOrEmpty())
                    {
                        sourceParameters = (from param in scvm.parameters
                                            select new { param.ParamName, param.ParamValue }).ToDictionary(t => t.ParamName, t => t.ParamValue);
                    }
                    cols = QueryMetaDataProxy.GetColumnMetaDataForQuery(scvm.SourceContent, DBHelpers.GetCelloSaaSConnectionStringName, sourceParameters);
                    break;
                case DataSourceConstants.StoredProcedureSource:
                    if (ds.DataSourceContent == null || string.IsNullOrEmpty(ds.DataSourceContent.Content))
                    {
                        break;
                    }

                    scvm = (SourceContentViewModel)SerializationHelper.DeSerializeObject<SourceContentViewModel>(ds.DataSourceContent.Content);

                    if (!scvm.parameters.IsNullOrEmpty())
                    {
                        sourceParameters = (from param in scvm.parameters
                                            select new { param.ParamName, param.ParamValue }).ToDictionary(t => t.ParamName, t => t.ParamValue);
                    }

                    cols = QueryMetaDataProxy.GetColumnMetaDataForSP(scvm.SourceContent, DBHelpers.GetCelloSaaSConnectionStringName, sourceParameters);
                    break;
                case DataSourceConstants.BuiltInQuerySource:
                    cols = QueryMetaDataProxy.GetColumnMetaDataForBuiltInQuery(ds.SourceContentId, DBHelpers.GetCelloSaaSConnectionStringName, tenantId);
                    break;
                default:
                    cols = null;
                    break;
            }
            return cols;
        }

        /// <summary>
        /// This method is used to gets all data sources.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllDataSources()
        {
            var ds = DataSourceProxy.GetAllSources(this.TenantId);

            var dsList = from d in ds
                         select new { d.Key, SourceName = d.Value.Name };

            return Json(dsList.ToArray(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to gets the column for data source.
        /// </summary>
        /// <param name="dataSourceId">data source identifier.</param>
        /// <returns></returns>
        public JsonResult GetColumnForDataSource(string dataSourceId)
        {
            if (string.IsNullOrEmpty(dataSourceId))
            {
                return null;
            }

            DataSource ds = (DataSource)DataSourceProxy.GetSource(dataSourceId, this.TenantId);

            if (ds == null || string.IsNullOrEmpty(ds.SourceTypeId))
            {
                ds = new DataSource
                {
                    SourceTypeId = DataSourceConstants.BuiltInQuerySource,
                    SourceContentId = dataSourceId,
                    DataSourceContent = null
                };
            }

            List<string> res = GetSourceColumns(ds, this.TenantId);

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to manages the charts.
        /// </summary>
        /// <param name="chartId">chart identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ManageCharts(string chartId)
        {
            try
            {
                InitChartDetailsView();

                ViewData["formMode"] = CurrentFormMode.Edit;

                if (string.IsNullOrEmpty(chartId))
                {
                    ViewData["formMode"] = CurrentFormMode.Insert;
                    return View();
                }

                ChartDetails chartDetails = ChartDetailsProxy.GetChartDetails(chartId, this.TenantId);

                if (chartDetails == null)
                {
                    return View();
                }

                PopulateChartDataSource(chartDetails);

                PopulateViewData(chartDetails);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", Resources.ChartDetailsResource.e_ManageChart);
            }

            return View();
        }

        /// <summary>
        /// This method is used to previews the chart.
        /// </summary>
        /// <param name="chartId">chart identifier.</param>
        /// <returns></returns>
        public ActionResult PreviewChart(string chartId, List<DynamicVariable> dynamicVariablesList)
        {
            try
            {
                if (string.IsNullOrEmpty(chartId))
                {
                    return null;
                }

                var dynamicVariables = FillDynamicVariableValues(dynamicVariablesList);

                PrepareForPreview(chartId, null, dynamicVariables);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", Resources.ChartDetailsResource.e_PreviewChart);
            }
            return PartialView("DisplayChart");
        }

        /// <summary>
        /// This method is used to manages the charts.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageCharts(FormCollection formCollection)
        {
            try
            {
                ValidateChartData(formCollection);

                ViewData["reportObjectId"] = ChartReportObjectId;

                ChartDetails chartDetail = new ChartDetails
                {
                    ChartName = formCollection["ChartDetails_Name"],
                    ChartType = formCollection["SeriesChartTypes"],
                    Status = true,
                    TenantId = this.TenantId
                };

                CheckAndAddDataSource(formCollection, chartDetail);

                if (!string.IsNullOrEmpty(formCollection["ChartDetails_ChartId"]))
                {
                    chartDetail.ChartId = formCollection["ChartDetails_ChartId"];
                }

                GetChartPropertiesFromView(formCollection, chartDetail, this.TenantId);

                GetChartAxisFieldValuesFromInput(formCollection, chartDetail);

                chartDetail.OtherProperties = null;

                var dynamicVariables = FillDynamicVariableValues(formCollection);

                PopulateViewData(chartDetail);

                if (formCollection["showPreview"].Equals("1"))
                {
                    PrepareForPreview(null, chartDetail, dynamicVariables);
                    ViewData["showPreview"] = 1;
                    return View();
                }

                string chartId = null;

                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrEmpty(formCollection["ChartDetails_ChartId"]))
                    {
                        chartDetail.UpdatedBy = UserIdentity.UserId;
                        chartDetail.UpdatedOn = DateTime.Now;
                        ChartDetailsProxy.UpdateChartDetails(chartDetail);
                    }
                    else
                    {
                        chartDetail.AddedBy = UserIdentity.UserId;
                        chartDetail.AddedOn = DateTime.Now;
                        chartId = ChartDetailsProxy.AddChartDetails(chartDetail);
                    }

                }

                ViewData["chartDetails"] = chartDetail;

                ViewData["ChartDetails_ChartId"] = null;

                if (!string.IsNullOrEmpty(chartId))
                {
                    ViewData["ChartDetails_ChartId"] = chartId;
                }

                if (ModelState.IsValid)
                {
                    TempData["Success"] = ChartDetailsResource.m_ChartSaveSuccess;
                    return RedirectToAction("Index");
                }
            }
            catch (DuplicateChartNameException duplicateChartNameException)
            {
                ModelState.AddModelError("DuplicateNameException", duplicateChartNameException.Message);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("Error", exception.Message);
            }
            return View();
        }

        /// <summary>
        /// This method is used to get the dynamic variables from the view form collection.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns>dictionary of dynamic variables.</returns>
        private static Dictionary<string, string> FillDynamicVariableValues(FormCollection formCollection)
        {
            Dictionary<string, string> dynamicVariables = new Dictionary<string, string>();

            var totalCount = formCollection["dynamicVariablesCount"] == null ? 0 : int.Parse(formCollection["dynamicVariablesCount"], CultureInfo.InvariantCulture);

            for (int i = 0; i < totalCount; i++)
            {
                var nameIndex = string.Format(CultureInfo.InvariantCulture, "dv[{0}].Name", i);
                var valueIndex = string.Format(CultureInfo.InvariantCulture, "dv[{0}].Value", i);

                string key = formCollection[nameIndex];
                string value = formCollection[valueIndex];

                if ((!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value)) && !dynamicVariables.ContainsKey(key))
                {
                    dynamicVariables.Add(key, value);
                }
            }

            if (dynamicVariables.Count > 0)
            {
                return dynamicVariables;
            }

            return null;
        }

        /// <summary>
        /// This method is used to get the dynamic variables from the view form collection.
        /// </summary>
        /// <param name="dynamicVariablesList">dynamic variable list.</param>
        /// <returns></returns>
        private static Dictionary<string, string> FillDynamicVariableValues(List<DynamicVariable> dynamicVariablesList)
        {
            if (dynamicVariablesList == null || dynamicVariablesList.Count < 1)
            {
                return null;
            }

            return (from dynamicVariable in dynamicVariablesList
                    select new { Key = dynamicVariable.Name, Value = dynamicVariable.Value }).ToDictionary(t => t.Key, t => t.Value);
        }

        /// <summary>
        /// This method is used to checks the and add data source.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <param name="chartDetail">chart detail.</param>
        private void CheckAndAddDataSource(FormCollection formCollection, ChartDetails chartDetail)
        {
            if (string.IsNullOrEmpty(formCollection["ChartDataSources"]))
            {
                return;
            }

            string sourceContentId = formCollection["ChartDataSources"];

            var matchedDataSources = DataSourceProxy.GetSourcesByContentId(sourceContentId, this.TenantId);

            if (matchedDataSources != null && matchedDataSources.Count > 0)
            {
                var dataSource = (DataSource)matchedDataSources.Values.ToList()[0];
                chartDetail.ChartSourceId = dataSource.Id;
                return;
            }

            var preBuiltQueries = QueryDetailsProxy.GetShallowQueryDetails(this.TenantId);

            if (preBuiltQueries == null || preBuiltQueries.Count < 1)
            {
                this.ModelState.AddModelError("Error", TableSourceResources.e_Invalid_DataSource);
            }

            var ds = new DataSource
                {
                    SourceContentId = sourceContentId,
                    SourceTypeId = DataSourceConstants.BuiltInQuerySource,
                    Status = true,
                    TenantId = this.TenantId,
                    CreatedBy = UserIdentity.UserId,
                    CreatedOn = DateTime.Now,
                    Name = (preBuiltQueries == null || !preBuiltQueries.ContainsKey(sourceContentId)) ? string.Empty : preBuiltQueries[sourceContentId],
                    DataSourceContent = null
                };

            chartDetail.ChartSourceId = ds.Id = DataSourceProxy.AddSource(ds);
        }

        /// <summary>
        /// This method is used to validates the chart data.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        private void ValidateChartData(FormCollection formCollection)
        {
            if (string.IsNullOrEmpty(formCollection["ChartDetails_Name"]))
            {
                ModelState.AddModelError("InvalidName", ChartDetailsResource.e_ChartTitle);
            }

            if (formCollection["SeriesChartTypes"] == "-1")
            {
                ModelState.AddModelError("InvalidSeriesType", ChartDetailsResource.e_ChartSeriesType);
            }

            if (formCollection["ChartDataSources"] == "-1")
            {
                ModelState.AddModelError("InvalidChartSource", ChartDetailsResource.e_ChartSource);
            }

            if (formCollection[ChartPropertyConstants.ChartType] == "-1")
            {
                ModelState.AddModelError("InvalidChartType", ChartDetailsResource.e_ChartType);
            }

            if (formCollection["XAxisColumns"] == "-1")
            {
                ModelState.AddModelError("InvalidXAxisColumns", ChartDetailsResource.e_XAxisColumn);
            }

            if (string.IsNullOrEmpty(formCollection["XAxisTitle"]))
            {
                ModelState.AddModelError("InvalidXAxisTitle", ChartDetailsResource.e_XAxisTitle);
            }

            if (formCollection["YAxisColumns"] == "-1")
            {
                ModelState.AddModelError("InvalidYAxisColumns", ChartDetailsResource.e_YAxisColumn);
            }

            if (string.IsNullOrEmpty(formCollection["YAxisTitle"]))
            {
                ModelState.AddModelError("InvalidYAxisTitle", ChartDetailsResource.e_YAxisTitle);
            }

            if (!string.IsNullOrEmpty(formCollection[ChartPropertyConstants.FontSize])
                && (int.Parse(formCollection[ChartPropertyConstants.FontSize], CultureInfo.InvariantCulture) < 5 || int.Parse(formCollection[ChartPropertyConstants.FontSize], CultureInfo.InvariantCulture) > 75))
            {
                ModelState.AddModelError("InvalidFontSize", ChartDetailsResource.e_FontSize);
            }

            if (!string.IsNullOrEmpty(formCollection[ChartPropertyConstants.Width]) &&
                int.Parse(formCollection[ChartPropertyConstants.Width], CultureInfo.InvariantCulture) < 100)
            {
                ModelState.AddModelError("InvalidWidth", ChartDetailsResource.e_ChartWidth);
            }

            if (!string.IsNullOrEmpty(formCollection[ChartPropertyConstants.Height]) && int.Parse(formCollection[ChartPropertyConstants.Height], CultureInfo.InvariantCulture) < 100)
            {
                ModelState.AddModelError("InvalidHeight", ChartDetailsResource.e_ChartHeight);
            }
        }

        /// <summary>
        /// This method is used to gets the name of the charts by.
        /// </summary>
        /// <param name="term">term.</param>
        /// <returns></returns>
        public JsonResult GetChartsByName(string term)
        {
            if (string.IsNullOrEmpty(term))
            {
                return null;
            }

            var chartDetails = ChartDetailsProxy.FindChartsByName(term, this.TenantId);

            if (chartDetails != null && chartDetails.Count > 0)
            {
                var results = from cd in chartDetails
                              select new
                              {
                                  id = cd.Key,
                                  label = cd.Value.ChartName,
                                  value = cd.Value.ChartName
                              };

                return Json(results, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        #endregion

        #region ChartPartialViews
        /// <summary>
        /// This method is used to bars the column.
        /// </summary>
        /// <param name="cfm">CFM.</param>
        /// <returns></returns>
        public ActionResult BarColumn(CurrentFormMode cfm)
        {
            if (cfm == CurrentFormMode.Insert)
            {
                BarColumnProperties();
            }
            return PartialView("BarColumn");
        }

        /// <summary>
        /// This method is used to lines the specified CFM.
        /// </summary>
        /// <param name="cfm">CFM.</param>
        /// <returns></returns>
        public ActionResult Line(CurrentFormMode cfm)
        {
            if (cfm == CurrentFormMode.Insert)
            {
                LineProperties();
            }
            return PartialView("Line");
        }

        /// <summary>
        /// This method is used to pies the specified CFM.
        /// </summary>
        /// <param name="cfm">CFM.</param>
        /// <returns></returns>
        public ActionResult Pie(CurrentFormMode cfm)
        {
            if (cfm == CurrentFormMode.Insert)
            {
                PieProperties();
            }
            return PartialView("Pie");
        }

        /// <summary>
        /// This method is used to points the specified CFM.
        /// </summary>
        /// <param name="cfm">CFM.</param>
        /// <returns></returns>
        public ActionResult Point(CurrentFormMode cfm)
        {
            if (cfm == CurrentFormMode.Insert)
            {
                PointProperties();
            }
            return PartialView("Point");
        }
        #endregion

        #region Chart Export Options

        /// <summary>
        /// This method is used to exports the chart.
        /// </summary>
        /// <param name="exportType">type of the export.</param>
        /// <param name="chartId">chart identifier.</param>
        /// <param name="dynamicVariableList">dynamic variable list.</param>
        /// <returns></returns>
        public FileResult ExportChart(string exportType, string chartId, List<DynamicVariable> dynamicVariableList)
        {
            //var routeValues = new { chartId = chartId, dynamicVariableList = dynamicVariableList };

            var exportOption = (ChartExportType)Enum.Parse(typeof(ChartExportType), exportType.ToUpperInvariant());

            switch (exportOption)
            {
                default:
                case ChartExportType.PDF:
                    return ExportChartAsPdf(chartId, dynamicVariableList);
                case ChartExportType.PNG:
                    return ExportChartAsImage(chartId, dynamicVariableList);
            }
        }

        /// <summary>
        /// This method is used to exports the chart as PDF.
        /// </summary>
        /// <param name="chartId">chart identifier.</param>
        /// <returns></returns>
        public FileResult ExportChartAsPdf(string chartId, List<DynamicVariable> dynamicVariableList)
        {
            if (string.IsNullOrEmpty(chartId))
            {
                return null;
            }

            DataTable resultDataTable;

            var dynamicVariables = FillDynamicVariableValues(dynamicVariableList);

            Chart c = CelloSaaS.ChartBuilder.ChartUtility.GetChartFromChartDetails(null, out resultDataTable, chartId, this.TenantId, dynamicVariables);

            var document = new Document();
            document.SetPageSize(new Rectangle((float)c.Width.Value + 200f, (float)c.Height.Value + 200f));

            string fileName = c.Titles[0].Text.Replace(" ", "_") + DateTime.Now.Ticks + ".pdf";

            PdfWriter.GetInstance(document, new FileStream(Server.MapPath("~/" + fileName), FileMode.Create));

            document.Open();

            using (var ms = new MemoryStream())
            {
                c.SaveImage(ms, ChartImageFormat.Png);
                ms.Seek(0, SeekOrigin.Begin);

                var chartImage = Image.GetInstance(ms.ToArray());

                chartImage.Alignment = Element.ALIGN_CENTER;

                document.Add(chartImage);

                document.Close();

                return File(Server.MapPath("~/" + fileName), "application/pdf", fileName);
            }
        }

        /// <summary>
        /// This method is used to exports the chart as image.
        /// </summary>
        /// <param name="chartId">chart identifier.</param>
        /// <returns></returns>
        public FileResult ExportChartAsImage(string chartId, List<DynamicVariable> dynamicVariableList)
        {
            if (string.IsNullOrEmpty(chartId))
            {
                return null;
            }

            DataTable resultDataTable;

            var dynamicVariables = FillDynamicVariableValues(dynamicVariableList);

            Chart c = CelloSaaS.ChartBuilder.ChartUtility.GetChartFromChartDetails(null, out resultDataTable, chartId, this.TenantId, dynamicVariables);

            string fileName = c.Titles[0].Text.Replace(" ", "_") + DateTime.Now.Ticks + ".png";

            using (var ms = new MemoryStream())
            {
                c.SaveImage(ms, ChartImageFormat.Png);
                ms.Seek(0, SeekOrigin.Begin);

                return File(ms.ToArray(), "image/png", fileName);
            }
        }
        #endregion

        #region Private Methods

        private void InitChartDetailsView(string dataSourceId = null, string chartType = null, string seriesChartType = null, string xAxisColumn = null, string yAxisColumn = null, string fontUnit = null, string heightUnit = null, string widthUnit = null)
        {
            ViewData["reportObjectId"] = ChartReportObjectId;

            if (string.IsNullOrEmpty(dataSourceId))
            {
                dataSourceId = "-1";
            }

            ViewData["ChartDataSources"] = new SelectList(ListDataSources(this.TenantId), "Key", "Value", dataSourceId);

            if (string.IsNullOrEmpty(chartType))
            {
                chartType = "-1";
            }
            ViewData[ChartPropertyConstants.ChartType] = new SelectList(GetAllChartTypes(), "Key", "Value", chartType);

            if (string.IsNullOrEmpty(seriesChartType))
            {
                seriesChartType = "-1";
            }
            ViewData["SeriesChartTypes"] = new SelectList(GetAllSeriesChartTypes(), "Key", "Value", seriesChartType);

            if (string.IsNullOrEmpty(xAxisColumn))
            {
                xAxisColumn = "-1";
            }
            ViewData["XAxisColumns"] = new SelectList(GetDataSourceColumns(null), "Key", "Value", xAxisColumn);

            if (string.IsNullOrEmpty(yAxisColumn))
            {
                yAxisColumn = "-1";
            }
            ViewData["YAxisColumns"] = new SelectList(GetDataSourceColumns(null), "Key", "Value", yAxisColumn);

            ViewData["SourceContentTypes"] = new SelectList(GetSourceConentTypes(), "Key", "Value", "-1");

            ViewData["preExistingSrc"] = new SelectList(GetPreExistingSources(null, this.TenantId), "Key", "Value", "-1");
        }

        private void PopulateChartDataSource(ChartDetails chartDetails)
        {
            DataSource chartDataSource = (DataSource)DataSourceProxy.GetSource(chartDetails.ChartSourceId, this.TenantId);

            if (chartDataSource == null || string.IsNullOrEmpty(chartDataSource.SourceContentId))
            {
                ModelState.AddModelError("statusMessage", ChartDetailsResource.e_InvalidDataSource);
                return;
            }

            ViewData["dataSource"] = chartDataSource;

            if (chartDataSource.DataSourceContent != null && !string.IsNullOrEmpty(chartDataSource.DataSourceContent.Content))
            {
                SourceContentViewModel scvm = (SourceContentViewModel)SerializationHelper.DeSerializeObject<SourceContentViewModel>(chartDataSource.DataSourceContent.Content);
                ViewData["ChartSourceContent"] = scvm.SourceContent;
                ViewData["SourceParameters"] = scvm.parameters;
            }

            ViewData["SourceContentTypes"] = new SelectList(GetSourceConentTypes(), "Key", "Value", chartDataSource.SourceTypeId);

            ViewData["preExistingSrc"] = new SelectList(GetPreExistingSources(chartDataSource.SourceTypeId, this.TenantId), "Key", "Value", "-1");
        }

        private void PopulateViewData(ChartDetails chartDetails)
        {
            ViewData["chartDetail"] = chartDetails;

            FillBasicChartProperties(chartDetails);

            ViewData["formMode"] = CurrentFormMode.Edit.ToString();

            if (!string.IsNullOrEmpty(chartDetails.ChartType))
            {
                ViewData["SeriesChartTypes"] = new SelectList(GetAllSeriesChartTypes(), "Key", "Value", chartDetails.ChartType);
            }

            var dataSource = (DataSource)DataSourceProxy.GetSource(chartDetails.ChartSourceId, this.TenantId);

            if (dataSource == null)
            {
                dataSource = new DataSource
                {
                    SourceContentId = chartDetails.ChartSourceId
                };
            }

            if (!string.IsNullOrEmpty(chartDetails.ChartSourceId))
            {
                ViewData["ChartDataSources"] = new SelectList(ListDataSources(this.TenantId), "Key", "Value", dataSource.SourceContentId);
            }

            if (chartDetails.ChartsAxisFields != null && chartDetails.ChartsAxisFields.ContainsKey(ChartAxesConstants.XAxis))
            {
                ViewData["XAxisColumns"] = new SelectList(GetDataSourceColumns(chartDetails.ChartSourceId), "Key", "Value", chartDetails.ChartsAxisFields[ChartAxesConstants.XAxis].FieldName);
            }

            if (chartDetails.ChartsAxisFields != null && chartDetails.ChartsAxisFields.ContainsKey(ChartAxesConstants.YAxis))
            {
                ViewData["YAxisColumns"] = new SelectList(GetDataSourceColumns(chartDetails.ChartSourceId), "Key", "Value", chartDetails.ChartsAxisFields[ChartAxesConstants.YAxis].FieldName);
            }

            ViewData["chartDetails"] = chartDetails;

            FillChartSpecificProperties(chartDetails);

            PopulateChartDataSource(chartDetails);
        }

        private void FillChartSpecificProperties(ChartDetails chartDetails)
        {
            switch (chartDetails.ChartType)
            {
                case "Bar":
                case "Column":
                    if (chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.BarWidth)
                        && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.BarColumnDrawingStyle)
                        && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.Color))
                    {
                        BarColumnProperties(chartDetails.ChartsProperties[ChartPropertyConstants.BarWidth].PropertyValue,
                            chartDetails.ChartsProperties[ChartPropertyConstants.BarColumnDrawingStyle].PropertyValue,
                            chartDetails.ChartsProperties[ChartPropertyConstants.Color].PropertyValue);
                    }
                    break;
                case "Line":
                    if (chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.PointLabels)
                        && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.Color))
                    {
                        LineProperties(chartDetails.ChartsProperties[ChartPropertyConstants.PointLabels].PropertyValue,
                            chartDetails.ChartsProperties[ChartPropertyConstants.Color].PropertyValue);
                    }
                    break;
                case "Pie":
                    if (chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.PieDoughnutLegendOptions)
                        && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.PieDrawingStyle))
                    {
                        PieProperties(chartDetails.ChartsProperties[ChartPropertyConstants.PieDoughnutLegendOptions].PropertyValue,
                            chartDetails.ChartsProperties[ChartPropertyConstants.PieDrawingStyle].PropertyValue);
                    }
                    break;
                case "Point":

                    if (chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.PointLabels)
                        && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.MarkerShape)
                        && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.MarkerSize)
                        && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.Color))
                    {
                        PointProperties(chartDetails.ChartsProperties[ChartPropertyConstants.PointLabels].PropertyValue,
                            chartDetails.ChartsProperties[ChartPropertyConstants.MarkerShape].PropertyValue,
                            chartDetails.ChartsProperties[ChartPropertyConstants.MarkerSize].PropertyValue,
                            chartDetails.ChartsProperties[ChartPropertyConstants.Color].PropertyValue);
                    }
                    break;
                default:
                    break;
            }
        }

        private void FillBasicChartProperties(ChartDetails chartDetails)
        {
            if (chartDetails.ChartsProperties != null && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.ChartType))
            {
                ViewData[ChartPropertyConstants.ChartType] = new SelectList(GetAllChartTypes(), "Key", "Value", chartDetails.ChartsProperties[ChartPropertyConstants.ChartType].PropertyValue);
            }

            if (chartDetails.ChartsProperties != null && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.Color))
            {
                ViewData[ChartPropertyConstants.Color] = chartDetails.ChartsProperties[ChartPropertyConstants.Color].PropertyValue;
            }

            if (chartDetails.ChartsProperties != null && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.FontSize))
            {
                ViewData[ChartPropertyConstants.FontSize] = chartDetails.ChartsProperties[ChartPropertyConstants.FontSize].PropertyValue;
            }

            if (chartDetails.ChartsProperties != null && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.Height))
            {
                ViewData[ChartPropertyConstants.Height] = chartDetails.ChartsProperties[ChartPropertyConstants.Height].PropertyValue;
            }

            if (chartDetails.ChartsProperties != null && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.Width))
            {
                ViewData[ChartPropertyConstants.Width] = chartDetails.ChartsProperties[ChartPropertyConstants.Width].PropertyValue;
            }

            if (chartDetails.ChartsProperties != null && chartDetails.ChartsProperties.ContainsKey(ChartPropertyConstants.SeriesName))
            {
                ViewData[ChartPropertyConstants.SeriesName] = chartDetails.ChartsProperties[ChartPropertyConstants.SeriesName].PropertyValue;
            }
        }

        private static void GetChartAxisFieldValuesFromInput(FormCollection fc, ChartDetails cd)
        {
            var xAxisField = new ChartAxisFields
                    {
                        AxisId = ChartAxesConstants.XAxis,
                        AxisName = ChartAxesConstants.XAxis,
                        FieldName = fc["XAxisColumns"],
                        Status = true,
                        Title = fc["XAxisTitle"],
                        ChartId = null
                    };
            if (!string.IsNullOrEmpty(fc["XChartAxisFields_Id"]))
            {
                xAxisField.AxisId = fc["XChartAxisFields_Id"];
            }

            if (!string.IsNullOrEmpty(cd.ChartId))
            {
                xAxisField.UpdatedBy = UserIdentity.UserId;
                xAxisField.UpdatedOn = DateTime.Now;
                xAxisField.ChartId = cd.ChartId;
            }
            else
            {
                xAxisField.AddedBy = UserIdentity.UserId;
                xAxisField.AddedOn = DateTime.Now;
            }

            cd.ChartsAxisFields.Add(ChartAxesConstants.XAxis, xAxisField);

            var yAxisField = new ChartAxisFields
                    {
                        AxisId = ChartAxesConstants.YAxis,
                        AxisName = ChartAxesConstants.YAxis,
                        FieldName = fc["YAxisColumns"],
                        Status = true,
                        Title = fc["YAxisTitle"],
                        ChartId = null
                    };

            if (!string.IsNullOrEmpty(fc["YChartAxisFields_Id"]))
            {
                yAxisField.AxisId = fc["YChartAxisFields_Id"];
            }

            if (!string.IsNullOrEmpty(cd.ChartId))
            {
                yAxisField.UpdatedBy = UserIdentity.UserId;
                yAxisField.UpdatedOn = DateTime.Now;
                yAxisField.ChartId = cd.ChartId;
            }
            else
            {
                yAxisField.AddedBy = UserIdentity.UserId;
                yAxisField.AddedOn = DateTime.Now;
            }

            cd.ChartsAxisFields.Add(ChartAxesConstants.YAxis, yAxisField);
        }

        private static void GetChartPropertiesFromView(FormCollection fc, ChartDetails cd, string tenantId)
        {
            var chartSection = new ChartSections
            {
                SectionId = ChartSectionConstants.Chart,
                SectionName = "Chart",
                Status = true
            };

            FillChartSpecificProperties(chartSection, fc, cd, tenantId);

            var fontSizeProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.FontSize,
                    PropertyValue = fc[ChartPropertyConstants.FontSize] + "~Pixel",
                    SectionPropertyId = ChartSectionPropertiesConstants.FontSize,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty { PropertyName = "FontSize", PropertyId = ChartPropertyConstants.FontSize },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.FontSize, fontSizeProperty);

            var seriesNameProperty = new ChartPropertyValues
            {
                PropertyId = ChartPropertyConstants.SeriesName,
                PropertyValue = fc[ChartPropertyConstants.SeriesName],
                SectionPropertyId = ChartSectionPropertiesConstants.SeriesName,
                Sections = chartSection,
                Status = true,
                TenantId = tenantId,
                Property =
                    new ChartProperty { PropertyName = "SeriesName", PropertyId = ChartPropertyConstants.FontSize },
                UpdatedBy = UserIdentity.UserId,
                UpdatedOn = DateTime.Now,
                ChartId = cd.ChartId,
                AddedBy = UserIdentity.UserId,
                AddedOn = DateTime.Now
            };

            cd.ChartsProperties.Add(ChartPropertyConstants.SeriesName, seriesNameProperty);

            var widthProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.Width,
                    PropertyValue = fc[ChartPropertyConstants.Width] + "~Pixel",
                    SectionPropertyId = ChartSectionPropertiesConstants.Width,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property = new ChartProperty { PropertyName = "Width", PropertyId = ChartPropertyConstants.Width },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.Width, widthProperty);

            var heightProperty = new ChartPropertyValues
                    {
                        PropertyId = ChartPropertyConstants.Height,
                        SectionPropertyId = ChartSectionPropertiesConstants.Height,
                        Sections = chartSection,
                        Status = true,
                        TenantId = tenantId,
                        Property = new ChartProperty
                        {
                            PropertyName = "Height",
                            PropertyId = ChartPropertyConstants.Height
                        }
                    };

            if (!string.IsNullOrEmpty(fc[ChartPropertyConstants.FontSize]) && !string.IsNullOrEmpty(fc[ChartPropertyConstants.Height]))
            {
                double chartHeight = double.Parse(fc[ChartPropertyConstants.Height], CultureInfo.InvariantCulture) + double.Parse(fc[ChartPropertyConstants.FontSize], CultureInfo.InvariantCulture);
                heightProperty.PropertyValue = chartHeight + "~" + fc["HeightUnitSpecs"];
            }
            else
            {
                heightProperty.PropertyValue = fc[ChartPropertyConstants.Height] + "~Pixel";
            }
            heightProperty.UpdatedBy = UserIdentity.UserId;
            heightProperty.UpdatedOn = DateTime.Now;
            heightProperty.ChartId = cd.ChartId;
            heightProperty.AddedBy = UserIdentity.UserId;
            heightProperty.AddedOn = DateTime.Now;

            cd.ChartsProperties.Add(ChartPropertyConstants.Height, heightProperty);

            var chartTypeProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.ChartType,
                    PropertyValue = fc[ChartPropertyConstants.ChartType],
                    SectionPropertyId = ChartSectionPropertiesConstants.ChartType,
                    SectionId = ChartSectionConstants.Chart,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty { PropertyName = "Height", PropertyId = ChartPropertyConstants.ChartType },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };
            cd.ChartsProperties.Add(ChartPropertyConstants.ChartType, chartTypeProperty);
        }

        private static void FillChartSpecificProperties(ChartSections chartSection, FormCollection fc, ChartDetails cd, string tenantId)
        {
            switch (cd.ChartType)
            {
                case "Bar":
                case "Column":
                    FillBarColumnProperties(chartSection, fc, cd, tenantId);
                    break;
                case "Line":
                    FillLineChartProperties(chartSection, fc, cd, tenantId);
                    break;
                case "Pie":
                    FillPieChartProperties(chartSection, fc, cd, tenantId);
                    break;
                case "Point":
                    FillPointChartProperties(chartSection, fc, cd, tenantId);
                    break;
                default:
                    return;
            }
        }

        private static void FillPointChartProperties(ChartSections chartSection, FormCollection fc, ChartDetails cd, string tenantId)
        {
            var pointLabelProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.PointLabels,
                    PropertyValue = fc[ChartPropertyConstants.PointLabels],
                    SectionPropertyId = ChartSectionPropertiesConstants.PointLabels,
                    SectionId = ChartSectionConstants.Chart,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty { PropertyName = "PointLabels", PropertyId = ChartPropertyConstants.PointLabels },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.PointLabels, pointLabelProperty);

            var markerShapeProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.MarkerShape,
                    PropertyValue = fc[ChartPropertyConstants.MarkerShape],
                    SectionPropertyId = ChartSectionPropertiesConstants.MarkerShape,
                    SectionId = ChartSectionConstants.Chart,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty { PropertyName = "MarkerShape", PropertyId = ChartPropertyConstants.MarkerShape },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.MarkerShape, markerShapeProperty);

            var markerSizeProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.MarkerSize,
                    PropertyValue = fc[ChartPropertyConstants.MarkerSize],
                    SectionPropertyId = ChartSectionPropertiesConstants.MarkerSize,
                    SectionId = ChartSectionConstants.Chart,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty { PropertyName = "MarkerSize", PropertyId = ChartPropertyConstants.MarkerSize },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.MarkerSize, markerSizeProperty);

            var colorProperty = new ChartPropertyValues
            {
                PropertyId = ChartPropertyConstants.Color,
                PropertyValue = fc[ChartPropertyConstants.Color],
                SectionPropertyId = ChartSectionPropertiesConstants.Color,
                Sections = chartSection,
                Status = true,
                TenantId = tenantId,
                Property = new ChartProperty { PropertyName = "Color", PropertyId = ChartPropertyConstants.Color },
                UpdatedBy = UserIdentity.UserId,
                UpdatedOn = DateTime.Now,
                ChartId = cd.ChartId,
                AddedBy = UserIdentity.UserId,
                AddedOn = DateTime.Now
            };

            cd.ChartsProperties.Add(ChartPropertyConstants.Color, colorProperty);
        }

        private static void FillLineChartProperties(ChartSections chartSection, FormCollection fc, ChartDetails cd, string tenantId)
        {
            var pointLabelProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.PointLabels,
                    PropertyValue = fc[ChartPropertyConstants.PointLabels],
                    SectionPropertyId = ChartSectionPropertiesConstants.PointLabels,
                    SectionId = ChartSectionConstants.Chart,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty { PropertyName = "PointLabels", PropertyId = ChartPropertyConstants.PointLabels },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.PointLabels, pointLabelProperty);

            var colorProperty = new ChartPropertyValues
            {
                PropertyId = ChartPropertyConstants.Color,
                PropertyValue = fc[ChartPropertyConstants.Color],
                SectionPropertyId = ChartSectionPropertiesConstants.Color,
                Sections = chartSection,
                Status = true,
                TenantId = tenantId,
                Property = new ChartProperty { PropertyName = "Color", PropertyId = ChartPropertyConstants.Color },
                UpdatedBy = UserIdentity.UserId,
                UpdatedOn = DateTime.Now,
                ChartId = cd.ChartId,
                AddedBy = UserIdentity.UserId,
                AddedOn = DateTime.Now
            };

            cd.ChartsProperties.Add(ChartPropertyConstants.Color, colorProperty);
        }

        private static void FillPieChartProperties(ChartSections chartSection, FormCollection fc, ChartDetails cd, string tenantId)
        {
            var legendProperty = new ChartPropertyValues
            {
                PropertyId = ChartPropertyConstants.PieDoughnutLegendOptions,
                PropertyValue = fc[ChartPropertyConstants.PieDoughnutLegendOptions],
                SectionPropertyId = ChartSectionPropertiesConstants.PieDoughnutLegendOptions,
                SectionId = ChartSectionConstants.Chart,
                Sections = chartSection,
                Status = true,
                TenantId = tenantId,
                Property = new ChartProperty
                {
                    PropertyName = "Pie_DoughNut_LegendOptions",
                    PropertyId = ChartPropertyConstants.PieDoughnutLegendOptions
                }
            };
            legendProperty.UpdatedBy = UserIdentity.UserId;
            legendProperty.UpdatedOn = DateTime.Now;
            legendProperty.ChartId = cd.ChartId;
            legendProperty.AddedBy = UserIdentity.UserId;
            legendProperty.AddedOn = DateTime.Now;

            cd.ChartsProperties.Add(ChartPropertyConstants.PieDoughnutLegendOptions, legendProperty);

            var drawingStyleProperty = new ChartPropertyValues
            {
                PropertyId = ChartPropertyConstants.PieDrawingStyle,
                PropertyValue = fc[ChartPropertyConstants.PieDrawingStyle],
                SectionPropertyId = ChartSectionPropertiesConstants.PieDrawingStyle,
                SectionId = ChartSectionConstants.Chart,
                Sections = chartSection,
                Status = true,
                TenantId = tenantId,
                Property = new ChartProperty
                {
                    PropertyName = "PieDrawingStyle",
                    PropertyId = ChartPropertyConstants.PieDrawingStyle
                }
            };
            drawingStyleProperty.UpdatedBy = UserIdentity.UserId;
            drawingStyleProperty.UpdatedOn = DateTime.Now;
            drawingStyleProperty.ChartId = cd.ChartId;
            drawingStyleProperty.AddedBy = UserIdentity.UserId;
            drawingStyleProperty.AddedOn = DateTime.Now;

            cd.ChartsProperties.Add(ChartPropertyConstants.PieDrawingStyle, drawingStyleProperty);
        }

        private static void FillBarColumnProperties(ChartSections chartSection, FormCollection fc, ChartDetails cd, string tenantId)
        {
            var widthProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.BarWidth,
                    PropertyValue = fc[ChartPropertyConstants.BarWidth],
                    SectionPropertyId = ChartSectionPropertiesConstants.BarWidthList,
                    SectionId = ChartSectionConstants.Chart,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty { PropertyName = "BarWidth", PropertyId = ChartPropertyConstants.BarWidth },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.BarWidth, widthProperty);

            var drawingStyleProperty = new ChartPropertyValues
                {
                    PropertyId = ChartPropertyConstants.BarColumnDrawingStyle,
                    PropertyValue = fc[ChartPropertyConstants.BarColumnDrawingStyle],
                    SectionPropertyId = ChartSectionPropertiesConstants.BarColumnDrawingStyle,
                    SectionId = ChartSectionConstants.Chart,
                    Sections = chartSection,
                    Status = true,
                    TenantId = tenantId,
                    Property =
                        new ChartProperty
                            {
                                PropertyName = "Bar_Column_DrawingStyle",
                                PropertyId = ChartPropertyConstants.BarColumnDrawingStyle
                            },
                    UpdatedBy = UserIdentity.UserId,
                    UpdatedOn = DateTime.Now,
                    ChartId = cd.ChartId,
                    AddedBy = UserIdentity.UserId,
                    AddedOn = DateTime.Now
                };

            cd.ChartsProperties.Add(ChartPropertyConstants.BarColumnDrawingStyle, drawingStyleProperty);

            var colorProperty = new ChartPropertyValues
            {
                PropertyId = ChartPropertyConstants.Color,
                PropertyValue = fc[ChartPropertyConstants.Color],
                SectionPropertyId = ChartSectionPropertiesConstants.Color,
                Sections = chartSection,
                Status = true,
                TenantId = tenantId,
                Property = new ChartProperty { PropertyName = "Color", PropertyId = ChartPropertyConstants.Color },
                UpdatedBy = UserIdentity.UserId,
                UpdatedOn = DateTime.Now,
                ChartId = cd.ChartId,
                AddedBy = UserIdentity.UserId,
                AddedOn = DateTime.Now
            };

            cd.ChartsProperties.Add(ChartPropertyConstants.Color, colorProperty);
        }

        #region Static Chart Properties
        private static Dictionary<string, string> GetAllChartTypes()
        {
            return new Dictionary<string, string>
            {
                {"-1","-- Choose A Chart Type--"},
                {"2D","2D"},
                {"3D","3D"},
            };
        }

        private void PointProperties(string pointLabel = null, string markerShape = null, string markerSize = null, string colorCode = null)
        {
            var pointLabelsList = new Dictionary<string, string>
            {
                {"TopLeft" ,"TopLeft"},
                {"Top" ,"Top"},
                {"TopRight","TopRight"},
                {"Right"   ,"Right"},
                {"BottomRight" ,"BottomRight"},
                {"BottomLeft","BottomLeft"},
                {"Bottom","Bottom"},
                {"Left"  ,"Left"},
                {"Center","Center"}
            };

            if (string.IsNullOrEmpty(pointLabel))
            {
                pointLabel = "None";
            }

            ViewData[ChartPropertyConstants.PointLabels] = new SelectList(pointLabelsList, "Key", "Value", pointLabel);

            var markerShapeList = new Dictionary<string, string>
            {
                {"Circle","Circle"},
                {"Diamond","Diamond"},
                {"Cross","Cross"},
                   {"Square","Square"},
                {"Triangle","Triangle"}
            };

            if (string.IsNullOrEmpty(markerShape))
            {
                markerShape = "Circle";
            }

            ViewData[ChartPropertyConstants.MarkerShape] = new SelectList(markerShapeList, "Key", "Value", markerShape);

            var markerSizeList = new Dictionary<string, string>
            {
                { "5"  ,"5"    },
                { "7"  ,"7"    },
                { "10" ,"10"   },
                { "12" ,"12"   },
                { "15" ,"15"   },
                { "18" ,"18"   }
            };

            if (string.IsNullOrEmpty(markerSize))
            {
                markerSize = "5";
            }

            ViewData[ChartPropertyConstants.MarkerSize] = new SelectList(markerSizeList, "Key", "Value", markerSize);

            if (string.IsNullOrEmpty(colorCode))
            {
                colorCode = "#abcdef";
            }

            ViewData[ChartPropertyConstants.Color] = colorCode;
        }

        private void PieProperties(string legend = null, string drawingstyle = null)
        {
            var pieDoughNutLegendOptions = new Dictionary<string, string>
            {
                {"Inside","Inside"},
                {"Outside","Outside"}
            };

            if (string.IsNullOrEmpty(legend))
            {
                legend = "Inside";
            }

            ViewData[ChartPropertyConstants.PieDoughnutLegendOptions] = new SelectList(pieDoughNutLegendOptions, "Key", "Value", legend);

            var pieDrawingStyle = new Dictionary<string, string>
            {
                {"SoftEdge" ,"SoftEdge" },
                {"Concave" ,"Concave"   }
            };

            if (string.IsNullOrEmpty(drawingstyle))
            {
                drawingstyle = "Default";
            }

            ViewData[ChartPropertyConstants.PieDrawingStyle] = new SelectList(pieDrawingStyle, "Key", "Value", drawingstyle);
        }

        private void LineProperties(string pointLabels = null, string colorCode = null)
        {
            var pointLabelsList = new Dictionary<string, string>
            {
                {   "Auto"          ,"Auto"             },
                {   "TopLeft"       ,"TopLeft"          },
                {   "Top"           ,"Top"              },
                {   "TopRight"      ,"TopRight"         },
                {   "Right"         ,"Right"            },
                {   "BottomRight"    ,"BottomRight"     },
                {   "Bottom"        ,"Bottom"           },
                {   "BottomLeft"    ,"BottomLeft"       },
                {   "Left"          ,"Left"             },
                {   "Center"        ,"Center"           }
            };

            if (string.IsNullOrEmpty(pointLabels))
            {
                pointLabels = "None";
            }

            ViewData[ChartPropertyConstants.PointLabels] = new SelectList(pointLabelsList, "Key", "Value", pointLabels);

            if (string.IsNullOrEmpty(colorCode))
            {
                colorCode = "#abcdef";
            }

            ViewData[ChartPropertyConstants.Color] = colorCode;
        }

        private void BarColumnProperties(string width = null, string drawingStyle = null, string colorCode = null)
        {
            var barWidthList = new Dictionary<string, string>
            {
                { "0.4"   ,"0.4"  },
                { "0.6"   ,"0.6"  },
                { "0.8"   ,"0.8"  },
                { "1.0"   ,"1.0"  }
            };

            if (string.IsNullOrEmpty(width))
            {
                width = "0.4";
            }

            ViewData[ChartPropertyConstants.BarWidth] = new SelectList(barWidthList, "Key", "Value", width);

            var barColumnDrawingStyle = new Dictionary<string, string>
            {
                {"Default"      ,"Default"      },
                {"Emboss"       ,"Emboss"       },
                {"Cylinder"     ,"Cylinder"     },
                {"Wedge"        ,"Wedge"        },
                {"LightToDark"  ,"LightToDark"  }

            };

            if (string.IsNullOrEmpty(drawingStyle))
            {
                drawingStyle = "Default";
            }

            ViewData[ChartPropertyConstants.BarColumnDrawingStyle] = new SelectList(barColumnDrawingStyle, "Key", "Value", drawingStyle);

            if (string.IsNullOrEmpty(colorCode))
            {
                colorCode = "#abcdef";
            }

            ViewData[ChartPropertyConstants.Color] = colorCode;
        }

        /// <summary>
        /// Gets all the Built-In series chart types.
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> GetAllSeriesChartTypes()
        {
            return new Dictionary<string, string>
            {
                {"-1","--Choose A Series--"},
                {"Point","Point"},
                {"Line","Line"},
                {"Bar","Bar"},
                {"Column","Column"},
                {"Pie","Pie"}
            };
        }

        private static Dictionary<string, string> GetSourceConentTypes()
        {
            return new Dictionary<string, string>
            {
	            {"-1","--Choose A SourceContent--"},
                {"295CE098-8B9B-454C-B65C-1FEC6F7B69EA","Text"},
                {"56B8A747-C55D-42A8-A801-2C66E9EC2AAC","Stored Procedure"},
                {"35782E2D-8FE9-4444-8C8D-D5F77855BE29","Built-In Query Source"}
            };
        }

        private static Dictionary<string, string> GetPreExistingSources(string sourceTypeId, string tenantId)
        {
            if (string.IsNullOrEmpty(sourceTypeId))
            {
                return new Dictionary<string, string>
                {
                    {"-1","--Choose A SourceContent--"},
                    {"new","--New SourceContent--"}
                };
            }

            switch (sourceTypeId)
            {
                case DataSourceConstants.TextQuerySource:
                case DataSourceConstants.StoredProcedureSource:
                    var textQuerySources = DataSourceProxy.GetSourcesByType(sourceTypeId, tenantId);

                    var textQuerySourcessList = (from t in textQuerySources
                                                 select new { Key = t.Key, Value = t.Value.Name }).ToDictionary(t => t.Key, t => t.Value);

                    return textQuerySourcessList;

                case DataSourceConstants.BuiltInQuerySource:
                    return QueryDetailsProxy.GetShallowQueryDetails(tenantId);
                default:
                    return null;
            }
        }

        #endregion

        private void PrepareForPreview(string chartId, ChartDetails chartDetail, Dictionary<string, string> dynamicVariables)
        {
            DataTable resultDataTable;

            Chart chart = CelloSaaS.ChartBuilder.ChartUtility.GetChartFromChartDetails(chartDetail, out resultDataTable, chartId, this.TenantId, dynamicVariables);

            ViewData["ChartObject"] = chart;

            ViewData["ChartData"] = resultDataTable;
        }

        private static Dictionary<string, string> ListDataSources(string tenantId)
        {
            var dataSourceList = QueryDetailsProxy.GetShallowQueryDetails(tenantId);

            var dataSources = new Dictionary<string, string>();
            dataSources.Add("-1", "-- Choose A Source --");

            foreach (var datasource in dataSourceList)
            {
                dataSources.Add(datasource.Key, datasource.Value);
            }

            return dataSources;
        }

        #endregion
    }

    /// <summary>
    /// This enum is used for getting the chart export type.
    /// </summary>
    public enum ChartExportType
    {
        PDF,
        PNG
    }
}
