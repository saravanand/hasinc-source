﻿using CelloSaaS.Library;
using CelloSaaS.ServiceProxies.TenantManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CelloSaaSWebClient.Controllers
{
    using CelloSaaS.AuthServer.Core.Ancilliary;
    using CelloSaaS.AuthServer.Core.Models;
    using CelloSaaS.AuthServer.Client.Extensions;
    using CelloSaaS.Model.TenantManagement;
    using CelloSaaSWebClient.Services;
    using System.Dynamic;
    using Newtonsoft.Json;
    using CelloSaaSWebClient.Models;
    using CelloSaaS.View;
    /// <summary>
    /// This class responsible for client management.
    /// </summary>
    public class ClientDetailsController : Controller
    {
        // GET: /Clients/
        [Route("Clients", Name = "ManageClients")]
        public async Task<ActionResult> Index()
        {
            FillStaticOptions(null);
            List<TenantDetails> childTenants = GetAllChildTenants();
            ViewData["ClientDetail.TenantId"] = new SelectList(childTenants, "TenantCode", "TenantCodeString", UserIdentity.TenantID);
            return View();
        }

        /// <summary>
        /// This method is used to get the list of clients for the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifiers.</param>
        /// <returns></returns>
        [Route("ClientList", Name = "ClientsList")]
        public async Task<PartialViewResult> ListAsync(string tenantId)
        {
            if (string.IsNullOrEmpty(tenantId))
            {
                ModelState.AddModelError("", "Tenant Id cannot be null or empty");
                return PartialView("ClientsGrid", null);
            }

            var clients = await ClientService.SearchClientsAsync(Guid.Parse(tenantId), null);

            if (clients == null || clients.Count < 1)
            {
                ModelState.AddModelError("", ClientService.ExceptionMessage);
                ClientService.ExceptionMessage = null;
                return PartialView("ClientsGrid", null);
            }

            List<ClientDetailDTO> clientDetails = new List<ClientDetailDTO>();

            clients.ForEach(client =>
            {
                InflateClientDetails(client);
                FillStaticOptions(client);
                clientDetails.Add(GetClientDTO(client));
            });

            return PartialView("ClientsGrid", clientDetails);
        }

        /// <summary>
        /// This method is used to get the client for the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PartialViewResult> ManageClient(string tenantId)
        {
            if (string.IsNullOrEmpty(tenantId))
            {
                ModelState.AddModelError("", "Tenant Id cannot be empty");
                return PartialView("ManageClient", null);
            }

            var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(tenantId, UserIdentity.TenantID);

            if (tenantDetails == null || tenantDetails.TenantDetails == null)
            {
                ModelState.AddModelError("", "Unable to fetch the tenant details");
                return PartialView("ManageClient", null);
            }

            string redirectUri = tenantDetails.TenantDetails.URL;

            if (string.IsNullOrEmpty(tenantDetails.TenantDetails.URL))
                redirectUri = this.Request.Url.GetBaseUrl();

            ClientDetailDTO client = new ClientDetailDTO
            {
                Name = "Client Details for " + tenantDetails.TenantDetails.TenantCodeString + " Application",
                TenantId = tenantDetails.TenantDetails.Identifier,
                Flows = "Code",
                RedirectUris = Newtonsoft.Json.JsonConvert.SerializeObject(new List<string> { new UriBuilder(redirectUri) { Path = "Account/AuthorizationCallBack" }.ToString() }),
            };

            InflateClientDetailsInDTO(client);
            FillStaticOptionsFromDTO(client);
            return PartialView("ManageClient", client);
        }

        /// <summary>
        ///  This method is used to get the client for the given tenant identifier and application type.
        /// </summary>
        /// <param name="Id">client identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="applicationType">application type.</param>
        /// <returns></returns>
        [Route("EditClient", Name = "Edit")]
        public async Task<PartialViewResult> ManageClients(string Id, string tenantId, string applicationType)
        {
            var clients = await ClientService.SearchClientsAsync(tenantId.ToGuid(), applicationType, null, null, Id);

            if (clients == null || clients.Count < 1)
            {
                ModelState.AddModelError("", ClientService.ExceptionMessage);
                ClientService.ExceptionMessage = null;
                return PartialView("ManageClient", null);
            }

            var client = clients.SingleOrDefault();
            InflateClientDetails(client);
            FillStaticOptions(client);
            var clientDetails = GetClientDTO(client);
            return PartialView("ManageClient", clientDetails);
        }

        /// <summary>
        /// This method is used to save the client details.
        /// </summary>
        /// <param name="collection">required client data.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("save", Name = "Save")]
        public async Task<PartialViewResult> Save(FormCollection collection)
        {
            ClientDetailDTO client = null;

            string id = collection["Id"];

            if (!string.IsNullOrEmpty(id) && id != Guid.Empty.ToString())
            {
                //client = await _clientService.GetAsync(collection["Id"]);
                ExpandoObject clientDetail = await ClientService.GetClientAsync(id);
                FillStaticOptions(clientDetail);
                DeflateClientDetails(clientDetail);
                client = GetClientDTO(clientDetail);
            }
            else
            {
                client = new ClientDetailDTO
                {
                    CreatedBy = UserIdentity.UserId.ToGuid(),
                    CreatedOn = DateTime.UtcNow,
                    Status = true
                };
                FillStaticOptionsFromDTO(client);
            }

            TryUpdateModel(client);

            ValidateClientDetails(collection);//, client);

            if (!string.IsNullOrEmpty(id) && id != Guid.Empty.ToString())
            {
                var redirectUri = client.RedirectUris;

                if (redirectUri.Contains(','))
                {
                    client.RedirectUris = JsonConvert.SerializeObject(redirectUri.Split(',').ToList());
                }
                else
                {
                    client.RedirectUris = JsonConvert.SerializeObject(new List<string> { redirectUri });
                }

                if (!string.IsNullOrEmpty(client.ScopeRestrictions) && client.ScopeRestrictions.Contains(','))
                    client.ScopeRestrictions = JsonConvert.SerializeObject(client.ScopeRestrictions.Split(',').ToList());
                else if (!string.IsNullOrEmpty(client.ScopeRestrictions))
                    client.ScopeRestrictions = JsonConvert.SerializeObject(new List<string> { client.ScopeRestrictions });

                client.UpdatedBy = UserIdentity.UserId.ToGuid();
                client.UpdatedOn = DateTime.UtcNow;
                client.Status = true;
            }

            if (!ModelState.IsValid)
            {
                return PartialView("ManageClient", client);
            }

            try
            {
                if (string.IsNullOrEmpty(id) || id == Guid.Empty.ToString())
                {
                    client.Id = Guid.NewGuid().ToString();
                    string clientId = await ClientService.AddClientAsync(client);

                    if (string.IsNullOrEmpty(clientId))
                    {
                        ModelState.AddModelError("Error", ClientService.ExceptionMessage);
                        ClientService.ExceptionMessage = null;
                    }
                    else
                    {
                        ModelState.AddModelError("Success", "Client Details Saved Successfully");
                    }
                }
                else
                {
                    int recordsAffected = await ClientService.UpdateClientAsync(client);
                    if (recordsAffected < 1)
                    {
                        ModelState.AddModelError("Error", ClientService.ExceptionMessage);
                        ClientService.ExceptionMessage = null;
                    }
                    else
                    {
                        ModelState.AddModelError("Success", "Client Details Saved Successfully");
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }
            return PartialView("ManageClient", client);
        }

        #region Private Members

        private static bool IsJson(string input)
        {
            input = input.Trim();
            return input.StartsWith("{") && input.EndsWith("}")
                   || input.StartsWith("[") && input.EndsWith("]");
        }

        private void InflateClientDetails(ExpandoObject client)
        {
            var redirectUris = client.TryGetStringValue("RedirectUris");

            if (!string.IsNullOrEmpty(redirectUris))
            {
                client.AddStringProperty("RedirectUris", IsJson(redirectUris) ? string.Join(",", Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(redirectUris)) : redirectUris);
            }

            var scopeRestrictions = client.TryGetStringValue("ScopeRestrictions");

            if (!string.IsNullOrEmpty(scopeRestrictions))
            {
                client.AddStringProperty("ScopeRestrictions", IsJson(redirectUris) ? string.Join(",", JsonConvert.DeserializeObject<List<string>>(scopeRestrictions)) : scopeRestrictions);
            }
        }

        private void InflateClientDetailsInDTO(ClientDetailDTO client)
        {
            if (!string.IsNullOrEmpty(client.RedirectUris))
            {
                client.RedirectUris = IsJson(client.RedirectUris) ? string.Join(",", Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(client.RedirectUris)) : client.RedirectUris;
            }

            if (!string.IsNullOrEmpty(client.ScopeRestrictions))
            {
                client.ScopeRestrictions = IsJson(client.ScopeRestrictions) ? string.Join(",", JsonConvert.DeserializeObject<List<string>>(client.ScopeRestrictions)) : client.ScopeRestrictions;
            }
        }

        private void DeflateClientDetails(ExpandoObject client)
        {
            var redirectUris = client.TryGetStringValue("RedirectUris");

            if (!string.IsNullOrEmpty(redirectUris))
            {
                string uris = JsonConvert.SerializeObject(redirectUris.Contains(',')
                                                            ? redirectUris.Split(',').ToList()
                                                            : new List<string> { redirectUris });
                client.AddStringProperty("RedirectUris", uris);
            }
            var scopeRestrictions = client.TryGetStringValue("ScopeRestrictions");

            if (!string.IsNullOrEmpty(scopeRestrictions))
            {
                string scopes = JsonConvert.SerializeObject(scopeRestrictions.Contains(',')
                                                                ? scopeRestrictions.Split(',').ToList()
                                                                : new List<string> { scopeRestrictions });
                client.AddStringProperty("ScopeRestrictions", scopes);
            }
        }

        private void ValidateClientDetails(FormCollection collection)
        {
            if (string.IsNullOrEmpty(collection["Name"]))
            {
                ModelState.AddModelError("Error", "Client Name cannot be empty");
                ModelState.AddModelError("ClientDetail.Name", "*");
            }

            if (string.IsNullOrEmpty(collection["Secret"]))
            {
                ModelState.AddModelError("Error", "Client Secret cannot be empty");
                ModelState.AddModelError("ClientDetail.Secret", "*");
            }

            if (!string.IsNullOrEmpty(collection["Uri"]) && !Util.ValidateURL(collection["Uri"]))
            {
                ModelState.AddModelError("Error", "Client Uri cannot be empty");
                ModelState.AddModelError("ClientDetail.Uri", "*");
            }

            if (!string.IsNullOrEmpty(collection["LogoUri"]) && !Util.ValidateURL(collection["LogoUri"]))
            {
                ModelState.AddModelError("Error", "Client LogoUri cannot be empty");
                ModelState.AddModelError("ClientDetail.LogoUri", "*");
            }

            if (string.IsNullOrEmpty(collection["RedirectUris"]) || !Util.ValidateURL(collection["RedirectUris"]))
            {
                ModelState.AddModelError("Error", "Client RedirectUris cannot be empty");
                ModelState.AddModelError("ClientDetail.RedirectUris", "*");
            }

            if (string.IsNullOrEmpty(collection["AccessTokenType"]))
            {
                ModelState.AddModelError("Error", "Client AccessTokenType cannot be empty");
                ModelState.AddModelError("ClientDetail.AccessTokenType", "*");
            }

            if (string.IsNullOrEmpty(collection["ApplicationTypes"]))
            {
                ModelState.AddModelError("Error", "Client ApplicationTypes cannot be empty");
                ModelState.AddModelError("ClientDetail.ApplicationTypes", "*");
            }

            if (string.IsNullOrEmpty(collection["ClientType"]))
            {
                ModelState.AddModelError("Error", "Client Type cannot be empty");
                ModelState.AddModelError("ClientDetail.ClientType", "*");
            }

            if (string.IsNullOrEmpty(collection["SigningKeyType"]))
            {
                ModelState.AddModelError("Error", "Client SigningKeyType cannot be empty");
                ModelState.AddModelError("ClientDetail.SigningKeyType", "*");
            }

        }

        private static List<TenantDetails> GetAllChildTenants()
        {
            Dictionary<string, TenantDetails> allChilds = TenantRelationProxy.GetAllChildTenants(UserIdentity.TenantID);

            var emptyMarker = new TenantDetails { TenantCode = "", TenantCodeString = "Choose A Tenant" };

            List<TenantDetails> childTenants = (allChilds != null && allChilds.Count > 0)
                                                ? allChilds.Values.ToList()
                                                : new List<TenantDetails>();

            var currentTenant = TenantProxy.GetTenantDetailsByTenantId(UserIdentity.TenantID, UserIdentity.TenantID);
            if (currentTenant != null && currentTenant.TenantDetails != null)
            {
                childTenants.Add(currentTenant.TenantDetails);
            }
            childTenants.Insert(0, emptyMarker);
            return childTenants;
        }

        private void FillStaticOptions(ExpandoObject client)
        {
            if (client == null || client.Count() < 1) return;

            string appType = client.TryGetStringValue("ApplicationTypes");
            string signingKeyType = client.TryGetStringValue("SigningKeyType");
            string accessTokenType = client.TryGetStringValue("AccessTokenType");
            string clientType = client.TryGetStringValue("ClientType");

            InitViewData(appType, signingKeyType, accessTokenType, clientType);
        }

        private void FillStaticOptionsFromDTO(ClientDetailDTO client)
        {
            if (client == null) return;

            string appType = client.ApplicationTypes;
            string signingKeyType = client.SigningKeyType;
            string accessTokenType = client.AccessTokenType;
            string clientType = client.ClientType;

            InitViewData(appType, signingKeyType, accessTokenType, clientType);
        }

        private void InitViewData(string appType, string signingKeyType, string accessTokenType, string clientType)
        {
            ViewData["ApplicationTypes"] = new SelectList(Enum.GetNames(typeof(ApplicationTypes)), appType);
            ViewData["ClientType"] = new SelectList(Enum.GetNames(typeof(ClientTypes)), clientType);
            ViewData["SigningKeyType"] = new SelectList(Enum.GetNames(typeof(SigningKeyTypes)), signingKeyType);
            ViewData["AccessTokenType"] = new SelectList(Enum.GetNames(typeof(AccessTokenType)), accessTokenType);
        }

        private static ClientDetailDTO GetClientDTO(ExpandoObject client)
        {
            var clientDetails = new ClientDetailDTO
            {
                Secret = client.TryGetStringValue("Secret"),
                Id = client.TryGetStringValue("Id"),
                Name = client.TryGetStringValue("Name"),
                Description = client.TryGetStringValue("Description"),
                Uri = client.TryGetStringValue("Uri"),
                ApplicationTypes = client.TryGetStringValue("ApplicationTypes"),
                AccessTokenType = client.TryGetStringValue("AccessTokenType"),
                CreatedOn = client.TryGetValue<DateTime>("CreatedOn"),
                ScopeRestrictions = client.TryGetStringValue("ScopeRestrictions"),
                TenantId = client.TryGetStringValue("TenantId"),
                RequireConsent = client.TryGetValue<bool>("RequireConsent"),
                LogoUri = client.TryGetStringValue("LogoUri"),
                AllowRememberConsent = client.TryGetValue<bool>("AllowRememberConsent"),
                Flows = client.TryGetStringValue("Flows"),
                RedirectUris = client.TryGetStringValue("RedirectUris"),
                IdentityTokenLifetime = client.TryGetLongValue("IdentityTokenLifetime"),
                AccessTokenLifetime = client.TryGetLongValue("AccessTokenLifetime"),
                RefreshTokenLifetime = client.TryGetLongValue("RefreshTokenLifetime"),
                AuthorizationCodeLifetime = client.TryGetLongValue("AuthorizationCodeLifetime"),
                PublicAccessAllowed = client.TryGetValue<bool>("PublicAccessAllowed"),
                ClientType = client.TryGetStringValue("ClientType")
            };
            return clientDetails;
        }
        #endregion
    }
}