namespace CelloSaaSApplication.Controllers
{
    using Resources;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using CelloSaaS.Library;
    using CelloSaaS.Model.Configuration;
    using CelloSaaS.ServiceProxies.Configuration;
    using System.Data;
    using System.Transactions;
    using CelloSaaS.Model.DataManagement;
    using CelloSaaS.ServiceProxies.DataManagement;
    using CelloSaaS.ServiceContracts.Configuration;
    using CelloSaaS.ServiceContracts.DataManagement;
    using CelloSaaS.Rules.Core;
    using CelloSaaS.Rules.Constants;
    using System.Globalization;

    /// <summary>
    /// This class is responsible for configurations.
    /// </summary>
    [HandleError]
    public class ConfigurationController : CelloSaaS.View.CelloController
    {
        //Logger policy name
        private const string DefaultPolicy = "GlobalExceptionLogger";

        //private string TenantId = UserIdentity.TenantID;
        private string PickupListTenantId = TenantContext.GetTenantId("_PickupList");
        private string ModuleConfigTenantId = TenantContext.GetTenantId("_ModuleConfig");
        //private string UserId = UserIdentity.UserId;

        #region Pickup List Public Methods

        /// <summary>
        /// This method is used to get the Pickup list.
        /// </summary>
        /// <returns>pickup list view.</returns>
        public ActionResult PickupList()
        {
            //Get the pickup list details
            GetPickupList();
            return View();
        }

        /// <summary>
        /// This method is used to get the Pickup list.
        /// </summary>
        /// <returns>pickup list view.</returns>
        public ActionResult PickupListDetails()
        {
            //Get pickup list details
            GetPickupList();
            return PartialView("PartialPickupList");
        }

        /// <summary>
        /// This method is used to render add Pickup list.
        /// </summary>
        /// <returns>partial view of add pickup list.</returns>
        public ActionResult AddPickupList()
        {
            PickupList pickupList = new PickupList();
            return PartialView("AddPickupList", pickupList);
        }

        /// <summary>
        /// This method is used to add Pickup list values.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns>
        /// Success message or Add Pickup list partial view.
        /// </returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object AddPickupList(FormCollection formCollection)
        {
            PickupList pickupList = new PickupList();

            try
            {
                UpdateModel(pickupList);

                // Validate pickup list details
                ValidatePickupListDetails(pickupList);

                if (ModelState.IsValid)
                {
                    pickupList.TenantCode = this.PickupListTenantId;

                    string result = PickupListProxy.AddPickupList(pickupList);

                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("Success", Resources.ConfigurationResource.s_PickupListAdd);

                        //Get pickup list details
                        GetPickupList();
                        return "Success";
                        //return PartialView("PartialPickupList");
                    }
                    else
                    {
                        this.ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListAdd);
                    }
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_PickupListAdd);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_DuplicateName);
            }

            return PartialView("AddPickupList", pickupList);
        }

        /// <summary>
        /// This method is used to get Pickup list for edit.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>edit pickup list partial view.</returns>
        public ActionResult EditPickupList(string pickupListId)
        {
            PickupList pickupList = new PickupList();

            try
            {
                //Get pickup list details of pickup list id
                pickupList = PickupListProxy.GetPickupListDetails(pickupListId);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_PickupListLoad);
            }

            return PartialView("EditPickupList", pickupList);
        }

        /// <summary>
        /// This method is used to update Pickup list.
        /// </summary>
        /// <returns>
        /// Success message or Edit Pickup list partial view.
        /// </returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object EditPickupList()
        {
            PickupList pickupList = new PickupList();

            try
            {
                UpdateModel(pickupList);

                // Validate pickup list details
                ValidatePickupListDetails(pickupList);

                if (ModelState.IsValid)
                {
                    //Update pickup list details
                    string result = PickupListProxy.UpdatePickupList(pickupList);

                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("Success", ConfigurationResource.s_PickupListUpdate);

                        // Get pickup list details
                        GetPickupList();
                        return "Success";
                        // return PartialView("PartialPickupList");
                    }

                    this.ModelState.AddModelError("Error", ConfigurationResource.e_PickupListUpdate);
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_PickupListUpdate);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_DuplicateName);
            }

            return PartialView("EditPickupList", pickupList);
        }

        /// <summary>
        /// This method is used to deactivate Pickup list detail.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>Pickup list details view.</returns>
        public ActionResult DeletePickupList(string pickupListId)
        {
            try
            {
                //Deactivate the pickup list
                PickupListProxy.DeactivatePickupList(pickupListId);
                ModelState.AddModelError("Success", Resources.ConfigurationResource.s_PickupListDelete);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListDelete);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_PickupListDependancy);
            }

            //Return pickup list details view
            return PickupListDetails();
        }

        /// <summary>
        /// This method is used to activate Pickup list detail.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>Pickup list details view.</returns>
        public ActionResult ActivatePickupList(string pickupListId)
        {
            try
            {
                //Activate the pickup list
                PickupListProxy.ActivatePickupList(pickupListId);
                ModelState.AddModelError("Success", ConfigurationResource.s_ActivatePickupList);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListDelete);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                ModelState.AddModelError("Error", ConfigurationResource.e_ActivatePickupList);
            }

            return PickupListDetails();
        }

        #endregion

        #region Pickup List Private Methods

        /// <summary>
        /// This method is used to get the Pickup list details.
        /// </summary>
        private void GetPickupList()
        {
            ViewData["CanCopyFromParent"] = false;

            try
            {
                var settingValue = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.PickupListTenantId, ModuleConfigurationConstant.EXTENDED_FIELD);

                ViewData["SettingSource"] = settingValue;

                if (!this.PickupListTenantId.Equals(CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase)
                    && !string.IsNullOrEmpty(settingValue)
                    && settingValue.Equals(ConfigurationSettingValueConstant.OWN_SETTING))
                {
                    ViewData["CanCopyFromParent"] = true;
                }

                // Fetch active/deactive pickup list details for tenant
                List<PickupList> lstPickupList = PickupListProxy.GetAllPickupLists(this.PickupListTenantId);
                ViewData["PickupList"] = lstPickupList;
            }
            //Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }

            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            //Catch pickup list exception and show exception message to user
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListLoad);
            }
        }

        /// <summary>
        /// This method is used to validate pickup list details.
        /// </summary>
        /// <param name="pickupList">pickup list values.</param>
        private void ValidatePickupListDetails(PickupList pickupList)
        {
            //Check pickup list name is null or empty
            if (string.IsNullOrWhiteSpace(pickupList.Name))
            {
                ModelState.AddModelError("Name", Resources.ConfigurationResource.e_InvalidName);
            }
        }

        #endregion

        #region Pickup List Values Public Methods

        /// <summary>
        /// This method is used to return view of Pickup list values.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>Pickup list values view.</returns>
        public ActionResult PickUpListValues(string pickupListId)
        {
            Guid pickuplistGuid;
            if (!String.IsNullOrEmpty(pickupListId) && Guid.TryParse(pickupListId, out pickuplistGuid))
            {
                var pickUpListDetails = CelloSaaS.ServiceProxies.Configuration.PickupListProxy.GetPickupListDetails(pickupListId);
                if (pickUpListDetails != null)
                {
                    ViewData["PickupistName"] = pickUpListDetails.Name;

                    //Get pickup list values for pickup list id
                    PickupListValueDetails(pickupListId);
                    return View();
                }
            }

            return HttpNotFound("Pickuplist not found!");
        }

        /// <summary>
        /// This method is used to return partial view of Pickup list values.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>Pickup list values partial view.</returns>
        public ActionResult PickupListValuesList(string pickupListId)
        {
            //Get pickup list values for pickup list id
            PickupListValueDetails(pickupListId);
            return PartialView("PartialPickupListValues");
        }

        /// <summary>
        /// This method is used to get the Pickup list values.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        private void PickupListValueDetails(string pickupListId)
        {
            try
            {
                var settingValue = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.PickupListTenantId, ModuleConfigurationConstant.EXTENDED_FIELD);

                ViewData["SettingSource"] = settingValue;

                List<PickupListValue> lstPickupListValues = null;

                //Get tenant pickup list values for pickup list id
                lstPickupListValues = PickupListProxy.GetAllPickupListValues(pickupListId, PickupListTenantId);

                //Tenant Pickup list values have details then check general pickup list details
                if (lstPickupListValues == null || lstPickupListValues.Count <= 0)
                {
                    //Get general pickup list values for pickup list id
                    List<PickupListValue> lstGeneralPickupListValues = PickupListProxy.GetPickupListValues(pickupListId, string.Empty);

                    //General Pickup list values have details then Pickup List for tenant set true
                    if (lstGeneralPickupListValues != null && lstGeneralPickupListValues.Count > 0)
                    {
                        ViewData["PickupListForTenant"] = true;
                    }
                }
                ViewData["PickupListId"] = pickupListId;
                ViewData["PickupListValue"] = lstPickupListValues;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueLoad);
            }
        }

        /// <summary>
        /// This method is used to copy all pickup list values from general to tenant specific.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>Pickup List Values View.</returns>
        public ActionResult CopyPickupListValues(string pickupListId)
        {
            try
            {
                List<PickupListValue> lstPickupListValues = null;

                //Get general pickup list values for pickup list id
                lstPickupListValues = PickupListProxy.GetPickupListValues(pickupListId, string.Empty);

                bool result = false;

                if (lstPickupListValues != null && lstPickupListValues.Count > 0)
                {
                    foreach (PickupListValue pickupListValue in lstPickupListValues)
                    {
                        pickupListValue.TenantCode = this.PickupListTenantId;
                        pickupListValue.Id = string.Empty;
                    }
                    result = PickupListProxy.AddPickupListValue(lstPickupListValues);
                }

                if (result)
                    this.ModelState.AddModelError("Success", Resources.ConfigurationResource.s_PickupListValuesCopy);
                else
                    this.ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValuesCopy);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValuesCopy);
            }

            //Get pickup list value details
            PickupListValueDetails(pickupListId);

            return PartialView("PartialPickupListValues");
        }

        /// <summary>
        /// This method is used to copy all pickup list values from parent to tenant specific
        /// </summary>
        /// <returns>pickup list values view.</returns>
        public ActionResult CopyAllPickupListsFromParent()
        {
            try
            {
                int count = PickupListProxy.CopyAllPickupListsFromParent(this.PickupListTenantId);
                TempData["Success"] = string.Format(CultureInfo.InvariantCulture, ConfigurationResource.s_CopyPickupListValue, count);
            }
            //Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                TempData["Error"] = Resources.ConfigurationResource.e_AccessDenied;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                TempData["Error"] = Resources.ConfigurationResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                TempData["Error"] = Resources.ConfigurationResource.e_ParameterEmptyOrNull;
            }
            //Catch pickup list exception and show exception message to user
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                TempData["Error"] = Resources.ConfigurationResource.e_PickupListValuesCopy;
            }

            return RedirectToAction("PickupList");
        }

        /// <summary>
        /// This method is used to render add Pickup list value for Pickup list identifier.
        /// </summary>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>Partial view of Add Pickup list value.</returns>
        public ActionResult AddPickupListValue(string pickupListId)
        {
            PickupListValue pickupListValue = new PickupListValue();

            try
            {
                List<RelationshipMetaData> relationshipList = PickupListProxy.GetRelationshipMetaData(pickupListId, this.PickupListTenantId, Relation.Child, string.Empty);
                List<RelationshipMetaData> activeRelationshipList = new List<RelationshipMetaData>();

                if (relationshipList != null && relationshipList.Count > 0)
                {
                    Dictionary<string, SelectList> pickupListDetails = new Dictionary<string, SelectList>();
                    foreach (RelationshipMetaData relation in relationshipList)
                    {
                        if (!relation.Status.Equals(true))
                        {
                            continue;
                        }

                        List<PickupListValue> pickupListValueList = PickupListProxy.GetPickupListValues(relation.TargetId, relation.TenantId);
                        SelectList selectList = new SelectList(pickupListValueList, "ItemID", "ItemName");
                        pickupListDetails.Add(relation.Id, selectList);
                        activeRelationshipList.Add(relation);
                    }

                    ViewData["PickupListDetails"] = pickupListDetails;
                }

                ViewData["RelationshipDetailss"] = activeRelationshipList;

                if (!string.IsNullOrEmpty(pickupListId))
                {
                    pickupListValue.PickupListId = pickupListId;
                    ViewData["PickupListId"] = pickupListId;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueAdd);
            }

            return PartialView("AddPickupListValue", pickupListValue);
        }

        /// <summary>
        /// This method is used to add Pickup list values.
        /// </summary>
        /// <returns>
        /// Success message or Add Pickup list partial view.
        /// </returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object AddPickupListValue()
        {
            PickupListValue pickupListValue = new PickupListValue();

            try
            {
                UpdateModel(pickupListValue);

                // Validate pickup list value details
                ValidatePickupListValueDetails(pickupListValue);

                List<RelationshipMetaData> relationshipList = PickupListProxy.GetRelationshipMetaData(pickupListValue.PickupListId, this.PickupListTenantId, Relation.Child, string.Empty);
                List<RelationshipMetaData> activeRelationshipList = new List<RelationshipMetaData>();
                List<PickupListValueMapping> pickupListValueMappingList = new List<PickupListValueMapping>();

                if (relationshipList != null && relationshipList.Count > 0)
                {
                    Dictionary<string, SelectList> pickupListDetails = new Dictionary<string, SelectList>();
                    PickupListValueMapping pickupListValueMapping = null;

                    foreach (RelationshipMetaData relation in relationshipList)
                    {
                        if (!relation.Status.Equals(true))
                        {
                            continue;
                        }

                        activeRelationshipList.Add(relation);

                        if (this.Request.Form[relation.Id] == null)
                        {
                            continue;
                        }

                        string targetValue = this.Request.Form[relation.Id];

                        List<PickupListValue> pickupListValueList = PickupListProxy.GetPickupListValues(relation.TargetId, relation.TenantId);
                        SelectList selectList = new SelectList(pickupListValueList, "ItemID", "ItemName", targetValue);
                        pickupListDetails.Add(relation.Id, selectList);

                        if (string.IsNullOrEmpty(targetValue))
                        {
                            this.ModelState.AddModelError("Val_" + relation.Id, string.Format("{0} {1}", Resources.ConfigurationResource.e_ValidTarget, relation.TargetName));
                        }
                        else
                        {
                            if (this.ModelState.IsValid)
                            {
                                pickupListValueMapping = new PickupListValueMapping();
                                PickupListValue targetPickupListValue = PickupListProxy.GetPickupListValue(targetValue, relation.TargetId, this.PickupListTenantId);
                                pickupListValueMapping.TargetValueId = targetPickupListValue.Id;
                                pickupListValueMapping.RelationshipMetaDataId = relation.Id;
                                pickupListValueMapping.TenantId = relation.TenantId;
                                pickupListValueMappingList.Add(pickupListValueMapping);
                            }
                        }
                    }

                    ViewData["PickupListDetails"] = pickupListDetails;

                }

                ViewData["RelationshipDetailss"] = activeRelationshipList;

                if (ModelState.IsValid)
                {
                    pickupListValue.TenantCode = this.PickupListTenantId;
                    pickupListValue.ItemId = pickupListValue.ItemId.Trim();
                    pickupListValue.ItemName = pickupListValue.ItemName.Trim();
                    string pickupListValueId = string.Empty;

                    using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        //Call add pickup list value method to add pickup list value details
                        pickupListValueId = PickupListProxy.AddPickupListValue(pickupListValue);

                        if (!string.IsNullOrEmpty(pickupListValueId))
                        {
                            if (pickupListValueMappingList != null && pickupListValueMappingList.Count > 0)
                            {
                                foreach (PickupListValueMapping pickupListValueMapping in pickupListValueMappingList)
                                {
                                    pickupListValueMapping.SourceValueId = pickupListValueId;
                                }
                                PickupListProxy.SavePickupListValueMapping(pickupListValueMappingList);
                            }

                            transactionScope.Complete();
                        }
                        else
                        {
                            this.ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueAdd);
                        }
                    }

                    if (!string.IsNullOrEmpty(pickupListValueId))
                    {
                        ModelState.AddModelError("Success", Resources.ConfigurationResource.s_PickupListValueAdd);
                        PickupListValueDetails(pickupListValue.PickupListId);
                        return PartialView("PartialPickupListValues");
                    }
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueAdd);
            }
            catch (PickupListValueDuplicateValueException duplicateValueException)
            {
                ExceptionService.HandleException(duplicateValueException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_DuplicateValue);
            }
            catch (PickupListValueDuplicateNameException duplicateNameException)
            {
                ExceptionService.HandleException(duplicateNameException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_DuplicateName);
            }

            return PartialView("AddPickupListValue", pickupListValue);
        }

        /// <summary>
        /// This method is used to get Pickup list value for edit.
        /// </summary>
        /// <param name="pickupListValueId">pickup list value identifier</param>
        /// <param name="pickupListId">pickup list identifier.</param>
        /// <returns>edit pickup list partial view</returns>
        public ActionResult EditPickupListValue(string pickupListValueId, string pickupListId)
        {
            PickupListValue pickupListValue = null;

            try
            {
                pickupListValue = PickupListProxy.GetPickupListValue(pickupListValueId, pickupListId);
                List<RelationshipMetaData> activeRelationshipList = new List<RelationshipMetaData>();
                List<RelationshipMetaData> relationshipList = PickupListProxy.GetRelationshipMetaData(pickupListId, this.PickupListTenantId, Relation.Child, string.Empty);

                if (relationshipList != null && relationshipList.Count > 0)
                {
                    pickupListValue = PickupListProxy.GetChildPickupListValueDetails(pickupListId, pickupListValueId, this.PickupListTenantId);

                    if (pickupListValue != null && pickupListValue.Details != null)
                    {
                        Dictionary<string, SelectList> pickupListDetails = new Dictionary<string, SelectList>();
                        int count = 2;

                        foreach (RelationshipMetaData relation in relationshipList)
                        {
                            if (!relation.Status.Equals(true))
                            {
                                continue;
                            }

                            activeRelationshipList.Add(relation);

                            List<PickupListValue> pickupListValueList = PickupListProxy.GetPickupListValues(relation.TargetId, relation.TenantId);
                            object selectedValue = pickupListValue.Details.ItemArray.Count() > count ? pickupListValue.Details[count] : null;
                            SelectList selectList = new SelectList(pickupListValueList, "ItemID", "ItemName", selectedValue);
                            pickupListDetails.Add(relation.Id, selectList);
                            count += 2;
                        }

                        ViewData["PickupListDetails"] = pickupListDetails;
                    }
                }

                ViewData["RelationshipDetailss"] = activeRelationshipList;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueLoad);
            }

            return PartialView("EditPickupListValue", pickupListValue);
        }

        /// <summary>
        /// This method is used to update Pickup list values.
        /// </summary>
        /// <returns>
        /// Success message or Edit Pickup list partial view.
        /// </returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object EditPickupListValue()
        {
            PickupListValue pickupListValue = new PickupListValue();

            try
            {
                UpdateModel(pickupListValue);

                // Validate pickup list value details
                ValidatePickupListValueDetails(pickupListValue);

                List<RelationshipMetaData> relationshipList = PickupListProxy.GetRelationshipMetaData(pickupListValue.PickupListId, this.PickupListTenantId, Relation.Child, string.Empty);
                List<RelationshipMetaData> activeRelationshipList = new List<RelationshipMetaData>();
                List<PickupListValueMapping> pickupListValueMappingList = new List<PickupListValueMapping>();

                if (relationshipList != null && relationshipList.Count > 0)
                {
                    pickupListValue = PickupListProxy.GetChildPickupListValueDetails(pickupListValue.PickupListId, pickupListValue.Id, this.PickupListTenantId);

                    Dictionary<string, SelectList> pickupListDetails = new Dictionary<string, SelectList>();
                    PickupListValueMapping pickupListValueMapping = null;

                    foreach (RelationshipMetaData relation in relationshipList)
                    {
                        if (!relation.Status.Equals(true))
                        {
                            continue;
                        }

                        activeRelationshipList.Add(relation);

                        if (this.Request.Form[relation.Id] == null)
                        {
                            continue;
                        }

                        string targetValue = this.Request.Form[relation.Id];

                        List<PickupListValue> pickupListValueList = PickupListProxy.GetPickupListValues(relation.TargetId, relation.TenantId);
                        SelectList selectList = new SelectList(pickupListValueList, "ItemID", "ItemName", targetValue);
                        pickupListDetails.Add(relation.Id, selectList);

                        if (string.IsNullOrEmpty(targetValue))
                        {
                            this.ModelState.AddModelError("Val_" + relation.Id, string.Format("{0} {1}", Resources.ConfigurationResource.e_ValidTarget, relation.TargetName));
                        }
                        else
                        {
                            if (this.ModelState.IsValid)
                            {
                                pickupListValueMapping = new PickupListValueMapping();
                                PickupListValue targetPickupListValue = PickupListProxy.GetPickupListValue(targetValue, relation.TargetId, this.PickupListTenantId);
                                pickupListValueMapping.TargetValueId = targetPickupListValue.Id;
                                pickupListValueMapping.RelationshipMetaDataId = relation.Id;
                                pickupListValueMapping.TenantId = relation.TenantId;
                                pickupListValueMappingList.Add(pickupListValueMapping);
                            }
                        }
                    }

                    ViewData["PickupListDetails"] = pickupListDetails;
                }

                ViewData["RelationshipDetailss"] = activeRelationshipList;

                if (ModelState.IsValid)
                {
                    string pickupListValueId = string.Empty;
                    pickupListValue.ItemId = pickupListValue.ItemId.Trim();
                    pickupListValue.ItemName = pickupListValue.ItemName.Trim();

                    using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        pickupListValueId = PickupListProxy.UpdatePickupListValue(pickupListValue);

                        // Pickup list value updated successfully then return success message else return edit pickup list value view
                        if (!string.IsNullOrEmpty(pickupListValueId))
                        {
                            if (pickupListValueMappingList != null && pickupListValueMappingList.Count > 0)
                            {
                                foreach (PickupListValueMapping pickupListValueMapping in pickupListValueMappingList)
                                {
                                    pickupListValueMapping.SourceValueId = pickupListValueId;
                                }

                                PickupListProxy.SavePickupListValueMapping(pickupListValueMappingList);
                            }

                            transactionScope.Complete();
                        }
                        else
                        {
                            this.ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueUpdate);
                        }
                    }

                    if (!string.IsNullOrEmpty(pickupListValueId))
                    {
                        ModelState.AddModelError("Success", Resources.ConfigurationResource.s_PickupListValueUpdate);
                        PickupListValueDetails(pickupListValue.PickupListId);
                        return PartialView("PartialPickupListValues");
                    }
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueUpdate);
            }
            catch (PickupListValueDuplicateValueException duplicateValueException)
            {
                ExceptionService.HandleException(duplicateValueException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_DuplicateValue);
            }
            catch (PickupListValueDuplicateNameException duplicateNameException)
            {
                ExceptionService.HandleException(duplicateNameException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_DuplicateName);
            }

            return PartialView("EditPickupListValue", pickupListValue);
        }

        /// <summary>
        /// This method is used to deactivate Pickup list value.
        /// </summary>
        /// <param name="pickupListValueId">pickup list value identifier</param>
        /// <param name="pickupListId">pickup list identifier</param>
        /// <param name="tenantCode">tenant code</param>
        /// <returns>pickup list value details partial view</returns>
        public ActionResult DeletePickupListValue(string pickupListValueId, string pickupListId, string tenantCode)
        {
            try
            {
                //Deactivaet the pickup list value
                PickupListProxy.DeactivatePickupListValue(pickupListValueId, pickupListId, tenantCode);
                ModelState.AddModelError("Success", Resources.ConfigurationResource.s_PickupListValueDelete);
            }
            //Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            //Catch pickup list exception and show exception message to user
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueDelete);
            }

            //Get pickup list value details 
            PickupListValueDetails(pickupListId);

            return PartialView("PartialPickupListValues");
        }

        /// <summary>
        /// This method is used to activate Pickup list value.
        /// </summary>
        /// <param name="pickupListValueId">pickup list value identifier</param>
        /// <param name="pickupListId">pickup list identifier</param>
        /// <param name="tenantCode">tenant code</param>
        /// <returns>pickup list value details partial view</returns>
        public ActionResult ActivatePickupListValue(string pickupListValueId, string pickupListId, string tenantCode)
        {
            try
            {
                //Activate the pickup list value
                PickupListProxy.ActivatePickupListValue(pickupListValueId, pickupListId, tenantCode);
                ModelState.AddModelError("Success", ConfigurationResource.s_ActivatePickupListValue);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_PickupListValueDelete);
            }

            PickupListValueDetails(pickupListId);

            return PartialView("PartialPickupListValues");
        }

        #endregion

        #region Pickup List Values Public Methods

        /// <summary>
        /// This method is used to validate the Pickup list value details.
        /// </summary>
        /// <param name="pickupListValue">pickup list value details.</param>
        private void ValidatePickupListValueDetails(PickupListValue pickupListValue)
        {
            //Check pickup list value is null or empty
            if (string.IsNullOrWhiteSpace(pickupListValue.ItemId))
            {
                ModelState.AddModelError("ItemId", Resources.ConfigurationResource.e_InvalidValue);
            }
            //Check pickup list value format. Input should contain only alphabets,numbers and (_,-,.)
            else
            {
                if (!Util.ValidateIdentifier(pickupListValue.ItemId))
                    ModelState.AddModelError("Error", Resources.ConfigurationResource.e_InvalidValue);
            }
            //Check pickup list value name is null or empty
            if (string.IsNullOrWhiteSpace(pickupListValue.ItemName))
            {
                ModelState.AddModelError("ItemName", Resources.ConfigurationResource.e_InvalidName);
            }
        }

        #endregion

        #region Relationship

        /// <summary>
        /// This method is used to get relationship details.
        /// </summary>
        /// <returns>Relationship Details View</returns>
        public ActionResult RelationshipDetailsList()
        {
            try
            {
                if (TempData["AddRelationshipSuccessMessage"] != null && !string.IsNullOrEmpty(TempData["AddRelationshipSuccessMessage"].ToString()))
                {
                    ModelState.AddModelError("RelationshipSuccessMessage", Resources.ConfigurationResource.s_AddRelationShip);
                }
                List<RelationshipMetaData> lstRelationshipMetaData = PickupListProxy.GetRelationshipMetaData(this.PickupListTenantId);
                ViewData["RelationshipMetaDataList"] = lstRelationshipMetaData;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_RelationshipLoad);
            }
            return View("RelationshipDetailsList");
        }

        /// <summary>
        /// This method is used to Add relationship metadata details.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddRelationshipMetaData()
        {
            try
            {
                GetTargetType(string.Empty);
                GetSourceValue(string.Empty);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("AccessError", Resources.ConfigurationResource.e_AccessDenied);
            } //Catch pickup list exception and show exception message to user
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("AccessError", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("AccessError", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_RelationshipAdd);
            }

            return View("AddRelationshipMetaData", new RelationshipMetaData());
        }

        /// <summary>
        /// This method is used to add relationship details.
        /// </summary>
        /// <param name="formCollection">from collection.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddRelationshipMetaData(FormCollection formCollection)
        {
            RelationshipMetaData relationshipMetaData = new RelationshipMetaData();
            UpdateModel(relationshipMetaData);
            ValidateRelationshipMetaData(relationshipMetaData);
            if (ModelState.IsValid)
            {
                try
                {
                    relationshipMetaData.TenantId = this.PickupListTenantId;
                    relationshipMetaData.RelationType = Convert.ToInt32(Relation.Child, CultureInfo.InvariantCulture).ToString(CultureInfo.InvariantCulture);
                    relationshipMetaData.Id = PickupListProxy.AddRelationshipMetaData(relationshipMetaData);
                    if (string.IsNullOrEmpty(relationshipMetaData.Id))
                        ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_RelationshipAdd);
                    else
                    {
                        TempData["AddRelationshipSuccessMessage"] = Resources.ConfigurationResource.s_AddRelationShip;
                        return RedirectToAction("RelationshipDetailsList", "Configuration");
                    }

                }
                //Catch UnauthorizedAccess exception and show exception message to user
                catch (UnauthorizedAccessException unauthorizedException)
                {
                    ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                    ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_AccessDenied);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                    ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
                }
                //Catch argument exception and show exception message to user
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, DefaultPolicy);
                    ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
                }
                //Catch pickup list exception and show exception message to user
                catch (PickupListException pickupListException)
                {
                    ExceptionService.HandleException(pickupListException, DefaultPolicy);
                    ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_RelationshipAdd);
                }
                //Catch DuplicateRelationshipMetaDataException and show exception message to user
                catch (DuplicateRelationshipMetaDataException pickupListException)
                {
                    ExceptionService.HandleException(pickupListException, DefaultPolicy);
                    ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_DuplicateRelationship);
                }
            }
            GetTargetType(relationshipMetaData.TargetType);
            GetSourceValue(relationshipMetaData.SourceId);
            return View("AddRelationshipMetaData", relationshipMetaData);
        }

        /// <summary>
        /// This method is used to deactivate relationship details.
        /// </summary>
        /// <param name="relationshipId">relationship identifier</param>
        /// <param name="status">relationship status(true/false).</param>
        /// <returns></returns>
        public ActionResult DeactivateRelationshipMetaData(string relationshipId, bool status)
        {
            try
            {
                RelationshipMetaData relationshipMetaData = PickupListProxy.GetRelationshipMetaDataDetails(relationshipId);
                if (relationshipMetaData != null && !string.IsNullOrEmpty(relationshipMetaData.Id))
                {
                    relationshipMetaData.Status = status;
                    PickupListProxy.UpdateRelationshipMetaData(relationshipMetaData);
                    if (status)
                    {
                        ModelState.AddModelError("RelationshipSuccessMessage", Resources.ConfigurationResource.s_ActivateRelationship);
                    }
                    else
                    {
                        ModelState.AddModelError("RelationshipSuccessMessage", Resources.ConfigurationResource.s_DeactivateRelationship);
                    }
                }
                else
                    ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_RelationshipUpdate);
            }
            //Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            //Catch pickup list exception and show exception message to user
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                ModelState.AddModelError("RelationshipErrorMessage", Resources.ConfigurationResource.e_RelationshipUpdate);
            }
            return RelationshipDetailsList();
        }

        /// <summary>
        /// This method is used to validate Relationship details.
        /// </summary>
        /// <param name="relationshipMetaData">relationship meta data details.</param>
        private void ValidateRelationshipMetaData(RelationshipMetaData relationshipMetaData)
        {
            if (string.IsNullOrEmpty(relationshipMetaData.SourceId))
            {
                ModelState.AddModelError("SourceId", "");
                ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_ChildValueMandatory);
            }
            if (string.IsNullOrEmpty(relationshipMetaData.TargetType))
            {
                ModelState.AddModelError("TargetType", "");
                ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_ParentTypeMandatory);
            }
            else
            {
                if (relationshipMetaData.TargetType == "Entity")
                {
                    if (string.IsNullOrEmpty(relationshipMetaData.TargetDisplayName) || string.IsNullOrEmpty(relationshipMetaData.TargetDisplayName.Trim()))
                    {
                        ModelState.AddModelError("TargetDisplayName", "");
                        ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_ParentDisplayNameMandatory);
                    }
                    if (string.IsNullOrEmpty(relationshipMetaData.TargetKeyName) || string.IsNullOrEmpty(relationshipMetaData.TargetKeyName.Trim()))
                    {
                        ModelState.AddModelError("TargetKeyName", "");
                        ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_ParentKeyNameMandatory);
                    }
                }
            }
            if ((!string.IsNullOrEmpty(relationshipMetaData.TargetId) &&
                 !string.IsNullOrEmpty(relationshipMetaData.TargetId.Trim())) &&
                !relationshipMetaData.TargetId.Equals("-- Select Parent --"))
            {
                return;
            }
            this.ModelState.AddModelError("TargetId", "");
            this.ModelState.AddModelError("AddRelationshipErrorMessage", Resources.ConfigurationResource.e_ParentValueMandatory);
        }

        /// <summary>
        /// This method is used to get relationship type details.
        /// </summary>
        /// <param name="targetTypeValue">relationship type value.</param>
        private void GetTargetType(string targetTypeValue)
        {
            List<string> targetType = new List<string> { "PickupList" };
            ViewData["TargetType"] = new SelectList(targetType, targetTypeValue);
        }

        /// <summary>
        /// This method is used to get pickup list value details.
        /// </summary>
        /// <param name="sourceId">pickup list identifier.</param>
        private void GetSourceValue(string sourceId)
        {
            //Get pickup list details for tenant
            List<PickupList> lstPickupList = PickupListProxy.GetPickupLists(this.PickupListTenantId);
            Dictionary<string, string> child = new Dictionary<string, string> { { "", "-- Select Child --" } };
            foreach (PickupList pickupList in lstPickupList)
            {
                child.Add(pickupList.Id, "P : " + pickupList.Name);
            }

            ViewData["SourceId"] = new SelectList(child, "Key", "Value", sourceId);
            List<string> target = new List<string>();
            ViewData["TargetId"] = new SelectList(target);
            ViewData["TargetKeyName"] = new SelectList(target);
        }

        /// <summary>
        /// This method is used to get parent details based on the type.
        /// </summary>
        /// <param name="targetType">target type(pick up list/entity).</param>
        /// <returns>Pickup list/Entity List</returns>
        public JsonResult GetParentDetails(string targetType)
        {
            try
            {
                List<string> parentDetails = new List<string>();
                if (targetType == "PickupList")
                {
                    //Get pickup lists for tenant
                    List<PickupList> lstPickupList = PickupListProxy.GetPickupLists(this.PickupListTenantId);
                    if (lstPickupList != null && lstPickupList.Count > 0)
                    {
                        foreach (PickupList pickupList in lstPickupList)
                        {
                            parentDetails.Add(pickupList.Id + "," + pickupList.Name);
                        }
                    }
                }
                else
                {
                    //Get entity details
                    Dictionary<string, EntityMetaData> entityList = DataManagementProxy.GetAllEntityMetaData();
                    if (entityList != null && entityList.Count > 0)
                    {
                        foreach (EntityMetaData entity in entityList.Values.ToList())
                        {
                            if (!string.IsNullOrEmpty(entity.SchemaTableName))
                            {
                                parentDetails.Add(entity.EntityIdentifier + "," + entity.EntityIdentifier);
                            }
                        }
                    }
                }
                if (parentDetails != null && parentDetails.Count > 0)
                {
                    return Json(new SelectList(parentDetails), JsonRequestBehavior.AllowGet);
                }
                return this.Json(Resources.ConfigurationResource.e_NoRecord, JsonRequestBehavior.AllowGet);
            }
            catch (ArgumentNullException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
            }
            catch (PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, DefaultPolicy);
            }

            return Json(Resources.ConfigurationResource.e_ErrorMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Module Config Setting
        /// <summary>
        /// This method is used for module configuration.
        /// </summary>
        /// <returns></returns>
        public ActionResult ModuleConfiguration()
        {
            Dictionary<string, ModuleConfiguration> moduleConfigList = null;

            try
            {
                moduleConfigList = ModuleConfigurationSettingProxy.GetAllModuleConfiguration();

                if (Request.HttpMethod == "POST" && Request.Form.Count > 0 && moduleConfigList != null)
                {
                    var newSettings = new Dictionary<string, string>();
                    foreach (string key in Request.Form.Keys)
                    {
                        if (moduleConfigList.ContainsKey(key)
                            && !newSettings.ContainsKey(key)
                            && !string.IsNullOrEmpty(Request.Form[key]))
                        {
                            newSettings.Add(key, Request.Form[key]);
                        }
                    }

                    //save new settings to db
                    ModuleConfigurationSettingProxy.UpdateModuleConfigurationSetting(this.ModuleConfigTenantId, newSettings);
                    ViewData["Success"] = ConfigurationResource.s_SaveConfigurationSettings;
                }

                var msettings = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.ModuleConfigTenantId);

                ViewData["ModuleConfigList"] = moduleConfigList;
                ViewData["ModuleConfigSettingValue"] = msettings;
            }
            catch (ArgumentNullException)
            {
                ModelState.AddModelError("Error", ConfigurationResource.e_ParameterEmptyOrNull);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(moduleConfigList);
        }

        #endregion

    }
}