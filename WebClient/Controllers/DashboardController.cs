﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.Configuration;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaSApplication.Models;

namespace CelloSaaSApplication.Controllers
{   
    /// <summary>
    /// This class is responsible for dashboard management.
    /// </summary>
    public class DashboardController : CelloSaaS.View.CelloController
    {
        private const string policyName = "GlobalExceptionLogger";
        
        /// <summary>
        /// This method is used to load tenant account dashboard view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if ((TempData["RedirectToSessionTenant"] != null && (bool)TempData["RedirectToSessionTenant"]) || UserIdentity.Privileges == null)
            {
                return RedirectToAction("SessionTenant", "Home");
            }

            return View();
        }

        /// <summary>
        /// This method is used to get the product dashboard view.
        /// </summary>
        /// <returns></returns>
        public ActionResult ProductDashboard()
        {
            return View();
        }

        /// <summary>
        /// This method is used to get the account details based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public PartialViewResult MyAccountDetails(string tenantId)
        {
            TenantViewModel model = null;

            try
            {
                if (string.IsNullOrEmpty(tenantId))
                    tenantId = UserIdentity.TenantID;

                var tenantInfo = TenantProxy.GetTenantDetailsByTenantId(tenantId, tenantId);
                var parentTenantId = tenantInfo.TenantDetails.ParentTenantId;

                var license = LicenseProxy.GetTenantLicenseHistory(tenantId).First();

                PricePlan plan = null;

                if (license != null && !string.IsNullOrEmpty(license.PricePlanId))
                {
                    plan = BillingPlanProxy.GetPricePlan(Guid.Parse(license.PricePlanId), Guid.Parse(parentTenantId));
                }

                int onlineUsers = UserDetailsProxy.GetOnlineUsersCount(tenantId);
                var userCount = UserDetailsProxy.GetUserCountByTenantId(tenantId);

                var childs = TenantRelationProxy.GetAllChildTenants(tenantId);
                int childTenants = childs != null ? childs.Count : 0;

                var tenantSetting = TenantSettingsProxy.GetTenantSettings(tenantId);
                string logo = string.Empty;

                if (tenantSetting != null
                    && tenantSetting.Setting != null
                    && tenantSetting.Setting.Attributes != null
                    && tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.Logo))
                {
                    logo = tenantSetting.Setting.Attributes[AttributeConstants.Logo];
                }

                DateTime startDate, endDate, chargeDate;
                CelloSaaS.View.Extensions.GetStartAndEndDates(license, out startDate, out endDate, out chargeDate);
                var usageAmounts = MeteringProxy.GetUsageAmount(null, startDate, endDate, tenantId);
                var childUsageAmounts = MeteringProxy.GetChildTenantsUsageAmount(null, startDate, endDate, tenantId);
                var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(license.PackageId);
                BillStatistics billStats = null;
                Dictionary<Guid, BillStatistics> b = null;

                if (tenantInfo.Types != null && !tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.OrgUnit)
                    && UserIdentity.HasPrivilege(PrivilegeConstants.ViewInvoice))
                {
                    DateTime fromDate = DateTime.ParseExact("01/01/" + DateTime.Now.Year, "MM/d/yyyy", CultureInfo.InvariantCulture);
                    DateTime toDate = DateTime.ParseExact("12/31/" + DateTime.Now.Year, "MM/d/yyyy", CultureInfo.InvariantCulture);

                    var bills = BillingProxy.GetRevenues(fromDate, toDate, tenantId);

                    if (bills == null)
                        bills = new Dictionary<DateTime, Tuple<int, double, double, double>>();

                    billStats = new BillStatistics()
                    {
                        TenantId = Guid.Parse(tenantId),
                        TotalBills = bills.Sum(x => x.Value.Item1),
                        TotalAmount = bills.Sum(x => x.Value.Item2),
                        TotalPaid = bills.Sum(x => x.Value.Item3),
                        TotalTax = bills.Sum(x => x.Value.Item4)
                    };

                    b = BillingProxy.GetBillStatistics(new BillStatisticsSearchCondition
                    {
                        TenantIds = new Guid[] { Guid.Parse(tenantId) }
                    });
                }

                model = new TenantViewModel
                {
                    Tenant = tenantInfo,
                    TenantDetails = tenantInfo.TenantDetails,
                    License = license,
                    Plan = plan,
                    UsageAmounts = usageAmounts,
                    ChildUsageAmounts = childUsageAmounts,
                    PackageDetails = packageDetails,
                    TotalUsers = userCount != null ? (userCount.ContainsKey(true) ? userCount[true] : 0) : 0,
                    OnlineUsers = onlineUsers,
                    TotalChildTenants = childTenants,
                    Logo = logo,
                    BillStatistics = billStats,
                    OverallOverdue = b != null && b.ContainsKey(Guid.Parse(tenantId)) ? b[Guid.Parse(tenantId)].Overdue : 0.0
                };
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, policyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, policyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_Access);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }

            return PartialView(model);
        }

        #region Tenant Hierarchy

        /// <summary>
        /// This method is used to get the hierarchy wise tenants.
        /// </summary>
        /// <returns></returns>
        [CelloSaaS.View.DisableTrackUsage]
        public PartialViewResult TenantHierarchyViz()
        {
            try
            {
                var tenantId = UserIdentity.TenantID;
                var tenantInfo = TenantProxy.GetTenantDetailsByTenantId(tenantId, tenantId);
                var tenantLicense = LicenseProxy.GetTenantLicense(tenantId);

                var tenantHierarchy = TenantRelationProxy.GetChildTenantHierarchy(tenantId);

                var licenses = new Dictionary<string, TenantLicense>();
                FetchTenantLicense(tenantHierarchy, licenses);

                ViewBag.rootTenant = tenantInfo;
                ViewBag.rootTenantLicense = tenantLicense;
                ViewBag.tenantHierarchy = tenantHierarchy;
                ViewBag.tenantLicenses = licenses;
                ViewBag.hierarchyData = GetHierarchyData(tenantInfo, tenantLicense, tenantHierarchy, licenses);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, policyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, policyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_Access);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to get the hierarchy data.
        /// </summary>
        /// <param name="root">root tenant.</param>
        /// <param name="tenantLicense">tenant license details.</param>
        /// <param name="tenantHierarchy">tenant hierarchy details.</param>
        /// <param name="licenses">collection of tenant licenses.</param>
        /// <returns></returns>
        private static TenantHierarchyViewModel GetHierarchyData(Tenant root, TenantLicense tenantLicense, Dictionary<string, TenantDetails> tenantHierarchy, Dictionary<string, TenantLicense> licenses)
        {
            var data = new TenantHierarchyViewModel { name = root.TenantDetails.TenantName, children = new List<TenantHierarchyViewModel>() };
            data.type = root.Types != null && root.Types.Count > 0 ? root.Types.First().Name : "NA";
            data.size = 5000;
            data.position = 0;
            //data.email = root.ContactDetail != null ? root.ContactDetail.Email : "NA";

            if (tenantHierarchy != null && tenantHierarchy.Count > 0)
            {
                //var searchResult = TenantProxy.SearchTenantDetails(new TenantDetailsSearchCondition
                //{
                //    TenantIds = tenantHierarchy.Keys.Select(x => Guid.Parse(x)).ToArray()
                //});

                //var allTenantInfos = searchResult.Items;
                VisitTenant(1, tenantHierarchy, data, licenses);
            }

            return data;
        }


        /// <summary>
        /// This method is used for getting the hierarchy of tenant based on the given parameters.
        /// </summary>
        /// <param name="pos">position.</param>
        /// <param name="tenantHierarchy">tenant hierarchy.</param>
        /// <param name="data">data.</param>
        /// <param name="licenses">collection of tenant licenses.</param>
        private static void VisitTenant(int pos, Dictionary<string, TenantDetails> tenantHierarchy, TenantHierarchyViewModel data, Dictionary<string, TenantLicense> licenses)
        {
            foreach (var t in tenantHierarchy.Values)
            {
                pos++;
                //var tenantInfo = allTenantInfos != null && allTenantInfos.ContainsKey(t.TenantCode) ? allTenantInfos[t.TenantCode] : null;

                //if (tenantInfo == null) continue;

                var tenantInfo = TenantProxy.GetTenantInfo(t.TenantCode);

                var types = tenantInfo != null ? tenantInfo.Types : null;

                var item = new TenantHierarchyViewModel { name = t.TenantName };
                item.size = 5000;
                item.position = (int)(pos / 20);
                item.type = types != null && types.Count > 0 ? types.First().Name : "NA";
                //item.email = tenantInfo.ContactDetail != null ? tenantInfo.ContactDetail.Email : "NA";

                if (t.childTenant != null && t.childTenant.Count > 0)
                {
                    item.children = new List<TenantHierarchyViewModel>();
                    data.size += (int)((Math.Sqrt(t.childTenant.Count) / 10) * 150);

                    //var searchResult = TenantProxy.SearchTenantDetails(new TenantDetailsSearchCondition
                    //{
                    //    TenantIds = t.childTenant.Keys.Select(x => Guid.Parse(x)).ToArray()
                    //});
                    FetchTenantLicense(t.childTenant, licenses);

                    VisitTenant(pos, t.childTenant, item, licenses);
                    data.children.Add(item);
                }
                else
                {
                    item.children = null;
                    data.children.Add(item);
                }
            }
        }
        /// <summary>
        /// This method is used to retrieve the tenant license.
        /// </summary>
        /// <param name="tenantHierarchy">collection of tenant hierarchy.</param>
        /// <param name="licenses">collection of tenant licenses.</param>
        private static void FetchTenantLicense(Dictionary<string, TenantDetails> tenantHierarchy, Dictionary<string, TenantLicense> licenses)
        {
            if (tenantHierarchy == null || tenantHierarchy.Count == 0)
            {
                return;
            }

            var tenantLicenses = LicenseProxy.SearchTenantLicenses(new TenantLicenseSearchCondition
            {
                TenantIds = tenantHierarchy.Select(x => Guid.Parse(x.Key)).ToArray(),
            });

            foreach (var item in tenantHierarchy)
            {
                var tid = Guid.Parse(item.Key);
                var tenantLicense = tenantLicenses.ContainsKey(tid) ? tenantLicenses[tid] : null;
                if (!licenses.ContainsKey(item.Key))
                {
                    licenses.Add(item.Key, tenantLicense);
                }
                FetchTenantLicense(item.Value.childTenant, licenses);
            }
        }

        #endregion

        #region Billing
        /// <summary>
        /// This method is used to get the un paid bills.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult UnPaidBills()
        {
            try
            {
                ViewBag.UnPaidBills = BillingProxy.SearchInvoices(new InvoiceSearchCondition
                {
                    TenantIds = new Guid[] { Guid.Parse(UserIdentity.TenantID) },
                    InvoiceStatus = InvoiceStatus.UnPaid
                });
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        #endregion

        #region Usage Alerts
        /// <summary>
        /// This method is used to get the usage alert.
        /// </summary>
        /// <param name="showIcon">showIcon (true/false)</param>
        /// <returns></returns>
        [CelloSaaS.View.DisableTrackUsage]
        public PartialViewResult UsageAlert(bool showIcon = false)
        {
            ViewBag.ShowAlertIcon = showIcon;
            try
            {
                string tenantId = UserIdentity.TenantID;

                var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(tenantId, tenantId);

                // do not show upgrade button for org units
                ViewData["ShowUpgradeButton"] = tenantDetails.Types != null && tenantDetails.Types.Any(x => x.ID != TenantTypeConstants.OrgUnit);

                var tenantLicense = LicenseProxy.GetTenantLicense(tenantId);

                if (tenantLicense != null)
                {
                    var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(tenantLicense.PackageId);
                    ViewData["PackageDetails"] = packageDetails;

                    DateTime startDate, endDate, chargeDate;
                    CelloSaaS.View.Extensions.GetStartAndEndDates(tenantLicense, out startDate, out endDate, out chargeDate);

                    var usageAmounts = MeteringProxy.GetUsageAmount(null, startDate, endDate, tenantId);
                    ViewData["UsageAmounts"] = usageAmounts;

                    var childUsageAmounts = MeteringProxy.GetChildTenantsUsageAmount(null, startDate, endDate, tenantId);
                    ViewData["ChildUsageAmounts"] = childUsageAmounts;
                }
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        #endregion

        #region Online Users
        /// <summary>
        /// This method is used to list the number of users in online based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        [CelloSaaS.View.DisableTrackUsage]
        public PartialViewResult OnlineUsers(string tenantId)
        {
            try
            {
                if (string.IsNullOrEmpty(tenantId))
                {
                    tenantId = UserIdentity.TenantID;
                }

                ViewBag.OnlineUsers = UserDetailsProxy.GetOnlineUsers(tenantId);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView();
        }

        #endregion
    }
}
