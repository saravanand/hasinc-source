﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.DataBackup.Model;
using CelloSaaS.DataBackup.ServiceContracts;
using CelloSaaS.DataBackup.ServiceProxy;
using CelloSaaS.Library;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for tenant data backup management.
    /// </summary>
    public class DataBackupController : CelloController
    {
        const string policyName = "DatabackupExceptionLogger";

        private string TenantId = TenantContext.GetTenantId("_DataBackup");
        private string UserId = UserIdentity.UserId;

        /// <summary>
        /// This method is sued to displays the backup request in a html table.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <param name="page">page.</param>
        /// <param name="pageSize">size of the page.</param>
        /// <returns></returns>
        public ActionResult Index(BackupRequestSearchCondition condition, int page = 1, int pageSize = 10)
        {
            if (condition == null)
                condition = new BackupRequestSearchCondition();

            try
            {
                condition.PageNo = page;
                condition.PageSize = pageSize;
                condition.PrimaryTenantId = this.TenantId;

                ViewBag.PageNumber = page;
                ViewBag.PageSize = pageSize;
                ViewBag.SortString = condition.SortString ?? "RequestDate";
                ViewBag.SortDirection = condition.SortDirection ?? CelloSaaS.Model.SortExpressionConstants.Decending;
                ViewBag.Condition = condition;
                ViewBag.CanCreate = false;

                var priv = AccessControlProxy.GetTenantAccessPrivileges(this.UserId, new string[] { PrivilegeConstants.CreateDatabackup });

                if (priv != null && priv.ContainsKey(PrivilegeConstants.CreateDatabackup)
                    && priv[PrivilegeConstants.CreateDatabackup].ContainsKey(this.TenantId))
                {
                    ViewBag.CanCreate = true;
                }

                var result = BackupRequestProxy.SearchBackupRequest(condition);
                ViewBag.DataBackupList = result.Result;
                ViewBag.TotalCount = result.TotalCount;

                if (result.Result != null && result.Result.Count > 0)
                {
                    var tids = result.Result.Select(x => x.Value.PrimaryTenantId).Distinct().ToArray();

                    var tenants = TenantProxy.GetTenantDetailsForShareUsers(tids);

                    ViewBag.TenantNames = tenants != null ? tenants.ToDictionary(x => x.Key, y => y.Value.TenantName) : null;
                }
            }
            catch (BackupRequestException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            if (TempData["Error"] != null)
            {
                ModelState.AddModelError("Error", TempData["Error"].ToString());
            }

            if (TempData["Success"] != null)
            {
                ModelState.AddModelError("Success", TempData["Success"].ToString());
            }

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("BackupRequestGrid");
            }

            return View(condition);
        }

        /// <summary>
        /// This method is used to views the request details.
        /// </summary>
        /// <param name="requestId">request identifier.</param>
        /// <param name="condition">condition.</param>
        /// <returns></returns>
        public ActionResult ViewRequestDetails(string requestId, BackupRequestSearchCondition condition)
        {
            if (string.IsNullOrEmpty(requestId))
            {
                ModelState.AddModelError("Error", Resources.DataBackupResource.e_RequestId);
            }

            ViewBag.Condition = condition;
            BackupRequest request = null;

            try
            {
                if (ModelState.IsValid)
                {
                    request = BackupRequestProxy.GetBackupRequestDetails(requestId);
                }
            }
            catch (BackupRequestException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(request);
        }
        /// <summary>
        /// This method is used to manages the backup request.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageBackupRequest()
        {
            PopulateTenantAndModeDropdowns();

            return View(new BackupRequest());
        }

        /// <summary>
        /// This method is used to populates the tenant and mode dropdowns.
        /// </summary>
        private void PopulateTenantAndModeDropdowns()
        {
            ViewBag.TenantIds = null;

            // startification tenant is removed use session tenant
            /*if (this.TenantId.Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase))
            {
                var tenantList = new List<TenantDetails>();
                
                //var childTenants = TenantRelationProxy.GetAllChildTenants(this.TenantId);
                var startifiedTenants = AccessControlProxy.GetTenantAccessPrivileges(this.UserId, new string[] { PrivilegeConstants.ViewDatabackup });
                var childTenants = (startifiedTenants != null && startifiedTenants.ContainsKey(PrivilegeConstants.ViewDatabackup)) ? startifiedTenants[PrivilegeConstants.ViewDatabackup] : null;

                if (childTenants != null && childTenants.Count > 0)
                {
                    tenantList.AddRange(childTenants.Values);
                }

                if (tenantList.Count > 0)
                {
                    ViewBag.TenantIds = tenantList.OrderBy(x => x.TenantName)
                                .Select(x => new SelectListItem
                                {
                                    Value = x.TenantCode,
                                    Text = x.TenantName,
                                    Selected = (x.TenantCode.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase))
                                });
                }
            }*/

            var tenantScopes = TenantAccessProxy.GetAllTenantScope();

            if (tenantScopes != null)
                ViewBag.TenantScopes = new SelectList(tenantScopes.Values.Select(x => new { Text = x.Name, Value = x.Id }), "Value", "Text");
            else
                ViewBag.TenantScopes = new SelectList(new List<object>() { new { Value = "Self", Text = "Self" } }, "Value", "Text");

            var sourceConnList = new List<SelectListItem>();
            foreach (ConnectionStringSettings conn in ConfigurationManager.ConnectionStrings)
            {
                if (conn.Name.StartsWith("Local", StringComparison.OrdinalIgnoreCase)) continue;
                sourceConnList.Add(new SelectListItem
                {
                    Text = conn.Name,
                    Value = conn.Name
                });
            }

            ViewBag.Sources = sourceConnList.OrderBy((x) => x.Text);
        }

        /// <summary>
        /// This method is used to manages the backup request.
        /// </summary>
        /// <param name="request">request.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageBackupRequest(BackupRequest request)
        {
            PopulateTenantAndModeDropdowns();

            try
            {
                ValidateBackupRequest(request);

                if (request.DestinationType == BackupDestinationType.Database)
                {
                    var dbdest = new DBBackupDestination();
                    TryUpdateModel(dbdest);
                    request.DestinationDetails = dbdest;
                    ValidateDatabaseDestinationDetails(dbdest);
                }
                else if (request.DestinationType == BackupDestinationType.File)
                {
                    var filedest = new FileBackupDestination();
                    TryUpdateModel(filedest);
                    request.DestinationDetails = filedest;
                    ValidateFileDestinationDetails(filedest);
                }

                var tenantIds = new List<string>() { request.PrimaryTenantId };

                if (!string.IsNullOrEmpty(this.Request.Form["TenantScopes"]))
                {
                    var tenantScopeId = this.Request.Form["TenantScopes"];
                    Dictionary<string, TenantDetails> tenantDetails = null;

                    if (tenantScopeId == TenantScopeConstant.CHILD_TENANT_SCOPE)
                    {
                        tenantDetails = TenantRelationProxy.GetAllChildTenants(request.PrimaryTenantId);
                    }
                    else if (tenantScopeId == TenantScopeConstant.IMMEDIATE_CHILD_TENANT_SCOPE)
                    {
                        tenantDetails = TenantRelationProxy.GetAllImmediateChildTenants(request.PrimaryTenantId);
                    }
                    else if (tenantScopeId == TenantScopeConstant.SELECTIVE_TENANT_SCOPE)
                    {
                        if (!string.IsNullOrEmpty(this.Request.Form["SelectedTenantIds"]))
                        {
                            var selectedTenantIds = this.Request.Form["SelectedTenantIds"].Split(',');
                            tenantDetails = TenantProxy.GetTenantDetailsForShareUsers(selectedTenantIds);
                        }
                        else
                        {
                            ModelState.AddModelError("TenantScopes", Resources.DataBackupResource.e_SelectOneTenant);
                        }
                    }

                    if (tenantDetails != null && tenantDetails.Count > 0)
                        tenantIds.AddRange(tenantDetails.Select(x => x.Key));
                }

                if (ModelState.IsValid)
                {
                    request.Source = CelloSaaS.Library.DataAccessLayer.DbMetaData.GetConnectionString(request.Source, request.PrimaryTenantId).ConnectionString;
                    request.TenantIds = tenantIds.ToArray();
                    request.BackupStatus = BackupStatus.New;
                    request.RequestBy = UserIdentity.UserId;

                    BackupRequestProxy.CreateBackupRequest(request);
                    TempData["Success"] = Resources.DataBackupResource.s_CreateBackupRequest;
                    return RedirectToAction("Index");
                }
            }
            catch (BackupRequestException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(request);
        }

        /// <summary>
        /// This method is used to validates the database destination details.
        /// </summary>
        /// <param name="dbdest">dbdest.</param>
        private void ValidateDatabaseDestinationDetails(DBBackupDestination dbdest)
        {
            if (string.IsNullOrEmpty(dbdest.ServerName))
            {
                ModelState.AddModelError("ServerName", Resources.DataBackupResource.e_ServerAddress);
            }

            if (string.IsNullOrEmpty(dbdest.UserName))
            {
                ModelState.AddModelError("UserName", Resources.DataBackupResource.e_ValidUserName);
            }

            if (string.IsNullOrEmpty(dbdest.Password))
            {
                ModelState.AddModelError("Password", Resources.DataBackupResource.e_ValidPassword);
            }

            if (string.IsNullOrEmpty(dbdest.DBName))
            {
                ModelState.AddModelError("DBName", Resources.DataBackupResource.e_ValidDatabase);
            }
        }

        /// <summary>
        /// This method is used to validates the FTP destination details.
        /// </summary>
        /// <param name="ftpDetails">FTP details.</param>
        private void ValidateFTPDestinationDetails(FTPDetails ftpDetails)
        {
            if (ftpDetails == null)
            {
                ModelState.AddModelError("Error", Resources.DataBackupResource.e_ValidFtp);
                return;
            }

            if (string.IsNullOrEmpty(ftpDetails.Path))
            {
                ModelState.AddModelError("FtpDetails.Path", Resources.DataBackupResource.e_ValidFtpPath);
            }

            if (string.IsNullOrEmpty(ftpDetails.ServerName))
            {
                ModelState.AddModelError("FtpDetails.ServerName", Resources.DataBackupResource.e_ValidFtpServer);
            }

            if (string.IsNullOrEmpty(ftpDetails.UserName))
            {
                ModelState.AddModelError("FtpDetails.UserName", Resources.DataBackupResource.e_ValidFtpUserName);
            }

            if (string.IsNullOrEmpty(ftpDetails.Password))
            {
                ModelState.AddModelError("FtpDetails.Password", Resources.DataBackupResource.e_ValidFtpPassword);
            }

            if (ftpDetails.PortNo <= 0 || ftpDetails.PortNo > 65535)
            {
                ModelState.AddModelError("FtpDetails.PortNo", Resources.DataBackupResource.e_ValidPortNumber);
            }
        }

        /// <summary>
        /// This method is used to validates the file destination details.
        /// </summary>
        /// <param name="filedest">filedest.</param>
        private void ValidateFileDestinationDetails(FileBackupDestination filedest)
        {
            if (string.IsNullOrEmpty(filedest.FilePath))
            {
                ModelState.AddModelError("FilePath", Resources.DataBackupResource.e_ValidFilePath);
            }

            ModelState.Remove("FtpDetails.PortNo");

            if (filedest.UploadToFtp)
            {
                ValidateFTPDestinationDetails(filedest.FtpDetails);
            }
        }

        /// <summary>
        /// This method is used to validates the backup request.
        /// </summary>
        /// <param name="request">request.</param>
        private void ValidateBackupRequest(BackupRequest request)
        {
            if (string.IsNullOrEmpty(request.PrimaryTenantId))
            {
                ModelState.AddModelError("PrimaryTenantId", Resources.DataBackupResource.e_ChoosePrimaryTenant);
            }

            if (string.IsNullOrEmpty(request.Source) || ConfigurationManager.ConnectionStrings[request.Source] == null)
            {
                ModelState.AddModelError("Source", Resources.DataBackupResource.e_ValidDataSource);
            }
        }

        /// <summary>
        /// This method is used to gets the child tenants.
        /// </summary>
        /// <param name="primaryTenantId">primary tenant identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetChildTenants(string primaryTenantId)
        {
            if (!string.IsNullOrEmpty(primaryTenantId))
            {
                var tenantDetails = TenantRelationProxy.GetAllChildTenants(primaryTenantId);
                List<TenantDetails> childTenants = null;

                if (tenantDetails != null && tenantDetails.Count() > 0)
                {
                    var stratifiedTenants = TenantProxy.GetStratifiedTenantsByUser(UserIdentity.UserId, UserIdentity.TenantID);

                    if (stratifiedTenants != null && stratifiedTenants.Count() > 0)
                    {
                        childTenants = new List<TenantDetails>();

                        foreach (var childTenant in tenantDetails.Values)
                        {
                            if (stratifiedTenants.ContainsKey(childTenant.TenantCode))
                            {
                                childTenants.Add(childTenant);
                            }
                        }
                    }
                }
                return childTenants != null && childTenants.Count() > 0 ? Json(childTenants.OrderBy(x => x.TenantName).Select(x => new { Id = x.TenantCode, Name = x.TenantName })) : null;
            }
            return null;
        }

        /// <summary>
        /// This method is used to gets the destination details form.
        /// </summary>
        /// <param name="destinationType">type of the destination.</param>
        /// <returns></returns>
        public ActionResult GetDestinationDetailsForm(BackupDestinationType destinationType)
        {
            IBackupDestination backupDest = null;
            if (destinationType == BackupDestinationType.Database)
            {
                backupDest = new DBBackupDestination();
            }
            else if (destinationType == BackupDestinationType.File)
            {
                backupDest = new FileBackupDestination
                {
                    FilePath = CelloSaaS.Library.Helpers.ConfigHelper.DataBackupPath,
                    FtpDetails = new FTPDetails
                    {
                        PortNo = 21
                    }
                };
            }

            return PartialView(destinationType.ToString() + "Form", backupDest);
        }

        /// <summary>
        /// This method is used to cancels the data backup request.
        /// </summary>
        /// <param name="requestId">request identifier.</param>
        /// <param name="condition">condition.</param>
        /// <returns></returns>
        public ActionResult CancelRequest(string requestId, BackupRequestSearchCondition condition)
        {
            if (string.IsNullOrEmpty(requestId))
            {
                TempData["Error"] = Resources.DataBackupResource.e_RequestIdNull;
            }

            try
            {
                if (BackupRequestProxy.CancelBackupRequest(requestId))
                {
                    TempData["Success"] = Resources.DataBackupResource.s_CancelRequest;
                }
                else
                {
                    TempData["Error"] = Resources.DataBackupResource.e_CancelRequest;
                }
            }
            catch (BackupRequestException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("Index", new
            {
                page = condition.PageNo,
                pageSize = condition.PageSize,
                BackupStatus = condition.BackupStatus,
                BackupMode = condition.BackupMode,
                BackupDestinationType = condition.BackupDestinationType,
                FromRequestDate = condition.FromRequestDate,
                ToRequestDate = condition.ToRequestDate,
                PrimaryTenantId = condition.PrimaryTenantId
            });
        }
    }
}
