namespace CelloSaaSApplication.Controllers
{
    using Resources;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;
    using System.Web.Mvc;
    using CelloSaaS.Library;
    using CelloSaaS.Model.AccessControlManagement;
    using CelloSaaS.Model.Configuration;
    using CelloSaaS.Model.DataManagement;
    using CelloSaaS.ServiceContracts.AccessControlManagement;
    using CelloSaaS.ServiceContracts.DataManagement;
    using CelloSaaS.ServiceContracts.LicenseManagement;
    using CelloSaaS.ServiceProxies.AccessControlManagement;
    using CelloSaaS.ServiceProxies.Configuration;
    using CelloSaaS.ServiceProxies.DataManagement;
    using System.Globalization;
    using CelloSaaS.Rules.Core;
    using CelloSaaS.Services.Configuration;
    using System.Data;
    using CelloSaaS.Model.EntityExtnManagement;
    using CelloSaaS.Rules.ServiceProxies;
    using CelloSaaS.ServiceProxies.LicenseManagement;
    using CelloSaaS.Model.LicenseManagement;
    using CelloSaaS.ServiceContracts.VirtualEntityManagement;
    using CelloSaaS.Model.VirtualEntityManagement;
    using CelloSaaS.ServiceProxies.VirtualEntityManagement;
    using CelloSaaS.ServiceContracts.ViewManagement;
    using CelloSaaS.Rules.ServiceContracts;
    using System.Transactions;
    using CelloSaaS.Rules.Core.Model;

    /// <summary>
    /// This class is responsible for data management.
    /// </summary>
    [HandleError]
    public class DataController : CelloSaaS.View.CelloController
    {
        private string TenantId = TenantContext.GetTenantId("_Entity");

        private const string DefaultPolicy = "GlobalExceptionLogger";

        #region Private members

        private const string PolicyName = "GlobalExceptionLogger";
        private string statusMessage = string.Empty;
        private const string ViewFieldPrefix = "ViewField_";
        private const string EditFieldPrefix = "EditField_";
        private const string ViewEntityPrefix = "View_";
        private const string EditEntityPrefix = "Edit_";

        /// <summary>
        /// Fills the features.
        /// </summary>
        /// <param name="tenantLicense">The tenant license.</param>
        private void FillFeatures(TenantLicense tenantLicense)
        {
            List<SelectListItem> lstFeature = new List<SelectListItem>();

            lstFeature.Add(new SelectListItem { Value = "-1", Text = "- Select -" });

            foreach (Feature feature in tenantLicense.LicenseFeatureDetails.Values)
            {
                if (ViewData["FeatureList"] != null && !string.IsNullOrEmpty(ViewData["FeatureList"].ToString()) && ViewData["FeatureList"].ToString().Equals(feature.FeatureCode))
                {
                    lstFeature.Add(new SelectListItem { Text = feature.FeatureCode, Value = feature.FeatureCode, Selected = true });
                }
                else
                {
                    lstFeature.Add(new SelectListItem { Text = feature.FeatureCode, Value = feature.FeatureCode });
                }
            }
            ViewData["FeatureList"] = lstFeature;
        }

        /// <summary>
        /// This method is used to get all field datatype and bind to dropdown list
        /// </summary>
        /// <param name="selectedValue">The selected value.</param>        
        private void PopulateTypeList(string selectedValue)
        {
            Dictionary<string, FieldDataType> dicFieldDataType = null;
            try
            {
                dicFieldDataType = DataManagementProxy.GetAllEntityFieldType();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
            }
            //Set type list to DataType dropdown
            if (dicFieldDataType != null && dicFieldDataType.Count > 0)
            {
                ViewData["TypeID"] = new SelectList((IEnumerable)dicFieldDataType.Values.ToList(), "TypeIdentifier", "TypeName", selectedValue);
            }
            else
            {
                ViewData["TypeID"] = new SelectList((IEnumerable)new Dictionary<string, FieldDataType>().Values.ToList(), "TypeIdentifier", "TypeName");
            }
        }

        ///<summary>
        ///Populate the entity details.
        ///</summary>
        private void PopulateEntityDetails(string referenceEntityId)
        {
            try
            {
                //Get all entity meta data
                var dicEntityMetaData = DataManagementProxy.GetAllEntityMetaData();
                List<SelectListItem> lstEntity = new List<SelectListItem>();
                lstEntity.Add(new SelectListItem { Value = "", Text = "-Select Entity List-" });
                if (dicEntityMetaData != null && dicEntityMetaData.Count > 0)
                {
                    foreach (EntityMetaData metaData in dicEntityMetaData.Values)
                    {
                        if (!string.IsNullOrEmpty(metaData.TenantIdColumnName))
                        {
                            if (metaData.EntityIdentifier.Equals(referenceEntityId))
                            {
                                lstEntity.Add(new SelectListItem { Text = metaData.EntityName, Value = metaData.EntityIdentifier, Selected = true });
                            }
                            else
                            {
                                lstEntity.Add(new SelectListItem { Text = metaData.EntityName, Value = metaData.EntityIdentifier });
                            }
                        }
                    }
                }
                ViewData["ReferenceEntityId"] = lstEntity;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException viewMetaDataException)
            {
                ExceptionService.HandleException(viewMetaDataException, DefaultPolicy);
                ModelState.AddModelError("Error", viewMetaDataException.Message);
            }
        }

        /// <summary>
        /// This method is used to get pickup list and bind to pickuplist dropdown
        /// </summary>
        /// <param name="selectedValue">The selected value.</param>        
        private void PopulatePickUpList(string selectedValue)
        {
            List<PickupList> pickUpList = null;
            //Create empty Id list to show "Select pickup list"
            PickupList pickUp = new PickupList { Id = string.Empty, Name = "--Select PickUp List--" };

            try
            {
                //Get the pickup list for logged in tenant
                pickUpList = PickupListProxy.GetPickupLists(this.TenantId);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
            }
            catch (ArgumentNullException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
            }


            if (pickUpList == null)
            {
                pickUpList = new List<PickupList>();
            }

            //Add empt Id list to pickuplist at the index 0 
            pickUpList.Insert(0, pickUp);
            ViewData["PickupListId"] = new SelectList((IEnumerable)pickUpList, "Id", "Name", selectedValue);
        }

        /// <summary>
        /// This method is used to validate Entity field Identifier and field name
        /// </summary>
        /// <param name="entityFieldMetaData">The entity field meta data.</param>
        private void ValidateFields(EntityFieldMetaData entityFieldMetaData)
        {
            if (string.IsNullOrEmpty(entityFieldMetaData.Name))
            {
                ModelState.AddModelError("Name", DataResource.e_fieldName_required);
            }

            if (string.IsNullOrEmpty(entityFieldMetaData.TypeID))
            {
                ModelState.AddModelError("TypeID", AccountResource.Mandatory);
            }

            if (entityFieldMetaData.TypeID == DataFieldType.PickUpField
                && string.IsNullOrEmpty(entityFieldMetaData.PickupListId))
            {
                this.ModelState.AddModelError("PickupListId", DataResource.e_pickupfield_required);
            }

            if (entityFieldMetaData.TypeID == DataFieldType.Entity
                && (string.IsNullOrEmpty(entityFieldMetaData.ReferenceEntityId) || entityFieldMetaData.ReferenceEntityId == "-1"))
            {
                this.ModelState.AddModelError("ReferenceEntityId", DataResource.e_pickupfield_required);
            }
        }

        /// <summary>
        /// This method is used when the scope is null remove the old Field Privilege for the role
        /// </summary>
        /// <param name="roleId">Role Name</param>
        /// <param name="Privilege">Privilege Name</param>
        private void DeleteFieldPrivilege(string roleId, string Privilege)
        {
            Dictionary<string, RolePrivilege> dictRolePrivilege = new Dictionary<string, RolePrivilege>();
            string[] RoleArray = new string[1];
            RoleArray[0] = roleId;
            try
            {
                dictRolePrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, RoleArray, Privilege);
            }
            //Catch DB Exception and throw as Privilege Exception
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, PolicyName);
            }
            //Catch License Exception and throw as Privilege Exception
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, PolicyName);
            }
            //Catch Argument Exception and throw as Privilege Exception
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }
            if (dictRolePrivilege.Keys.Count <= 0)
            {
            }
            else
            {
                string[] dictRolePrivilegeArray = new string[1];
                dictRolePrivilegeArray[0] = Privilege;
                try
                {
                    PrivilegeProxy.DeletePrivilegesForRole(this.TenantId, roleId, dictRolePrivilegeArray);
                }
                //Catch DB Exception and throw as Privilege Exception
                catch (DbException dbException)
                {
                    ExceptionService.HandleException(dbException, PolicyName);
                }
            }
        }

        /// <summary>
        /// This method is used to get the available datascopes for the given entity.
        /// </summary>
        /// <param name="entityId">entityId</param>
        private void GetAllDataScopeForEntity(string entityId)
        {
            Dictionary<string, DataScope> dictDataScopes = new Dictionary<string, DataScope>();
            string[] strEntities = new string[1];
            strEntities[0] = entityId;
            //DataScope for each entities
            Dictionary<string, Dictionary<string, DataScope>> dictEntityDataScope = new Dictionary<string, Dictionary<string, DataScope>>();
            dictEntityDataScope = DataAccessProxy.GetAllDataScopeBasedonEntity(strEntities);
            if (dictEntityDataScope.Keys.Count > 0)
            { dictDataScopes = dictEntityDataScope[entityId]; }
            List<DataScope> lstDataScope = new List<DataScope>();
            foreach (DataScope dataScope in dictDataScopes.Values)
            {
                lstDataScope.Add(dataScope);
            }
            lstDataScope.Insert(0, new DataScope { Id = "-1", Name = "-All-" });
            ViewData["DataScope"] = lstDataScope;
        }

        #endregion

        #region Public members

        /// <summary>
        /// This is method is used to get form method for ManageField view.
        /// </summary>
        /// <param name="fieldId">The field identifier.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        public ActionResult ManageField(string fieldId, string entityId)
        {
            //Set the passed entityId and fieldId in view
            ViewData["EntityId"] = entityId;
            ViewData["fieldId"] = fieldId;
            EntityFieldMetaData entityFieldMetaData = null;

            try
            {
                if (string.IsNullOrEmpty(entityId))
                {
                    return HttpNotFound("Invalid request!");
                }

                var entityMetaData = DataManagementProxy.GetEntityMetaData(entityId, this.TenantId);

                if (entityMetaData == null)
                {
                    return HttpNotFound("Invalid request!");
                }

                ViewData["EntityName"] = entityMetaData.EntityName;

                //Get entity field meta data for passed entityId,fieldId and logged in tenant
                if (!string.IsNullOrEmpty(fieldId))
                {
                    var extnSettingValue = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.TenantId, ModuleConfigurationConstant.EXTENDED_FIELD);

                    if (extnSettingValue == ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING)
                    {
                        entityFieldMetaData = DataManagementProxy.GetOwnEntityFieldMetaData(entityId, this.TenantId, fieldId);
                    }
                    else
                    {
                        entityFieldMetaData = DataManagementProxy.GetEntityFieldMetaData(entityId, this.TenantId, fieldId);
                    }

                }

                if (entityFieldMetaData != null)
                {
                    //Fill the dropdown list with saved Ids as selected values
                    this.PopulateTypeList(entityFieldMetaData.TypeID);
                    this.PopulatePickUpList(entityFieldMetaData.PickupListId);
                    this.PopulateEntityDetails(entityFieldMetaData.ReferenceEntityId);
                }
                else
                {
                    entityFieldMetaData = new EntityFieldMetaData();
                    entityFieldMetaData.entityExtnColumnDetails = new EntityExtendedColumnDetails();
                    // Fill the dropdown list with empty Id as selected values
                    this.PopulateTypeList(string.Empty);
                    this.PopulatePickUpList(string.Empty);
                    this.PopulateEntityDetails(string.Empty);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_datamanagement_getting_field_metadata);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("AccessError", Resources.DataResource.e_AccessDenied);
            }

            return View(entityFieldMetaData);
        }

        /// <summary>
        /// This method is used to POST form method for manage field view.
        /// and  add or update entity field metadata.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManageField(FormCollection formCollection)
        {
            //Get entityId and fieldId from hidden fields in formcollection
            string entityId = formCollection["EntityId"];
            string fieldId = formCollection["FieldId"];

            //Set entityId and fieldId to hidden field
            ViewData["EntityId"] = entityId;
            ViewData["fieldId"] = fieldId;

            statusMessage = string.Empty;
            EntityFieldMetaData entityFieldMetaData = new EntityFieldMetaData();
            //Update model with entity field metadata
            TryUpdateModel(entityFieldMetaData);

            //Validate the mandatory fields
            this.ValidateFields(entityFieldMetaData);

            try
            {
                if (string.IsNullOrEmpty(entityId))
                {
                    return HttpNotFound("Invalid request!");
                }

                var entityMetaData = DataManagementProxy.GetEntityMetaData(entityId, this.TenantId);

                if (entityMetaData == null)
                {
                    return HttpNotFound("Invalid request!");
                }

                ViewData["EntityName"] = entityMetaData.EntityName;

                if (!string.IsNullOrEmpty(entityFieldMetaData.Name) && !Util.ValidateIdentifierWithSpace(entityFieldMetaData.Name))
                {
                    ModelState.AddModelError("Name", Resources.DataResource.e_SpecialCharacter);
                }

                if (ModelState.IsValid)
                {
                    entityFieldMetaData.TenantId = this.TenantId;
                    //Save entered entity field metadata with passed entityId and logged in tenantId
                    bool savedStatus = DataManagementProxy.SaveEntityFieldMetaData(entityId, this.TenantId, entityFieldMetaData);

                    if (!savedStatus)
                    {
                        statusMessage = Resources.DataResource.e_saved;
                    }
                    else
                    {
                        //If saved successfuly,then redirect to FieldList                            
                        TempData["Success"] = Resources.DataResource.s_saved_successfully;
                        return RedirectToAction("FieldList", new { entityId = entityId });
                    }
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                statusMessage = Resources.DataResource.e_NullParms;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                statusMessage = Resources.DataResource.e_NullParms;
            }
            catch (InvalidDataTypeConversionException invalidDataTypeConversionException)
            {
                ExceptionService.HandleException(invalidDataTypeConversionException, PolicyName);
                statusMessage = Resources.DataResource.e_invalid_conversion;
            }
            catch (AlreadyAssignedException alreadyAssignedException)
            {
                ExceptionService.HandleException(alreadyAssignedException, PolicyName);
                statusMessage = Resources.DataResource.e_already_assigned;
            }
            catch (MaximumLimitException maximumLimitException)
            {
                ExceptionService.HandleException(maximumLimitException, PolicyName);
                statusMessage = Resources.DataResource.e_maximum_limit;
            }
            catch (FieldNameUniquenessException fieldNameUniquenessException)
            {
                ExceptionService.HandleException(fieldNameUniquenessException, PolicyName);
                statusMessage = Resources.DataResource.e_fieldname_duplicate;
            }
            catch (InvalidFieldTypeException invalidFieldTypeException)
            {
                ExceptionService.HandleException(invalidFieldTypeException, PolicyName);
                statusMessage = Resources.DataResource.e_invalide_datatype;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                statusMessage = Resources.DataResource.e_datamanagement_saving_field_metadata;
            }
            catch (InvalidOperationException invalidOperationException)
            {
                ExceptionService.HandleException(invalidOperationException, PolicyName);
                this.ValidateFields(entityFieldMetaData);
            }

            if (!string.IsNullOrEmpty(statusMessage))
            {
                ModelState.AddModelError("StatusMessage", statusMessage);
            }

            //Fill the dropdown list with saved Ids as selected values
            this.PopulateTypeList(entityFieldMetaData.TypeID);
            this.PopulatePickUpList(entityFieldMetaData.PickupListId);
            this.PopulateEntityDetails(entityFieldMetaData.ReferenceEntityId);
            return View(entityFieldMetaData);
        }

        /// <summary>
        /// This is used to GET form method for EntityList view.
        /// This is used to fetch all entities.
        /// </summary>
        /// <returns></returns>
        public ActionResult EntityList()
        {
            if (TempData["argumentNullException"] != null && !string.IsNullOrEmpty(TempData["argumentNullException"].ToString()))
            {
                ModelState.AddModelError("StatusMessage", TempData["argumentNullException"].ToString());
            }
            if (TempData["argumentException"] != null && !string.IsNullOrEmpty(TempData["argumentException"].ToString()))
            {
                ModelState.AddModelError("StatusMessage", TempData["argumentException"].ToString());
            }
            if (TempData["virtualEntityException"] != null && !string.IsNullOrEmpty(TempData["virtualEntityException"].ToString()))
            {
                ModelState.AddModelError("StatusMessage", TempData["virtualEntityException"].ToString());
            }
            try
            {
                var extnSettingValue = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.TenantId, ModuleConfigurationConstant.EXTENDED_FIELD);
                int count = 0;
                ViewData["Source"] = extnSettingValue;
                ViewData["TenantId"] = this.TenantId;
                ViewData["HasExtendedFields"] = "true";


                //Get all entity meta data
                var dicEntityMetaData = DataManagementProxy.GetAllEntityMetaData();
                if (dicEntityMetaData != null && dicEntityMetaData.Count > 0)
                {
                    ViewData["EntityMetaData"] = dicEntityMetaData.Values.ToList();

                    var entityIds = dicEntityMetaData.Values.Select(x => x.EntityIdentifier).ToArray();

                    ViewBag.EntityValidationRules = RuleSetMetadataProxy.CheckEntityHasRule(entityIds, EntityRuleType.Validation);
                    ViewBag.EntityPreprocessRules = RuleSetMetadataProxy.CheckEntityHasRule(entityIds, EntityRuleType.Preprocessor);
                }

                Dictionary<string, int> ExtendedFields = DataManagementProxy.GetExtendedFieldsByTenantId(this.TenantId);

                if (ExtendedFields != null && ExtendedFields.Count() > 0)
                {
                    foreach (KeyValuePair<string, int> extendedFieldsValues in ExtendedFields)
                    {
                        if (extendedFieldsValues.Value > 0)
                        {
                            count = count + 1;
                        }
                    }
                    if (count == 0 || count < dicEntityMetaData.Count)
                    {
                        ViewData["HasExtendedFields"] = "false";
                    }
                    else
                    {
                        ViewData["HasExtendedFields"] = "true";
                    }
                }
                else
                {
                    ViewData["HasExtendedFields"] = "false";
                }

            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_datamanagement_getting_extendedfieldlist);
            }
            return View();
        }

        /// <summary>
        /// This method is used to add display column name.
        /// </summary>
        /// <param name="formCollection">form collection</param>
        /// <returns></returns>
        public JsonResult AddDisplayColumnName(FormCollection formCollection)
        {
            string error = string.Empty;
            if (formCollection["entityId"] != null && string.IsNullOrEmpty(formCollection["entityId"]))
            {
                return Json(new { Error = Resources.DataResource.e_NullEntity });
            }
            if (formCollection["DisplayColumnName"] != null && string.IsNullOrEmpty(formCollection["DisplayColumnName"]))
            {
                if (formCollection["DisplayColumnName"] == "-1")
                {
                    return Json(new { Error = Resources.DataResource.e_SelectDisplayColumnName });
                }
                return Json(new { Error = Resources.DataResource.e_NullDisplayColumnName });
            }
            try
            {
                if (DataManagementProxy.UpdateDisplayColumName(formCollection["entityId"], formCollection["DisplayColumnName"], this.TenantId))
                {
                    return Json(new { Success = Resources.DataResource.s_DisplayName_Saved_Successfully });
                }
                else
                {
                    return Json(new { Success = Resources.DataResource.e_DisplayName_Saved_failure });
                }
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, PolicyName);
                error = exception.Message;
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, PolicyName);
                error = exception.Message;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                error = dataManagementException.Message;
            }
            return Json(new { Error = error });
        }

        /// <summary>
        /// This is used to  GET form method for FieldList view.
        /// This is used to fetch all extn field metadata for selected entity identifier.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public ActionResult FieldList(string entityId)
        {
            try
            {
                if (string.IsNullOrEmpty(entityId))
                {
                    return HttpNotFound("Invalid request!");
                }

                var entityMetaData = DataManagementProxy.GetEntityMetaData(entityId, this.TenantId);

                if (entityMetaData == null)
                {
                    return HttpNotFound("Invalid request!");
                }

                ViewData["EntityName"] = entityMetaData.EntityName;

                TempData["CanUseDisplayColumnName"] = false;
                List<SelectListItem> lstFieldMetaData = new List<SelectListItem>();
                lstFieldMetaData.Add(new SelectListItem { Value = "-1", Text = "- Select -" });

                if (TempData["Success"] != null && !string.IsNullOrEmpty(TempData["Success"].ToString()))
                {
                    ModelState.AddModelError("SuccessMessage", TempData["Success"].ToString());
                }
                var extnSettingValue = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.TenantId, ModuleConfigurationConstant.EXTENDED_FIELD);

                ViewData["Source"] = extnSettingValue;
                ViewData["TenantId"] = this.TenantId;

                if (this.TenantId.ToUpperInvariant() == (ProductAdminConstants.ProductAdminTenantId).ToUpperInvariant())
                {
                    ViewData["CheckExtendedFields"] = "true";
                }
                else
                {
                    if (DataManagementProxy.CheckExtendedFieldsByTenantIdAndEntity(this.TenantId, entityId))
                    {
                        ViewData["CheckExtendedFields"] = "true";
                    }
                    else
                    {
                        ViewData["CheckExtendedFields"] = "false";
                    }
                }

                if (extnSettingValue == ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING)
                {
                    // get own exentended fieds
                    var ownEntityFieldMetaData = DataManagementProxy.GetOwnExtendedFields(entityId, this.TenantId);

                    if (ownEntityFieldMetaData != null && ownEntityFieldMetaData.Count > 0)
                    {
                        ViewData["OwnEntityFieldMetaData"] = ownEntityFieldMetaData.Values.ToList();
                    }
                }

                //Get all fields for passed entityId and logged in tenant Id
                var extendedFields = DataManagementProxy.GetExtendedFields(entityId, this.TenantId);

                if (extendedFields != null && extendedFields.Count > 0)
                {
                    ViewData["DicEntityFieldMetaData"] = extendedFields.Values.ToList();

                    if (entityMetaData != null)
                    {
                        if (!string.IsNullOrEmpty(entityMetaData.TenantId))
                        {
                            TempData["CanUseDisplayColumnName"] = true;
                        }
                        if (string.IsNullOrEmpty(entityMetaData.DisplayColumnName))
                        {
                            foreach (KeyValuePair<string, EntityFieldMetaData> entityFieldMetaData in extendedFields)
                            {
                                lstFieldMetaData.Add(new SelectListItem { Text = entityFieldMetaData.Value.Name, Value = entityFieldMetaData.Value.EntityFieldIdentifier });
                            }
                        }
                        else
                        {
                            foreach (KeyValuePair<string, EntityFieldMetaData> entityFieldMetaData in extendedFields)
                            {
                                if (entityFieldMetaData.Value.entityExtnColumnDetails.ColumnName != null && entityFieldMetaData.Value.entityExtnColumnDetails.ColumnName.Equals(entityMetaData.DisplayColumnName))
                                {
                                    lstFieldMetaData.Add(new SelectListItem { Text = entityFieldMetaData.Value.Name, Value = entityFieldMetaData.Value.EntityFieldIdentifier, Selected = true });
                                }
                                else
                                {
                                    lstFieldMetaData.Add(new SelectListItem { Text = entityFieldMetaData.Value.Name, Value = entityFieldMetaData.Value.EntityFieldIdentifier });
                                }
                            }
                        }
                    }
                }
                ViewData["FieldMetaData"] = lstFieldMetaData;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_datamanagement_getting_fieldlist);
            }

            return View();
        }

        /// <summary>
        /// This is used to GET form method for FieldList view.
        /// This is used to fetch all extn field metadata for selected entity identifier.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public ActionResult BaseFieldList(string entityId)
        {
            Dictionary<string, EntityFieldMetaData> dicEntityFieldMetaData = new Dictionary<string, EntityFieldMetaData>();
            try
            {
                var entityMetaData = DataManagementProxy.GetEntityMetaData(entityId, this.TenantId);

                if (entityMetaData == null)
                {
                    return HttpNotFound("Invalid request!");
                }
                ViewData["EntityName"] = entityMetaData.EntityName;

                //Get all base field for given entityId
                dicEntityFieldMetaData = DataManagementProxy.GetBaseFields(entityId);
                if (dicEntityFieldMetaData != null && dicEntityFieldMetaData.Count > 0)
                {
                    ViewData["DicEntityFieldMetaData"] = dicEntityFieldMetaData.Values.ToList();
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_datamanagement_getting_basefield);
            }
            return View();
        }

        /// <summary>
        /// Adds the virtual entity.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddVirtualEntity()
        {
            if (ViewData["EntityName"] == null || string.IsNullOrEmpty(ViewData["EntityName"].ToString()))
            {
                ViewData["EntityName"] = string.Empty;
            }
            if (ViewData["FeatureList"] == null || string.IsNullOrEmpty(ViewData["FeatureList"].ToString()))
            {
                ViewData["FeatureList"] = string.Empty;
            }
            try
            {
                TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);
                FillFeatures(tenantLicense);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("Error", licenseException.Message);
            }

            return View();
        }

        /// <summary>
        /// Adds the virtual entity.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddVirtualEntity(FormCollection formCollection)
        {
            if (formCollection.Count > 0 && formCollection != null)
            {
                if (string.IsNullOrEmpty(formCollection["EntityName"]))
                {
                    ModelState.AddModelError("Error", Resources.DataResource.e_entityName_required);
                }
                else
                {
                    ViewData["EntityName"] = formCollection["EntityName"].ToString();
                    if (!Util.ValidateIdentifier(formCollection["EntityName"].ToString()))
                    {
                        ModelState.AddModelError("Error", Resources.DataResource.e_SpecialCharacterEntity);
                    }
                }
                if (string.IsNullOrEmpty(formCollection["Features"]))
                {
                    ModelState.AddModelError("Error", Resources.DataResource.e_FeatureName);
                }
                else if (formCollection["Features"] == "-1")
                {
                    ModelState.AddModelError("Error", Resources.DataResource.e_SelectFeature);
                }
                else
                {
                    ViewData["FeatureList"] = formCollection["Features"].ToString();
                }

                try
                {
                    TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);
                    FillFeatures(tenantLicense);
                    if (ModelState.IsValid)
                    {
                        EntityMetaData entityMetaData = new EntityMetaData();
                        entityMetaData.EntityIdentifier = formCollection["EntityName"].ToString();
                        entityMetaData.TenantId = this.TenantId;
                        using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                        {
                            VirtualEntityProxy.CreateVirtualEntity(entityMetaData, formCollection["Features"]);
                            AddBusinessRulesToVirtualEntity(entityMetaData.EntityIdentifier, this.TenantId, UserIdentity.UserId);
                            transactionScope.Complete();
                        }
                        TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.s_Entity_Saved_Successfully, formCollection["EntityName"].ToString()); ;
                        return RedirectToAction("EntityList");
                    }
                }
                catch (BusinessRuleException businessRuleException)
                {
                    ExceptionService.HandleException(businessRuleException, PolicyName);
                    ModelState.AddModelError("Error", businessRuleException.Message);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, PolicyName);
                    ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, PolicyName);
                    ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
                }
                catch (LicenseException licenseException)
                {
                    ExceptionService.HandleException(licenseException, DefaultPolicy);
                    ModelState.AddModelError("Error", licenseException.Message);
                }
                catch (VirtualEntityException virtualEntityException)
                {
                    ExceptionService.HandleException(virtualEntityException, PolicyName);
                    ModelState.AddModelError("Error", virtualEntityException.Message);
                }
                catch (UnauthorizedAccessException unauthorizedAccessExcepion)
                {
                    ExceptionService.HandleException(unauthorizedAccessExcepion, PolicyName);
                    ModelState.AddModelError("Error", unauthorizedAccessExcepion.Message);
        
                }
            }
            return View();
        }

        /// <summary>
        /// This method used to add the business rule to virtual entity.
        /// </summary>
        /// <param name="entityIdentifier">entity identifier</param>
        /// <param name="tenantId">tenant identifier</param>
        /// <param name="userId">user identifier</param>
        private void AddBusinessRulesToVirtualEntity(string entityIdentifier, string tenantId, string userId)
        {
            //Validation Rule
            string validationRule = RuleSetMetadataHelper.GetValidationRuleCode(entityIdentifier);
            string validationRuleName = RuleSetMetadataHelper.GetValidationRuleName(entityIdentifier);

            //Pre-Processor  Rule
            string preProcessorRule = RuleSetMetadataHelper.GetPreProcessorRuleCode(entityIdentifier);
            string preProcessorRuleName = RuleSetMetadataHelper.GetPreProcessorRuleName(entityIdentifier);


            RuleSetMetadata rsMetadata = new RuleSetMetadata();
            rsMetadata.Code = validationRule;
            rsMetadata.TenantId = tenantId;
            rsMetadata.Name = validationRuleName;
            rsMetadata.CreatedBy = userId;
            rsMetadata.ScreenType = ScreenType.ConditionAction;
            rsMetadata.Category = "VirtualEntity Validation Rule";

            //Create validation rule set for virtual entity
            var ruleMetadatService = (IRuleSetMetadataService)ServiceLocator.GetServiceImplementation(typeof(IRuleSetMetadataService));
            string rsValidationId = ruleMetadatService.CreateRuleSetMetadata(rsMetadata);

            rsMetadata = new RuleSetMetadata();
            rsMetadata.Code = preProcessorRule;
            rsMetadata.TenantId = tenantId;
            rsMetadata.Name = preProcessorRuleName;
            rsMetadata.CreatedBy = userId;
            rsMetadata.Status = true;
            rsMetadata.ScreenType = ScreenType.DecisionTable;
            rsMetadata.Category = "VirtualEntity Preprocessor Rule";

            //Create preprocessor rule set for virtual entity
            string rsPreprocessorId = ruleMetadatService.CreateRuleSetMetadata(rsMetadata);

            //Create Activity definition for Validation Rule
            RuleActivityDefinition activityDefinition = new RuleActivityDefinition();
            activityDefinition.ActivityType = ActivityType.XMLRuleActivity;
            activityDefinition.RulesetCode = validationRule;
            activityDefinition.ServiceOutputXML = "<root></root>";
            activityDefinition.TenantId = tenantId;
            activityDefinition.CreatedBy = userId;

            ruleMetadatService.AddRuleActivity(activityDefinition);


            //Create Activity definition for preprocessor rule
            activityDefinition = new RuleActivityDefinition();
            activityDefinition.ActivityType = ActivityType.XMLRuleActivity;
            activityDefinition.RulesetCode = preProcessorRule;
            activityDefinition.ServiceOutputXML = "<root></root>";
            activityDefinition.TenantId = tenantId;
            activityDefinition.CreatedBy = userId;

            ruleMetadatService.AddRuleActivity(activityDefinition);
        }

        /// <summary>
        /// This method is used to deletes the virtual entity.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        //[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteVirtualEntity(string entityId, string entityName)
        {
            if (string.IsNullOrEmpty(entityId))
            {
                TempData["argumentNullException"] = Resources.DataResource.e_entityName_required;
            }
            try
            {
                using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    VirtualEntityProxy.DeleteVirtualEntity(entityId, this.TenantId);
                    DeleteBusinessRuleForVirtualEntity(entityId, this.TenantId);
                    transactionScope.Complete();
                }
                TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.s_Entity_Deleted_Successfully, entityName);
            }
            catch (BusinessRuleException businessRuleException)
            {
                ExceptionService.HandleException(businessRuleException, PolicyName);
                TempData["virtualEntityException"] = businessRuleException.Message;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                TempData["argumentNullException"] = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                TempData["argumentException"] = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (VirtualEntityException virtualEntityException)
            {
                ExceptionService.HandleException(virtualEntityException, PolicyName);
                TempData["virtualEntityException"] = virtualEntityException.Message;
            }
            catch (UnauthorizedAccessException unauthorizedAccessExcepion)
            {
                ExceptionService.HandleException(unauthorizedAccessExcepion, PolicyName);
                ModelState.AddModelError("Error", unauthorizedAccessExcepion.Message);

            }
            return RedirectToAction("EntityList");
        }

        /// <summary>
        /// This method used to delete the business rules for virtual entity.
        /// </summary>
        /// <param name="entityId">entity identifier</param>
        /// <param name="tenantId">tenant identifier</param>
        private void DeleteBusinessRuleForVirtualEntity(string entityId, string tenantId)
        {
            string validationRule = RuleSetMetadataHelper.GetValidationRuleCode(entityId);
            string preProcessorRule = RuleSetMetadataHelper.GetPreProcessorRuleCode(entityId); ;

            var ruleMetadatService = (IRuleSetMetadataService)ServiceLocator.GetServiceImplementation(typeof(IRuleSetMetadataService));

            //Delete the  validation rule set for virtual entity
            ruleMetadatService.DeleteRuleSetMetadata(validationRule, tenantId);

            //Delete the  preprocessor rule set for virtual entity
            ruleMetadatService.DeleteRuleSetMetadata(preProcessorRule, tenantId);
        }

        /// <summary>
        /// This method is used to delete selected field metadata and return back to Field List.
        /// </summary>
        /// <param name="fieldId">field identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteEntityField(string fieldId, string entityId, string name)
        {
            try
            {
                //Delete entity field metadata for given entityId,fieldId and logged in tenantId
                bool deletedStatus = DataManagementProxy.DeleteEntityFieldMetaData(entityId, this.TenantId, fieldId);

                if (deletedStatus)
                {
                    TempData["SuccessMessage"] = Resources.DataResource.s_deleted_successfully;
                    return RedirectToAction("FieldList", new { entityId = entityId });
                }
                else
                {
                    ModelState.AddModelError("Error", Resources.DataResource.e_deleted);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                statusMessage = Resources.DataResource.e_deleted;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                statusMessage = Resources.DataResource.e_deleted;
            }
            catch (ForeignKeyException foreignKeyException)
            {
                ExceptionService.HandleException(foreignKeyException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_foreignkey_exception);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_datamanagement_deleting_field_metadata);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_deleteExtendedField);
            }
            catch (FieldMetaException fieldMetaException)
            {
                ExceptionService.HandleException(fieldMetaException, PolicyName);
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_DisplayColumnName);
            }

            this.FieldList(entityId);

            return View("FieldList");
        }

        /// <summary>
        /// This method is used to copy parent extended fields
        /// </summary>
        /// <returns></returns>
        public ActionResult CopyAllParentExtendedFields()
        {
            try
            {
                var count = DataManagementProxy.CopyAllParentExtendedFields(TenantId);
                if (count == 0)
                {
                    TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.e_noParentFields);
                }
                else
                {
                    TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.s_copyAllExtendedField, count);
                }
                // TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.s_copyAllExtendedField, count);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                statusMessage = Resources.DataResource.e_copying_field_metadata;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                statusMessage = Resources.DataResource.e_copying_field_metadata;
            }
            catch (DataManagementException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (FieldNameUniquenessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
          
            return RedirectToAction("EntityList");
        }

        /// <summary>
        /// This method is used to copy the parent extended fields.
        /// </summary>
        /// <returns></returns>
        public ActionResult CopyVirtualEntity()
        {
            try
            {
                var count = VirtualEntityProxy.CopyVirtualEntity(this.TenantId);
                if (count == 0)
                {
                    TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.e_noParentFields);
                }
                else
                {
                    TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.s_copyAllExtendedField, count);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                statusMessage = Resources.DataResource.e_copying_field_metadata;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                statusMessage = Resources.DataResource.e_copying_field_metadata;
            }
            catch (DataManagementException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (FieldNameUniquenessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("Error", licenseException.Message);
            }
            catch (VirtualEntityException virtualEntityException)
            {
                ExceptionService.HandleException(virtualEntityException, PolicyName);
                ModelState.AddModelError("Error", virtualEntityException.Message);
            }
            catch (UnauthorizedAccessException unauthorizedAccessExcepion)
            {
                ExceptionService.HandleException(unauthorizedAccessExcepion, PolicyName);
                ModelState.AddModelError("Error", unauthorizedAccessExcepion.Message);

            }

            return RedirectToAction("EntityList");
        }

        /// <summary>
        /// This method is used to copy the parent extended fields.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public ActionResult CopyParentExtendedFields(string entityId)
        {
            try
            {
                var count = DataManagementProxy.CopyParentExtendedFields(entityId, TenantId);
                if (count == 0)
                {
                    TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.e_noParentFields);
                }
                else
                {
                    TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.s_copyExtendedField, count);
                }
                //   TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataResource.s_copyExtendedField, count);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                statusMessage = Resources.DataResource.e_copying_field_metadata;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                statusMessage = Resources.DataResource.e_copying_field_metadata;
            }
            catch (DataManagementException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (FieldNameUniquenessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("FieldList", new { entityId });
        }

        /// <summary>
        /// Page loading
        /// This is used to GET form method for Field Privileges view.
        /// This is used to fetch all the field metadata for selected entity identifier.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleid">module identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public ActionResult FieldPrivilege(string roleId, string moduleid, string entityId)
        {
            privileges(roleId, moduleid, entityId);
            return View();
        }

        /// <summary>
        /// This method is used to add field privileges to role.
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object FieldPrivilegeDetails()
        {
            string roleId = string.Empty;
            statusMessage = string.Empty;
            string entityId = Request.Form["hfEntityid"].ToString();

            Dictionary<string, EntityFieldMetaData> dicEntityFieldMetaData = new Dictionary<string, EntityFieldMetaData>();

            try
            {
                //Get all fields for passed entityId and logged in tenant Id
                dicEntityFieldMetaData = DataManagementProxy.GetFields(entityId, this.TenantId);
                if (dicEntityFieldMetaData != null && dicEntityFieldMetaData.Count > 0)
                { ViewData["DicEntityFieldMetaData"] = dicEntityFieldMetaData.Values.ToList(); }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_fieldlist;
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", Resources.DataResource.e_datamanagement_getting_entitylist);

            //Get Role Name and Role id from form
            if (!string.IsNullOrEmpty(Request.Form["RoleName"]))
            {
                roleId = Request.Form["RoleName"].ToString();

                List<RolePrivilege> lstRolePrivilege = new List<RolePrivilege>();

                //Adds missing privileges
                UpdatePrivilege(entityId);

                int iFlag = 0;
                foreach (EntityFieldMetaData FieldMetadata in ((IEnumerable<EntityFieldMetaData>)ViewData["DicEntityFieldMetaData"]))
                {
                    string ViewPermission = ViewFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();
                    string EditPermission = EditFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();
                    iFlag = 0;

                    if (!string.IsNullOrEmpty(Request.Form[ViewPermission]) && Request.Form["ViewCheckbox" + FieldMetadata.EntityFieldIdentifier].ToString() != "false")
                    {
                        RolePrivilege rpRolePrivilege = new RolePrivilege { RoleId = roleId, TenantId = this.TenantId };
                        string strdataScopeId = Request.Form[ViewPermission].ToString();
                        if (strdataScopeId == "-1")
                        { strdataScopeId = null; }
                        rpRolePrivilege.PrivilegeId = ViewPermission;
                        rpRolePrivilege.DataScopeId = strdataScopeId;
                        lstRolePrivilege.Add(rpRolePrivilege);
                    }
                    else
                    {
                        iFlag = 1;
                        DeleteFieldPrivilege(roleId, ViewPermission);
                    }
                    if (!string.IsNullOrEmpty(Request.Form[EditPermission]) && Request.Form["EditCheckbox" + FieldMetadata.EntityFieldIdentifier].ToString() != "false")
                    {
                        RolePrivilege rpRolePrivilege = new RolePrivilege { RoleId = roleId, TenantId = this.TenantId };
                        string strdataScopeId = Request.Form[EditPermission].ToString();
                        if (strdataScopeId == "-1")
                        { strdataScopeId = null; }
                        rpRolePrivilege.PrivilegeId = EditPermission;
                        rpRolePrivilege.DataScopeId = strdataScopeId;
                        lstRolePrivilege.Add(rpRolePrivilege);
                    }
                    else
                    {
                        iFlag = 1;
                        DeleteFieldPrivilege(roleId, EditPermission);
                    }
                }

                if (lstRolePrivilege.Count > 0)
                {
                    try
                    {
                        PrivilegeProxy.SavePrivilegesToRole(this.TenantId, lstRolePrivilege);
                        ModelState.AddModelError("success", Resources.DataResource.s_datasaved);
                    }
                    //Catch DB Exception and throw as Privilege Exception
                    catch (DbException dbException)
                    {
                        ExceptionService.HandleException(dbException, PolicyName);
                        ModelState.AddModelError("Error", Resources.DataResource.e_Addprivilige);
                    }
                    //Catch Argument Exception and throw as Privilege Exception
                    catch (ArgumentException argumentException)
                    {
                        ExceptionService.HandleException(argumentException, PolicyName);
                        ModelState.AddModelError("Error", Resources.DataResource.e_Addprivilige);
                    }
                    catch (PrivilegeException privilegeException)
                    {
                        ExceptionService.HandleException(privilegeException, PolicyName);
                        ModelState.AddModelError("Error", Resources.DataResource.e_PriviligeExist);
                    }
                }
                else if (iFlag == 1)
                {
                    ModelState.AddModelError("success", Resources.DataResource.s_Update);
                }
                //Fetching the Role Details
                string[] RoleArray = new string[1];
                RoleArray[0] = roleId;
                Dictionary<string, RolePrivilege> dictPrivilege = new Dictionary<string, RolePrivilege>();
                try
                {
                    dictPrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, RoleArray);
                }
                //Catch DB Exception and throw as Privilege Exception
                catch (DbException dbException)
                {
                    ExceptionService.HandleException(dbException, PolicyName);
                }
                //Catch License Exception and throw as Privilege Exception
                catch (LicenseException licenseException)
                {
                    ExceptionService.HandleException(licenseException, PolicyName);
                }
                //Catch Argument Exception and throw as Privilege Exception
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, PolicyName);
                }
                ViewData["RolePrivilege"] = dictPrivilege;
            }
            else
            {
                ModelState.AddModelError("valRoleName", AccountResource.Mandatory);
                ModelState.AddModelError("Error", Resources.DataResource.e_SelectRole);
            }


            //Loading all the DataScopes for the Tenant.
            GetAllDataScopeForEntity(entityId);

            //Getting all the roles
            List<Role> Roles = new List<Role>();
            try
            {
                Roles = RoleProxy.GetTenantRoleDetails(this.TenantId);
            }
            //Catch DB Exception and throw as Role Exception
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, PolicyName);
            }
            //Catch Argument Exception and throw as Role Exception
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }

            //Fetching Global Roles
            List<Role> GlobalRoles = new List<Role>();
            try
            {
                GlobalRoles = RoleProxy.GetGlobalRoleDetails();
            }
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, PolicyName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }
            //Binding all the roles
            foreach (Role role in GlobalRoles)
            {
                Roles.Add(role);
            }

            Roles.Insert(0, new Role { RoleId = "", RoleName = "- Select User Role -" });
            ViewData["Roles"] = Roles;
            ViewData["SelectedRole"] = roleId;
            ViewData["strEntityid"] = entityId;
            ViewData["moduleId"] = Request.Form["hfmoduleId"].ToString();
            //ViewData["Roles"] = new SelectList((IEnumerable)Roles, "RoleId", "RoleName");
            return View("~/Views/DataScopeRoles/PartialFieldPrivilege.ascx");
        }

        /// <summary>
        /// This method us used to get all privilege and returns list of Privilege identifiers.
        /// </summary>
        /// <returns></returns>
        private static List<string> GetAllPrivilege()
        {
            List<Privilege> privilegeList = null;
            List<string> privilegeIds = new List<string>();
            privilegeList = PrivilegeProxy.GetAllPrivileges();
            if (privilegeList != null)
            {
                foreach (Privilege privilege in privilegeList)
                {
                    if (!privilegeIds.Contains(privilege.Id))
                    {
                        privilegeIds.Add(privilege.Id);
                    }
                }
            }
            return privilegeIds;
        }

        /// <summary>
        /// This method us used to  Update the privilege.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        private void UpdatePrivilege(string entityId)
        {
            List<string> privilegeIds = null;

            privilegeIds = GetAllPrivilege();
            //Missing Privileges
            List<Privilege> missingPrivileges = new List<Privilege>();
            //Updating Privilege
            foreach (EntityFieldMetaData FieldMetadata in ((IEnumerable<EntityFieldMetaData>)ViewData["DicEntityFieldMetaData"]))
            {
                string ViewPermission = ViewFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();
                string EditPermission = EditFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();

                if (privilegeIds == null || privilegeIds.Contains(ViewPermission))
                {
                    continue;
                }

                Privilege privilege = new Privilege();
                privilege.Id = privilege.Name = privilege.Description = ViewPermission;
                privilege.BasePrivilege = ViewEntityPrefix + entityId;
                missingPrivileges.Add(privilege);

                if (privilegeIds.Contains(EditPermission))
                {
                    continue;
                }
                privilege = new Privilege();
                privilege.Id = privilege.Name = privilege.Description = EditPermission;
                privilege.BasePrivilege = EditEntityPrefix + entityId;
                missingPrivileges.Add(privilege);
            }

            if (missingPrivileges.Count > 0)
            {
                PrivilegeProxy.AddPrivileges(missingPrivileges);
            }
        }

        /// <summary>
        /// Happens when role is changed
        /// Role Drop Down Selected Index change.
        /// Loading the scopes for the field privileges
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleid">module identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public object FieldPrivilegeLoad(string roleId, string moduleid, string entityId)
        {
            privileges(roleId, moduleid, entityId);
            return PartialView("PartialFieldPrivilege");
        }

        /// <summary>
        /// This method is used to load the Privlege page with details
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleid">module identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        private void privileges(string roleId, string moduleid, string entityId)
        {
            if (string.IsNullOrEmpty(moduleid))
            {
                moduleid = Request.Form["hfmoduleId"].ToString();
            }
            Dictionary<string, EntityFieldMetaData> dicEntityFieldMetaData = new Dictionary<string, EntityFieldMetaData>();
            try
            {
                //Get all fields for passed entityId and logged in tenant Id
                dicEntityFieldMetaData = DataManagementProxy.GetFields(entityId, this.TenantId);
                if (dicEntityFieldMetaData != null && dicEntityFieldMetaData.Count > 0)
                { ViewData["DicEntityFieldMetaData"] = dicEntityFieldMetaData.Values.ToList(); }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_fieldlist;
            }
            //Get Role Name and Role id from form
            if (!string.IsNullOrEmpty(roleId))
            {
                string[] RoleArray = new string[1];
                RoleArray[0] = roleId;
                Dictionary<string, RolePrivilege> dictPrivilege = new Dictionary<string, RolePrivilege>();
                try
                {
                    dictPrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, RoleArray);
                }
                //Catch DB Exception and throw as Privilege Exception
                catch (DbException dbException)
                {
                    ExceptionService.HandleException(dbException, PolicyName);
                }
                //Catch License Exception and throw as Privilege Exception
                catch (LicenseException licenseException)
                {
                    ExceptionService.HandleException(licenseException, PolicyName);
                }
                //Catch Argument Exception and throw as Privilege Exception
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, PolicyName);
                }
                ViewData["RolePrivilege"] = dictPrivilege;
            }

            List<Role> Roles = new List<Role>();
            try
            {
                Roles = RoleProxy.GetTenantRoleDetails(this.TenantId);
            }
            //Catch DB Exception and throw as Role Exception
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, PolicyName);
            }
            //Catch Argument Exception and throw as Role Exception
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }

            //Fetching Global Roles
            List<Role> GlobalRoles = new List<Role>();
            try
            {
                GlobalRoles = RoleProxy.GetGlobalRoleDetails();
            }
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, PolicyName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
            }
            //Binding all the roles
            foreach (Role role in GlobalRoles)
            {
                Roles.Add(role);
            }
            Roles.Insert(0, new Role { RoleId = "", RoleName = "- Select User Role -" });
            ViewData["Roles"] = Roles;
            ViewData["SelectedRole"] = roleId;

            //Loading all the DataScopes for the Tenant.
            GetAllDataScopeForEntity(entityId);

            ViewData["strEntityid"] = entityId;
            ViewData["moduleId"] = moduleid;
        }

        #endregion

        #region Entity Rules

        /// <summary>
        /// This method provide UI based on the rule type and the entity identifier.
        /// </summary>
        /// <param name="entityIdentifier">entity Identifier</param>
        /// <param name="ruleType">rule type</param>
        /// <returns></returns>
        public ActionResult ManageEntityRule(string entityIdentifier, string ruleType)
        {
            if (string.IsNullOrEmpty(entityIdentifier) || string.IsNullOrEmpty(ruleType))
            {
                return RedirectToAction("EntityList");
            }

            string ruleSetCode = string.Format(CultureInfo.InvariantCulture, "{0}_{1}Rule", entityIdentifier, ruleType);
            string screenType = CelloSaaS.Rules.Core.ScreenType.ConditionAction.ToString();

            return RedirectToAction("Manage", "Rules", new { ruleSetCode, screenType });
        }

        #endregion
    }
}
