using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.DataManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;

namespace CelloSaaSApplication.Controllers
{
    using Resources;
    using CelloSaaS.ServiceContracts.DataManagement;
    using CelloSaaS.Model.Configuration;
    using CelloSaaS.ServiceProxies.Configuration;
    using System.Globalization;

    /// <summary>
    /// This class is responsible for data scope roles.
    /// </summary>
    [HandleError]
    public class DataScopeRolesController : CelloSaaS.View.CelloController
    {
        private string TenantId = TenantContext.GetTenantId(new Role().EntityIdentifier);

        // Logger policy name
        /// <summary>
        /// Default Policy
        /// </summary>
        private const string DefaultPolicy = "GlobalExceptionLogger";

        #region Role Public Methods

        /// <summary>
        /// This method is used to get role details.
        /// </summary>
        /// <returns>Partial view of Role list</returns>
        public ActionResult RoleDetails()
        {
            // Get role details
            RoleList();
            return PartialView("RoleList");
        }

        /// <summary>
        /// This method is used to get the role details.
        /// </summary>
        /// <returns>Role details</returns>
        public ActionResult RoleDetailsList()
        {
            // Get role details
            RoleList();
            return View();
        }

        /// <summary>
        /// This method is used to add role details.
        /// </summary>
        /// <returns>Partial view of Add role details</returns>
        public ActionResult AddRoleDetails()
        {
            Role role = new Role();

            // get the mapping service list
            ViewData["ServiceList"] = GetServiceList();

            return PartialView("AddRole", role);
        }

        /// <summary>
        /// Gets the mapping service list.
        /// </summary>
        /// <returns>collection of service list</returns>
        private static IList<Service> GetServiceList()
        {
            Dictionary<string, Service> serviceslist = UserIdentity.IsInRole(RoleConstants.ProductAdmin) ? LicenseProxy.GetAllServices() : LicenseProxy.GetServicesByUserId(UserIdentity.UserId);

            return (serviceslist != null) ? serviceslist.Values.ToList() : null;
        }

        /// <summary>
        /// This method is used to add role details.
        /// </summary>
        /// <param name="formcollection">form collection.</param>
        /// <returns>
        /// If role created successfully then return success or return partial view of add role details
        /// </returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object AddRoleDetails(FormCollection formcollection)
        {
            Role role = new Role();

            try
            {
                // Update role details from form
                UpdateModel(role);

                // Validate role details
                ValidateRole(role);

                string[] roleServiceIds = Request.Form["roleService"] != null ? Request.Form["roleService"].Split(',').ToArray() : null;

                if (UserIdentity.Roles.Contains(RoleConstants.ServiceAdmin))
                {
                    if (string.IsNullOrEmpty(formcollection["IsGlobalRole"])
                        || roleServiceIds == null || roleServiceIds.Count() == 0)
                    {
                        ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_SelectOneService);
                    }
                }

                if (ModelState.IsValid)
                {
                    string result = string.Empty;

                    // Check Is Global Role checkbox
                    result = string.IsNullOrEmpty(formcollection["IsGlobalRole"]) ? RoleProxy.AddRole(this.TenantId, role.RoleName, role.Description) : RoleProxy.AddRole(null, role.RoleName, role.Description, roleServiceIds);

                    // If role details added successfully then return success or return add role partial view
                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.s_RoleCreate);
                        return RolesResource.s_SuccessMessage;
                    }
                    this.ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleCreate);
                }
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch role exception and show user exception message to user
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleCreate);
            }
            // Catch data exception and show duplicate name error message to user
            catch (DataException dataEception)
            {
                ExceptionService.HandleException(dataEception, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_DuplicateRoleName);
            }

            // get the mapping service list
            ViewData["ServiceList"] = GetServiceList();

            return PartialView("AddRole", role);
        }

        /// <summary>
        /// This method is used to bind the fields in Edit form.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <returns>Partial view of edit role</returns>
        public ActionResult RoleUpdate(string roleId)
        {
            Role roleDetail = new Role();
            try
            {
                // Get role details of role id
                roleDetail = RoleProxy.GetRoleDetailsByRoleId(roleId);

                // get the mapping service list
                ViewData["ServiceList"] = GetServiceList();

                // get services mapped the given role
                Dictionary<string, Service> roleServices = LicenseProxy.GetServicesByRoleId(roleId);

                ViewData["RoleServices"] = roleServices != null ? roleServices.Keys.ToArray() : null;
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch role exception and show exception message to user
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleLoad);
            }

            return PartialView("EditRole", roleDetail);
        }

        /// <summary>
        /// This method is to update the role.
        /// </summary>
        /// <returns>Success/Partial view of Edit role</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RoleUpdate()
        {
            Role roleDetails = new Role();
            try
            {
                //Update role details from form
                UpdateModel(roleDetails);
                // Validate role details
                ValidateRole(roleDetails);

                if (ModelState.IsValid)
                {
                    string result = string.Empty;

                    //Update role details
                    result = RoleProxy.UpdateRole(roleDetails, string.Empty);

                    // If role updated successfully return success message else return edit role partial view
                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.s_RoleUpdate);
                        return Content(Resources.RolesResource.s_SuccessMessage);
                    }
                    this.ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleUpdate);
                }
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch role exception and show exception message to user
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleUpdate);
            }

            // get the mapping service list
            ViewData["ServiceList"] = GetServiceList();

            return PartialView("EditRole", roleDetails);
        }

        /// <summary>
        /// This method is used to check the role have any privileges ,if not the role will get deleted based on the role identifier.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <returns>Partial view of Role list</returns>
        public ActionResult DeactivateRole(string roleId)
        {
            try
            {
                // Deactivate the role details
                RoleProxy.DeleteRole(!IsGlobalRole(roleId) ? this.TenantId : string.Empty, roleId);

                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.s_RoleDelete);
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch role exception and show exception message to user
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleDelete);
            }
            // Catch data exception and show exception message to user
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleDelete + Resources.RolesResource.e_RoleDeleteHasPrivilege);
            }

            // Get all role details
            RoleList();
            return PartialView("RoleList");
        }

        #endregion

        #region Role Private Methods

        /// <summary>
        /// This  is the get method of Role list.
        /// If user is Service Admin then get roles for his service only.
        /// For other users get the roles by tenant license and intersect with
        /// logged in user roles.
        /// </summary>
        private void RoleList()
        {
            try
            {
                List<Role> roleGlobalRole = new List<Role>();

                // check the logged in user is a service admin
                if (UserIdentity.Roles.Contains(RoleConstants.ServiceAdmin))
                {
                    // get his services
                    Dictionary<string, Service> userServices = LicenseProxy.GetServicesByUserId(UserIdentity.UserId);

                    if (userServices != null && userServices.Count > 0)
                    {
                        // get roles mapped to his services
                        Dictionary<string, Role> roleList = RoleProxy.GetRolesByServiceIds(userServices.Keys.ToArray());

                        // remove the product & service admin roles
                        if (roleList != null && roleList.Count > 0)
                        {
                            roleList.Remove(RoleConstants.ProductAdmin);
                            roleList.Remove(RoleConstants.ServiceAdmin);
                        }

                        roleGlobalRole = roleList != null ? roleList.Values.ToList() : null;
                        ViewData["RoleDetails"] = roleGlobalRole;
                        return;
                    }
                }


                // Get tenant role details
                List<Role> roleTenantRole = RoleProxy.GetTenantRoleDetails(this.TenantId);

                if (roleTenantRole != null && roleTenantRole.Count > 0)
                {
                    roleTenantRole.RemoveAll(r => r.RoleId.Equals(RoleConstants.ProductAdmin, StringComparison.OrdinalIgnoreCase));
                }

                // Get global role details
                roleGlobalRole = RoleProxy.GetGlobalRolesByTenant(this.TenantId);

                // Remove product admin role
                if (roleGlobalRole != null && roleGlobalRole.Count > 0)
                {
                    List<string> lstUserRoles = null;

                    // Get the logged in user roles
                    if (UserIdentity.Roles != null && UserIdentity.Roles.Count() > 0)
                    {
                        lstUserRoles = UserIdentity.Roles.ToList();
                    }

                    // If user have roles check user have tenant admin or product admin  
                    if (lstUserRoles != null && lstUserRoles.Count > 0 && (lstUserRoles.Contains(RoleConstants.ProductAdmin) || lstUserRoles.Contains(RoleConstants.TenantAdmin)))
                    {
                        // If user have tenant admin role then remove product admin role from role list
                        if (lstUserRoles.Contains(RoleConstants.TenantAdmin) && !lstUserRoles.Contains(RoleConstants.ProductAdmin))
                        {
                            for (int i = 0; i < roleGlobalRole.Count; i++)
                            {
                                if (!roleGlobalRole[i].RoleId.Equals(RoleConstants.ProductAdmin))
                                {
                                    continue;
                                }
                                roleGlobalRole.RemoveAt(i);
                                break;
                            }
                        }
                    }
                    // If user does not have any roles remove product admin and tenant admin roles from role list
                    else
                    {
                        for (int i = 0; i < roleGlobalRole.Count; i++)
                        {
                            // Remove product admin role from role list
                            if (roleGlobalRole[i].RoleId.Equals(RoleConstants.ProductAdmin))
                            {
                                roleGlobalRole.RemoveAt(i);
                                i--;
                                continue;
                            }
                            // Remove tenant admin role from role list
                            if (roleGlobalRole[i].RoleId.Equals(RoleConstants.TenantAdmin))
                            {
                                roleGlobalRole.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                    }
                }

                if (roleTenantRole != null && roleTenantRole.Count > 0)
                {
                    roleGlobalRole.AddRange(roleTenantRole);
                }

                ViewData["RoleDetails"] = roleGlobalRole;
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch role exception and show exception message to user
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleLoad);
            }
        }

        /// <summary>
        /// This to validate the role details.
        /// </summary>
        /// <param name="role">role details</param>
        private void ValidateRole(Role role)
        {
            // If role id is null or empty(create) then validate role name.
            if (string.IsNullOrEmpty(role.RoleId))
            {
                // Check role name is null or empty
                if (string.IsNullOrEmpty(role.RoleName) || string.IsNullOrEmpty(role.RoleName.Trim()))
                {
                    ModelState.AddModelError("RoleName", "");
                    ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_RoleNameMandatory);
                }
                // Check role name is in correct format or not
                else
                {
                    if (!Util.ValidateIdentifierWithSpace(role.RoleName))
                        ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_RoleName);
                }
            }
            // Check role description is null or empty
            if (!string.IsNullOrEmpty(role.Description) && !string.IsNullOrEmpty(role.Description.Trim()))
            {
                return;
            }
            this.ModelState.AddModelError("Description", "");
            this.ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_Description);
        }

        #endregion

        #region Role Privilege Public Methods

        /// <summary>
        /// This method is used to get role privilege details.
        /// </summary>
        /// <param name="strRoleId">role identifier</param>
        /// <param name="mode">mode-edit/insert</param>
        /// <param name="rolePrivilegeId">role privilege identifier</param>
        /// <param name="dataScopeId">data scope identifier</param>
        /// <param name="roleName">role name</param>
        /// <returns>role privilege list details view</returns>
        public ActionResult RolePrivilegeList(string strRoleId, string mode, string rolePrivilegeId, string dataScopeId, string roleName)
        {
            // Get role privilege details and set into view data
            SetRolePrivilegeList(strRoleId, roleName);
            // If mode is not null then assign the role privilege id in View data and get the data scope details
            if (mode != null)
            {
                ViewData["EditRolePrivilegeId"] = rolePrivilegeId;
                // Get Data Scope details
                GetDataScopeDetails(dataScopeId);
            }
            return View("RolePrivilegeList");
        }

        /// <summary>
        /// This method is used to add privileges to role.
        /// </summary>
        /// <param name="strRoleId">role identifier</param>
        /// <param name="roleName">role name</param>
        /// <returns>view of privilege and data scope details</returns>
        public ActionResult AddRolePrivilege(string strRoleId, string roleName)
        {
            // Get role privilege details and set into view data
            SetRolePrivilegeList(strRoleId, roleName);
            return View();
        }

        /// <summary>
        /// This method is used to add privileges to role.
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object AddRolePrivilege()
        {
            string roleId = string.Empty;
            string message;
            try
            {
                string roleName = string.Empty;
                // Get Role Name and Role id from form
                if (Request.Form["RoleName"] != null && Request.Form["RoleId"] != null)
                {
                    roleId = Request.Form["RoleId"].ToString();
                    roleName = Request.Form["RoleName"].ToString();
                }
                ViewData["RoleName"] = roleName;
                ViewData["RoleId"] = roleId;


                // Get all data scopes
                List<DataScope> lstDataScope = new List<DataScope>();
                lstDataScope = DataAccessProxy.GetAllDataScope();

                // Get privilege and data scope details
                GetPrivilegeAndDataScopeDetails(roleId, out message);


                List<string> ltPrivilege = new List<string>();
                // Get the selected privilege ids
                if (Request.Form["privilege"] != null && !string.IsNullOrEmpty(Request.Form["privilege"].ToString()))
                {
                    ltPrivilege.AddRange(Request.Form["privilege"].Split(',').ToList());
                    ViewData["ListPrivilege"] = ltPrivilege;
                }

                List<RolePrivilege> ltRolePrivilege = new List<RolePrivilege>();
                // Check if any one of the privilege is selected or not
                if (ltPrivilege != null && ltPrivilege.Count > 0)
                {
                    foreach (string privilegeId in ltPrivilege)
                    {
                        // Get datascope of the privilege
                        string dataScopeId = Request.Form["DataScope_" + privilegeId];
                        // Check datascope is selected or not. If selected then add it to dict or add error message
                        if (!string.IsNullOrEmpty(dataScopeId))
                        {
                            //create instance for role privilege and set all details and add it in the list
                            RolePrivilege rpRolePrivilege = new RolePrivilege
                                {
                                    RoleId = roleId,
                                    PrivilegeId = privilegeId,
                                    TenantId = this.TenantId,
                                    DataScopeId = dataScopeId
                                };
                            ltRolePrivilege.Add(rpRolePrivilege);
                            ViewData["DataScope_" + privilegeId] = new SelectList((IEnumerable)lstDataScope, "ID", "Name", dataScopeId);
                        }
                        else
                            ModelState.AddModelError("valDataScope_" + privilegeId, AccountResource.Mandatory);
                    }
                }
                else
                    ModelState.AddModelError("AddRolePrivilegeStatusMessage", Resources.RolesResource.e_PrivilegeSelect);

                // Check role privilege list have details or not
                if (ModelState.IsValid)
                {
                    //Add privileges to role
                    PrivilegeProxy.AddPrivilegesToRole(this.TenantId, ltRolePrivilege);
                    ModelState.AddModelError("AddRolePrivilegeStatusMessage", Resources.RolesResource.s_PrivilegeAdd);
                    return Resources.RolesResource.s_SuccessMessage;
                }
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch data access exception and show exception message to user
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeAdd);
            }
            // Catch privilege exception and show exception message to user
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeAdd);
            }
            // Get privilege and datascope details for role identifier
            GetPrivilegeAndDataScopeDetails(roleId, out message);

            return PartialView("PartialPrivilegeList");
        }

        /// <summary>
        /// This method is used to edit the role privilege details.
        /// </summary>
        /// <param name="rolePrivilegeId">role privilege identifier.</param>
        /// <param name="dataScopeId">data scope identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <param name="roleName">name of the role.</param>
        /// <returns>View of Role privilege list</returns>
        public ActionResult UpdateRolePrivilege(string rolePrivilegeId, string dataScopeId, string roleId, string roleName)
        {
            try
            {
                // Check data scope is selected or not
                if (string.IsNullOrEmpty(dataScopeId))
                {
                    ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScope);
                }
                //Set role privilege id in view data
                ViewData["EditRolePrivilegeId"] = rolePrivilegeId;

                if (ModelState.IsValid)
                {
                    // Create role privilege object and assign the values
                    RolePrivilege rolePrivilege = new RolePrivilege { Id = rolePrivilegeId, DataScopeId = dataScopeId, RoleId = roleId };
                    //Update data scope for role privilege
                    string success = PrivilegeProxy.UpdateRolePrivilege(rolePrivilege);
                    // If role privilege updated successfully then clear view data role privilege id
                    if (!string.IsNullOrEmpty(success))
                    {
                        ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.s_DataScopeUpdate);
                        ViewData["EditRolePrivilegeId"] = null;
                    }
                    else
                        ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScopeUpdate);
                }
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch privilege exception and show exception message to user
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScopeUpdate);
            }
            // Get role privilege details and set into viewdata
            SetRolePrivilegeList(roleId, roleName);
            // Get all data scope
            GetDataScopeDetails(dataScopeId);

            return View("RolePrivilegeList");
        }

        /// <summary>
        /// This method is used to delete role privilege details.
        /// </summary>
        /// <param name="roleId">role identifier</param>
        /// <param name="rolePrivilegeId">role privilege identifier</param>
        /// <param name="roleName">role name</param>
        /// <returns>View of Role Privilege List</returns>
        public ActionResult DeleteRolePrivilege(string roleId, string rolePrivilegeId, string roleName)
        {
            try
            {
                // Delete role privilege details and get the success message
                PrivilegeProxy.DeleteRolePrivilege(rolePrivilegeId);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.s_PrivilegeDelete);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_AccessDenied);
            }
            // Catch privilege exception and show exception message to user
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDelete);
            }
            // Catch data access exception and show exception message to user
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDelete);
            }
            // Get role privilege details and set into viewdata
            SetRolePrivilegeList(roleId, roleName);

            return View("RolePrivilegeList");
        }

        /// <summary>
        /// This method is used to get the privilege list of roles.
        /// </summary>
        /// <returns>Message/Partial view of privilege list</returns>
        public object GetPrivilegeDetails()
        {
            string message;
            // Get privilege and datascope details
            GetPrivilegeAndDataScopeDetails(string.Empty, out message);
            if (ModelState.IsValid)
            {
                return PartialView("PartialPrivilegeList");
            }
            return message;
        }

        #endregion

        #region Role Privilege Private Methods

        /// <summary>
        /// This method is used to get data scope details.
        /// </summary>
        /// <param name="dataScopeId">data scope identifier</param>
        private void GetDataScopeDetails(string dataScopeId)
        {
            try
            {
                // Get data scope details
                List<DataScope> lstDataScope = new List<DataScope>();
                lstDataScope = DataAccessProxy.GetAllDataScope();
                ViewData["DataScope"] = new SelectList((IEnumerable)lstDataScope, "ID", "Name", dataScopeId);
            }
            // Catch data access exception and show exception message to user
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScopeLoad);
            }
        }

        /// <summary>
        /// This method is used to get role privilege details and set into view data.
        /// </summary>
        /// <param name="strRoleId">role identifier.</param>
        /// <param name="roleName">role name.</param>
        private void SetRolePrivilegeList(string strRoleId, string roleName)
        {
            try
            {
                string roleId = strRoleId;
                List<RolePrivilege> lstRolePrivilege = new List<RolePrivilege>();
                if (!string.IsNullOrEmpty(roleId.Trim()))
                {
                    //Set role ids string array
                    string[] arrRoleName = { roleId };
                    // Get role privilege details
                    Dictionary<string, RolePrivilege> dictRolePrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, arrRoleName);
                    if (dictRolePrivilege != null && dictRolePrivilege.Count > 0)
                        lstRolePrivilege = dictRolePrivilege.Values.ToList();
                }
                ViewData["RolePrivilegeForRole"] = lstRolePrivilege;
                ViewData["RoleName"] = roleName;
                ViewData["RoleId"] = roleId;
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch privilege exception and show exception message to user
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_RolePrivilegeLoad);
            }
        }

        /// <summary>
        /// This method is used to get privilege and data scope details.
        /// </summary>
        /// <param name="strRoleId">The STR role identifier.</param>
        /// <param name="message">The message.</param>
        private void GetPrivilegeAndDataScopeDetails(string strRoleId, out string message)
        {
            message = string.Empty;
            try
            {
                string roleId = strRoleId;
                string roleName = string.Empty;

                // Get Role Name and Role Id from form
                if (Request.Form["RoleName"] != null && Request.Form["RoleId"] != null)
                {
                    roleId = Request.Form["RoleId"].ToString();
                    roleName = Request.Form["RoleName"].ToString();
                }
                ViewData["RoleName"] = roleName;
                ViewData["RoleId"] = roleId;

                List<Privilege> lstPrivilege = new List<Privilege>();
                if (Request.Form["SearchString"] != null)
                {
                    // Get search string from form
                    ViewData["SearchString"] = Request.Form["SearchString"].ToString();
                    string serachString = Request.Form["SearchString"].ToString();
                    // Chech the search string
                    if (!string.IsNullOrEmpty(serachString.Trim()))
                    {
                        // Get privilege based on the search string
                        lstPrivilege = PrivilegeProxy.GetPrivileges(serachString, this.TenantId);
                    }
                    else
                    {
                        message = Resources.RolesResource.e_PrivilegeSearch;
                        ModelState.AddModelError("SearchStringMessage", AccountResource.Mandatory);
                    }
                }
                // Check role id
                if (!string.IsNullOrEmpty(roleId))
                {
                    //create string array for role id
                    string[] arrRoleId = { roleId };
                    // Get the role privileges for role id
                    Dictionary<string, RolePrivilege> dictRolePrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, arrRoleId);
                    // Convert dictionary values into list
                    List<RolePrivilege> lstRolePrivilege = dictRolePrivilege.Values.ToList();
                    ViewData["RolePrivilegeForRole"] = lstRolePrivilege;
                    // Remove privileges from privilege list if already available for this role
                    if (lstPrivilege != null && lstPrivilege.Count > 0 && lstRolePrivilege != null && lstRolePrivilege.Count > 0)
                    {
                        foreach (RolePrivilege rolePrivilege in lstRolePrivilege)
                        {
                            foreach (Privilege privilege in lstPrivilege)
                            {
                                if (!privilege.Id.Trim().Equals(rolePrivilege.PrivilegeId.Trim()))
                                {
                                    continue;
                                }
                                lstPrivilege.Remove(privilege);
                                break;
                            }
                        }
                    }
                }
                ViewData["NewPrivileges"] = lstPrivilege;

                // Get all data scopes
                List<DataScope> lstDataScope = new List<DataScope>();
                lstDataScope = DataAccessProxy.GetAllDataScope();

                if (lstPrivilege != null && lstPrivilege.Count > 0)
                {
                    //Assign datascope details in the view data
                    foreach (Privilege privilege in lstPrivilege)
                    {
                        ViewData["DataScope_" + privilege.Id] = new SelectList((IEnumerable)lstDataScope, "ID", "Name");
                    }
                }
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch privilege exception and show exception message to user
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDataScopeLoad);
            }
            // Catch data access exception and show exception message to user
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, DefaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDataScopeLoad);
            }
        }

        #endregion

        #region Manage Privilege Public Methods

        /// <summary>
        /// This method is used loading Manage Privilege Details.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleId">module identifier.</param>
        /// <returns>Returning the view</returns>
        public ActionResult ManagePrivilege(string roleId, string moduleId)
        {
            ManagePrivilegeDetails(roleId, moduleId);
            return View();
        }

        /// <summary>
        /// This method is used to loading the Details based on the Drop down list change.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleId">module identifier.</param>
        /// <returns>Returning the partial view.</returns>
        public ActionResult ManagePrivilegePartialView(string roleId, string moduleId)
        {
            ManagePrivilegeDetails(roleId, moduleId);
            return PartialView("PartialMangePrivilege");
        }

        /// <summary>
        /// This method is used to inserting or updating the Privilege Details for the Role in the module.
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagePrivilege()
        {
            ViewData["EntityPrivileges"] = null;
            string roleId = string.Empty;
            string statusMessage = string.Empty;
            string moduleId = string.Empty;

            // Fetching all the modules
            List<Module> lstModule = new List<Module>();
            Dictionary<string, Module> modules = new Dictionary<string, Module>();

            // Fetching all the modules based on the Tenant License.
            TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);
            if (tenantLicense != null && tenantLicense.LicenseModuleDetails != null && tenantLicense.LicenseModuleDetails.Count > 0)
                modules = tenantLicense.LicenseModuleDetails;

            lstModule = modules.Values.ToList();
            lstModule.Insert(0, new Module { ModuleCode = "-1", ModuleName = "- All -" });
            ViewData["Modules"] = lstModule;

            ViewData["DictEntityMetaData"] = null;

            // Get Role Name and Role id from form
            if (!string.IsNullOrEmpty(Request.Form["RoleName"]))
            {
                roleId = Request.Form["RoleName"].ToString();
                ViewData["SelectedRole"] = roleId;

                moduleId = Request.Form["ModuleName"].ToString();
                ViewData["SelectedModule"] = moduleId;
                // Get All EntityMetaData IsExtendable for the role and modules
                Dictionary<string, EntityMetaData> dctEntityMetaData = GetModuleFeatures(Request.Form["ModuleName"], modules);

                // List to store the role privileges
                List<RolePrivilege> lstRolePrivilege = new List<RolePrivilege>();

                // Load the other privileges for Entities and the datascopes for each entities
                Dictionary<string, Dictionary<string, EntityPrivilege>> dictOtherEntityPrivileges = LoadDataScopeAndPrivileges(dctEntityMetaData, roleId);

                bool hasToCopy = false;

                // if tenant changes the global role privilege then check if it is already copied 
                // else copy it once.
                if (!ProductAdminConstants.IsProductAdminTenant && IsGlobalRole(roleId))
                {
                    // check privileges for role by tenant id
                    //Dictionary<string, RolePrivilege> dictRolePrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, new string[] { roleId });
                    //hasToCopy = (dictRolePrivilege == null || dictRolePrivilege.Count == 0) ? true : false;
                    Dictionary<string, int> privilegeCount = PrivilegeProxy.GetRolePrivilegeCount(this.TenantId, new string[] { roleId });
                    hasToCopy = (privilegeCount != null && privilegeCount.ContainsKey(roleId) && privilegeCount[roleId] > 0) ? false : true;
                }

                try
                {
                    if (dctEntityMetaData != null && dctEntityMetaData.Count > 0)
                    {
                        foreach (EntityMetaData EntityMetadata in dctEntityMetaData.Values)
                        {

                            string ViewPermission = "View_" + EntityMetadata.EntityIdentifier.ToString();
                            string AddPermission = "Add_" + EntityMetadata.EntityIdentifier.ToString();
                            string EditPermission = "Edit_" + EntityMetadata.EntityIdentifier.ToString();
                            string DeletePermission = "Delete_" + EntityMetadata.EntityIdentifier.ToString();

                            if (!string.IsNullOrEmpty(Request.Form[ViewPermission]) && Request.Form["ViewCheckbox" + EntityMetadata.EntityIdentifier].ToString() != "false")
                            {
                                //Adding the role privileges to list or updating the role privilege
                                rolePrivileges(roleId, ViewPermission, lstRolePrivilege, ViewPermission, hasToCopy);
                            }
                            else
                            {
                                // Deleting the roleprivileges
                                DeleteFieldPrivilege(roleId, ViewPermission);
                            }
                            if (!string.IsNullOrEmpty(Request.Form[AddPermission]) && Request.Form["AddCheckbox" + EntityMetadata.EntityIdentifier].ToString() != "false")
                            {
                                rolePrivileges(roleId, AddPermission, lstRolePrivilege, AddPermission, hasToCopy);
                            }
                            else
                            {
                                DeleteFieldPrivilege(roleId, AddPermission);
                            }
                            if (!string.IsNullOrEmpty(Request.Form[EditPermission]) && Request.Form["EditCheckbox" + EntityMetadata.EntityIdentifier].ToString() != "false")
                            {
                                rolePrivileges(roleId, EditPermission, lstRolePrivilege, EditPermission, hasToCopy);
                            }
                            else
                            {
                                DeleteFieldPrivilege(roleId, EditPermission);
                            }
                            if (!string.IsNullOrEmpty(Request.Form[DeletePermission]) && Request.Form["DeleteCheckbox" + EntityMetadata.EntityIdentifier].ToString() != "false")
                            {
                                rolePrivileges(roleId, DeletePermission, lstRolePrivilege, DeletePermission, hasToCopy);
                            }
                            else
                            {
                                DeleteFieldPrivilege(roleId, DeletePermission);
                            }

                            // Other Privileges for the entity
                            if (dictOtherEntityPrivileges == null ||
                                !dictOtherEntityPrivileges.Keys.Contains(EntityMetadata.EntityIdentifier))
                            {
                                continue;
                            }
                            Dictionary<string, EntityPrivilege> dictPrivilege = new Dictionary<string, EntityPrivilege>();
                            dictPrivilege = dictOtherEntityPrivileges[EntityMetadata.EntityIdentifier];
                            foreach (EntityPrivilege entityPrivilege in dictPrivilege.Values)
                            {
                                string strPrivilege = entityPrivilege.PrivilegeId + EntityMetadata.EntityIdentifier;
                                if (!string.IsNullOrEmpty(this.Request.Form[strPrivilege]) && this.Request.Form["PrivilegeCheckbox" + entityPrivilege.PrivilegeId].ToString() != "false")
                                {
                                    this.rolePrivileges(roleId, strPrivilege, lstRolePrivilege, entityPrivilege.PrivilegeId, hasToCopy);
                                }
                                else
                                {
                                    this.DeleteFieldPrivilege(roleId, entityPrivilege.PrivilegeId);
                                }
                            }
                        }
                    }

                    if (lstRolePrivilege.Count > 0)
                    {
                        string tenantId = this.TenantId;

                        // save the  role privileges
                        PrivilegeProxy.AddPrivilegesToRole(tenantId, lstRolePrivilege);

                        statusMessage = Resources.RolesResource.s_DataSaved;
                        ModelState.AddModelError("success", statusMessage);

                    }
                    else
                    {
                        statusMessage = Resources.RolesResource.s_DataUpdated;
                        ModelState.AddModelError("success", statusMessage);
                    }
                }
                catch (UnauthorizedAccessException uex)
                {
                    ExceptionService.HandleException(uex, DefaultPolicy);
                    statusMessage = Resources.RolesResource.e_AccessDenied;
                    ModelState.AddModelError("error", statusMessage);
                }
                // Catch Argument Null Exception and throw as Privilege Exception
                catch (ArgumentNullException argumentException)
                {
                    ExceptionService.HandleException(argumentException, DefaultPolicy);
                    statusMessage = Resources.RolesResource.e_AddPrivilege;
                    ModelState.AddModelError("error", statusMessage);
                }
                // Catch Argument Exception and throw as Privilege Exception
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, DefaultPolicy);
                    statusMessage = Resources.RolesResource.e_AddPrivilege;
                    ModelState.AddModelError("error", statusMessage);
                }
                // Catch DB Exception and throw as Privilege Exception
                catch (DbException dbException)
                {
                    ExceptionService.HandleException(dbException, DefaultPolicy);
                    statusMessage = Resources.RolesResource.e_AddPrivilege;
                    ModelState.AddModelError("error", statusMessage);
                }

                // Loading the default Privileges
                LoadRolePrivileges(roleId);
            }
            else
            {
                ViewData["SelectedRole"] = string.Empty;
                ViewData["SelectedModule"] = string.Empty;
                ModelState.AddModelError("error", Resources.RolesResource.e_RoleNameEmpty);
            }

            LoadRolesForTenant(this.TenantId);

            return PartialView("PartialMangePrivilege");
        }


        #endregion

        #region Manage Privilege Private Methods

        /// <summary>
        /// This method is used to loading the modules and details
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleId">module identifier.</param>
        private void ManagePrivilegeDetails(string roleId, string moduleId)
        {
            ViewData["EntityPrivileges"] = null;
            List<Module> lstModule = new List<Module>();

            // Fetching all the modules
            Dictionary<string, Module> modules = new Dictionary<string, Module>();

            // Fetching all the modules based on the Tenant License.
            TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);

            if (tenantLicense != null && tenantLicense.LicenseModuleDetails != null && tenantLicense.LicenseModuleDetails.Count > 0)
            {
                modules = tenantLicense.LicenseModuleDetails;
                lstModule = modules.Values.ToList();
            }

            lstModule.Insert(0, new Module { ModuleCode = "-1", ModuleName = "- All -" });
            ViewData["Modules"] = lstModule;

            // store the selections
            if (!string.IsNullOrEmpty(moduleId))
            {
                ViewData["SelectedModule"] = moduleId;
            }
            else
            {
                ViewData["SelectedModule"] = string.Empty;
            }

            if (!string.IsNullOrEmpty(roleId))
            {
                ViewData["SelectedRole"] = roleId;
                ViewData["DictEntityMetaData"] = null;

                if (string.IsNullOrEmpty(moduleId))
                {
                    moduleId = "-1";
                }

                // Dictionary of Entity Metadata based on the module id and role id. 
                Dictionary<string, EntityMetaData> dctEntityMetaData = GetModuleFeatures(moduleId, modules);

                if (dctEntityMetaData != null && dctEntityMetaData.Count > 0)
                {
                    // Load the other privileges for Entities and the data scopes for each entities
                    LoadDataScopeAndPrivileges(dctEntityMetaData, roleId);
                }

                // Loading the Role Privileges for the role 
                LoadRolePrivileges(roleId);
            }
            else
            {
                ViewData["SelectedRole"] = string.Empty;
            }

            // Loading the Roles and DataScopes
            LoadRolesForTenant(this.TenantId);
        }

        /// <summary>
        /// This method is used to adding privileges to the list or updating the role privileges.
        /// </summary>
        /// <param name="roleId">role identifier</param>
        /// <param name="permission">permission eg:View_identifier</param>
        /// <param name="lstRolePrivilege">list to add</param>
        /// <param name="privilegeId">privilege identifier</param>
        /// <param name="hasToCopy">if set to <c>true</c> [has to copy].</param>
        private void rolePrivileges(string roleId, string permission, List<RolePrivilege> lstRolePrivilege, string privilegeId, bool hasToCopy)
        {
            RolePrivilege rpRolePrivilege = new RolePrivilege { RoleId = roleId, TenantId = this.TenantId };

            string strdataScopeId = Request.Form[permission].ToString();

            if (strdataScopeId == "-1")
            {
                strdataScopeId = null;
            }

            rpRolePrivilege.PrivilegeId = privilegeId;
            rpRolePrivilege.DataScopeId = strdataScopeId;

            //Updating or adding the privileges.
            if (!string.IsNullOrEmpty(Request.Form["hf" + privilegeId]) && !hasToCopy)
            {
                rpRolePrivilege.Id = Request.Form["hf" + privilegeId].ToString();
                PrivilegeProxy.UpdateRolePrivilege(rpRolePrivilege);
            }
            else
            {
                lstRolePrivilege.Add(rpRolePrivilege);
            }
        }

        /// <summary>
        /// This method is used to get dictionary of Entity Metadata based on the module id and role id.
        /// If module id is null dictionary contains for all the modules
        /// </summary>
        /// <param name="moduleId">module identifier</param>
        /// <param name="modules">collection of modules</param>
        /// <returns>
        /// collection of Entity Metadata
        /// </returns>
        private Dictionary<string, EntityMetaData> GetModuleFeatures(string moduleId, Dictionary<string, Module> modules)
        {
            string[] strModules = modules.Keys.ToArray();

            // Dictionary of features based on the modules
            Dictionary<string, Dictionary<string, Feature>> modeuleFeatures = null;

            modeuleFeatures = LicenseProxy.GetFeatureByModuleIds(strModules);

            if (modeuleFeatures != null && modeuleFeatures.Count > 0)
            {
                Dictionary<string, Feature> featureList = new Dictionary<string, Feature>();

                // if module is not select all features based on the modules
                if (moduleId != "-1")
                {
                    featureList = modeuleFeatures[moduleId];
                }
                else
                {
                    foreach (Dictionary<string, Feature> feature in modeuleFeatures.Values)
                    {
                        foreach (KeyValuePair<string, Feature> kvp in feature)
                        {
                            featureList.Add(kvp.Key, kvp.Value);
                        }
                    }
                }

                var dctEntityMetaData = DataManagementProxy.GetAllEntityMetaData(featureList.Keys.ToArray());

                if (dctEntityMetaData != null)
                    ViewData["DictEntityMetaData"] = dctEntityMetaData.Values.ToList();

                return dctEntityMetaData;
            }

            return null;
        }

        /// <summary>
        /// This method is used to loading the data scope for each entities.
        /// Fetching all the privileges based on the Entities.
        /// Load the other privileges for Entities
        /// </summary>
        /// <param name="dctEntityMetaData">dictionary of entity metadata</param>
        /// <param name="roleId">role identifier.</param>
        /// <returns>
        /// other entity privileges for the entities.
        /// </returns>
        private Dictionary<string, Dictionary<string, EntityPrivilege>> LoadDataScopeAndPrivileges(Dictionary<string, EntityMetaData> dctEntityMetaData, string roleId)
        {
            if (dctEntityMetaData == null || dctEntityMetaData.Count < 1) return null;
            // Fetching Extra Privileges for the roles
            string[] strEntities = null;
            strEntities = dctEntityMetaData.Keys.ToArray();
            Dictionary<string, Dictionary<string, EntityPrivilege>> dictOtherEntityPrivilege = null;
            try
            {
                // DataScope for each entities    
                Dictionary<string, Dictionary<string, DataScope>> dictEntityDataScope = new Dictionary<string, Dictionary<string, DataScope>>();
                dictEntityDataScope = DataAccessProxy.GetAllDataScopeBasedonEntity(strEntities);
                ViewData["EntityDataScope"] = dictEntityDataScope;

                // Fetching all the privileges based on the Entities.
                Dictionary<string, Dictionary<string, EntityPrivilege>> dictEntityPrivilege = new Dictionary<string, Dictionary<string, EntityPrivilege>>();
                dictEntityPrivilege = EntityPrivilegeProxy.GetAllEntityPrivileges(strEntities);

                Dictionary<string, Privilege> assignablePrivileges = PrivilegeProxy.GetCurrentUsersAssignablePrivilegesForRole(roleId);

                // filter by assignable privileges
                if (assignablePrivileges != null && assignablePrivileges.Count > 0)
                {
                    Dictionary<string, Dictionary<string, EntityPrivilege>> filteredEntityPrivilege = new Dictionary<string, Dictionary<string, EntityPrivilege>>();

                    foreach (KeyValuePair<string, Dictionary<string, EntityPrivilege>> entityPrivilege in dictEntityPrivilege)
                    {
                        string entityKey = entityPrivilege.Key;
                        Dictionary<string, EntityPrivilege> filteredPrivilege = new Dictionary<string, EntityPrivilege>();

                        foreach (KeyValuePair<string, EntityPrivilege> privilege in entityPrivilege.Value)
                        {
                            if (assignablePrivileges.ContainsKey(privilege.Key))
                            {
                                filteredPrivilege.Add(privilege.Key, privilege.Value);
                            }
                        }

                        if (filteredPrivilege.Count > 0)
                        {
                            filteredEntityPrivilege.Add(entityKey, filteredPrivilege);
                        }
                    }

                    dictEntityPrivilege = filteredEntityPrivilege;
                    // Load the other privileges for Entities                    

                }
                ViewData["EntityPrivileges"] = dictEntityPrivilege;
                dictOtherEntityPrivilege = LoadOtherPrivileges(dictEntityPrivilege);
                if (dictOtherEntityPrivilege != null && dictOtherEntityPrivilege.Keys.Count > 0)
                {
                    ViewData["OtherEntityPrivilege"] = dictOtherEntityPrivilege;
                }
                else
                {
                    ViewData["OtherEntityPrivilege"] = null;
                }

            }
            catch (ArgumentNullException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchRoles);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchRoles);
            }

            return dictOtherEntityPrivilege;
        }

        /// <summary>
        /// This method is used to loading the data scopes and the Roles
        /// </summary>
        /// <param name="tenantId">The tenant identifier.</param>
        private void LoadRolesForTenant(string tenantId)
        {
            // Getting all the roles from the logged in tenant
            List<Role> Roles = new List<Role>();
            try
            {
                Roles = RoleProxy.GetTenantRoleDetails(tenantId);
                if (Roles != null)
                {
                    Roles.RemoveAll(r => r.RoleId.Equals(RoleConstants.ProductAdmin, StringComparison.OrdinalIgnoreCase));
                }
            }
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchRoles);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchRoles);
            }

            // Fetching Global Roles
            List<Role> GlobalRoles = null;
            try
            {
                // Get global roles accessible by the tenant [license]
                GlobalRoles = RoleProxy.GetGlobalRolesByTenant(this.TenantId);

                if (GlobalRoles != null && GlobalRoles.Count > 0)
                {
                    //Binding all the roles
                    Roles.AddRange(GlobalRoles);
                }
            }
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchRoles);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchRoles);
            }

            Roles.Insert(0, new Role { RoleId = "", RoleName = "- Select User Role -" });
            ViewData["Roles"] = Roles;
        }

        /// <summary>
        /// This method is used to remove the old Field Privilege for the role if the Scope is null
        /// </summary>
        /// <param name="roleId">role name</param>
        /// <param name="privilegeId">privilege name</param>
        private void DeleteFieldPrivilege(string roleId, string privilegeId)
        {
            try
            {
                string tenantId = this.TenantId;

                //if (ProductAdminConstants.IsProductAdminTenant && IsGlobalRole(roleId)
                //    && !roleId.Equals(RoleConstants.ServiceAdmin, StringComparison.InvariantCulture))
                //{
                //    tenantIdentifier = string.Empty;
                //}

                Dictionary<string, RolePrivilege> dictRolePrivilege = PrivilegeProxy.GetPrivilegesForRoles(tenantId, new string[] { roleId }, privilegeId);

                if (dictRolePrivilege != null && dictRolePrivilege.Count > 0)
                {
                    // if logged in user is product admin tenant then pass null value
                    //if (ProductAdminConstants.IsProductAdminTenant && IsGlobalRole(roleId)
                    //    && !roleId.Equals(RoleConstants.ServiceAdmin, StringComparison.InvariantCulture))
                    //{
                    //    tenantId = string.Empty;
                    //}

                    PrivilegeProxy.DeletePrivilegesForRole(tenantId, roleId, new string[] { privilegeId });
                }
            }
            // Catch DB Exception and throw as Privilege Exception
            catch (PrivilegeException dbException)
            {
                ExceptionService.HandleException(dbException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_DeletePrivileges);
            }
            // Catch License Exception and throw as Privilege Exception
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_DeletePrivileges);
            }
            // Catch Argument Exception and throw as Privilege Exception
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_DeletePrivileges);
            }
        }

        /// <summary>
        /// This method is used to loading role privileges for the given role identifier
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        private void LoadRolePrivileges(string roleId)
        {
            Dictionary<string, RolePrivilege> dictPrivilege = null;

            try
            {
                string tenantid = this.TenantId;
                //if (ProductAdminConstants.IsProductAdminTenant
                //           && IsGlobalRole(roleId)
                //           && !roleId.Equals(RoleConstants.ServiceAdmin, StringComparison.InvariantCulture))
                //{
                //    tenantid = string.Empty;
                //}
                // get all the privileges available for the given role id.
                dictPrivilege = PrivilegeProxy.GetPrivilegesForRoles(tenantid, new string[] { roleId });

                // if role privilege is empty and the role is global role
                // the fetch the global role privilege list
                if (IsGlobalRole(roleId) && (dictPrivilege == null || dictPrivilege.Count == 0))
                {
                    dictPrivilege = PrivilegeProxy.GetPrivilegesForRoles(null, new string[] { roleId });
                }
            }
            // Catch DB Exception and throw as Privilege Exception
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchPrivileges);
            }
            // Catch License Exception and throw as Privilege Exception
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchPrivileges);
            }
            // Catch Argument Exception and throw as Privilege Exception
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("error", Resources.RolesResource.e_FetchPrivileges);
            }

            // store them in view data
            ViewData["RolePrivilege"] = dictPrivilege;
        }

        /// <summary>
        /// This method is used to determines whether [is global role] [the specified role id].
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <returns>
        /// 	<c>true</c> if [is global role] [the specified role id]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsGlobalRole(string roleId)
        {
            return roleId.StartsWith("GR$", StringComparison.OrdinalIgnoreCase)
                && !roleId.Equals(RoleConstants.ServiceAdmin, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// This is method is used to loading the other privileges for each entities
        /// </summary>
        /// <param name="dictEntityPrivilege">entities with privileges.</param>
        /// <returns>collection entities with privileges</returns>
        private static Dictionary<string, Dictionary<string, EntityPrivilege>> LoadOtherPrivileges(Dictionary<string, Dictionary<string, EntityPrivilege>> dictEntityPrivilege)
        {
            Dictionary<string, Dictionary<string, EntityPrivilege>> dictOtherEntityPrivilege = new Dictionary<string, Dictionary<string, EntityPrivilege>>();
            Dictionary<string, EntityPrivilege> dictOtherPrivilege;

            foreach (string strEntityKey in dictEntityPrivilege.Keys)
            {
                dictOtherPrivilege = new Dictionary<string, EntityPrivilege>();
                Dictionary<string, EntityPrivilege> dictEntity = new Dictionary<string, EntityPrivilege>();
                dictEntity = dictEntityPrivilege[strEntityKey];
                foreach (string strPrivilegeKey in dictEntity.Keys)
                {
                    if (strPrivilegeKey == "View_" + strEntityKey)
                    {
                        continue;
                    }
                    if (strPrivilegeKey == null)
                    {
                        continue;
                    }
                    if (strPrivilegeKey == "Edit_" + strEntityKey)
                    {
                        continue;
                    }
                    if (strPrivilegeKey == "Delete_" + strEntityKey)
                    {
                        continue;
                    }
                    if (strPrivilegeKey != "Add_" + strEntityKey)
                    {
                        dictOtherPrivilege.Add(strPrivilegeKey, dictEntity[strPrivilegeKey]);
                    }
                }

                if (dictOtherPrivilege.Keys.Count > 0)
                {
                    dictOtherEntityPrivilege.Add(strEntityKey, dictOtherPrivilege);
                }
            }

            return dictOtherEntityPrivilege;
        }

        #endregion

        #region DynamicVariableMapping
        /// <summary>
        /// This method is used to dynamic variable mapping based on the given role privilege identifier and data scope identifier.
        /// </summary>
        /// <param name="rolePrivilegeId">role privilege identifier.</param>
        /// <param name="datascopeId">data scope identifier.</param>
        /// <returns></returns>
        public ActionResult DynamicVariableMapping(string rolePrivilegeId, string datascopeId)
        {
            ViewData["RolePrivilegeId"] = rolePrivilegeId;
            ViewData["DynamicVariableList"] = DataAccessProxy.GetDynamicVariables(datascopeId);
            ViewData["DynamicDatascopeValues"] = PrivilegeProxy.GetDynamicDatascopeValues(rolePrivilegeId, datascopeId);
            return View();
        }

        /// <summary>
        /// This method is used to save data scope value.
        /// </summary>
        /// <param name="dynamicDatascopeValueId">dynamic data scope value.</param>
        /// <param name="dynamicVariableId">dynamic variable identifier.</param>
        /// <param name="rolePrivilegeId">role privilege identifier.</param>
        /// <param name="value">value</param>
        /// <returns></returns>
        public JsonResult SaveDataScopeValue(string dynamicDatascopeValueId, string dynamicVariableId, string rolePrivilegeId, string value)
        {
            string errorMessage = Resources.RolesResource.e_SaveDatascopeValue;
            try
            {

                DynamicDatascopeValue dynamicDatascopeValue = new DynamicDatascopeValue();
                if (!string.IsNullOrEmpty(dynamicDatascopeValueId))
                {
                    dynamicDatascopeValue.DynamicDatascopeValueId = dynamicDatascopeValueId;
                }
                dynamicDatascopeValue.DynamicVariableId = dynamicVariableId;
                dynamicDatascopeValue.RolePrivilegeId = rolePrivilegeId;
                dynamicDatascopeValue.Value = value;

                if (string.IsNullOrEmpty(dynamicDatascopeValueId))
                {
                    PrivilegeProxy.AddDynamicDatascopeValue(dynamicDatascopeValue);
                }
                else
                {
                    PrivilegeProxy.UpdateDynamicDatascopeValue(dynamicDatascopeValue);
                }
                return Json(new { Success = Resources.RolesResource.s_Saved });
            }
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, DefaultPolicy);
                errorMessage = privilegeException.Message;
            }
            return Json(new { Error = errorMessage });
        }

        #endregion

        #region Private members

        private string policyName = "GlobalExceptionLogger";

        private string statusMessage = string.Empty;
        private string statusMessageSuccess = string.Empty;

        private const string viewFieldPrefix = "ViewField_";

        private const string editFieldPrefix = "EditField_";

        private const string viewEntityPrefix = "View_";

        private const string editEntityPrefix = "Edit_";

        public ActionResult DefaultData()
        {
            return RedirectToAction("Index", "Home");
        }
        /// <summary>
        /// This method is sued to get all field data type and bind to dropdown list
        /// </summary>
        /// <param name="selectedValue">selected value.</param>
        private void PopulateTypeList(string selectedValue)
        {
            Dictionary<string, FieldDataType> dicFieldDataType = null;
            try
            {
                dicFieldDataType = DataManagementProxy.GetAllEntityFieldType();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
            }
            //Set type list to DataType dropdown
            if (dicFieldDataType != null && dicFieldDataType.Count > 0)
                ViewData["TypeID"] = new SelectList((IEnumerable)dicFieldDataType.Values.ToList(), "TypeIdentifier", "TypeName", selectedValue);
            else
                ViewData["TypeID"] = new SelectList((IEnumerable)new Dictionary<string, FieldDataType>().Values.ToList(), "TypeIdentifier", "TypeName");
        }

        /// <summary>
        /// This method is used to get pickup list and bind to pickup list dropdown
        /// </summary>
        /// <param name="selectedValue">selected value.</param>
        private void PopulatePickUpList(string selectedValue)
        {
            List<PickupList> pickUpList = null;
            //Create empty Id list to show "Select pickup list"
            PickupList pickUp = new PickupList();
            pickUp.Id = string.Empty;
            pickUp.Name = "--Select PickUp List--";
            try
            {
                //Get the pickup list for logged in tenant
                pickUpList = PickupListProxy.GetPickupLists(this.TenantId);
                if (pickUpList != null && pickUpList.Count > 0)
                {
                    pickUpList.Sort(delegate(PickupList p1, PickupList p2) { return p1.Name.CompareTo(p2.Name); });
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
            }
            catch (ArgumentNullException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
            }
            if (pickUpList == null)
                pickUpList = new List<PickupList>();
            //Add empt Id list to pickuplist at the index 0 
            pickUpList.Insert(0, pickUp);
            ViewData["PickupListId"] = new SelectList((IEnumerable)pickUpList, "Id", "Name", selectedValue);
        }

        /// <summary>
        /// This method is used to validate Entity field Identifier and field name
        /// </summary>
        /// <param name="entityFieldMetaData">entity field meta data.</param>
        private void ValidateFields(EntityFieldMetaData entityFieldMetaData)
        {
            if (string.IsNullOrEmpty(entityFieldMetaData.EntityFieldIdentifier) || string.IsNullOrEmpty(entityFieldMetaData.EntityFieldIdentifier.Trim()))
                ModelState.AddModelError("EntityFieldIdentifier", "*");
            if (string.IsNullOrEmpty(entityFieldMetaData.Name) || string.IsNullOrEmpty(entityFieldMetaData.Name.Trim()))
                ModelState.AddModelError("Name", "Just");
            if (string.IsNullOrEmpty(entityFieldMetaData.TypeID))
                ModelState.AddModelError("TypeID", "*");
            if (entityFieldMetaData.TypeID == DataFieldType.PickUpField)
                if (string.IsNullOrEmpty(entityFieldMetaData.PickupListId))
                    ModelState.AddModelError("PickupListId", "*");
        }

        /// <summary>
        /// This method is sued to get the available data scopes for the given entity.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        private void GetAllDataScopeForEntity(string entityId)
        {
            string[] strEntities = new string[] { entityId };

            //DataScope for each entities
            Dictionary<string, Dictionary<string, DataScope>> dictEntityDataScope = new Dictionary<string, Dictionary<string, DataScope>>();
            dictEntityDataScope = DataAccessProxy.GetAllDataScopeBasedonEntity(strEntities);

            Dictionary<string, DataScope> dictDataScopes = new Dictionary<string, DataScope>();
            if (dictEntityDataScope.Keys.Count > 0 && dictEntityDataScope.ContainsKey(entityId))
                dictDataScopes = dictEntityDataScope[entityId];

            List<DataScope> lstDataScope = new List<DataScope>(dictDataScopes.Values.ToList());
            lstDataScope.Insert(0, new DataScope { Id = "-1", Name = "-All-" });
            ViewData["DataScope"] = lstDataScope;
        }

        #endregion

        #region Public members

        /// <summary>
        /// This is GET form method for ManageField view.
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="entityId"></param>
        /// <param name="statusMessage"></param>
        /// <returns></returns>
        public ActionResult ManageField(string fieldId, string entityId)
        {
            //Set the passed entityId and fieldId in view
            ViewData["EntityId"] = entityId;
            ViewData["fieldId"] = fieldId;
            EntityFieldMetaData entityFieldMetaData = null;
            try
            {
                //Get entity field meta data for passed entityId,fieldId and logged in tenant
                if (!string.IsNullOrEmpty(fieldId))
                    entityFieldMetaData = DataManagementProxy.GetEntityFieldMetaData(entityId, this.TenantId, fieldId);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.RolesResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.RolesResource.e_ParameterEmptyOrNull;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_field_metadata;
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            if (entityFieldMetaData != null)
            {
                //Fill the dropdown list with saved Ids as selected values
                this.PopulateTypeList(entityFieldMetaData.TypeID);
                this.PopulatePickUpList(entityFieldMetaData.PickupListId);
                return View(entityFieldMetaData);
            }
            else
            {
                //Fill the dropdown list with empty Id as selected values
                this.PopulateTypeList(string.Empty);
                this.PopulatePickUpList(string.Empty);
                return View(new EntityFieldMetaData());
            }

        }

        /// <summary>
        /// This is POST form method for managefield view.
        /// This is used to add or update entity field metadata.
        /// </summary>
        /// <param name="formCollection"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManageField(FormCollection formCollection)
        {
            //Get entityId and fieldId from hiddenfields in formcollection
            string entityId = formCollection["EntityId"];
            string fieldId = formCollection["FieldId"];

            string statusMessage = string.Empty;
            bool savedStatus = false;
            bool isFieldIdentifierUnique = true;
            EntityFieldMetaData entityFieldMetaData = new EntityFieldMetaData();
            try
            {
                //Update model with entity field metadata
                UpdateModel(entityFieldMetaData);
                //Validate the mandatory fields
                this.ValidateFields(entityFieldMetaData);
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(fieldId))
                    {
                        //Check the unquiness of field Identifier
                        isFieldIdentifierUnique = DataManagementProxy.IsFieldIdentifierUnique(entityFieldMetaData.EntityFieldIdentifier);
                        if (!isFieldIdentifierUnique)
                            statusMessage = Resources.DataResource.e_fieldidentifier_duplicate;
                    }
                    if (isFieldIdentifierUnique)
                    {
                        //Save entered entity field metadata with passed entityId and logged in tenantId
                        savedStatus = DataManagementProxy.SaveEntityFieldMetaData(entityId, this.TenantId, entityFieldMetaData);
                        if (!savedStatus)
                            statusMessage = Resources.DataResource.e_saved;
                    }
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.DataResource.e_NullParms;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.DataResource.e_NullParms;
            }
            catch (FieldNameUniquenessException fieldNameUniquenessException)
            {
                ExceptionService.HandleException(fieldNameUniquenessException, policyName);
                statusMessage = Resources.DataResource.e_fieldname_duplicate;
            }
            catch (InvalidFieldTypeException invalidFieldTypeException)
            {
                ExceptionService.HandleException(invalidFieldTypeException, policyName);
                statusMessage = Resources.DataResource.e_invalide_datatype;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_saving_field_metadata;
            }
            catch (InvalidOperationException invalidOperationException)
            {
                ExceptionService.HandleException(invalidOperationException, policyName);
                this.ValidateFields(entityFieldMetaData);
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            //Set entityId and fieldId to hidden field
            ViewData["EntityId"] = entityId;
            if (string.IsNullOrEmpty(Convert.ToString(ViewData["fieldId"], CultureInfo.InvariantCulture)))
                ViewData["fieldId"] = fieldId;
            if (entityFieldMetaData != null)
            {
                //Fill the dropdown list with saved Ids as selected values
                this.PopulateTypeList(entityFieldMetaData.TypeID);
                this.PopulatePickUpList(entityFieldMetaData.PickupListId);
            }
            else
            {
                //Fill the dropdown list with empty Id as selected values
                this.PopulateTypeList(string.Empty);
                this.PopulatePickUpList(string.Empty);
            }
            if (savedStatus)
            {
                //If saved successfuly,then redirect to FieldList
                return RedirectToAction("FieldList", new { entityId = entityId });
            }
            return View(entityFieldMetaData);
        }

        /// <summary>
        /// This is GET form method for EntityList view.
        /// This is used to fetch all entities.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult EntityList()
        {
            Dictionary<string, EntityMetaData> dicEntityMetaData = new Dictionary<string, EntityMetaData>();
            try
            {
                //Get all entity meta data
                dicEntityMetaData = DataManagementProxy.GetAllEntityMetaData();
                if (dicEntityMetaData != null && dicEntityMetaData.Count > 0)
                    ViewData["EntityMetaData"] = dicEntityMetaData.Values.ToList();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_entitylist;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_entitylist;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_entitylist;
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            return View();
        }

        /// <summary>
        /// This is GET form method for FieldList view.
        /// This is used to fetch all extn field metadata for selected entityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult FieldList(string entityId)
        {
            Dictionary<string, EntityFieldMetaData> dicEntityFieldMetaData = new Dictionary<string, EntityFieldMetaData>();
            try
            {
                //Get all fields for passed entityId and logged in tenant Id

                dicEntityFieldMetaData = DataManagementProxy.GetExtendedFields(entityId, this.TenantId);
                if (dicEntityFieldMetaData != null && dicEntityFieldMetaData.Count > 0)
                    ViewData["DicEntityFieldMetaData"] = dicEntityFieldMetaData.Values.ToList();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.RolesResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.RolesResource.e_ParameterEmptyOrNull;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_fieldlist;
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            return View();
        }

        /// <summary>
        /// This is GET form method for FieldList view.
        /// This is used to fetch all extn field metadata for selected entityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult BaseFieldList(string entityId)
        {
            Dictionary<string, EntityFieldMetaData> dicEntityFieldMetaData = new Dictionary<string, EntityFieldMetaData>();
            try
            {
                //Get all base field for given entityId
                dicEntityFieldMetaData = DataManagementProxy.GetBaseFields(entityId);
                if (dicEntityFieldMetaData != null && dicEntityFieldMetaData.Count > 0)
                    ViewData["DicEntityFieldMetaData"] = dicEntityFieldMetaData.Values.ToList();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                ModelState.AddModelError("StatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                ModelState.AddModelError("StatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_basefield;
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            return View();
        }

        /// <summary>
        /// To delete selected field metadata and return back to Field List
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="entityId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult DeleteEntityField(string fieldId, string entityId)
        {
            try
            {
                //Delete entity field metadata for given entityId,fieldId and logged in tenantId
                bool deletedStatus = DataManagementProxy.DeleteEntityFieldMetaData(entityId, this.TenantId, fieldId);
                if (deletedStatus)
                    statusMessageSuccess = Resources.DataResource.s_deleted_successfully;
                else
                    statusMessage = Resources.DataResource.e_deleted;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.DataResource.e_deleted;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.DataResource.e_deleted;
            }
            catch (ForeignKeyException foreignKeyException)
            {
                ExceptionService.HandleException(foreignKeyException, policyName);
                statusMessage = Resources.DataResource.e_foreignkey_exception;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_deleting_field_metadata;
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            if (!string.IsNullOrEmpty(statusMessageSuccess))
                ModelState.AddModelError("StatusMessageSuccess", statusMessageSuccess);
            this.FieldList(entityId);
            return View("FieldList");
        }


        /// <summary>
        /// Page loading
        /// This is GET form method for Field Privileges view.
        /// This is used to fetch all the field metadata for selected entityId
        /// </summary>
        /// <param name="roleId">roleId</param>
        /// <param name="moduleid">moduleid</param>
        /// <param name="entityId">entityId</param>
        /// <returns></returns>
        public ActionResult FieldPrivilege(string roleId, string moduleid, string entityId)
        {
            privileges(roleId, moduleid, entityId);
            return View();
        }

        /// <summary>
        /// This method is used to add field privileges to role
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object FieldPrivilegeDetails()
        {
            string roleId = string.Empty;
            string statusMessage = string.Empty;
            string entityId = Request.Form["hfEntityid"].ToString();

            Dictionary<string, EntityFieldMetaData> dicEntityFieldMetaData = new Dictionary<string, EntityFieldMetaData>();

            try
            {
                //Get all fields for passed entityId and logged in tenant Id
                dicEntityFieldMetaData = DataManagementProxy.GetFields(entityId, this.TenantId);
                if (dicEntityFieldMetaData != null && dicEntityFieldMetaData.Count > 0)
                    ViewData["DicEntityFieldMetaData"] = dicEntityFieldMetaData.Values.ToList();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_fieldlist;
            }

            //Get Role Name and Role id from form
            if (!string.IsNullOrEmpty(Request.Form["RoleName"]))
            {
                roleId = Request.Form["RoleName"].ToString();

                List<RolePrivilege> lstRolePrivilege = new List<RolePrivilege>();

                //Adds missing privileges
                UpdatePrivilege(entityId, roleId);

                int iFlag = 0;
                foreach (EntityFieldMetaData FieldMetadata in ((IEnumerable<EntityFieldMetaData>)ViewData["DicEntityFieldMetaData"]))
                {
                    string ViewPermission = viewFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();
                    string EditPermission = editFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();
                    iFlag = 0;

                    if (!string.IsNullOrEmpty(Request.Form[ViewPermission]) && Request.Form["ViewCheckbox" + FieldMetadata.EntityFieldIdentifier].ToString() != "false")
                    {
                        RolePrivilege rpRolePrivilege = new RolePrivilege();
                        rpRolePrivilege.RoleId = roleId;
                        rpRolePrivilege.TenantId = this.TenantId;
                        string strdataScopeId = Request.Form[ViewPermission].ToString();
                        if (strdataScopeId == "-1")
                            strdataScopeId = null;
                        rpRolePrivilege.PrivilegeId = ViewPermission;
                        rpRolePrivilege.DataScopeId = strdataScopeId;
                        lstRolePrivilege.Add(rpRolePrivilege);
                    }
                    else
                    {
                        iFlag = 1;
                        DeleteFieldPrivilege(roleId, ViewPermission);
                    }
                    if (!string.IsNullOrEmpty(Request.Form[EditPermission]) && Request.Form["EditCheckbox" + FieldMetadata.EntityFieldIdentifier].ToString() != "false")
                    {
                        RolePrivilege rpRolePrivilege = new RolePrivilege();
                        rpRolePrivilege.RoleId = roleId;
                        rpRolePrivilege.TenantId = this.TenantId;
                        string strdataScopeId = Request.Form[EditPermission].ToString();
                        if (strdataScopeId == "-1")
                            strdataScopeId = null;
                        rpRolePrivilege.PrivilegeId = EditPermission;
                        rpRolePrivilege.DataScopeId = strdataScopeId;
                        lstRolePrivilege.Add(rpRolePrivilege);
                    }
                    else
                    {
                        iFlag = 1;
                        DeleteFieldPrivilege(roleId, EditPermission);
                    }
                }

                if (lstRolePrivilege.Count > 0)
                {
                    try
                    {
                        PrivilegeProxy.SavePrivilegesToRole(this.TenantId, lstRolePrivilege);
                        ModelState.AddModelError("success", Resources.DataResource.s_datasaved);
                    }
                    //Catch DB Exception and throw as Privilege Exception
                    catch (DbException dbException)
                    {
                        ExceptionService.HandleException(dbException, policyName);
                        ModelState.AddModelError("Error", Resources.DataResource.e_Addprivilige);
                    }
                    //Catch Argument Exception and throw as Privilege Exception
                    catch (ArgumentException argumentException)
                    {
                        ExceptionService.HandleException(argumentException, policyName);
                        ModelState.AddModelError("Error", Resources.DataResource.e_Addprivilige);
                    }
                    catch (PrivilegeException privilegeException)
                    {
                        ExceptionService.HandleException(privilegeException, policyName);
                        ModelState.AddModelError("Error", Resources.DataResource.e_PriviligeExist);
                    }
                }
                else if (iFlag == 1)
                {
                    ModelState.AddModelError("success", Resources.DataResource.s_Update);
                }
                //Fetching the Role Details
                string[] RoleArray = new string[] { roleId };
                Dictionary<string, RolePrivilege> dictPrivilege = new Dictionary<string, RolePrivilege>();
                try
                {
                    dictPrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, RoleArray, null);
                }
                //Catch DB Exception and throw as Privilege Exception
                catch (DbException dbException)
                {
                    ExceptionService.HandleException(dbException, policyName);
                }
                //Catch License Exception and throw as Privilege Exception
                catch (LicenseException licenseException)
                {
                    ExceptionService.HandleException(licenseException, policyName);
                }
                //Catch Argument Exception and throw as Privilege Exception
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                }
                ViewData["RolePrivilege"] = dictPrivilege;
            }
            else
            {
                ModelState.AddModelError("valRoleName", "*");
                ModelState.AddModelError("Error", Resources.DataResource.e_SelectRole);
            }


            //Loading all the DataScopes for the Tenant.
            GetAllDataScopeForEntity(entityId);

            //Getting all the roles
            List<Role> Roles = new List<Role>();
            try
            {
                Roles = RoleProxy.GetTenantRoleDetails(this.TenantId);
            }
            //Catch DB Exception and throw as Role Exception
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, policyName);
            }
            //Catch Argument Exception and throw as Role Exception
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
            }

            //Fetching Global Roles
            List<Role> GlobalRoles = new List<Role>();
            try
            {
                GlobalRoles = RoleProxy.GetGlobalRoleDetails();
            }
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, policyName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
            }
            //Binding all the roles
            Roles.AddRange(GlobalRoles);

            Roles.Insert(0, new Role { RoleId = "", RoleName = "- Select User Role -" });
            ViewData["Roles"] = Roles;
            ViewData["SelectedRole"] = roleId;
            ViewData["strEntityid"] = entityId;
            ViewData["moduleId"] = Request.Form["hfmoduleId"].ToString();
            //ViewData["Roles"] = new SelectList((IEnumerable)Roles, "RoleId", "RoleName");
            return View("PartialFieldPrivilege");
        }

        /// <summary>
        /// Gets all privilege and returns list of PrivilegeIds
        /// </summary>
        /// <param name="roleId">The Role Identifier</param>
        /// <returns></returns>
        private static List<string> GetAllPrivilege(string roleId)
        {
            //List<Privilege> privilegeList = null;
            //List<string> privilegeIds = new List<string>();
            //privilegeList = PrivilegeProxy.GetAllPrivileges();
            //if (privilegeList != null)
            //{
            //    foreach (Privilege privilege in privilegeList)
            //        if (!privilegeIds.Contains(privilege.Id))
            //            privilegeIds.Add(privilege.Id);
            //}
            //return privilegeIds;

            var dictPrivilege = PrivilegeProxy.GetPrivilegesForRoles(UserIdentity.TenantID, new string[] { roleId }, null);

            if (dictPrivilege == null || dictPrivilege.Count < 1)
                return null;

            return dictPrivilege.Values.Select(p => p.PrivilegeId).ToList();
        }

        private void UpdatePrivilege(string entityId, string roleId = null)
        {
            List<string> privilegeIds = null;

            privilegeIds = GetAllPrivilege(roleId);

            //Missing Privileges
            List<Privilege> missingPrivileges = new List<Privilege>();
            //Updating Privilege
            foreach (EntityFieldMetaData FieldMetadata in ((IEnumerable<EntityFieldMetaData>)ViewData["DicEntityFieldMetaData"]))
            {
                string ViewPermission = viewFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();
                string EditPermission = editFieldPrefix + FieldMetadata.EntityFieldIdentifier.ToString();

                if (privilegeIds != null && !privilegeIds.Contains(ViewPermission))
                {
                    Privilege privilege = new Privilege();
                    privilege.Id = privilege.Name = privilege.Description = ViewPermission;
                    privilege.BasePrivilege = viewEntityPrefix + entityId;
                    missingPrivileges.Add(privilege);
                }

                if (privilegeIds != null && !privilegeIds.Contains(EditPermission))
                {
                    Privilege privilege = new Privilege();
                    privilege.Id = privilege.Name = privilege.Description = EditPermission;
                    privilege.BasePrivilege = editEntityPrefix + entityId;
                    missingPrivileges.Add(privilege);
                }
            }

            if (missingPrivileges.Count > 0)
                PrivilegeProxy.AddPrivileges(missingPrivileges);
        }

        /// <summary>
        /// Happends when role is changed
        /// Role Drop Down Selected Index change. 
        /// Loading the scopes for the field privileges
        /// </summary>
        /// <param name="roleId">roleId</param>
        /// <param name="moduleid">moduleid</param>
        /// <param name="entityId">EntityId</param>
        /// <returns></returns>
        public object FieldPrivilegeLoad(string roleId, string moduleid, string entityId)
        {
            privileges(roleId, moduleid, entityId);
            return PartialView("PartialFieldPrivilege");
        }

        /// <summary>
        /// Loading the Privlege page with details
        /// </summary>
        /// <param name="roleId">roleId</param>
        /// <param name="moduleid">moduleid</param>
        /// <param name="entityId">entityId</param>
        private void privileges(string roleId, string moduleid, string entityId)
        {
            if (string.IsNullOrEmpty(moduleid))
            {
                moduleid = Request.Form["hfmoduleId"].ToString();
            }
            try
            {
                //Get all fields for passed entityId and logged in tenant Id
                Dictionary<string, EntityFieldMetaData> dicEntityFieldMetaData = DataManagementProxy.GetFields(entityId, this.TenantId);
                if (dicEntityFieldMetaData != null && dicEntityFieldMetaData.Count > 0)
                    ViewData["DicEntityFieldMetaData"] = dicEntityFieldMetaData.Values.ToList();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, policyName);
                statusMessage = Resources.DataResource.e_datamanagement_getting_fieldlist;
            }
            //Get Role Name and Role id from form
            if (!string.IsNullOrEmpty(roleId))
            {
                string[] RoleArray = new string[] { roleId };
                Dictionary<string, RolePrivilege> dictPrivilege = new Dictionary<string, RolePrivilege>();
                try
                {
                    dictPrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, RoleArray, null);
                }
                //Catch DB Exception and throw as Privilege Exception
                catch (DbException dbException)
                {
                    ExceptionService.HandleException(dbException, policyName);
                }
                //Catch License Exception and throw as Privilege Exception
                catch (LicenseException licenseException)
                {
                    ExceptionService.HandleException(licenseException, policyName);
                }
                //Catch Argument Exception and throw as Privilege Exception
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                }
                ViewData["RolePrivilege"] = dictPrivilege;
            }

            List<Role> Roles = new List<Role>();
            try
            {
                Roles = RoleProxy.GetTenantRoleDetails(this.TenantId);
            }
            //Catch DB Exception and throw as Role Exception
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, policyName);
            }
            //Catch Argument Exception and throw as Role Exception
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
            }

            //Fetching Global Roles
            List<Role> GlobalRoles = new List<Role>();
            try
            {
                GlobalRoles = RoleProxy.GetGlobalRoleDetails();
            }
            catch (DbException dbException)
            {
                ExceptionService.HandleException(dbException, policyName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
            }
            //Binding all the roles
            Roles.AddRange(GlobalRoles);

            Roles.Insert(0, new Role { RoleId = "", RoleName = "- Select User Role -" });
            ViewData["Roles"] = Roles;
            ViewData["SelectedRole"] = roleId;

            //Loading all the DataScopes for the Tenant.
            GetAllDataScopeForEntity(entityId);

            ViewData["strEntityid"] = entityId;
            ViewData["moduleId"] = moduleid;
        }
        #endregion

    }
}
