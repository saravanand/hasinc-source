﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.QueryBuilderLibrary.ServiceProxies;
using CelloSaaS.Reporting.DataSources;
using CelloSaaS.Reporting.DataSources.Model;
using CelloSaaS.Reporting.DataSources.Model.ViewModel;
using CelloSaaS.Reporting.DataSources.ServiceContracts;
using CelloSaaS.Reporting.DataSources.ServiceProxies;
using Resources;

namespace CelloSaaSApplication.Controllers
{
    using CelloSaaS.Reporting.DataSources.Helpers;
    using CelloSaaS.View;
    /// <summary>
    /// This class is responsible for data source management.
    /// </summary>
    public class DataSourcesController : CelloController
    {
        /// <summary>
        /// This method is used to get the manage data source view based on the given edit data source identifier and view object identifier.
        /// </summary>
        /// <param name="editDataSourceId">edit data source identifier.</param>
        /// <param name="viewObjectId">view object identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public PartialViewResult ManageDataSources(string editDataSourceId, string viewObjectId)
        {
            if (!string.IsNullOrEmpty(editDataSourceId))
            {
                DataSource dataSource = (DataSource)DataSourceProxy.GetSource(editDataSourceId, UserIdentity.TenantID);

                ViewData["dataSource"] = dataSource;

                SourceContentViewModel sourceContentViewModel = null;

                if (dataSource.DataSourceContent != null && !string.IsNullOrEmpty(dataSource.DataSourceContent.Content))
                {
                    sourceContentViewModel = (SourceContentViewModel)SerializationHelper.DeSerializeObject<SourceContentViewModel>(dataSource.DataSourceContent.Content);
                    ViewData["sourceContentModel"] = sourceContentViewModel;
                }
                GetSourceContentTypes(dataSource.SourceTypeId);

                if (dataSource.SourceTypeId.Equals(DataSourceConstants.BuiltInQuerySource, StringComparison.OrdinalIgnoreCase))
                {
                    GetPreBuiltQueries(dataSource.SourceContentId);
                }
                else
                {
                    GetPreBuiltQueries();
                }
            }
            else
            {
                GetSourceContentTypes();
                GetPreBuiltQueries();
            }
            ViewData["viewObjectId"] = viewObjectId;
            return PartialView("ManageDataSources");
        }

        /// <summary>
        /// This method id used to get the manage data source view based on the source content view model.
        /// </summary>
        /// <param name="scvm">source content view model.</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ManageDataSources(SourceContentViewModel scvm)
        {
            GetSourceContentTypes();
            GetPreBuiltQueries();
            FillDataSources();

            if (scvm != null && !string.IsNullOrEmpty(scvm.SourceContentTypes))
            {
                if (scvm.SourceContentTypes.Equals("-1"))
                {
                    ViewData["statusMessage"] = DataSourcesResource.e_InvalidSourceContentType;
                    return PartialView("ManageDataSources");
                }

                DataSource ds = new DataSource();

                ds.Id = scvm.DataSource_Id;
                ds.Name = scvm.DataSource_Name;
                ds.SourceTypeId = scvm.SourceContentTypes;
                ds.TenantId = UserIdentity.TenantID;

                // in case of a pre-builtin query, we have the query id as the contentid
                if (scvm.SourceContentTypes.Equals(DataSourceConstants.BuiltInQuerySource, StringComparison.OrdinalIgnoreCase))
                {
                    ds.DataSourceContent = null;
                    if (scvm.BuiltInQuerySources.Equals("-1") || scvm.BuiltInQuerySources.Equals("undefined", StringComparison.OrdinalIgnoreCase))
                    {
                        throw new DataSourceException(DataSourcesResource.e_InvalidSource);
                    }
                    else
                    {
                        ds.SourceContentId = scvm.BuiltInQuerySources;
                    }
                }
                else // in other cases, the query id is not the content id
                {
                    ds.DataSourceContent = new SourceContent
                    {
                        ContentId = scvm.SourceContent_Id,
                        Content = SerializationHelper.SerializeObject(scvm)
                    };

                    ds.SourceContentId = scvm.SourceContent_Id;
                    ds.DataSourceContent.UpdatedBy = ds.DataSourceContent.CreatedBy = UserIdentity.UserId;
                    ds.DataSourceContent.UpdatedOn = ds.DataSourceContent.CreatedOn = DateTime.Now;
                    ds.DataSourceContent.Status = true;
                }

                // do the insertion here
                try
                {
                    if (string.IsNullOrEmpty(scvm.DataSource_Id))
                    {
                        ds.CreatedBy = UserIdentity.UserId;
                        ds.CreatedOn = DateTime.Now;
                        ds.Status = true;
                        string dataSourceId = DataSourceProxy.AddSource(ds);
                        scvm.DataSource_Id = dataSourceId;

                        if (!string.IsNullOrEmpty(dataSourceId))
                        {
                            ViewData["statusMessage"] = DataSourcesResource.m_SaveSuccess;
                        }
                        else
                        {
                            ViewData["statusMessage"] = DataSourcesResource.e_SaveError;
                        }
                    }
                    else
                    {
                        ds.UpdatedBy = UserIdentity.UserId;
                        ds.UpdatedOn = ds.UpdatedOn;
                        if (DataSourceProxy.UpdateSource(ds) > 0)
                        {
                            ViewData["statusMessage"] = DataSourcesResource.m_SaveSuccess;
                        }
                        else
                        {
                            ViewData["statusMessage"] = DataSourcesResource.e_SaveError;
                        }
                    }
                }
                catch (InvalidSourceContentException iscex)
                {
                    ModelState.AddModelError("statusMessage", iscex.Message);
                    ViewData["statusMessage"] = iscex.Message;
                    return PartialView("ManageDataSources");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("statusMessage", ex.Message);
                    ViewData["statusMessage"] = ex.Message;
                    return PartialView("ManageDataSources");
                }
            }
            FillDataSources(scvm.DataSource_Id);
            return PartialView("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewObjectId">view object identifier.</param>
        /// <param name="dataSourceId">data source identifier.</param>
        /// <returns></returns>
        public PartialViewResult Index(string viewObjectId, string dataSourceId)
        {
            FillDataSources(dataSourceId);

            ViewData["viewObjectId"] = string.IsNullOrEmpty(viewObjectId)
                                        ? Guid.NewGuid().ToString()
                                        : viewObjectId;


            return PartialView("Index");
        }

        /// <summary>
        /// This method is used to retrieve the source parameters.
        /// </summary>
        /// <param name="viewObjectId">view object identifier.</param>
        /// <param name="paramSourceContent">parameter source content.</param>
        /// <param name="ContentType">content type</param>
        /// <returns></returns>
        public PartialViewResult FetchSourceParameters(string viewObjectId, string paramSourceContent, string ContentType)
        {
            List<string> contentParameters = null;

            if (ContentType.Equals(DataSourceConstants.StoredProcedureSource, StringComparison.OrdinalIgnoreCase))
            {
                contentParameters = QueryMetaDataProxy.DiscoverSqlSpParameters(paramSourceContent, DBHelpers.GetCelloSaaSConnectionStringName);
                //return Json(contentParameters.Distinct(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                MatchCollection mc = Regex.Matches(paramSourceContent, RegexHelper.QueryParametersPattern, RegexOptions.IgnoreCase);

                if (mc == null || mc.Count < 1)
                {
                    return null;
                }

                contentParameters = new List<string>();

                foreach (var m in mc)
                {
                    contentParameters.Add(m.ToString());
                }
            }
            if (contentParameters == null || contentParameters.Count < 1)
            {
                return null;
            }

            List<Parameters> parameters = new List<Parameters>();

            foreach (var param in contentParameters.Distinct())
            {
                parameters.Add(new Parameters { ParamName = param, ParamValue = null });
            }

            ViewData["parameters"] = parameters;

            return PartialView("SourceContentParameters");
        }

        /// <summary>
        /// This method is used to delete the data source.
        /// </summary>
        /// <param name="viewObjectId">view object identifier.</param>
        /// <param name="deleteDataSourceId">delete data source identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult DeleteSource(string viewObjectId, string deleteDataSourceId)
        {
            FillDataSources();
            if (string.IsNullOrEmpty(deleteDataSourceId))
            {
                return PartialView("Index");
            }

            if (DataSourceProxy.DeleteSource(deleteDataSourceId, UserIdentity.TenantID) > 0)
            {
                ViewData["successMessage"] = DataSourcesResource.m_DeleteSuccess;
                TempData["statusMessage"] = DataSourcesResource.m_DeleteSuccess;
            }
            else
            {
                ViewData["successMessage"] = DataSourcesResource.e_DeleteError;
            }

            return PartialView("Index");
        }

        /// <summary>
        /// This method is used to get pre built queries.
        /// </summary>
        /// <param name="selectedQueryId">selected query identifier.</param>
        private void GetPreBuiltQueries(string selectedQueryId = null)
        {
            var queries = QueryDetailsProxy.GetShallowQueryDetails(UserIdentity.TenantID);

            var preBuiltQueries = new Dictionary<string, string>();

            preBuiltQueries.Add("-1", "--Choose A BuiltInQuery--");

            foreach (var existingQuery in queries)
            {
                preBuiltQueries.Add(existingQuery.Key, existingQuery.Value);
            }

            if (string.IsNullOrEmpty(selectedQueryId))
            {
                selectedQueryId = "-1";
            }
            ViewData["BuiltInQuerySources"] = new SelectList(preBuiltQueries, "Key", "Value", selectedQueryId);
        }

        /// <summary>
        /// This method is used to fill data sources.
        /// </summary>
        /// <param name="dataSourceId">data source identifier.</param>
        private void FillDataSources(string dataSourceId = null)
        {
            Dictionary<string, IObjectSource> dataSourcesList = DataSourceProxy.GetAllSources(UserIdentity.TenantID);

            var resultList = new Dictionary<string, string>();
            resultList.Add("-1", "--Choose A DataSource--");

            foreach (var listItem in dataSourcesList)
            {
                resultList.Add(listItem.Key, listItem.Value.Name);
            }

            dataSourceId = string.IsNullOrEmpty(dataSourceId) ? "-1" : dataSourceId;

            ViewData["dataSources"] = new SelectList(resultList, "Key", "Value", dataSourceId);
        }

        /// <summary>
        /// This method is used to get the source content type.
        /// </summary>
        /// <param name="selectedTypeId">selected type identifier.</param>
        private void GetSourceContentTypes(string selectedTypeId = null)
        {
            Dictionary<string, string> SrcContentTypes = new Dictionary<string, string>
            {
	            {"-1","--Choose A SourceContent--"},
                {"295CE098-8B9B-454C-B65C-1FEC6F7B69EA","Text"},
                {"56B8A747-C55D-42A8-A801-2C66E9EC2AAC","Stored Procedure"},
                {"35782E2D-8FE9-4444-8C8D-D5F77855BE29","Built-In Query Source"}
            };

            if (string.IsNullOrEmpty(selectedTypeId))
            {
                selectedTypeId = "-1";
            }
            ViewData["SourceContentTypes"] = new SelectList(SrcContentTypes, "Key", "Value", selectedTypeId);
        }
    }
}
