using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.ServiceContracts.DataManagement;
using CelloSaaS.ServiceContracts.ViewManagement;
using CelloSaaS.ServiceProxies.Configuration;
using CelloSaaS.ServiceProxies.DataManagement;
using CelloSaaS.ServiceProxies.ViewManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for data view management.
    /// </summary>
    [HandleError]
    public class DataViewController : CelloSaaS.View.CelloController
    {
        private string TenantId = TenantContext.GetTenantId("_DataView");
        //private string TenantId = UserIdentity.TenantID;
        private const string PolicyName = "GlobalExceptionLogger";

        const string UpdateButton = "Update";
        const string CancelButton = "Cancel";
        private const string DefaultPolicy = "GlobalExceptionLogger";

        #region GlobalFieldList

        //[CelloSaaS.View.RequiresPermissionActionFilter(AuthorizationRule = "P:ProvisionTenant")] 
        /// <summary>
        /// This method is used to get global field list.
        /// </summary>
        /// <returns></returns>
        public ActionResult GlobalFieldList()
        {
            try
            {
                List<DataViewFieldMetaData> fieldList = null;
                // Get all global fields available for the tenant
                Dictionary<int, DataViewFieldMetaData> globalFields = ViewMetaDataProxy.GetGlobalFields(TenantId);
                // Convert the dictionary to list
                if (globalFields != null)
                    fieldList = globalFields.Values.ToList();
                return View(fieldList);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_GetGlobalField);
            }
            return View();
        }

        #endregion GlobalFieldList

        #region GlobalFieldDetails

        /// <summary>
        /// This method is used to get global field details based on the global field identifier.
        /// </summary>
        /// <param name="passedGlobalFieldID">global field identifier.</param>
        /// <returns></returns>
        public ActionResult GlobalFieldDetails(int passedGlobalFieldID)
        {
            try
            {
                DataViewFieldMetaData globalFieldProperties = null;
                // Get the global field properties for the passed tenantId and globalFieldId
                globalFieldProperties = ViewMetaDataProxy.GetGlobalFieldProperties(this.TenantId, passedGlobalFieldID);

                // Load the View
                return View(globalFieldProperties);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_GetGlobalfieldProperties);
            }
            return View();
        }

        /// <summary>
        /// This method is used to get the global field details based on the field values and navigate.
        /// </summary>
        /// <param name="fieldValues">field values.</param>
        /// <param name="Navigate">navigation.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]   
        public ActionResult GlobalFieldDetails(DataViewFieldMetaData fieldValues, string Navigate)
        {
            try
            {
                // Check whether update/cancel button is called
                if (Navigate == UpdateButton)
                {
                    // Validate the form values
                    ValidateFormData(fieldValues);

                    // Check if any invalid data exists
                    if (ModelState.IsValid)
                    {
                        fieldValues.CreatedBy = UserIdentity.UserId;
                        fieldValues.UpdatedBy = UserIdentity.UserId;
                        // Update the global field properties
                        ViewMetaDataProxy.UpdateGlobalField(fieldValues, TenantId);

                        // If no exception happens during update, return to globalFieldList view
                        return RedirectToAction("GlobalFieldList", "DataView");
                    }
                    //else
                    //    // If invalid values are there, then load the same page
                    //    return View(fieldValues);
                }
                else
                {
                    // If cancel button is clicked, return to globalFieldList view
                    return RedirectToAction("GlobalFieldList", "DataView");
                }
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_UpdateGlobalField);
            }
            return View(fieldValues);
        }

        #endregion GlobalFieldDetails

        #region DataViewFieldList

        /// <summary>
        /// This method is used to get the data view list details.
        /// </summary>
        /// <param name="dataViewID">data view identifier.</param>        
        /// <param name="isDeleted">isDeleted (true/false).</param>
        /// <returns></returns>
        public ActionResult DataViewFieldList(string dataViewID, bool? isDeleted)
        {
            if (TempData["SuccessMessage"] != null && !string.IsNullOrEmpty(TempData["SuccessMessage"].ToString()))
            {
                ModelState.AddModelError("SuccessMessage", TempData["SuccessMessage"].ToString());
            }
            return LoadDataViewListDetails(dataViewID, string.Empty, isDeleted);
        }

        /// <summary>
        /// This method is used to manages the columns data view field list.
        /// </summary>
        /// <param name="dataViewID">data view identifier.</param>
        /// <param name="entityID">entity identifier.</param>
        /// <param name="isDeleted">isDeleted (true/false).</param>
        /// <returns></returns>
        public ActionResult ManageColumnsDataViewFieldList(string dataViewID, string entityID, bool? isDeleted)
        {
            return LoadDataViewListDetails(dataViewID, entityID, isDeleted);
        }

        /// <summary>
        /// This method is used to loads the date view list details.
        /// </summary>
        /// <param name="dataViewID">data view identifier.</param>
        /// <param name="entityID">entity identifier.</param>
        /// <param name="isDeleted">isDeleted (true/false).</param>
        /// <returns></returns>
        private ActionResult LoadDataViewListDetails(string dataViewID, string entityID, bool? isDeleted)
        {
            try
            {
                if (string.IsNullOrEmpty(dataViewID)) return HttpNotFound("Invalid request!");

                var extnSettingValue = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.TenantId, ModuleConfigurationConstant.EXTENDED_FIELD);

                ViewData["ExtnSettingValue"] = extnSettingValue;

                List<DataViewFieldMetaData> fieldMetaData = null;
                StringBuilder ordinalList = new StringBuilder();

                // Get dataView meta data for the passed dataView ID
                DataViewMetaData dataViewMetaData = ViewMetaDataProxy.GetDataViewMetaData(TenantId, dataViewID, entityID);

                if (dataViewMetaData == null) return HttpNotFound("Invalid request!");

                // Convert the available fields dictionary to list
                if (dataViewMetaData.AvailableFields != null)
                    fieldMetaData = dataViewMetaData.AvailableFields.Values.ToList();

                // Get all ordinals and save it in a with csv string variable.
                if (fieldMetaData != null)
                {
                    foreach (DataViewFieldMetaData fieldData in fieldMetaData)
                    {
                        if (fieldData.Ordinal <= 0)
                        {
                            continue;
                        }
                        ordinalList.Append(fieldData.Ordinal);
                        ordinalList.Append(",");
                    }
                }
                else
                {
                    // If fieldMetaData is null, assign to an empty list. 
                    //(If exception happens, null is passed to view. To differentiate, the exception scenario instance is created )
                    fieldMetaData = new List<DataViewFieldMetaData>();
                }

                // Remove the last comma character in the csv string
                if (ordinalList.Length > 0)
                    ordinalList.Remove(ordinalList.Length - 1, 1);

                // Save the following values to the view's hidden variable. Will be used during postback
                ViewData["OrdinalList"] = ordinalList.ToString();
                ViewData["DataViewID"] = dataViewID;
                ViewData["EntityID"] = dataViewMetaData.DataView.MainEntity;
                TempData["DataViewName"] = ViewData["DataViewName"] = dataViewMetaData != null && dataViewMetaData.DataView != null ? dataViewMetaData.DataView.Name : dataViewMetaData.DataView.DataViewID;
                ViewData["FieldList"] = fieldMetaData;//.AsPagination(page ?? 1, 10);

                // This value is used when post back happens in the same page during delete.
                // Based on the value, success message is displayed
                ViewData["IsDeleted"] = isDeleted;

                // Load the View
                return View(ViewData["FieldList"]);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }

            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_GetDataViewMetaData);
            }
            return View();
        }

        /// <summary>
        /// This method is used to delete the extended fields.
        /// </summary>
        /// <param name="deleteFieldID">field identifier.</param>
        /// <param name="id">identifier.</param>
        /// <param name="passedDataViewID">data view identifier.</param>
        /// <param name="passedEntityID">entity identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteExtendedField(string deleteFieldID, int id, string passedDataViewID, string passedEntityID)
        {
            try
            {
                // Delete the extendedField
                bool isDeleted = ViewMetaDataProxy.DeleteExtendedField(id, this.TenantId, passedDataViewID);
                // Reload the page with isDeleted value set based on which the success message is displayed
                return RedirectToAction("DataViewFieldList", "DataView", new { dataViewID = passedDataViewID, entityID = passedEntityID, isDeleted = isDeleted });
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_DeleteExtendedField);
            }
            return RedirectToAction("DataViewFieldList", "DataView", new { dataViewID = passedDataViewID, entityID = passedEntityID, isDeleted = false });
        }

        #endregion DataViewFieldList

        #region DataViewList

        /// <summary>
        /// This method is used to get all data view.
        /// </summary>
        /// <returns></returns>
        public ActionResult DataViewList()
        {
            try
            {
                List<CelloSaaS.Model.ViewManagement.DataView> dataViews = ViewMetaDataProxy.GetDataViews(this.TenantId);
                //List<EntityMetaData> metaDataList = new List<EntityMetaData>();
                var allMetaData = DataManagementProxy.GetAllEntityMetaData(this.TenantId);
                ViewData["AllEntityMetadata"] = allMetaData;
                return View(dataViews);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_GetDataViews);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_GetEntityMetaData);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("Error", licenseException.Message);
            }
            return View();
        }

        /// <summary>
        /// This method is used to delete the virtual entity data view.
        /// </summary>
        /// <param name="dataViewId">The data view identifier.</param>
        /// <param name="mainEntity">entity.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="dataViewName">data view name</param>
        /// <returns></returns>
        public ActionResult DeleteVirtualEntityDataView(string dataViewId, string mainEntity, string tenantId, string dataViewName)
        {
            Guard.NullOrEmpty("dataViewId", dataViewId);
            Guard.NullOrEmpty("mainEntity", mainEntity);
            Guard.NullOrEmpty("tenantId", tenantId);
            try
            {
                ViewMetaDataProxy.DeleteDataView(dataViewId, tenantId, mainEntity);
                TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataViewResource.s_DataView_Deleted_Successfully, dataViewName);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                TempData["Error"] = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (ViewMetaDataException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("DataViewList");
        }

        /// <summary>
        /// This method is used to add the virtual entity data view.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddVirtualEntityDataView()
        {
            if (ViewData["MainEntity"] == null || string.IsNullOrEmpty(ViewData["MainEntity"].ToString()))
            {
                ViewData["MainEntity"] = string.Empty;
            }
            if (ViewData["DataViewID"] == null || string.IsNullOrEmpty(ViewData["DataViewID"].ToString()))
            {
                ViewData["DataViewID"] = string.Empty;
            }
            if (ViewData["DataViewName"] == null || string.IsNullOrEmpty(ViewData["DataViewName"].ToString()))
            {
                ViewData["DataViewName"] = string.Empty;
            }
            if (ViewData["DataViewDescription"] == null || string.IsNullOrEmpty(ViewData["DataViewDescription"].ToString()))
            {
                ViewData["DataViewDescription"] = string.Empty;
            }

            try
            {
                FillMainEntity();
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, PolicyName);
                ModelState.AddModelError("Error", Resources.DataResource.e_datamanagement_getting_entitylist);
            }
            return View();
        }

        /// <summary>
        /// This method is used to fill entity.
        /// </summary>
        private void FillMainEntity()
        {
            var dicEntityMetaData = DataManagementProxy.GetAllEntityMetaData();
            List<SelectListItem> lstEntity = new List<SelectListItem>();
            if (dicEntityMetaData != null && dicEntityMetaData.Count > 0)
            {
                lstEntity.Add(new SelectListItem { Value = "-1", Text = "- Select -" });

                foreach (KeyValuePair<string, EntityMetaData> dataView in dicEntityMetaData)
                {
                    if (!string.IsNullOrEmpty(dataView.Value.TenantId))
                    {
                        if (ViewData["MainEntity"] != null && !string.IsNullOrEmpty(ViewData["MainEntity"].ToString()) && ViewData["MainEntity"].ToString().Equals(dataView.Value.EntityIdentifier))
                        {
                            lstEntity.Add(new SelectListItem { Text = dataView.Value.EntityName, Value = dataView.Value.EntityIdentifier, Selected = true });
                        }
                        else
                        {
                            lstEntity.Add(new SelectListItem { Text = dataView.Value.EntityName, Value = dataView.Value.EntityIdentifier });
                        }
                    }
                }
            }
            ViewData["MainEntity"] = lstEntity;
        }

        /// <summary>
        /// This method is used to add virtual entity data view.
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddVirtualEntityDataView(FormCollection formCollection)
        {
            if (formCollection.Count > 0 && formCollection != null)
            {
                if (formCollection["DataView.MainEntity"] == "-1")
                {
                    ModelState.AddModelError("DataView.MainEntity", Resources.DataViewResource.e_SelectEntity);
                }
                else if (string.IsNullOrEmpty(formCollection["DataView.MainEntity"]))
                {
                    ModelState.AddModelError("DataView.MainEntity", Resources.DataViewResource.e_NullEntity);
                    ViewData["MainEntity"] = string.Empty;
                }
                else
                {
                    ViewData["MainEntity"] = formCollection["DataView.MainEntity"].ToString();
                }
                if (string.IsNullOrEmpty(formCollection["DataView.DataViewID"]))
                {
                    ModelState.AddModelError("DataView.DataViewID", Resources.DataViewResource.e_NullDataViewId);
                    ViewData["DataViewID"] = string.Empty;
                }
                else
                {
                    ViewData["DataViewId"] = formCollection["DataView.DataViewID"].ToString();
                    if (!Util.ValidateIdentifier(formCollection["DataView.DataViewID"].ToString()))
                    {
                        ModelState.AddModelError("DataView.DataViewID", Resources.DataViewResource.e_SpecialCharacterDataView);
                    }

                }
                if (string.IsNullOrEmpty(formCollection["DataView.Name"]))
                {
                    ModelState.AddModelError("DataView.Name", Resources.DataViewResource.e_DataViewName);
                    ViewData["DataViewName"] = string.Empty;
                }
                else
                {
                    ViewData["DataViewName"] = formCollection["DataView.Name"].ToString();
                }
                if (!string.IsNullOrEmpty(formCollection["DataView.Description"]))
                {
                    ViewData["DataViewDescription"] = formCollection["DataView.Description"].ToString();
                }
                else
                {
                    ModelState.AddModelError("DataView.Description", Resources.DataViewResource.e_DataViewDescription);
                    ViewData["DataViewDescription"] = string.Empty;
                }
                try
                {
                    FillMainEntity();
                    if (ModelState.IsValid)
                    {
                        DataViewMetaData dataViewMetaData = new DataViewMetaData();
                        dataViewMetaData.DataView = new DataView();
                        dataViewMetaData.TenantId = this.TenantId;
                        TryUpdateModel(dataViewMetaData);
                        ViewMetaDataProxy.CreateDataView(dataViewMetaData);
                        TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.DataViewResource.s_DataView_Saved_Successfully, formCollection["DataView.Name"]);
                        return RedirectToAction("DataViewList");
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, PolicyName);
                    ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, PolicyName);
                    ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
                }
                catch (ViewMetaDataException viewMetaDataException)
                {
                    ExceptionService.HandleException(viewMetaDataException, PolicyName);
                    ModelState.AddModelError("Error", viewMetaDataException.Message);
                }
            }
            return View();
        }

        /// <summary>
        /// This method is used to edit the virtual entity data view.
        /// </summary>
        /// <param name="dataViewId">The data view identifier.</param>
        /// <param name="mainEntity">The main entity.</param>
        /// <param name="tenantId">The tenant identifier.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <returns></returns>
        public ActionResult EditVirtualEntityDataView(string dataViewId, string mainEntity, string tenantId, string name, string description)
        {
            Guard.NullOrEmpty("dataViewId", dataViewId);
            Guard.NullOrEmpty("mainEntity", mainEntity);
            Guard.NullOrEmpty("tenantId", tenantId);
            Guard.NullOrEmpty("name", name);
            DataViewMetaData dataViewMetaData = new DataViewMetaData();
            dataViewMetaData.DataView = new DataView();
            try
            {
                dataViewMetaData.TenantId = tenantId;
                dataViewMetaData.DataView.DataViewID = dataViewId;
                dataViewMetaData.DataView.Description = description;
                dataViewMetaData.DataView.Name = name;
                dataViewMetaData.DataView.MainEntity = mainEntity;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            return View(dataViewMetaData);
        }

        /// <summary>
        /// This method is used to edit the virtual entity data view.
        /// </summary>
        /// <param name="formCollection">form collection</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditVirtualEntityDataView(FormCollection formCollection)
        {
            DataViewMetaData dataViewMetaData = new DataViewMetaData();
            dataViewMetaData.DataView = new DataView();
            if (formCollection.Count > 0 && formCollection != null)
            {
                if (string.IsNullOrEmpty(formCollection["DataView.Name"]))
                {
                    ModelState.AddModelError("Error", Resources.DataViewResource.e_DataViewName);
                }
                try
                {
                    dataViewMetaData.TenantId = this.TenantId;
                    TryUpdateModel(dataViewMetaData);
                    if (ModelState.IsValid)
                    {
                        ViewMetaDataProxy.UpdateDataView(dataViewMetaData);
                        TempData["Success"] = string.Format(Resources.DataViewResource.s_DataView_Saved_Successfully, formCollection["DataView.Name"]);
                        return RedirectToAction("DataViewList");
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, PolicyName);
                    ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, PolicyName);
                    ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
                }
                catch (ViewMetaDataException viewMetaDataException)
                {
                    ExceptionService.HandleException(viewMetaDataException, PolicyName);
                    ModelState.AddModelError("Error", viewMetaDataException.Message);
                }
            }
            return View(dataViewMetaData);
        }

        #endregion DataViewList

        #region DataViewFieldDetails

        /// <summary>
        /// This method is used to get the data view field details.
        /// </summary>
        /// <param name="fieldID">field Identifier.</param>
        /// <param name="ordinalList">ordinal list.</param>
        /// <param name="dataViewID">data view Identifier.</param>
        /// <param name="exId">ex identifier.</param>
        /// <param name="fieldType">type of the field.</param>
        /// <param name="entityID">entity Identifier.</param>
        /// <param name="globalFieldID">global field Identifier.</param>
        /// <param name="newField">new field.</param>
        /// <returns></returns>
        public ActionResult DataViewFieldDetails(string fieldID, string ordinalList, string dataViewID, int exId, FieldType fieldType, string entityID, int globalFieldID, string newField)
        {
            DataViewFieldMetaData dataViewMetaData = null;
            StringBuilder updatedOrdinalList = new StringBuilder();

            try
            {
                if (string.IsNullOrEmpty(dataViewID)) return HttpNotFound("Invalid request!");

                var dataViewMetadata = ViewMetaDataProxy.GetDataView(dataViewID);

                if (dataViewMetadata == null) return HttpNotFound("Invalid request!");

                ViewData["DataViewName"] = dataViewMetadata.Name;

                // If ID is 0, then it is an extended field. Else the field already exists. So get from the information from DB
                if (exId > 0)
                {
                    dataViewMetaData = ViewMetaDataProxy.GetDataViewFieldDetails(this.TenantId, fieldID, dataViewID, globalFieldID) ??
                                       new DataViewFieldMetaData
                        {
                            ID = exId,
                            FieldID = fieldID,
                            TenantID = this.TenantId,
                            FieldType = fieldType,
                            IsExtendedField = false,
                            GlobalFieldID = globalFieldID
                        };
                    if (dataViewMetaData.IsExtendedField && dataViewMetaData.EntityFieldMetaData != null)
                    {
                        ViewData["lengthField"] = !dataViewMetaData.EntityFieldMetaData.TypeID.Equals(DataFieldType.Varchar);
                        ViewData["regularExpressionField"] = (dataViewMetaData.EntityFieldMetaData.TypeID.Equals(DataFieldType.PickUpField) || dataViewMetaData.EntityFieldMetaData.TypeID.Equals(DataFieldType.Boolean));
                    }
                    if (dataViewMetaData.ID == 0)
                    {
                        dataViewMetaData.ID = exId;
                    }
                }
                else
                {
                    //if the field is an extended field, get the properties from extended field service
                    EntityFieldMetaData field = CelloSaaS.ServiceProxies.DataManagement.DataManagementProxy.GetEntityFieldMetaData(entityID, TenantId, fieldID);
                    dataViewMetaData = new DataViewFieldMetaData { FieldID = fieldID, TenantID = this.TenantId, FieldType = fieldType, DisplayName = field.Name };
                    dataViewMetaData.DisplayName = field.Name;
                    dataViewMetaData.MaxLength = field.Length;
                    dataViewMetaData.RegularExpression = field.ValidationRegEx;
                    dataViewMetaData.IsEditable = true;
                    dataViewMetaData.IsVisible = true;
                    dataViewMetaData.IsExtendedField = true;
                    dataViewMetaData.PickupListId = field.PickupListId;

                    ViewData["lengthField"] = !field.TypeID.Equals(DataFieldType.Varchar);
                    ViewData["regularExpressionField"] = (field.TypeID.Equals(DataFieldType.PickUpField) || field.TypeID.Equals(DataFieldType.Boolean));
                }

                if (!string.IsNullOrEmpty(ordinalList))
                {
                    // Remove the current ordinal from the list and recreate the ordinal csv
                    string[] existingOrdinals = ordinalList.Split(',');
                    ArrayList existingOrdinalsArray = new ArrayList(existingOrdinals);
                    existingOrdinalsArray.Remove(Convert.ToString(dataViewMetaData.Ordinal, CultureInfo.InvariantCulture));

                    foreach (string ordinal in existingOrdinalsArray)
                    {
                        updatedOrdinalList.Append(ordinal);
                        updatedOrdinalList.Append(",");
                    }
                    if (updatedOrdinalList.Length > 0)
                        updatedOrdinalList.Remove(updatedOrdinalList.Length - 1, 1);

                }

                // Set the postback values to the View's hidden variable
                ViewData["OrdinalList"] = updatedOrdinalList.ToString();
                ViewData["DataViewID"] = dataViewID;
                ViewData["EntityID"] = entityID;

                if (dataViewMetaData.FieldType.Equals(FieldType.DropdownList) || dataViewMetaData.FieldType.Equals(FieldType.CascadeDropDownList))
                {
                    PickupListMetaData pickupListMetaData = dataViewMetaData as PickupListMetaData;
                    ViewData["IsMultiLine"] = pickupListMetaData != null ? pickupListMetaData.IsMultiLine : false;
                    if (dataViewMetaData.FieldType.Equals(FieldType.CascadeDropDownList))
                    {
                        CascadePickupListMetaData cascadePickupListMetaData = dataViewMetaData as CascadePickupListMetaData;
                        ViewData["ControlFieldId"] = cascadePickupListMetaData.ControlFieldId;
                    }
                }

                if (!String.IsNullOrEmpty(dataViewMetaData.PickupListId))
                {
                    CelloSaaS.Model.Configuration.PickupList pickupList = CelloSaaS.ServiceProxies.Configuration.PickupListProxy.GetPickupListDetails(dataViewMetaData.PickupListId);
                    if (pickupList != null)
                        ViewData["PickupListName"] = pickupList.Name;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_GetDataViewField);
            }
            //catch (DataManagementException dataManagementException)
            //{
            //    ExceptionService.HandleException(dataManagementException, DefaultPolicy);
            //    ModelState.AddModelError("Error", dataManagementException.Message);
            //}
            GetPikupListType(fieldType);
            ViewData["ParentField"] = new SelectList(new List<string> { "--Select--" });
            return View(dataViewMetaData);
        }

        /// <summary>
        /// This method is used to get Pickup List type details.
        /// </summary>
        /// <param name="fieldType">field type</param>
        private void GetPikupListType(FieldType fieldType)
        {
            Dictionary<int, string> pickupListType = new Dictionary<int, string>
                {
                    { (int)FieldType.DropdownList, FieldType.DropdownList.ToString() },
                    { (int)FieldType.CascadeDropDownList, FieldType.CascadeDropDownList.ToString() }
                };
            //Add drop down list as type
            //Add cascade drop down list as type
            //Assign select list of pickup list type in View data
            ViewData["PickupListType"] = new SelectList(pickupListType, "Key", "Value", (int)fieldType);
        }

        /// <summary>
        /// This method is used to get parent pickup list details of FieldMetaData pickup list.
        /// </summary>
        /// <param name="entityId">field entity identifier.</param>
        /// <param name="fieldType">field type.</param>
        /// <param name="childPickupListId">field meta data pickup list identifier.</param>
        /// <param name="controlFieldId">control field identifier.</param>
        /// <returns></returns>
        public JsonResult GetParentFieldDetails(string entityId, string fieldType, string childPickupListId, string controlFieldId)
        {
            try
            {
                if (FieldType.CascadeDropDownList.Equals((FieldType)Convert.ToInt32(fieldType, CultureInfo.InvariantCulture)))
                {
                    //Get Relationship details for passed pickup list id
                    List<RelationshipMetaData> relationshipDetails = PickupListProxy.GetRelationshipMetaData(childPickupListId, this.TenantId, Relation.Child, string.Empty);
                    //Check if passed pickup list has relation then get the parent pickup list ids
                    if (relationshipDetails != null && relationshipDetails.Count > 0)
                    {
                        List<string> parentPickupListIds = new List<string>();
                        //Loop through relationship meta data and get parent opickup list ids
                        foreach (RelationshipMetaData relation in relationshipDetails)
                        {
                            if (relation.Status.Equals(true))
                                parentPickupListIds.Add(relation.TargetId);
                        }

                        //Get fields for passed entity name
                        Dictionary<string, EntityFieldMetaData> entityFields = CelloSaaS.ServiceProxies.DataManagement.DataManagementProxy.GetFields(entityId, this.TenantId);
                        List<string> parentFields = new List<string>();
                        //Check if entity fields have values or not.
                        if (entityFields != null && entityFields.Count > 0)
                        {
                            //Loop through entity fields to get parent field ids
                            foreach (EntityFieldMetaData entityMetaData in entityFields.Values.ToList())
                            {
                                //Check if entity meta data has parent pickup list id. If has then add field name in the result list
                                if (!string.IsNullOrEmpty(entityMetaData.PickupListId) && parentPickupListIds.Contains(entityMetaData.PickupListId))
                                    parentFields.Add(entityMetaData.EntityFieldIdentifier + "," + entityMetaData.Name);
                            }
                            if (parentFields.Count > 0)
                            {
                                parentFields.Insert(0, "--Select--,--Select--");
                                string selectedValue = string.Empty;
                                if (!string.IsNullOrEmpty(controlFieldId))
                                {
                                    EntityFieldMetaData selectedParentEntityField = entityFields[controlFieldId];
                                    if (selectedParentEntityField != null)
                                        selectedValue = selectedParentEntityField.EntityFieldIdentifier + "," + selectedParentEntityField.Name;
                                }
                                SelectList fieldIds = new SelectList(parentFields, selectedValue);
                                return Json(fieldIds, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            catch (CelloSaaS.ServiceContracts.Configuration.PickupListException pickupListException)
            {
                ExceptionService.HandleException(pickupListException, DefaultPolicy);
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            catch (ViewMetaDataException viewMetaDataException)
            {
                ExceptionService.HandleException(viewMetaDataException, DefaultPolicy);
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            return Json("NoRelation", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to get the data view field details based on given parameters.
        /// </summary>
        /// <param name="fieldValues">field values.</param>
        /// <param name="Navigate">navigate.</param>
        /// <param name="passedDataViewID">passed data view identifier.</param>
        /// <param name="updatedOrdinalList">updated ordinal list.</param>
        /// <param name="passedEntityID">passed entity identifier.</param>
        /// <param name="passedOrdinal">passed ordinal.</param>
        /// <param name="IsExtendedField">if set to <c>true</c> [is extended field].</param>
        /// <param name="passedPickupListID">passed pickup list identifier.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DataViewFieldDetails(DataViewFieldMetaData fieldValues, string Navigate, string passedDataViewID, string updatedOrdinalList, string passedEntityID, int passedOrdinal, bool IsExtendedField, string passedPickupListID)
        {
            try
            {
                if (string.IsNullOrEmpty(passedDataViewID)) return HttpNotFound("Invalid request!");

                var dataViewMetadata = ViewMetaDataProxy.GetDataView(passedDataViewID);

                if (dataViewMetadata == null) return HttpNotFound("Invalid request!");

                ViewData["DataViewName"] = dataViewMetadata.Name;

                //Set editable is true for base fields
                if (!fieldValues.IsExtendedField)
                    fieldValues.IsEditable = true;
                // Check whether update/cancel button is called
                //if (Navigate == UpdateButton)
                //{
                // Validate the form values
                ValidateFormData(fieldValues);

                if (!string.IsNullOrEmpty(Request.Form["PickupListType"]))
                    fieldValues.FieldType = (FieldType)Convert.ToInt32(Request.Form["PickupListType"].ToString(), CultureInfo.InvariantCulture);

                // Set the ordinal from the hidden variable
                fieldValues.Ordinal = passedOrdinal;
                if (IsExtendedField)
                {
                    fieldValues.IsExtendedField = true;
                    if (passedOrdinal == 0)
                    {
                        if (!String.IsNullOrEmpty(updatedOrdinalList))
                        {
                            string[] existingOrdinals = updatedOrdinalList.Split(',');
                            ArrayList existingOrdinalsArray = new ArrayList(existingOrdinals);
                            fieldValues.Ordinal = GetMaxOrdinal(existingOrdinalsArray);
                        }
                        else
                            fieldValues.Ordinal = 1;
                    }
                }
                //Get Field type                    
                if (fieldValues.FieldType.Equals(FieldType.CascadeDropDownList))
                {
                    //Get parent control field id                        
                    if (string.IsNullOrEmpty(Request.Form["ParentField"]) || Convert.ToString(Request.Form["ParentField"], CultureInfo.InvariantCulture).Equals("--Select--"))
                    {
                        ModelState.AddModelError("valParentField", Resources.DataViewResource.e_ParentField);
                        ViewData["ControlFieldID"] = string.Empty;
                    }
                    else
                        ViewData["ControlFieldID"] = Request.Form["ParentField"].ToString();

                }
                bool isMultiLine = false;
                if (Request.Form["IsMultiLine"] != null)
                {
                    isMultiLine = Request.Form["IsMultiLine"].ToString().Equals("false") ? false : true;
                    ViewData["IsMultiLine"] = isMultiLine;
                }
                // Check if any invalid data exists
                if (ModelState.IsValid)
                {
                    fieldValues.CreatedBy = UserIdentity.UserId;
                    fieldValues.UpdatedBy = UserIdentity.UserId;
                    fieldValues.TenantID = TenantId;
                    if (fieldValues.FieldType.Equals(FieldType.DropdownList))
                        fieldValues = FillPickupListMetaData(fieldValues, isMultiLine);
                    else if (fieldValues.FieldType.Equals(FieldType.CascadeDropDownList))
                        fieldValues = FillCascadePickupListMetaData(fieldValues, isMultiLine, Request.Form["ParentField"]);
                    // If ID exists, then the field already exists in dataviewFields
                    if (Convert.ToInt32(ModelState["ID"].Value.AttemptedValue, CultureInfo.InvariantCulture) > 0)
                    {
                        fieldValues.PickupListId = passedPickupListID;
                        ViewMetaDataProxy.UpdateDataViewField(fieldValues, TenantId, passedDataViewID);
                    }
                    else
                    {
                        fieldValues.PickupListId = passedPickupListID;
                        ViewMetaDataProxy.AddExtendedField(fieldValues, passedDataViewID);
                    }
                    TempData["SuccessMessage"] = Resources.DataViewResource.s_save;
                    return RedirectToAction("DataViewFieldList", "DataView", new { dataViewID = passedDataViewID, entityID = passedEntityID });
                }
                //}
                //else
                //{
                //    return RedirectToAction("DataViewFieldList", "DataView", new { dataViewID = passedDataViewID, entityID = passedEntityID });
                //}
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_save);
            }

            ViewData["DataViewID"] = passedDataViewID;
            ViewData["EntityID"] = passedEntityID;

            GetPikupListType(fieldValues.FieldType);
            ViewData["ParentField"] = new SelectList(new List<string> { "--Select--" });
            return View(fieldValues);
        }

        /// <summary>
        /// This method is used to fill the cascade pickup list meta data.
        /// </summary>
        /// <param name="dataViewFieldMetaData">data view field meta data.</param>
        /// <param name="isMultiLine">if set to <c>true</c> [is multi line].</param>
        /// <param name="contolFieldId">control field identifier.</param>
        /// <returns></returns>
        private static DataViewFieldMetaData FillCascadePickupListMetaData(DataViewFieldMetaData dataViewFieldMetaData, bool isMultiLine, string contolFieldId)
        {
            CascadePickupListMetaData cascadePickupListMetaData = new CascadePickupListMetaData
                {
                    Description = dataViewFieldMetaData.Description,
                    DisplayName = dataViewFieldMetaData.DisplayName,
                    EntityFieldIdentifier = dataViewFieldMetaData.EntityFieldIdentifier,
                    EntityIdentifier = dataViewFieldMetaData.EntityIdentifier,
                    FieldID = dataViewFieldMetaData.FieldID,
                    FieldType = dataViewFieldMetaData.FieldType,
                    GlobalField_FieldID = dataViewFieldMetaData.GlobalField_FieldID,
                    GlobalFieldID = dataViewFieldMetaData.GlobalFieldID,
                    ID = dataViewFieldMetaData.ID,
                    IsEditable = dataViewFieldMetaData.IsEditable,
                    IsExtendedField = dataViewFieldMetaData.IsExtendedField,
                    IsFixed = dataViewFieldMetaData.IsFixed,
                    IsMandatory = dataViewFieldMetaData.IsMandatory,
                    IsVisible = dataViewFieldMetaData.IsVisible,
                    MaxLength = dataViewFieldMetaData.MaxLength,
                    Ordinal = dataViewFieldMetaData.Ordinal,
                    PickupListId = dataViewFieldMetaData.PickupListId,
                    RegularExpression = dataViewFieldMetaData.RegularExpression,
                    TenantID = dataViewFieldMetaData.TenantID,
                    CreatedBy = dataViewFieldMetaData.CreatedBy,
                    CreatedOn = dataViewFieldMetaData.CreatedOn,
                    UpdatedBy = dataViewFieldMetaData.UpdatedBy,
                    UpdatedOn = dataViewFieldMetaData.UpdatedOn,
                    IsMultiLine = isMultiLine,
                    ControlFieldId = contolFieldId,
                    ViewDataName = dataViewFieldMetaData.ViewDataName,
                    DefaultValue = dataViewFieldMetaData.DefaultValue,
                    IsHidden = dataViewFieldMetaData.IsHidden
                };

            return cascadePickupListMetaData;
        }

        /// <summary>
        /// This method is used to fill the pickup list meta data.
        /// </summary>
        /// <param name="dataViewFieldMetaData">data view field meta data.</param>
        /// <param name="isMultiLine">if set to <c>true</c> [is multi line].</param>
        /// <returns></returns>
        private static DataViewFieldMetaData FillPickupListMetaData(DataViewFieldMetaData dataViewFieldMetaData, bool isMultiLine)
        {
            PickupListMetaData pickupListMetaData = new PickupListMetaData
                {
                    Description = dataViewFieldMetaData.Description,
                    DisplayName = dataViewFieldMetaData.DisplayName,
                    EntityFieldIdentifier = dataViewFieldMetaData.EntityFieldIdentifier,
                    EntityIdentifier = dataViewFieldMetaData.EntityIdentifier,
                    FieldID = dataViewFieldMetaData.FieldID,
                    FieldType = dataViewFieldMetaData.FieldType,
                    GlobalField_FieldID = dataViewFieldMetaData.GlobalField_FieldID,
                    GlobalFieldID = dataViewFieldMetaData.GlobalFieldID,
                    ID = dataViewFieldMetaData.ID,
                    IsEditable = dataViewFieldMetaData.IsEditable,
                    IsExtendedField = dataViewFieldMetaData.IsExtendedField,
                    IsFixed = dataViewFieldMetaData.IsFixed,
                    IsMandatory = dataViewFieldMetaData.IsMandatory,
                    IsVisible = dataViewFieldMetaData.IsVisible,
                    MaxLength = dataViewFieldMetaData.MaxLength,
                    Ordinal = dataViewFieldMetaData.Ordinal,
                    PickupListId = dataViewFieldMetaData.PickupListId,
                    RegularExpression = dataViewFieldMetaData.RegularExpression,
                    TenantID = dataViewFieldMetaData.TenantID,
                    CreatedBy = dataViewFieldMetaData.CreatedBy,
                    CreatedOn = dataViewFieldMetaData.CreatedOn,
                    UpdatedBy = dataViewFieldMetaData.UpdatedBy,
                    UpdatedOn = dataViewFieldMetaData.UpdatedOn,
                    IsMultiLine = isMultiLine,
                    ViewDataName = dataViewFieldMetaData.ViewDataName,
                    DefaultValue = dataViewFieldMetaData.DefaultValue,
                    IsHidden = dataViewFieldMetaData.IsHidden
                };

            return pickupListMetaData;
        }

        /// <summary>
        /// This method is used to returns the max ordinal for the collection passed.
        /// </summary>
        /// <param name="existingOrdinalsArray">existing ordinals array.</param>
        /// <returns></returns>
        private static int GetMaxOrdinal(ArrayList existingOrdinalsArray)
        {
            int maxOrdinal = 0;
            foreach (string ordinal in existingOrdinalsArray)
                if (Convert.ToInt32(ordinal, CultureInfo.InvariantCulture) > maxOrdinal)
                    maxOrdinal = Convert.ToInt32(ordinal, CultureInfo.InvariantCulture);

            return maxOrdinal + 1;
        }

        /// <summary>
        /// This method is used to validates the form data.
        /// </summary>
        /// <param name="fieldValues">field values.</param>
        private void ValidateFormData(DataViewFieldMetaData fieldValues)
        {
            if (string.IsNullOrEmpty(fieldValues.DisplayName))
            {
                ModelState.AddModelError("DataViewSummary", Resources.DataViewResource.e_DisplayName);
            }
            if (fieldValues.MaxLength < 0)
            {
                ModelState.AddModelError("DataViewSummary", Resources.DataViewResource.e_MaxLength);
            }
        }

        #endregion DataViewFieldDetails

        #region DataViewFieldListReorder

        /// <summary>
        /// This method is used to record the data view field list based on the data view and entity identifier.
        /// </summary>
        /// <param name="dataViewId">data view identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public ActionResult DataViewFieldListReorder(string dataViewId, string entityId)
        {
            ViewData["DataViewID"] = dataViewId;
            ViewData["EntityID"] = entityId;

            try
            {
                StringBuilder fieldIds = new StringBuilder();
                IEnumerable<CelloSaaS.Model.ViewManagement.DataViewFieldMetaData> fieldMetaData = null;
                DataViewMetaData dataViewMetaData = ViewMetaDataProxy.GetDataViewMetaData(TenantId, dataViewId, entityId);

                if (dataViewMetaData != null && dataViewMetaData.AvailableFields != null && dataViewMetaData.AvailableFields.Values != null)
                {
                    fieldMetaData = dataViewMetaData.AvailableFields.Values.ToList().OrderBy(a => a.Ordinal);
                }

                foreach (DataViewFieldMetaData field in fieldMetaData)
                {
                    if (field.ID <= 0)
                    {
                        continue;
                    }
                    fieldIds.Append(field.ID);
                    fieldIds.Append(",");
                }

                fieldIds.Remove(fieldIds.Length - 1, 1);
                ViewData["FieldIds"] = fieldIds.ToString();

                return View(fieldMetaData);
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_GetDataViewMetaData);
            }

            return PartialView("DataViewFieldListReorder");
        }

        /// <summary>
        /// This method is used to record the data view field list based on the given parameters.
        /// </summary>
        /// <param name="IDs"> identifiers.</param>
        /// <param name="passedDataViewId">data view identifier.</param>
        /// <param name="passedEntityId">entity identifier.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DataViewFieldListReorder(string IDs, string passedDataViewId, string passedEntityId)
        {
            try
            {
                //if (Navigate == UpdateButton)
                //{
                // Convert the stringArray to intArray
                string[] idStrArray = IDs.Split(',');
                ArrayList idArray = new ArrayList();
                foreach (string id in idStrArray)
                    idArray.Add(Convert.ToInt32(id, CultureInfo.InvariantCulture));

                // Update by Ordinal
                ViewMetaDataProxy.UpdateFieldOrdinal(TenantId, passedDataViewId, passedEntityId, idArray, UserIdentity.UserId);
                //}
                TempData["SuccessMessage"] = Resources.DataViewResource.s_Rearranged;
                return RedirectToAction("DataViewFieldList", "DataView", new { dataViewID = passedDataViewId, entityID = passedEntityId });
            }
            //Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_ParameterEmptyOrNull);
            }
            catch (ViewMetaDataException ex)
            {
                // Set the error message to the control in view to show to the user.
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("ExceptionMessage", Resources.DataViewResource.e_UpdateOrdinal);
            }

            return PartialView("DataViewFieldListReorder");
        }

        #endregion DataViewFieldListReorder
    }
}
