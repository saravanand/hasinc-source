﻿using CelloSaaS.DbAnalytics.Helper;
using CelloSaaS.DbAnalytics.Model;
using CelloSaaS.DbAnalytics.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace WebApplication.Controllers
{
    /// <summary>
    /// This class is responsible for database analytics.
    /// </summary>
    public class DatabaseAnalyticsController : CelloController
    {
        private const string DefaultPolicy = "GlobalExceptionLogger";

        #region Public method

        /// <summary>
        /// This method used to render the database analytics page.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="filter">filter.</param>
        /// <returns></returns>
        public ActionResult Index(string tenantId, string filter)
        {
            try
            {
                // Get All tenant list.
                var tenantList = GetTenants();
                ViewData["TenantList"] = tenantList;

                if (tenantList != null && !string.IsNullOrWhiteSpace(tenantId) && tenantList.ContainsKey(tenantId))
                {
                    DbAnalyticsSearchCondition condition = new DbAnalyticsSearchCondition();
                    condition.Filter = DbAnalytcisHelper.ParseEnum<DbAnalyticsFilter>(filter);
                    condition.TenantId = tenantId;

                    if (condition.Filter.Equals(DbAnalyticsFilter.Days))
                    {
                        condition.EndDate=DateTime.Now.AddDays(-1);
                    }
                    // get database analytics details based on condition.
                    var databaseAnalytics = GetDatabaseAnalyticsDetails(condition);

                    if (databaseAnalytics != null && databaseAnalytics.Count > 0)
                    {
                        // get server name list.
                        GetServerNameList(condition.TenantId, condition.Filter);

                        if (condition.Filter.Equals(DbAnalyticsFilter.Days))
                        {
                            //get start date and end date.
                            DateTime analyticsStartDate = databaseAnalytics.OrderBy(c => c.AnalyticsMetrics.AnalyticsDate).Select(c => c.AnalyticsMetrics.AnalyticsDate).FirstOrDefault();
                            DateTime analyticsEndDate = databaseAnalytics.OrderByDescending(c => c.AnalyticsMetrics.AnalyticsDate).Select(c => c.AnalyticsMetrics.AnalyticsDate).FirstOrDefault();
                            ViewBag.analyticsStartDate = (analyticsStartDate - DateTime.Now.Date).TotalDays;
                            ViewBag.analyticsEndDate = (analyticsEndDate - DateTime.Now.Date).TotalDays;

                            ViewData["DbAnalyticsMetrics"] = new SelectList(Enum.GetNames(typeof(DbAnalyticsMetrics)), condition.Metrics);
                            ViewData["DbAnalyticsOperands"] = new SelectList(Enum.GetNames(typeof(DbAnalyticsOperands)), condition.SearchOperand);
                        }

                        // Average the analytics details by sweep count.
                        ViewBag.AnalyticsDetails = ProcessAnalyticsDetails(databaseAnalytics, condition.Filter);
                    }
                }

            }
            catch (ArgumentNullException argumentNullException)
            {
                ModelState.AddModelError("Error", argumentNullException.Message);
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
            }
            catch (ArgumentException argumentException)
            {
                ModelState.AddModelError("Error", argumentException.Message);
                ExceptionService.HandleException(argumentException, DefaultPolicy);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ModelState.AddModelError("Error", "You don't have permission to access this service.");
                ExceptionService.HandleException(unauthorizedAccessException, DefaultPolicy);
            }
            catch (TenantException tenantException)
            {
                ModelState.AddModelError("Error", tenantException.Message);
                ExceptionService.HandleException(tenantException, DefaultPolicy);
            }
            catch (DbAnalyticsException dbAnalyticsException)
            {
                ModelState.AddModelError("Error", dbAnalyticsException.Message);
                ExceptionService.HandleException(dbAnalyticsException, DefaultPolicy);
            }
            if (this.Request.IsAjaxRequest())
            {
                return PartialView("PartialDatabaseAnalytics");
            }
            return View();
        }

        /// <summary>
        /// This method is used to used to search the database analytics details.
        /// </summary>
        /// <param name="searchCondition">db analytics search condition.</param>
        /// <param name="tenantId">contain tenant unique identifier.</param>       
        /// <param name="filter">contain filter.</param>
        /// <returns></returns>
        public PartialViewResult SearchDatabaseAnalytics(DbAnalyticsSearchCondition searchCondition, string tenantId, string filter)
        {
            try
            {
                if (searchCondition == null || string.IsNullOrWhiteSpace(tenantId) || string.IsNullOrWhiteSpace(filter))
                {
                    ModelState.AddModelError("Error", "Given tenantId/filter is not valid");
                }
                else
                {
                    searchCondition.TenantId = tenantId;
                    searchCondition.Filter = DbAnalytcisHelper.ParseEnum<DbAnalyticsFilter>(filter);

                    if (searchCondition.ServerNames != null && searchCondition.ServerNames.Count > 0 && searchCondition.ServerNames.Contains("All"))
                    {
                        searchCondition.ServerNames = null;
                    }

                    if (searchCondition.DbNames != null && searchCondition.DbNames.Count > 0 && searchCondition.DbNames.Contains("All"))
                    {
                        searchCondition.DbNames = null;
                    }

                    // Average the analytics details by sweep count.
                    ViewBag.AnalyticsDetails = ProcessAnalyticsDetails(GetDatabaseAnalyticsDetails(searchCondition), searchCondition.Filter); ;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", argumentNullException.Message);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ModelState.AddModelError("Error", "You don't have permission to access this service.");
                ExceptionService.HandleException(unauthorizedAccessException, DefaultPolicy);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", argumentException.Message);
            }
            catch (DbAnalyticsException dbAnalyticsException)
            {
                ExceptionService.HandleException(dbAnalyticsException, DefaultPolicy);
                ModelState.AddModelError("Error", dbAnalyticsException.Message);
            }
            return PartialView("PreviewDatabaseAnalytics");
        }

        /// <summary>
        /// This method used to get the data base name by server.
        /// </summary>
        /// <param name="serverName">server name.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="filter">filter.</param>
        /// <returns></returns>
        public JsonResult GetDatabaseName(List<string> serverName, string tenantId, string filter)
        {
            if (string.IsNullOrEmpty(tenantId) || string.IsNullOrEmpty(filter))
            {
                return Json(new { Error = Resources.TenantResource.e_InvalidRequest });
            }

            try
            {
                // get the database name list.
                var databaseNameList = LoadDatabaseNames(serverName, tenantId, filter);
                return Json(databaseNameList);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, DefaultPolicy);
                return Json(new { Error = "You don't have permission to access this service." });
            }
            catch (DbAnalyticsException dbAnalyticsException)
            {
                ExceptionService.HandleException(dbAnalyticsException, DefaultPolicy);
                return Json(new { Error = dbAnalyticsException.Message });
            }

        }

        #endregion Public method

        #region Private method

        /// <summary>
        /// This method used to search the database analytics.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <returns>collection of database analytics details.</returns>
        private List<DatabaseAnalytics> GetDatabaseAnalyticsDetails(DbAnalyticsSearchCondition condition)
        {
            if (condition.Filter.Equals(DbAnalyticsFilter.Days))
            {
                if (condition.StartDate.HasValue && condition.EndDate.HasValue)
                {
                    if (condition.StartDate.Equals(condition.EndDate))
                    {
                        condition.StartDate = condition.EndDate;
                        condition.Filter = DbAnalyticsFilter.Hours;
                    }
                }
            }
            ViewBag.Filter = condition.Filter;
            //Search the data base analytics by filter.
            return DbAnalyticsServiceProxy.SearchDatabaseAnalytics(condition);

        }

        /// <summary>
        /// This method used to get the all active tenants.
        /// </summary>
        /// <returns>collection of tenant details.</returns>
        private Dictionary<string, TenantDetails> GetTenants()
        {
            // Checking current user is product admin role.
            if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
            {
                // get all active tenants.
                var tenatDetails = TenantProxy.GetAllActiveTenantDetails();

                if (tenatDetails == null || tenatDetails.Count == 0)
                    return null;

                tenatDetails = tenatDetails.Where(x => x.Value.ApprovalStatus == TenantApprovalStatus.APPROVED)
                                  .ToDictionary(x => x.Key, y => y.Value);
                return tenatDetails;
            }
            return null;
        }

        /// <summary>
        /// This method used to get average the analytics data by sweep.
        /// </summary>
        /// <param name="analyticsList">list of analytics details.</param>
        /// <returns>collection of process analytics details</returns>
        private Dictionary<DateTime, AnalyticsMetrics> ProcessAnalyticsDetails(List<DatabaseAnalytics> analyticsList, DbAnalyticsFilter filter)
        {
            if (analyticsList == null || analyticsList.Count < 1)
                return null;

            double sweepCount = 1;

            if (DbAnalyticsFilter.Days.Equals(filter))
            {
                sweepCount = ConfigHelper.SweepCount;
            }
            else if (DbAnalyticsFilter.Weeks.Equals(filter))
            {
                sweepCount = ConfigHelper.SweepCount * 7;
            }

            return analyticsList.Select(y => new AnalyticsMetrics()
                       {
                           AnalyticsDate = y.AnalyticsMetrics.AnalyticsDate,
                           MemoryUsage = Math.Round(y.AnalyticsMetrics.MemoryUsage / sweepCount, 3),
                           CpuTime = Math.Round(y.AnalyticsMetrics.CpuTime / sweepCount, 3),
                           Reads = Math.Round(y.AnalyticsMetrics.Reads / sweepCount, 3),
                           Writes = Math.Round(y.AnalyticsMetrics.Writes / sweepCount, 3),
                           LogicalReads = Math.Round(y.AnalyticsMetrics.LogicalReads / sweepCount, 3),
                           NewConnection = Math.Round(y.AnalyticsMetrics.NewConnection / sweepCount, 3),
                           TotalConnection = Math.Round(y.AnalyticsMetrics.TotalConnection / sweepCount, 3)
                       }).ToDictionary(c => c.AnalyticsDate, c => c);

        }

        /// <summary>
        /// This method used to get the server name list.
        /// </summary>
        /// <param name="serverNameList">list of server names.</param>
        private void GetServerNameList(List<string> serverNameList)
        {
            List<SelectListItem> serverListItem = new List<SelectListItem>();

            if (serverNameList != null && serverNameList.Count > 0)
            {
                serverListItem = (from item in serverNameList.OrderBy(c => c)
                                  select new SelectListItem
                                  {
                                      Text = item,
                                      Value = item,
                                  }).ToList<SelectListItem>();
            }
            serverListItem.Insert(0, new SelectListItem { Text = "All", Value = "All", Selected = true });

            ViewBag.ServerListItem = serverListItem;
        }

        /// <summary>
        ///  This method used to get the server name list.
        /// </summary>
        /// <param name="tenantId">containing the tenant identifier.</param>
        /// <param name="filter">containing the filter.</param>
        private void GetServerNameList(string tenantId, DbAnalyticsFilter filter)
        {
            //get server name list.
            var serverNameList = DbAnalyticsServiceProxy.GetServerNames(tenantId, filter);
            List<SelectListItem> serverListItem = new List<SelectListItem>();

            if (serverNameList != null && serverNameList.Count > 0)
            {
                serverListItem = (from item in serverNameList.OrderBy(c => c)
                                  select new SelectListItem
                                  {
                                      Text = item,
                                      Value = item,
                                  }).ToList<SelectListItem>();
            }
            serverListItem.Insert(0, new SelectListItem { Text = "All", Value = "All", Selected = true });
            ViewBag.ServerListItem = serverListItem;
        }

        /// <summary>
        /// This method used to get the database name list.
        /// </summary>
        /// <param name="serverNames">containing the server name.</param>
        /// <param name="tenantId">containing the tenant identifier.</param>
        /// <param name="filter">containing the filter.</param>
        /// <returns>list of database name selected list item.</returns>
        private List<SelectListItem> LoadDatabaseNames(List<string> serverNames, string tenantId, string filter)
        {
            var databaseNameList = new List<SelectListItem>();

            if (serverNames != null && serverNames.Count > 0 && !serverNames.Contains("All"))
            {
                // get the database name list.
                var databaseList = DbAnalyticsServiceProxy.GetDatabaseNames(tenantId, serverNames, DbAnalytcisHelper.ParseEnum<DbAnalyticsFilter>(filter));

                if (databaseList != null)
                {
                    databaseNameList = (from item in databaseList.OrderBy(c => c)
                                        select new SelectListItem
                                        {
                                            Text = item,
                                            Value = item,
                                        }).ToList<SelectListItem>();
                }
            }
            databaseNameList.Insert(0, new SelectListItem { Text = "All", Value = "All", Selected = true });
            return databaseNameList;
        }

        #endregion Private method

    }
}
