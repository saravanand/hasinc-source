﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.Library;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.ServiceProxies.DataManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using System.Text;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for entity tenant scope management.
    /// </summary>
    public class EntityTenantScopeController : CelloController
    {
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This method is used to manage the tenant scope.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageTenantScope()
        {
            string selectedTenantId = string.Empty;
            string selectedModuleId = "-1";

            selectedTenantId = LoadTenant();

            if (!string.IsNullOrEmpty(selectedTenantId))
            {
                ViewData["SelectedTenantId"] = selectedTenantId;
                ViewData["SelectedModuleId"] = selectedModuleId;                
                LoadModule();
            }
            else
            {
                ModelState.AddModelError("error", Resources.EntityTenantScopeResource.e_NoChildTenant);
            }
            return View();
        }

        /// <summary>
        /// This method is used to partially the manage tenant scope.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="moduleId">module identifier.</param>
        /// <returns></returns>
        public ActionResult PartialManageTenantScope(string tenantId, string moduleId)
        {
            if (string.IsNullOrEmpty(moduleId))
            {
                moduleId = "-1";
            }
            LoadEntityAndModule(moduleId);
            string selectedTenantId = string.Empty;
            if (string.IsNullOrEmpty(tenantId))
            {
                selectedTenantId = LoadTenant();
            }
            else
            {
                selectedTenantId = tenantId;
                LoadTenant();
            }

            ViewData["SelectedTenantId"] = selectedTenantId;
            ViewData["SelectedModuleId"] = moduleId;

            Dictionary<string, EntityTenantScope> entityTenantScopeList = null;
            entityTenantScopeList = TenantAccessProxy.GetAllEntityTenantScope(selectedTenantId);
            ViewData["EntityTenantScopeList"] = entityTenantScopeList;
            LoadTenantScope();
            return PartialView("PartialManageTenantScope");
        }

        /// <summary>
        /// This method is used to updates the tenant scope.
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateTenantScope()
        {
            string tenantId = Request.Form["TenantName"];
            string moduleId = Request.Form["ModuleName"];
            LoadEntityAndModule(moduleId);
            Dictionary<string, EntityTenantScope> entityTenantScopeList = GetEntityTenantScope(tenantId);

            TenantAccessProxy.UpdateEntityTenantScope(tenantId, entityTenantScopeList);
            ModelState.AddModelError("success", Resources.EntityTenantScopeResource.s_UpdateTenantScope);
            ViewData["EntityTenantScopeList"] = entityTenantScopeList;
            LoadTenantScope();
            return PartialView("PartialManageTenantScope");
        }

        /// <summary>
        /// This method is used to loads the entity and module.
        /// </summary>
        private void LoadEntityAndModule(string moduleId)
        {
            Dictionary<string, Module> modules = LoadModule();

            string[] strModules = modules.Keys.ToArray();

            // Dictionary of features based on the modules
            Dictionary<string, Dictionary<string, Feature>> modeuleFeatures = null;

            modeuleFeatures = LicenseProxy.GetFeatureByModuleIds(strModules);

            if (modeuleFeatures != null && modeuleFeatures.Count > 0)
            {
                Dictionary<string, Feature> featureList = new Dictionary<string, Feature>();

                // if module is not select all features based on the modules
                if (!string.IsNullOrEmpty(moduleId) && moduleId != "-1")
                {
                    featureList = modeuleFeatures[moduleId];
                }
                else
                {
                    foreach (Dictionary<string, Feature> feature in modeuleFeatures.Values)
                    {
                        foreach (KeyValuePair<string, Feature> kvp in feature)
                        {
                            featureList.Add(kvp.Key, kvp.Value);
                        }
                    }
                }

                string[] strFeatures = featureList.Keys.ToArray();

                // Dictionary of Entity metadata based on the features.
                var dctEntityMetaData = DataManagementProxy.GetAllEntityMetaData(strFeatures);
                var entityBasedTenantScopelist = TenantAccessProxy.GetAllEntityBasedTenantScope();
                Dictionary<string, EntityMetaData> filterdEntity = null;
                if (entityBasedTenantScopelist != null && entityBasedTenantScopelist.Count > 0 && dctEntityMetaData != null && dctEntityMetaData.Count > 0)
                {
                    filterdEntity = new Dictionary<string, EntityMetaData>();
                    foreach (var entityBasedTenantScope in entityBasedTenantScopelist)
                    {
                        if (dctEntityMetaData.ContainsKey(entityBasedTenantScope.Key) && !filterdEntity.ContainsKey(entityBasedTenantScope.Key))
                        {
                            filterdEntity.Add(entityBasedTenantScope.Key, dctEntityMetaData[entityBasedTenantScope.Key]);
                        }
                    }
                }
                ViewData["DictEntityMetaData"] = filterdEntity == null ? null : filterdEntity.Values.ToList();
            }
        }

        /// <summary>
        /// This method is used to load all licensed modules.
        /// </summary>
        /// <returns>collection of module<./returns>
        private Dictionary<string, Module> LoadModule()
        {
            List<Module> lstModule = null;
            Dictionary<string, Module> modules = null;
            TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(UserIdentity.TenantID);
            if (tenantLicense != null && tenantLicense.LicenseModuleDetails != null && tenantLicense.LicenseModuleDetails.Count > 0)
                modules = tenantLicense.LicenseModuleDetails;

            lstModule = modules.Values.ToList();
            lstModule.Insert(0, new Module { ModuleCode = "-1", ModuleName = "- All -" });
            ViewData["Modules"] = lstModule;
            return modules;
        }

        /// <summary>
        /// This method is used to load the tenant.
        /// </summary>
        private string LoadTenant()
        {
            Dictionary<string, TenantDetails> tenantDetailList = TenantRelationProxy.GetAllActiveImmediateChildTenants(UserIdentity.TenantID);
            if (tenantDetailList != null && tenantDetailList.Count > 0)
            {
                ViewData["TenantList"] = tenantDetailList.Values.ToList();
                return tenantDetailList.Values.FirstOrDefault().TenantCode;
            }
            return string.Empty;
        }

        /// <summary>
        /// This method is used to loads the tenant scope.
        /// </summary>
        private void LoadTenantScope()
        {
            Dictionary<string, TenantScope> tenantScopeList = TenantAccessProxy.GetAllTenantScope();
            if (tenantScopeList != null && tenantScopeList.Count > 0)
            {
                ViewData["TenantScopeList"] = tenantScopeList.Values.ToList();
            }
        }

        /// <summary>
        /// This method is used to gets the entity tenant scope based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>collection of entity tenant scope.</returns>
        private Dictionary<string, EntityTenantScope> GetEntityTenantScope(string tenantId)
        {
            List<EntityMetaData> entityMetaDataList = (List<EntityMetaData>)ViewData["DictEntityMetaData"];

            Dictionary<string, EntityTenantScope> entityTenantScopeList = new Dictionary<string, EntityTenantScope>();


            foreach (var entityMetaData in entityMetaDataList)
            {
                string viewTenantScopeId = Request.Form["ddl_view_" + entityMetaData.EntityIdentifier];
                string editTenantScopeId = Request.Form["ddl_edit_" + entityMetaData.EntityIdentifier];
                string deleteTenantScopeId = Request.Form["ddl_delete_" + entityMetaData.EntityIdentifier];

                string viewSelectiveTenant = Request.Form["hdn_view_" + entityMetaData.EntityIdentifier];
                string editSelectiveTenant = Request.Form["hdn_edit_" + entityMetaData.EntityIdentifier];
                string deleteSelectiveTenant = Request.Form["hdn_delete_" + entityMetaData.EntityIdentifier];

                if ((!string.IsNullOrEmpty(viewTenantScopeId) && !viewTenantScopeId.Equals(TenantScopeConstant.DEFAULT_TENANT_SCOPE)) ||
                    (!string.IsNullOrEmpty(editTenantScopeId) && !editTenantScopeId.Equals(TenantScopeConstant.DEFAULT_TENANT_SCOPE)) ||
                    (!string.IsNullOrEmpty(deleteTenantScopeId) && !deleteTenantScopeId.Equals(TenantScopeConstant.DEFAULT_TENANT_SCOPE)))
                {
                    entityTenantScopeList.Add(entityMetaData.EntityIdentifier, new EntityTenantScope()
                    {
                        TenantId = tenantId,
                        ViewTenantScopeId = viewTenantScopeId.Equals(TenantScopeConstant.DEFAULT_TENANT_SCOPE) ? string.Empty : viewTenantScopeId,
                        EditTenantScopeId = editTenantScopeId.Equals(TenantScopeConstant.DEFAULT_TENANT_SCOPE) ? string.Empty : editTenantScopeId,
                        DeleteTenantScopeId = deleteTenantScopeId.Equals(TenantScopeConstant.DEFAULT_TENANT_SCOPE) ? string.Empty : deleteTenantScopeId,
                        EntityId = entityMetaData.EntityIdentifier,
                        ViewSelectiveTenant = string.IsNullOrEmpty(viewSelectiveTenant) ? string.Empty : viewSelectiveTenant,
                        EditSelectiveTenant = string.IsNullOrEmpty(editSelectiveTenant) ? string.Empty : editSelectiveTenant,
                        DeleteSelectiveTenant = string.IsNullOrEmpty(deleteSelectiveTenant) ? string.Empty : deleteSelectiveTenant
                    });
                }
            }
            return entityTenantScopeList;
        }

        /// <summary>
        /// This method is used to populates the tenant based on the given tenant identifier and entity identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="selectedTenetIds">selected tenant identifiers</param>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public ActionResult PopulateTenant(string tenantId,string selectedTenetIds, string entityId)
        {
            ViewData["EntityId"] = entityId;
            string[] entityArr = entityId.Split('_');
            ViewData["SelectedTenantDetails"] = selectedTenetIds;
            ViewData["TenantDetails"] = TenantRelationProxy.GetAllChildTenants(tenantId);
            EntityTenantScope entityTenantScope = new EntityTenantScope();
            entityTenantScope = TenantAccessProxy.GetEntityTenantScopeByEntity(entityArr[2].ToString(), tenantId);
            ViewData["EntityTenantScope"] = entityTenantScope;
            return PartialView("PopulateTenant", entityTenantScope);
        }
    }
}
