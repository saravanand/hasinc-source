﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CelloSaaSApplication.Controllers
{   
    /// <summary>
    /// This class is responsible for handling the errors.
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// This method is used to get the error index page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }

            return View();
        }

        /// <summary>
        /// This method is used to get the PageNotFound page when the requested page or url is not found.
        /// </summary>
        /// <param name="url">url.</param>
        /// <returns></returns>
        public ActionResult PageNotFound(string url)
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;

            // If the url is relative ('NotFound' route) then replace with Requested path
            ViewBag.RequestedUrl = !string.IsNullOrEmpty(url) && Request.Url.OriginalString.Contains(url) & Request.Url.OriginalString != url ? Request.Url.OriginalString : url;
            // Dont get the user stuck in a 'retry loop' by
            // allowing the Referrer to be the same as the Request
            ViewBag.ReferrerUrl = Request.UrlReferrer != null && Request.UrlReferrer.OriginalString != ViewBag.RequestedUrl ? Request.UrlReferrer.OriginalString : null;

            return View();
        }

         /// <summary>
        /// This method is used to get the InvalidAction page when error occurred.
         /// </summary>
         /// <param name="actionName">action name</param>
         /// <returns></returns>
        public ActionResult InvalidAction(string actionName)
        {
            ViewData["ErrorMessage"] = "You are unauthorized to access the URL";
            return View();
        }

        /// <summary>
        /// This method is used to get the InvalidRequest page when error occurred.
        /// </summary>
        /// <returns></returns>
        public ActionResult InvalidRequest()
        {
            ViewData["InvalidRequest"] = true;
            return View("Index");
        }

        /// <summary>
        ///  This method is used to get the AccessDenied page when trying to access an unauthorized resource.
        /// </summary>
        /// <returns></returns>
        public ActionResult AccessDenied()
        {
            return View();
        }
    }
}
