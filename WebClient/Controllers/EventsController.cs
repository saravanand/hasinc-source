﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.EventScheduler.Model;
using CelloSaaS.EventScheduler.ServiceContracts;
using CelloSaaS.EventScheduler.ServiceProxies;
using CelloSaaS.Integration;
using CelloSaaS.Integration.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.Template.Model;
using CelloSaaS.Template.ServiceProxies;
using CelloSaaS.View;
using CelloSaaS.WorkFlow.Model;
using CelloSaaS.WorkFlow.ServiceProxies;
using CelloSaaS.WorkFlow.ServiceContracts;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for managing the events.
    /// </summary>
    public class EventsController : CelloSaaS.View.CelloController
    {
        private const string policyName = "EventSchedulerExceptionLogger";
        private string TenantId = TenantContext.GetTenantId("_CelloEvents");
        private string UserId = UserIdentity.UserId;

        /// <summary>
        /// This method is used to load an event manage index page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This method is used to get an event details based on the event identifier.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <returns></returns>
        public PartialViewResult GetEventDetails(string eventId)
        {
            if (string.IsNullOrEmpty(eventId))
            {
                ModelState.AddModelError("Error", Resources.EventsResource.e_EventId);
            }

            EventMetadata eventDetails = null;

            if (ModelState.IsValid)
            {
                try
                {
                    eventDetails = EventServiceProxy.GetEventDetailsByEventId(eventId, this.TenantId);
                    ViewBag.EventDetails = eventDetails;
                    LoadTemplateList(eventDetails != null ? eventDetails.TemplateId : string.Empty);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    ModelState.AddModelError("Error", argumentNullException.Message);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    ModelState.AddModelError("Error", argumentException.Message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (EventActivityException eventDetailsException)
                {
                    ExceptionService.HandleException(eventDetailsException, policyName);
                    ModelState.AddModelError("Error", eventDetailsException.Message);
                }
                catch (TemplateException templateException)
                {
                    ExceptionService.HandleException(templateException, policyName);
                    ModelState.AddModelError("Error", templateException.Message);
                }
            }

            return PartialView(eventDetails);
        }

        /// <summary>
        /// This method is used to load the template list based on the template identifier.
        /// </summary>
        /// <param name="templateId">template identifier.</param>
        private void LoadTemplateList(string templateId)
        {
            Dictionary<string, TemplateDetail> templateDetails = TemplateProxy.GetAllTemplateDetailsByCategory(this.TenantId, "EventTemplate");

            if (templateDetails != null && templateDetails.Count > 0)
            {
                ViewData["TemplateList"] = new SelectList((IEnumerable)templateDetails.Values.OrderBy(x => x.TemplateName), "TemplateId", "TemplateName", templateId);
            }
            else
            {
                ViewData["TemplateList"] = new List<SelectListItem>() { new SelectListItem { Text = Resources.EventsResource.opt_TemplateNotFound, Value = "", Selected = true } };
            }
        }

        /// <summary>
        /// This method is used to add the event job details.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <param name="jobId">job identifier.</param>
        /// <param name="wfJobParam">workflow job parameters.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult AddEventJob(string eventId, string jobId, WFJobParameter wfJobParam)
        {
            if (string.IsNullOrEmpty(eventId))
            {
                return Json(new { Error = Resources.EventsResource.e_EventIdRequest });
            }

            ViewData["eventId"] = eventId;
            ViewData["jobId"] = jobId;
            ViewData["wfJobParam"] = wfJobParam;

            ValidateJobParameter(wfJobParam);

            if (ModelState.IsValid)
            {
                try
                {
                    EventSchedulerProxy.AddEventJob(eventId, jobId, this.TenantId);

                    var jparam = new JobParameters
                    {
                        EventId = eventId,
                        JobId = jobId,
                        EventJobParameter = wfJobParam,
                        TenantId = this.TenantId,
                        CreatedBy = this.UserId,
                        Status = true
                    };

                    JobParameterServiceProxy.AddJobParameter(jparam);

                    return Json(new { Success = Resources.EventsResource.s_AddEventJob });
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    return Json(new { Error = argumentNullException.Message });
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    return Json(new { Error = argumentException.Message });
                }
                catch (EventScheduleException eventSheduleException)
                {
                    ExceptionService.HandleException(eventSheduleException, policyName);
                    return Json(new { Error = eventSheduleException.Message });
                }
                catch (EventActivityException activityEventLogException)
                {
                    ExceptionService.HandleException(activityEventLogException, policyName);
                    return Json(new { Error = activityEventLogException.Message });
                }

            }

            return Json(new { Error = ModelState.Values.SelectMany(x => x.Errors) });
        }

        /// <summary>
        /// This method is used to update the job parameter details.
        /// </summary>
        /// <param name="id">unique identifier.</param>
        /// <param name="eventId">event identifier.</param>
        /// <param name="jobId">job identifier.</param>
        /// <param name="wfJobParam">workflow identifier.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult UpdateJobParameter(string id, string eventId, string jobId, WFJobParameter wfJobParam)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(eventId) || string.IsNullOrEmpty(jobId))
            {
                return Json(new { Error = Resources.EventsResource.e_Request });
            }

            ValidateJobParameter(wfJobParam);

            if (ModelState.IsValid)
            {
                try
                {
                    var jparam = new JobParameters
                    {
                        JobParameterId = id,
                        EventId = eventId,
                        JobId = jobId,
                        EventJobParameter = wfJobParam,
                        TenantId = this.TenantId,
                        UpdatedBy = this.UserId,
                        Status = true
                    };

                    JobParameterServiceProxy.UpdateJobParameter(jparam);

                    return Json(new { Success = Resources.EventsResource.s_UpdateJobParameter });
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    return Json(new { Error = argumentNullException.Message });
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    return Json(new { Error = argumentException.Message });
                }
                catch (EventActivityException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    return Json(new { Error = ex.Message });
                }
            }

            return Json(new { Error = ModelState.Values.SelectMany(x => x.Errors) });
        }

        /// <summary>
        /// This method is used to validation method for job parameter.
        /// </summary>
        /// <param name="wfJobParam">workflow parameters.</param>
        private void ValidateJobParameter(WFJobParameter wfJobParam)
        {
            if (string.IsNullOrEmpty(wfJobParam.WFName))
            {
                ModelState.AddModelError("WFName", Resources.EventsResource.e_WFName);
            }

            /*if (string.IsNullOrEmpty(wfJobParam.ServiceEndpointId))
            {
                ModelState.AddModelError("ServiceEndpointId", Resources.EventsResource.e_ServiceEndPoint);
            }
             
            if (string.IsNullOrEmpty(wfJobParam.XSLString))
            {
                ModelState.AddModelError("XSLString", Resources.EventsResource.e_Xsl);
            }*/

            if (string.IsNullOrEmpty(wfJobParam.MapIdXPath))
            {
                ModelState.AddModelError("MapIdXPath", Resources.EventsResource.e_MapIdXPath);
            }
        }

        /// <summary>
        /// This method is sued to load the job parameter partial page .
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <param name="jobId">job identifier.</param>
        /// <returns></returns>
        public PartialViewResult GetJobParameter(string eventId, string jobId)
        {
            if (string.IsNullOrEmpty(eventId))
            {
                ModelState.AddModelError("Error", Resources.EventsResource.e_EventId);
            }

            if (string.IsNullOrEmpty(jobId))
            {
                ModelState.AddModelError("Error", Resources.EventsResource.e_JobId);
            }

            ViewBag.eventId = eventId;
            ViewBag.jobId = jobId;

            JobParameters jobParam = null;
            WFJobParameter wfparam = null;

            if (ModelState.IsValid)
            {
                try
                {
                    jobParam = JobParameterServiceProxy.GetJobParameterDetails(eventId, jobId, this.TenantId);

                    if (jobParam != null && !string.IsNullOrEmpty(jobParam.ParameterXML))
                    {
                        ViewBag.id = jobParam.JobParameterId;
                        wfparam = CelloXmlSerializer.DeserializeWithType(jobParam.ParameterXML) as WFJobParameter;

                        if (wfparam != null)
                        {
                            LoadServiceEndpointList(wfparam.ServiceEndpointId);
                            LoadWorkflowDetails(wfparam.WFName);
                        }
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    ModelState.AddModelError("Error", argumentNullException.Message);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    ModelState.AddModelError("Error", argumentException.Message);
                }
                catch (EventActivityException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return PartialView(wfparam);
        }

        /// <summary>
        /// This method is used to load the event job details view page.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <returns></returns>
        public PartialViewResult GetEventJobDetails(string eventId)
        {
            if (string.IsNullOrEmpty(eventId))
            {
                ModelState.AddModelError("Error", Resources.EventsResource.e_EventId);
            }

            ViewData["eventId"] = eventId;
            ViewData["jobId"] = "ab860f9a-74b4-e111-98c8-000000000000";

            if (ModelState.IsValid)
            {
                try
                {
                    LoadServiceEndpointList();

                    LoadWorkflowDetails();

                    var scheduleIds = EventSchedulerProxy.GetSchedules(eventId, this.TenantId);

                    if (scheduleIds != null && scheduleIds.Count > 0)
                    {
                        ViewBag.EventJobs = SchedulerProxy.GetJobs(scheduleIds.Select(x => x.Key).ToArray(), this.TenantId);
                    }
                }
                catch (CelloServiceEndpointException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    ModelState.AddModelError("Error", argumentNullException.Message);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    ModelState.AddModelError("Error", argumentNullException.Message);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    ModelState.AddModelError("Error", argumentException.Message);
                }
                catch (ScheduleException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (JobException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to load the work flow details.
        /// </summary>
        /// <param name="selectedId">selected identifier.</param>
        private void LoadWorkflowDetails(string selectedId = null)
        {
            var selectList = new List<SelectListItem>();
            try
            {
                List<WorkflowModel> workflowList = WorkflowServiceProxy.GetWorkflows(UserIdentity.TenantID);
                if (workflowList != null && workflowList.Count > 0)
                {
                    selectList.AddRange(new SelectList(workflowList.OrderBy(x => x.Name), "Name", "Name", selectedId));
                }
            }
            catch (WorkFlowException wfException)
            {
                ExceptionService.HandleException(wfException, policyName);
            }

            selectList.Insert(0, new SelectListItem { Value = "", Text = "--Select--" });
            selectList.Add(new SelectListItem { Value = "-1", Text = "--" + Resources.EventsResource.CreateNewWorkflow + "--" });
            ViewData["WFName"] = selectList;
        }

        /// <summary>
        /// This method is used to load the service end point list.
        /// </summary>
        /// <param name="selectedId">selected identifier.</param>
        private void LoadServiceEndpointList(string selectedId = null)
        {
            var selectList = new List<SelectListItem>();

            try
            {
                var eps = CelloServiceEndpointProxy.GetServiceEndpointByTenantId(this.TenantId);

                if (eps != null)
                {
                    selectList.AddRange(new SelectList(eps.Values.OrderBy(x => x.Name), "Id", "Name", selectedId));
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                // fail safe it the tenant does not have integration module
                ExceptionService.HandleException(ex, policyName);
            }
            catch (CelloServiceEndpointException ex)
            {
                // fail safe it the tenant does not have integration module
                ExceptionService.HandleException(ex, policyName);
            }

            selectList.Insert(0, new SelectListItem { Value = "", Text = "--Select--" });

            ViewData["ServiceEndpointId"] = selectList;
        }

        /// <summary>
        /// This method is used to delete the event job details by event identifier and job identifier.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <param name="jobId">job identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteEventJob(string eventId, string jobId)
        {
            if (!string.IsNullOrEmpty(eventId) && !string.IsNullOrEmpty(jobId))
            {
                try
                {
                    EventSchedulerProxy.DeleteEventJob(eventId, jobId, this.TenantId);
                    return Json(new { Success = Resources.EventsResource.s_DeleteEventJob });
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    return Json(new { Error = argumentNullException.Message });
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    return Json(new { Error = argumentException.Message });
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (EventDetailsException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    return Json(new { Error = ex.Message });
                }
            }

            return Json(new { Error = Resources.EventsResource.e_DeleteEventJob }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to load the partial event page based on the event identifier.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <returns></returns>
        public PartialViewResult ManageEvent(string eventId)
        {
            EventMetadata eventDetails = null;

            try
            {
                if (!string.IsNullOrEmpty(eventId))
                {
                    eventDetails = EventServiceProxy.GetEventDetailsByEventId(eventId, this.TenantId);
                }
                else
                {
                    eventDetails = new EventMetadata();
                }

                LoadTemplateList(eventDetails != null ? eventDetails.TemplateId : string.Empty);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                ModelState.AddModelError("Error", argumentNullException.Message);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                ModelState.AddModelError("Error", argumentException.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (EventActivityException eventActivityException)
            {
                ExceptionService.HandleException(eventActivityException, policyName);
                ModelState.AddModelError("Error", eventActivityException.Message);
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, policyName);
                ModelState.AddModelError("Error", templateException.Message);
            }

            return PartialView(eventDetails);
        }

        /// <summary>
        /// This method is used to load the manage event page.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageEvent(FormCollection formCollection)
        {
            EventMetadata eventMetadata = new EventMetadata();

            try
            {
                TryUpdateModel(eventMetadata);

                string templateId = string.Empty;

                if (formCollection["TemplateList"] != null)
                {
                    eventMetadata.TemplateId = templateId = formCollection["TemplateList"].ToString();
                }

                if (string.IsNullOrEmpty(eventMetadata.EventName))
                {
                    ModelState.AddModelError("EventName", Resources.EventsResource.e_EventName);
                }

                LoadTemplateList(eventMetadata.TemplateId);

                if (ModelState.IsValid)
                {
                    bool isGlobal = bool.Parse(formCollection["IsGlobal"] ?? "false");
                    eventMetadata.TenantId = (UserIdentity.IsInRole(RoleConstants.ProductAdmin) && isGlobal) ? string.Empty : this.TenantId;

                    if (string.IsNullOrEmpty(eventMetadata.EventId))
                    {
                        string eventId = EventServiceProxy.AddEventMetadata(eventMetadata);
                        EventTemplateServiceProxy.MapEventTemplate(eventId, templateId, this.TenantId);

                        return Json(new { Success = Resources.EventsResource.s_CreateEvent });
                    }
                    else
                    {
                        EventServiceProxy.UpdateEventMetadata(eventMetadata);
                        EventTemplateServiceProxy.MapEventTemplate(eventMetadata.EventId, templateId, this.TenantId);

                        return Json(new { Success = Resources.EventsResource.s_updateEvent });
                    }
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                ModelState.AddModelError("Error", argumentNullException.Message);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                ModelState.AddModelError("Error", argumentException.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, policyName);
                ModelState.AddModelError("Error", templateException.Message);
            }
            catch (DuplicateEventNameException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (EventActivityException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (EventTemplateException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView(eventMetadata);
        }

        /// <summary>
        /// This method is used to delete the event based on the given event identifier.
        /// </summary>
        /// <param name="eventId">event identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteEvent(string eventId)
        {
            if (!string.IsNullOrEmpty(eventId))
            {
                try
                {
                    if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                    {
                        // only product admin can delete events
                        var eventDetails = EventServiceProxy.GetEventDetailsByEventId(eventId, UserIdentity.TenantID);

                        if (eventDetails == null)
                        {
                            return Json(new { Error = "Invalid EventId are passed" });
                        }

                        if (string.IsNullOrEmpty(eventDetails.TenantId))
                        {
                            return Json(new { Error = "Sorry!, global event should not be delete." });
                        }

                        EventServiceProxy.DeleteEventMetadata(eventId);
                        return Json(new { Success = Resources.EventsResource.s_DeleteEvent });
                    }
                    else
                    {
                        return Json(new { Error = Resources.EventsResource.e_DeleteAccessDenied });
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    return Json(new { Error = argumentNullException.Message });
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    return Json(new { Error = argumentException.Message });
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    return Json(new { Error = ex.Message });
                }
                catch (EventDetailsException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    return Json(new { Error = ex.Message });
                }
            }

            return Json(new { Error = Resources.EventsResource.e_DeleteEvent });
        }

        /// <summary>
        /// This method is used to get all events based on the given tenant identifier.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllEventDetails()
        {
            try
            {
                var events = EventServiceProxy.GetAllEvents(this.TenantId);

                if (events != null)
                {
                    // group by events category
                    var categoryEvents = new Dictionary<string, List<EventMetadata>>();

                    foreach (var item in events.Values)
                    {
                        var category = item.Category ?? "Default";

                        if (!categoryEvents.ContainsKey(category))
                        {
                            categoryEvents.Add(category, new List<EventMetadata>());
                        }

                        categoryEvents[category].Add(item);
                    }

                    List<object> lstevnts = new List<object>();

                    foreach (var item in categoryEvents)
                    {
                        var childs = item.Value.Select(x =>
                        new
                        {
                            data = new { title = x.EventName, icon = "/Content/images/event-ico.png" },
                            attr = new { id = x.EventId, title = x.Description },
                            state = "",
                        });

                        lstevnts.Add(new
                        {
                            data = new { title = item.Key, icon = "/Content/images/category-ico.png" },
                            attr = new { @class = "category", rel = "category", id = item.Key },
                            children = childs,
                            state = "opened"
                        });
                    }

                    var root = new
                    {
                        data = new { title = "Events", icon = "/Content/images/events-root-ico.png" },
                        attr = new { @class = "root", rel = "root", id = "events" },
                        children = lstevnts,
                        state = "opened"
                    };

                    return Json(root, JsonRequestBehavior.AllowGet);
                }
            }
            catch (EventActivityException ex)
            {
                ExceptionService.HandleException(ex, policyName);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to searches the event audit.
        /// </summary>
        /// <returns></returns>
        public ActionResult SearchEventAudit()
        {
            try
            {
                LoadEventList();

                var eventAuditDetails = GetEventAudit(new EventAuditSearchCondition(), 1, "EventAudit_CreatedOn", SortExpressionConstants.Decending, 10);
                string[] userIDs = eventAuditDetails != null && eventAuditDetails.TotalCount > 0 ? eventAuditDetails.Result.Select(c => c.Value.UserId).ToArray() : null;

                LoadUserList(userIDs);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (EventActivityException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to loads the event list.
        /// </summary>
        private void LoadEventList()
        {
            var events = EventServiceProxy.GetAllEvents(this.TenantId);

            var eventSelectList = new List<SelectListItem>();

            if (events != null && events.Count > 0)
            {
                eventSelectList.Add(new SelectListItem
                {
                    Text = "All",
                    Value = ""
                });

                foreach (var e in events)
                {
                    eventSelectList.Add(new SelectListItem
                    {
                        Text = e.Value.EventName,
                        Value = e.Value.EventId
                    });
                }
            }
            else
            {
                eventSelectList.Add(new SelectListItem
                {
                    Text = Resources.EventsResource.e_Event,
                    Value = ""
                });
            }

            ViewData["EventId"] = eventSelectList;
        }

        /// <summary>
        /// This method is used to search the event audit details.
        /// </summary>
        /// <param name="eventAuditsearchcondition">event audit search condition.</param>
        /// <param name="page">page.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="pageSize">page size.</param>
        /// <returns></returns>
        [HttpPost]
        [DisableTrackUsage]
        public PartialViewResult SearchEventAudit(EventAuditSearchCondition eventAuditsearchcondition, int? page, string sortString, string sortDirection, int pageSize = 10)
        {
            EventAuditSearchResult eventAuditDetails = null;

            try
            {
                eventAuditDetails = GetEventAudit(eventAuditsearchcondition, page, sortString, sortDirection, pageSize);
                string[] userIDs = eventAuditDetails != null && eventAuditDetails.TotalCount > 0 ? eventAuditDetails.Result.Select(c => c.Value.UserId).ToArray() : null;
                LoadUserList(userIDs);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                ModelState.AddModelError("Error", argumentNullException.Message);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                ModelState.AddModelError("Error", argumentException.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (EventActivityException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView("ActivityStreamView", eventAuditDetails != null && eventAuditDetails.Result != null ? eventAuditDetails.Result.Values.ToList() : null);
        }

        /// <summary>
        /// This method is used to get the event audit details(server side pagination).
        /// </summary>
        /// <param name="eventAuditsearchcondition">event audit search condition</param>
        /// <param name="page">page</param>
        /// <param name="sortString">sort string</param>
        /// <param name="sortDirection">sort direction</param>
        /// <param name="pageSize">page size</param>
        /// <returns></returns>
        private EventAuditSearchResult GetEventAudit(EventAuditSearchCondition eventAuditsearchcondition, int? page, string sortString, string sortDirection, int pageSize = 10)
        {
            if (!AccessControlProxy.CheckPermission(PrivilegeConstants.ViewEventAudit))
            {
                ModelState.AddModelError("Error", Resources.EventsResource.e_ViewEventDenied);
                return null;
            }

            sortString = string.IsNullOrEmpty(sortString) ? EventSortStringConstants.EventAuditCreatedOn : sortString;
            eventAuditsearchcondition.TenantId = this.TenantId;
            eventAuditsearchcondition.SortString = sortString;
            eventAuditsearchcondition.SortExpression = string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(SortExpressionConstants.Decending) ? SortExpressionConstants.Decending : SortExpressionConstants.Ascending;
            eventAuditsearchcondition.SetTotalCount = true;

            eventAuditsearchcondition.RecordStart = (!page.HasValue || page.Equals(1) ? 1 : ((int)((page - 1) * pageSize) + 1));
            eventAuditsearchcondition.RecordEnd = (!page.HasValue ? pageSize : ((int)page * pageSize));
            ViewData["eventAuditSearchCondition"] = eventAuditsearchcondition;

            var eventAuditSearchResult = EventAuditServiceProxy.GetEventAuditDetails(eventAuditsearchcondition);

            ViewData["page"] = !page.HasValue ? 1 : (int)page;
            ViewData["PageNumber"] = !page.HasValue ? 1 : (int)page;
            ViewData["PageSize"] = pageSize;
            ViewData["SortString"] = sortString;
            ViewData["SortDirection"] = eventAuditsearchcondition.SortExpression;
            ViewData["TotalCount"] = eventAuditSearchResult != null ? eventAuditSearchResult.TotalCount : 0;
            ViewData["EventAuditDetails"] = eventAuditSearchResult != null && eventAuditSearchResult.Result != null ? eventAuditSearchResult.Result.Values.ToList() : null;

            return eventAuditSearchResult;
        }

       /// <summary>
        /// This method is used to load the user name for the user identifier for display purpose.
       /// </summary>
       /// <param name="userIDs">list of user identifiers</param>
        public void LoadUserList(string[] userIDs)
        {
            ViewBag.UserList1 = userIDs != null && userIDs.Count() > 0 ? UserDetailsProxy.GetUserNameForAudit(userIDs) : null;
        }

        #region Test

        [ValidateInput(false)]
        public ActionResult TestEvent(string eventName, string subjectXmlValue)
        {
            if (!string.IsNullOrEmpty(eventName) && !string.IsNullOrEmpty(subjectXmlValue)
                && Request.HttpMethod.ToUpperInvariant() == "POST")
            {
                ViewBag.eventName = eventName;
                ViewBag.subjectXmlValue = subjectXmlValue;

                try
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(subjectXmlValue); // validate xml input

                    EventRegisterProxy.RegisterEvent(new Event
                    {
                        EventName = eventName,
                        SubjectXmlValue = subjectXmlValue,
                        UserId = UserIdentity.UserId,
                        TenantId = UserIdentity.TenantID
                    });

                    ModelState.AddModelError("Success", string.Format(CultureInfo.InvariantCulture, "'{0}' Event has been successfully triggered!", eventName));
                }
                catch (EventDetailsException ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (XmlException ex)
                {
                    ModelState.AddModelError("subjectXmlValue", "Input Xml is not valid! Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.ToString());
                }
            }
            else if (Request.HttpMethod.ToUpperInvariant() == "POST")
            {
                if (string.IsNullOrEmpty(eventName))
                {
                    ModelState.AddModelError("Error", "Event name is required!");
                }

                if (string.IsNullOrEmpty(subjectXmlValue))
                {
                    ModelState.AddModelError("Error", "Valid Input Xml is required!");
                }
            }

            return View();
        }

        #endregion
    }
}
