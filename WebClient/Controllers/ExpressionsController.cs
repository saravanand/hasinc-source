﻿namespace CelloSaaSApplication.Controllers
{
    using System;
    using System.Linq;
    using System.Data;
    using System.Web.Mvc;    
    using System.Collections.Generic;
    using CelloSaaS.ExpressionBuilder.ServiceProxies;
    using CelloSaaS.ExpressionBuilder.Inputs;
    using CelloSaaS.ExpressionBuilder.Helpers;
    using CelloSaaS.ExpressionBuilder.Functions;
    using System.Globalization;
    using CelloSaaS.View;

    /// <summary>
    /// This class is responsible for managing the expressions.
    /// </summary>
    public class ExpressionsController : CelloController
    {
        /// <summary>
        /// This method is used to gets all functions.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllFunctions()
        {
            var functionsList = GetFunctions(null);

            return (functionsList == null || functionsList.Count < 1)
                       ? Json(new { Error = "Exception occured while fetching the functions" })
                       : Json(functionsList);
        }

        /// <summary>
        /// This method is used to gets the function definition based on the given function name.
        /// </summary>
        /// <param name="functionName">name of the function.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetFunctionDefinition(string functionName)
        {
            return string.IsNullOrEmpty(functionName)
                ? this.Json(new { Error = "Invalid Function Name" })
                : this.Json(FunctionServiceProxy.GetFunctionByFunctionName(functionName));
        }

        /// <summary>
        /// This method is used to manages the expressions.
        /// </summary>
        /// <param name="expressions">expressions.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ManageExpressions(List<CelloSaaS.ExpressionBuilder.ViewModels.Expression> expressions)
        {
            if (expressions == null || expressions.Count < 1)
            {
                return Json(new { Error = "No expressions received" });
            }

            //var sqlString = string.Empty;
            //SqlDbType returnType = SqlDbType.Variant;

            List<ExpressionColumn> expressionColumn = new List<ExpressionColumn>();

            foreach (var expression in expressions)
            {
                // for product admin's expr's
                if (expression.IsFreeFlowExpression)
                {
                    continue;
                }

                var functions = expression.FunctionList;

                if (functions == null || functions.Count < 1)
                {
                    return Json(new { Error = string.Format(CultureInfo.InvariantCulture,"No Functions Received for the expression {0}", expression.Name) });
                }
            }

            var expressionObjectManager = new ExpressionManager();

            try
            {
                var expressionCols = expressionObjectManager.BuildExpressionFromViewModel(expressions);

                if (expressionCols != null && expressionCols.Count > 0)
                {
                    expressionColumn.AddRange(expressionCols);
                }
            }
            catch (ArgumentException argumentException)
            {
                return Json(new { Error = string.Format(CultureInfo.InvariantCulture,"The exception {0} occured while processing functions", argumentException.Message) });
            }
            catch (Exception exception)
            {
                return Json(new { Error = string.Format(CultureInfo.InvariantCulture,"The exception {0} occured while processing functions", exception.Message) });
            }

            var returnValues = from e in expressionColumn
                               select new { eName = e.Name, eOrdinal = e.Ordinal, TranslatedSQL = e.TranslatedContent, exprType = e.Type, exprFields = e.ExpressionFields, isFreeFlowExpression = e.IsFreeFlowExpression };

            return Json(returnValues);
            //return Json(new { TranslatedSQL = sqlString, Type = returnType });
        }

        /// <summary>
        /// This method is used to gets the return type of the functions.
        /// </summary>
        /// <param name="returnType">return type.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetFunctionsByType(string returnType)
        {
            var functionsList = GetFunctions(returnType);

            return (functionsList == null || functionsList.Count < 1)
                       ? Json(new { Error = "No matching functions found" })
                       : Json(functionsList);
        }

        /// <summary>
        /// This method is used to gets the functions based on the given return type.
        /// </summary>
        /// <param name="returnType">return type.</param>
        /// <returns>collection of function</returns>
        private static Dictionary<string, IFunction> GetFunctions(string returnType)
        {
            if (string.IsNullOrEmpty(returnType))
            {
                return (FunctionServiceProxy.GetAllFunctions());
            }

            var fnReturnType = (SqlDbType)Enum.Parse(typeof(SqlDbType), returnType);

            return (FunctionServiceProxy.GetFunctionsByReturnType(fnReturnType));
        }
    }
}
