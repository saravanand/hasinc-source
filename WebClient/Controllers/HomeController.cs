﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.EventScheduler.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaSApplication.Models;
using System.Security.Claims;
using System.Threading;
using System.Transactions;
using System.Net.Http;
using System.Web;
using Microsoft.AspNet.Identity;


namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for home page.
    /// </summary>
    [HandleError]
    public class HomeController : CelloSaaS.View.CelloController
    {
        private const string policyName = "GlobalExceptionLogger";

        /// <summary>
        /// This method is used to view the home index page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            TenantContext.Clear();

            if ((TempData["RedirectToSessionTenant"] != null && (bool)TempData["RedirectToSessionTenant"]) || UserIdentity.Privileges == null)
            {
                return RedirectToAction("SessionTenant", "Home");
            }

            return View();
        }

        #region Session Tenant

        /// <summary>
        /// This method is used to choose a session tenant.
        /// </summary>
        /// <returns></returns>
        public ActionResult SessionTenant()
        {
            if (!string.IsNullOrEmpty(UserIdentity.SessionTenantID))
            {
                return View();
            }

            var currentTenant = TenantProxy.GetTenantInfo(UserIdentity.LoggedInUserTenantId);
            ViewBag.currentTenant = currentTenant;

            Dictionary<string, TenantDetails> stratifiedTenantDetails = TenantProxy.GetStratifiedTenantsByUser(UserIdentity.UserId, UserIdentity.LoggedInUserTenantId);

            if (stratifiedTenantDetails == null || stratifiedTenantDetails.Count < 1)
            {
                return View();
            }

            var consolidatedTenantDetails = new List<TenantDetails>();

            if (stratifiedTenantDetails != null && stratifiedTenantDetails.Count > 0)
            {
                // self tenant mapping is removed, but self tenant linking is not allowed, so no need to check that
                if (stratifiedTenantDetails.ContainsKey(UserIdentity.TenantID))
                {
                    stratifiedTenantDetails.Remove(UserIdentity.TenantID);
                }
            }

            foreach (var tenantDetails in stratifiedTenantDetails.Where(td => td.Value.ApprovalStatus.Equals("Approved", StringComparison.OrdinalIgnoreCase)))
            {
                IEnumerable<TenantDetails> found = consolidatedTenantDetails.Where(t => t.TenantCode.Equals(tenantDetails.Key, StringComparison.OrdinalIgnoreCase));

                if (found != null && found.Count() > 0)
                {
                    continue;
                }
                consolidatedTenantDetails.Add(tenantDetails.Value);
            }

            if (stratifiedTenantDetails != null && stratifiedTenantDetails.Count == 1 && stratifiedTenantDetails.ContainsKey(UserIdentity.TenantID))
                return RedirectToAction("Index", "Home");

            if (consolidatedTenantDetails == null || consolidatedTenantDetails.Count < 1)
            {
                return RedirectToAction("Index", "Home");
                //ModelState.AddModelError("Error", Resources.HomeResource.e_PrimaryTenantAllowed);
                //return View();
            }

            var lstTenants = new List<TenantViewModel>();

            string[] childTenantIds = consolidatedTenantDetails.Select(x => x.TenantCode).ToArray();
            var gchildTenantIds = childTenantIds.Select(x => Guid.Parse(x)).ToArray();

            var searchResult = TenantProxy.GetTenantDetailsForShareUsers(childTenantIds);

            foreach (var item in consolidatedTenantDetails)
            {
                var tenantId = item.TenantCode;
                Guid gTenantId = Guid.Parse(tenantId);

                if (searchResult == null || !searchResult.ContainsKey(tenantId)) continue;

                var tenant = searchResult[tenantId];

                lstTenants.Add(new TenantViewModel
                {
                    TenantDetails = item
                });
            }

            ViewData["TenantList"] = lstTenants;
            ViewData["sessionTenantId"] = TempData["SessionTenant"];

            return View();
        }

        /// <summary>
        /// This method is used to sets the session tenant based on the given session tenant identifier.
        /// </summary>
        /// <param name="sessionTenantId">session tenant identifier.</param>
        /// <returns></returns>
        public ActionResult SetSessionTenant(string sessionTenantId)
        {
            if (string.IsNullOrEmpty(sessionTenantId))
            {
                TempData["Error"] = Resources.HomeResource.e_chooseAValidTenant;
                return RedirectToAction("SessionTenant");
            }
            try
            {

                var tenantLicense = LicenseProxy.GetTenantLicense(sessionTenantId);

                if (tenantLicense == null)
                {
                    tenantLicense = LicenseProxy.GetInActiveTenantLicense(sessionTenantId);
                }

                if (tenantLicense != null && tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value < DateTime.Now)
                {
                    TempData["Error"] = string.Format(Resources.HomeResource.e_TenantLicenseExpired, tenantLicense.ValidityEnd.Value.ToUIDateTimeString());
                    TempData["SessionTenant"] = sessionTenantId;
                    return RedirectToAction("SessionTenant");
                }

                // get the roles in case the user and the session tenant are all linked other than the stratification
                List<string> linkedUserRoles = new List<string>();

                FillUserRoles(sessionTenantId, linkedUserRoles);

                // get the roles for this userid for the given session tenant
                string[] sessionRoles = linkedUserRoles.ToArray();

                if (sessionRoles == null || sessionRoles.Length < 1)
                {
                    TempData["Error"] = Resources.HomeResource.e_RolesNotAvailable;
                    TempData["SessionTenant"] = sessionTenantId;
                    return RedirectToAction("SessionTenant");
                }

                // we swap the primary tenant and session tenant roles and tenant identifiers here
                /*var cookieData = new CookieData
                {
                    userName = UserIdentity.Name,
                    userKey = UserIdentity.UserId,
                    roles = UserIdentity.Roles,
                    tenantId = UserIdentity.TenantID,
                    loggedInUserTenantId = UserIdentity.LoggedInUserTenantId,
                    loggedInUserRoles = UserIdentity.LoggedInUserRoles,
                    sessiontenantid = sessionTenantId,
                    sessionRoles = sessionRoles
                };*/

                // add the new cookie
                /*System.Web.HttpContext.Current.Response.Cookies.Add(AuthenticationCookieHelper.GetAuthenticationCookie(cookieData));*/
                ClaimsIdentity sessionIdentity = null;

                if (Thread.CurrentPrincipal is System.Security.Claims.ClaimsPrincipal)
                {
                    ClaimsIdentity identity = ((ClaimsPrincipal)Thread.CurrentPrincipal).Identities.SingleOrDefault<ClaimsIdentity>();
                    sessionIdentity = SetSessionIdentity(identity, sessionTenantId, sessionRoles);
                }

                ResetAuthenticationInfo(sessionIdentity);

                var userDetails = UserDetailsProxy.GetUserDetailsByUserId(UserIdentity.UserId);
                EventRegisterProxy.RegisterEvent(
                               new Event
                               {
                                   EventId = CelloEventConstant.LoginEventId,
                                   TenantId = sessionTenantId,
                                   UserId = UserIdentity.UserId,
                                   SubjectId = UserIdentity.UserId,
                                   SubjectType = "UserDetails",
                                   SubjectXmlValue = userDetails.SerializeToXml()
                               });

                return RedirectToAction("Index");
            }
            catch (CelloSaaS.ServiceContracts.UserManagement.UserDetailException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = Resources.HomeResource.e_GetUserDetails;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = Resources.HomeResource.e_AccessDenied;
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = Resources.HomeResource.e_ParameterNullOrEmpty;
            }
            catch (CelloSaaS.ServiceContracts.LicenseManagement.LicenseException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = Resources.HomeResource.e_GetTenantLicence;
            }

            return RedirectToAction("SessionTenant");
        }

        /// <summary>
        /// This method is used to set the session identity.
        /// </summary>
        /// <param name="identity">identity</param>
        /// <param name="sessionTenantId">session tenant identifier</param>
        /// <param name="sessionRoles">list of session roles</param>
        /// <returns></returns>
        private ClaimsIdentity SetSessionIdentity(ClaimsIdentity identity, string sessionTenantId, string[] sessionRoles)
        {
            if (identity == null) return identity;

            var sessionClaims = new List<Claim>(identity.Claims);

            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(CelloClaimTypes.TenantId, StringComparison.OrdinalIgnoreCase)));
            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(ClaimTypes.Role, StringComparison.OrdinalIgnoreCase)));
            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(CelloClaimTypes.SessionTenantId, StringComparison.OrdinalIgnoreCase)));
            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(CelloClaimTypes.LoggedInUserTenantId, StringComparison.OrdinalIgnoreCase)));
            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(CelloClaimTypes.SessionRoles, StringComparison.OrdinalIgnoreCase)));

            sessionClaims.AddRange(
                    new List<Claim>
                        {
                            new Claim(CelloClaimTypes.LoggedInUserTenantId,UserIdentity.LoggedInUserTenantId),
                            new Claim(CelloClaimTypes.SessionTenantId, sessionTenantId),
                            new Claim(CelloClaimTypes.TenantId, sessionTenantId),
                            new Claim(ClaimTypes.Role, string.Join(",", sessionRoles)),
                            new Claim(CelloClaimTypes.SessionRoles, string.Join(",",UserIdentity.Roles))
                        });

            return new ClaimsIdentity(sessionClaims, DefaultAuthenticationTypes.ApplicationCookie);
        }

        /// <summary>
        /// This method is used to reset the session identity.
        /// </summary>
        /// <param name="identity">identity.</param>
        /// <returns></returns>
        private ClaimsIdentity ResetSessionIdentity(ClaimsIdentity identity)
        {
            if (identity == null) return identity;

            var sessionClaims = new List<Claim>(identity.Claims);
            string[] roles = RoleProxy.GetUserRolesForTenant(UserIdentity.UserId, UserIdentity.LoggedInUserTenantId, UserIdentity.LoggedInUserTenantId);

            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(CelloClaimTypes.TenantId, StringComparison.OrdinalIgnoreCase)));
            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(ClaimTypes.Role, StringComparison.OrdinalIgnoreCase)));
            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(CelloClaimTypes.SessionTenantId, StringComparison.OrdinalIgnoreCase)));
            sessionClaims.Remove(sessionClaims.FirstOrDefault(sc => sc.Type.Equals(CelloClaimTypes.SessionRoles, StringComparison.OrdinalIgnoreCase)));

            sessionClaims.AddRange(
                    new List<Claim>
                        {
                            new Claim(CelloClaimTypes.SessionTenantId, string.Empty),
                            new Claim(CelloClaimTypes.TenantId, UserIdentity.LoggedInUserTenantId),
                            new Claim(ClaimTypes.Role, string.Join(",", roles != null ? roles: UserIdentity.LoggedInUserRoles))
                        });

            return new ClaimsIdentity(sessionClaims, DefaultAuthenticationTypes.ApplicationCookie);
        }

        /// <summary>
        /// This method is used to Reset the authentication info.
        /// </summary>
        /// <param name="identity">identity.</param>
        private void ResetAuthenticationInfo(ClaimsIdentity identity)
        {
            if (identity == null) return;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted
                }))
            {
                var ctx = Request.GetOwinContext();

                ctx.Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                var expiryTimeSeconds = identity.FindFirstValue(ClaimTypes.Expiration);

                long SessionTimeOut = CelloSaaS.View.AppSettingHelper.GetCookieTimeout();

                if (!string.IsNullOrEmpty(expiryTimeSeconds))
                {
                    long.TryParse(expiryTimeSeconds, out SessionTimeOut);
                }

                ctx.Authentication.SignIn(new Microsoft.Owin.Security.AuthenticationProperties
                {
                    ExpiresUtc = new DateTimeOffset(DateTime.UtcNow.AddSeconds(SessionTimeOut))
                }, identity);
                scope.Complete();
            }
        }

        /// <summary>
        /// This method is used to fills the user roles with the following.
        /// <para>Roles obtained to this user for this tenant via stratification</para>
        /// <para>Roles obtained to this user for this tenant via the user link or user sharing</para>
        /// </summary>
        /// <param name="sessionTenantId">session tenant identification.</param>
        /// <param name="linkedUserRoles">linked user roles.</param>
        private static void FillUserRoles(string sessionTenantId, List<string> linkedUserRoles)
        {
            string roleAssigningTenantId = UserIdentity.TenantID;

            if (TenantUserAssociationProxy.CheckLinkExists(UserIdentity.UserId, sessionTenantId, CelloSaaS.Model.UserManagement.RequestStatus.Approved))
            {
                roleAssigningTenantId = sessionTenantId;
            }

            string[] stratifiedRoles = RoleProxy.GetUserRolesForTenant(UserIdentity.UserId, sessionTenantId, roleAssigningTenantId);

            if (stratifiedRoles != null && stratifiedRoles.Count() > 0)
            {
                linkedUserRoles.AddRange(stratifiedRoles);
            }
        }

        /// <summary>
        /// This method is used to resets the session tenant.
        /// </summary>
        /// <returns></returns>
        public ActionResult ResetSessionTenant()
        {
            try
            {
                var userDetails = UserDetailsProxy.GetUserDetailsByUserId(UserIdentity.UserId);
                EventRegisterProxy.RegisterEvent(
                               new Event
                               {
                                   EventId = CelloEventConstant.LogOutEventId,
                                   TenantId = UserIdentity.TenantID,
                                   UserId = UserIdentity.UserId,
                                   SubjectId = UserIdentity.UserId,
                                   SubjectType = "UserDetails",
                                   SubjectXmlValue = userDetails.SerializeToXml()
                               });
            }
            catch (CelloSaaS.ServiceContracts.UserManagement.UserDetailException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = Resources.HomeResource.e_GetUserDetails;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = Resources.HomeResource.e_AccessDenied;
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = Resources.HomeResource.e_ParameterNullOrEmpty;
            }

            ClaimsIdentity sessionIdentity = null;

            if (Thread.CurrentPrincipal is System.Security.Claims.ClaimsPrincipal)
            {
                string[] roles = RoleProxy.GetUserRolesForTenant(UserIdentity.UserId, UserIdentity.LoggedInUserTenantId, UserIdentity.LoggedInUserTenantId);
                ClaimsIdentity identity = ((ClaimsPrincipal)Thread.CurrentPrincipal).Identities.SingleOrDefault<ClaimsIdentity>();
                sessionIdentity = ResetSessionIdentity(identity);
            }

            // remove the old cookie
            //this.HttpContext.Request.Cookies.Remove(System.Web.Security.FormsAuthentication.FormsCookieName);

            // add the new cookie
            //System.Web.HttpContext.Current.Response.Cookies.Add(AuthenticationCookieHelper.GetAuthenticationCookie(cookieData));

            ResetAuthenticationInfo(sessionIdentity);

            return RedirectToAction("SessionTenant");
        }

        #endregion

        #region Tenant Startification dropdown

        /// <summary>
        /// This method is used to gets the tenants for current user.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="privilege">privilege.</param>
        /// <returns></returns>
        [CelloSaaS.View.DisableTrackUsage]
        public JsonResult GetTenantsForCurrentUser(string entityId, string privilege)
        {
            if (!string.IsNullOrEmpty(entityId) && Util.ValidateIdentifier(entityId) && entityId.StartsWith("_", StringComparison.OrdinalIgnoreCase))
            {
                return GetTenantsForCurrentUser(privilege);
            }

            try
            {
                if (!string.IsNullOrEmpty(privilege) && Util.ValidateIdentifier(privilege))
                {
                    var tenantsList = AccessControlProxy.GetTenantAccessPrivileges(UserIdentity.UserId, new string[] { privilege });

                    var tenantDetailsList = tenantsList[privilege];

                    if (tenantDetailsList != null && tenantDetailsList.Count > 0)
                    {
                        var tenantNameAndId = tenantDetailsList.OrderBy(x => x.Value.TenantName).Select(t => new
                        {
                            TenantId = t.Value.TenantCode,
                            TenantName = t.Value.TenantName
                        });

                        return Json(tenantNameAndId);
                    }
                }
            }
            catch
            {
                return Json(new { Error = Resources.ErrorMessage.e_UnableToProcess });
            }

            return Json(new { Error = Resources.ErrorMessage.e_UnableToProcess });
        }

        /// <summary>
        /// This method is used to gets the tenant for current user.
        /// </summary>
        /// <param name="privilege">privilege.</param>
        /// <returns></returns>
        [CelloSaaS.View.DisableTrackUsage]
        private JsonResult GetTenantsForCurrentUser(string privilege)
        {
            if (!string.IsNullOrEmpty(privilege) && Util.ValidateIdentifier(privilege))
            {
                try
                {
                    var tenantDetailsList = AccessControlProxy.GetTenantAccessPrivileges(UserIdentity.UserId, new string[] { privilege });

                    if (tenantDetailsList != null && tenantDetailsList.Count > 0)
                    {
                        var tenantNameAndId = tenantDetailsList[privilege].OrderBy(x => x.Value.TenantName).Select(t => new
                        {
                            TenantId = t.Value.TenantCode,
                            TenantName = t.Value.TenantName
                        });

                        return Json(tenantNameAndId);
                    }
                }
                catch
                {
                    return Json(new { Error = Resources.ErrorMessage.e_UnableToProcess });
                }
            }

            return Json(new { Error = Resources.ErrorMessage.e_UnableToProcess });
        }

        #endregion

        #region Menu dummys

        public ActionResult Admin()
        {
            return View("Index");
        }

        public ActionResult Tenant()
        {
            return View("Index");
        }

        public ActionResult DefaultAccess()
        {
            return View("Index");
        }

        public ActionResult Configuration()
        {
            return View("Index");
        }

        public ActionResult Reporting()
        {
            return View("Index");
        }

        public ActionResult Workflow()
        {
            return View("Index");
        }

        public ActionResult BusinessRule()
        {
            return View("Index");
        }

        public ActionResult MyActions()
        {
            return View("Index");
        }

        public ActionResult DefaultTemplate()
        {
            return View("Index");
        }

        #endregion
    }
}