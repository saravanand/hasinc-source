﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Introspection;
using CelloSaaS.Library;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for introspection.
    /// </summary>
    [DisableTrackUsage]
    public class IntrospectionController : Controller
    {
        //
        /// <summary>
        /// This method is used for Introspection Initialization.
        /// </summary>
        /// <param name="requestId">request identifier.</param>
        /// <returns></returns>
        public ActionResult Index(string requestId)
        {
            var model = IntrospectionDataProxy.GetIntrospectionData(requestId);
            if (!string.IsNullOrEmpty(UserIdentity.TenantID))
            {
                ViewBag.License = LicenseProxy.GetTenantLicense(UserIdentity.TenantID);
            }
            return View(model);
        }
        /// <summary>
        /// This method is used to re-evaluate and back the data.
        /// </summary>
        /// <param name="data">data.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SelectQuery(DbInterospectionData data)
        {
            DataTable model = null;
            try
            {
                model = IntrospectionDataProxy.ExecuteSelectQuery(data);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }
            return View(model);
        }
        /// <summary>
        /// This method is used to get data of each request.
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewRequests()
        {
            var model = IntrospectionDataProxy.GetAllIntrospectionData();
            return View(model);
        }
    }
}
