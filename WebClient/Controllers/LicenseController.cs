﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.License;
using CelloSaaS.License.CelloActivationService;
using System.Text.RegularExpressions;
using CelloSaaS.License.ActivationDetailsService;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class responsible for license view model.
    /// </summary>
    public class LicenseViewModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance has error.
        /// </summary>
        /// <value><c>true</c> if this instance has error; otherwise, <c>false</c>.</value>
        public bool HasError { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the system info.
        /// </summary>
        /// <value>The system info.</value>
        public string SystemInfo { get; set; }

        /// <summary>
        /// Gets or sets the activation key.
        /// </summary>
        /// <value>The activation key.</value>
        public string ActivationKey { get; set; }

        /// <summary>
        /// Gets or sets the name of the license.
        /// </summary>
        /// <value>The name of the license.</value>
        public string LicenseName { get; set; }

        /// <summary>
        /// Gets or Sets OperatingSystem value
        /// </summary>
        public string OperatingSystem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is trial.
        /// </summary>
        /// <value><c>true</c> if this instance is trial; otherwise, <c>false</c>.</value>
        public bool IsTrial { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is expired.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is expired; otherwise, <c>false</c>.
        /// </value>
        public bool IsExpired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is activated.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is activated; otherwise, <c>false</c>.
        /// </value>
        public bool IsActivated { get; set; }

        /// <summary>
        /// Gets or sets the days left.
        /// </summary>
        /// <value>The days left.</value>
        public TimeSpan DaysLeft { get; set; }

        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>The name of the company.</value>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        /// <value>The name of the customer.</value>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the email id.
        /// </summary>
        /// <value>The email id.</value>
        public string EmailId { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>The phone number.</value>
        public string PhoneNumber { get; set; }
    }

    /// <summary>
    /// This class is responsible for getting license details and validate activation.
    /// </summary>
    [HandleError]
    public class LicenseController : Controller
    {
        
        /// <summary>
        /// This method is used to get the license information page.
        /// </summary>
        /// <param name="wa">wa</param>
        /// <returns></returns>
        public ActionResult Index(string wa)
        {
            if (!CelloLicenseService.IsActivated)
            {
                ViewData["ActivationDetail"] = new ActivationDetail
                {
                    CustomerName = CelloLicenseService.CustomerName,
                    CompanyName = CelloLicenseService.CompanyName,
                    EmailId = CelloLicenseService.EmailId,
                    PhoneNumber = CelloLicenseService.PhoneNumber
                };
            }

            if (TempData["Status"] != null)
            {
                ViewData["Status"] = TempData["Status"];
            }

            if (!string.IsNullOrEmpty(wa))
            {
                if (wa == "Activate" && !CelloLicenseService.LicenseName.StartsWith("Trial", StringComparison.OrdinalIgnoreCase))
                {
                    ViewData["Status"] = CelloLicenseService.Activate();
                }
                else if (wa == "Deactivate")
                {
                    ViewData["Status"] = CelloLicenseService.Deactivate();
                }
            }

            return View(new LicenseViewModel
            {
                HasError = CelloLicenseService.HasError,
                ErrorMessage = CelloLicenseService.ErrorMessage,
                OperatingSystem = CelloLicenseService.OperatingSystemType,
                SystemInfo = CelloLicenseService.SystemInformationKey,
                ActivationKey = CelloLicenseService.ActivationKey,
                LicenseName = CelloLicenseService.LicenseName,
                IsValid = CelloLicenseService.IsValid,
                IsTrial = CelloLicenseService.IsTrial,
                IsExpired = CelloLicenseService.IsSubscriptionExpired,
                IsActivated = CelloLicenseService.IsActivated,
                DaysLeft = CelloLicenseService.SubscriptionDaysLeft,
                CompanyName = CelloLicenseService.CompanyName,
                CustomerName = CelloLicenseService.CustomerName,
                EmailId = CelloLicenseService.EmailId,
                PhoneNumber = CelloLicenseService.PhoneNumber
            });
        }

        /// <summary>
        /// This method is used to get the license information page.
        /// </summary>
        /// <param name="formCollection">form collection</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(FormCollection formCollection)
        {
            // get the details form form collection
            ActivationDetail activationDetail = new ActivationDetail();
            FillCelloSaaSLicenseDetail(activationDetail, formCollection);
            validate(activationDetail);

            if (ModelState.IsValid)
            {
                TempData["Status"] = CelloLicenseService.Activate(activationDetail);
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["ActivationDetail"] = activationDetail;
            }

            LicenseViewModel licenseViewModel = new LicenseViewModel()
            {
                HasError = !ModelState.IsValid,
                ErrorMessage = CelloLicenseService.ErrorMessage,
                OperatingSystem = CelloLicenseService.OperatingSystemType,
                SystemInfo = CelloLicenseService.SystemInformationKey,
                ActivationKey = CelloLicenseService.ActivationKey,
                LicenseName = CelloLicenseService.LicenseName,
                IsValid = CelloLicenseService.IsValid,
                IsTrial = CelloLicenseService.IsTrial,
                IsExpired = CelloLicenseService.IsSubscriptionExpired,
                IsActivated = CelloLicenseService.IsActivated,
                DaysLeft = CelloLicenseService.SubscriptionDaysLeft,
                CompanyName = CelloLicenseService.CompanyName,
                CustomerName = CelloLicenseService.CustomerName,
                EmailId = CelloLicenseService.EmailId,
                PhoneNumber = CelloLicenseService.PhoneNumber
            };

            return View(licenseViewModel);
        }

        /// <summary>
        /// This method is used to validate the Activation details.
        /// </summary>
        /// <param name="activationDetail">The activation detail.</param>
        private void validate(ActivationDetail activationDetail)
        {
            if (string.IsNullOrEmpty(activationDetail.CompanyName))
            {
                ModelState.AddModelError("CompanyName", Resources.LicenseResource.e_CompanyName);
            }
            else
            {
                if (activationDetail.CompanyName.Length < 3 || activationDetail.CompanyName.Length > 100)
                {
                    ModelState.AddModelError("CompanyName", Resources.LicenseResource.i_CompanyName);
                }
            }

            if (string.IsNullOrEmpty(activationDetail.CustomerName))
            {
                ModelState.AddModelError("CustomerName", Resources.LicenseResource.e_CustomerName);
            }
            else
            {
                if (activationDetail.CustomerName.Length < 3 || activationDetail.CustomerName.Length > 100)
                {
                    ModelState.AddModelError("CustomerName", Resources.LicenseResource.i_CustomerName);
                }
            }

            if (string.IsNullOrEmpty(activationDetail.EmailId))
            {
                ModelState.AddModelError("CustomerEmail", Resources.LicenseResource.e_CustomerEMail);
            }
            else
            {
                if (!Util.ValidateEmailId(activationDetail.EmailId))
                {
                    ModelState.AddModelError("CustomerEmail", Resources.LicenseResource.i_CustomerEMail);
                }
            }

            if (string.IsNullOrEmpty(activationDetail.PhoneNumber))
            {
                ModelState.AddModelError("PhoneNumber", Resources.LicenseResource.e_PhoneNumber);
            }
            else
            {
                if (activationDetail.PhoneNumber.Length < 5 || activationDetail.PhoneNumber.Length > 15)
                {
                    ModelState.AddModelError("PhoneNumber", Resources.LicenseResource.i_PhoneNumber);
                }
            }

        }

        /// <summary>
        /// This method is used to fill the CelloSaaS License Details
        /// </summary>
        /// <param name="activationDetail">activation detail.</param>
        /// <param name="formCollection">form collection.</param>
        private void FillCelloSaaSLicenseDetail(ActivationDetail activationDetail, FormCollection formCollection)
        {
            activationDetail.CompanyName = formCollection["CompanyName"];
            activationDetail.CustomerName = formCollection["CustomerName"];
            activationDetail.EmailId = formCollection["CustomerEmail"];
            activationDetail.PhoneNumber = formCollection["PhoneNumber"];
            activationDetail.IpAddress = Request.UserHostAddress;
        }
    }
}
