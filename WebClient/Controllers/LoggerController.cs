﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.Logger;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Notification.ServiceProxies;
using CelloSaaS.ServiceContracts.Logger;
using CelloSaaS.ServiceProxies.Logger;
using CelloSaaS.ServiceProxies.TenantManagement;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for manage application exceptions and logs.
    /// </summary>
    public class LoggerController : CelloSaaS.View.CelloController
    {
        private static LoggingSettings loggingSection = WebConfigurationManager.OpenWebConfiguration("/").GetSection("loggingConfiguration") as LoggingSettings;

        private const string DefaultPolicy = "GlobalExceptionLogger";

        private string[] AdditionSearchType = { "User Name", "Category Name", "Machine Name", "Title", "Message" };

        /// <summary>
        /// This method is used to get application log details based on the search condition.
        /// </summary>
        /// <param name="search">search condition.</param>
        /// <param name="page">page.</param>
        /// <param name="searchField">search field.</param>
        /// <param name="searchValue">search value.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="pageSize">size of the page.</param>
        /// <returns></returns>
        public ActionResult Index(CelloLogEntrySearchCondition search, int? page, string searchField, string searchValue, string sortString, string sortDirection, int pageSize = 50)
        {
            IEnumerable<CelloLogEntry> entries = null;

            List<SelectListItem> lstTenantList = new List<SelectListItem>();
            List<SelectListItem> lstTraceEventType = new List<SelectListItem>();
            Dictionary<string, int> totals = new Dictionary<string, int>();

            try
            {
                sortString = string.IsNullOrEmpty(sortString) ? "TimeStamp" : sortString;
                sortDirection = string.IsNullOrEmpty(sortDirection) ? SortExpressionConstants.Decending : sortDirection;

                search.TenantId = search.TenantId == null ? "-1" : search.TenantId;
                search.Severage = search.Severage == null ? "-1" : search.Severage;
                search.SortString = sortString;
                search.SortDirection = sortDirection;
                search.Offset = page.HasValue ? (page.Value * pageSize) - (pageSize - 1) : 1;
                // search.Offset = (page.HasValue && page.Value != 1) ? page.Value + (pageSize - 1) : 0;
                search.PageSize = pageSize;
                // search.FromDate = (search.FromDate == null && search.ToDate == null) ? DateTime.Now.AddMonths(-2) : search.FromDate.Value;
                //search.ToDate = (search.ToDate == null) ? DateTime.Now : search.ToDate.Value;

                var activeTenant = TenantProxy.GetAllActiveTenantDetails();

                activeTenant = activeTenant.OrderBy(x => x.Value.TenantName).ToDictionary(x => x.Key, x => x.Value);

                lstTenantList.Add(new SelectListItem { Value = "-1", Text = "All", Selected = search.TenantId == "-1" ? true : false });

                foreach (var item in activeTenant)
                {
                    if (search.TenantId == item.Value.TenantCode)
                    {
                        lstTenantList.Add(new SelectListItem { Value = item.Value.TenantCode, Text = item.Value.TenantName, Selected = true });
                    }
                    else
                        lstTenantList.Add(new SelectListItem { Value = item.Value.TenantCode, Text = item.Value.TenantName });
                }

                var traceEventType = ExceptionService.GetTraceEventType();

                if (traceEventType.Count > 0)
                {
                    lstTraceEventType.Add(new SelectListItem { Value = "-1", Text = "All", Selected = search.Severage == "-1" ? true : false });

                    foreach (string name in traceEventType)
                    {
                        if (search.Severage == name)
                        {
                            lstTraceEventType.Add(new SelectListItem { Value = name, Text = name, Selected = true });
                        }
                        else
                        {
                            lstTraceEventType.Add(new SelectListItem { Value = name, Text = name });
                        }
                    }
                }

                var result = LoggerProxy.SearchLog(search);

                if (result != null)
                {
                    entries = result.Items;
                    totals.Add("TotalCount", result.TotalCount);
                    totals.Add("TotalError", result.TotalError);
                    totals.Add("TotalCritical", result.TotalCritical);
                    totals.Add("TotalWarning", result.TotalWarning);
                }
            }
            catch (FormatException fex)
            {
                ExceptionService.HandleException(fex, DefaultPolicy);
                ModelState.AddModelError("Error", fex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (LoggerException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            ViewData["TenantList"] = lstTenantList;
            ViewData["PageNumber"] = !page.HasValue ? 1 : (int)page;
            ViewData["SortString"] = sortString;
            ViewData["SortDirection"] = sortDirection;
            ViewData["TotalCount"] = totals;
            ViewData["PageSize"] = pageSize;
            ViewData["search"] = search;
            ViewData["lstTraceEventType"] = lstTraceEventType;
            ViewData["AdditionalSearchType"] = GetAddtionSearchType();

            if (Request.IsAjaxRequest())
            {
                return PartialView("PartialLog", entries);
            }

            return View(entries);
        }

        /// <summary>
        /// This method is used to list the addition search type.
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> GetAddtionSearchType()
        {
            List<SelectListItem> search = new List<SelectListItem>();
            for (int i = 0; i < AdditionSearchType.Length; i++)
            {
                search.Add(new SelectListItem { Value = AdditionSearchType[i], Text = AdditionSearchType[i] });
            }
            return search;
        }

        /// <summary>
        /// This method is used to get the exception email subscriptions.
        /// </summary>
        /// <returns></returns>
        public ActionResult Subscriptions()
        {
            List<ExceptionSubscription> model = null;

            try
            {
                model = LoggerProxy.GetAllExceptionSubscriptions();
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(model);
        }

        /// <summary>
        /// This method is used to manage exception email subscription based on the given identifier.
        /// </summary>
        /// <param name="id">unique identifier.</param>
        /// <returns></returns>
        public ActionResult ManageSubscription(Guid? id)
        {
            ExceptionSubscription model = null;

            try
            {
                if (id.HasValue && id.Value != Guid.Empty)
                {
                    model = LoggerProxy.GetExceptionSubscription(id.Value);

                    if (model == null) return HttpNotFound();
                }
                else
                {
                    model = new ExceptionSubscription();
                    model.Status = true;
                }

                if (Request.HttpMethod == "POST")
                {
                    TryUpdateModel(model);
                    model.Categories = this.Request.Form["Categories"];

                    ValidateExceptionSubscriptionModel(model);

                    if (ModelState.IsValid)
                    {
                        LoggerProxy.SaveExceptionSubscription(model);
                        TempData["Success"] = string.Format(CultureInfo.CurrentCulture, "'{0}' Subscription saved successfully!", model.Name);
                        return RedirectToAction("Subscriptions");
                    }
                }

                if (loggingSection != null)
                {
                    var selectedCategories = model.Categories != null ? model.Categories.Split(',') : new string[0];
                    var categories = new List<SelectListItem>();
                    foreach (var item in loggingSection.TraceSources)
                    {
                        categories.Add(new SelectListItem { Text = item.Name, Value = item.Name, Selected = selectedCategories.Contains(item.Name) });
                    }
                    ViewBag.Categories = categories;
                }

                List<SelectListItem> lstNotificationType = new List<SelectListItem>();
                lstNotificationType.Add(new SelectListItem { Value = "", Text = "-All-" });

                var notificationDetails = NotificationConfigurationProxy.GetNotificationMasterDetailsByTenantId(UserIdentity.TenantID);
                if (notificationDetails != null)
                {
                    foreach (var notificationDetail in notificationDetails)
                    {
                        lstNotificationType.Add(new SelectListItem
                        {
                            Text = notificationDetail.NotificationName,
                            Value = notificationDetail.NotificationId
                        });
                    }
                    ViewBag.NotificationNames = lstNotificationType;
                }
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(model);
        }

        /// <summary>
        /// This method is used to validate the exception subscription model.
        /// </summary>
        /// <param name="model">exception subscription model.</param>
        private void ValidateExceptionSubscriptionModel(ExceptionSubscription model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Please enter a name");
            }

            if (string.IsNullOrEmpty(model.To))
            {
                ModelState.AddModelError("To", "Please enter a 'To' email address");
            }

            if (model.NotificationId == Guid.Empty)
            {
                ModelState.AddModelError("NotificationId", "Please select a notification");
            }

            if (string.IsNullOrEmpty(model.Categories))
            {
                ModelState.AddModelError("Categories", "Please choose at-least one category");
            }
        }

        /// <summary>
        /// This method is used to delete exception subscription based on the given identifier.
        /// </summary>
        /// <param name="id">unique identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteSubscription(Guid? id)
        {
            if (!id.HasValue || id.Value == Guid.Empty)
                return HttpNotFound();

            ExceptionSubscription model = null;

            if (id.HasValue && id.Value != Guid.Empty)
            {
                model = LoggerProxy.GetExceptionSubscription(id.Value);
                if (model == null) return HttpNotFound();
            }

            try
            {
                LoggerProxy.DeleteExceptionSubscription(id.Value);
                TempData["Success"] = string.Format(CultureInfo.CurrentCulture, "'{0}' Subscription deleted successfully!", model.Name);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("Subscriptions");
        }
    }
}
