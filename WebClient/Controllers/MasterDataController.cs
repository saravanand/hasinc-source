using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.Library.Configuration;
using CelloSaaS.Model.MasterDataManagement;
using CelloSaaS.ServiceProxies.MasterDataManagement;
using CelloSaaS.Library;
using System.Data;
using CelloSaaS.ServiceContracts.MasterDataManagement;
using System.Collections;
using System.Text.RegularExpressions;
using System.Globalization;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for master data.
    /// </summary>
    [HandleError]
    public class MasterDataController : CelloSaaS.View.CelloController
    {
        #region Private Members

        private const string _masterDataIdentifierName = "MasterDataIdentifierName";
        private const string _modelPrefix = "MasterDataEntity";
        private const string policyName = "MasterDataExceptionLogger";
        private const string _validatePrefix = "val";
        private const string PRODUCTROLEID = "GR$Product_Admin";
        private const string ISFORALL = "All";
        private MasterDataConfiguration masterDataConfiguration = (MasterDataConfiguration)System.Configuration.ConfigurationManager.GetSection(MasterDataConfiguration.SectionName);

        /// <summary>
        /// Converts list of masterData entity to DataTable to bind the grid.
        /// It replaces child relation column of Id with Values.
        /// </summary>
        /// <param name="lstMasterDataEntity"></param>
        /// <param name="masterDataName"></param>
        /// <returns></returns>
        private static DataTable ConvertListToDataTable(List<GenericMasterData> lstMasterDataEntity, string masterDataName)
        {
            DataTable masterDataTable = new DataTable();
            ChildRelationData childRelationData = MasterDataServiceProxy.GetChildRelationData(masterDataName, UserIdentity.TenantID);

            //Getting masterData element for the given masterDataName from config file.
            MasterDataConfiguration masterDataConfiguration = (MasterDataConfiguration)System.Configuration.ConfigurationManager.GetSection(MasterDataConfiguration.SectionName);
            MasterDataElement masterDataElement = masterDataConfiguration.MasterData.Get(masterDataName);

            //Creates datatable schema.
            foreach (MasterDataColumnElement column in masterDataElement.Columns)
                masterDataTable.Columns.Add(column.DisplayHeader);
            masterDataTable.Columns.Add(_masterDataIdentifierName);

            foreach (GenericMasterData masterDataEntity in lstMasterDataEntity)
            {
                DataRow masterDataRow = masterDataTable.NewRow();
                masterDataRow[_masterDataIdentifierName] = masterDataEntity.Identifier;
                foreach (MasterDataColumnElement column in masterDataElement.Columns)
                {
                    if (masterDataEntity.Properties != null && masterDataEntity.Properties.ContainsKey(column.Name))
                        if (column.DisplayAs == DisplayAs.PickupList || column.DisplayAs == DisplayAs.DDL)
                        {
                            if (childRelationData != null && childRelationData.ChildData != null && childRelationData.ChildData.Count > 0 && childRelationData.ChildData.ContainsKey(column.Name))
                            {
                                if (childRelationData.ChildData[column.Name].IdValueCollection.ContainsKey(masterDataEntity.Properties[column.Name].ToString()))
                                {
                                    masterDataRow[column.DisplayHeader] = childRelationData.ChildData[column.Name].IdValueCollection[masterDataEntity.Properties[column.Name].ToString()];
                                }
                            }
                        }
                        else
                        {
                            masterDataRow[column.DisplayHeader] = masterDataEntity.Properties[column.Name];
                        }
                }
                masterDataTable.Rows.Add(masterDataRow);
            }
            return masterDataTable;
        }

        /// <summary>
        /// Loads dropDown list values for Pickuplist and DDL columns
        /// </summary>
        /// <param name="masterDataName"></param>
        /// <param name="masterDataEntity"></param>
        private void LoadPickupList(string masterDataName, GenericMasterData masterDataEntity)
        {
            string statusMessage = string.Empty;
            ChildRelationData childRelationData = null;
            try
            {
                childRelationData = MasterDataServiceProxy.GetChildRelationData(masterDataName, UserIdentity.TenantID);
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, policyName);
                statusMessage = Resources.MasterDataResource.e_ChildRelationData_Fetch;
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, policyName);
                statusMessage = Resources.MasterDataResource.a_ChildRelationData_Fetch;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.MasterDataResource.e_ChildRelationData_Fetch;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.MasterDataResource.e_ChildRelationData_Fetch;
            }

            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);

            MasterDataElement masterDataElement = masterDataConfiguration.MasterData.Get(masterDataName);

            foreach (MasterDataColumnElement column in masterDataElement.Columns)
            {
                if (column.DisplayAs != DisplayAs.PickupList && column.DisplayAs != DisplayAs.DDL)
                {
                    continue;
                }
                string defaultValue = "--Select " + column.DisplayHeader + "--";
                Dictionary<string, string> idValueCollection = new Dictionary<string, string>();
                string selectedValue = string.Empty;
                //Finds the selected value to bind edit templates
                if (masterDataEntity != null && masterDataEntity.Properties != null)
                    if (masterDataEntity.Properties.ContainsKey(column.Name))
                        selectedValue = masterDataEntity.Properties[column.Name].ToString();
                if (childRelationData != null && childRelationData.ChildData != null && childRelationData.ChildData.Count > 0 && childRelationData.ChildData.ContainsKey(column.Name))
                    idValueCollection = childRelationData.ChildData[column.Name].IdValueCollection;

                List<KeyValuePair<string, string>> list = idValueCollection.ToList();
                list.Insert(0, new KeyValuePair<string, string>("", defaultValue));

                this.ViewData[_modelPrefix + "." + column.FieldId] = new SelectList((IEnumerable)list, "Key", "Value", selectedValue);
            }
        }

        #endregion

        #region Actions

        /// <summary>
        /// This method is used to fetch list of masterData details.
        /// </summary>
        /// <param name="masterDataName">master data name.</param>
        public void FetchMasterDataList(string masterDataName)
        {
            string statusMessage = string.Empty;
            Dictionary<string, GenericMasterData> dicMasterDataEntity = null;
            try
            {
                string entityIdentifier = string.Empty;
                MasterDataElement masterDataElement = masterDataConfiguration.MasterData.Get(masterDataName);
                if (masterDataElement != null && !string.IsNullOrEmpty(masterDataElement.ViewPrivilege))
                    Context.SetPrivilegeContext(masterDataElement.ViewPrivilege);
                if (masterDataElement != null)
                    entityIdentifier = masterDataElement.EntityName;
                dicMasterDataEntity = MasterDataServiceProxy.FetchMasterDataList(masterDataName, UserIdentity.TenantID, entityIdentifier);
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, policyName);
                statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, policyName);
                statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, policyName);
                statusMessage = Resources.MasterDataResource.a_MasterDataDetails_Fetch;
            }
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            if (dicMasterDataEntity != null)
            {
                try
                {
                    ViewData["MasterDataList"] = ConvertListToDataTable(dicMasterDataEntity.Values.ToList<GenericMasterData>(), masterDataName);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, policyName);
                    statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
                }
            }
        }

        /// <summary>
        /// This method is used to fetch list of masterData details and returns list view
        /// </summary>
        /// <param name="Id">name of the master data.</param>
        /// <returns>Action result</returns>
        public ActionResult MasterDataList(string Id)
        {
            if (TempData["Success"] != null)
            {
                ViewData["Success"] = TempData["Success"];
            }

            ViewData["MasterDataName"] = Id;
            FetchMasterDataList(Id);
            return View();
        }

        /// <summary>
        /// This method is used to return Add or edit masterData view with masterData details.
        /// </summary>
        /// <param name="masterDataName">master data name.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <returns></returns>
        public ActionResult MasterDataDetails(string masterDataName, string referenceId)
        {
            string statusMessage = string.Empty;
            GenericMasterData masterDataEntity = new GenericMasterData();

            if (string.IsNullOrEmpty(referenceId))
            {
                ViewData["FormMode"] = CurrentFormMode.Insert;
                masterDataEntity.TenantId = UserIdentity.TenantID;
            }
            else
            {
                ViewData["FormMode"] = CurrentFormMode.Edit;
                try
                {
                    MasterDataElement masterDataElement = masterDataConfiguration.MasterData.Get(masterDataName);

                    if (masterDataElement != null && !string.IsNullOrEmpty(masterDataElement.ViewPrivilege))
                    {
                        Context.SetPrivilegeContext(masterDataElement.ViewPrivilege);
                    }

                    string entityIdentifier = masterDataElement != null ? masterDataElement.EntityName : string.Empty;
                    masterDataEntity = MasterDataServiceProxy.FetchMasterDataDetails(masterDataName, referenceId, entityIdentifier);

                    if (masterDataEntity == null)
                    {
                        return RedirectToAction("Error", "PageNotFound");
                    }
                }
                catch (MasterDataException masterDataException)
                {
                    ExceptionService.HandleException(masterDataException, policyName);
                    statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    ExceptionService.HandleException(unauthorizedAccessException, policyName);
                    statusMessage = Resources.MasterDataResource.a_MasterDataDetails_Fetch;
                }
            }


            ViewData["MasterDataName"] = masterDataName;
            ViewData["MasterDataEntity"] = masterDataEntity;
            ViewData["MasterDataIdentity"] = referenceId;
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            LoadPickupList(masterDataName, masterDataEntity);
            return View();
        }

        /// <summary>
        /// This method is used to delete a master data details and return to list view of master data.
        /// </summary>
        /// <param name="masterDataName">master data name.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteMasterDataDetails(string masterDataName, string referenceId)
        {
            string statusMessage = string.Empty;
            string statusMessageSuccess = string.Empty;
            GenericMasterData masterDataEntity = new GenericMasterData();
            try
            {
                string entityIdentifier = string.Empty;
                MasterDataElement masterDataElement = masterDataConfiguration.MasterData.Get(masterDataName);
                if (masterDataElement != null && !string.IsNullOrEmpty(masterDataElement.DeletePrivilege))
                    Context.SetPrivilegeContext(masterDataElement.DeletePrivilege);
                if (masterDataElement != null)
                    entityIdentifier = masterDataElement.EntityName;
                masterDataEntity = MasterDataServiceProxy.FetchMasterDataDetails(masterDataName, referenceId, entityIdentifier);
                Dictionary<string, object> dataColumn = new Dictionary<string, object>();
                if (masterDataEntity != null && masterDataEntity.Properties != null && masterDataElement != null)
                {
                    foreach (MasterDataColumnElement column in masterDataElement.Columns)
                    {
                        if (masterDataEntity.Properties.ContainsKey(column.FieldId))
                            dataColumn.Add(column.FieldId, masterDataEntity.Properties[column.FieldId]);

                    }
                }
                masterDataEntity.Properties = dataColumn;
                MasterDataServiceProxy.DeleteMasterDataDetails(masterDataEntity);
                statusMessageSuccess = string.Format(CultureInfo.InvariantCulture, Resources.MasterDataResource.s_MasterDataDetails_Delete, masterDataName.EndsWith("Details", StringComparison.OrdinalIgnoreCase) ? masterDataName.Replace("Details", "") : masterDataName);
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, policyName);
                statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Reference;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Delete;
            }
            catch (MasterDataReferenceException masterDataReferenceException)
            {
                ExceptionService.HandleException(masterDataReferenceException, policyName);
                statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Reference;
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, policyName);
                statusMessage = Resources.MasterDataResource.a_MasterDataDetails_Delete;
            }



            ViewData["MasterDataName"] = masterDataName;
            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);
            if (!string.IsNullOrEmpty(statusMessageSuccess))
                ModelState.AddModelError("StatusMessageSuccess", statusMessageSuccess);
            FetchMasterDataList(masterDataName);
            return PartialView("MasterDataGrid", ViewData["MasterDataList"] as DataTable);
        }

        /// <summary>
        /// This method is used to add or update master data.
        /// If master data is added or updated successfully, it returns list view.
        /// Else it returns details view with error message.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterDataDetails(FormCollection formCollection)
        {
            string masterDataName = formCollection["MasterDataName"];
            string modelPrefix = formCollection["ModelPrefix"];
            string referenceId = formCollection["MasterDataIdentity"];
            string statusMessage = string.Empty;
            //Getting masterData element for the given masterDataName from config file.
            MasterDataElement masterDataElement = masterDataConfiguration.MasterData.Get(masterDataName);
            GenericMasterData masterDataEntity = null;

            if (masterDataElement != null)
            {
                try
                {
                    masterDataEntity = new GenericMasterData(masterDataElement.EntityName);
                    UpdateModel(masterDataEntity);
                    masterDataEntity.Properties = new Dictionary<string, object>();
                    masterDataEntity.MasterDataName = masterDataName;

                    if (string.IsNullOrEmpty(formCollection[modelPrefix + "." + ISFORALL]))
                    {
                        masterDataEntity.TenantId = UserIdentity.TenantID;
                    }

                    masterDataEntity.Identifier = referenceId;
                    masterDataEntity.Status = true;
                    foreach (MasterDataColumnElement column in masterDataElement.Columns)
                    {
                        string value = string.Empty;
                        if (!string.IsNullOrEmpty(formCollection[modelPrefix + "." + column.FieldId]))
                            value = formCollection[modelPrefix + "." + column.FieldId].Trim();

                        if (!string.IsNullOrEmpty(column.RegularExpression))
                        {
                            Regex regex = new Regex(column.RegularExpression);
                            if (!regex.IsMatch(value))
                            {
                                ModelState.AddModelError(_validatePrefix + modelPrefix + "." + column.FieldId, "*");
                                ModelState.AddModelError("StatusMessage", string.IsNullOrEmpty(column.RegularExpressionErrorMessage) ? "Invalid" : column.RegularExpressionErrorMessage);
                            }
                        }

                        if (column.Mandatory && string.IsNullOrEmpty(value))
                        {
                            ModelState.AddModelError(_validatePrefix + modelPrefix + "." + column.FieldId, "*");
                            ModelState.AddModelError("StatusMessage", string.IsNullOrEmpty(column.RequiredErrorMessage) ? string.Format(Resources.MasterDataResource.m_Required, column.DisplayHeader) : column.RequiredErrorMessage);
                        }

                        if (column.DisplayAs.Equals(DisplayAs.CheckBox))
                        {
                            value = string.IsNullOrEmpty(value) ? false.ToString() : value.Equals("on", StringComparison.OrdinalIgnoreCase) ? true.ToString() : false.ToString();
                        }

                        masterDataEntity.Properties.Add(column.Name, string.IsNullOrEmpty(value) ? string.Empty : value);
                    }
                    if (ModelState.IsValid)
                    {
                        if (string.IsNullOrEmpty(referenceId))
                        {
                            if (!string.IsNullOrEmpty(masterDataElement.CreatePrivilege))
                            {
                                Context.SetPrivilegeContext(masterDataElement.CreatePrivilege);
                            }
                            masterDataEntity.Identifier = MasterDataServiceProxy.CreateMasterDataDetails(masterDataEntity);
                            TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.MasterDataResource.s_MasterDataDetails_Save, masterDataName.EndsWith("Details", StringComparison.OrdinalIgnoreCase) ? masterDataName.Replace("Details", "") : masterDataName);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(masterDataElement.UpdatePrivilege))
                            {
                                Context.SetPrivilegeContext(masterDataElement.UpdatePrivilege);
                            }
                            MasterDataServiceProxy.UpdateMasterDataDetails(masterDataEntity);
                            TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.MasterDataResource.s_MasterDataDetails_Update, masterDataName.EndsWith("Details", StringComparison.OrdinalIgnoreCase) ? masterDataName.Replace("Details", "") : masterDataName);
                        }

                        return RedirectToAction("MasterDataList", new { id = masterDataName });
                    }
                }
                catch (MasterDataException masterDataException)
                {
                    ExceptionService.HandleException(masterDataException, policyName);
                    statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Save;
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, policyName);
                    statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Save;
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    ExceptionService.HandleException(unauthorizedAccessException, policyName);
                    statusMessage = Resources.MasterDataResource.a_MasterDataDetails_Save;
                }
            }
            else
            {
                statusMessage = Resources.MasterDataResource.e_SaveDetails;
                masterDataEntity = new GenericMasterData();
            }

            ViewData["FormMode"] = (string.IsNullOrEmpty(referenceId)) ? CurrentFormMode.Insert : CurrentFormMode.Edit;
            ViewData["MasterDataEntity"] = masterDataEntity;
            ViewData["MasterDataIdentity"] = referenceId;
            ViewData["MasterDataName"] = masterDataName;

            if (!string.IsNullOrEmpty(statusMessage))
            {
                ModelState.AddModelError("StatusMessage", statusMessage);
            }

            LoadPickupList(masterDataName, masterDataEntity);
            return View();
        }

        #endregion
    }
}
