﻿using CelloSaaS.Library.Helpers;
using CelloSaaS.MultiTenancyEnablement;
using CelloSaaS.MultiTenancyEnablement.Models;
using CelloSaaSApplication.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for multi tenancy enablement.
    /// </summary>
    public class MultiTenancyEnablementController : CelloSaaS.View.CelloController
    {
        //
        // GET: /TenantDBMigration/
        enum TabSteps : int
        {
            step1, step2, step3, step4
        }

        public ActionResult Index()
        {
            ViewBag.step = TabSteps.step1;
            return View();
        }
        /// <summary>
        /// This method is used to connecting to database.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            ViewBag.step = TabSteps.step1;
            HttpContext.Session.Add("__MyAppSession", string.Empty);
            try
            {
                List<TableMetadata> tableSchemaList;

                var dbMetadata = new WebApplication.Models.MultiTenancyConnectionStringHelper.DatabaseConnectionMetadata();

                TryUpdateModel(dbMetadata);

                ValidateDatabaseMetadata(dbMetadata);

                if (!ModelState.IsValid)
                {
                    return View();
                }

                var connBuilder = new ConnectionStrings();
                connBuilder.DataSource = dbMetadata.ServerName;
                connBuilder.InitialCatalog = dbMetadata.InstanceName;
                connBuilder.UserId = dbMetadata.LoginId;
                connBuilder.Password = dbMetadata.Password;
                tableSchemaList = TableServiceProxy.GetTables(connBuilder,Session.SessionID, null);

                if (tableSchemaList == null || tableSchemaList.Count < 1)
                {
                    ModelState.AddModelError("", "Unable to find any table in the given database ");
                    ViewBag.step = TabSteps.step1;
                    return View();
                }

                ViewBag.step = TabSteps.step2;

                return View(tableSchemaList);
               
            }
           
            catch (DBMigrationException dbMigrationException)
            {
                ViewBag.step = TabSteps.step1;
                ModelState.AddModelError(string.Empty, dbMigrationException.Message);
                return View();
            }
            catch (KeyNotFoundException)
            {
                ViewBag.step = TabSteps.step1;
                ModelState.AddModelError(string.Empty, "Session Expired");
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.step = TabSteps.step1;
                ModelState.AddModelError(string.Empty, ex.Message);
                return View();
            }
          

        }

        /// <summary>
        /// This method is used to return summary Report.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        public ActionResult Summary(FormCollection formCollection)
        {
            try
            {
                ViewBag.step = TabSteps.step3;
                ViewBag.TablePrefix = formCollection["Tenant.DBMigration.TablePrefix"];

                return View("Index", TableServiceProxy.GetTables(null,Session.SessionID, formCollection.AllKeys.ToList()));
            }
            catch (KeyNotFoundException)
            {
                ModelState.AddModelError(string.Empty, "Session Expired");
            }
            catch (DBMigrationException dbMigrationException)
            {
                ModelState.AddModelError(string.Empty, dbMigrationException.Message);
            }

            ViewBag.step = TabSteps.step1;
            return View("Index");
        }

        /// <summary>
        /// This method is sued to generate the script for the selected table.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        public ActionResult GenerateScript(FormCollection formCollection)
        {
            try
            {
                string tablePrefix = ConfigHelper.DbMigrationUtilityTablePrefix;
                if (!string.IsNullOrEmpty(formCollection["Tenant.DBMigration.TablePrefix"]))
                    tablePrefix = formCollection["Tenant.DBMigration.TablePrefix"];
                ViewBag.step = TabSteps.step4;
                return View("Index", TableServiceProxy.GetQuery(Session.SessionID, formCollection.AllKeys.ToList(), tablePrefix));
            }
            catch (KeyNotFoundException)
            {
                ViewBag.step = TabSteps.step1;
                ModelState.AddModelError(string.Empty, "Session Expired");
                return View("Index");
            }
        }

        /// <summary>
        /// This method is used to download the sql query for migration
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        public ActionResult DownloadFile(FormCollection formCollection)
        {
            if (formCollection.Count < 1 && formCollection[0] == null)
            {
                
                ModelState.AddModelError(string.Empty, "Download Content Not Found");
                ViewBag.step = TabSteps.step4;
                return View();

            }
            string fullQuery = formCollection[0];
            fullQuery = fullQuery.Replace("\r", string.Empty);
            fullQuery = fullQuery.Replace("\n", string.Empty);

            fullQuery = fullQuery.Replace("~$", "\n");

            string fileName = string.Format(CultureInfo.InvariantCulture, "{0}_{1}.sql", "DBMigrationScript", DateTime.Now.Ticks.ToString());

            return File(Encoding.UTF8.GetBytes(fullQuery), "text/plain", fileName);
        }

        #region private members
        /// <summary>
        /// Validating DataBase Meta Data Variables
        /// </summary>
        /// <param name="dbMetadata"></param>
        private void ValidateDatabaseMetadata(WebApplication.Models.MultiTenancyConnectionStringHelper.DatabaseConnectionMetadata dbMetadata)
        {
            if (string.IsNullOrEmpty(dbMetadata.ServerName))
            {
                ModelState.AddModelError("", "ServerName Name Cannot Be Empty");
            }
            if (string.IsNullOrEmpty(dbMetadata.InstanceName))
            {
                ModelState.AddModelError("", "InstanceName Name Cannot Be Empty");
            }
            if (string.IsNullOrEmpty(dbMetadata.LoginId))
            {
                ModelState.AddModelError("", "LoginId Name Cannot Be Empty");
            }
            if (string.IsNullOrEmpty(dbMetadata.Password))
            {
                ModelState.AddModelError("", "Password Name Cannot Be Empty");
            }
        }

        #endregion

    }
}
