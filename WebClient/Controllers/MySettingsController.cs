﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.MasterData;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.Rules.Core;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.Configuration;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.MasterDataManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.ServiceProxies.Configuration;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.MasterData;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.ServiceProxies.ViewManagement;
using CelloSaaS.View;
using CelloSaaS.ServiceContracts.DataManagement;
using CelloSaaS.Model.DataManagement;
using CelloSaaSApplication.Models;
using System.Globalization;
using CelloSaaS.AuthServer.Client.Models;
using CelloSaaS.Library.Encryption;
using Newtonsoft.Json;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for managing self tenant plans and contact details, themes and language settings.
    /// </summary>
    public class MySettingsController : CelloController
    {
        private const string DefaultPolicy = "GlobalExceptionLogger";
        private static string[] validImageTypes = new string[] { "image/jpeg", "image/png", "image/gif", "image/svg+xml" };

        private string TenantId = UserIdentity.TenantID;
        private string UserId = UserIdentity.UserId;

        /// <summary>
        /// This method is used for manages the settings.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageSettings(BillFrequency? frequency)
        {
            if (TempData["Success"] != null)
            {
                ViewData["Success"] = TempData["Success"];
            }

            frequency = frequency == null ? BillFrequency.Monthly : frequency.Value;

            Tenant tenant = LoadAccountSettings(frequency.Value);

            ExtendedEntityRow tenantExtnRows = null;

            if (tenant.ExtendedRow == null || tenant.ExtendedRow.ExtendedEntityColumnValues == null || tenant.ExtendedRow.ExtendedEntityColumnValues.Count < 1)
            {
                var extendedRows = ServiceLocator.Resolve<IDataManagementService>().GetExtendedEntityRows(tenant.EntityIdentifier, tenant.TenantDetails.ParentTenantId, new string[] 
                { 
                    tenant.TenantDetails.Identifier 
                });

                if (extendedRows != null && extendedRows.Count > 0 && extendedRows.ContainsKey(tenant.TenantDetails.Identifier))
                {
                    tenantExtnRows = extendedRows[tenant.TenantDetails.Identifier];
                }
            }
            else
            {
                tenantExtnRows = tenant.ExtendedRow;
            }

            if (tenantExtnRows != null && tenantExtnRows.ExtendedEntityColumnValues != null && tenantExtnRows.ExtendedEntityColumnValues.Count > 0)
            {
                ViewBag.ExtendedRows = tenantExtnRows;
                string discountCode = tenantExtnRows.ExtendedEntityColumnValues.ContainsKey(Constants.DiscountCodeExtnFieldName)
                                        ? tenantExtnRows.ExtendedEntityColumnValues[Constants.DiscountCodeExtnFieldName].Value
                                        : string.Empty;
                ViewBag.DiscountCode = discountCode;

                string membershipId = tenantExtnRows.ExtendedEntityColumnValues.ContainsKey(Constants.MembershipIdExtnFieldName)
                                        ? tenantExtnRows.ExtendedEntityColumnValues[Constants.MembershipIdExtnFieldName].Value
                                        : string.Empty;
                ViewBag.MembershipId = membershipId;

                TenantLicense tenantlicense = ViewBag.CurrentLicense as TenantLicense;
                if (tenantlicense != null && !string.IsNullOrEmpty(tenantlicense.PricePlanId) && !string.IsNullOrEmpty(discountCode))
                {
                    ViewBag.DiscountedPricePlanId = tenantlicense.PricePlanId;
                }
            }

            if (ConfigHelper.EnableExternalAuthentication)
                AuthorizationSettings();
            return View(tenant);
        }

        /// <summary>
        /// This method is used to manage tenant details, theme, logo update.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageSettings(FormCollection formCollection)
        {
            Tenant tenant = null;
            TenantSetting tenantSetting = null;
            string licensePackageId = null, pricePlanId = null, discountCode = null, membershipId = null;

            BillFrequency frequency = (BillFrequency)Enum.Parse(typeof(BillFrequency), formCollection["BillFrequency"]);

            discountCode = formCollection["DiscountCode"];
            membershipId = formCollection["MembershipId"];

            ViewBag.DiscountCode = discountCode;
            ViewBag.MembershipId = membershipId;

            var parentTenant = TenantRelationProxy.GetParentTenant(this.TenantId);
            tenant = TenantProxy.GetTenantDetailsByTenantId(this.TenantId, parentTenant != null && !string.IsNullOrEmpty(parentTenant.TenantCode) ? parentTenant.TenantCode : null);

            TryUpdateModel(tenant);
            ValidateTenant(tenant);

            tenantSetting = TenantSettingsProxy.GetTenantSettings(this.TenantId);
            FillTenantSettings(tenantSetting);

            licensePackageId = this.Request["PackageId"];
            pricePlanId = this.Request["PricePlanId"];
            ViewBag.PackageId = licensePackageId;
            ViewBag.PricePlanId = pricePlanId;

            if (string.IsNullOrEmpty(licensePackageId) || string.IsNullOrEmpty(licensePackageId.Trim()))
            {
                ModelState.AddModelError("LicensePackage", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Package);
            }

            string discountedPricePlanId = null;
            try
            {
                discountedPricePlanId = TenantExtensions.GetDiscountedPricePlanId(discountCode, licensePackageId, tenant.TenantDetails.ParentTenantId, frequency);
                if (!string.IsNullOrEmpty(discountedPricePlanId))
                {
                    ViewBag.DiscountedPricePlanId = discountedPricePlanId;
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("DiscountCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.MySettingsResource.e_DiscountCode);
            }

            if (!string.IsNullOrEmpty(discountCode) && !string.IsNullOrEmpty(licensePackageId) && string.IsNullOrEmpty(discountedPricePlanId))
            {
                ModelState.AddModelError("DiscountCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.MySettingsResource.e_DiscountCode);
            }

            if (ConfigHelper.EnableExternalAuthentication)
            {
                // DO AuthServer Updates
                ManageAuthorizationSettings(formCollection);
                AuthorizationSettings();
            }

            if (ModelState.IsValid)
            {
                tenant.TenantDetails.ParentTenant = parentTenant;

                var extendedRow = tenant.GetDiscountandMembershipExtensions(discountCode, membershipId);
                tenant.ExtendedRow = extendedRow;

                ViewBag.PricePlanId = !string.IsNullOrEmpty(discountedPricePlanId) ? discountedPricePlanId : pricePlanId;
                if (UpdateAccountSettings(tenant, tenantSetting, licensePackageId, !string.IsNullOrEmpty(discountedPricePlanId)
                                                                                ? discountedPricePlanId
                                                                                : pricePlanId))
                {
                    return RedirectToAction("ManageSettings");
                }
            }

            LoadAccountSettings(frequency);

            return View(tenant);
        }

        /// <summary>
        /// This method is used for self user details view/edit.
        /// </summary>
        /// <returns></returns>
        public ActionResult UserSettings()
        {
            UserDetails userDetails = null;
            try
            {
                if (string.IsNullOrEmpty(UserIdentity.SessionTenantID))
                    userDetails = UserDetailsProxy.GetUserDetailsByUserId(this.UserId, this.TenantId);
                else
                    userDetails = UserDetailsProxy.GetUserDetailsByUserId(this.UserId, UserIdentity.LoggedInUserTenantId);



                if (this.Request.HttpMethod.Equals("POST", StringComparison.OrdinalIgnoreCase))
                {
                    TryUpdateModel(userDetails, "UserDetails");
                    ValidateUserDetails(userDetails);

                    if (ModelState.IsValid)
                    {
                        UserDetailsProxy.UpdateUser(userDetails);
                        ModelState.AddModelError("Success", Resources.MySettingsResource.s_Success);
                    }
                }

                ViewData["UserDetails"] = userDetails;
                LoadUserCountry(userDetails.Address != null ? userDetails.Address.CountryId : string.Empty);

                if (ConfigHelper.EnableExternalAuthentication)
                    InitUserSettings();
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (DuplicateEmailIDException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (DuplicateNameException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UserDetailException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (PickupListException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BusinessRuleException bex)
            {
                ExceptionService.HandleException(bex, DefaultPolicy);
                if (bex.Data != null)
                {
                    int count = 1;
                    foreach (DictionaryEntry er in bex.Data)
                    {
                        foreach (string error in (IEnumerable<string>)er.Value)
                        {
                            ModelState.AddModelError("Error" + (count++), error);
                        }
                    }
                }
            }

            return View(userDetails);
        }

        /// <summary>
        /// This method is used to initialize user settings.
        /// </summary>
        private void InitUserSettings()
        {
            var userSettings = UserSettingsProxy.GetUserSettings(this.UserId);

            Dictionary<string, string> tenantSettings = GetTenantSettingCollection();

            var authenticationProviders = CelloSaaSWebClient.Services.AuthenticationService.GetAuthenticationProviders();

            string idp = AppSettingHelper.CelloOpenIdentity;

            if (tenantSettings.ContainsKey(AuthServerSettingConstants.IdentityProviders)) idp = tenantSettings[AuthServerSettingConstants.IdentityProviders];

            ViewBag.IdentityProvider = authenticationProviders[idp.ToLowerInvariant()];

            if (userSettings == null || userSettings.Setting == null || userSettings.Setting.Attributes == null || userSettings.Setting.Attributes.Count < 1
                || !userSettings.Setting.Attributes.ContainsKey(AuthServerSettingConstants.LoginProvider))
            {
                var authTypes = CelloSaaSWebClient.Services.AuthenticationService.GetAuthenticationTypes(AppSettingHelper.CelloOpenIdentity);

                ViewBag.AuthenticationType = Constants.CelloOpenIdProviderName;
                return;
            }

            var tenantAuthTypes = CelloSaaSWebClient.Services.AuthenticationService.GetAuthenticationTypes(tenantSettings[AuthServerSettingConstants.IdentityProviders]);

            var authTypId = userSettings.Setting.Attributes.ContainsKey(AuthServerSettingConstants.LoginProvider)
                            ? userSettings.Setting.Attributes[AuthServerSettingConstants.LoginProvider]
                            : AppSettingHelper.CelloAuthentication;

            ViewBag.AuthenticationType = tenantAuthTypes[idp][authTypId];
        }

        #region Private Methods

        /// <summary>
        /// Load tenant account details
        /// </summary>
        /// <returns></returns>
        private Tenant LoadAccountSettings(BillFrequency frequency)
        {
            Tenant tenant = null;

            try
            {
                //tenant = TenantProxy.GetTenantInfo(this.TenantId);
                var parentTenant = TenantRelationProxy.GetParentTenant(this.TenantId);
                tenant = TenantProxy.GetTenantDetailsByTenantId(this.TenantId, parentTenant != null && !string.IsNullOrEmpty(parentTenant.TenantCode) ? parentTenant.TenantCode : string.Empty);

                TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);
                tenant.Address = tenant.Address ?? new Address();

                var currentPricePlanId = tenantLicense.PricePlanId;

                if (!string.IsNullOrEmpty(currentPricePlanId))
                {
                    var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(currentPricePlanId), Guid.Parse(parentTenant.Identifier));
                    ViewBag.PricePlanFrequency = pricePlan.BillFrequency.ToString("F");
                }

                LoadThemeAndLogo();
                LoadCompanySize(tenant.TenantDetails.CompanySize, ProductAdminConstants.ProductAdminTenantId);
                LoadCountry(tenant.Address.CountryId);
                LoadSubscripitionDetails(tenant.TenantDetails.TenantCode, tenantLicense.PackageId);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (TenantException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (PickupListException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            return tenant;
        }

        /// <summary>
        /// Update account settings
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="tenantSetting"></param>
        /// <param name="licensePackageId"></param>
        private bool UpdateAccountSettings(Tenant tenant, TenantSetting tenantSetting,
            string licensePackageId, string pricePlanId)
        {
            if (string.IsNullOrEmpty(pricePlanId))
                pricePlanId = null;

            try
            {
                bool generateInvoice = false;
                BillingSetting billSetting = null;
                string message = Resources.MySettingsResource.s_UpdateAccount;
                var gtenantId = Guid.Parse(this.TenantId);
                TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);
                bool canUpgrade = UserIdentity.HasPrivilege(PrivilegeConstants.SelfUpgradePackage);

                if (canUpgrade && (!string.IsNullOrEmpty(licensePackageId)
                    && !licensePackageId.Equals(tenantLicense.PackageId, StringComparison.OrdinalIgnoreCase))
                    || (tenantLicense.PricePlanId != pricePlanId))
                {
                    if (!string.IsNullOrEmpty(pricePlanId))
                    {
                        var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(licensePackageId);

                        if (packageDetails == null || packageDetails.IsEvaluation)
                        {
                            ModelState.AddModelError("Error", Resources.MySettingsResource.e_InvalidPackage);
                            return false;
                        }

                        var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(pricePlanId), Guid.Parse(tenant.TenantDetails.ParentTenantId));

                        if (pricePlan == null)
                        {
                            ModelState.AddModelError("Error", Resources.MySettingsResource.e_InvalidPricePlan);
                            return false;
                        }

                        bool requirePaymentAccount = tenant.TenantDetails.EnableAutoDebit;

                        if (packageDetails.ExpiryDays > 0 && tenantLicense.TrialEndDate.HasValue && tenantLicense.TrialEndDate.Value > DateTime.Now)
                        {
                            // current subscription has trial period
                            var history = LicenseProxy.GetTenantLicenseHistory(this.TenantId);
                            var firstLicense = history.OrderBy(x => x.ValidityStart).First();

                            var trialEndDate = firstLicense.ValidityStart.AddDays(packageDetails.ExpiryDays);

                            if (trialEndDate > DateTime.Now)
                            {
                                requirePaymentAccount = false; // still has some trial period left
                            }
                        }

                        if (requirePaymentAccount)
                        {
                            // if trial period expires or upgarde to paid plan
                            // if pre paid mode and payment not given then ask to provide payment details before upgrade
                            billSetting = BillingProxy.GetBillingSetting(pricePlan.BillFrequency, gtenantId);

                            if (billSetting.Mode == BillCycleMode.Prepaid)
                            {
                                var paymentAccount = PaymentProxy.GetAccountSetting(gtenantId);

                                if (paymentAccount == null || string.IsNullOrEmpty(paymentAccount.StoreKey) || paymentAccount.StoreKey.Split('/').Length != 2 || !paymentAccount.Status)
                                {
                                    ModelState.AddModelError("Error", Resources.MySettingsResource.e_ProvidePaymentDetails);
                                    return false;
                                }
                                else
                                {
                                    // trials ended and has valid payment account
                                    // generate bill and debit
                                    generateInvoice = true;
                                }
                            }
                        }
                    }

                    tenantLicense = new TenantLicense()
                    {
                        TenantLicenseCode = tenantLicense.TenantLicenseCode,
                        PackageId = licensePackageId,
                        TenantId = tenantLicense.TenantId,
                        PricePlanId = pricePlanId,
                        NumberOfUsers = tenantLicense.NumberOfUsers,
                        ValidityStart = DateTime.Now
                    };

                    message = Resources.MySettingsResource.s_UpdateAccountSubPlan;
                }

                TenantProxy.UpdateTenantInfo(tenant, tenantLicense);
                TenantSettingsProxy.UpdateTenantSettings(tenantSetting);

                if (generateInvoice)
                {
                    if (!ConfigHelper.ApplyGlobalBillSettingForPrepaidMode)
                    {
                        billSetting.TenantId = gtenantId;
                        billSetting.ChargeDay = DateTime.Now.Day + (billSetting.ChargeDay - billSetting.StartDay);
                        billSetting.StartDay = DateTime.Now.Day;
                        BillingProxy.UpdateBillingSetting(billSetting);
                    }

                    BillingProxy.GenerateInvoice(gtenantId);
                }

                TempData["Success"] = message;
                return true;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ParameterEmptyOrNull);
            }
            catch (BillingException billingException)
            {
                ExceptionService.HandleException(billingException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", billingException.Message);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCreate);
            }
            catch (UserDetailException userDetailException)
            {
                ExceptionService.HandleException(userDetailException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.UserResource.e_UserUpdate);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Access);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCreate);
            }
            catch (BusinessRuleException bex)
            {
                ExceptionService.HandleException(bex, DefaultPolicy);
                if (bex.Data != null)
                {
                    foreach (DictionaryEntry er in bex.Data)
                    {
                        foreach (string error in (IEnumerable<string>)er.Value)
                        {
                            ModelState.AddModelError("Error", error);
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Load the subscription details
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="selectedPackageId"></param>
        private void LoadSubscripitionDetails(string tenantId, string selectedPackageId = null)
        {
            var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(tenantId, UserIdentity.TenantID);
            var licenseHistory = LicenseProxy.GetTenantLicenseHistory(tenantId);
            var currentLicense = LicenseProxy.GetTenantLicense(tenantId);
            var availablePackages = LicenseProxy.GetAssignableLicensePackages(tenantId);

            var parentTenantId = Guid.Parse(tenantDetails.TenantDetails.ParentTenantId);
            var pricePlans = new Dictionary<Guid, PricePlan>();
            var orderedPkgPricePlans = new Dictionary<string, List<PricePlan>>();

            List<PickupListValue> lstPickupListValues = PickupListProxy.GetAllPickupListValues(PickupListConstants.SelfRegistrationPackagePickupList, parentTenantId.ToString()) ?? new List<PickupListValue>();

            var pickupIds = lstPickupListValues.Where(x => x.Status == true).Select(x => x.ItemId);

            foreach (var lic in licenseHistory.Where(x => !string.IsNullOrEmpty(x.PricePlanId)))
            {
                if (!pricePlans.ContainsKey(Guid.Parse(lic.PricePlanId)))
                {
                    var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(lic.PricePlanId), parentTenantId);

                    if (pricePlan != null)
                    {
                        pricePlans.Add(pricePlan.Id, pricePlan);
                    }
                }
            }

            if (availablePackages != null && availablePackages.Count > 0)
            {
                var packagePricePlans = new Dictionary<string, List<PricePlan>>();
                availablePackages = availablePackages.Where(x => pickupIds.Contains(x.LicensepackageId) || x.LicensepackageId == currentLicense.PackageId).ToList();

                foreach (var package in availablePackages)
                {
                    var plans = BillingPlanProxy.GetPricePlansByPackageId(Guid.Parse(package.LicensepackageId), parentTenantId);

                    if (plans != null && plans.Count > 0)
                    {
                        packagePricePlans.Add(package.LicensepackageId, plans.Values.ToList());
                    }
                }
                orderedPkgPricePlans = packagePricePlans.OrderBy(pp => (pp.Value != null && pp.Value.Count > 0) ? pp.Value[0].Price : 0).ToDictionary(t => t.Key, t => t.Value);
                ViewBag.PackagePricePlans = orderedPkgPricePlans;
            }

            if (string.IsNullOrEmpty(selectedPackageId))
            {
                selectedPackageId = currentLicense.PackageId;
            }

            if (!string.IsNullOrEmpty(selectedPackageId))
            {
                if (availablePackages == null)
                    availablePackages = new List<LicensePackage>();

                var currentPackage = availablePackages.Where(x => x.LicensepackageId.Equals(selectedPackageId, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                if (currentPackage == null)
                {
                    var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(selectedPackageId);
                    if (packageDetails != null)
                    {
                        availablePackages.Add(new LicensePackage
                        {
                            LicensepackageId = selectedPackageId,
                            Name = packageDetails.PackageName,
                            Description = packageDetails.PackageDescription,
                            IntegrateForBilling = packageDetails.IntegrateForBilling,
                            IsEvaluation = packageDetails.IsEvaluation,
                            TenantId = packageDetails.TenantId
                        });
                    }
                }
            }

            List<LicensePackage> avlPkgs = new List<LicensePackage>();

            if (orderedPkgPricePlans != null && orderedPkgPricePlans.Count > 0)
            {
                foreach (var pricedPkg in orderedPkgPricePlans)
                {
                    LicensePackage licensePkg = availablePackages.Where(ap => ap.LicensepackageId == pricedPkg.Key).FirstOrDefault();
                    if (licensePkg != null)
                    {
                        avlPkgs.Add(licensePkg);
                    }
                }
            }

            ViewBag.AvailablePackages = avlPkgs;
            ViewBag.PackageId = selectedPackageId;
            ViewBag.PricePlans = pricePlans;
            ViewBag.TenantDetails = tenantDetails;
            ViewBag.CurrentLicense = currentLicense;
            ViewBag.LicenseHistory = licenseHistory;
        }

        /// <summary>
        /// Fills the tenant settings.
        /// </summary>
        /// <param name="themeName">Name of the theme.</param>
        /// <returns></returns>
        private void FillTenantSettings(TenantSetting tenantSetting)
        {
            string themeName = this.Request["Theme"];
            ViewData["Theme"] = themeName;

            if (!string.IsNullOrEmpty(themeName))
            {
                if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.Theme))
                {
                    tenantSetting.Setting.Attributes[AttributeConstants.Theme] = themeName;
                }
                else
                {
                    tenantSetting.Setting.Attributes.Add(AttributeConstants.Theme, themeName);
                }
            }

            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;
                if (hpf.ContentLength <= 0)
                    return;

                var fileInfo = new System.IO.FileInfo(hpf.FileName);

                if (validImageTypes.Contains(hpf.ContentType))
                {
                    string path = this.Server.MapPath(@"~\" + ConfigHelper.LogoImageFolderPath + @"\");
                    string logoImageName = string.Format(CultureInfo.InvariantCulture, "{0}_logo.{1}", this.TenantId, fileInfo.Extension.StartsWith(".") ? fileInfo.Extension.TrimStart('.') : fileInfo.Extension);

                    FileStorageProxy.StoreFile(path, logoImageName, 0);

                    if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.Logo))
                    {
                        tenantSetting.Setting.Attributes[AttributeConstants.Logo] = logoImageName;
                    }
                    else
                    {
                        tenantSetting.Setting.Attributes.Add(AttributeConstants.Logo, logoImageName);
                    }
                }
                else
                {
                    ModelState.AddModelError("Logo", Resources.MySettingsResource.e_LogoError);
                }
            }
        }

        /// <summary>
        /// Validates the tenant.
        /// </summary>
        /// <param name="tenant">The tenant.</param>
        private void ValidateTenant(Tenant tenant)
        {
            ValidateTenantDetails(tenant.TenantDetails);
            ValidateContactDetails(tenant.ContactDetail);
            ValidateBillingAddress(tenant.Address);
        }

        /// <summary>
        /// Validates the billing address
        /// </summary>
        /// <param name="address"></param>
        private void ValidateBillingAddress(Address address)
        {
            if (string.IsNullOrWhiteSpace(address.Address1))
            {
                this.ModelState.AddModelError("Address.Address1", "");
                this.ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Address);
            }

            if (string.IsNullOrWhiteSpace(address.City))
            {
                ModelState.AddModelError("Address.City", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_City);
            }

            if (string.IsNullOrWhiteSpace(address.State))
            {
                ModelState.AddModelError("Address.State", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_State);
            }

            if (string.IsNullOrWhiteSpace(address.CountryId))
            {
                ModelState.AddModelError("Address.CountryId", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Country);
            }

            if (string.IsNullOrWhiteSpace(address.PostalCode))
            {
                ModelState.AddModelError("Address.PostalCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.UserResource.e_Zipcode);
            }
            else if (!Util.ValidateAlphaNumericWithSpace(address.PostalCode))
            {
                ModelState.AddModelError("Address.PostalCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.UserResource.e_Zipcode_SpecialChars);
            }
        }

        /// <summary>
        /// Validates the contact details.
        /// </summary>
        /// <param name="contactDetails">The contact details.</param>
        private void ValidateContactDetails(ContactDetails contactDetails)
        {
            if (string.IsNullOrWhiteSpace(contactDetails.FirstName))
            {
                ModelState.AddModelError("ContactDetail.FirstName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_FirstName);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.LastName))
            {
                ModelState.AddModelError("ContactDetail.LastName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_LastName);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Phone))
            {
                ModelState.AddModelError("ContactDetail.Phone", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Phone);
            }
            else if (!Util.ValidatePhoneNumber(contactDetails.Phone))
            {
                ModelState.AddModelError("ContactDetail.Phone", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.MySettingsResource.e_PhoneNumberInvalid);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Email))
            {
                ModelState.AddModelError("ContactDetail.Email", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ContactEmail);
            }
            else if (!Util.ValidateEmailId(contactDetails.Email))
            {
                ModelState.AddModelError("ContactDetail.Email", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ContactInvalidEmailId);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Fax))
            {
                contactDetails.Fax = contactDetails.Phone;
            }
        }

        /// <summary>
        /// Validates the tenant details.
        /// </summary>
        /// <param name="tenantDetails">The tenant details.</param>
        private void ValidateTenantDetails(TenantDetails tenantDetails)
        {
            if (string.IsNullOrWhiteSpace(tenantDetails.TenantName))
            {
                ModelState.AddModelError("TenantDetails.TenantName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantName);
            }

            if (string.IsNullOrWhiteSpace(tenantDetails.CompanySize) || tenantDetails.CompanySize.Equals("-1"))
            {
                tenantDetails.CompanySize = null;
            }
        }

        /// <summary>
        /// Validates the user details.
        /// </summary>
        /// <param name="user">The user.</param>
        private void ValidateUserDetails(UserDetails user)
        {
            if (string.IsNullOrWhiteSpace(user.User.FirstName))
            {
                this.ModelState.AddModelError("UserDetails.User.FirstName", "");
                this.ModelState.AddModelError("UserErrorMessage", Resources.TenantResource.e_UserFirstname);
            }

            if (string.IsNullOrWhiteSpace(user.User.LastName))
            {
                ModelState.AddModelError("UserDetails.User.LastName", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_LastName);
            }

            if (!string.IsNullOrEmpty(user.Address.Address1) || !string.IsNullOrEmpty(user.Address.City) || !string.IsNullOrEmpty(user.Address.State)
                   || !string.IsNullOrEmpty(user.Address.CountryId) || !string.IsNullOrEmpty(user.Address.PostalCode))
            {
                if (string.IsNullOrWhiteSpace(user.Address.Address1))
                {
                    this.ModelState.AddModelError("UserDetails.Address.Address1", "");
                    this.ModelState.AddModelError("UserErrorMessage", Resources.TenantResource.e_Address);
                }

                if (string.IsNullOrWhiteSpace(user.Address.City))
                {
                    ModelState.AddModelError("UserDetails.Address.City", "");
                    ModelState.AddModelError("UserErrorMessage", Resources.TenantResource.e_City);
                }

                if (string.IsNullOrWhiteSpace(user.Address.State))
                {
                    ModelState.AddModelError("UserDetails.Address.State", "");
                    ModelState.AddModelError("UserErrorMessage", Resources.TenantResource.e_State);
                }

                if (string.IsNullOrWhiteSpace(user.Address.CountryId))
                {
                    ModelState.AddModelError("UserDetails.Address.CountryId", "");
                    ModelState.AddModelError("UserErrorMessage", Resources.TenantResource.e_Country);
                }

                if (string.IsNullOrWhiteSpace(user.Address.PostalCode))
                {
                    ModelState.AddModelError("UserDetails.Address.PostalCode", "");
                    ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_Zipcode);
                }
            }
            else
            {
                user.Address = null;
            }
        }

        /// <summary>
        /// Load theme list and tenant set logo
        /// </summary>
        private void LoadThemeAndLogo()
        {
            string logoName = ConfigHelper.DefaultLogo;
            string themeName = ConfigHelper.DefaultTheme;
            TenantSetting tenantSettings = TenantSettingsProxy.GetTenantSettings(this.TenantId);

            if (tenantSettings != null && tenantSettings.Setting != null && tenantSettings.Setting.Attributes != null && tenantSettings.Setting.Attributes.Count > 0)
            {
                if (tenantSettings.Setting.Attributes.ContainsKey(AttributeConstants.Logo))
                {
                    logoName = tenantSettings.Setting.Attributes[AttributeConstants.Logo];
                }

                if (tenantSettings.Setting.Attributes.ContainsKey(AttributeConstants.Theme))
                {
                    themeName = tenantSettings.Setting.Attributes[AttributeConstants.Theme];
                }
            }

            var appThemeSubDir = new DirectoryInfo(Server.MapPath("~/App_Themes")).GetDirectories();

            List<SelectListItem> themeList = appThemeSubDir.Where(x => x.Name != ".svn")
                                                            .Select(x =>
                                                                new SelectListItem
                                                                {
                                                                    Text = x.Name,
                                                                    Value = x.Name,
                                                                    Selected = (x.Name == themeName)
                                                                }).ToList();
            ViewData["Themes"] = themeList;
            ViewData["ImageURL"] = Url.Content(@"~/" + ConfigHelper.LogoImageFolderPath + @"/" + logoName);
        }

        /// <summary>
        /// Loads the size of the company.
        /// </summary>
        /// <param name="companySize">Size of the company.</param>
        /// <param name="tenantId">The tenant id.</param>
        private void LoadCompanySize(string companySize, string tenantId)
        {
            List<PickupListValue> lstPickupListValues = PickupListProxy.GetAllPickupListValues(PickupListConstants.CompanySizePickupList, tenantId);
            Dictionary<string, string> companySizeDetails = new Dictionary<string, string>();
            companySizeDetails.Add("-1", "-Select-");
            if (lstPickupListValues != null && lstPickupListValues.Count > 0)
            {
                lstPickupListValues.ForEach(x => companySizeDetails.Add(x.ItemId, x.ItemName));
            }

            ViewData["CompanySize"] = new SelectList(companySizeDetails, "Key", "Value", companySize);
        }

        /// <summary>
        /// Loads the country.
        /// </summary>
        /// <param name="selectedId">The selected id.</param>
        private void LoadCountry(string selectedId)
        {
            try
            {
                List<Country> lstcountry = CountryProxy.GetAllCountry();
                Country country = new Country { CountryId = "", Name = "--Select Country--" };
                lstcountry.Insert(0, country);
                ViewData["Address.CountryId"] = new SelectList((IEnumerable)lstcountry, "CountryId", "Name", selectedId);
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_CountryLoaded);
            }
        }

        /// <summary>
        /// Load user details country
        /// </summary>
        /// <param name="selectedId"></param>
        private void LoadUserCountry(string selectedId)
        {
            try
            {
                List<Country> lstcountry = CountryProxy.GetAllCountry();
                Country country = new Country { CountryId = "", Name = "--Select Country--" };
                lstcountry.Insert(0, country);
                ViewData["UserDetails.Address.CountryId"] = new SelectList((IEnumerable)lstcountry, "CountryId", "Name", selectedId);
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_CountryLoaded);
            }
        }

        #endregion

        #region AuthServerPrivateMembers

        /// <summary>
        /// This method is used to initialize the tenant settings.
        /// </summary>
        /// <returns></returns>
        private TenantSetting InitTenantSettings()
        {
            TenantSetting setting = null;
            if (UserIdentity.TenantSettings == null || UserIdentity.TenantSettings.Count > 0)
            {
                setting = new TenantSetting
                {
                    TenantID = this.TenantId,
                    Setting = new Settings
                    {
                        Attributes = UserIdentity.TenantSettings
                    }
                };
            }
            else
            {
                setting = TenantSettingsProxy.GetTenantSettings(this.TenantId);
            }
            return setting;
        }

        /// <summary>
        /// This method is used for authorization settings.
        /// </summary>
        private void AuthorizationSettings()
        {
            Dictionary<string, string> setting = GetTenantSettingCollection();

            #region Basic Settings

            var baseSettings = InitReadOnlySettings(setting);
            ViewBag.AuthSettings = null;
            if (baseSettings != null)
                ViewBag.AuthSettings = Newtonsoft.Json.JsonConvert.SerializeObject(baseSettings);

            #endregion

            #region Social Identity Settings

            var socialSettings = InitSocialLoginSettings(baseSettings, setting);
            ViewBag.Settings = null;
            if (socialSettings != null)
                ViewBag.Settings = Newtonsoft.Json.JsonConvert.SerializeObject(socialSettings);

            #endregion

            #region LDAP Auth
            ViewBag.LDAPConfig = null;
            ViewBag.LdapAuthInfo = null;

            var ldapSettings = InitLDAPSettings(baseSettings, setting);
            if (ldapSettings != null)
            {
                ViewBag.LDAPConfig = Newtonsoft.Json.JsonConvert.SerializeObject(ldapSettings.Item1);
                ViewBag.LdapAuthInfo = Newtonsoft.Json.JsonConvert.SerializeObject(ldapSettings.Item2);
            }
            #endregion

            #region User Registration Settings

            ViewBag.AllowUserRegistration = false;
            if (setting != null && setting.Count > 0 && setting.ContainsKey(AuthServerSettingConstants.AllowUserRegistration))
            {
                ViewBag.AllowUserRegistration = Convert.ToBoolean(setting[AuthServerSettingConstants.AllowUserRegistration]);
            }

            #endregion
        }

        /// <summary>
        /// This method is used for getting the collection of tenant settings.
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetTenantSettingCollection()
        {
            Dictionary<string, string> setting = null;
            if (UserIdentity.TenantSettings != null && UserIdentity.TenantSettings.Count > 0)
            {
                setting = UserIdentity.TenantSettings;
            }
            else
            {
                var tmpSetting = TenantSettingsProxy.GetTenantSettings(this.TenantId);
                setting = tmpSetting != null && tmpSetting.Setting != null && tmpSetting.Setting.Attributes != null
                        ? tmpSetting.Setting.Attributes : new Dictionary<string, string>();
            }
            return setting;
        }

        /// <summary>
        /// This method is used to initialize the LDAP settings.
        /// </summary>
        /// <param name="baseSettings">base settings.</param>
        /// <param name="setting">settings.</param>
        /// <returns></returns>
        private Tuple<AuthorizationSettings, LDAPAuthenticationInfo> InitLDAPSettings(AuthorizationTypeSettings baseSettings, Dictionary<string, string> setting)
        {
            if (baseSettings.TypeId != AppSettingHelper.LDAPIdentity)
                return null;

            AuthorizationSettings endPointConfig = null;

            if (setting != null && setting.Count > 0 && setting.ContainsKey(AuthServerSettingConstants.AuthorizationSettings))
            {
                endPointConfig = JsonConvert.DeserializeObject<AuthorizationSettings>(setting[AuthServerSettingConstants.AuthorizationSettings]);
            }

            LDAPAuthenticationInfo authInfo = null;

            if (setting.ContainsKey(AuthServerSettingConstants.LDAPAdminUserName) && setting.ContainsKey(AuthServerSettingConstants.LDAPAdminUserPassword))
            {
                authInfo = new LDAPAuthenticationInfo
                {
                    LdapUserName = setting[AuthServerSettingConstants.LDAPAdminUserName],
                    LdapUserPassword = setting[AuthServerSettingConstants.LDAPAdminUserPassword],
                    LdapConnectionString = ServiceLocator.Resolve<IPasswordEncrptionService>().DecryptPassword(this.TenantId, setting[AuthServerSettingConstants.LDAPConnectionString])
                };
            }

            return new Tuple<AuthorizationSettings, LDAPAuthenticationInfo>(endPointConfig, authInfo);
        }

        /// <summary>
        /// This method is used to initialize social login settings.
        /// </summary>
        /// <param name="authTypeSetting">authentication type setting.</param>
        /// <param name="setting">collection of settings.</param>
        /// <returns></returns>
        private List<AuthorizationTypeClaimMap> InitSocialLoginSettings(AuthorizationTypeSettings authTypeSetting, Dictionary<string, string> setting)
        {
            if (authTypeSetting.TypeId != AppSettingHelper.SocialIdentity)
                return null;

            string claimSetting = null;

            if (setting != null && setting.Count > 0 && setting.ContainsKey(AuthServerSettingConstants.ClaimMap))
            {
                claimSetting = setting[AuthServerSettingConstants.ClaimMap];
            }

            Dictionary<string, string> claimMap = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(claimSetting))
                claimMap = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(claimSetting);

            List<string> authManagerRoles = GetAuthorizationManagerRoles();

            // for any normal user, show values in read only mode
            if (!UserIdentity.Roles.Any(r => authManagerRoles.Contains(r, StringComparer.OrdinalIgnoreCase)))
            {
                return new List<AuthorizationTypeClaimMap>
                {
                    new AuthorizationTypeClaimMap
                    {
                        ReadOnly = true,
                        AuthTypeId = authTypeSetting.TypeId,
                        AuthTypeName = authTypeSetting.TypeName,
                        ClaimMap = claimMap.ContainsKey(authTypeSetting.TypeId) ? claimMap[authTypeSetting.TypeId] : UserIdentificationClaimMapping.Email.ToString("F")
                    }
                };
            }
            else
            {
                var authTypes = CelloSaaSWebClient.Services.AuthenticationService.GetAuthenticationTypes(AppSettingHelper.SocialIdentity);
                if (authTypes != null && authTypes.Count > 0 && authTypes.ContainsKey(AppSettingHelper.SocialIdentity))
                {
                    List<AuthorizationTypeClaimMap> maps = new List<AuthorizationTypeClaimMap>();
                    foreach (var typ in authTypes[AppSettingHelper.SocialIdentity])
                    {
                        maps.Add(new AuthorizationTypeClaimMap
                        {
                            ReadOnly = true,
                            AuthTypeId = typ.Key,
                            AuthTypeName = typ.Value,
                            ClaimMap = claimMap.ContainsKey(typ.Key) ? claimMap[typ.Key] : UserIdentificationClaimMapping.Email.ToString("F")
                        });
                    }
                    return maps;
                }
            }
            return null;
        }

        /// <summary>
        /// This method is used to initialize the read only settings.
        /// </summary>
        /// <param name="tenantSetting">collection of tenant settings.</param>
        /// <returns></returns>
        private AuthorizationTypeSettings InitReadOnlySettings(Dictionary<string, string> tenantSetting)
        {
            if (tenantSetting == null || tenantSetting.Count < 1)
            {
                return new AuthorizationTypeSettings
                {
                    ProviderId = AppSettingHelper.CelloOpenIdentity,
                    TypeId = AppSettingHelper.CelloAuthentication,
                    DefaultScopes = AppSettingHelper.DefaultScopes
                };
            }
            AuthorizationTypeSettings settings = new AuthorizationTypeSettings();
            // init the values from the tenant settings in case of a tenant admin

            List<string> authManagerRoles = GetAuthorizationManagerRoles();

            if (UserIdentity.Roles.Any(r => authManagerRoles.Contains(r, StringComparer.OrdinalIgnoreCase)))
            {
                settings.ProviderId = tenantSetting.ContainsKey(AuthServerSettingConstants.IdentityProviders)
                                        ? tenantSetting[AuthServerSettingConstants.IdentityProviders] : ConfigHelper.CelloOpenIdProvider;
                settings.TypeId = tenantSetting.ContainsKey(AuthServerSettingConstants.DefaultAuthenticationType)
                                        ? tenantSetting[AuthServerSettingConstants.DefaultAuthenticationType] : null;
                settings.DefaultScopes = tenantSetting.ContainsKey(AuthServerSettingConstants.DefaultScopes)
                                        ? tenantSetting[AuthServerSettingConstants.DefaultScopes] : AppSettingHelper.DefaultScopes;
            }

            var identityProviders = CelloSaaSWebClient.Services.AuthenticationService.GetAuthenticationProviders();

            if (identityProviders != null && identityProviders.Count > 0 && identityProviders.ContainsKey(settings.ProviderId))
                settings.ProviderName = identityProviders[settings.ProviderId];

            var authenticationTypes = CelloSaaSWebClient.Services.AuthenticationService.GetAuthenticationTypes(settings.ProviderId);

            if (authenticationTypes != null && authenticationTypes.Count > 0 && authenticationTypes.ContainsKey(settings.ProviderId)
                && authenticationTypes[settings.ProviderId].ContainsKey(settings.TypeId))
            {
                settings.TypeName = authenticationTypes[settings.ProviderId][settings.TypeId];
            }

            var userSettings = UserSettingsProxy.GetUserSettings(this.UserId);

            if (userSettings == null || userSettings.Setting == null || userSettings.Setting.Attributes == null || userSettings.Setting.Attributes.Count < 1)
                return settings;

            if (userSettings.Setting.Attributes.ContainsKey(AuthServerSettingConstants.LoginProvider))
                settings.TypeId = userSettings.Setting.Attributes[AuthServerSettingConstants.LoginProvider];

            settings.ProviderId = settings.ProviderId ?? AppSettingHelper.CelloOpenIdentity;
            settings.TypeId = settings.TypeId ?? AppSettingHelper.CelloAuthentication;

            return settings;
        }

        /// <summary>
        /// This method is used to get the list of authorization manager roles.
        /// </summary>
        /// <returns></returns>
        private static List<string> GetAuthorizationManagerRoles()
        {
            var authManager = AppSettingHelper.AuthorizationSettingManagerRoles;

            List<string> authManagerRoles = string.IsNullOrEmpty(authManager)
                                                ? new List<string> { RoleConstants.TenantAdmin }
                                                : (authManager.Contains(',') ? authManager.Split(',').ToList() : new List<string> { authManager });
            return authManagerRoles;
        }

        /// <summary>
        /// This method is used to manage authorization settings.
        /// </summary>
        /// <param name="collection"> from collection.</param>
        private void ManageAuthorizationSettings(FormCollection collection)
        {
            bool userRegistration = Convert.ToBoolean(collection[AuthServerSettingConstants.AllowUserRegistration]);

            TenantSetting setting = InitTenantSettings();

            //check existing setting
            if (setting != null && setting.Setting != null && setting.Setting.Attributes != null && setting.Setting.Attributes.Count > 0 && (!setting.Setting.Attributes.ContainsKey(AuthServerSettingConstants.AllowUserRegistration)))
            {
                if (userRegistration)
                    setting.Setting.Attributes.Add(AuthServerSettingConstants.AllowUserRegistration, true.ToString());
            }
            else if (setting != null && setting.Setting != null && setting.Setting.Attributes != null && setting.Setting.Attributes.Count > 0 && setting.Setting.Attributes.ContainsKey(AuthServerSettingConstants.AllowUserRegistration) && userRegistration != Convert.ToBoolean(setting.Setting.Attributes[AuthServerSettingConstants.AllowUserRegistration]) && userRegistration)
                setting.Setting.Attributes[AuthServerSettingConstants.AllowUserRegistration] = true.ToString();

            TenantSettingsProxy.UpdateTenantSettings(setting);

            IPasswordEncrptionService cryptoService = ServiceLocator.Resolve<CelloSaaS.Library.Encryption.IPasswordEncrptionService>();

            AuthorizationTypeSettings basicSettings = new AuthorizationTypeSettings
            {
                DefaultScopes = collection["DefaultScopes"],
                ProviderId = collection["ProviderId"],
                TypeId = collection["TypeId"]
            };

            ViewBag.AuthSettings = JsonConvert.SerializeObject(basicSettings);

            if (string.IsNullOrEmpty(collection["DefaultScopes"]))
            {
                ModelState.AddModelError("", "The default scopes cannot be null or empty");
            }

            if (collection["ProviderId"] == AppSettingHelper.SocialIdentity)
            {
                UpdateSocialAuthSettings(setting);
            }
            else if (collection["ProviderId"] == AppSettingHelper.LDAPIdentity)
            {
                UpdateLDAPAuthSettings(setting, cryptoService);
            }
        }

        /// <summary>
        /// This method is used to update social authentication settings.
        /// </summary>
        /// <param name="setting">tenant setting.</param>
        private void UpdateSocialAuthSettings(TenantSetting setting)
        {
            List<AuthorizationTypeClaimMap> claimMaps = new List<AuthorizationTypeClaimMap>();
            TryUpdateModel(claimMaps);

            ViewBag.Settings = JsonConvert.SerializeObject(claimMaps);

            if (!ModelState.IsValid) return;
            // store the map as a claim map
            var socialClaimMapping = claimMaps.Select(x => new { x.AuthTypeId, x.ClaimMap }).ToDictionary(t => t.AuthTypeId, t => t.ClaimMap);

            if (setting.Setting.Attributes.ContainsKey(AuthServerSettingConstants.ClaimMap))
            {
                setting.Setting.Attributes.Remove(AuthServerSettingConstants.ClaimMap);
            }

            setting.Setting.Attributes.Add(AuthServerSettingConstants.ClaimMap, Newtonsoft.Json.JsonConvert.SerializeObject(socialClaimMapping));

            TenantSettingsProxy.UpdateTenantSettings(setting);
        }

        /// <summary>
        /// This method is used to update LDAP Authentication settings.
        /// </summary>
        /// <param name="setting">tenant settings.</param>
        /// <param name="cryptoService">password encryption service.</param>
        private void UpdateLDAPAuthSettings(TenantSetting setting, IPasswordEncrptionService cryptoService)
        {
            LDAPAuthenticationInfo ldapAuthInfo = new LDAPAuthenticationInfo();
            TryUpdateModel(ldapAuthInfo);

            ViewBag.LDAPAuthInfo = JsonConvert.SerializeObject(ldapAuthInfo);

            AuthorizationSettings endPointConfig = new AuthorizationSettings();
            TryUpdateModel(endPointConfig);

            ViewBag.LDAPConfig = JsonConvert.SerializeObject(endPointConfig);

            if (!ModelState.IsValid) return;
            if (setting.Setting.Attributes.ContainsKey(AuthServerSettingConstants.AuthorizationSettings))
                setting.Setting.Attributes.Remove(AuthServerSettingConstants.AuthorizationSettings);

            if (setting.Setting.Attributes.ContainsKey(AuthServerSettingConstants.LDAPAdminUserName))
                setting.Setting.Attributes.Remove(AuthServerSettingConstants.LDAPAdminUserName);

            if (setting.Setting.Attributes.ContainsKey(AuthServerSettingConstants.LDAPAdminUserPassword))
                setting.Setting.Attributes.Remove(AuthServerSettingConstants.LDAPAdminUserPassword);

            if (setting.Setting.Attributes.ContainsKey(AuthServerSettingConstants.LDAPConnectionString))
                setting.Setting.Attributes.Remove(AuthServerSettingConstants.LDAPConnectionString);

            setting.Setting.Attributes.Add(AuthServerSettingConstants.AuthorizationSettings, Newtonsoft.Json.JsonConvert.SerializeObject(endPointConfig));
            setting.Setting.Attributes.Add(AuthServerSettingConstants.LDAPAdminUserName, ldapAuthInfo.LdapUserName);
            setting.Setting.Attributes.Add(AuthServerSettingConstants.LDAPAdminUserPassword, cryptoService.EncryptPassword(this.TenantId, ldapAuthInfo.LdapUserPassword));
            setting.Setting.Attributes.Add(AuthServerSettingConstants.LDAPConnectionString, cryptoService.EncryptPassword(this.TenantId, ldapAuthInfo.LdapConnectionString));
        }

        #endregion
    }
}
