﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Notification.Model;
using CelloSaaS.Notification.Configuration;
using CelloSaaS.Notification.Model.Dispatch;
using CelloSaaS.Notification.Model.Email;
using CelloSaaS.Notification.Model.Ftp;
using CelloSaaS.Library;
using System.Text.RegularExpressions;
using CelloSaaS.Notification.Model.Content;
using CelloSaaS.Library.Configuration;
using CelloSaaS.Model.MasterDataManagement;
using CelloSaaS.ServiceProxies.MasterDataManagement;
using CelloSaaS.ServiceContracts.MasterDataManagement;
using CelloSaaS.Notification.NotificationManager;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.Notification.ServiceProxies;
using System.Data.Common;
using CelloSaaS.Template.Model;
using CelloSaaS.Template.ServiceProxies;
using System.Text;
using CelloSaaS.Model.TenantManagement;
using System.Globalization;
namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for notification configurations.
    /// </summary>
    public class NotificationConfigController : CelloSaaS.View.CelloController
    {
        // Logger policy name
        private const string _defaultPolicy = "NotificationExceptionLogger";

        private string TenantId { get { return TenantContext.GetTenantId("_Notification"); } }

        /// <summary>
        /// This Success is used to return Success
        /// </summary>
        private const string Sussess = "Success";

        #region Notification Master Configuration

        /// <summary>
        /// This method is used to shows the notification master details.
        /// </summary>
        /// <returns></returns>
        public ActionResult NotificationMasterDetails()
        {
            NotificationDetailsList();
            return View();
        }

        /// <summary>
        /// This method is used to adds the master notification.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageNotification(string notificationMasterId)
        {
            ViewData["IsProductAdmin"] = ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase);
            NotificationDetails notificationDetails = null;          
            
            try
            {
                // Get notification details
                notificationDetails = string.IsNullOrEmpty(notificationMasterId) ? new NotificationDetails()
                                        : NotificationConfigurationProxy.GetNotificationMasterDetailsById(notificationMasterId);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, "NotificationExceptionLogger");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_AccessDenied);
            }
            catch (NotificationException notificationException)
            {
                ExceptionService.HandleException(notificationException, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_NotificationDetails);
            }

            return PartialView("ManageNotification", notificationDetails);
        }

        /// <summary>
        /// This method is used to adds the notification master.
        /// </summary>
        /// <param name="notificationDetails">notification details.</param>
        /// <param name="isPublic">isPublic (true/false).</param>
        /// <returns></returns>
        [HttpPost]
        public object ManageNotification(NotificationDetails notificationDetails, bool isPublic)
        {
            ValidateNotificationDetails(notificationDetails);
            notificationDetails.CreatedOn = DateTime.Now;
            notificationDetails.CreatedBy = UserIdentity.UserId;
            notificationDetails.TenantId = isPublic ? string.Empty : this.TenantId;

            if (ModelState.IsValid)
            {
                try
                {
                    string result = string.IsNullOrEmpty(notificationDetails.NotificationId)
                       ? NotificationConfigurationProxy.ConfigureNotificationMaster(notificationDetails)
                       : NotificationConfigurationProxy.UpdateNotificationMasterDetails(notificationDetails);

                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("ConfigStatusMessage", Resources.NotificationConfigResource.s_NotificationMasterConfig);
                        return Sussess;
                    }
                    else
                    {
                        ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_NotificationMasterConfig);
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_AccessDenied);
                }
                catch (DuplicateNotificationNameException duplicateException)
                {
                    ExceptionService.HandleException(duplicateException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_DuplicateNotificationName);
                }
                catch (NotificationException notificationException)
                {
                    ExceptionService.HandleException(notificationException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_NotificationMasterConfig);
                }

            }

            ViewData["IsProductAdmin"] = ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase);
            return PartialView("ManageNotification", notificationDetails);
        }

        /// <summary>
        /// This method is used to removes the notification master details.
        /// </summary>
        /// <param name="notificationMasterId">notification master identifier.</param>
        /// <returns></returns>
        public ActionResult RemoveMasterDetails(string notificationMasterId)
        {
            try
            {
                NotificationConfigurationProxy.RemoveConfiguration(notificationMasterId);
                ModelState.AddModelError("Success", Resources.NotificationConfigResource.s_DeleteNotification);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("Error", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (NotificationException notificationException)
            {
                ExceptionService.HandleException(notificationException, _defaultPolicy);
                ModelState.AddModelError("Error", Resources.NotificationConfigResource.e_DeleteNotification);
            }

            NotificationDetailsList();
            return PartialView("NotificationMaster");

        }

        /// <summary>
        /// This method is used to shows the notification details.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowNotificationDetails()
        {
            // Get notification details
            NotificationDetailsList();
            if (Request.IsAjaxRequest())
            {
                return PartialView("NotificationMaster");
            }
            return View("NotificationMasterDetails");
        }

        #endregion

        /// <summary>
        /// This method is used to manages the notification master details.
        /// </summary>
        /// <param name="notificationMasterId">notification master identifier.</param>
        /// <returns></returns>
        public object ManageMasterDetails(string notificationMasterId, string isGlobalEdit)
        {
            NotificationDetails notificationDetails = new NotificationDetails();
            string tenantId = this.TenantId;
            if (Convert.ToBoolean(isGlobalEdit, CultureInfo.InvariantCulture))
            {
                tenantId = string.Empty;
            }
            try
            {
                notificationDetails = NotificationServiceProxy.GetNotificationDetails(notificationMasterId, tenantId);

                if (notificationDetails != null)
                {
                    ViewData["IsGlobal"] = false;
                    if (string.IsNullOrEmpty(notificationDetails.TenantId))
                    {
                        ViewData["IsGlobal"] = true;
                        ViewData["GlobalEdit"] = true;
                        if (Convert.ToBoolean(isGlobalEdit, CultureInfo.InvariantCulture))
                        {
                            ViewData["IsGlobal"] = false;
                        }
                    }

                    ViewData["NotificationMasterId"] = notificationDetails.NotificationId;
                    ViewData["NotificationName"] = notificationDetails.NotificationName;
                    ViewData["NotificationType"] = notificationDetails.NotificationDestination.NotificationDestinationType.ToString();
                    ViewData["NotificationContentDetailsId"] = notificationDetails.NotificationContent.NotificationContentId;

                    if (notificationDetails.NotificationContent.Template != null)
                    {
                        GetTemplateNames(this.TenantId, notificationDetails.NotificationContent.Template.TemplateId);
                        ViewData["NotificationTemplate"] = notificationDetails.NotificationContent.Template;
                        ViewData["NotificationTemplateId"] = notificationDetails.NotificationContent.Template.TemplateId;
                        ViewData["TemplateName"] = notificationDetails.NotificationContent.Template.TemplateName;
                        ViewData["HasTemplate"] = true;
                    }
                    else
                    {
                        GetTemplateNames(this.TenantId, string.Empty);
                    }

                    if (notificationDetails.NotificationDestination.NotificationDestinationType.Equals(NotificationDestinationType.Email))
                    {
                        EmailDestination emailDestination = (EmailDestination)notificationDetails.NotificationDestination;
                        TextMessageContent content = (TextMessageContent)notificationDetails.NotificationContent;
                        ViewData["EmailContent"] = content;
                        ViewData["EmailDestination"] = emailDestination;
                        ViewData["TextContent"] = content.Content;
                        return PartialView("ManageEmailDetails");
                    }
                    else if (notificationDetails.NotificationDestination.NotificationDestinationType.Equals(NotificationDestinationType.BatchEmail))
                    {
                        BatchEmailDestination batchEmailDestination = (BatchEmailDestination)notificationDetails.NotificationDestination;
                        EmailDestination emailDestination = (EmailDestination)batchEmailDestination.CommonNotificationDestination;
                        TextMessageContent content = (TextMessageContent)notificationDetails.NotificationContent;
                        ViewData["EmailContent"] = content;
                        ViewData["EmailDestination"] = emailDestination;
                        ViewData["TextContent"] = content.Content;
                        return PartialView("ManageEmailDetails");
                    }
                    else if (notificationDetails.NotificationDestination.NotificationDestinationType.Equals(NotificationDestinationType.Ftp) || notificationDetails.NotificationDestination.NotificationDestinationType.Equals(NotificationDestinationType.Sftp))
                    {
                        FtpDestination ftpDestination = (FtpDestination)notificationDetails.NotificationDestination;
                        FileContent fileContent = (FileContent)notificationDetails.NotificationContent;
                        ViewData["FtpDestination"] = ftpDestination;
                        ViewData["FileContent"] = fileContent;
                        return PartialView("ManageFtpDetails");
                    }
                    else if (notificationDetails.NotificationDestination.NotificationDestinationType.Equals(NotificationDestinationType.BatchFtp))
                    {
                        BatchFtpDestination batchFtpDestination = (BatchFtpDestination)notificationDetails.NotificationDestination;
                        FtpDestination ftpDestination = (FtpDestination)batchFtpDestination.CommonNotificationDestination;
                        FileContent fileContent = (FileContent)notificationDetails.NotificationContent;
                        ViewData["FtpDestination"] = ftpDestination;
                        ViewData["FileContent"] = fileContent;
                        return PartialView("ManageFtpDetails");
                    }
                    else if (notificationDetails.NotificationDestination.NotificationDestinationType.Equals(NotificationDestinationType.SystemNotification))
                    {
                        SystemNotificationDestination systemNotificationDestination = (SystemNotificationDestination)notificationDetails.NotificationDestination;
                        SystemNotificationContent systemNotificationContent = (SystemNotificationContent)notificationDetails.NotificationContent;
                        ViewData["SystemNotificationContent"] = systemNotificationContent;
                        ViewData["SystemNotificationDestination"] = systemNotificationDestination;
                        return PartialView("ManageSystemNotificationDetails");
                    }
                    else if (notificationDetails.NotificationDestination.NotificationDestinationType.Equals(NotificationDestinationType.BatchSystemNotification))
                    {
                        BatchSystemNotificationDestination systemNotificationDestination = (BatchSystemNotificationDestination)notificationDetails.NotificationDestination;
                        SystemNotificationContent systemNotificationContent = (SystemNotificationContent)notificationDetails.NotificationContent;
                        ViewData["Type"] = NotificationContentType.BatchSystemNotification.ToString();
                        ViewData["SystemNotificationContent"] = systemNotificationContent;
                        ViewData["SystemNotificationDestination"] = (SystemNotificationDestination)systemNotificationDestination.CommonNotificationDestination;
                        return PartialView("ManageSystemNotificationDetails");
                    }
                }
                else
                {
                    notificationDetails = NotificationConfigurationProxy.GetNotificationMasterDetailsById(notificationMasterId);

                    if (string.IsNullOrEmpty(notificationDetails.TenantId) && !Convert.ToBoolean(isGlobalEdit,CultureInfo.InvariantCulture))
                    {
                        return "NoDispatch";
                    }
                    ViewData["NotificationMasterId"] = notificationDetails.NotificationId;
                    ViewData["NotificationName"] = notificationDetails.NotificationName;
                }
            }
            catch (ArgumentNullException)
            {
                ModelState.AddModelError("NotificationDetails", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException)
            {
                ModelState.AddModelError("NotificationDetails", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException)
            {
                ModelState.AddModelError("NotificationDetails", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.ConfigurationResource.e_AccessDenied);
            }
            catch (NotificationException)
            {
                ModelState.AddModelError("NotificationDetails", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_NotificationDetails);
            }

            GetNotificationTypes();

            return PartialView("ManageMasterDetails", notificationDetails);

        }

        /// <summary>
        /// This method is used to manages the master details.
        /// </summary>
        /// <param name="formCollection">form collection</param>
        /// <param name="notificationType">type of the notification.</param>
        /// <param name="notificationId">notification identification.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManageMasterDetails(FormCollection formCollection, string notificationType, string notificationId)
        {
            ViewData["GlobalConfig"] = ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase) && Convert.ToBoolean(formCollection["GlobalConfig"]).Equals(true) && string.IsNullOrEmpty(formCollection["TenantId"]);

            ViewData["HasTemplate"] = string.IsNullOrEmpty(formCollection["NotificationTemplateId"]) ? false : true;

            NotificationDetails notificationDetails = new NotificationDetails();
            notificationDetails.NotificationId = notificationId;
            notificationDetails.TenantId = this.TenantId;
            notificationDetails.CreatedBy = UserIdentity.UserId;

            ViewData["NotificationMasterId"] = notificationId;
            ViewData["NotificationType"] = notificationType;
            ViewData["NotificationName"] = formCollection["NotificationName"];
            GetTemplateNames(notificationDetails.TenantId, formCollection["NotificationTemplateId"] ?? string.Empty);
            NotificationDestinationType destinationType;

            if (Enum.TryParse(notificationType, out destinationType))
            {
                switch (destinationType)
                {
                    case NotificationDestinationType.Email:
                        SetEmailViewData(formCollection);
                        return PartialView("ManageEmailDetails");

                    case NotificationDestinationType.BatchEmail:
                        SetEmailViewData(formCollection);
                        return PartialView("ManageEmailDetails");

                    case NotificationDestinationType.Ftp:
                        SetFtpViewData(formCollection, NotificationDestinationType.Ftp);
                        return PartialView("ManageFtpDetails");

                    case NotificationDestinationType.BatchFtp:
                        SetFtpViewData(formCollection, NotificationDestinationType.BatchFtp);
                        return PartialView("ManageFtpDetails");

                    case NotificationDestinationType.Sftp:
                        SetFtpViewData(formCollection, NotificationDestinationType.Sftp);
                        return PartialView("ManageFtpDetails");

                    case NotificationDestinationType.BatchSftp:
                        SetFtpViewData(formCollection, NotificationDestinationType.BatchSftp);
                        return PartialView("ManageFtpDetails");

                    case NotificationDestinationType.SystemNotification:
                        SetSystemNotificationViewData(formCollection);
                        return PartialView("ManageSystemNotificationDetails");

                    case NotificationDestinationType.BatchSystemNotification:
                        ViewData["IsBatch"] = destinationType.Equals(NotificationDestinationType.BatchSystemNotification);
                        SetSystemNotificationViewData(formCollection);
                        return PartialView("ManageSystemNotificationDetails");

                    default:
                        break;
                }
            }

            GetNotificationTypes();
            return PartialView("ManageMasterDetails", notificationDetails);
        }

        /// <summary>
        /// This method is used to manages the email details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public object ManageEmailDetails(FormCollection formCollection, bool hasTemplate)
        {
            EmailDestination emailDestination = new EmailDestination();
            TryUpdateModel(emailDestination);

            string NotificationTemplateId = formCollection["NotificationTemplateId"];
            string content = formCollection["TextContent"];
            string templateId = formCollection["Templates"];
            string notificationType = formCollection["NotificationType"];

            TextMessageContent notificationContent = new TextMessageContent();
            TryUpdateModel(notificationContent);

            notificationContent.TemplateId = templateId;
            INotificationTemplate notificationTemplate = null;
            if (hasTemplate)
            {
                notificationTemplate = new TextMessageNotificationTemplate();
                notificationTemplate.TemplateId = templateId;
                ValidateTemplate(notificationTemplate);
                notificationContent.Content = null;
                content = string.Empty;
            }
            else
            {
                notificationContent.TemplateId = string.Empty;
                notificationContent.RuleSetCode = string.Empty;
                notificationContent.Content = new StringBuilder(content);
            }

            notificationContent.Template = notificationTemplate;
            ValidateEmailDestination(emailDestination);
            if (!hasTemplate)
            {
                if (string.IsNullOrEmpty(content) || content == "<br>")
                {
                    ViewData["HasTemplate"] = false;
                    notificationContent.Content = new StringBuilder(" ");
                    ModelState.AddModelError("Content", "");
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ContentMandatory);
                }
            }
            if (string.IsNullOrEmpty(notificationContent.Subject))
            {
                ModelState.AddModelError("Subject", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SubjectMandatory);
            }
            if (ModelState.IsValid)
            {
                string result = string.Empty;
                try
                {
                    if (string.IsNullOrEmpty(emailDestination.NotificationDestinationId))
                    {
                        NotificationDetails notificationDetails = new NotificationDetails { NotificationId = formCollection["NotificationMasterId"] };

                        NotificationDestinationType destinationType;
                        if (Enum.TryParse(notificationType, out destinationType))
                        {
                            switch (destinationType)
                            {
                                case NotificationDestinationType.Email:
                                    notificationDetails.NotificationContent = notificationContent;
                                    notificationDetails.NotificationDestination = emailDestination;
                                    break;
                                case NotificationDestinationType.BatchEmail:
                                    notificationDetails.NotificationContent = new BatchTextMessageContent { CommonNotificationContent = notificationContent };
                                    notificationDetails.NotificationDestination = new BatchEmailDestination { CommonNotificationDestination = emailDestination };
                                    notificationDetails.NotificationContent.Template = notificationTemplate;
                                    notificationDetails.NotificationContent.TemplateId = notificationContent.TemplateId;
                                    break;
                            }
                        }

                        if (ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(formCollection["IsGlobalConfig"]) && Convert.ToBoolean(formCollection["IsGlobalConfig"]))
                        {
                            notificationDetails.TenantId = string.Empty;
                        }
                        else
                        {
                            notificationDetails.TenantId = this.TenantId;
                        }

                        EmailDetails emailDetails = FillEmailDetails(formCollection);
                        emailDetails.EnableSSL = emailDestination.EnableSSL;
                        notificationContent.CreatedBy = emailDetails.CreatedBy = notificationDetails.CreatedBy = UserIdentity.UserId;
                        NotificationConfigurationProxy.ConfigureEmailDetails(notificationDetails, emailDetails);
                        result = notificationDetails.NotificationId;
                    }
                    else
                    {
                        result = NotificationConfigurationProxy.UpdateEmailDetails(emailDestination, notificationContent, content);
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterNullOrEmpty);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterNullOrEmpty);
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_AccessDenied);
                }
                catch (NotificationException notificationException)
                {
                    ExceptionService.HandleException(notificationException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_UpdateEmailDispatch);
                }

                if (!string.IsNullOrEmpty(result))
                {
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.s_UpdateEmailDispatch);
                    return Content(Sussess);
                }
                else
                {
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_UpdateEmailDispatch);
                }
            }
            GetTemplateNames(this.TenantId, templateId);
            ViewData["NotificationType"] = notificationType;
            ViewData["TextContent"] = content;
            ViewData["NotificationTemplateId"] = NotificationTemplateId;
            ViewData["EmailContent"] = notificationContent;
            ViewData["EmailDestination"] = emailDestination;
            ViewData["IsGlobal"] = false;
            ViewData["HasTemplate"] = hasTemplate;
            ViewData["NotificationMasterId"] = formCollection["NotificationMasterId"];
            ViewData["GlobalConfig"] = formCollection["IsGlobalConfig"];
            return PartialView("ManageEmailDetails");
        }

        /// <summary>
        /// This method is used to manages the FTP details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public object ManageFtpDetails(FormCollection formCollection)
        {
            FileContent fileContent = new FileContent();
            UpdateModel(fileContent);

            FtpDestination ftpDestination = new FtpDestination();
            UpdateModel(ftpDestination);

            ftpDestination.UpdatedBy = fileContent.UpdatedBy = UserIdentity.UserId;
            ValidateFtpDestination(ftpDestination);

            string notificationType = formCollection["NotificationType"];
            if (ModelState.IsValid)
            {
                try
                {
                    if (string.IsNullOrEmpty(ftpDestination.NotificationDestinationId))
                    {
                        NotificationDetails notificationDetails = new NotificationDetails { NotificationId = formCollection["NotificationMasterId"] };

                        NotificationDestinationType destinationType;
                        if (Enum.TryParse(notificationType, out destinationType))
                        {
                            switch (destinationType)
                            {
                                case NotificationDestinationType.Ftp:
                                    notificationDetails.NotificationContent = fileContent;
                                    notificationDetails.NotificationDestination = ftpDestination;
                                    break;
                                case NotificationDestinationType.BatchFtp:
                                    notificationDetails.NotificationContent = new BatchFileContent { CommonNotificationContent = fileContent };
                                    notificationDetails.NotificationDestination = new BatchFtpDestination { CommonNotificationDestination = ftpDestination };
                                    break;
                                case NotificationDestinationType.Sftp:
                                    ftpDestination.IsSecured = true;
                                    notificationDetails.NotificationContent = fileContent;
                                    notificationDetails.NotificationDestination = ftpDestination;
                                    break;
                                case NotificationDestinationType.BatchSftp:

                                    notificationDetails.NotificationContent = new BatchFileContent { CommonNotificationContent = fileContent };
                                    notificationDetails.NotificationDestination = new BatchFtpDestination { CommonNotificationDestination = ftpDestination, IsSecured = true };
                                    break;
                            }
                        }

                        notificationDetails.TenantId = ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase) && Convert.ToBoolean(formCollection["IsGlobalConfig"])
                                                        ? string.Empty : this.TenantId;

                        FtpDetails ftpDetails = FillFtpDetails(formCollection);
                        fileContent.CreatedBy = ftpDetails.CreatedBy = notificationDetails.CreatedBy = UserIdentity.UserId;
                        NotificationConfigurationProxy.ConfigureFtpDetails(notificationDetails, ftpDetails);
                        return Content(Sussess);
                    }
                    else
                    {
                        NotificationConfigurationProxy.UpdateFtpDetails(ftpDestination, fileContent);
                        return Content(Sussess);
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterNullOrEmpty);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterNullOrEmpty);
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_AccessDenied);
                }
                catch (NotificationException notificationException)
                {
                    ExceptionService.HandleException(notificationException, _defaultPolicy);
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_UpdateFtpDetails);
                }
            }
            ViewData["IsGlobal"] = false;
            ViewData["FtpDestination"] = ftpDestination;
            ViewData["FileContent"] = fileContent;
            ViewData["NotificationType"] = notificationType;
            ViewData["NotificationMasterId"] = formCollection["NotificationMasterId"];
            ViewData["GlobalConfig"] = formCollection["IsGlobalConfig"];
            return PartialView("ManageFtpDetails");

        }

        /// <summary>
        /// This method is used to manages the system notification details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult ManageSystemNotificationDetails(FormCollection formCollection)
        {
            bool hasTemplate = formCollection["HasTemplate"] != null && formCollection["HasTemplate"].ToString().Equals("on");
            NotificationDetails notificationDetails = new NotificationDetails();
            notificationDetails.NotificationId = formCollection["NotificationMasterId"];

            SystemNotificationContent systemNotificationContent = new SystemNotificationContent();
            systemNotificationContent.RuleSetCode = formCollection["RuleSetCode"];
            systemNotificationContent.CreatedBy = systemNotificationContent.UpdatedBy = UserIdentity.UserId;

            INotificationTemplate template = null;

            if (hasTemplate)
            {
                template = new TextMessageNotificationTemplate();
                template.TemplateId = formCollection["Templates"].ToString();
                template.CreatedBy = template.UpdatedBy = UserIdentity.UserId;
                systemNotificationContent.TemplateId = template.TemplateId;
            }
            else
            {
                systemNotificationContent.TemplateId = string.Empty;
                systemNotificationContent.RuleSetCode = string.Empty;
                systemNotificationContent.Content = new System.Text.StringBuilder(formCollection["Content"]);
            }

            SystemNotificationDestination systemNotificationDestination = new SystemNotificationDestination();
            UpdateModel(systemNotificationDestination);

            systemNotificationDestination.CreatedBy = systemNotificationDestination.UpdatedBy = UserIdentity.UserId;


            if (!string.IsNullOrEmpty(formCollection["NotificationContentId"]))
            {
                systemNotificationContent.NotificationContentId = formCollection["NotificationContentId"];
            }

            systemNotificationContent.Template = template;

            notificationDetails.TenantId = this.TenantId;

            notificationDetails.TenantId = ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase)
                                            && !string.IsNullOrEmpty(formCollection["IsGlobalConfig"])
                                            && Convert.ToBoolean(formCollection["IsGlobalConfig"])
                                            ? string.Empty : this.TenantId;


            NotificationDestinationType destinationType;
            Enum.TryParse(formCollection["NotificationType"], out destinationType);

            notificationDetails.NotificationDestination = systemNotificationDestination;
            notificationDetails.NotificationContent = systemNotificationContent;

            if (destinationType.Equals(NotificationDestinationType.BatchSystemNotification) && string.IsNullOrEmpty(systemNotificationContent.NotificationContentId))
            {
                notificationDetails.NotificationContent = new BatchSystemNotificationContent();
                notificationDetails.NotificationContent.Template = template;
                notificationDetails.NotificationDestination = new BatchSystemNotificationDestination();
            }

            notificationDetails.CreatedBy = UserIdentity.UserId;
            notificationDetails.UpdatedBy = UserIdentity.UserId;
            if (template != null)
            {
                ValidateTemplate(template);
            }
            else
            {
                if (systemNotificationContent.Content == null || string.IsNullOrEmpty(systemNotificationContent.Content.ToString()) || systemNotificationContent.Content.ToString().Equals("<br>"))
                {
                    ModelState.AddModelError("Content", "");
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ContentMandatory);
                }
            }
            try
            {
                if (ModelState.IsValid)
                {
                    NotificationConfigurationProxy.ConfigureSystemNotificationDetails(notificationDetails, systemNotificationDestination, systemNotificationContent);
                    return Content(Sussess);
                }
            }
            catch (ArgumentNullException)
            {
                ModelState.AddModelError("Update", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterNullOrEmpty);
            }
            catch (ArgumentException)
            {
                ModelState.AddModelError("Update", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterNullOrEmpty);
            }
            catch (UnauthorizedAccessException)
            {
                ModelState.AddModelError("Update", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_AccessDenied);
            }
            catch (NotificationException)
            {
                ModelState.AddModelError("Update", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_UpdateSystemNotification);
            }

            ViewData["SystemNotificationContent"] = systemNotificationContent;
            ViewData["SystemNotificationDestination"] = systemNotificationDestination;
            ViewData["NotificationMasterId"] = notificationDetails.NotificationId;
            ViewData["NotificationType"] = formCollection["NotificationType"];
            ViewData["IsGlobal"] = false;
            ViewData["IsBatch"] = destinationType.Equals(NotificationDestinationType.BatchSystemNotification);

            GetTemplateNames(this.TenantId, string.Empty);
            ViewData["NotificationTemplate"] = template != null ? template : new TextMessageNotificationTemplate();
            ViewData["HasTemplate"] = template != null;

            return PartialView("ManageSystemNotificationDetails");
        }

        /// <summary>
        /// This method is used to manages the notification audit details.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageAuditDetails()
        {
            ViewData["MyDetails"] = null;
            GetNotificationTypes();
            GetNotificationNames();
            GetNotificationStatus();
            return View();
        }

        /// <summary>
        /// This method is used to get the my notification details.
        /// </summary>
        /// <returns></returns>
        public ActionResult MyNotificationDetails()
        {
            ViewData["MyDetails"] = "Yes";
            GetNotificationNames();
            GetNotificationStatus();
            return View("ManageAuditDetails");
        }

        /// <summary>
        /// This method is used to manages the audit details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageAuditDetails(FormCollection formCollection, int page = 1, int pageSize = 10)
        {
            return ManageNotificationAuditDetails(formCollection, page, pageSize);
        }

        /// <summary>
        /// This method is used to show my notification details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MyNotificationDetails(FormCollection formCollection, int page = 1, int pageSize = 10)
        {
            return ManageNotificationAuditDetails(formCollection, page, pageSize);
        }

        #region Private Methods

        /// <summary>
        /// This method is used to set the Ftp ViewData
        /// </summary>
        /// <param name="formCollection"></param>
        /// <param name="notificationDestinationType"></param>
        private void SetFtpViewData(FormCollection formCollection, NotificationDestinationType notificationDestinationType)
        {
            FtpDestination ftpDestination = new FtpDestination();
            FileContent fileContent = new FileContent();
            if (!string.IsNullOrEmpty(formCollection["IsNew"]) || !Convert.ToBoolean(formCollection["IsNew"], CultureInfo.InvariantCulture))
            {
                UpdateModel(fileContent);
                UpdateModel(ftpDestination);
            }

            if (notificationDestinationType.Equals(NotificationDestinationType.Sftp) || notificationDestinationType.Equals(NotificationDestinationType.BatchSftp))
            {
                ftpDestination.IsSecured = true;
            }

            ViewData["FtpDestination"] = ftpDestination;
            ViewData["FileContent"] = fileContent;
        }

        /// <summary>
        /// This method is used to set the Email ViewData
        /// </summary>
        /// <param name="formCollection"></param>
        private void SetEmailViewData(FormCollection formCollection)
        {
            EmailDestination emailDestination = new EmailDestination();
            TextMessageContent textMessageContent = new TextMessageContent();
            if (!string.IsNullOrEmpty(formCollection["IsNew"]) || !Convert.ToBoolean(formCollection["IsNew"], CultureInfo.InvariantCulture))
            {
                UpdateModel(textMessageContent);
                UpdateModel(emailDestination);
                ViewData["HasTemplate"] = string.IsNullOrEmpty(formCollection["NotificationTemplateId"]) ? false : true;
                ViewData["TextContent"] = formCollection["TextContent"];
            }

            ViewData["EmailDestination"] = emailDestination;
            ViewData["EmailContent"] = textMessageContent;
        }

        /// <summary>
        /// This method is used to set the SystemNotification ViewData
        /// </summary>
        /// <param name="formCollection"></param>
        private void SetSystemNotificationViewData(FormCollection formCollection)
        {
            SystemNotificationContent systemNotificationContent = new SystemNotificationContent();
            SystemNotificationDestination systemNotificationDestination = new SystemNotificationDestination();

            if (!string.IsNullOrEmpty(formCollection["IsNew"]) || !Convert.ToBoolean(formCollection["IsNew"], CultureInfo.InvariantCulture))
            {
                UpdateModel(systemNotificationContent);
                UpdateModel(systemNotificationDestination);
                systemNotificationContent.Content = new StringBuilder(formCollection["TextContent"]);
            }
            ViewData["SystemNotificationContent"] = systemNotificationContent;
            ViewData["SystemNotificationDestination"] = systemNotificationDestination;
            ViewData["NotificationTemplate"] = new TextMessageNotificationTemplate();
            ViewData["IsGlobal"] = false;
        }

        /// <summary>
        ///This method is used to Manages the notification audit details.
        /// </summary>
        /// <param name="formCollection">The form collection.</param>
        /// <returns></returns>
        private ActionResult ManageNotificationAuditDetails(FormCollection formCollection, int page = 1, int pageSize = 10)
        {
            NotificationAuditSearchCondition notificationAuditSearchCondition = new NotificationAuditSearchCondition();
            TryUpdateModel(notificationAuditSearchCondition);
            notificationAuditSearchCondition.TenantId = this.TenantId;
            notificationAuditSearchCondition.PageNumber = page;
            notificationAuditSearchCondition.PageSize = pageSize;
            notificationAuditSearchCondition.SetTotalCount = true;
            NotificationDestinationType? notificationDestinationType = notificationAuditSearchCondition.DestinationType;
            
            NotificationStatus notificationStatus;
            if (!string.IsNullOrEmpty(formCollection["NotificationStatus"]) && Enum.TryParse(formCollection["NotificationStatus"], out notificationStatus))
            {
                notificationAuditSearchCondition.NotificationStatus = notificationStatus;
            }


            ViewBag.Condition = notificationAuditSearchCondition;
            ViewBag.PageNumber = page;
            ViewBag.PageSize = pageSize;
            ViewBag.SortString = notificationAuditSearchCondition.SortString ?? "NotificationAudit_CreatedOn";
            ViewBag.SortExpression = notificationAuditSearchCondition.SortExpression ?? CelloSaaS.Model.SortExpressionConstants.Decending;
            ViewBag.Condition = notificationAuditSearchCondition;

            if (!string.IsNullOrEmpty(formCollection["BatchNotificationType"]) || notificationAuditSearchCondition.DestinationTypes != null)
            {
                notificationAuditSearchCondition.DestinationType = null;
                notificationAuditSearchCondition.DestinationTypes.Add(NotificationDestinationType.BatchSystemNotification);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    NotificationAuditSearchResult notificationAuditSearchResult = NotificationAuditProxy.SearchNotificationAuditDetails(notificationAuditSearchCondition);
                    ViewData["NotificationAudit"] = notificationAuditSearchResult != null ? notificationAuditSearchResult.Result : null;
                    ViewData["TotalCount"] = notificationAuditSearchResult != null ? notificationAuditSearchResult.TotalCount : 0;
                    notificationAuditSearchCondition.DestinationType = notificationDestinationType;
                    switch (notificationDestinationType)
                    {
                        case NotificationDestinationType.Email:
                        case NotificationDestinationType.BatchEmail:
                            return PartialView("EmailNotificationAudit");

                        case NotificationDestinationType.Ftp:
                        case NotificationDestinationType.BatchFtp:
                        case NotificationDestinationType.Sftp:
                        case NotificationDestinationType.BatchSftp:
                            return PartialView("FtpNotificationAudit");

                        case NotificationDestinationType.SystemNotification:
                        case NotificationDestinationType.BatchSystemNotification:
                            return PartialView("SystemNotificationAudit");
                    }
                }
                catch (ArgumentException)
                {
                    ModelState.AddModelError("NotificationName", "");
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterNullOrEmpty);
                }
                catch (NotificationException)
                {
                    ModelState.AddModelError("NotificationName", "");
                    ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_GetAuditDetails);
                }
            }

            GetNotificationTypes();
            GetNotificationNames();
            GetNotificationStatus();
            return PartialView("ManageNotificationAudit");
        }

        /// <summary>
        /// This Method is used to Fill the email details from the form collection
        /// </summary>
        /// <param name="formCollection">The form collection</param>
        /// <returns>email details</returns>
        private static EmailDetails FillEmailDetails(FormCollection formCollection)
        {
            EmailDetails emailDetails = new EmailDetails();
            emailDetails.Sender = formCollection["SenderAddress"];
            emailDetails.SmtpAddress = formCollection["SmtpAddress"];
            emailDetails.SmtpUserName = formCollection["SmtpUserName"];
            emailDetails.SmtpPassword = formCollection["SmtpPassword"];
            emailDetails.PortNumber = Convert.ToInt32(formCollection["PortNumber"], CultureInfo.InvariantCulture);
            emailDetails.Subject = formCollection["Subject"];
            emailDetails.AttachmentFileFolderPath = formCollection["AttachmentFileFolderPath"];
            emailDetails.AttachmentFiles = string.IsNullOrEmpty(formCollection["AttachmentFiles"]) ? null : formCollection["AttachmentFiles"].Split(',');
            emailDetails.Content = formCollection["TextContent"];

            return emailDetails;
        }

        /// <summary>
        /// This method is used to fill the ftp details
        /// </summary>
        /// <param name="formCollection">The form collection</param>
        /// <returns>Ftp details</returns>
        private static FtpDetails FillFtpDetails(FormCollection formCollection)
        {
            FtpDetails ftpDetails = new FtpDetails();
            ftpDetails.FtpAddress = formCollection["FtpAddress"];
            ftpDetails.FtpUserName = formCollection["UserName"];
            ftpDetails.FtpPassword = formCollection["Password"];
            ftpDetails.FileName = formCollection["FileName"];
            ftpDetails.FilePath = formCollection["FilePath"];
            return ftpDetails;
        }

        /// <summary>
        /// Notifications the details list.
        /// </summary>
        private void NotificationDetailsList()
        {
            try
            {
                List<NotificationDetails> notificationDetails = NotificationConfigurationProxy.GetNotificationMasterDetailsByTenantId(this.TenantId);
                ViewData["IsProductAdmin"] = ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase);
                ViewData["NotificationDetails"] = notificationDetails;
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_AccessDenied);
            }
            catch (NotificationException notificationException)
            {
                ExceptionService.HandleException(notificationException, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
        }

        /// <summary>
        /// Validates the notification details.
        /// </summary>
        /// <param name="notificationDetails">The notification details.</param>
        private void ValidateNotificationDetails(NotificationDetails notificationDetails)
        {
            if (string.IsNullOrEmpty(notificationDetails.NotificationName))
            {
                ModelState.AddModelError("NotificationName", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_NotificationNameMandatory);
            }
        }

        /// <summary>
        /// Validates the email destination.
        /// </summary>
        /// <param name="emailDestination">The email destination.</param>
        private void ValidateEmailDestination(EmailDestination emailDestination)
        {
            if (string.IsNullOrEmpty(emailDestination.SenderAddress))
            {
                ModelState.AddModelError("Sender", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SenderAddressMandatory);
            }
            else if (!IsEmail(emailDestination.SenderAddress))
            {
                ModelState.AddModelError("Sender", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SenderEmailFormat);
            }

            if (string.IsNullOrEmpty(emailDestination.SmtpAddress))
            {
                ModelState.AddModelError("SmtpAddress", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SmtpAddressMandatory);
            }

            if (string.IsNullOrEmpty(emailDestination.SmtpUserName))
            {
                ModelState.AddModelError("SmtpUserName", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SmtpUserNameMandatory);
            }

            if (string.IsNullOrEmpty(emailDestination.SmtpPassword))
            {
                ModelState.AddModelError("SmtpPassword", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SmtpPasswordMandatory);
            }
        }

        /// <summary>
        /// Validates the FTP destination.
        /// </summary>
        /// <param name="ftpDestination">The FTP destination.</param>
        private void ValidateFtpDestination(FtpDestination ftpDestination)
        {
            if (string.IsNullOrEmpty(ftpDestination.FtpAddress))
            {
                ModelState.AddModelError("FtpAddress", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SenderAddressMandatory);
            }

            if (string.IsNullOrEmpty(ftpDestination.UserName))
            {
                ModelState.AddModelError("FtpUserName", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SmtpUserNameMandatory);
            }

            if (string.IsNullOrEmpty(ftpDestination.Password))
            {
                ModelState.AddModelError("FtpPassword", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_SmtpPasswordMandatory);
            }
        }

        /// <summary>
        /// Validates the template.
        /// </summary>
        /// <param name="template">The template.</param>
        private void ValidateTemplate(INotificationTemplate template)
        {
            if (string.IsNullOrEmpty(template.TemplateId))
            {
                ModelState.AddModelError("TemplateName", "");
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_TemplateNameMandatory);
            }
        }

        /// <summary>
        /// Gets the notification types.
        /// </summary>
        private void GetNotificationTypes()
        {
            List<SelectListItem> lstNotificationType = new List<SelectListItem>();
            //lstNotificationType.Add(new SelectListItem { Value = "-1", Text = "-Select-" });
            var enumSource = Enum.GetValues(typeof(CelloSaaS.Notification.Model.NotificationDestinationType));
            foreach (var value in enumSource)
            {
                if (value.ToString().Equals(NotificationDestinationType.Fax.ToString()) || 
                    value.ToString().Equals(NotificationDestinationType.BatchSystemNotification.ToString()) || value.ToString().Equals(NotificationDestinationType.SystemNotification.ToString()))
                {
                    continue;
                }

                lstNotificationType.Add(new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString()
                });
            }

            ViewData["NotificationTypes"] = lstNotificationType;
        }

        /// <summary>
        /// Gets the notification Names.
        /// </summary>
        private void GetNotificationNames()
        {
            List<SelectListItem> lstNotificationType = new List<SelectListItem>();
            lstNotificationType.Add(new SelectListItem { Value = "", Text = "-All-" });

            List<NotificationDetails> notificationDetails = NotificationConfigurationProxy.GetNotificationMasterDetailsByTenantId(this.TenantId);
            foreach (var notificationDetail in notificationDetails)
            {
                lstNotificationType.Add(new SelectListItem
                {
                    Text = notificationDetail.NotificationName,
                    Value = notificationDetail.NotificationId
                });
            }

            ViewData["NotificationNames"] = lstNotificationType;
        }

        private void GetNotificationStatus()
        {
            List<SelectListItem> lstNotificationStatus = new List<SelectListItem>();
            lstNotificationStatus.Add(new SelectListItem { Value = "", Text = "-All-" });
            var enumSource = Enum.GetValues(typeof(CelloSaaS.Notification.Model.NotificationStatus));
            foreach (var value in enumSource)
            {
                if (value.ToString().Equals(NotificationStatus.Open.ToString()) || value.ToString().Equals(NotificationStatus.InProgress.ToString()))
                {
                    continue;
                }

                lstNotificationStatus.Add(new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString()
                });
            }

            ViewData["NotificationStatus"] = lstNotificationStatus;
        }
        /// <summary>
        /// Gets the template names.
        /// </summary>
        /// <param name="tenantId">The tenant id.</param>
        /// <param name="templateId">The template id.</param>
        private void GetTemplateNames(string tenantId, string templateId)
        {
            TextMessageNotificationTemplate template = new TextMessageNotificationTemplate();
            if (!string.IsNullOrEmpty(templateId))
            {
                template.TemplateId = templateId;
            }

            try
            {
                Dictionary<string, string> templateNames = new Dictionary<string, string>();
                Dictionary<string, TemplateDetail> templateDetails = TemplateProxy.GetAllTemplateDetailsByCategory(tenantId, "NotificationTemplate");
                if (templateDetails != null && templateDetails.Count > 0)
                {
                    templateNames = templateDetails.ToDictionary(k => k.Key, v => v.Value.TemplateName);
                }

                ViewData["Templates"] = new SelectList(templateNames, "Key", "Value", template.TemplateId);
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_AccessDenied);
            }
            catch (TemplateException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("ConfigStatusMessageSummary", Resources.NotificationConfigResource.e_FetchTemplate);
            }
        }

        /// <summary>
        /// Determines whether the specified input email is email.
        /// </summary>
        /// <param name="inputEmail">The input email.</param>
        /// <returns>
        /// 	<c>true</c> if the specified input email is email; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z]{1,3}([a-zA-Z0-9_\-\.]+))@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }

        #endregion
    }
}
