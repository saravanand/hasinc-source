using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Library;
using System.Data;
using CelloSaaS.ServiceContracts.LicenseManagement;
using System.Globalization;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.View;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaSApplication.Models;
using CelloSaaS.ServiceContracts.TenantManagement;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for package management.
    /// </summary>
    [HandleError]
    public class PackageController : CelloSaaS.View.CelloController
    {
        #region members

        string TenantId = TenantContext.GetTenantId(new CelloSaaS.Model.LicenseManagement.PackageDetails().EntityIdentifier);

        private const string PolicyName = "GlobalExceptionLogger";
        private const string ASSIGNED = "Assigned";
        private const string ASSIGNABLE = "Assignable";

        #endregion

        #region Package Management

        /// <summary>
        /// This method is used to get package details list.
        /// </summary>
        /// <returns></returns>
        public ActionResult PackageList()
        {
            try
            {
                if (TempData["PackageSuccessMessage"] != null && !string.IsNullOrEmpty(TempData["PackageSuccessMessage"].ToString()))
                {
                    ModelState.AddModelError("PackageSuccessMessage", TempData["PackageSuccessMessage"].ToString());
                }

                //Get package details
                List<LicensePackage> packageDetails = LicenseProxy.GetAllLicensePackage(TenantId);
                ViewData["PackageDetails"] = packageDetails;
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageLoad);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageLoad);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageLoad);
            }
            return View();
        }

        /// <summary>
        /// This method is used to get tenant license Package distribution graph.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult PackageDistribution()
        {
            try
            {
                var immediateChildTenant = TenantRelationProxy.GetAllImmediateChildTenants(this.TenantId);

                if (immediateChildTenant != null && immediateChildTenant.Count > 0)
                {
                    var activeTenants = immediateChildTenant.Values.Where(t => t.ApprovalStatus.Equals(TenantApprovalStatus.APPROVED)).ToList();
                    var tenantLicenses = LicenseProxy.SearchTenantLicenses(new TenantLicenseSearchCondition
                    {
                        TenantIds = activeTenants.Select(x => Guid.Parse(x.TenantCode)).ToArray(),
                    });

                    var lstTenants = new ConcurrentBag<TenantViewModel>();

                    Parallel.ForEach(activeTenants, (item) =>
                    {
                        var tenantId = Guid.Parse(item.TenantCode);
                        var license = tenantLicenses.ContainsKey(tenantId) ? tenantLicenses[tenantId] : null;

                        lstTenants.Add(new TenantViewModel
                        {
                            TenantDetails = item,
                            License = license
                        });
                    });

                    ViewData["TenantDetails"] = lstTenants;
                }
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, PolicyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, PolicyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_Access);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to get add package view.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddPackage()
        {
            try
            {
                ViewData["TenantId"] = TenantId;
                FillAssignablePackage(TenantId, false);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
            }
            catch (UnauthorizedAccessException licenseException)
            {
                ExceptionService.HandleException(licenseException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
            }

            var assignedModules = new List<string>();
            assignedModules.Add("AccessControl");
            assignedModules.Add("Configuration");
            assignedModules.Add("Setting");
            assignedModules.Add("Tenant");
            assignedModules.Add("User");
            assignedModules.Add("Notification");
            assignedModules.Add("Billing");

            var assignedFeatures = new List<string>();
            assignedFeatures.Add("ManageAllRole");
            assignedFeatures.Add("ManageRole");
            assignedFeatures.Add("ManageRolePrivileges");
            assignedFeatures.Add("ManageRoleFeatures");
            assignedFeatures.Add("ManageUserRoles");
            assignedFeatures.Add("TenantAccessFeature");
            assignedFeatures.Add("ManageAudit");
            assignedFeatures.Add("ManageDataViewExtn");
            assignedFeatures.Add("ManageEntityExtn");
            assignedFeatures.Add("ManageEvents");
            assignedFeatures.Add("ManageModuleConfiguration");
            assignedFeatures.Add("ManagePickupList");
            assignedFeatures.Add("ManageRules");
            assignedFeatures.Add("ManageSettingTemplate");
            assignedFeatures.Add("ManageTenantSettingsTemplate");
            assignedFeatures.Add("ManagePackageSettings");
            assignedFeatures.Add("ManageRoleSettings");
            assignedFeatures.Add("ManageTenantSettings");
            assignedFeatures.Add("SelfTenantAdministrator");
            assignedFeatures.Add("ManageAllUser");
            assignedFeatures.Add("ManageUser");
            assignedFeatures.Add("ManageNotification");
            assignedFeatures.Add("ManageInvoice");

            ViewData["SelectedAssignedModules"] = assignedModules;
            ViewData["SelectedAssignedFeatures"] = assignedFeatures;

            return View(new PackageDetails());
        }

        /// <summary>
        /// This method is used to add package details.
        /// </summary>
        /// <param name="formcollection">form collection.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPackage(FormCollection formcollection)
        {
            PackageDetails packageDetails = new PackageDetails();

            TryUpdateModel(packageDetails);

            //Check package Names
            if (string.IsNullOrWhiteSpace(packageDetails.PackageName))
            {
                ModelState.AddModelError("PackageName", Resources.PackageResource.e_PackageName);
            }
            else if (!Util.ValidateIdentifierWithSpace(packageDetails.PackageName))
            {
                ModelState.AddModelError("PackageName", Resources.PackageResource.e_ValidPackageName);
            }

            if (string.IsNullOrWhiteSpace(packageDetails.PackageDescription))
            {
                ModelState.AddModelError("PackageDescription", Resources.PackageResource.e_PackageDescription);
            }

            if (packageDetails.ExpiryDays < 0)
            {
                ModelState.AddModelError("ExpiryDays", Resources.PackageResource.e_TrialPeriodDays);
            }

            if (packageDetails.IsEvaluation && packageDetails.ExpiryDays <= 0)
            {
                ModelState.AddModelError("ExpiryDays", Resources.PackageResource.e_TrialDays);
            }

            List<string> assignedServices = null;
            List<string> assignedModules = new List<string>();
            List<string> assignedFeatures = new List<string>();
            Dictionary<string, double> assignedUsages = new Dictionary<string, double>();
            //Check services.
            FillPackageDetails("assigned", formcollection, assignedServices, assignedModules, assignedFeatures, assignedUsages);
            ViewData["SelectedAssignedServices"] = assignedServices;
            ViewData["SelectedAssignedModules"] = assignedModules;
            ViewData["SelectedAssignedFeatures"] = assignedFeatures;
            ViewData["SelectedAssignedUsages"] = assignedUsages;

            List<string> assignableServices = null;
            List<string> assignableModules = new List<string>();
            List<string> assignableFeatures = new List<string>();
            Dictionary<string, double> assignableUsages = new Dictionary<string, double>();

            FillPackageDetails("assignable", formcollection, assignableServices, assignableModules, assignableFeatures, assignableUsages);
            ViewData["SelectedAssignableServices"] = assignableServices;
            ViewData["SelectedAssignableModules"] = assignableModules;
            ViewData["SelectedAssignableFeatures"] = assignableFeatures;
            ViewData["SelectedAssignableUsages"] = assignableUsages;

            if (ModelState.IsValid && assignedUsages != null && assignedUsages.Count > 0)
            {
                ValidateUsage(this.TenantId, assignedUsages, assignableUsages);
            }

            if (assignedModules != null && assignedModules.Contains("Package")
                && (assignableModules == null || assignableModules.Count == 0))
            {
                ModelState.AddModelError("Error", Resources.PackageResource.e_ChooseOneModule);
            }

            if (ModelState.IsValid)
            {
                string successMessage = string.Empty;

                packageDetails.TenantId = TenantId;
                packageDetails.Status = true;
                packageDetails.PackageServiceDetails = GetPackageServiceDetails(assignedServices);
                packageDetails.PacakageModuleDetails = GetPackageModuleDetails(assignedModules, assignableModules);
                packageDetails.PacakgeFeatureDetails = GetPackageFeatureDetails(assignedFeatures, assignableFeatures);
                FillUsageDetails(packageDetails, assignedUsages, assignableUsages);

                try
                {
                    if (string.IsNullOrEmpty(packageDetails.PackageId))
                    {
                        packageDetails.PackageId = LicenseProxy.InsertPackage(packageDetails);
                        successMessage = Resources.PackageResource.s_PackageInsert;

                        // add to self registration package pickup list by default if added by product admin
                        if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                        {
                            CelloSaaS.ServiceProxies.Configuration.PickupListProxy.AddPickupListValue(new CelloSaaS.Model.Configuration.PickupListValue
                            {
                                TenantCode = packageDetails.TenantId,
                                PickupListId = CelloSaaS.Model.PickupListConstants.SelfRegistrationPackagePickupList,
                                ItemId = packageDetails.PackageId,
                                ItemName = packageDetails.PackageName,
                                Description = packageDetails.PackageDescription,
                                CreatedBy = UserIdentity.UserId,
                                CreatedOn = DateTime.Now,
                                Status = true
                            });
                        }
                    }
                    else
                    {
                        packageDetails.PackageId = LicenseProxy.UpdatePackage(packageDetails);
                        successMessage = Resources.PackageResource.s_PackageUpdate;
                    }

                    if (string.IsNullOrEmpty(packageDetails.PackageId))
                    {
                        ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageInsert);
                    }
                    else
                    {
                        TempData["PackageSuccessMessage"] = successMessage;
                        return RedirectToAction("PackageList", "Package");
                    }
                }
                catch (LicenseException licenseException)
                {
                    ExceptionService.HandleException(licenseException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageInsert);
                }
                catch (UnauthorizedAccessException licenseException)
                {
                    ExceptionService.HandleException(licenseException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageInsert);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageInsert);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageInsert);
                }
                catch (DataException dataException)
                {
                    ExceptionService.HandleException(dataException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_DuplicateName);
                }
                catch (Exception exception)
                {
                    ExceptionService.HandleException(exception, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", exception.Message);
                }
            }

            FillAssignablePackage(TenantId, true);

            return View(packageDetails);
        }

        /// <summary>
        /// This method is used to get edit package view.
        /// </summary>
        /// <param name="packageId">package identifier.</param>
        /// <returns></returns>
        public ActionResult EditPackage(string packageId)
        {
            Guid tmp;
            PackageDetails packageDetails = null;

            if (string.IsNullOrEmpty(packageId) || !Guid.TryParse(packageId, out tmp))
            {
                return HttpNotFound("Package not found!");
            }
            else
            {
                try
                {
                    packageDetails = LicenseProxy.GetPackageDetailsByPackageId(packageId);
                    if (packageDetails == null)
                    {
                        return HttpNotFound("Package not found!");
                    }
                    FillAssignablePackage(TenantId, true);

                   
                    if (packageDetails != null && !string.IsNullOrEmpty(packageDetails.TenantId)
                        && !AccessControlProxy.CheckTenantAccessWithTenantScope(UserIdentity.UserId, PrivilegeConstants.UpdatePackage, packageDetails.TenantId, new PackageDetails().EntityIdentifier, CelloSaaS.Model.FetchType.View, UserIdentity.TenantID))
                    {
                        packageDetails = null;
                        ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_AccessDenied);
                    }
                    else if (packageDetails != null)
                    {
                        AssignPackageDetailsToViewData(packageDetails);
                    }
                    else
                    {
                        ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageLoad);
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
                }
                catch (LicenseException licenseException)
                {
                    ExceptionService.HandleException(licenseException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    ExceptionService.HandleException(unauthorizedAccessException, PolicyName);
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_AccessDenied);
                }
            }

            return View("AddPackage", packageDetails);
        }

        /// <summary>
        /// This method is used to fill the usage details.
        /// </summary>
        /// <param name="packageDetails">package details.</param>
        /// <param name="assignedUsages">collection of assigned usages.</param>
        /// <param name="assignableUsages">collection of assignable usage.</param>
        private void FillUsageDetails(PackageDetails packageDetails, Dictionary<string, double> assignedUsages, Dictionary<string, double> assignableUsages)
        {
            var tenantInfo = CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantInfo(this.TenantId);
            bool isIsvOrReseller = tenantInfo.Types != null && tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);

            if (isIsvOrReseller)
            {
                packageDetails.PackageUsageDetails = GetPackageUsageDetails(assignedUsages, assignableUsages);
            }
            else
            {
                TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);
                if (tenantLicense == null)
                {
                    return;
                }

                PackageDetails availablePackage = LicenseProxy.GetPackageDetailsByPackageId(tenantLicense.PackageId);
                if (availablePackage == null || availablePackage.PackageUsageDetails == null)
                {
                    return;
                }
                
                Dictionary<string, PackageUsage> packageUsageDetails = new Dictionary<string, PackageUsage>();
                foreach (var packageUsage in availablePackage.PackageUsageDetails)
                {
                    if (packageDetails.PacakageModuleDetails.ContainsKey(packageUsage.Value.ModuleId))
                    {
                        packageUsageDetails.Add(packageUsage.Key, packageUsage.Value);
                    }
                }

                packageDetails.PackageUsageDetails = packageUsageDetails.Count > 0 ? packageUsageDetails : null;
            }
        }

        /// <summary>
        /// This method is used to assign package details to view data.
        /// </summary>
        /// <param name="packageDetails">package details.</param>
        private void AssignPackageDetailsToViewData(PackageDetails packageDetails)
        {
            if (packageDetails.PackageServiceDetails != null && packageDetails.PackageServiceDetails.Count > 0)
            {
                ViewData["SelectedAssignedServices"] = packageDetails.PackageServiceDetails.Keys.ToList();
            }

            if (packageDetails.PacakageModuleDetails != null && packageDetails.PacakageModuleDetails.Count > 0)
            {
                ViewData["SelectedAssignedModules"] = packageDetails.PacakageModuleDetails.Keys.ToList();
            }

            if (packageDetails.PacakgeFeatureDetails != null && packageDetails.PacakgeFeatureDetails.Count > 0)
            {
                ViewData["SelectedAssignedFeatures"] = packageDetails.PacakgeFeatureDetails.Keys.ToList();
            }

            if (packageDetails.PackageUsageDetails != null && packageDetails.PackageUsageDetails.Count > 0)
            {
                ViewData["SelectedAssignedUsages"] = GetUsageDetails(packageDetails.PackageUsageDetails);
            }

            PackageDetails assignablePackageDetails = LicenseProxy.GetAssignablePackageDetailsByPackageId(packageDetails.PackageId);

            if (assignablePackageDetails != null)
            {
                if (assignablePackageDetails.PackageServiceDetails != null && assignablePackageDetails.PackageServiceDetails.Count > 0)
                {
                    ViewData["SelectedAssignableServices"] = assignablePackageDetails.PackageServiceDetails.Keys.ToList();
                }

                if (assignablePackageDetails.PacakageModuleDetails != null && assignablePackageDetails.PacakageModuleDetails.Count > 0)
                {
                    ViewData["SelectedAssignableModules"] = assignablePackageDetails.PacakageModuleDetails.Keys.ToList();
                }

                if (assignablePackageDetails.PacakgeFeatureDetails != null && assignablePackageDetails.PacakgeFeatureDetails.Count > 0)
                {
                    ViewData["SelectedAssignableFeatures"] = assignablePackageDetails.PacakgeFeatureDetails.Keys.ToList();
                }

                if (assignablePackageDetails.PackageUsageDetails != null && assignablePackageDetails.PackageUsageDetails.Count > 0)
                {
                    ViewData["SelectedAssignableUsages"] = GetUsageDetails(assignablePackageDetails.PackageUsageDetails);
                }
            }
            else
            {
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_PackageLoad);
            }
        }

        /// <summary>
        /// This method is used to get the usage details.
        /// </summary>
        /// <param name="packageUsageDetails">package usage details.</param>
        /// <returns></returns>
        private static Dictionary<string, double> GetUsageDetails(Dictionary<string, PackageUsage> packageUsageDetails)
        {
            if (packageUsageDetails == null || packageUsageDetails.Count < 1)
            {
                return null;
            }

            Dictionary<string, double> usageDetails = new Dictionary<string, double>();
            packageUsageDetails.Values.ToList().ForEach(x => usageDetails.Add(x.UsageCode, x.MaximumCapacity != null ? (double)x.MaximumCapacity : 0));
            return usageDetails;
        }

        /// <summary>
        /// This method is used to fill the assignable package.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        private void FillAssignablePackage(string tenantId, bool isEdit)
        {
            TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(tenantId);
            if (tenantLicense != null)
            {
                PackageDetails availablePackage = LicenseProxy.GetAssignablePackageDetailsByPackageId(tenantLicense.PackageId);

                // Get service list
                Dictionary<string, PackageService> availableServiceDetails = availablePackage.PackageServiceDetails;
                if (availableServiceDetails != null && availableServiceDetails.Count > 0)
                {
                    ViewData["ServiceDetails"] = availableServiceDetails.Values.ToList();
                    Dictionary<string, Dictionary<string, Module>> moduleByService = LicenseProxy.GetModuleByServiceIds(availableServiceDetails.Keys.ToArray());
                    if (moduleByService != null && moduleByService.Count > 0)
                    {
                        ViewData["ModuleDetailsByService"] = moduleByService;
                    }
                }

                Dictionary<string, PackageModule> availableModuleDetails = availablePackage.PacakageModuleDetails;
                if (availableModuleDetails != null && availableModuleDetails.Count > 0)
                {
                    ViewData["ModuleDetails"] = availableModuleDetails.OrderBy(x => x.Value.ModuleName).ToDictionary(x => x.Key, v => v.Value);
                    Dictionary<string, Dictionary<string, Feature>> featureDetails = LicenseProxy.GetFeatureByModuleIds(availableModuleDetails.Keys.ToArray());
                    ViewData["FeatureDetails"] = featureDetails;

                    Dictionary<string, Dictionary<string, Usage>> usageDetails = LicenseProxy.GetUsageByModuleIds(availableModuleDetails.Keys.ToArray());
                    ViewData["UsageDetails"] = usageDetails;

                    if (!isEdit && usageDetails != null && usageDetails.Count > 0 && availablePackage.PackageUsageDetails != null)
                    {
                        FillMaximumusage(availablePackage, usageDetails);
                    }
                }



                ViewData["AvailableFeatureDetails"] = availablePackage.PacakgeFeatureDetails;
                ViewData["AvailableUsageDetails"] = availablePackage.PackageUsageDetails;
            }
        }

        /// <summary>
        /// This method is used to fill the maximum usage.
        /// </summary>
        /// <param name="availablePackage">available package.</param>
        /// <param name="usageDetails">usage details.</param>
        private static void FillMaximumusage(PackageDetails availablePackage, Dictionary<string, Dictionary<string, Usage>> usageDetails)
        {
            foreach (Dictionary<string, Usage> usageDetail in usageDetails.Values)
            {
                if (usageDetail == null)
                {
                    continue;
                }

                foreach (var usage in usageDetail)
                {
                    if (!availablePackage.PackageUsageDetails.ContainsKey(usage.Key) || usage.Value == null)
                    {
                        continue;
                    }
                    usage.Value.MaximumCapacityUsage = availablePackage.PackageUsageDetails[usage.Key].MaximumCapacity;
                }
            }
        }

        /// <summary>
        /// This method is used to validate the usage.
        /// </summary>
        /// <param name="tenantId">tenant identifier</param>
        /// <param name="assignedUsages">collection of assigned usage</param>
        /// <param name="assignableUsages">collection of assignable usage</param>
        private void ValidateUsage(string tenantId, Dictionary<string, double> assignedUsages, Dictionary<string, double> assignableUsages)
        {
            TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(tenantId);
            Dictionary<string, PackageUsage> availablePackageUsage = null;

            if (tenantLicense != null)
            {
                PackageDetails availablePackage = LicenseProxy.GetAssignablePackageDetailsByPackageId(tenantLicense.PackageId);
                availablePackageUsage = availablePackage.PackageUsageDetails;
            }

            foreach (var assignedUsage in assignedUsages)
            {
                if (assignedUsage.Value < 0)
                {
                    ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.i_Assigned + assignedUsage.Key + Resources.PackageResource.e_NegativeInteger);
                }

                if (assignableUsages != null && assignableUsages.ContainsKey(assignedUsage.Key))
                {
                    if (assignableUsages[assignedUsage.Key] < 0)
                    {
                        ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.i_Assignable + assignedUsage.Key + Resources.PackageResource.e_NegativeInteger);
                    }
                    else if (assignedUsage.Value > 0 && (assignableUsages[assignedUsage.Key] == 0 || assignableUsages[assignedUsage.Key] > assignedUsage.Value))
                    {
                        ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.i_Assignable + assignedUsage.Key + Resources.PackageResource.e_AssignedLessAssignable + assignedUsage.Key);
                    }
                }

                if (availablePackageUsage != null && availablePackageUsage.ContainsKey(assignedUsage.Key))
                {
                    if ((availablePackageUsage[assignedUsage.Key].MaximumCapacity != 0 && availablePackageUsage[assignedUsage.Key].MaximumCapacity < assignedUsage.Value) || (availablePackageUsage[assignedUsage.Key].MaximumCapacity > 0 && assignedUsage.Value == 0))
                    {
                        ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_UsageLimit + assignedUsage.Key);
                    }
                }
            }
        }

        /// <summary>
        /// This method is used to fill the package details.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="formcollection">The formcollection.</param>
        /// <param name="services">list of services.</param>
        /// <param name="modules">list of modules.</param>
        /// <param name="features">list of features.</param>
        private void FillPackageDetails(string type, FormCollection formcollection, List<string> services, List<string> modules, List<string> features, Dictionary<string, double> usages)
        {
            string displayType = (type.Equals("assigned") ? ASSIGNED : ASSIGNABLE);
            if (formcollection["service"] != null && !string.IsNullOrEmpty(formcollection["service"]))
            {
                services = formcollection["service"].Split(',').ToList();

                foreach (string serviceCode in services)
                {
                    if (!string.IsNullOrEmpty(formcollection[type + "_module_" + serviceCode]))
                    {
                        string[] moduleIds = formcollection[type + "_module_" + serviceCode].Split(',');
                        modules.AddRange(moduleIds.ToList());
                        FillFeaturesAndUsages(type, formcollection, features, usages, displayType, moduleIds);
                    }
                    else
                    {
                        ModelState.AddModelError("PackageErrorMessage", string.Format(CultureInfo.InvariantCulture, Resources.PackageResource.e_SelectModulleIn, serviceCode + "(" + displayType + ")"));
                    }
                }
            }

            if (!string.IsNullOrEmpty(formcollection[type + "_module_other"]))
            {
                string[] moduleIds = formcollection[type + "_module_other"].Split(',');
                modules.AddRange(moduleIds.ToList());
                FillFeaturesAndUsages(type, formcollection, features, usages, displayType, moduleIds);
            }

            if ((modules == null || modules.Count == 0) && displayType == ASSIGNED)
            {
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_SelectModule + "(" + displayType + ")");
            }
        }

        /// <summary>
        /// This method is used to fill the features and usages.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="formcollection">The formcollection.</param>
        /// <param name="features">list of features.</param>
        /// <param name="assignableUsages">collection assignable usages.</param>
        /// <param name="displayType">The display type.</param>
        /// <param name="moduleIds">collection of module identifiers.</param>
        private void FillFeaturesAndUsages(string type, FormCollection formcollection, List<string> features, Dictionary<string, double> assignableUsages, string displayType, string[] moduleIds)
        {
            foreach (string moduleCode in moduleIds)
            {
                if (formcollection[type + "_feature_" + moduleCode] != null && !string.IsNullOrEmpty(formcollection[type + "_feature_" + moduleCode]))
                {
                    features.AddRange(formcollection[type + "_feature_" + moduleCode].Split(',').ToList());
                }
                else
                {
                    ModelState.AddModelError("PackageErrorMessage", string.Format(CultureInfo.InvariantCulture, Resources.PackageResource.e_SelectFeatureIn, moduleCode + "(" + displayType + ")"));
                }

                if (formcollection[type + "_usage_" + moduleCode] != null && !string.IsNullOrEmpty(formcollection[type + "_usage_" + moduleCode]))
                {
                    List<string> usageCodes = formcollection[type + "_usage_" + moduleCode].Split(',').ToList();
                    usageCodes.ForEach(x =>
                    {
                        double amt = 0;
                        if (formcollection[type + "_usage_" + moduleCode + "_" + x] != null)
                        {
                            if (!Double.TryParse(formcollection[type + "_usage_" + moduleCode + "_" + x], out amt))
                            {
                                ModelState.AddModelError(type + "_usage_" + moduleCode + "_" + x, Resources.PackageResource.e_ValidInt + " " + type + " " + x);
                            }
                        }
                        assignableUsages.Add(x, amt);
                    });
                }
            }
        }

        /// <summary>
        /// This method is used to get module details.
        /// </summary>
        /// <param name="assignedModules">list of assigned modules.</param>
        /// <param name="assignableModules">list of assignable modules.</param>
        /// <returns></returns>
        private static Dictionary<string, PackageModule> GetPackageModuleDetails(List<string> assignedModules, List<string> assignableModules)
        {
            Dictionary<string, PackageModule> packageModules = new Dictionary<string, PackageModule>();
            if (assignedModules != null && assignedModules.Count > 0)
            {
                foreach (string moduleCode in assignedModules)
                {
                    PackageModule packageModule = new PackageModule { ModuleCode = moduleCode, IsPossessed = true };

                    packageModules.Add(moduleCode, packageModule);
                }
            }
            if (assignableModules != null && assignableModules.Count > 0)
            {
                foreach (string moduleCode in assignableModules)
                {
                    if (packageModules.ContainsKey(moduleCode))
                    {
                        packageModules[moduleCode].IsAssignable = true;
                    }
                    else
                    {
                        PackageModule packageModule = new PackageModule { ModuleCode = moduleCode, IsAssignable = true };
                        packageModules.Add(moduleCode, packageModule);
                    }
                }
            }
            return packageModules;
        }

        /// <summary>
        /// This method is used to get package service details.
        /// </summary>
        /// <param name="services">list of services.</param>
        /// <returns></returns>
        private static Dictionary<string, PackageService> GetPackageServiceDetails(List<string> services)
        {
            if (services != null && services.Count > 0)
            {
                Dictionary<string, PackageService> packageServices = new Dictionary<string, PackageService>();
                foreach (string serviceCode in services)
                {
                    PackageService packageService = new PackageService { ServiceCode = serviceCode, IsPossessed = true };

                    packageServices.Add(serviceCode, packageService);
                }
                return packageServices;
            }
            return null;
        }

        /// <summary>
        /// This method is used to get package feature details.
        /// </summary>
        /// <param name="assignedFeatures">list of assigned features.</param>
        /// <param name="assignableFeatures">list of assignable features.</param>
        /// <returns></returns>
        private static Dictionary<string, PackageFeature> GetPackageFeatureDetails(List<string> assignedFeatures, List<string> assignableFeatures)
        {
            Dictionary<string, PackageFeature> packageFeatures = new Dictionary<string, PackageFeature>();
            if (assignedFeatures != null && assignedFeatures.Count > 0)
            {
                foreach (string featureCode in assignedFeatures)
                {
                    PackageFeature packageFeature = new PackageFeature { FeatureId = featureCode, IsPossessed = true };

                    packageFeatures.Add(featureCode, packageFeature);
                }
            }
            if (assignableFeatures != null && assignableFeatures.Count > 0)
            {
                foreach (string featureCode in assignableFeatures)
                {
                    if (packageFeatures.ContainsKey(featureCode))
                    {
                        packageFeatures[featureCode].IsAssignable = true;
                    }
                    else
                    {
                        PackageFeature packageFeature = new PackageFeature { FeatureId = featureCode, IsAssignable = true };
                        packageFeatures.Add(featureCode, packageFeature);
                    }
                }
            }
            return packageFeatures;
        }

        /// <summary>
        /// This method is used to get package usage details.
        /// </summary>
        /// <param name="assignedUsage">collection of assigned usages.</param>
        /// <param name="assignableUsage">collection of assignable usage.</param>
        /// <returns></returns>
        private static Dictionary<string, PackageUsage> GetPackageUsageDetails(Dictionary<string, double> assignedUsage, Dictionary<string, double> assignableUsage)
        {
            Dictionary<string, PackageUsage> packageUsages = new Dictionary<string, PackageUsage>();

            if (assignedUsage != null && assignedUsage.Count > 0)
            {
                foreach (KeyValuePair<string, double> usage in assignedUsage)
                {
                    PackageUsage packageUsage = new PackageUsage();
                    packageUsage.UsageCode = usage.Key;
                    packageUsage.IsPossessed = true;
                    if (usage.Value > 0)
                    {
                        packageUsage.MaximumCapacity = usage.Value;
                    }

                    packageUsages.Add("assignedUsage_" + usage.Key, packageUsage);
                }
            }

            if (assignableUsage != null && assignableUsage.Count > 0)
            {
                foreach (KeyValuePair<string, double> usage in assignableUsage)
                {
                    PackageUsage packageUsage = new PackageUsage();
                    packageUsage.UsageCode = usage.Key;
                    packageUsage.IsAssignable = true;
                    if (usage.Value > 0)
                    {
                        packageUsage.MaximumCapacity = usage.Value;
                    }

                    packageUsages.Add("assignableUsage_" + usage.Key, packageUsage);
                }
            }

            return packageUsages;
        }

        #endregion

        #region Package Management New

        /// <summary>
        /// This method is used to manage the packages based on the given package identifier.
        /// </summary>
        /// <param name="packageId">package identifier.</param>
        /// <returns></returns>
        public ActionResult ManagePackage(string packageId)
        {
            PackageDetails model = new PackageDetails();
            try
            {
                ViewData["TenantId"] = TenantId;

                if (string.IsNullOrEmpty(packageId))
                {
                    FillAssignablePackage(TenantId, false);
                }
                else
                {
                    FillAssignablePackage(TenantId, true);
                    model = LicenseProxy.GetPackageDetailsByPackageId(packageId);
                    AssignPackageDetailsToViewData(model);
                }
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
            }
            catch (UnauthorizedAccessException licenseException)
            {
                ExceptionService.HandleException(licenseException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, PolicyName);
                ModelState.AddModelError("PackageErrorMessage", Resources.PackageResource.e_ModuleLoad);
            }

            return View(model);
        }

        /// <summary>
        /// This method is used to manage the packages.
        /// </summary>
        /// <param name="packageDetails">package details.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManagePackage(PackageDetails packageDetails)
        {
            return View();
        }

        #endregion

        #region Package Price Plan Management

        /// <summary>
        /// This method is used to manage the price plan based on the given package identifier
        /// </summary>
        /// <param name="packageId">package identifier</param>
        /// <returns></returns>
        public ActionResult PricePlanList(Guid? packageId)
        {
            Dictionary<Guid, PricePlan> pricePlans = null;

            try
            {
                if (!packageId.HasValue)
                {
                    ModelState.AddModelError("Error", Resources.PackageResource.e_InvalidPackageId);
                }
                else
                {
                    var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(packageId.Value.ToString());

                    if (packageDetails == null)
                    {
                        ModelState.AddModelError("Error", Resources.PackageResource.e_PackageDetails);
                    }
                    else
                    {
                        ViewData["packageDetails"] = packageDetails;

                        pricePlans = BillingPlanProxy.GetPricePlansByPackageId(packageId.Value, Guid.Parse(this.TenantId));
                    }
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(pricePlans);
        }

        /// <summary>
        /// This method is used to manage the price plan list based on the given price plan and package identifier.
        /// </summary>
        /// <param name="pricePlanId">price plan identifier.</param>
        /// <param name="packageId">package identifier.</param>
        /// <returns></returns>
        public ActionResult ManagePricePlan(Guid? pricePlanId, Guid? packageId)
        {
            PricePlan pricePlan = null;

            try
            {
                if (!packageId.HasValue)
                {
                    ModelState.AddModelError("Error", Resources.PackageResource.e_InvalidPackageId);
                }
                else
                {
                    var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(packageId.Value.ToString());

                    if (packageDetails == null)
                    {
                        ModelState.AddModelError("Error", Resources.PackageResource.e_PackageDetails);
                    }
                    else
                    {
                        ViewData["packageDetails"] = packageDetails;

                        if (!pricePlanId.HasValue)
                        {
                            pricePlan = new PricePlan();
                            pricePlan.PackageId = packageId.Value;
                        }
                        else
                        {
                            pricePlan = BillingPlanProxy.GetPricePlan(pricePlanId.Value, Guid.Parse(this.TenantId));
                        }
                    }
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("PlanLineItemsGrid", pricePlan.LineItems);
            }

            return View(pricePlan);
        }

        /// <summary>
        /// This method is used to manage the price plan.
        /// </summary>
        /// <param name="forms">forms.</param>
        /// <param name="packageId">package identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManagePricePlan(FormCollection forms, Guid packageId)
        {
            var pricePlan = new PricePlan();

            TryUpdateModel(pricePlan, "PricePlan");

            ModelState.Clear();
            ValidatePricePlan(pricePlan);

            try
            {
                var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(packageId.ToString());

                if (packageDetails == null)
                {
                    ModelState.AddModelError("Error", Resources.PackageResource.e_PackageDetails);
                }
                else
                {
                    ViewData["packageDetails"] = packageDetails;

                    if (ModelState.IsValid)
                    {
                        var tenantId = Guid.Parse(this.TenantId);
                        pricePlan.TenantId = tenantId;

                        if (pricePlan.Id != Guid.Empty)
                        {
                            var originalPricePlan = BillingPlanProxy.GetPricePlan(pricePlan.Id, tenantId);

                            if (originalPricePlan != null)
                            {
                                originalPricePlan.Name = pricePlan.Name;
                                originalPricePlan.Description = pricePlan.Description;
                                originalPricePlan.BillFrequency = pricePlan.BillFrequency;
                                originalPricePlan.Price = pricePlan.Price;
                                originalPricePlan.SkipCalculation = pricePlan.SkipCalculation;

                                if (originalPricePlan.SkipCalculation)
                                {
                                    originalPricePlan.LineItems = null;
                                }

                                BillingPlanProxy.UpdatePricePlan(originalPricePlan);
                                TempData["Success"] = string.Format(Resources.PackageResource.s_UpdatePricePlan, pricePlan.Name);
                            }
                            else
                            {
                                TempData["Error"] = Resources.PackageResource.e_PricePlanNotFound;
                            }
                        }
                        else
                        {
                            BillingPlanProxy.AddPricePlan(pricePlan);
                            TempData["Success"] = string.Format(Resources.PackageResource.s_AddPricePlan, pricePlan.Name);
                        }

                        return RedirectToAction("PricePlanList", new { packageId = packageId });
                    }
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (DuplicatePricePlanException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(pricePlan);
        }

        /// <summary>
        /// This method is used to validate the price plan.
        /// </summary>
        /// <param name="pricePlan">price plan details</param>
        private void ValidatePricePlan(PricePlan pricePlan)
        {
            if (string.IsNullOrWhiteSpace(pricePlan.Name))
            {
                ModelState.AddModelError("PricePlan.Name", Resources.PackageResource.e_planname);
            }

            if (pricePlan.Price < 0)
            {
                ModelState.AddModelError("PricePlan.Price", Resources.PackageResource.e_PlanPrice);
            }

            if (pricePlan.LineItems != null)
            {
                foreach (var lineitem in pricePlan.LineItems)
                {
                    ValidatePricePlanLineItem(lineitem);
                }
            }
        }

        /// <summary>
        /// This method is used to validate the price plan line items
        /// </summary>
        /// <param name="lineitem">line items details</param>
        private void ValidatePricePlanLineItem(PricePlanLineItem lineitem)
        {
            if (lineitem == null)
            {
                ModelState.AddModelError("PricePlan.LineItem", Resources.PackageResource.e_LineItem);
                return;
            }

            if (string.IsNullOrWhiteSpace(lineitem.Name))
            {
                ModelState.AddModelError("PricePlan.LineItem.Name", Resources.PackageResource.e_lineItemName);
            }

            if (lineitem.PriceTable != null)
            {
                if (string.IsNullOrEmpty(lineitem.PriceTable.SlabMeterVariable))
                {
                    ModelState.AddModelError("PricePlan.LineItem.PriceTable.SlabMeterVariable", Resources.PackageResource.e_SlabUsagevariable);
                }

                if (lineitem.PriceTable.CalculationType == BillingConstants.CalculationTypeConstants.StepPricing
                    && string.IsNullOrEmpty(lineitem.PriceTable.ActedOnMeterVariable))
                {
                    ModelState.AddModelError("PricePlan.LineItem.PriceTable.ActedOnMeterVariable", Resources.PackageResource.e_ActedonUsage);
                }

                if (lineitem.PriceTable.PriceSlabs != null)
                {
                    foreach (var ps in lineitem.PriceTable.PriceSlabs)
                    {
                        if (ps.StartValue == ps.EndValue)
                        {
                            ModelState.AddModelError("PricePlan.LineItem.PriceTable.PriceSlabs.StartValue", Resources.PackageResource.e_SlabValueEqual);
                        }

                        if (ps.StartValue > ps.EndValue)
                        {
                            ModelState.AddModelError("PricePlan.LineItem.PriceTable.PriceSlabs.StartValue", Resources.PackageResource.e_SlabStartValueGreater);
                        }

                        // show one error per time
                        if (!ModelState.IsValid)
                            return;
                    }
                }
            }
        }

        /// <summary>
        /// This method is used to manage plan line items based on the line item identifier and price plan identifier
        /// </summary>
        /// <param name="lineItemId">line item identifier</param>
        /// <param name="pricePlanId">price plan identifier</param>
        /// <returns></returns>
        public ActionResult ManagePlanLineItem(Guid? lineItemId, Guid? pricePlanId)
        {
            var tenantId = Guid.Parse(this.TenantId);
            PricePlanLineItem lineItem = null;

            if (!pricePlanId.HasValue)
            {
                ModelState.AddModelError("Error", Resources.PackageResource.e_pricePlan);
            }

            try
            {
                var pricePlan = BillingPlanProxy.GetPricePlan(pricePlanId.Value, tenantId);

                if (pricePlan != null && pricePlan.LineItems != null)
                {
                    var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(pricePlan.PackageId.ToString());
                    ViewData["packageDetails"] = packageDetails;

                    if (lineItemId.HasValue && pricePlan.LineItems.Any(x => x.Id == lineItemId.Value))
                    {
                        lineItem = pricePlan.LineItems.Single(x => x.Id == lineItemId);
                    }
                    else
                    {
                        lineItem = new PricePlanLineItem();
                        lineItem.PricePlanId = pricePlanId.Value;
                        lineItem.PriceTable = new PriceTable();
                        lineItem.PriceTable.PriceSlabs = new List<PriceSlab>();
                    }
                }
                else
                {
                    ModelState.AddModelError("Error", Resources.PackageResource.e_LineItemNotAvailable);
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView(lineItem);
        }

        /// <summary>
        /// This method is used to manage plan line items.
        /// </summary>
        /// <param name="lineItem">line items</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ManagePlanLineItem(PricePlanLineItem lineItem)
        {
            var tenantId = Guid.Parse(this.TenantId);
            ModelState.Clear();
            ValidatePricePlanLineItem(lineItem);

            if (lineItem.PricePlanId == Guid.Empty)
            {
                return Json(new { Error = Resources.PackageResource.e_PricePlanInvalid });
            }

            try
            {
                var pricePlan = BillingPlanProxy.GetPricePlan(lineItem.PricePlanId, tenantId);

                if (pricePlan == null)
                {
                    return Json(new { Error = Resources.PackageResource.e_PricePlanInvalid });
                }

                if (pricePlan.LineItems == null)
                    pricePlan.LineItems = new List<PricePlanLineItem>();

                if (ModelState.IsValid)
                {
                    string message = string.Format(CultureInfo.InvariantCulture, Resources.PackageResource.s_AddlineItem, lineItem.Name);

                    if (lineItem.Id != Guid.Empty && pricePlan.LineItems.Any(x => x.Id == lineItem.Id))
                    {
                        // update existing line items
                        pricePlan.LineItems.Remove(pricePlan.LineItems.Single(x => x.Id == lineItem.Id));
                        message = string.Format(CultureInfo.InvariantCulture, Resources.PackageResource.s_UpdateLineItem, lineItem.Name);
                    }

                    pricePlan.LineItems.Add(lineItem);
                    BillingPlanProxy.UpdatePricePlan(pricePlan);
                    return Json(new { Success = message });
                }
                else
                {
                    return Json(new { Error = ModelState.ErrorString() });
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to delete the plan line item identifier.
        /// </summary>
        /// <param name="lineItemId">line item identifier.</param>
        /// <param name="pricePlanId">price plan identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeletePlanLineItem(Guid? lineItemId, Guid? pricePlanId)
        {
            var tenantId = Guid.Parse(this.TenantId);

            if (lineItemId.HasValue && pricePlanId.HasValue)
            {
                try
                {
                    var pricePlan = BillingPlanProxy.GetPricePlan(pricePlanId.Value, tenantId);

                    if (pricePlan != null && pricePlan.LineItems != null && pricePlan.LineItems.Any(x => x.Id == lineItemId.Value))
                    {
                        pricePlan.LineItems.Remove(pricePlan.LineItems.Single(x => x.Id == lineItemId.Value));
                        BillingPlanProxy.UpdatePricePlan(pricePlan);

                        return Json(new { Success = Resources.PackageResource.s_DeleteLineItem });
                    }
                    else
                    {
                        return Json(new { Error = Resources.PackageResource.e_LineItemNotFound });
                    }
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (BillingException ex)
                {
                    ExceptionService.HandleException(ex, PolicyName);
                    return Json(new { Error = ex.Message });
                }
            }

            return Json(new { Error = Resources.PackageResource.e_Request });
        }

        /// <summary>
        /// This method is used to delete the package price plan based on the price plan and package identifier.
        /// </summary>
        /// <param name="pricePlanId">price plan identifier.</param>
        /// <param name="packageId">package identifier.</param>
        /// <returns></returns>
        public ActionResult DeletePricePlan(Guid pricePlanId, Guid packageId)
        {
            try
            {
                var tenantId = Guid.Parse(this.TenantId);
                var pricePlan = BillingPlanProxy.GetPricePlan(pricePlanId, tenantId);

                if (pricePlan == null)
                {
                    TempData["Error"] = Resources.PackageResource.e_PriceplanNotAvailable;
                }
                else
                {
                    BillingPlanProxy.DeletePricePlan(pricePlanId, tenantId);
                    TempData["Success"] = string.Format(Resources.PackageResource.s_DeletePricePlan, pricePlan.Name);
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, PolicyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("PricePlanList", new { packageId = packageId });
        }

        #endregion
    }
}
