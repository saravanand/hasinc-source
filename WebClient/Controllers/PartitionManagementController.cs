﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.PartitionManagement.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.License;
using CelloSaaS.PartitionManagement.Model;
using System.Text.RegularExpressions;
using CelloSaaS.View;



namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for partition management.
    /// </summary>
    public class PartitionManagementController : CelloController
    {
        private const string DefaultPolicy = "PartitionManagementExceptionLogger";

        public ActionResult Index()
        {
            return View();
        }

        #region DataModule

        #region public methods

        /// <summary>
        /// This method is used to manage data module view.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageDataModule()
        {
            GetAllDataModuleRecord();

            return View(new DataModule());
        }

        /// <summary>
        /// This method is used to add data module page used for add or update the dataModule record when user click add/update button.
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddDataModuleDetails(FormCollection formCollection)
        {
            DataModule dataModule = new DataModule();

            try
            {
                if (formCollection.AllKeys.Contains("Id"))
                {
                    Guid tempDataModuleID = Guid.Parse(formCollection["Id"]);

                    if (tempDataModuleID != Guid.Empty)
                    {
                        dataModule = DataModuleProxy.GetDataModuleById(tempDataModuleID);
                    }
                }

                TryUpdateModel(dataModule);
                ModelState.Clear();

                ValidateDataModule(dataModule);

                if (ModelState.IsValid)
                {
                    if (dataModule.Id == Guid.Empty)
                    {
                        DataModuleProxy.AddDataModule(dataModule);
                        TempData["DataModuleSuccess"] = string.Format(Resources.PartitionManagementResource.s_AddDataModuleSuccess, dataModule.DataModuleName);

                        return RedirectToAction("ManageDataModule");
                    }
                    else
                    {
                        DataModuleProxy.UpdateDataModule(dataModule);
                        TempData["DataModuleSuccess"] = string.Format(Resources.PartitionManagementResource.s_UpdateDataModuleSuccess, dataModule.DataModuleName);

                        return RedirectToAction("ManageDataModule");
                    }
                }
            }
            catch (DuplicateDataModuleNameException duplicateDataModuleNameException)
            {
                ExceptionService.HandleException(duplicateDataModuleNameException, DefaultPolicy);
                ModelState.AddModelError("DataModuleWarning", duplicateDataModuleNameException.Message);
            }
            catch (DuplicateDataModuleCodeException duplicateDataModuleCodeException)
            {
                ExceptionService.HandleException(duplicateDataModuleCodeException, DefaultPolicy);
                ModelState.AddModelError("DataModuleWarning", duplicateDataModuleCodeException.Message);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_AddUpdateDataModule);
            }

            return View("AddDataModuleDetails", dataModule);
        }

        /// <summary>
        /// This method is used to edit data module name.
        /// </summary>
        /// <param name="dataModuleId">data module identifier.</param>
        /// <returns></returns>
        public ActionResult EditDataModuleDetails(Guid dataModuleId)
        {
            if (dataModuleId == Guid.Empty)
            {
                return HttpNotFound();
            }
            DataModule dataModule = null;

            try
            {
                dataModule = DataModuleProxy.GetDataModuleById(dataModuleId);

                if (dataModule == null)
                {
                    return HttpNotFound();
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_EditDataModule);
            }

            return View("AddDataModuleDetails", dataModule);
        }

        /// <summary>
        /// This method is used to load all data module.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult DataModuleDetailsList()
        {
            GetAllDataModuleRecord();
            return PartialView();
        }

        /// <summary>
        /// This method is used to delete data module.
        /// </summary>
        /// <param name="dataModuleId">data module identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteDataModuleDetails(Guid dataModuleId, string dataModuleName)
        {
            try
            {
                if (dataModuleId != Guid.Empty)
                {
                    DataModuleProxy.DeleteDataModule(dataModuleId);
                    TempData["DataModuleSuccess"] = string.Format(Resources.PartitionManagementResource.s_DeleteDataModuleSuccess, dataModuleName);
                }
                else
                {
                    ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_InvalidDataModuleID);
                }
            }
            catch (IsDataModuleReferredException isDataModuleReferedException)
            {
                ExceptionService.HandleException(isDataModuleReferedException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_IsDataModuleReferedException);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_DeleteDataModule);
            }
            GetAllDataModuleRecord();
            return PartialView("DataModuleDetailsList");
        }

        /// <summary>
        /// This method is used to refresh the form.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddDataModuleDetails()
        {
            return View("AddDataModuleDetails", new DataModule());
        }

        #endregion  public methods

        #region Private methods

        /// <summary>
        /// This method is used to get the all data module record from database.
        /// </summary>
        private void GetAllDataModuleRecord()
        {
            try
            {
                ViewData["DataModuleList"] = DataModuleProxy.GetAllDataModule();
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataModuleError", Resources.PartitionManagementResource.e_GetAllDataModule);
            }
        }

        /// <summary>
        /// This method is used to validate the data module records.
        /// </summary>
        /// <param name="dataModule">data module details.</param>
        private void ValidateDataModule(DataModule dataModule)
        {
            if (string.IsNullOrWhiteSpace(dataModule.DataModuleName))
            {
                ModelState.AddModelError("DataModuleWarning", Resources.PartitionManagementResource.e_DataModuleName);
                ModelState.AddModelError("DataModuleName", "");
            }
            else if (dataModule.DataModuleName.Length > 100)
            {
                ModelState.AddModelError("DataModuleWarning", string.Format(Resources.PartitionManagementResource.e_DataModuleNameLength, dataModule.DataModuleName));
                ModelState.AddModelError("DataModuleName", "");
            }
            //else if (!Regex.IsMatch(dataModule.DataModuleName, @"^[\w\s]+$"))
            //{
            //    ModelState.AddModelError("DataModuleWarning", string.Format("Only alphanumeric charcters are allowed in {0}  name.Please check it.", dataModule.DataModuleName));
            //    ModelState.AddModelError("DataModuleName", "");
            //}

            if (string.IsNullOrWhiteSpace(dataModule.DataModuleCode))
            {
                ModelState.AddModelError("DataModuleWarning", Resources.PartitionManagementResource.e_DataModuleCode);
                ModelState.AddModelError("DataModuleCode", "");
            }
            else if (!Regex.IsMatch(dataModule.DataModuleCode, @"^[\p{L}\d'_]+$"))
            {
                ModelState.AddModelError("DataModuleWarning", string.Format(Resources.PartitionManagementResource.e_FormatDataModuleCode, dataModule.DataModuleCode));
                ModelState.AddModelError("DataModuleCode", "");
            }
            else if (dataModule.DataModuleCode.Length > 100)
            {
                ModelState.AddModelError("DataModuleWarning", string.Format(Resources.PartitionManagementResource.e_DataModuleCodeLength, dataModule.DataModuleName));
                ModelState.AddModelError("DataModuleName", "");
            }

            if (string.IsNullOrWhiteSpace(dataModule.Description))
            {
                ModelState.AddModelError("DataModuleWarning", Resources.PartitionManagementResource.e_Description);
                ModelState.AddModelError("Description", "");
            }
        }

        #endregion Private methods

        #endregion DataModule

        #region DataServer

        #region Public Methods

        /// <summary>
        /// This method is used to manage data server view .
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageDataServer()
        {
            GetAllDataServerRecord();

            return View(new DataServer());
        }

        /// <summary>
        /// This method is used to view the partial data server page used for add or update the data partition record when user click add/update button.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddDataServerDetails(FormCollection formCollection)
        {
            DataServer dataServer = new DataServer();

            try
            {
                if (formCollection.AllKeys.Contains("Id"))
                {
                    Guid tempDataServerID = Guid.Parse(formCollection["Id"]);

                    if (tempDataServerID != Guid.Empty)
                    {
                        dataServer = DataServerProxy.GetDataServerById(tempDataServerID);
                    }

                }
                TryUpdateModel(dataServer);
                ModelState.Clear();

                ValidateDataServer(dataServer);

                if (ModelState.IsValid)
                {

                    if (dataServer.Id == Guid.Empty)
                    {
                        DataServerProxy.AddDataServer(dataServer);

                        return Content("AddSuccess");
                    }
                    else
                    {
                        DataServerProxy.UpdateDataServer(dataServer);

                        return Content("UpdateSuccess");
                    }
                }
            }
            catch (InvalidConnectionStringException inValidConnectionStringException)
            {
                ExceptionService.HandleException(inValidConnectionStringException, DefaultPolicy);
                ModelState.AddModelError("DataServerWarning", inValidConnectionStringException.Message);
            }
            catch (DuplicateDataServerNameException duplicateDataServerNameException)
            {
                ExceptionService.HandleException(duplicateDataServerNameException, DefaultPolicy);
                ModelState.AddModelError("DataServerWarning", duplicateDataServerNameException.Message);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_AddUpdateDataServer);
            }

            return View("AddDataServerDetails", dataServer);
        }

        /// <summary>
        /// This method is used to edit data server.
        /// </summary>
        /// <param name="dataServerId">data server identifier.</param>
        /// <returns></returns>
        public PartialViewResult EditDataServerDetails(Guid dataServerId)
        {
            DataServer dataServer = null;

            try
            {
                if (dataServerId != Guid.Empty)
                {
                    dataServer = DataServerProxy.GetDataServerById(dataServerId);
                }
                else
                {
                    ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_InvalidDataServerId);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_EditDataServer);
            }

            return PartialView("AddDataServerDetails", dataServer);
        }

        /// <summary>
        /// This method is used to load all data partition.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult DataServerDetailsList()
        {
            GetAllDataServerRecord();
            return PartialView();
        }

        /// <summary>
        /// This method is used to delete data server.
        /// </summary>
        /// <param name="dataServerId">data server identifier.</param>
        /// <param name="dataServerName">data server name.</param>
        /// <returns></returns>
        public ActionResult DeleteDataServerDetails(Guid dataServerId, string dataServerName)
        {
            try
            {
                if (dataServerId != Guid.Empty)
                {
                    DataServerProxy.DeleteDataServer(dataServerId);
                    ModelState.AddModelError("DataServerSuccess", string.Format(Resources.PartitionManagementResource.s_DeleteDataServerSuccess, dataServerName));
                }
                else
                {
                    ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_InvalidDataServerId);
                }
            }
            catch (IsDataServerReferredException isDataServerReferedException)
            {
                ExceptionService.HandleException(isDataServerReferedException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_IsDataServerReferedException);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_DeleteDataServer);
            }
            GetAllDataServerRecord();
            return View("DataServerDetailsList");
        }

        /// <summary>
        /// This method is used to add data server details.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddDataServerDetails()
        {
            return View("AddDataServerDetails", new DataServer());
        }

        #endregion public Methods

        #region Private Methods

        /// <summary>
        /// This method is used to get the all data partition record from database.
        /// </summary>
        private void GetAllDataServerRecord()
        {
            try
            {
                ViewData["DataServerList"] = DataServerProxy.GetAllDataServer();
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataServerError", Resources.PartitionManagementResource.e_GetAllDataServer);
            }
        }

        /// <summary>
        /// This method is used to validate the partitionSet records.
        /// </summary>
        /// <param name="dataServer">data server.</param>
        private void ValidateDataServer(DataServer dataServer)
        {
            if (string.IsNullOrWhiteSpace(dataServer.Name))
            {
                ModelState.AddModelError("DataServerWarning", Resources.PartitionManagementResource.e_DataServerName);
                ModelState.AddModelError("Name", "");
            }
            else if (dataServer.Name.Length > 100)
            {
                ModelState.AddModelError("DataServerWarning", string.Format(Resources.PartitionManagementResource.e_DataServerNameLength, dataServer.Name));
                ModelState.AddModelError("Name", "");
            }
            //else if (!Regex.IsMatch(dataServer.DataServerName, @"^[\w\s]+$"))
            //{
            //    ModelState.AddModelError("DataServerWarning", string.Format("Only alphanumeric charcters are allowed in {0}  name.Please check it.", dataServer.DataServerName));
            //    ModelState.AddModelError("PartitionName", "");
            //}

            if (string.IsNullOrWhiteSpace(dataServer.ServerAddress))
            {
                ModelState.AddModelError("DataServerWarning", Resources.PartitionManagementResource.e_ServeAddress);
                ModelState.AddModelError("ServerAddress", "");
            }

            if (string.IsNullOrWhiteSpace(dataServer.DataBaseName))
            {
                ModelState.AddModelError("DataServerWarning", Resources.PartitionManagementResource.e_DataBaseName);
                ModelState.AddModelError("DataBaseName", "");
            }

            if (string.IsNullOrWhiteSpace(dataServer.ConnectionString))
            {
                ModelState.AddModelError("DataServerWarning", Resources.PartitionManagementResource.e_ConnectionString);
                ModelState.AddModelError("ConnectionString", "");
            }

        }



        #endregion Private Methods

        #endregion DataServer

        #region DataPartition

        #region Public Methods

        /// <summary>
        /// This method is used to manage data partition view.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageDataPartition()
        {
            GetAllDataPartitionRecord();
            LoadAllDataPartitionStats();

            return View(new DataPartition());
        }

        /// <summary>
        /// This method is used to add data partition page used for add or update the DataPartition record when user click add/update button.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddDataPartitionDetails(FormCollection formCollection)
        {
            DataPartition dataPartition = new DataPartition();

            try
            {
                if (formCollection.AllKeys.Contains("Id"))
                {
                    Guid tempDataPartitionID = Guid.Parse(formCollection["Id"]);

                    if (tempDataPartitionID != Guid.Empty)
                    {
                        dataPartition = DataPartitionProxy.GetDataPartitionById(tempDataPartitionID);
                    }
                }
                TryUpdateModel(dataPartition);
                ModelState.Clear();

                ValidateDataPartition(dataPartition);

                if (ModelState.IsValid)
                {
                    if (dataPartition.Id == Guid.Empty)
                    {
                        DataPartitionProxy.AddDataPartition(dataPartition);
                        //  ModelState.AddModelError("DataPartitionSuccess", string.Format("{0} record is inserted  Successfully.", dataPartition.DataPartitionName));

                        return Content("AddSuccess");
                    }
                    else
                    {
                        DataPartitionProxy.UpdateDataPartition(dataPartition);
                        //   ModelState.AddModelError("DataPartitionSuccess", string.Format("{0} record is Updated  Successfully.", dataPartition.DataPartitionName));

                        return Content("UpdateSuccess");
                    }
                }
            }
            catch (DuplicateDataPartitionNameException duplicateDataPartitionNameException)
            {
                ExceptionService.HandleException(duplicateDataPartitionNameException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionWarning", duplicateDataPartitionNameException.Message);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_AddUpdateDataPartition);
            }

            return View("AddDataPartitionDetails", dataPartition);
        }

        /// <summary>
        /// This method is used to edit data partition.
        /// </summary>
        /// <param name="dataPartitionId">data partition identifier.</param>
        /// <returns></returns>
        public PartialViewResult EditDataPartitionDetails(Guid dataPartitionId)
        {
            DataPartition dataPartition = null;

            try
            {
                if (dataPartitionId != Guid.Empty)
                {
                    dataPartition = DataPartitionProxy.GetDataPartitionById(dataPartitionId);
                }
                else
                {
                    ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_InvalidDataPartitionId);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_EditDataPartition);
            }

            return PartialView("AddDataPartitionDetails", dataPartition);
        }

        /// <summary>
        /// This method is used load all data partition.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult DataPartitionDetailsList()
        {
            GetAllDataPartitionRecord();
            return PartialView();
        }

        /// <summary>
        /// This method is used for set/reset the Default Control.
        /// </summary>
        /// <param name="dataPartitionId">data partition identifier.</param>
        public PartialViewResult SetDefalutDataPartition(Guid dataPartitionId)
        {
            try
            {
                DataPartitionProxy.SetDataPartitionDefault(dataPartitionId);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionError", Resources.PartitionManagementResource.e_DeleteDataPartition);
            }
            GetAllDataPartitionRecord();
            return PartialView("DataPartitionDetailsList", new DataPartition());
        }

        /// <summary>
        /// This method is used to refresh the add data partition form.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddDataPartitionDetails()
        {
            return PartialView("AddDataPartitionDetails", new DataPartition());
        }

        /// <summary>
        /// This method is used to load data partition statistics
        /// </summary>
        /// <returns></returns>
        public PartialViewResult LoadDataPartitionStatistics()
        {
            LoadAllDataPartitionStats();
            return PartialView();
        }

        #endregion public Methods

        #region Private Methods

        /// <summary>
        /// This method is used to load all data partition statistics.
        /// </summary>
        private void LoadAllDataPartitionStats()
        {
            ViewData["DataPartitionStats"] = DataPartitionProxy.GetDataPartitionStatistics();
        }

        /// <summary>
        /// This method is used to get the all data partition record from database.
        /// </summary>
        private void GetAllDataPartitionRecord()
        {
            try
            {
                ViewData["DataPartitionList"] = DataPartitionProxy.GetAllDataPartition();
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.PartitionManagementResource.e_GetAllDataPartition);
            }
        }

        /// <summary>
        /// This method is used to validate the data partition records.
        /// </summary>
        /// <param name="dataPartition">data partition details.</param>
        private void ValidateDataPartition(DataPartition dataPartition)
        {
            if (string.IsNullOrWhiteSpace(dataPartition.DataPartitionName))
            {
                ModelState.AddModelError("DataPartitionWarning", Resources.PartitionManagementResource.e_DataPartitionName);
                ModelState.AddModelError("DataPartitionName", "");
            }
            else if (dataPartition.DataPartitionName.Length > 100)
            {
                ModelState.AddModelError("DataPartitionWarning", string.Format(Resources.PartitionManagementResource.e_DataPartitionNameLength, dataPartition.DataPartitionName));
                ModelState.AddModelError("DataPartitionName", "");
            }
            //else if (!Regex.IsMatch(dataPartition.DataPartitionName, @"^[\w\s]+$"))
            //{
            //    ModelState.AddModelError("DataPartitionWarning", string.Format("Only alphanumeric charcters are allowed in {0}  name.Please check it.", dataPartition.DataPartitionName));
            //    ModelState.AddModelError("DataPartitionName", "");
            //}

            if (string.IsNullOrWhiteSpace(dataPartition.Description))
            {
                ModelState.AddModelError("DataPartitionWarning", Resources.PartitionManagementResource.e_DataPartitionDescription);
                ModelState.AddModelError("Description", "");
            }
        }

        #endregion Private Methods

        #endregion DataPartition

        #region DataPartitionMapping

        #region Public Methods
        /// <summary>
        /// This method is used to manage data partition mapping view.
        /// </summary>
        /// <param name="dataPartitionId">data partition identifier.</param>
        /// <returns></returns>
        public ActionResult ManageDataPartitionMapping(Guid dataPartitionId)
        {
            if (dataPartitionId == Guid.Empty)
            {
                return HttpNotFound("Invalid request");
            }
            else
            {
                var dataPartition = LoadAllDataPartitionMapping(dataPartitionId);

                if (dataPartition == null)
                {
                    return HttpNotFound("Invalid request");
                }
                GetMapList(new DataPartitionMapping(), dataPartitionId);
                ViewData["DataPartitionID"] = dataPartitionId;
                ViewData["DataPartitionName"] = dataPartition.DataPartitionName;
                return View(new DataPartitionMapping());
            }
        }

        /// <summary>
        /// This method is used to add data partition mapping details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddDataPartitionMappingDetails(FormCollection formCollection)
        {
            Guid tempDataPartitionId = new Guid();

            if (formCollection.AllKeys.Contains("DataPartitionID"))
            {
                tempDataPartitionId = Guid.Parse(formCollection["DataPartitionID"]);
            }

            if (tempDataPartitionId == Guid.Empty)
            {
                return RedirectToAction("ManageDataPartition", "PartitionManagement");
            }
            else
            {
                DataPartitionMapping dataPartitionMapping = new DataPartitionMapping();

                try
                {
                    if (formCollection.AllKeys.Contains("Id"))
                    {
                        Guid tempDataPartitionMappingId = Guid.Parse(formCollection["Id"]);

                        if (tempDataPartitionMappingId != Guid.Empty)
                        {
                            dataPartitionMapping = DataPartitionProxy.GetDataPartitionMappingById(tempDataPartitionMappingId);
                        }
                    }
                    TryUpdateModel(dataPartitionMapping);
                    ModelState.Clear();

                    ValidateDataPartitionMapping(dataPartitionMapping);

                    if (ModelState.IsValid)
                    {
                        if (dataPartitionMapping.Id == Guid.Empty)
                        {
                            DataPartitionProxy.AddDataPartitionMapping(dataPartitionMapping);

                            return Content("AddSuccess");
                        }
                        else
                        {
                            DataPartitionProxy.UpdateDataPartitionMapping(dataPartitionMapping);

                            return Content("UpdateSuccess");
                        }
                    }
                }
                catch (DuplicateMappingException duplicateMappingException)
                {
                    ExceptionService.HandleException(duplicateMappingException, DefaultPolicy);
                    ModelState.AddModelError("DataPartitionMappingWarning", duplicateMappingException.Message);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                    ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, DefaultPolicy);
                    ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
                }
                catch (PartitionManagementException partitionManagementException)
                {
                    ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                    ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_AddUpdateDataPartitionMapping);
                }

                ViewData["DataPartitionID"] = tempDataPartitionId;
                GetMapList(dataPartitionMapping, tempDataPartitionId);

                return View("AddDataPartitionMappingDetails", dataPartitionMapping);
            }
        }

        /// <summary>
        /// This method is used to edit data partition mapping.
        /// </summary>
        /// <param name="dataPartitionMappingId">data partition mapping identifier.</param>
        /// <returns></returns>
        public ActionResult EditDataPartitionMappingDetails(Guid dataPartitionMappingId)
        {
            DataPartitionMapping dataPartitionMap = null;

            try
            {
                if (dataPartitionMappingId != Guid.Empty)
                {
                    dataPartitionMap = DataPartitionProxy.GetDataPartitionMappingById(dataPartitionMappingId);
                }
                else
                {
                    ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_InvalidDataPartitionMappingId);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_EditDataPartitionMapping);
            }

            if (dataPartitionMap != null && dataPartitionMap.DataPartitionId != Guid.Empty)
            {
                ViewData["DataPartitionID"] = dataPartitionMap.DataPartitionId;
                GetMapList(dataPartitionMap, dataPartitionMap.DataPartitionId);
            }
            return PartialView("AddDataPartitionMappingDetails", dataPartitionMap);
        }

        /// <summary>
        /// This method is used to delete data partition mapping.
        /// </summary>
        /// <param name="dataPartitionMappingId">data partition identifier.</param>
        /// <param name="dataPartitionId">data partition identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteDataPartitionMappingDetails(Guid dataPartitionMappingId, Guid dataPartitionId)
        {

            if (dataPartitionId == Guid.Empty)
            {
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_InvalidDataPartitionId);
            }
            else
            {
                try
                {
                    if (dataPartitionMappingId != Guid.Empty)
                    {
                        DataPartitionProxy.DeleteDataPartitionMapping(dataPartitionMappingId);
                        ModelState.AddModelError("DataPartitionMappingSuccess", "Mapping details are deleted successfully");
                    }
                    else
                    {
                        ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_InvalidDataPartitionMappingId);
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                    ModelState.AddModelError("Error", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, DefaultPolicy);
                    ModelState.AddModelError("Error", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
                }
                catch (PartitionManagementException partitionManagementException)
                {
                    ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                    ModelState.AddModelError("Error", Resources.PartitionManagementResource.e_DeleteDataPartitionMapping);
                }

                LoadAllDataPartitionMapping(dataPartitionId);
            }
            return PartialView("DataPartitionMappingDetailsList");
        }

        /// <summary>
        /// This method is used to load all data partition mapping
        /// </summary>
        /// <param name="dataPartitionId">data partition identifier.</param>
        /// <returns></returns>
        public ActionResult DataPartitionMappingDetailsList(Guid dataPartitionId)
        {
            if (dataPartitionId == Guid.Empty)
            {
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_InvalidDataPartitionId);
            }
            else
            {
                LoadAllDataPartitionMapping(dataPartitionId);
            }
            return PartialView("DataPartitionMappingDetailsList");
        }

        /// <summary>
        /// This method is used to refresh the form based on the data partition identifier.
        /// </summary>
        /// <param name="dataPartitionId">data partition identifier.</param>
        /// <returns></returns>
        public ActionResult AddDataPartitionMappingDetails(Guid dataPartitionId)
        {
            if (dataPartitionId == Guid.Empty)
            {
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_InvalidDataPartitionId);
            }
            else
            {
                GetMapList(new DataPartitionMapping(), dataPartitionId);
                ViewData["DataPartitionID"] = dataPartitionId;
            }
            return View("AddDataPartitionMappingDetails", new DataPartitionMapping());
        }

        #endregion public Methods

        #region Private Methods

        /// <summary>
        /// This method is used to get the data for dropdown.
        /// </summary>
        /// <param name="dataPartitionMapping">data partition mapping details.</param>
        /// <param name="dataPartitionId">data partition identifier.</param>
        private void GetMapList(DataPartitionMapping dataPartitionMapping, Guid dataPartitionId)
        {
            try
            {
                if (dataPartitionMapping != null)
                {
                    DataPartition dataPartition = DataPartitionProxy.GetDataPartitionById(dataPartitionId);

                    if (dataPartition != null)
                    {
                        Dictionary<Guid, DataServer> dataServerList = DataServerProxy.GetAllDataServer();
                        Dictionary<Guid, DataModule> dataModuleList = DataModuleProxy.GetAllDataModule();

                        List<SelectListItem> dataServerItem = new List<SelectListItem>();

                        if (dataServerList != null && dataServerList.Count > 0)
                        {
                            dataServerItem = (from rt in dataServerList.Values
                                              select new SelectListItem
                                              {
                                                  Text = rt.Name,
                                                  Value = rt.Id.ToString(),
                                                  Selected = dataPartitionMapping.DataServerId != Guid.Empty ? dataPartitionMapping.DataServerId.Equals(rt.Id) : false
                                              }).ToList();


                        }

                        dataServerItem.Insert(0, new SelectListItem() { Text = "Select DataServer", Value = Guid.Empty.ToString() });
                        ViewData["DataServerList"] = dataServerItem;

                        Dictionary<Guid, string> tempDataModuleList = null;

                        if (dataModuleList != null && dataModuleList.Count > 0)
                        {
                            if (dataPartition.DataPartitionMappings != null && dataPartition.DataPartitionMappings.Count > 0)
                            {
                                tempDataModuleList = new Dictionary<Guid, string>();
                                foreach (var item in dataModuleList)
                                {
                                    bool status = dataPartitionMapping.Id == Guid.Empty ? dataPartition.DataPartitionMappings.Any(c => c.DataModuleId == item.Key) : dataPartition.DataPartitionMappings.Any(c => c.DataModuleId == item.Key && c.Id != dataPartitionMapping.Id);

                                    if (!status)
                                    {
                                        tempDataModuleList.Add(item.Value.Id, item.Value.DataModuleName);
                                    }
                                }
                            }
                            else
                            {
                                tempDataModuleList = dataModuleList.Values.ToDictionary(c => c.Id, c => c.DataModuleName);
                            }
                        }
                        List<SelectListItem> dataModuleItem = new List<SelectListItem>();

                        if (tempDataModuleList != null && tempDataModuleList.Count > 0)
                        {
                            dataModuleItem = (from rt in tempDataModuleList
                                              select new SelectListItem
                                              {
                                                  Text = rt.Value,
                                                  Value = rt.Key.ToString(),
                                                  Selected = dataPartitionMapping.DataModuleId != Guid.Empty ? dataPartitionMapping.DataModuleId.Equals(rt.Key) : false
                                              }).ToList();
                        }

                        dataModuleItem.Insert(0, new SelectListItem() { Text = "Select DataModule", Value = Guid.Empty.ToString() });
                        ViewData["DataModuleList"] = dataModuleItem;
                    }
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_GetAllMapList);
            }
        }

        /// <summary>
        /// This method is used to load the data partition mapping data.
        /// </summary>
        private DataPartition LoadAllDataPartitionMapping(Guid dataPartitionId)
        {
            DataPartition datapartition = null;
            try
            {
                datapartition = DataPartitionProxy.GetDataPartitionById(dataPartitionId);

                if (datapartition != null)
                {
                    ViewData["DataPartitionMappingList"] = datapartition.DataPartitionMappings != null && datapartition.DataPartitionMappings.Count > 0 ? datapartition.DataPartitionMappings : null;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_ParameterEmptyOrNull);
            }
            catch (PartitionManagementException partitionManagementException)
            {
                ExceptionService.HandleException(partitionManagementException, DefaultPolicy);
                ModelState.AddModelError("DataPartitionMappingError", Resources.PartitionManagementResource.e_GetAllDataPartitionMapping);
            }

            return datapartition;
        }

        /// <summary>
        /// This method is used to validate the data partition mapping.
        /// </summary>
        /// <param name="dataPartitionMapping">data partition mapping details.</param>
        private void ValidateDataPartitionMapping(DataPartitionMapping dataPartitionMapping)
        {
            if (dataPartitionMapping.DataModuleId == Guid.Empty)
                ModelState.AddModelError("DataPartitionMappingWarning", Resources.PartitionManagementResource.e_DataModule);

            if (dataPartitionMapping.DataServerId == Guid.Empty)
                ModelState.AddModelError("DataPartitionMappingWarning", Resources.PartitionManagementResource.e_DataServer);
        }

        #endregion Private Methods

        #endregion DataPartitionMapping

    }
}
