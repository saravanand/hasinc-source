﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.Reporting;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.QueryBuilderLibrary;
using CelloSaaS.QueryBuilderLibrary.Model;
using CelloSaaS.QueryBuilderLibrary.ServiceContracts;
using CelloSaaS.QueryBuilderLibrary.ServiceProxies;
using CelloSaaS.Reporting.DataSources.ServiceProxies;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.Util.Library.Export;
using CelloSaaS.View;
using CelloSaaSApplication.Models;
using Resources;
using CelloSaaS.Library.Helpers;
using System.Globalization;
using CelloSaaS.ChartBuilder.ServiceProxies;
using CelloSaaS.ChartBuilder.ServiceContracts;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Web;
using CelloSaaS.Model.GlobalVariables;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for query builder.
    /// </summary>
    public class QueryBuilderController : CelloController
    {
        //Logger policy name
        private const string DefaultPolicy = "ReportingExceptionLogger";

        private string TenantId = TenantContext.GetTenantId("_QueryBuilder");

        #region Public Methods

        /// <summary>
        /// This method is used to gets the primary entity primary key.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetPrimaryEntityPrimaryKey(string entityId)
        {
            try
            {
                var primaryKeyFieldInfo = FieldProxy.GetPrimaryFieldsForEntity(entityId, this.TenantId);
                return Json(primaryKeyFieldInfo.Values.ToList());
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to gets the primary entity tenant field.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetPrimaryEntityTenantField(string entityId)
        {
            try
            {
                var tenantFieldInfo = FieldProxy.GetTenantFieldForEntity(entityId, this.TenantId);
                return Json(tenantFieldInfo);
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to copy the global query.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <returns></returns>
        public ActionResult CopyGlobalQuery(string queryId)
        {
            try
            {
                int copyStatus = QueryDetailsProxy.CopyGlobalQuery(queryId, this.TenantId, UserIdentity.UserId);

                if (copyStatus < 1)
                {
                    TempData["Success"] = QueryBuilderResource.e_GlobalQueryCopyFailed;
                }
                else
                {
                    TempData["Success"] = string.Format(QueryBuilderResource.m_GlobalQueryCopiedSuccessfully, copyStatus);
                }
            }
            catch (DuplicateQueryDetailsNameException duplicateQueryDetailsNameException)
            {
                TempData["Error"] = duplicateQueryDetailsNameException.Message;
            }
            catch (Exception exception)
            {
                TempData["Error"] = exception.Message;
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to copy all queries from parent.
        /// </summary>
        /// <returns></returns>
        public ActionResult CopyAllQueriesFromParent()
        {
            try
            {
                int copiedQueryCount = QueryDetailsProxy.CopyAllQueriesFromParent(this.TenantId);

                if (copiedQueryCount > 0)
                {
                    TempData["Success"] = string.Format(QueryBuilderResource.m_QueriesCopiedSuccessfully, copiedQueryCount);
                }
                else
                {
                    TempData["Error"] = QueryBuilderResource.m_NoQueriesAvailableToCopyFromParent;
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// This method is used to get global queries.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult GlobalQueries()
        {
            Dictionary<string, QueryDetails> globalQueries = new Dictionary<string, QueryDetails>();
            try
            {
                globalQueries = QueryDetailsProxy.GetAllGlobalReportQueries();
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("Error", exception.Message);
            }
            return PartialView(globalQueries.Values.ToList());
        }

        /// <summary>
        /// This method is used to load the query builder page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<QueryDetails> queryDataList = null;

            try
            {
                if (TempData["Status"] != null)
                {
                    ModelState.AddModelError("Success", TempData["Status"].ToString());
                }

                queryDataList = QueryDetailsProxy.GetAllQueryDetails(this.TenantId);
                ViewData["QueryList"] = queryDataList;
            }
            catch (QueryBuilderException ex)
            {
                ModelState.AddModelError("", ex.Message);
                ExceptionService.HandleException(ex, DefaultPolicy);
            }

            TempDataToModelState();
            return View();
        }

        private void TempDataToModelState()
        {
            foreach (var key in TempData.Keys)
            {
                ModelState.AddModelError(key, TempData[key].ToString());
            }
        }

        /// <summary>
        /// This method is used to checks the edit ability based on the given query identifier.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <returns></returns>
        public JsonResult CheckEditability(string queryId)
        {
            if (string.IsNullOrEmpty(queryId))
            {
                return Json(new { Message = QueryBuilderResource.e_Invalid_Query, Status = "Error" });
            }

            var mappedSources = DataSourceProxy.GetSourcesByContentId(queryId, this.TenantId);

            if (mappedSources == null || mappedSources.Count <= 0)
            {
                return Json(new { Message = QueryBuilderResource.m_NoConsumersForThisQuery, Status = "Success" });
            }

            foreach (var mappedSource in mappedSources)
            {
                var charts = ChartDetailsProxy.GetChartsBySourceId(mappedSource.Value.Id, mappedSource.Value.TenantId);

                var tableSources = TableSourceProxy.GetTableSourcesBySourceContentId(queryId, mappedSource.Value.TenantId);

                if ((charts != null && charts.Count > 0) || (tableSources != null && tableSources.Count > 0))
                {
                    return Json(new { Message = QueryBuilderResource.e_QueryInUse, Status = "Error" });
                }
            }
            return Json(new { Message = QueryBuilderResource.m_NoConsumersForThisQuery, Status = "Success" });
        }

        /// <summary>
        /// This method is used to deletes the query based on the given query identifier.
        /// </summary>
        /// <param name="queryId">The query identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteQuery(string queryId)
        {
            if (string.IsNullOrEmpty(queryId))
            {
                return RedirectToAction("Index");
            }
            try
            {
                var mappedSources = DataSourceProxy.GetSourcesByContentId(queryId, this.TenantId);

                if (mappedSources == null || mappedSources.Count <= 0)
                {
                    QueryDetailsProxy.DeleteQueryDetails(queryId);
                    this.TempData["Success"] = QueryBuilderResource.s_deleteQuery;
                    return RedirectToAction("Index");
                }
                foreach (var mappedSource in mappedSources)
                {
                    var charts = ChartDetailsProxy.GetChartsBySourceId(mappedSource.Value.Id, mappedSource.Value.TenantId);

                    var tableSources = TableSourceProxy.GetTableSourcesBySourceContentId(
                        queryId, mappedSource.Value.TenantId);

                    if ((charts != null && charts.Count > 0) || (tableSources != null && tableSources.Count > 0))
                    {
                        this.TempData["Error"] = QueryBuilderResource.e_QueryInUse;
                    }
                    else
                    {
                        DataSourceProxy.DeleteSource(mappedSource.Value.Id, mappedSource.Value.TenantId);
                    }
                }
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                this.TempData["Error"] = ex.Message;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                this.TempData["Error"] = ex.Message;
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to manages the query
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <param name="QueryType">type of the query.</param>
        /// <returns></returns>
        public ActionResult ManageQuery(string queryId, QueryTypes QueryType)
        {
            string moduleId = string.Empty;
            string primaryEntityId = string.Empty;
            string queryName = string.Empty;
            string queryDescription = string.Empty;
            bool isGlobalQuery = false;

            if (QueryType.Equals(QueryTypes.DatascopeQuery) && !UserIdentity.IsInRole(RoleConstants.ProductAdmin))
            {
                TempData["Error"] = QueryBuilderResource.e_accessdenied;
                return RedirectToAction("Index");
            }

            if (!string.IsNullOrEmpty(queryId))
            {
                try
                {
                    var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(queryId);

                    if (queryDetails != null && queryDetails.QueryInfo != null)
                    {
                        moduleId = queryDetails.QueryInfo.ModuleId;
                        primaryEntityId = queryDetails.QueryInfo.PrimaryEntityIdentifier;
                        queryName = queryDetails.Name;
                        queryDescription = queryDetails.Description;
                        ViewData["renderMode"] = queryDetails.RenderMode;
                        isGlobalQuery = string.IsNullOrEmpty(queryDetails.TenantId) ? true : false;
                    }
                    else
                    {
                        queryId = string.Empty;
                    }
                }
                catch (QueryBuilderException ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        ModelState.AddModelError("", ex.InnerException.Message);
                    }
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    ModelState.AddModelError("", ex.Message);
                    if (ex.InnerException != null)
                    {
                        ModelState.AddModelError("", ex.InnerException.Message);
                    }
                }
            }

            if (string.IsNullOrEmpty(moduleId) && !string.IsNullOrEmpty(queryId))
            {
                ModelState.AddModelError("", QueryBuilderResource.e_Invalid_Query);
            }

            LoadModules(moduleId);
            ViewData["IsGlobalQuery"] = isGlobalQuery;
            ViewData["moduleId"] = moduleId;
            ViewData["primaryEntityId"] = primaryEntityId;
            ViewData["queryId"] = queryId;
            ViewData["queryName"] = queryName;
            ViewData["queryDescription"] = queryDescription;
            ViewData["FormMode"] = string.IsNullOrEmpty(queryId) ? CurrentFormMode.Insert : CurrentFormMode.Edit;
            ViewData["QueryType"] = QueryType;

            return View();
        }

        /// <summary>
        /// This method is used to gets the query info based on the given query identifier.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetQueryInfo(string queryId)
        {
            if (!this.Request.IsAjaxRequest())
            {
                return Json(new { Error = QueryBuilderResource.e_InvalidRequest });
            }

            try
            {
                var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(queryId);

                return queryDetails != null
                           ? this.Json(queryDetails.QueryInfo)
                           : this.Json(new { Error = QueryBuilderResource.e_NoQueryFound });
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return this.Json(new { Error = QueryBuilderResource.e_QueryFetch });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return this.Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to gets the query details.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetQueryDetails(string queryId)
        {
            if (!Request.IsAjaxRequest())
            {
                return Json(new { Error = QueryBuilderResource.e_InvalidRequest });
            }

            try
            {
                var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(queryId);

                if (queryDetails != null)
                {
                    var createdUser = UserDetailsProxy.GetUserDetailsByUserId(queryDetails.CreatedBy);

                    string createdBy = (createdUser != null && createdUser.MembershipDetails != null)
                        ? createdUser.MembershipDetails.UserName
                        : queryDetails.CreatedBy;

                    string createdOn = queryDetails.CreatedOn.ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat(), CultureInfo.InvariantCulture);

                    return Json(new
                    {
                        QueryId = queryDetails.Identifier,
                        Name = queryDetails.Name,
                        Description = queryDetails.Description,
                        CreatedOn = createdOn,
                        CreatedBy = createdBy,
                        ModuleId = queryDetails.QueryInfo.ModuleId,
                        PrimaryEntityId = queryDetails.QueryInfo.PrimaryEntityIdentifier
                    });
                }
                return this.Json(new { Error = QueryBuilderResource.e_NoQueryFound });
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = QueryBuilderResource.e_QueryFetch });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to get all global variables.
        /// </summary>
        /// <param name="queryType">qery type</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllGlobalVariables(QueryTypes queryType)
        {
            var globalVariables = FieldProxy.GetAllGlobalVariables(queryType);

            if (globalVariables == null || globalVariables.Count < 1)
            {
                return Json(new { Error = QueryBuilderResource.e_nilGlobalVariables });
            }

            return Json(globalVariables, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to manages the query.
        /// </summary>
        /// <param name="forms">forms.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageQuery(FormCollection forms)
        {
            LoadModules();
            ViewData["queryId"] = forms["queryId"];
            ViewData["queryName"] = string.Empty;
            ViewData["queryDescription"] = string.Empty;
            ViewData["FormMode"] = string.IsNullOrEmpty(forms["queryId"]) ? CurrentFormMode.Insert : CurrentFormMode.Edit;

            if (Request.IsAjaxRequest())
            {
                return PartialView("QueryGenerator");
            }

            return View();
        }

        /// <summary>
        /// This method is used for query relations.
        /// </summary>
        /// <param name="parentQueryId">parent query identifier.</param>
        /// <returns></returns>
        public ActionResult QueryRelations(string parentQueryId)
        {
            if (string.IsNullOrEmpty(parentQueryId))
            {
                ModelState.AddModelError("", "Invalid queryId!");
            }

            try
            {
                var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(parentQueryId);

                if (queryDetails == null)
                {
                    ModelState.AddModelError("", "Invalid queryId!");
                }
                else
                {
                    ViewData["QueryDetails"] = queryDetails;

                    var queryRelationList = QueryDetailsProxy.GetQueryRelations(parentQueryId, this.TenantId);
                    ViewData["QueryRelationList"] = queryRelationList != null ? queryRelationList.Values : null;

                    var relatedQueries = QueryDetailsProxy.GetAllQueryDetails(this.TenantId);
                    ViewData["RelatedQueriesList"] = relatedQueries != null ? relatedQueries.ToDictionary(x => x.Identifier, y => y) : null;
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("QueryRelationGrid");
            }

            return View();
        }

        /// <summary>
        /// This method is used to manage the query relation.
        /// </summary>
        /// <param name="queryRelationId">query relation identifier.</param>
        /// <param name="parentQueryId">parent query identifier.</param>
        /// <returns></returns>
        public ActionResult ManageQueryRelation(string queryRelationId, string parentQueryId)
        {
            QueryRelation relation = new QueryRelation();

            try
            {
                if (Request.HttpMethod == "POST")
                {
                    TryUpdateModel(relation, "QueryRelation");

                    if (string.IsNullOrEmpty(relation.Name))
                    {
                        ModelState.AddModelError("QueryRelation.Name", "Relation name required!");
                    }

                    if (string.IsNullOrEmpty(relation.Name))
                    {
                        ModelState.AddModelError("QueryRelation.Name", "Relation name required");
                    }

                    if (string.IsNullOrEmpty(relation.RelatedQueryId))
                    {
                        ModelState.AddModelError("QueryRelation.RelatedQueryId", "Choose a related query");
                    }

                    Request.InputStream.Position = 0;
                    var jsonStr = new StreamReader(Request.InputStream).ReadToEnd();
                    var jObj = new JavaScriptSerializer().Deserialize<IDictionary<string, object>>(jsonStr);

                    if (jObj.ContainsKey("QueryRelation.VariableMappings") && (jObj["QueryRelation.VariableMappings"] as IDictionary<string, object>) != null)
                    {
                        relation.VariableMappings = new Dictionary<string, string>();
                        foreach (var kv in jObj["QueryRelation.VariableMappings"] as IDictionary<string, object>)
                        {
                            relation.VariableMappings.Add(kv.Key, kv.Value.ToString());
                        }
                    }

                    if (ModelState.IsValid)
                    {
                        relation.TenantId = this.TenantId;
                        relation.ParentQueryId = parentQueryId;

                        if (string.IsNullOrEmpty(relation.Id))
                        {
                            QueryDetailsProxy.AddQueryRelation(relation);
                            return Json(new { Success = "Query Relation added successfully!" });
                        }
                        else
                        {
                            QueryDetailsProxy.UpdateQueryRelation(relation);
                            return Json(new { Success = "Query Relation updated successfully!" });
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(queryRelationId))
                {
                    relation = QueryDetailsProxy.GetQueryRelation(queryRelationId, this.TenantId);
                }

                var relatedQueries = QueryDetailsProxy.GetAllQueryDetails(this.TenantId);

                if (relatedQueries != null)
                {
                    relatedQueries.RemoveAll(x => x.Identifier == parentQueryId);
                }

                ViewData["RelatedQueries"] = relatedQueries;

                var queryRelations = QueryDetailsProxy.GetQueryRelations(parentQueryId, this.TenantId);

                ViewData["CanAddChildQuery"] = true;

                if (queryRelations != null && queryRelations.Count > 0)
                {
                    ViewData["CanAddChildQuery"] = !queryRelations.Any(x => x.Value.RelationType == QueryRelationType.Child);
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }

            return View(relation);
        }

        /// <summary>
        /// This method is used to get the variable mapping.
        /// </summary>
        /// <param name="queryRelationId">query relation identifier.</param>
        /// <param name="parentQueryId">parent query identifier.</param>
        /// <param name="queryId">query identifier.</param>
        /// <returns></returns>
        public ActionResult GetVariableMappings(string queryRelationId, string parentQueryId, string queryId)
        {
            if (string.IsNullOrEmpty(queryId) || string.IsNullOrEmpty(parentQueryId))
            {
                ModelState.AddModelError("", "Invalid input!");
            }

            try
            {
                if (!string.IsNullOrEmpty(queryRelationId))
                {
                    ViewData["QueryRelationDetails"] = QueryDetailsProxy.GetQueryRelation(queryRelationId, this.TenantId);
                }

                var dynVars = QueryDetailsProxy.GetDynamicVariables(queryId);
                ViewData["DynamicVariables"] = dynVars;

                if (dynVars != null && dynVars.Count > 0)
                {
                    var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(parentQueryId);
                    ViewData["ParentQueryDetails"] = queryDetails;
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to delete the query relation base don the given query relation identifier.
        /// </summary>
        /// <param name="queryRelationId">query relation identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteQueryRelation(string queryRelationId)
        {
            if (string.IsNullOrEmpty(queryRelationId))
            {
                return Json(new { Error = "Invalid input!" });
            }

            try
            {
                QueryDetailsProxy.DeleteQueryRelation(queryRelationId, this.TenantId);

                return Json(new { Success = "Query relation deleted successfully!" });
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to save the query.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <param name="queryName">query name.</param>
        /// <param name="queryDescription">query description.</param>
        /// <param name="renderMode">render mode.</param>
        /// <param name="queryInfo">query info.</param>
        /// <param name="isglobal">isGlobal (true/false)</param>
        /// <param name="queryType">type of query.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveQuery(string queryId, string queryName, string queryDescription, QueryRenderMode renderMode, QueryInfo queryInfo, bool isglobal, QueryTypes queryType)
        {
            if (string.IsNullOrEmpty(queryName))
            {
                return Json(new { Error = QueryBuilderResource.e_InvalidQueryName });
            }

            if (queryInfo == null)
            {
                return Json(new { Error = QueryBuilderResource.e_NullQueryInfo });
            }

            QueryDetails queryDetails = new QueryDetails
            {
                Identifier = queryId,
                Name = queryName,
                Description = queryDescription,
                QueryInfo = queryInfo,
                CreatedBy = UserIdentity.UserId,
                CreatedOn = DateTime.Now,
                RenderMode = renderMode,
                Status = true,
                IsGlobal = isglobal,
                QueryType = queryType
            };

            queryDetails.TenantId = this.TenantId;

            string errorMessage = string.Empty;

            try
            {
                QueryDetailsProxy.SaveQuery(queryDetails);

                if (!string.IsNullOrEmpty(queryDetails.Identifier))
                {
                    TempData["Status"] = QueryBuilderResource.s_SaveQuery;
                }

                return Json(new { QueryId = queryDetails.Identifier });
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (InvalidQueryException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_InvalidQuery;
            }
            catch (DuplicateQueryDetailsNameException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_DuplicateQueryName;
            }
            catch (QueryConfigurationException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_SaveError;

                if (ex.InnerException != null)
                {
                    errorMessage += ex.InnerException.Message;
                }
            }

            return Json(new { Error = errorMessage });
        }

        /// <summary>
        /// This method is used to copies the given query.
        /// </summary>
        /// <param name="queryId">query identifier to copy.</param>
        /// <param name="queryName">new query name.</param>
        /// <param name="queryDescription">new query description.</param>
        /// <returns></returns>
        public JsonResult CopyQuery(string queryId, string queryName, string queryDescription)
        {
            if (string.IsNullOrEmpty(queryId))
            {
                return Json(new { Error = "Select a valid Query!" });
            }

            if (string.IsNullOrEmpty(queryName))
            {
                return Json(new { Error = "Enter a valid Query Name!" });
            }

            try
            {
                // get the actual query details
                var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(queryId);

                if (queryDetails == null)
                {
                    return Json(new { Error = "Query details not found! Invalid queryId!" });
                }

                queryDetails.Identifier = string.Empty; // save as new query
                queryDetails.Name = queryName; // new name
                queryDetails.Description = queryDescription; // new descrition

                QueryDetailsProxy.SaveQuery(queryDetails); // effectively make a copy of this query
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
            catch (DuplicateQueryDetailsNameException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }


            return Json(new { Success = "Query copied successfully!" });
        }

        /// <summary>
        /// This method is used to gets the name of all entity.
        /// </summary>
        /// <param name="moduleId">module identifier.</param>
        /// <returns></returns>
        public JsonResult GetEntitiesForModuleId(string moduleId)
        {
            try
            {
                List<string> entityMetadataList = QueryDetailsProxy.GetAllViewableEntities(moduleId, this.TenantId);

                if (entityMetadataList != null && entityMetadataList.Count > 0)
                {
                    entityMetadataList.Sort();
                    return Json(entityMetadataList);
                }
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }

            return Json(new { Error = string.Format(QueryBuilderResource.e_NoModuleForEntity, moduleId) });
        }

        /// <summary>
        /// This method is used get the entity field
        /// </summary>
        /// <param name="queryId">query identifier</param>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public JsonResult GetFieldInfoForEntityId(string queryId, string entityId)
        {
            try
            {
                Dictionary<string, EntityFieldInfo> columns = null;

                if (string.IsNullOrEmpty(queryId))
                {
                    columns = QueryDetailsProxy.GetFieldInfoForEntityId(entityId, this.TenantId);
                }
                else
                {
                    columns = QueryDetailsProxy.GetFieldInfoForEntityId(queryId, entityId, this.TenantId);
                }

                if (columns != null && columns.Count > 0)
                {
                    return Json(columns.Values.Select(c =>
                        new
                        {
                            c.DisplayName,
                            c.EntityName,
                            c.FieldIdentifier,
                            c.MappedFieldName,
                            c.IsExtensionField,
                            c.IsReferenceField,
                            c.IsNullable,
                            c.MaxLength,
                            c.Type,
                            c.IsExpressionColumn,
                            c.IsVisible,
                            c.HasPrivilege
                        }));
                }
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }

            return Json(new { Error = string.Format(QueryBuilderResource.e_NoEntityFields, entityId) });
        }

        /// <summary>
        /// This method is used to exports the saved query.
        /// </summary>
        /// <param name="type">type.</param>
        /// <param name="QueryList">query list.</param>
        /// <param name="exportType">of the export.</param>
        /// <returns></returns>
        public ContentResult ExportSavedQuery(string type, string QueryList, ExportFormat exportType, List<DynamicVariable> dynamicVariables)
        {
            string errorMessage = QueryBuilderResource.e_ExportError;

            if (string.IsNullOrEmpty(QueryList))
            {
                return Content(errorMessage);
            }

            try
            {
                //ExportFormat exportType = (ExportFormat)Enum.Parse(typeof(ExportFormat), type);

                QueryDetails queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(QueryList);

                if (queryDetails.IsGlobal)
                {
                    queryDetails.TenantId = this.TenantId;
                }

                var dynamicVariablesList = PreprocessDynamicVaribles(dynamicVariables);

                System.Data.DataTable dataTable = QueryDetailsProxy.ExecuteQueryDetails(queryDetails, null, dynamicVariablesList);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";

                queryDetails.Name = queryDetails.Name.Replace(' ', '_');

                if (exportType == ExportFormat.PDF)
                {
                    Response.AddHeader("content-disposition", "attachment;filename=" + queryDetails.Name + "_" + DateTime.Now.ToShortDateString() + ".pdf");
                    Response.ContentType = "application/pdf";

                    ExportUtil.ExportPDF(dataTable, Response.OutputStream, UserIdentity.Name);
                }
                else if (exportType == ExportFormat.Excel)
                {
                    Response.AddHeader("content-disposition", "attachment;filename=" + queryDetails.Name + "_" + DateTime.Now.ToShortDateString() + ".csv");
                    Response.ContentType = "application/vnd.ms-excel";

                    dataTable.ToCSV();
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(dataTable.ToCSV());

                    Response.OutputStream.Write(bytes, 0, bytes.Length);

                    //ExportUtil.ExportExcel(dataTable, Response.OutputStream);
                }
                else if (exportType == ExportFormat.CSV)
                {
                    Response.AddHeader("content-disposition", "attachment;filename=" + queryDetails.Name + "_" + DateTime.Now.ToShortDateString() + ".csv");
                    Response.ContentType = "text/csv";

                    ExportUtil.ExportCSV(dataTable, Response.OutputStream);
                }

                Response.Flush();

                // do not generate any other content
                Response.SuppressContent = true;

                // Directs the thread to finish, bypassing additional processing
                HttpContext.ApplicationInstance.CompleteRequest();
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (InvalidQueryException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_InvalidQuery;
            }
            catch (QueryConfigurationException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_ExportError;
            }
            catch (ExportException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }

            return Content(errorMessage);
        }

        /// <summary>
        /// This method is used to exports the specified export type.
        /// </summary>
        /// <param name="exportType">type of the export.</param>
        /// <param name="queryId"> query identifier.</param>
        /// <param name="queryName">name of the query.</param>
        /// <param name="queryDescription">query description.</param>
        /// <param name="queryInfo">query info.</param>
        /// <returns></returns>
        [HttpPost]
        public ContentResult Export(ExportFormat exportType, string queryId, string queryName, string queryDescription, QueryInfo queryInfo)
        {
            string errorMessage = QueryBuilderResource.e_ExportError;

            QueryDetails queryDetails = new QueryDetails
            {
                Identifier = queryId,
                Name = queryName,
                Description = queryDescription,
                QueryInfo = queryInfo,
                TenantId = this.TenantId,
                CreatedBy = UserIdentity.UserId,
                CreatedOn = DateTime.Now,
                Status = true
            };

            try
            {
                DataTable dataTable = QueryDetailsProxy.ExecuteQueryDetails(queryDetails, null);

                queryDetails.Name = string.IsNullOrEmpty(queryDetails.Name) ? "Un_Saved_Query" : queryDetails.Name.Replace(' ', '_');

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";

                if (exportType == ExportFormat.PDF)
                {
                    Response.AddHeader("content-disposition", "attachment;filename=" + queryDetails.Name + "_" + DateTime.Now.ToShortDateString() + ".pdf");
                    Response.ContentType = "application/pdf";

                    ExportUtil.ExportPDF(dataTable, Response.OutputStream, UserIdentity.Name);
                }
                else if (exportType == ExportFormat.Excel)
                {
                    Response.AddHeader("content-disposition", "attachment;filename=" + queryDetails.Name + "_" + DateTime.Now.ToShortDateString() + ".csv");
                    Response.ContentType = "application/vnd.ms-excel";

                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(dataTable.ToCSV());

                    Response.OutputStream.Write(bytes, 0, bytes.Length);

                    //ExportUtil.ExportExcel(dataTable, Response.OutputStream);
                }
                else if (exportType == ExportFormat.CSV)
                {
                    Response.AddHeader("content-disposition", "attachment;filename=" + queryDetails.Name + "_" + DateTime.Now.ToShortDateString() + ".csv");
                    Response.ContentType = "text/csv";

                    ExportUtil.ExportCSV(dataTable, Response.OutputStream);
                }

                Response.Flush();

                // do not generate any other content
                Response.SuppressContent = true;

                // Directs the thread to finish, bypassing additional processing
                HttpContext.ApplicationInstance.CompleteRequest();
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (InvalidQueryException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_InvalidQuery;
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_InvalidQuery;
            }
            catch (QueryConfigurationException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = QueryBuilderResource.e_QueryExport;
            }
            catch (ExportException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;
            }

            return Content(errorMessage);
        }

        /// <summary>
        /// This method is used to executes the paged query.
        /// </summary>
        /// <param name="queryBuilderSearchCondition">query builder search condition.</param>
        /// <returns></returns>
        public JsonResult ExcecutePagedQuery(QueryBuilderSearchCondition queryBuilderSearchCondition, List<DynamicVariable> dynamicVariables)
        {
            QuerySearchCondition querySearchCondition = new QuerySearchCondition { SetTotalCount = true };

            if ((queryBuilderSearchCondition.iDisplayStart != null && queryBuilderSearchCondition.iDisplayLength != null))
            {
                if (queryBuilderSearchCondition.iDisplayLength.Value < 0)
                {
                    queryBuilderSearchCondition.iDisplayStart = queryBuilderSearchCondition.iDisplayLength = 0;
                }

                querySearchCondition = new QuerySearchCondition
                {
                    RecordStart = queryBuilderSearchCondition.iDisplayStart.Value + 1,
                    RecordEnd = queryBuilderSearchCondition.iDisplayStart.Value + queryBuilderSearchCondition.iDisplayLength.Value,
                    SetTotalCount = true,
                };
            }

            DataTable dataTable = null;
            int totalRecords;

            var dynamicVariablesList = PreprocessDynamicVaribles(dynamicVariables);

            if (!string.IsNullOrEmpty(queryBuilderSearchCondition.queryId) && queryBuilderSearchCondition.queryInfo == null)
            {
                dataTable = QueryDetailsProxy.ExecuteQueryById(queryBuilderSearchCondition.queryId, this.TenantId, querySearchCondition, dynamicVariablesList);
                totalRecords = querySearchCondition.TotalCount;
            }
            else
            {
                var queryDetails = new QueryDetails
                {
                    Identifier = queryBuilderSearchCondition.queryId,
                    Name = queryBuilderSearchCondition.queryName,
                    Description = queryBuilderSearchCondition.queryDescription,
                    QueryInfo = queryBuilderSearchCondition.queryInfo,
                    TenantId = this.TenantId,
                    CreatedBy = UserIdentity.UserId,
                    CreatedOn = DateTime.Now,
                    Status = true,
                    QueryType = queryBuilderSearchCondition.queryType
                };

                dataTable = QueryDetailsProxy.ExecuteQueryDetails(queryDetails, querySearchCondition, dynamicVariablesList);

                totalRecords = querySearchCondition.TotalCount;
            }

            if (dataTable == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            var objlist = (from DataRow dr in dataTable.Rows select dr.ItemArray).Cast<object>().ToList();

            ViewData["totalRecords"] = totalRecords;

            queryBuilderSearchCondition.sEcho = totalRecords / (queryBuilderSearchCondition.iDisplayStart.Value + queryBuilderSearchCondition.iDisplayLength.Value);

            var jdatatableobj = new
            {
                sEcho = queryBuilderSearchCondition.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = objlist
            };
            return Json(jdatatableobj, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to preview the query result
        /// </summary>
        /// <param name="queryId">query identifier</param>
        /// <param name="querySearchCondition">query search condition</param>
        /// <returns></returns>
        public ActionResult PreviewQueryResult(string queryId, QuerySearchCondition querySearchCondition, List<DynamicVariable> dynamicVariables, Dictionary<string, string> dynvars, QueryRenderMode renderMode = QueryRenderMode.Both, bool hideSettings = false)
        {
            if (string.IsNullOrEmpty(queryId))
            {
                ModelState.AddModelError("Error", "Invalid input!");
            }

            ViewData["queryId"] = queryId;
            ViewData["hideSettings"] = hideSettings;
            ViewData["renderMode"] = renderMode;
            DataTable dataTable = null;

            if (ModelState.IsValid)
            {
                try
                {

                    var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(queryId);

                    if (queryDetails == null)
                    {
                        ModelState.AddModelError("Error", "Query details not available!");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(queryDetails.TenantId))
                        {
                            // global query so set the current tenant to execute
                            queryDetails.TenantId = this.TenantId;
                        }

                        ViewData["queryDetails"] = queryDetails;

                        if (queryDetails.RenderMode != renderMode && queryDetails.RenderMode != QueryRenderMode.Both)
                        {
                            ModelState.AddModelError("Error", string.Format("Cannot render this query in {0} mode.", renderMode));
                        }
                        else
                        {
                            ViewData["dynamicVariables"] = QueryDetailsProxy.GetDynamicVariables(queryId);

                            if (querySearchCondition == null)
                            {
                                querySearchCondition = new QuerySearchCondition { RecordStart = 0, RecordEnd = 50 };
                            }

                            if (querySearchCondition.RecordEnd == 0)
                            {
                                querySearchCondition.RecordEnd = 50;
                            }

                            querySearchCondition.SetTotalCount = true;

                            var _dvars = PreprocessDynamicVaribles(dynamicVariables);

                            dataTable = QueryDetailsProxy.ExecuteQueryDetails(queryDetails, querySearchCondition, _dvars);

                            ViewData["totalRecords"] = querySearchCondition.TotalCount;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    ModelState.AddModelError("Error", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("QueryResult", dataTable);
            }

            return View(dataTable);
        }

        /// <summary>
        /// This method is used to executes the query.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <param name="queryName">name of the Query</param>
        /// <param name="queryDescription">query description.</param>
        /// <param name="queryInfo">query info.</param>
        /// <param name="querySearchCondition">search condition object for paging.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExecuteQuery(string queryId, string queryName, string queryDescription, QueryInfo queryInfo, bool isglobal, QueryTypes queryType, List<DynamicVariable> dynamicVariables = null, QuerySearchCondition querySearchCondition = null, QueryRenderMode renderMode = QueryRenderMode.Both)
        {
            if (queryInfo == null)
            {
                return Json(new { Error = QueryBuilderResource.e_NullQueryInfo });
            }

            ViewData["renderMode"] = renderMode;

            QueryDetails queryDetails = new QueryDetails
            {
                Identifier = queryId,
                Name = queryName,
                Description = queryDescription,
                RenderMode = renderMode,
                QueryInfo = queryInfo,
                TenantId = this.TenantId,
                CreatedBy = UserIdentity.UserId,
                CreatedOn = DateTime.Now,
                Status = true,
                IsGlobal = isglobal,
                QueryType = queryType
            };

            ViewData["queryDetails"] = queryDetails;
            DataTable dataTable = null;

            try
            {
                if (querySearchCondition == null)
                {
                    querySearchCondition = new QuerySearchCondition { RecordStart = 0, RecordEnd = 50, SetTotalCount = true };
                }
                else if (querySearchCondition.RecordEnd == 0)
                {
                    querySearchCondition.RecordEnd = 50;
                }

                querySearchCondition.SetTotalCount = true;

                var dynamicVariablesList = PreprocessDynamicVaribles(dynamicVariables);

                dataTable = QueryDetailsProxy.ExecuteQueryDetails(queryDetails, querySearchCondition, dynamicVariablesList);

                ViewData["totalRecords"] = querySearchCondition.TotalCount;
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (InvalidQueryException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", QueryBuilderResource.e_InvalidQuery);
            }
            catch (DuplicateQueryDetailsNameException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", QueryBuilderResource.e_DuplicateQueryName);
            }
            catch (QueryConfigurationException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                this.ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            catch (GlobalVariablesException globalVariablesException)
            {
                ExceptionService.HandleException(globalVariablesException, DefaultPolicy);
                this.ModelState.AddModelError("", globalVariablesException.InnerException != null ? globalVariablesException.InnerException.Message : globalVariablesException.Message);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                this.ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }

            return PartialView("QueryResult", dataTable);
        }

        /// <summary>
        /// This method is used to execute the query by query id with the dynamic variables
        /// </summary>
        /// <param name="queryId">Query Identifier</param>
        /// <param name="dynamicVariables">Dynamic Variables List</param>
        /// <param name="querySearchCondition">The Query Search Condition</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExecuteQueryById(string queryId, List<DynamicVariable> dynamicVariables, QuerySearchCondition querySearchCondition, QueryRenderMode renderMode = QueryRenderMode.Both)
        {
            if (string.IsNullOrEmpty(queryId))
            {
                ModelState.AddModelError("", QueryBuilderResource.e_InvalidQueryId);
            }

            ViewData["renderMode"] = renderMode;
            DataTable dataTable = null;

            try
            {
                var queryDetails = QueryDetailsProxy.GetQueryDetailsByQueryId(queryId);

                ViewData["queryDetails"] = queryDetails;

                if (ModelState.IsValid)
                {
                    querySearchCondition.SetTotalCount = true;
                    querySearchCondition.RecordStart = 0;
                    querySearchCondition.RecordEnd = 50;

                    var dynamicVariablesList = PreprocessDynamicVaribles(dynamicVariables);

                    if (dynamicVariablesList != null && dynamicVariablesList.Count > 0)
                    {
                        // pass the dict here
                        dataTable = QueryDetailsProxy.ExecuteQueryById(queryId, this.TenantId, querySearchCondition, dynamicVariablesList);
                    }
                    else
                    {
                        dataTable = QueryDetailsProxy.ExecuteQueryById(queryId, this.TenantId, querySearchCondition);
                    }

                    ViewData["totalRecords"] = querySearchCondition.TotalCount;
                }
            }
            catch (ArgumentException ex)
            {

                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (InvalidQueryException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", QueryBuilderResource.e_InvalidQuery);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", QueryBuilderResource.e_InvalidQuery);
            }
            catch (QueryConfigurationException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("", ex.Message);
            }

            return PartialView("QueryResult", dataTable);
        }

        /// <summary>
        /// This method is used to preprocesses the dynamic variables.
        /// </summary>
        /// <param name="dynamicVariables">list of dynamic variables.</param>
        /// <returns></returns>
        private Dictionary<string, string> PreprocessDynamicVaribles(List<DynamicVariable> dynamicVariables)
        {
            var dynamicVariablesList = new Dictionary<string, string>();

            if (dynamicVariables != null && dynamicVariables.Count > 0)
            {
                foreach (var dynamicVariable in dynamicVariables)
                {
                    if (!dynamicVariablesList.ContainsKey(dynamicVariable.Name))
                    {
                        dynamicVariablesList.Add(dynamicVariable.Name, string.IsNullOrEmpty(dynamicVariable.Value) ? string.Empty : HttpUtility.UrlDecode(dynamicVariable.Value));
                    }
                }
            }

            foreach (string key in Request.QueryString.Keys)
            {
                if (key.Contains("["))
                {
                    string name = key.Substring(key.IndexOf('[') + 1, key.IndexOf(']') - key.IndexOf('[') - 1);

                    if (!dynamicVariablesList.ContainsKey(name))
                    {
                        dynamicVariablesList.Add(name, Request.QueryString[key]);
                    }

                    ViewData["hideSettings"] = true;
                }
            }

            return dynamicVariablesList;
        }

        /// <summary>
        /// This method is used to gets the related entities for entity identifier.
        /// </summary>
        /// <param name="moduleId">module identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <returns></returns>
        public JsonResult GetRelatedEntitiesForEntityId(string moduleId, string entityId)
        {
            try
            {
                var relatedEntities = QueryDetailsProxy.GetViewableRelatedEntities(moduleId, entityId, this.TenantId);

                if (relatedEntities != null && relatedEntities.Count > 0)
                {
                    return Json(relatedEntities);
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }

            return Json(new { Error = string.Format(QueryBuilderResource.e_NilRelatedEntity, entityId) });
        }

        /// <summary>
        /// This method is used gets the field value.
        /// </summary>
        /// <param name="fieldInfo">field info.</param>
        /// <param name="q">q.</param>
        /// <returns></returns>       
        public JsonResult GetFieldValue(EntityFieldInfo fieldInfo, string q)
        {
            try
            {
                var values = FieldProxy.GetFieldValues(fieldInfo, q, this.TenantId);

                if (values != null && values.Count > 0)
                {
                    return Json(values);
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }

            var tmp = new List<string> { QueryBuilderResource.e_NoValues };
            return Json(tmp);
        }

        /// <summary>
        /// This method returns the dynamic variables in an IEnumerable format.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <returns>
        /// IEnumerable of DynamicVariable.
        /// </returns>
        /// <remarks>
        /// <see cref="DynamicVariable"/>
        /// </remarks>
        [HttpPost]
        public JsonResult GetDynamicVariables(string queryId)
        {
            try
            {
                var dynamicVariablesList = QueryDetailsProxy.GetDynamicVariables(queryId);

                if (dynamicVariablesList == null || dynamicVariablesList.Count < 1)
                {
                    return Json(new { Error = QueryBuilderResource.e_NoDynamicVariables });
                }

                List<DynamicVariable> dynamicVariableCollection = new List<DynamicVariable>();

                foreach (var dynamicVariables in dynamicVariablesList)
                {
                    DynamicVariable dynamicVariable = new DynamicVariable
                    {
                        VariableId = Guid.NewGuid().ToString(),
                        Name = dynamicVariables.Key,
                        Type = dynamicVariables.Value
                    };

                    dynamicVariableCollection.Add(dynamicVariable);
                }

                return Json(dynamicVariableCollection);
            }
            catch (QueryBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to update field privileges based on the given list of fields.
        /// </summary>
        /// <param name="fields">list of fields.</param>
        /// <returns></returns>
        public JsonResult UpdateFieldPrivileges(List<EntityFieldInfo> fields)
        {
            if (fields == null || fields.Count < 1)
            {
                return Json(new { Message = QueryBuilderResource.e_NoFields });
            }

            return Json(QueryDetailsProxy.UpdateFieldPrivileges(fields));
        }

        #endregion

        #region Save as chart and render chart

        /// <summary>
        /// This method is used to save query as chart source.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <param name="chartName">chart name.</param>
        /// <param name="chartOptions">chart options.</param>
        /// <returns></returns>
        public JsonResult SaveAsChart(string queryId, string chartName, string chartOptions)
        {
            if (string.IsNullOrEmpty(queryId) || string.IsNullOrEmpty(chartName) || string.IsNullOrEmpty(chartOptions))
            {
                return Json(new { Error = "Invalid input!" });
            }

            try
            {
                ChartDetailsProxy.SaveQueryAsChart(queryId, chartName, chartOptions, this.TenantId, UserIdentity.UserId);
            }
            catch (DuplicateChartNameException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = "Duplicate chart name! Please give another name." });
            }
            catch (ChartBuilderException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }

            return Json(new { Success = true });
        }

        /// <summary>
        /// This method is used to preview the rendered chart.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        /// <param name="type">type.</param>
        /// <param name="theme">theme.</param>
        /// <returns></returns>
        public ActionResult PreviewRenderChart(string queryId, string type = "line", string theme = "default")
        {
            ViewData["queryId"] = queryId;
            ViewData["chart.type"] = type;
            ViewData["chart.theme"] = theme;
            return View();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Loads the modules for the tenant bought license packages.
        /// </summary>
        private void LoadModules(string moduleId = null)
        {
            List<SelectListItem> lstModule = new List<SelectListItem>();

            try
            {
                Dictionary<string, Module> modules = EntityProxy.GetLicensePackageModulesForTenant(this.TenantId);

                if (modules == null || modules.Count == 0)
                {
                    ModelState.AddModelError("Error", QueryBuilderResource.e_LicenseModules);
                }
                else
                {
                    var sortedModules = modules.Values.ToList();
                    sortedModules.Sort(delegate(Module p1, Module p2) { return p1.ModuleName.CompareTo(p2.ModuleName); });

                    foreach (var item in sortedModules)
                    {
                        lstModule.Add(new SelectListItem
                        {
                            Text = item.ModuleName,
                            Value = item.ModuleCode,
                            Selected = !string.IsNullOrEmpty(moduleId) && moduleId.Equals(item.ModuleCode) ? true : false
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
                ExceptionService.HandleException(ex, DefaultPolicy);
            }

            lstModule.Insert(0, new SelectListItem
            {
                Value = "-1",
                Text = "--Select a module--"
            });

            ViewData["ModuleList"] = lstModule;
        }

        #endregion
    }
}
