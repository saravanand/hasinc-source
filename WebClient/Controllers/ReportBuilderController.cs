using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using CelloSaaS.ChartBuilder.Model;
using CelloSaaS.ChartBuilder.ServiceContracts;
using CelloSaaS.Library;
using CelloSaaS.QueryBuilderLibrary.ServiceProxies;
using CelloSaaS.ReportBuilder.Model;
using CelloSaaS.ReportBuilder.Model.ViewModels;
using CelloSaaS.ReportBuilder.ServiceContracts;
using CelloSaaS.ReportBuilder.ServiceProxies;
using CelloSaaS.Reporting.DataSources.Model;
using CelloSaaS.Reporting.DataSources.Model.ViewModel;
using CelloSaaS.Reporting.DataSources.ServiceContracts;
using CelloSaaS.Reporting.DataSources.ServiceProxies;
using Resources;

namespace CelloSaaSApplication.Controllers
{
    using System.IO;
    using CelloSaaS.ChartBuilder;
    using CelloSaaS.Reporting.DataSources.Helpers;
    using CelloSaaS.View;
    using CelloSaaSApplication.Models;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System.Globalization;
    using CelloSaaS.ChartBuilder.ServiceProxies;

    /// <summary>
    /// This class is responsible for report building.
    /// </summary>
    public class ReportBuilderController : CelloController
    {
        private string TenantId = TenantContext.GetTenantId("_ReportBuilder");


        #region Private Constants
        /// <summary>
        /// 
        /// </summary>
        private const string DefaultPolicy = "ReportingExceptionLogger";

        /// <summary>
        /// Gets the error status.
        /// </summary>
        private static string ErrorStatus { get { return "Error"; } }

        /// <summary>
        /// Gets the success status.
        /// </summary>
        private static string SuccessStatus { get { return "Success"; } }

        /// <summary>
        /// 
        /// </summary>
        private const string TableReportObject = "A0DFECED-33F3-4DE1-91C4-3203E5EB2194";
        /// <summary>
        /// 
        /// </summary>
        private const string TextReportObject = "AE02E1E0-F37A-41B9-8098-64C9E71360C1";
        /// <summary>
        /// 
        /// </summary>
        private const string ImageReportObject = "5A3B3969-F960-4B1F-B9DD-9DE2A776AEA5";
        /// <summary>
        /// 
        /// </summary>
        private const string ChartReportObject = "35F17E82-DC46-491F-A6E2-A874258DC135";

        #endregion

        /// <summary>
        /// This method is used to add a report.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult AddReport()
        {
            return this.PartialView("ReportDetailPartialView");
        }

        /// <summary>
        /// This method is used to get manage reports view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                if (TempData["Error"] != null)
                {
                    ModelState.AddModelError("Error", TempData["Error"].ToString());
                }
                if (TempData["Success"] != null)
                {
                    ModelState.AddModelError("Success", TempData["Success"].ToString());
                }
                Dictionary<string, Report> dicReportList = ReportProxy.GetAllReports(this.TenantId);

                ViewData["ReportList"] = (dicReportList != null && dicReportList.Count > 0) ? dicReportList.Values.ToList() : null;

                return View();
            }
            catch (ReportBuilderException ex)
            {
                ModelState.AddModelError("", ex.Message);
                ExceptionService.HandleException(ex, DefaultPolicy);
            }
            return View();
        }

        /// <summary>
        /// This method is used to get the report designer view based on the given report identifier.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        public ActionResult ReportDesigner(string reportId)
        {
            if (string.IsNullOrEmpty(reportId))
            {
                return RedirectToAction("Index");
            }

            ViewData["report"] = ReportProxy.GetShallowReportDetail(reportId, this.TenantId);
            ViewData["reportId"] = reportId;

            return View();
        }

        /// <summary>
        /// This method is used to gets the data source based on the given data source identifier.
        /// </summary>
        /// <param name="dataSourceId">data source identifier.</param>
        /// <returns></returns>
        public JsonResult GetDataSource(string dataSourceId)
        {
            try
            {
                DataSource dataSource = (DataSource)DataSourceProxy.GetSource(dataSourceId, this.TenantId);
                List<dynamic> dataSources = new List<dynamic>();

                if (dataSource != null)
                {
                    dataSources.Add(dataSource);
                }

                SourceContentViewModel sourceContent = null;

                if (dataSource.DataSourceContent != null && !string.IsNullOrEmpty(dataSource.DataSourceContent.Content))
                {
                    sourceContent = (SourceContentViewModel)SerializationHelper.DeSerializeObject<SourceContentViewModel>(dataSource.DataSourceContent.Content);
                    dataSources.Add(sourceContent);
                }
                return Json(dataSources, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Error = Resources.ReportBuilderResource.e_GetDataSource }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This method is used to gets all data sources.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllDataSources()
        {
            try
            {
                Dictionary<string, IObjectSource> dataSources = DataSourceProxy.GetAllSources(this.TenantId);

                var dataSourceList = from dataSource in dataSources
                                     select new { dataSource.Key, SourceName = dataSource.Value.Name };

                return Json(dataSourceList.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Error = Resources.ReportBuilderResource.e_FetchDataSource }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This method is used to gets the table source.
        /// </summary>
        /// <param name="sourceId">SRC identifier.</param>
        /// <returns></returns>
        public JsonResult GetTableSource(string sourceId)
        {
            try
            {
                TableSource tableSource = (TableSource)TableSourceProxy.GetSource(sourceId, this.TenantId);

                SourceContentViewModel sourceContent = null;

                if (tableSource.DataSourceValues != null && tableSource.DataSourceValues.DataSourceContent != null && !string.IsNullOrEmpty(tableSource.DataSourceValues.DataSourceContent.Content))
                {
                    sourceContent = (SourceContentViewModel)SerializationHelper.DeSerializeObject<SourceContentViewModel>(tableSource.DataSourceValues.DataSourceContent.Content);
                }

                dynamic[] tableSourceContents = new dynamic[]
                {
                    tableSource,
                    sourceContent
                };

                return Json(tableSourceContents, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Error = Resources.ReportBuilderResource.e_FetchTableSource }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This method is used to gets the columns.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <param name="reportObjectId">report object identifier.</param>
        /// <param name="sourceId">source identifier.</param>
        /// <param name="reportObjectType">type of the report object.</param>
        /// <returns></returns>
        public JsonResult GetColumns(string reportId, string reportObjectId, string sourceId, string reportObjectType)
        {
            if (string.IsNullOrEmpty(reportObjectId))
            {
                return Json(new { Error = ReportBuilderResource.e_ReportIdEmpty }, JsonRequestBehavior.DenyGet);
            }

            if (string.IsNullOrEmpty(sourceId))
            {
                return Json(new { Error = ReportBuilderResource.e_ReportSourceIdMissing }, JsonRequestBehavior.DenyGet);
            }

            if (string.IsNullOrEmpty(reportObjectType))
            {
                return Json(new { Error = ReportBuilderResource.e_ReportObjectTypeIdMissing }, JsonRequestBehavior.DenyGet);
            }

            SourceContentViewModel sourceContent = null;
            DataSource dataSource = null;
            try
            {
                // check if there is a column information available for this reportobject.
                var existingColumnMapping = TableSourceProxy.GetTableResultColumns(reportId, reportObjectId, sourceId, this.TenantId);
                if (existingColumnMapping != null || existingColumnMapping.Count > 0)
                {
                    return Json(existingColumnMapping.Values.ToList(), JsonRequestBehavior.DenyGet);
                }
            }
            catch (DataSourceException dataSourceException)
            {
                return Json(new { Error = dataSourceException.Message }, JsonRequestBehavior.DenyGet);
            }

            switch (reportObjectType.ToUpperInvariant())
            {
                case TableReportObject:
                    TableSource ts = (TableSource)TableSourceProxy.GetSource(sourceId, this.TenantId);
                    if (ts != null && ts.DataSourceValues != null && !string.IsNullOrEmpty(ts.DataSourceValues.SourceContentId))
                    {
                        dataSource = ts.DataSourceValues;
                    }
                    else
                    {
                        return Json(new { Error = TableSourceResources.e_InvalidSource }, JsonRequestBehavior.DenyGet);
                    }
                    break;
                case ChartReportObject:
                    ChartDetails chartDetail = ChartDetailsProxy.GetChartDetails(sourceId, this.TenantId);

                    if (chartDetail != null && !string.IsNullOrEmpty(chartDetail.ChartSourceId))
                    {
                        dataSource = (DataSource)DataSourceProxy.GetSource(chartDetail.ChartSourceId, this.TenantId);
                    }
                    else
                    {
                        return Json(new { Error = TableSourceResources.e_InvalidSource }, JsonRequestBehavior.DenyGet);
                    }

                    break;
                default:
                    return Json(new { Error = TableSourceResources.e_InvalidSource }, JsonRequestBehavior.DenyGet);
            }

            List<string> columnNames = new List<string>();

            // in case of a txt src or an sp
            if (dataSource != null && dataSource.DataSourceContent != null && !string.IsNullOrEmpty(dataSource.DataSourceContent.Content))
            {
                sourceContent = (SourceContentViewModel)SerializationHelper.DeSerializeObject<SourceContentViewModel>(dataSource.DataSourceContent.Content);

                var sourceParameters = (from p in sourceContent.parameters
                                        select new { p.ParamName, p.ParamValue }).ToDictionary(t => t.ParamName, t => t.ParamValue);

                switch (sourceContent.SourceContentTypes)
                {
                    case DataSourceConstants.BuiltInQuerySource:
                        columnNames = QueryMetaDataProxy.GetColumnMetaDataForBuiltInQuery(sourceContent.SourceContent_Id, CelloSaaS.Library.DAL.Constants.ApplicationConnectionString, this.TenantId);
                        break;
                    case DataSourceConstants.TextQuerySource:
                        columnNames = QueryMetaDataProxy.GetColumnMetaDataForQuery(sourceContent.SourceContent, CelloSaaS.Library.DAL.Constants.ApplicationConnectionString, sourceParameters);
                        break;
                    case DataSourceConstants.StoredProcedureSource:
                        columnNames = QueryMetaDataProxy.GetColumnMetaDataForSP(sourceContent.SourceContent, CelloSaaS.Library.DAL.Constants.ApplicationConnectionString, sourceParameters);
                        break;
                    default:
                        break;
                }
            }
            else if (dataSource != null && !string.IsNullOrEmpty(dataSource.SourceContentId) && !string.IsNullOrEmpty(dataSource.SourceTypeId))
            {
                switch (dataSource.SourceContentId.ToUpperInvariant())
                {
                    case DataSourceConstants.BuiltInQuerySource:
                        columnNames = QueryMetaDataProxy.GetColumnMetaDataForBuiltInQuery(dataSource.SourceContentId, CelloSaaS.Library.DAL.Constants.ApplicationConnectionString, this.TenantId);
                        break;
                    default:
                        break;
                }
            }

            List<TableResultColumns> columns = new List<TableResultColumns>();

            if (columnNames.Count > 0)
            {
                int ordinal = 0;
                columns = (from col in columnNames
                           select new TableResultColumns
                           {
                               ReportObjectId = reportObjectId,
                               Column = col,
                               DisplayName = col,
                               Ordinal = ordinal++
                           }).ToList();
            }

            if (columns != null && columns.Count > 0)
            {
                return Json(columns, JsonRequestBehavior.DenyGet);
            }
            return Json(new { Error = ReportBuilderResource.e_GetColumn }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to manages the report.
        /// </summary>
        /// <param name="report">report.</param>
        /// <returns></returns>
        public JsonResult ManageReport(Report report)
        {
            try
            {
                report.TenantId = this.TenantId;

                if (!string.IsNullOrEmpty(report.ReportId))
                {
                    report.UpdatedBy = UserIdentity.UserId;
                    report.UpdatedOn = DateTime.Now;

                    int updateStatus = ReportProxy.UpdateReport(report);

                    return (updateStatus > 0)
                            ? Json(new { Success = ReportBuilderResource.s_ReportUpdate }, JsonRequestBehavior.DenyGet)
                            : Json(new { Error = ReportBuilderResource.e_ReportSave }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    report.CreatedBy = UserIdentity.UserId;
                    report.CreatedOn = DateTime.Now;
                    report.Status = true;

                    var status = ReportProxy.AddReport(report);

                    return (string.IsNullOrEmpty(status))
                            ? Json(new { Status = ReportBuilderResource.e_ReportSave }, JsonRequestBehavior.DenyGet)
                            : Json(new { ReportId = status }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (DuplicateReportNameException duplicateNameException)
            {
                return Json(new { Error = duplicateNameException.Message }, JsonRequestBehavior.DenyGet);
            }
            catch (ReportBuilderException reportBuilderException)
            {
                return Json(new { Error = reportBuilderException.Message }, JsonRequestBehavior.DenyGet);
            }
        }

        /// <summary>
        /// This method is used to updates the report object ordinal.
        /// </summary>
        /// <param name="reportObjectPositions">report object positions.</param>
        /// <returns></returns>
        public JsonResult UpdateReportObjectOrdinal(List<ReportObjectPositions> reportObjectPositions)
        {
            if (reportObjectPositions == null || reportObjectPositions.Count < 0)
            {
                return Json(new { Error = ReportBuilderResource.e_ReportObjectPositionsEmpty }, JsonRequestBehavior.DenyGet);
            }

            Dictionary<string, int> reportObjectOrdinals = new Dictionary<string, int>();

            foreach (var reportObj in reportObjectPositions)
            {
                if (!string.IsNullOrEmpty(reportObj.ReportObjectId) && !reportObjectOrdinals.ContainsKey(reportObj.ReportObjectId))
                {
                    reportObjectOrdinals.Add(reportObj.ReportObjectId, int.Parse(reportObj.Ordinal, CultureInfo.InvariantCulture));
                }
            }

            try
            {
                string reportId = reportObjectPositions[0].ReportId;
                int status = ReportProxy.UpdateReportObjectOrdinals(reportId, this.TenantId, reportObjectOrdinals);

                return Json(new { Status = ReportBuilderResource.s_ReportOrdinalUpdate }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                return Json(new { Error = ReportBuilderResource.e_ReportOrdinalUpdate }, JsonRequestBehavior.DenyGet);
            }
        }

        /// <summary>
        /// This method is used to manages the report object.
        /// </summary>
        /// <param name="reportObjectViewModel">report object view model.</param>
        /// <returns></returns>
        public JsonResult ManageReportObject(ReportObjectViewModel reportObjectViewModel)
        {
            if (reportObjectViewModel == null)
            {
                return Json(new { Error = ReportBuilderResource.e_MandatoryReportObject }, JsonRequestBehavior.DenyGet);
            }

            if (!string.IsNullOrEmpty(reportObjectViewModel.ObjectId) && string.IsNullOrEmpty(reportObjectViewModel.ObjectName))
            {
                return Json(new { Error = ReportBuilderResource.e_MandatoryReportObjectName }, JsonRequestBehavior.DenyGet);
            }

            ReportObject reportObject = new ReportObject
            {
                TenantId = this.TenantId,
                Status = false,
                Ordinal = reportObjectViewModel.Ordinal,
                ReportId = reportObjectViewModel.ReportId,
                ReportObjectId = reportObjectViewModel.ObjectId,
                RowsPerPage = reportObjectViewModel.RowsPerPage,
                TypeId = reportObjectViewModel.TypeId,
                SourceId = reportObjectViewModel.SourceId,
                Name = reportObjectViewModel.ObjectName
            };

            if (reportObjectViewModel.status == 1)
            {
                reportObject.Status = true;
            }

            if (string.IsNullOrEmpty(reportObjectViewModel.TypeId))
            {
                return Json(new { Error = ReportBuilderResource.e_MandatoryTypeId }, JsonRequestBehavior.DenyGet);
            }

            if (!string.IsNullOrEmpty(reportObjectViewModel.SourceId))
            {
                switch (reportObjectViewModel.TypeId.ToUpperInvariant())
                {
                    case ReportObjectTypeConstants.TableObjectType:
                        reportObject.SourceObject = new TableSource
                        {
                            Id = reportObjectViewModel.SourceId,
                            Name = reportObjectViewModel.SourceName,
                            Description = reportObjectViewModel.SourceDesc,
                            DataSourceValues = null
                        };
                        break;
                    case ReportObjectTypeConstants.ImageObjectType:
                        reportObject.SourceObject = new ImageSource
                        {
                            Id = reportObjectViewModel.SourceId,
                            Name = reportObjectViewModel.SourceName
                        };
                        break;
                    case ReportObjectTypeConstants.TextObjectType:
                        reportObject.SourceObject = new TextSource
                        {
                            Id = reportObjectViewModel.SourceId,
                            Name = reportObjectViewModel.SourceName
                        };
                        break;
                    default:
                        reportObject.SourceObject = null;
                        break;
                }

                if (reportObject.SourceObject != null)
                {
                    reportObject.SourceObject.TenantId = this.TenantId;
                    reportObject.SourceObject.Status = true;
                }
            }

            string resultId = string.Empty;

            // insert report object here
            if (string.IsNullOrEmpty(reportObject.ReportObjectId))
            {
                reportObject.CreatedBy = reportObject.CreatedBy = UserIdentity.UserId;
                reportObject.CreatedOn = reportObject.CreatedOn = DateTime.Now;
                reportObject.Status = reportObject.Status = true;

                List<string> reportObjectIds = ReportProxy.AddReportObject(new List<ReportObject> { reportObject });
                if (reportObjectIds != null && reportObjectIds.Count > 0)
                {
                    resultId = reportObjectIds[0];
                }
            }
            else
            {
                reportObject.UpdatedBy = reportObject.UpdatedBy = UserIdentity.UserId;
                reportObject.UpdatedOn = reportObject.UpdatedOn = DateTime.Now;

                if (string.IsNullOrEmpty(reportObject.SourceId))
                {
                    return Json(new { Error = ReportBuilderResource.e_UpdateReportObject }, JsonRequestBehavior.DenyGet);
                }

                var status = ReportProxy.UpdateReportObjects(new Dictionary<string, ReportObject> { { reportObject.ReportObjectId, reportObject } });
                if (status > 0)
                {
                    resultId = reportObject.ReportObjectId;
                }
            }
            // get the inserted report object's id here and return that 
            return Json(new { ReportObjectId = resultId }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to saves the table result columns.
        /// </summary>
        /// <param name="tableSourceColumns">table source columns.</param>
        /// <returns></returns>
        public JsonResult SaveTableResultColumns(List<TableResultColumns> tableSourceColumns)
        {
            if (tableSourceColumns == null || tableSourceColumns.Count < 1)
            {
                return Json(new { ColumnsSaved = -1 }, JsonRequestBehavior.DenyGet);
            }

            Dictionary<string, TableResultColumns> tableResultColumns = new Dictionary<string, TableResultColumns>();

            foreach (TableResultColumns tableResultColumn in tableSourceColumns)
            {
                tableResultColumn.TenantId = this.TenantId;

                if (tableResultColumn.Ordinal < 0)
                {
                    tableResultColumn.Ordinal = 0;
                }

                if (string.IsNullOrEmpty(tableResultColumn.ColumnsId))
                {
                    tableResultColumn.CreatedBy = UserIdentity.UserId;
                    tableResultColumn.CreatedOn = DateTime.Now;
                    tableResultColumn.Status = true;
                    tableResultColumns.Add(Guid.NewGuid().ToString(), tableResultColumn);
                }
                else
                {
                    tableResultColumn.UpdatedBy = UserIdentity.UserId;
                    tableResultColumn.UpdatedOn = DateTime.Now;
                    tableResultColumns.Add(tableResultColumn.ColumnsId, tableResultColumn);
                }
            }
            try
            {
                var columnsSaved = TableSourceProxy.UpdateTableSourceColumns(tableResultColumns, this.TenantId);
                return Json(new { ColumnsSaved = columnsSaved }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                return Json(new { ColumnsSaved = -1 }, JsonRequestBehavior.DenyGet);
            }
        }

        /// <summary>
        /// This method is used to deletes the report object.
        /// </summary>
        /// <param name="reportObjectId">report object identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteReportObject(string reportObjectId)
        {
            try
            {
                if (ReportProxy.DeleteReportObject(reportObjectId, this.TenantId) > 0)
                {
                    return Json(new { Success = SuccessStatus }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception)
            {
                return Json(new { Error = ErrorStatus }, JsonRequestBehavior.DenyGet);
            }
            return Json(new { Error = ErrorStatus }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to gets all reports by source identifier.
        /// </summary>
        /// <param name="sourceId">source identifier.</param>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        public JsonResult GetAllReportsBySourceId(string sourceId, string reportId)
        {
            if (string.IsNullOrEmpty(sourceId) || string.IsNullOrEmpty(reportId))
            {
                return Json(new { Error = ReportBuilderResource.e_SourceIdEmpty }, JsonRequestBehavior.DenyGet);
            }

            Dictionary<string, Report> sourceBasedReports = null;

            try
            {
                sourceBasedReports = ReportProxy.GetAllReportsBySourceId(this.TenantId, sourceId);
            }
            catch (Exception)
            {
                return null;
            }

            if (sourceBasedReports == null || sourceBasedReports.Count < 1)
            {
                return null;
            }

            if (sourceBasedReports.ContainsKey(reportId))
            {
                sourceBasedReports.Remove(reportId);
            }

            if (sourceBasedReports.Count > 0)
            {
                var reportData = from report in sourceBasedReports
                                 select new Report
                                 {
                                     ReportId = report.Key,
                                     Name = report.Value.Name,
                                     Description = report.Value.Description
                                 };

                return Json(reportData);
            }
            return Json(new { Error = ReportBuilderResource.e_SourceIdEmpty }, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to gets the report.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetReport(string reportId)
        {
            if (string.IsNullOrEmpty(reportId))
            {
                return null;
            }

            Report report = null;

            try
            {
                report = ReportProxy.GetReport(reportId, this.TenantId);
            }
            catch (DataSourceException dataSourceException)
            {
                return Json("Error" + dataSourceException.Message);
            }

            if (report == null || report.ReportObjects == null || report.ReportObjects.Count <= 0)
            {
                return this.Json(report, JsonRequestBehavior.DenyGet);
            }

            foreach (var reportObj in report.ReportObjects)
            {
                if (string.IsNullOrEmpty(reportObj.Value.SourceId))
                {
                    continue;
                }
                switch (reportObj.Value.TypeId.ToUpperInvariant())
                {
                    case ReportObjectTypeConstants.TableObjectType:
                        reportObj.Value.SourceObject = TableSourceProxy.GetSource(reportObj.Value.SourceId, this.TenantId);
                        break;
                    case ReportObjectTypeConstants.TextObjectType:
                        reportObj.Value.SourceObject = TextSourceProxy.GetSource(reportObj.Value.SourceId, this.TenantId);
                        break;
                    case ReportObjectTypeConstants.ChartObjectType:
                        reportObj.Value.SourceObject = null;
                        break;
                    case ReportObjectTypeConstants.ImageObjectType:
                        reportObj.Value.SourceObject = null;
                        break;
                }
            }

            return this.Json(report, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to deletes the report.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteReport(string reportId)
        {
            try
            {
                if (string.IsNullOrEmpty(reportId))
                {
                    TempData["Error"] = "Report Identifier cannot be null or empty";
                }

                if (ReportProxy.DeleteReport(reportId, this.TenantId) > 0)
                {
                    TempData["Success"] = "Report Deletion Successful";
                }
            }
            catch (Exception exception)
            {
                TempData["Error"] = exception.Message;
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to reports the viewer.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        public ActionResult ReportViewer(string reportId)
        {
            ViewData["reportId"] = reportId;
            return View();
        }

        /// <summary>
        /// This method is used to executes the report.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <param name="reportObjectId">report object identifier.</param>
        /// <param name="reportObjectTypeId">report object type identifier.</param>
        /// <param name="reportObjectSourceId">report object source identifier.</param>
        /// <param name="rowsPerPage">rows per page.</param>
        /// <returns></returns>
        public JsonResult ExecuteReport(string reportId, string reportObjectId, string reportObjectTypeId, string reportObjectSourceId, int rowsPerPage)
        {
            try
            {
                switch (reportObjectTypeId.ToUpperInvariant())
                {
                    case ReportObjectTypeConstants.TableObjectType:
                        var dt = TableSourceProxy.ExecuteSource(reportObjectSourceId, reportId, reportObjectId, this.TenantId, 0, rowsPerPage);
                        return Json(GetMarkUpForDataTable(dt, reportId, reportObjectId, reportObjectSourceId, reportObjectTypeId).ToString());
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                return Json(null);
            }

            return Json(null);
        }

        /// <summary>
        /// This method is used to get dynamic variable from the view model.
        /// </summary>
        /// <param name="dynamicVariables">list of dynamic variables</param>
        /// <returns></returns>
        private static Dictionary<string, string> GetDynamicVariablesFromViewModel(List<DynamicVariable> dynamicVariables)
        {
            return (from dv in dynamicVariables
                    select new { Key = dv.Name, Value = dv.Value }).ToDictionary(t => t.Key, t => t.Value);
        }

        /// <summary>
        /// This method is used to execute the report object.
        /// </summary>
        /// <param name="reportObjectModel">report object model</param>
        /// <returns></returns>
        public JsonResult ExecuteReportObject(ReportObjectModel reportObjectModel)
        {
            try
            {
                switch (reportObjectModel.TypeId.ToUpperInvariant())
                {
                    case ReportObjectTypeConstants.TableObjectType:

                        DataTable dataTable = null;

                        if (reportObjectModel.DynamicVariables != null && reportObjectModel.DynamicVariables.Count > 0)
                        {
                            var dynamicVariables = GetDynamicVariablesFromViewModel(reportObjectModel.DynamicVariables);
                            dataTable = TableSourceProxy.ExecuteSource(reportObjectModel.SourceId, reportObjectModel.ReportId, reportObjectModel.ObjectId, this.TenantId, 0, reportObjectModel.RowsPerPage, dynamicVariables);
                        }
                        else
                        {
                            dataTable = TableSourceProxy.ExecuteSource(reportObjectModel.SourceId, reportObjectModel.ReportId, reportObjectModel.ObjectId, this.TenantId, 0, reportObjectModel.RowsPerPage);
                        }
                        return Json(GetMarkUpForDataTable(dataTable, reportObjectModel.ReportId, reportObjectModel.ObjectId, reportObjectModel.SourceId, reportObjectModel.TypeId).ToString(), JsonRequestBehavior.DenyGet);
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.DenyGet);
            }

            return Json(null, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to exports the report as PDF.
        /// </summary>
        /// <param name="reportViewModel">report view model.</param>
        /// <returns>
        /// PDF File as FileResult
        /// </returns>
        public FileResult ExportReportAsPdf(ReportViewModel reportViewModel)
        {
            string fileName = "InvalidReport" + DateTime.Now.Ticks + ".pdf";

            var document = new Document();

            if (reportViewModel == null || reportViewModel.ReportObjects == null || reportViewModel.ReportObjects.Count < 1)
            {
                document.Open();
                document.NewPage();
                document.Add(new Paragraph(ReportBuilderResource.e_InvalidReportOrObject));
                document.Close();
                return File(Server.MapPath("~/" + fileName), "application/pdf", fileName);
            }


            int pageCount = 0;

            bool exceptionalState = false;

            try
            {
                fileName = Regex.Replace(reportViewModel.Name, @"[\s*]", "_") + DateTime.Now.Ticks + ".pdf";

                PdfWriter pdfWriter = PdfWriter.GetInstance(document, new FileStream(Server.MapPath("~/" + fileName), FileMode.Create));
                pdfWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                CelloSaaS.Util.Library.Export.TwoColumnHeaderFooter pageEventHandler = new CelloSaaS.Util.Library.Export.TwoColumnHeaderFooter();
                pdfWriter.PageEvent = pageEventHandler;

                // Define the page header
                pageEventHandler.Title = reportViewModel.Name;
                pageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 6, Font.BOLD);
            }
            catch (Exception exception)
            {
                document.Open();
                document.NewPage();
                document.Add(new Paragraph(exception.Message));
                exceptionalState = true;
            }

            if (exceptionalState)
            {
                document.Close();
                return File(Server.MapPath("~/" + fileName), "application/pdf", fileName);
            }

            foreach (var reportObject in reportViewModel.ReportObjects)
            {
                if (string.IsNullOrEmpty(reportObject.SourceId))
                {
                    document.Open();
                    document.Add(new Paragraph(ReportBuilderResource.e_InvalidReportObject));
                    continue;
                }

                Dictionary<string, string> dynamicVariables = null;

                if (reportObject.DynamicVariables != null && reportObject.DynamicVariables.Count > 0)
                {
                    dynamicVariables = GetDynamicVariablesFromViewModel(reportObject.DynamicVariables);
                }

                switch (reportObject.TypeId.ToUpperInvariant())
                {
                    case ReportObjectTypeConstants.TableObjectType:
                        if (pageCount > 0)
                        {
                            document.SetPageSize(PageSize.A4);
                        }
                        try
                        {
                            var dataTable = TableSourceProxy.ExecuteSource(reportObject.SourceId, reportViewModel.ReportId, reportObject.ObjectId, this.TenantId, 0, 0, dynamicVariables);
                            document = CelloSaaS.Util.Library.Export.ExportUtil.GetPdfDocumentForDataTable(document, dataTable, null, UserIdentity.LoggedInUserName, reportObject.Name);
                        }
                        catch (Exception exception)
                        {
                            document.Open();
                            document.Add(new Paragraph(exception.Message));
                            continue;
                        }
                        break;
                    case ReportObjectTypeConstants.ChartObjectType:
                        try
                        {
                            var chartBytes = ChartUtility.GetChartAsBytes(reportObject.SourceId, this.TenantId, dynamicVariables);
                            var chartImage = Image.GetInstance(chartBytes);

                            chartImage.Alignment = Image.ALIGN_CENTER;

                            document.SetPageSize(new Rectangle(chartImage.Width + 200f, chartImage.Height + 250f));

                            document.Open();

                            BaseFont baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                            Font font = new Font(baseFont, 16, Font.BOLD);
                            document.Add(new Paragraph(reportObject.Name, font));
                            document.Add(chartImage);
                        }
                        catch (Exception exception)
                        {
                            document.Open();
                            document.Add(new Paragraph(exception.Message));
                            continue;
                        }
                        break;
                }
                pageCount++;

                if (pageCount < reportViewModel.ReportObjects.Count)
                {
                    document.NewPage();
                }
            }
            document.Close();
            return File(Server.MapPath("~/" + fileName), "application/pdf", fileName);
        }

        /// <summary>
        /// This method is used to exports the report.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        public FileResult ExportReport(string reportId)
        {
            Report report = null;

            var document = new Document();

            int pageCount = 0;

            bool exceptionalState = false;

            string fileName = string.Empty;

            try
            {
                report = ReportProxy.GetReport(reportId, this.TenantId);

                fileName = report.Name.Replace(" ", "_") + DateTime.Now.Ticks + ".pdf";

                PdfWriter pdfWriter = PdfWriter.GetInstance(document, new FileStream(Server.MapPath("~/" + fileName), FileMode.Create));
                pdfWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                CelloSaaS.Util.Library.Export.TwoColumnHeaderFooter pageEventHandler = new CelloSaaS.Util.Library.Export.TwoColumnHeaderFooter();
                pdfWriter.PageEvent = pageEventHandler;

                // Define the page header
                pageEventHandler.Title = report.Name;
                pageEventHandler.HeaderFont = FontFactory.GetFont(BaseFont.COURIER_BOLD, 6, Font.BOLD);
            }
            catch (Exception exception)
            {
                document.Open();
                document.NewPage();
                document.Add(new Paragraph(exception.Message));
                exceptionalState = true;
            }

            if (report == null)
            {
                document.Open();
                document.NewPage();
                document.Add(new Paragraph(ReportBuilderResource.e_InvalidReport));
                exceptionalState = true;
            }

            if (report != null && report.ReportObjects.Count < 1)
            {
                document.Open();
                document.NewPage();
                document.Add(new Paragraph("No Valid ReportObjects in the Current Report"));
                exceptionalState = true;
            }

            if (exceptionalState)
            {
                document.Close();
                return File(Server.MapPath("~/" + fileName), "application/pdf", fileName);
            }

            foreach (var reportObject in report.ReportObjects)
            {
                if (string.IsNullOrEmpty(reportObject.Value.SourceId))
                {
                    document.Open();
                    document.Add(new Paragraph(ReportBuilderResource.e_NoValidObject));
                    continue;
                }

                switch (reportObject.Value.TypeId.ToUpperInvariant())
                {
                    case ReportObjectTypeConstants.TableObjectType:
                        if (pageCount > 0)
                        {
                            document.SetPageSize(PageSize.A4);
                        }
                        try
                        {
                            var dt = TableSourceProxy.ExecuteSource(reportObject.Value.SourceId, reportId, reportObject.Key, this.TenantId, 0, 0);
                            document = CelloSaaS.Util.Library.Export.ExportUtil.GetPdfDocumentForDataTable(document, dt, null, UserIdentity.LoggedInUserName, reportObject.Value.Name);
                        }
                        catch (Exception exception)
                        {
                            document.Open();
                            document.Add(new Paragraph(exception.Message));
                            continue;
                        }
                        break;
                    case ReportObjectTypeConstants.ChartObjectType:
                        try
                        {
                            var chartBytes = ChartUtility.GetChartAsBytes(reportObject.Value.SourceId, this.TenantId);
                            var chartImage = Image.GetInstance(chartBytes);

                            chartImage.Alignment = Image.ALIGN_CENTER;

                            document.SetPageSize(new Rectangle(chartImage.Width + 200f, chartImage.Height + 250f));

                            document.Open();

                            BaseFont baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                            Font font = new Font(baseFont, 16, Font.BOLD);
                            document.Add(new Paragraph(reportObject.Value.Name, font));
                            document.Add(chartImage);
                        }
                        catch (Exception exception)
                        {
                            document.Open();
                            document.Add(new Paragraph(exception.Message));
                            continue;
                        }
                        break;
                }
                pageCount++;

                if (pageCount < report.ReportObjects.Count)
                {
                    document.NewPage();
                }
            }
            document.Close();
            return File(Server.MapPath("~/" + fileName), "application/pdf", fileName);
        }

        /// <summary>
        /// This method is used to executes the paged report.
        /// </summary>
        /// <param name="reportBuilderSearchCondition">report builder search condition.</param>
        /// <returns></returns>
        public JsonResult ExecutePagedReport(ReportBuilderSearchCondition reportBuilderSearchCondition, List<DynamicVariable> dynamicVariablesList)
        {
            Dictionary<string, string> dynamicVariables = null;

            //if (reportBuilderSearchCondition.dynamicVariablesList != null && reportBuilderSearchCondition.dynamicVariablesList.Count > 0)
            //{
            //    dynamicVariables = GetDynamicVariablesFromViewModel(reportBuilderSearchCondition.dynamicVariablesList);
            //}

            if (dynamicVariablesList != null && dynamicVariablesList.Count > 0)
            {
                dynamicVariables = GetDynamicVariablesFromViewModel(dynamicVariablesList);
            }

            switch (reportBuilderSearchCondition.ReportObjectTypeId.ToUpperInvariant())
            {
                case ReportObjectTypeConstants.TableObjectType:
                    var dt = TableSourceProxy.ExecuteSource(reportBuilderSearchCondition.ReportObjectSourceId, reportBuilderSearchCondition.ReportId, reportBuilderSearchCondition.ReportObjectId, this.TenantId, reportBuilderSearchCondition.iDisplayStart, (reportBuilderSearchCondition.iDisplayStart + reportBuilderSearchCondition.iDisplayLength), dynamicVariables);

                    List<Object> resultObjects = new List<object>();

                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            resultObjects.Add(dr.ItemArray);
                        }
                    }

                    var thisTableSource = (TableSource)TableSourceProxy.GetSource(reportBuilderSearchCondition.ReportObjectSourceId, this.TenantId);

                    if (thisTableSource == null || string.IsNullOrEmpty(thisTableSource.DataSourceId)
                        || thisTableSource.DataSourceValues == null || string.IsNullOrEmpty(thisTableSource.DataSourceValues.SourceContentId))
                    {
                        return Json("Error: Invaild Source", JsonRequestBehavior.AllowGet);
                    }

                    int totalRecords = QueryDetailsProxy.TotalCount(thisTableSource.DataSourceValues.SourceContentId, thisTableSource.TenantId, dynamicVariables);

                    reportBuilderSearchCondition.sEcho = totalRecords / (reportBuilderSearchCondition.iDisplayStart.Value + reportBuilderSearchCondition.iDisplayLength.Value);

                    var jdatatableobj = new
                    {
                        sEcho = reportBuilderSearchCondition.sEcho,
                        iTotalRecords = totalRecords,
                        iTotalDisplayRecords = totalRecords,
                        aaData = resultObjects
                    };
                    return Json(jdatatableobj, JsonRequestBehavior.AllowGet);
                default:
                    break;
            }

            return Json(new { Error = ReportBuilderResource.e_ExecuteReport }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to gets the mark up for data table.
        /// </summary>
        /// <param name="dataTable">data table.</param>
        /// <param name="reportId">report identifier.</param>
        /// <param name="reportObjectId">report object identifier.</param>
        /// <param name="reportObjectSourceId">report object source identifier.</param>
        /// <param name="reportObjectTypeId">report object type identifier.</param>
        /// <returns></returns>
        public StringBuilder GetMarkUpForDataTable(DataTable dataTable, string reportId, string reportObjectId, string reportObjectSourceId, string reportObjectTypeId)
        {
            StringBuilder htmlMarkUpBuilder = new StringBuilder();

            if (dataTable == null || dataTable.Rows.Count < 1)
            {
                htmlMarkUpBuilder.Append("No Data Found");
                return htmlMarkUpBuilder;
            }

            htmlMarkUpBuilder.Append(string.Format(CultureInfo.InvariantCulture,"<table class=\"celloTable {0}\"><thead><tr>", reportObjectId));

            htmlMarkUpBuilder.Append(
                string.Format(CultureInfo.InvariantCulture,"<input type='hidden' class='ReportId' value='" + reportId + "' />"));

            htmlMarkUpBuilder.Append(
               string.Format(CultureInfo.InvariantCulture,"<input type='hidden' class='ReportObjectId' value='" + reportObjectId + "' />"));

            htmlMarkUpBuilder.Append(
               string.Format(CultureInfo.InvariantCulture,"<input type='hidden' class='SourceId' value='" + reportObjectSourceId + "' />"));

            htmlMarkUpBuilder.Append(
               string.Format(CultureInfo.InvariantCulture,"<input type='hidden' class='TypeId' value='" + reportObjectTypeId + "' />"));

            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                if (dataTable.Columns[i].DataType.Equals(typeof(Guid)))
                {
                    dataTable.Columns.RemoveAt(i);
                }
                htmlMarkUpBuilder.Append(string.Format(CultureInfo.InvariantCulture,"<th>{0}</th>", dataTable.Columns[i].ColumnName));
            }

            htmlMarkUpBuilder.Append("</tr></thead><tbody>");

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                htmlMarkUpBuilder.Append("<tr>");
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    htmlMarkUpBuilder.Append(string.Format(CultureInfo.InvariantCulture,"<td>{0}</td>", dataTable.Rows[i][j]));
                }
                htmlMarkUpBuilder.Append("</tr>");
            }

            htmlMarkUpBuilder.Append("</tbody></table>");
            return htmlMarkUpBuilder;
        }

        /// <summary>
        /// This method is used to edits the report object.
        /// </summary>
        /// <param name="reportObject">report object.</param>
        /// <returns></returns>
        public PartialViewResult EditReportObject(ReportObject reportObject)
        {
            if (reportObject == null)
            {
                return null;
            }

            ViewData["viewObjectId"] = reportObject.ReportObjectId;
            ViewData["reportObject"] = reportObject;
            ViewData["ordinal"] = reportObject.Ordinal;
            ViewData["reportObjectTypeId"] = reportObject.TypeId;

            SetReportTypeInfo(reportObject.TypeId);

            if (reportObject.TypeId.Equals(ReportObjectTypeConstants.ChartObjectType, StringComparison.OrdinalIgnoreCase))
            {
                GetChartsList(reportObject.SourceId);
            }

            return PartialView("ReportObject");
        }

        /// <summary>
        /// This method is used to get the report object.
        /// </summary>
        /// <param name="reportObjectViewModel">report object view model.</param>
        /// <returns></returns>
        public PartialViewResult ReportObject(ReportObjectViewModel reportObjectViewModel)
        {
            if (string.IsNullOrEmpty(reportObjectViewModel.ViewId) && !string.IsNullOrEmpty(reportObjectViewModel.ObjectId))
            {
                reportObjectViewModel.ViewId = reportObjectViewModel.ObjectId + "_" + reportObjectViewModel.Ordinal;
            }
            else if (string.IsNullOrEmpty(reportObjectViewModel.ViewId) && string.IsNullOrEmpty(reportObjectViewModel.ObjectId))
            {
                reportObjectViewModel.ViewId = Guid.NewGuid().ToString();
            }

            ViewData["viewObjectId"] = reportObjectViewModel.ViewId;
            ViewData["reportObject"] = null;

            if (!string.IsNullOrEmpty(reportObjectViewModel.ObjectId))
            {
                var reportObject = new ReportObject
                {
                    ReportObjectId = reportObjectViewModel.ObjectId,
                    Ordinal = reportObjectViewModel.Ordinal,
                    ReportId = reportObjectViewModel.ReportId,
                    SourceId = reportObjectViewModel.SourceId,
                    TypeId = reportObjectViewModel.TypeId
                };
                ViewData["reportObject"] = reportObject;
            }

            ViewData["ordinal"] = reportObjectViewModel.Ordinal;
            ViewData["reportObjectSourceId"] = reportObjectViewModel.SourceId;
            ViewData["reportheader"] = string.Empty;
            ViewData["reportObjectTypeName"] = string.Empty;
            ViewData["reportObjectId"] = reportObjectViewModel.ObjectId;

            SetReportTypeInfo(reportObjectViewModel.TypeId);

            if (reportObjectViewModel.TypeId.Equals(ReportObjectTypeConstants.ChartObjectType, StringComparison.OrdinalIgnoreCase))
            {
                GetChartsList(reportObjectViewModel.SourceId);
            }

            return PartialView("ReportObject");
        }

        /// <summary>
        /// This method is used to sets the report type info.
        /// </summary>
        /// <param name="reportObjectTypeId">report object type identifier.</param>
        private void SetReportTypeInfo(string reportObjectTypeId)
        {
            var reportObjectTypes = GetReportObjectTypeMappings();

            if (!string.IsNullOrEmpty(reportObjectTypeId) && (reportObjectTypes.ContainsKey(reportObjectTypeId.ToUpperInvariant())))
            {
                ViewData["reportheader"] = reportObjectTypes[reportObjectTypeId.ToUpperInvariant()];
                ViewData["reportObjectTypeName"] = reportObjectTypes[reportObjectTypeId.ToUpperInvariant()];
                ViewData["reportObjectTypeId"] = reportObjectTypeId.ToUpperInvariant();
            }
            else if (!string.IsNullOrEmpty(reportObjectTypeId) && reportObjectTypes.ContainsKey(reportObjectTypeId.ToUpperInvariant()))
            {
                ViewData["reportheader"] = reportObjectTypes[reportObjectTypeId.ToUpperInvariant()];
                ViewData["reportObjectTypeName"] = reportObjectTypes[reportObjectTypeId.ToUpperInvariant()];
                ViewData["reportObjectTypeId"] = reportObjectTypeId.ToUpperInvariant();
            }

            ViewData["reportObjectTypeMapping"] = reportObjectTypes;
        }

        /// <summary>
        /// This method is used to gets the charts list.
        /// </summary>
        /// <param name="selectedChartId">selected chart identifier.</param>
        private void GetChartsList(string selectedChartId)
        {
            var chartsList = ChartDetailsProxy.GetAllChartsList(this.TenantId);

            var allCharts = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(selectedChartId))
            {
                selectedChartId = "-1";
            }

            if (chartsList != null && chartsList.Count > 0)
            {
                allCharts.Add("-1", "--Choose A Chart Source--");
                foreach (var chart in chartsList)
                {
                    allCharts.Add(chart.Key, chart.Value);
                }
            }
            else
            {
                allCharts.Add("-1", "--No Chart Sources Available--");
            }

            ViewData["ChartDataSources"] = new SelectList(allCharts, "Key", "Value", selectedChartId);
        }

        /// <summary>
        /// This method is used to get the reports by name.
        /// </summary>
        /// <param name="term">term.</param>
        /// <returns></returns>
        public JsonResult GetReportsByName(string term)
        {
            var searchTerms = ReportProxy.FindReports(term, this.TenantId);

            if (searchTerms != null && searchTerms.Count > 0)
            {
                var results = from st in searchTerms
                              select new
                              {
                                  id = st.Key,
                                  label = st.Value.Name,
                                  value = st.Value.Description
                              };

                return Json(results, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        /// <summary>
        /// This method is used to gets the report object type mappings.
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> GetReportObjectTypeMappings()
        {
            var reportObjTypes = ReportProxy.GetReportObjectTypes();
            var reportObjectTypeMapping = (from rot in reportObjTypes
                                           select new { rot.Key, rot.Value.Name }).ToDictionary(t => t.Key, t => t.Name);

            return reportObjectTypeMapping;
        }

        /// <summary>
        /// This method is used to reset the report object status.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <param name="reportObjectId">report object identifier.</param>
        /// <param name="futureState">state of the future.</param>
        /// <returns></returns>
        public JsonResult ResetReportObjectStatus(string reportId, string reportObjectId, int futureState)
        {
            if (string.IsNullOrEmpty(reportId) || string.IsNullOrEmpty(reportObjectId) || (futureState < 0 || futureState > 1))
            {
                return Json(false, JsonRequestBehavior.DenyGet);
            }

            if (ReportProxy.ResetReportObjectStatus(reportId, reportObjectId, futureState, this.TenantId) <= 0)
            {
                return Json(false, JsonRequestBehavior.DenyGet);
            }
            return Json(true, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to deletes the report object source.
        /// </summary>
        /// <param name="sourceId">source identifier.</param>
        /// <param name="reportId">report identifier.</param>
        /// <param name="reportObjectTypeId">report object type identifier.</param>
        /// <param name="reportObjectId">report object identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteReportObjectSource(string sourceId, string reportId, string reportObjectTypeId, string reportObjectId)
        {
            if (string.IsNullOrEmpty(sourceId) || string.IsNullOrEmpty(reportId) || string.IsNullOrEmpty(reportObjectTypeId) || string.IsNullOrEmpty(reportObjectId))
            {
                return null;
            }

            int deleteStatus = -1;

            switch (reportObjectTypeId)
            {
                case ReportObjectTypeConstants.TableObjectType:

                    deleteStatus = TableSourceProxy.DeleteSource(sourceId, this.TenantId);

                    return deleteStatus > 0 ? this.Json(deleteStatus) : null;
                case ReportObjectTypeConstants.TextObjectType:
                    deleteStatus = TextSourceProxy.DeleteSource(sourceId, this.TenantId);

                    return deleteStatus > 0 ? this.Json(deleteStatus) : null;

                case ReportObjectTypeConstants.ImageObjectType:
                case ReportObjectTypeConstants.ChartObjectType:
                    return null;
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// This method is used to get the print view based on the given report identifier.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        public ActionResult PrintView(string reportId)
        {
            ViewData["reportId"] = reportId;
            ReportViewModel reportViewModel = null;
            try
            {
                reportViewModel = GetReportViewModelForReport(reportId, this.TenantId);
            }
            catch (Exception exception)
            {
                TempData["Error"] = exception.Message;
                return RedirectToAction("Index");
            }

            return this.View("PrintView", reportViewModel);
        }

        /// <summary>
        /// This method is used to pre execute the report by given report identifier.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PreExecuteReportByReportId(string reportId)
        {
            if (string.IsNullOrEmpty(reportId))
            {
                return Json(new { Error = Resources.ReportBuilderResource.e_ReportId });
            }

            ReportViewModel executingReport = GetReportViewModelForReport(reportId, this.TenantId);

            if (executingReport == null || executingReport.ReportObjects == null || executingReport.ReportObjects.Count < 0)
            {
                return Json(new { Error = Resources.ReportBuilderResource.e_InvalidReportOrObject });
            }

            return Json(executingReport);
        }

        /// <summary>
        /// This method is sued to gets the report view model for report.
        /// </summary>
        /// <param name="reportId">report identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>ReportViewModel</returns>
        private static ReportViewModel GetReportViewModelForReport(string reportId, string tenantId)
        {
            Report executingReport = ReportProxy.GetReport(reportId, tenantId);

            if (executingReport == null || executingReport.ReportObjects == null || executingReport.ReportObjects.Count < 1)
            {
                return null;
            }

            ReportViewModel reportViewModel = new ReportViewModel
            {
                Name = executingReport.Name,
                ReportId = executingReport.ReportId
            };

            foreach (ReportObject reportObject in executingReport.ReportObjects.Values)
            {
                List<DynamicVariable> dynamicVariableList = null;

                Dictionary<string, string> dynamicVariables = null;

                switch (reportObject.TypeId.ToUpperInvariant())
                {
                    case TextReportObject:
                    case ImageReportObject:
                    default:
                        break;
                    case ChartReportObject:
                        var chart = ChartDetailsProxy.GetChartDetails(reportObject.SourceId, tenantId);

                        if (chart == null || string.IsNullOrEmpty(chart.ChartSourceId))
                        {
                            break;
                        }

                        dynamicVariables = DataSourceProxy.GetDynamicVariables(chart.ChartSourceId, tenantId);
                        break;
                    case TableReportObject:
                        dynamicVariables = TableSourceProxy.GetDynamicVariables(reportObject.SourceId, tenantId);
                        break;
                }

                if (dynamicVariables != null && dynamicVariables.Count > 0)
                {
                    dynamicVariableList = (from dynamicVariable in dynamicVariables
                                           select new DynamicVariable
                                           {
                                               Name = dynamicVariable.Key,
                                               VariableId = Guid.NewGuid().ToString(),
                                               Value = dynamicVariable.Value
                                           }).ToList();
                }

                ReportObjectModel reportObjectModel = new ReportObjectModel
                {
                    ReportId = executingReport.ReportId,
                    ObjectId = reportObject.ReportObjectId,
                    Name = reportObject.Name,
                    TypeId = reportObject.TypeId,
                    SourceId = reportObject.SourceId,
                    Ordinal = reportObject.Ordinal,
                    DynamicVariables = dynamicVariableList,
                    RowsPerPage = reportObject.RowsPerPage
                };

                reportViewModel.ReportObjects.Add(reportObjectModel);
            }

            return reportViewModel;
        }

        [HttpPost]
        [Obsolete("No longer needed")]
        public JsonResult PreExecuteReport(ReportViewModel reportViewModel)
        {
            if (reportViewModel == null || reportViewModel.ReportObjects == null || reportViewModel.ReportObjects.Count < 1)
            {
                return Json(new { Message = Resources.ReportBuilderResource.e_ReportData });
            }

            foreach (ReportObjectModel reportObjectModel in reportViewModel.ReportObjects)
            {
                switch (reportObjectModel.TypeId)
                {
                    case ReportObjectTypeConstants.TableObjectType:
                        var dynamicVariables = TableSourceProxy.GetDynamicVariables(reportObjectModel.SourceId, this.TenantId);

                        if (dynamicVariables == null || dynamicVariables.Count < 1)
                        {
                            continue;
                        }

                        foreach (var dynamicVariable in dynamicVariables)
                        {
                            reportObjectModel.DynamicVariables.Add(new DynamicVariable
                            {
                                VariableId = Guid.NewGuid().ToString(),
                                Name = dynamicVariable.Key,
                                Value = dynamicVariable.Value
                            });
                        }
                        break;
                    default:
                        break;
                }
            }

            return Json(reportViewModel);
        }

        #region UnUsedMethods

        #endregion

    }
}
