using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using CelloSaaS.Library;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.DataManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using MvcContrib.Pagination;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for managing the roles.
    /// </summary>
    [HandleError]
    public class RolesController : CelloSaaS.View.CelloController
    {
        private string TenantId = TenantContext.GetTenantId(new Role().EntityIdentifier);

        // Logger policy name
        private const string _defaultPolicy = "GlobalExceptionLogger";

        #region Role Public Methods

        /// <summary>
        /// This method is used to get role details.
        /// </summary>
        /// <returns>Partial view of Role list</returns>
        public ActionResult RoleDetails()
        {
            // Get role details
            RoleList();
            TenantDetails parentTenant = TenantRelationProxy.GetParentTenant(TenantId);
            ViewData["CanCopyParent"] = parentTenant != null;
            if (Request.IsAjaxRequest())
            {
                return PartialView("RoleList");
            }
            return View("RoleDetailsList");
        }

        /// <summary>
        /// This method is used to get the role details.
        /// </summary>
        /// <returns>Role details</returns>
        public ActionResult RoleDetailsList()
        {
            // Get role details
            RoleList();
            TenantDetails parentTenant = TenantRelationProxy.GetParentTenant(TenantId);
            ViewData["CanCopyFromParent"] = parentTenant != null;
            return View();
        }

        /// <summary>
        /// This method is used to copies all roles from parent.
        /// </summary>
        /// <returns></returns>
        public ActionResult CopyAllRolesFromParent()
        {
            try
            {
                int count = RoleProxy.CopyAllRolesAndPrivilegesFromParent(TenantId);
                TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.RolesResource.s_CopySuccess, count);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                TempData["Error"] = Resources.ConfigurationResource.e_AccessDenied;
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                TempData["Error"] = Resources.ConfigurationResource.e_PickupListValuesCopy;
            }

            return RedirectToAction("RoleDetailsList");
        }

        /// <summary>
        /// This method is used to get add role details.
        /// </summary>
        /// <returns>Partial view of Add role details</returns>
        public ActionResult AddRoleDetails()
        {
            Role role = new Role();

            // get the mapping service list
            ViewData["ServiceList"] = GetServiceList();

            return PartialView("AddRole", role);
        }

        /// <summary>
        /// This method is used to gets the mapping service list.
        /// </summary>
        private static IList<Service> GetServiceList()
        {
            Dictionary<string, Service> serviceslist;

            if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
            {
                serviceslist = LicenseProxy.GetAllServices();
            }
            else
            {
                serviceslist = LicenseProxy.GetServicesByUserId(UserIdentity.UserId);
            }

            return (serviceslist != null) ? serviceslist.Values.ToList() : null;
        }

        /// <summary>
        /// This method is used to add role details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns>
        /// If role created successfully then return success or return partial view of add role details
        /// </returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object AddRoleDetails(FormCollection formCollection)
        {
            Role role = new Role();

            try
            {
                // Update role details from form
                UpdateModel(role);

                // Validate role details
                ValidateRole(role);

                string[] roleServiceIds = Request.Form["roleService"] != null ? Request.Form["roleService"].Split(',').ToArray() : null;

                if (UserIdentity.Roles.Contains(RoleConstants.ServiceAdmin))
                {
                    if (string.IsNullOrEmpty(formCollection["IsGlobalRole"])
                        || roleServiceIds == null || roleServiceIds.Count() == 0)
                    {
                        ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_ServiceMandatory);
                    }
                }

                if (ModelState.IsValid)
                {
                    string result = string.Empty;

                    // Check Is Global Role checkbox
                    if (string.IsNullOrEmpty(formCollection["IsGlobalRole"]))
                    {
                        // add tenant specific role
                        result = RoleProxy.AddRole(this.TenantId, role.RoleName, role.Description);
                    }
                    else
                    {
                        // add global role
                        result = RoleProxy.AddRole(null, role.RoleName, role.Description, roleServiceIds);
                    }

                    // If role details added successfully then return success or return add role partial view
                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.s_RoleCreate);
                        //The 'Success' key should be in English. It is used in JavaScript validation.
                        return Content("Success");
                        //The Resource File key value should be in English. If use.
                        //return Content(Resources.RolesResource.s_SuccessMessage); 
                    }
                    else
                    {
                        ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleCreate);
                    }
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleCreate);
            }
            catch (DataException dataEception)
            {
                ExceptionService.HandleException(dataEception, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_DuplicateRoleName);
            }

            // get the mapping service list
            ViewData["ServiceList"] = GetServiceList();

            return PartialView("AddRole", role);
        }

        /// <summary>
        /// This method is to bind the fields in Edit form.
        /// </summary>
        /// <param name="roleId">role identifier.</param>        
        /// <returns>Partial view of edit role</returns>
        public ActionResult RoleUpdate(string roleId)
        {
            Role roleDetail = new Role();

            try
            {
                // Get role details of role id
                roleDetail = RoleProxy.GetRoleDetailsByRoleId(roleId);

                if (roleDetail == null)
                    return HttpNotFound("Role not found!");

                // get the mapping service list
                ViewData["ServiceList"] = GetServiceList();

                // get services mapped the given role
                Dictionary<string, Service> roleServices = LicenseProxy.GetServicesByRoleId(roleId);

                ViewData["RoleServices"] = roleServices != null ? roleServices.Keys.ToArray() : null;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleLoad);
            }

            return PartialView("EditRole", roleDetail);
        }

        /// <summary>
        /// This method is used to update the role.
        /// </summary>        
        /// <returns>Success/Partial view of Edit role</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RoleUpdate()
        {
            Role roleDetails = new Role();
            try
            {
                //Update role details from form
                UpdateModel(roleDetails);

                // Validate role details
                ValidateRole(roleDetails);

                if (ModelState.IsValid)
                {
                    string result = string.Empty;

                    if (!roleDetails.RoleId.StartsWith("GR$", StringComparison.OrdinalIgnoreCase))
                    {
                        roleDetails.TenantId = this.TenantId;
                    }

                    //Update role details
                    result = RoleProxy.UpdateRole(roleDetails, string.Empty);

                    // If role updated successfully return success message else return edit role partial view
                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.s_RoleUpdate);
                        //The 'Success' key should be in English. It is used in JavaScript validation.
                        return Content("Success");
                        //The Resource File key value should be in English. If use.
                        //return Content(Resources.RolesResource.s_SuccessMessage);
                    }
                    else
                    {
                        ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleUpdate);
                    }
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_RoleUpdate);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_DuplicateRoleName);
            }

            // get the mapping service list
            ViewData["ServiceList"] = GetServiceList();

            return PartialView("EditRole", roleDetails);
        }

        /// <summary>
        /// This method is used to check the role have any privileges ,if not the role will get deleted.
        /// </summary>
        /// <param name="roleid">role identifier</param>        
        /// <returns>Partial view of Role list</returns>
        public ActionResult DeactivateRole(string roleId)
        {
            try
            {
                var tenantId = this.TenantId;

                // Deactivate the role details
                if (!IsGlobalRole(roleId))
                {
                    RoleProxy.DeleteRole(tenantId, roleId);
                }
                else
                {
                    // if global role pass tenant id as empty
                    RoleProxy.DeleteRole(string.Empty, roleId);
                }

                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.s_RoleDelete);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleDelete);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleDelete + " " + Resources.RolesResource.e_RoleDeleteHasPrivilege);
            }

            // Get all role details
            RoleList();
            return PartialView("RoleList");
        }

        /// <summary>
        /// This method is the used to activate the deactivated role.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <returns>Partial view of Role list</returns>
        public ActionResult ActivateRole(string roleId)
        {
            try
            {
                Role role = RoleProxy.GetRoleDetailsByRoleId(roleId);

                if (role != null)
                {
                    role.Status = true;
                    RoleProxy.UpdateRole(role, string.Empty);
                    ModelState.AddModelError("RoleListMessage", Resources.RolesResource.s_RoleActivate);
                }
                else
                {
                    return HttpNotFound("Role not found!");
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RoleStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleActivate);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleActivate + " " + Resources.RolesResource.e_RoleDeleteHasPrivilege);
            }

            // Get all role details
            RoleList();

            return PartialView("RoleList");
        }

        #endregion

        #region Role Private Methods

        /// <summary>
        /// This is the get method of Role list.
        /// If user is Service Admin then get roles for his service only.
        /// For other users get the roles by tenant license and intersect with
        /// logged in user roles.
        /// </summary>        
        private void RoleList()
        {
            if (TempData["Success"] != null)
            {
                ModelState.AddModelError("RoleListMessage", TempData["Success"].ToString());
            }

            if (TempData["Error"] != null)
            {
                ModelState.AddModelError("RoleStatusMessage", TempData["Error"].ToString());
            }

            try
            {
                List<Role> roleGlobalRole = null;

                // check the logged in user is a service admin
                if (UserIdentity.IsInRole(RoleConstants.ServiceAdmin))
                {
                    // get his services
                    Dictionary<string, Service> userServices = LicenseProxy.GetServicesByUserId(UserIdentity.UserId);

                    if (userServices != null && userServices.Count > 0)
                    {
                        // get roles mapped to his services
                        Dictionary<string, Role> roleList = RoleProxy.GetRolesByServiceIds(userServices.Keys.ToArray());

                        // remove the product & service admin roles
                        if (roleList != null && roleList.Count > 0)
                        {
                            roleList.Remove(RoleConstants.ProductAdmin);
                            roleList.Remove(RoleConstants.ServiceAdmin);

                            roleList.Values.ToList().ForEach(role =>
                            {
                                roleGlobalRole.Add(role);
                            });
                        }

                        ViewData["RoleDetails"] = roleGlobalRole.GroupBy(x => x.RoleId).Select(x => x.First()).ToList();
                        return;
                    }
                }

                // Get global role details
                roleGlobalRole = RoleProxy.GetGlobalRolesByTenant(this.TenantId);

                // Remove product admin role
                if (roleGlobalRole != null && roleGlobalRole.Count > 0)
                {
                    if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                        roleGlobalRole.RemoveAll(r => r.RoleId.Equals(RoleConstants.ServiceAdmin, StringComparison.Ordinal));

                    // Get tenant role details
                    List<Role> roleTenantRole = RoleProxy.GetAllTenantRoleDetails(this.TenantId);

                    if (roleTenantRole != null && roleTenantRole.Count > 0)
                    {
                        roleTenantRole.RemoveAll(r => r.RoleId.Equals(RoleConstants.ProductAdmin, StringComparison.Ordinal));
                        roleGlobalRole.AddRange(roleTenantRole);
                    }
                }

                ViewData["RoleDetails"] = roleGlobalRole.GroupBy(x => x.RoleId).Select(x => x.First()).ToList();
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleLoad);
            }
        }

        /// <summary>
        /// This method is used to validate the role details.
        /// </summary>
        /// <param name="role">role details.</param>
        /// <returns>True/False</returns>
        private void ValidateRole(Role role)
        {
            // Check role name is null or empty
            if (string.IsNullOrWhiteSpace(role.RoleName))
            {
                ModelState.AddModelError("RoleName", "");
                ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_RoleNameMandatory);
            }
            // Check role name is in correct format or not
            else
            {
                if (!Util.ValidateIdentifierWithSpace(role.RoleName))
                    ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_RoleName);
            }

            // Check role description is null or empty
            if (string.IsNullOrWhiteSpace(role.Description))
            {
                ModelState.AddModelError("Description", "");
                ModelState.AddModelError("RoleStatusMessageSummary", Resources.RolesResource.e_Description);
            }
        }

        #endregion

        #region Role Privilege Public Methods

        /// <summary>
        /// This method is used to get role privilege details.
        /// </summary>
        /// <param name="strRoleId">role identifier.</param>
        /// <param name="mode">mode-edit/insert.</param>
        /// <param name="rolePrivilegeId">role privilege identifier.</param>
        /// <param name="dataScopeId">data scope identifier.</param>
        /// <param name="roleName">role name.</param>
        /// <returns>Role privilege list details view.</returns>
        public ActionResult RolePrivilegeList(string strRoleId, string mode, string rolePrivilegeId, string dataScopeId, string roleName)
        {
            // Get role privilege details and set into viewdata
            SetRolePrivilegeList(strRoleId, roleName);

            // If mode is not null then assign the role privilege id in Viewdata and get the data scope details
            if (mode != null)
            {
                ViewData["EditRolePrivilegeId"] = rolePrivilegeId;
                GetDataScopeDetails(dataScopeId); // Get Data Scope details
            }

            return View("RolePrivilegeList");
        }

        /// <summary>
        /// This method is used to add privileges to role
        /// </summary>
        /// <param name="roleName">role name.</param>
        /// <param name="strRoleId">role identifier.</param>
        /// <returns>View of privilege and data scope details</returns>
        public ActionResult AddRolePrivilege(string strRoleId, string roleName)
        {
            // Get role privilege details and set into viewdata
            SetRolePrivilegeList(strRoleId, roleName);
            return View();
        }

        /// <summary>
        /// This method is used to add privileges to role.
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object AddRolePrivilege()
        {
            string roleId = Request.Form["RoleId"];

            try
            {
                List<string> ltPrivilege = new List<string>();

                // Get the selected privilege ids
                if (Request.Form["privilege"] != null && !string.IsNullOrEmpty(Request.Form["privilege"].ToString()))
                {
                    ltPrivilege.AddRange(Request.Form["privilege"].Split(',').ToList());
                    ViewData["ListPrivilege"] = ltPrivilege;
                }

                List<RolePrivilege> ltRolePrivilege = new List<RolePrivilege>();

                if (ltPrivilege != null && ltPrivilege.Count > 0)
                {
                    // Get all data scopes
                    List<DataScope> lstDataScope = DataAccessProxy.GetAllDataScope();

                    foreach (string privilegeId in ltPrivilege)
                    {
                        // Get datascope of the privilege
                        string dataScopeId = Request.Form["DataScope_" + privilegeId];

                        // Check datascope is selected or not. If selected then add it to dict or add error message
                        if (!string.IsNullOrEmpty(dataScopeId))
                        {
                            //create instance for role privilege and set all details and add it in the list
                            RolePrivilege rpRolePrivilege = new RolePrivilege();
                            rpRolePrivilege.RoleId = roleId;
                            rpRolePrivilege.PrivilegeId = privilegeId;
                            rpRolePrivilege.TenantId = this.TenantId;
                            rpRolePrivilege.DataScopeId = dataScopeId;
                            ltRolePrivilege.Add(rpRolePrivilege);

                            ViewData["DataScope_" + privilegeId] = new SelectList(lstDataScope, "ID", "Name", dataScopeId);
                        }
                        else
                            ModelState.AddModelError("valDataScope_" + privilegeId, "*");
                    }
                }
                else
                {
                    ModelState.AddModelError("AddRolePrivilegeStatusMessage", Resources.RolesResource.e_PrivilegeSelect);
                }

                // Check role privilege list have details or not
                if (ModelState.IsValid)
                {
                    //Add privileges to role
                    PrivilegeProxy.AddPrivilegesToRole(this.TenantId, ltRolePrivilege);
                    ModelState.AddModelError("AddRolePrivilegeStatusMessage", Resources.RolesResource.s_PrivilegeAdd);
                    return Resources.RolesResource.s_SuccessMessage;
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeAdd);
            }
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeAdd);
            }

            // Get privilege and datascope details for role id
            GetPrivilegeAndDataScopeDetails(roleId);

            return PartialView("PartialPrivilegeList");
        }

        /// <summary>
        /// This method is used to edit the role privilege details.
        /// </summary>
        /// <param name="rolePrivilegeId">role privilege identifier.</param>
        /// <param name="dataScopeId">data scope identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <returns>View of Role privilege list</returns>
        public ActionResult UpdateRolePrivilege(string rolePrivilegeId, string dataScopeId, string roleId, string roleName)
        {
            try
            {
                // Check data scope is selected or not
                if (string.IsNullOrEmpty(dataScopeId))
                {
                    ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScope);
                }

                //Set role privilege id in view data
                ViewData["EditRolePrivilegeId"] = rolePrivilegeId;

                if (ModelState.IsValid)
                {
                    // Create role privilege object and assign the values
                    RolePrivilege rolePrivilege = new RolePrivilege();
                    rolePrivilege.Id = rolePrivilegeId;
                    rolePrivilege.DataScopeId = dataScopeId;
                    rolePrivilege.RoleId = roleId;

                    // Update data scope for role privilege
                    string success = PrivilegeProxy.UpdateRolePrivilege(rolePrivilege);

                    // If role privilege updated successfully then clear view data role privilege id
                    if (!string.IsNullOrEmpty(success))
                    {
                        ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.s_DataScopeUpdate);
                        ViewData["EditRolePrivilegeId"] = null;
                    }
                    else
                    {
                        ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScopeUpdate);
                    }
                }
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScopeUpdate);
            }

            // Get role privilege details and set into viewdata
            SetRolePrivilegeList(roleId, roleName);

            // Get all data scope
            GetDataScopeDetails(dataScopeId);

            return View("RolePrivilegeList");
        }

        /// <summary>
        /// This method is used to delete role privilege details.
        /// </summary>
        /// <param name="roleName">role name.</param>
        /// <param name="rolePrivilegeId">role privilege identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <returns>View of Role Privilege List</returns>
        public ActionResult DeleteRolePrivilege(string roleId, string rolePrivilegeId, string roleName)
        {
            try
            {
                PrivilegeProxy.DeleteRolePrivilege(rolePrivilegeId);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.s_PrivilegeDelete);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDelete);
            }
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDelete);
            }

            // Get role privilege details and set into viewdata
            SetRolePrivilegeList(roleId, roleName);

            return View("RolePrivilegeList");
        }

        /// <summary>
        /// This method is used to get the privilege list of roles.
        /// </summary>
        /// <returns>Message/Partial view of privilege list.</returns>
        public object GetPrivilegeDetails()
        {
            GetPrivilegeAndDataScopeDetails(string.Empty);
            return PartialView("PartialPrivilegeList");
        }


        #endregion

        #region Role Privilege Private Methods

        /// <summary>
        /// This method is used to get data scope details.
        /// </summary>
        /// <param name="dataScopeId">data scope identifier.</param>
        private void GetDataScopeDetails(string dataScopeId)
        {
            try
            {
                List<DataScope> lstDataScope = DataAccessProxy.GetAllDataScope();

                if (lstDataScope != null)
                    ViewData["DataScope"] = new SelectList(lstDataScope, "ID", "Name", dataScopeId);
            }
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_DataScopeLoad);
            }
        }

        /// <summary>
        /// This method is used to get role privilege details and set into view data.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="roleName">role name.</param>
        private void SetRolePrivilegeList(string roleId, string roleName)
        {
            ViewData["RoleName"] = roleName;
            ViewData["RoleId"] = roleId;

            try
            {
                List<RolePrivilege> lstRolePrivilege = null;

                if (!string.IsNullOrEmpty(roleId))
                {
                    Dictionary<string, RolePrivilege> dictRolePrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, new string[] { roleId });

                    if (dictRolePrivilege != null && dictRolePrivilege.Count > 0)
                    {
                        lstRolePrivilege = dictRolePrivilege.Values.ToList();
                    }
                }

                ViewData["RolePrivilegeForRole"] = lstRolePrivilege;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_RolePrivilegeLoad);
            }
        }

        /// <summary>
        /// This method is used to get privilege and data scope details
        /// <param name="roleId">role identifier.</param>
        /// </summary>
        private void GetPrivilegeAndDataScopeDetails(string roleId)
        {
            try
            {
                string roleName = string.Empty;

                // Get Role Name and Role Id from form
                if (Request.Form["RoleName"] != null && Request.Form["RoleId"] != null)
                {
                    roleId = Request.Form["RoleId"].ToString();
                    roleName = Request.Form["RoleName"].ToString();
                }

                ViewData["RoleName"] = roleName;
                ViewData["RoleId"] = roleId;

                List<Privilege> lstPrivilege = null;

                if (!string.IsNullOrWhiteSpace(Request.Form["SearchString"]))
                {
                    string searchString = Request.Form["SearchString"].ToString().Trim();
                    ViewData["SearchString"] = searchString;

                    lstPrivilege = PrivilegeProxy.GetPrivileges(searchString, this.TenantId);
                }

                // Check role id
                if (!string.IsNullOrEmpty(roleId))
                {
                    Dictionary<string, RolePrivilege> dictRolePrivilege = PrivilegeProxy.GetPrivilegesForRoles(this.TenantId, new string[] { roleId });

                    if (dictRolePrivilege != null && dictRolePrivilege.Count > 0)
                    {
                        List<RolePrivilege> lstRolePrivilege = dictRolePrivilege.Values.ToList();
                        ViewData["RolePrivilegeForRole"] = lstRolePrivilege;

                        // Remove privileges from privilege list if already available for this role
                        if (lstPrivilege != null && lstPrivilege.Count > 0)
                        {
                            foreach (RolePrivilege rolePrivilege in lstRolePrivilege)
                            {
                                foreach (Privilege privilege in lstPrivilege)
                                {
                                    if (privilege.Id.Equals(rolePrivilege.PrivilegeId))
                                    {
                                        lstPrivilege.Remove(privilege);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                ViewData["NewPrivileges"] = lstPrivilege;

                // Get all data scopes
                List<DataScope> lstDataScope = DataAccessProxy.GetAllDataScope();

                if (lstDataScope != null && lstPrivilege != null && lstPrivilege.Count > 0)
                {
                    //Assign datascope details in the view data
                    foreach (Privilege privilege in lstPrivilege)
                    {
                        ViewData["DataScope_" + privilege.Id] = new SelectList(lstDataScope, "ID", "Name");
                    }
                }
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (PrivilegeException privilegeException)
            {
                ExceptionService.HandleException(privilegeException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDataScopeLoad);
            }
            catch (DataAccessException dataAccessException)
            {
                ExceptionService.HandleException(dataAccessException, _defaultPolicy);
                ModelState.AddModelError("RolePrivilegeListStatusMessage", Resources.RolesResource.e_PrivilegeDataScopeLoad);
            }
        }

        #endregion

        #region Manage Privilege Public Methods

        /// <summary>
        /// This method is used to gets the assignable privileges for module.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleId">module identifier.</param>
        /// <param name="featureId">feature identifier.</param> 
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>partial privilege list view</returns>
        public ActionResult GetAssignablePrivilegesForModule(string roleId, string moduleId, string featureId, string tenantId)
        {
            if (string.IsNullOrEmpty(roleId))
            {
                ModelState.AddModelError("Error", Resources.RolesResource.e_RoleIdMandatory);
            }

            if (string.IsNullOrEmpty(moduleId))
            {
                ModelState.AddModelError("Error", Resources.RolesResource.e_ModuleIdMandatory);
            }

            if (string.IsNullOrEmpty(featureId))
            {
                ModelState.AddModelError("Error", Resources.RolesResource.e_FeatureIdMandatory);
            }

            if (string.IsNullOrEmpty(tenantId))
            {
                tenantId = this.TenantId;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // get assigned privileges 
                    Dictionary<string, RolePrivilege> rolePrivileges = PrivilegeProxy.GetPrivilegesForRoles(tenantId, new string[] { roleId });

                    // get assignable privileges
                    Dictionary<string, Privilege> assignablePrivileges = GetPrivilegesByModuleFeature(tenantId, moduleId, featureId);

                    // remove already assigned privilege ids
                    if (rolePrivileges != null && rolePrivileges.Count > 0
                        && assignablePrivileges != null && assignablePrivileges.Count > 0)
                    {
                        foreach (KeyValuePair<string, RolePrivilege> item in rolePrivileges)
                        {
                            assignablePrivileges.Remove(item.Value.PrivilegeId);
                        }
                    }

                    var tenantLicense = LicenseProxy.GetTenantLicense(tenantId);
                    if (tenantLicense != null && tenantLicense.LicensePrivilegeDetails != null)
                    {
                        List<string> availablePrivileges = tenantLicense.LicensePrivilegeDetails.Keys.ToList();
                        assignablePrivileges = assignablePrivileges.Where(x => availablePrivileges.Contains(x.Key)).ToDictionary(x => x.Key, y => y.Value);
                    }

                    ViewData["PrivilegeList"] = assignablePrivileges;
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_ParameterEmptyOrNull);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_AccessDenied);
                }
                catch (PrivilegeException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_FetchPrivilege);
                }
                catch (LicenseException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_FetchTenantLicense);
                }

            }

            return PartialView("PrivilegeList");
        }

        /// <summary>
        /// This method is used to gets the assignable privileges for module.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleId">module identifier.</param>
        /// <param name="featureId">feature identifier.</param> 
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>partial privilege list view</returns>
        public ActionResult GetAssignedPrivilegesForModule(string roleId, string moduleId, string featureId, string tenantId)
        {
            if (string.IsNullOrEmpty(roleId))
            {
                ModelState.AddModelError("Error", Resources.RolesResource.e_RoleIdMandatory);
            }

            if (string.IsNullOrEmpty(moduleId))
            {
                ModelState.AddModelError("Error", Resources.RolesResource.e_ModuleIdMandatory);
            }

            if (string.IsNullOrEmpty(featureId))
            {
                ModelState.AddModelError("Error", Resources.RolesResource.e_FeatureIdMandatory);
            }

            if (string.IsNullOrEmpty(tenantId))
            {
                tenantId = this.TenantId;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // get assigned privileges 
                    Dictionary<string, RolePrivilege> rolePrivileges = PrivilegeProxy.GetPrivilegesForRoles(tenantId, new string[] { roleId });

                    Dictionary<string, Privilege> privilegeList = null;

                    // get assignable privileges
                    Dictionary<string, Privilege> licenseFilteredPrivileges = GetPrivilegesByModuleFeature(tenantId, moduleId, featureId);

                    // remove already assigned privilege ids
                    if (rolePrivileges != null && rolePrivileges.Count > 0
                        && licenseFilteredPrivileges != null && licenseFilteredPrivileges.Count > 0)
                    {
                        var filteredPrivilegeIds = new List<string>();

                        foreach (KeyValuePair<string, RolePrivilege> item in rolePrivileges)
                        {
                            if (licenseFilteredPrivileges.ContainsKey(item.Value.PrivilegeId))
                            {
                                filteredPrivilegeIds.Add(item.Value.PrivilegeId);
                            }
                        }

                        if (filteredPrivilegeIds.Count > 0)
                            privilegeList = PrivilegeProxy.GetPrivileges(filteredPrivilegeIds.ToArray());
                    }

                    ViewData["PrivilegeList"] = privilegeList;
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_ParameterEmptyOrNull);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_AccessDenied);
                }
                catch (PrivilegeException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.RolesResource.e_FetchPrivilege);
                }
            }

            return PartialView("PrivilegeList");
        }

        /// <summary>
        /// This method is used to adds the privileges to role.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="privilegeIds">list of privilege identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult AddPrivilegesToRole(string roleId, string[] privilegeIds, string tenantId)
        {
            if (Request.IsAjaxRequest())
            {
                if (string.IsNullOrEmpty(roleId))
                {
                    return Json(new { Error = Resources.RolesResource.e_RoleIdMandatory });
                }

                if (privilegeIds == null || privilegeIds.Length == 0)
                {
                    return Json(new { Error = Resources.RolesResource.e_PrivilegeIdMandatory });
                }

                if (string.IsNullOrEmpty(tenantId))
                {
                    tenantId = this.TenantId;
                }

                string error = Resources.RolesResource.e_AddPrivilegeToRole;

                if (ModelState.IsValid)
                {
                    try
                    {
                        List<RolePrivilege> rolePrivileges = new List<RolePrivilege>();

                        foreach (var privilegeId in privilegeIds)
                        {
                            rolePrivileges.Add(new RolePrivilege
                            {
                                RoleId = roleId,
                                TenantId = tenantId,
                                PrivilegeId = privilegeId,
                                CreatedBy = UserIdentity.UserId,
                                CreatedOn = DateTime.Now,
                                Status = true
                            });
                        }

                        // add the privileges to the role  
                        PrivilegeProxy.AddPrivilegesToRole(tenantId, rolePrivileges);

                        return Json(new { Success = string.Format(CultureInfo.InvariantCulture, Resources.RolesResource.s_AddPrivilege, privilegeIds.Length) });
                    }
                    catch (ArgumentNullException exception)
                    {
                        ExceptionService.HandleException(exception, _defaultPolicy);
                        error = exception.Message;
                    }
                    catch (ArgumentException exception)
                    {
                        ExceptionService.HandleException(exception, _defaultPolicy);
                        error = exception.Message;
                    }
                    catch (UnauthorizedAccessException exception)
                    {
                        ExceptionService.HandleException(exception, _defaultPolicy);
                        error = exception.Message;
                    }
                    catch (PrivilegeException pExp)
                    {
                        ExceptionService.HandleException(pExp, _defaultPolicy);
                        error = pExp.Message;
                    }
                }

                return Json(new { Error = error });
            }

            return Json(new { Error = Resources.RolesResource.e_InvalidRequest });
        }

        /// <summary>
        /// This method is used to deletes the privileges from role.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="privilegeIds">list of privilege identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult DeletePrivilegesFromRole(string roleId, string[] privilegeIds, string tenantId)
        {
            if (Request.IsAjaxRequest())
            {
                if (string.IsNullOrEmpty(roleId))
                {
                    return Json(new { Error = Resources.RolesResource.e_RoleIdMandatory });
                }

                if (privilegeIds == null || privilegeIds.Length == 0)
                {
                    return Json(new { Error = Resources.RolesResource.e_PrivilegeIdMandatory });
                }

                if (string.IsNullOrEmpty(tenantId))
                {
                    tenantId = this.TenantId;
                }

                string error = Resources.RolesResource.e_DeletePrivilege;

                if (ModelState.IsValid)
                {
                    try
                    {
                        // delete the privileges from role
                        PrivilegeProxy.DeletePrivilegesForRole(tenantId, roleId, privilegeIds);

                        return Json(new { Success = string.Format(CultureInfo.InvariantCulture, Resources.RolesResource.s_DeletePrivilege, privilegeIds.Length) });
                    }
                    catch (ArgumentNullException exception)
                    {
                        ExceptionService.HandleException(exception, _defaultPolicy);
                    }
                    catch (ArgumentException exception)
                    {
                        ExceptionService.HandleException(exception, _defaultPolicy);
                    }
                    catch (UnauthorizedAccessException exception)
                    {
                        ExceptionService.HandleException(exception, _defaultPolicy);
                    }
                    catch (PrivilegeException ex)
                    {
                        ExceptionService.HandleException(ex, _defaultPolicy);
                        error = ex.Message;
                    }
                }

                return Json(new { Error = error });
            }

            return Json(new { Error = Resources.RolesResource.e_InvalidRequest });
        }

        /// <summary>
        /// This method is used to manage privilege details based on the given role identifier.
        /// </summary>
        /// <param name="roleId">role Identifier.</param>
        /// <returns>Returning the view</returns>
        public ActionResult ManagePrivilege(string roleId)
        {
            // check input parameters
            if (string.IsNullOrEmpty(roleId))
            {
                return HttpNotFound("Invalid role identifier");
            }
            else
            {
                try
                {
                    // get the role details
                    Role role = RoleProxy.GetRoleDetailsByRoleId(roleId);

                    if (role == null || string.IsNullOrEmpty(role.RoleId))
                    {
                        return HttpNotFound("Role not found!");
                    }
                    else
                    {
                        List<SelectListItem> lstModule = new List<SelectListItem>();
                        lstModule.Add(new SelectListItem { Value = "-1", Text = "- All -" });

                        // fetch modules by role service mapping
                        Dictionary<string, Service> roleServices = LicenseProxy.GetServicesByRoleId(roleId);

                        if (roleServices != null && roleServices.Count > 0)
                        {
                            var modules = LicenseProxy.GetModuleByServiceIds(roleServices.Keys.ToArray());

                            List<string> moduleIdFilterDuplicateContainer = new List<string>();

                            if (modules != null && modules.Count > 0)
                            {
                                // loop through all service modules
                                foreach (var item in modules.Values)
                                {
                                    // check for duplicate and add to module select list
                                    item.Values.ToList().ForEach(module =>
                                    {
                                        // check if already exists
                                        if (!moduleIdFilterDuplicateContainer.Contains(module.ModuleCode))
                                        {
                                            moduleIdFilterDuplicateContainer.Add(module.ModuleCode);

                                            lstModule.Add(new SelectListItem
                                            {
                                                Text = module.ModuleName,
                                                Value = module.ModuleCode
                                            });
                                        }
                                    });
                                }
                            }
                        }
                        else
                        {
                            // Fetching all the modules based on the Tenant License.
                            TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);

                            if (tenantLicense != null && tenantLicense.LicenseModuleDetails != null
                                && tenantLicense.LicenseModuleDetails.Count > 0)
                            {
                                lstModule.AddRange(tenantLicense.LicenseModuleDetails.Select(m => new SelectListItem
                                {
                                    Text = m.Value.ModuleName,
                                    Value = m.Key
                                }).OrderBy(t => t.Value));
                            }
                        }

                        ViewData["RoleName"] = role.RoleName;
                        ViewData["RoleId"] = role.RoleId;
                        ViewData["Modules"] = lstModule;
                    }
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    throw;
                }
                catch (RoleException ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (LicenseException ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
            }

            return View();
        }

        /// <summary>
        /// This method is used to get features by module identifiers.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <param name="moduleId">module identifier.</param>
        /// <returns></returns>
        public JsonResult GetFeaturesByModule(string roleId, string moduleId)
        {
            if (string.IsNullOrEmpty(roleId))
            {
                return Json(new { Error = Resources.RolesResource.e_RoleIdMandatory });
            }

            if (string.IsNullOrEmpty(moduleId))
            {
                return Json(new { Error = Resources.RolesResource.e_ModuleIdMandatory });
            }

            try
            {

                TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(this.TenantId);

                if (tenantLicense != null && tenantLicense.LicenseFeatureDetails != null)
                {
                    string[] moduleIds = new string[] { moduleId };

                    if (moduleId == "-1")
                    {
                        moduleIds = tenantLicense.LicenseModuleDetails.Keys.ToArray();
                    }

                    List<string> licenseFeatures = tenantLicense.LicenseFeatureDetails.Keys.ToList();
                    var moduleFeatures = LicenseProxy.GetFeatureByModuleIds(moduleIds);

                    if (moduleFeatures != null && moduleFeatures.Count > 0)
                    {
                        List<Feature> featureList = new List<Feature>();

                        foreach (var mid in moduleIds)
                        {
                            if (moduleFeatures.ContainsKey(mid) && moduleFeatures[mid] != null)
                            {
                                featureList.AddRange(moduleFeatures[mid].Values.Where(x => licenseFeatures.Contains(x.FeatureCode)));
                            }
                        }

                        return Json(featureList.Select(item => new
                        {
                            Text = item.FeatureName,
                            Value = item.FeatureCode
                        }));
                    }
                }
            }
            catch (ArgumentNullException lex)
            {
                ExceptionService.HandleException(lex, _defaultPolicy);
            }
            catch (ArgumentException lex)
            {
                ExceptionService.HandleException(lex, _defaultPolicy);
            }
            catch (UnauthorizedAccessException lex)
            {
                ExceptionService.HandleException(lex, _defaultPolicy);
            }
            catch (LicenseException lex)
            {
                ExceptionService.HandleException(lex, _defaultPolicy);
            }

            return Json(new { Error = Resources.RolesResource.e_FetchFeature });
        }

        #region Manage Privilege Private Methods

        /// <summary>
        /// This method is used to get the assignable privileges.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="moduleId">The module identifier.</param>
        /// <param name="featureId">feature identifier.</param>
        /// <returns>list of assignable privileges.</returns>
        private static Dictionary<string, Privilege> GetPrivilegesByModuleFeature(string tenantId, string moduleId, string featureId)
        {
            Dictionary<string, Privilege> featurePrivileges = null;

            // get privilege details for selected module & feature
            if (featureId != "-1")
            {
                featurePrivileges = PrivilegeProxy.GetPrivilegesbyFeatureIds(new string[] { featureId });
            }
            else
            {
                string[] moduleIds = null;

                // get privileges by module
                if (moduleId != "-1")
                {
                    moduleIds = new string[] { moduleId };
                }
                else
                {
                    var lic = LicenseProxy.GetTenantLicense(tenantId);

                    if (lic != null && lic.LicenseModuleDetails != null)
                    {
                        moduleIds = lic.LicenseModuleDetails.Values.Select(m => m.ModuleCode).ToArray();
                    }
                }

                if (moduleIds != null)
                {
                    featurePrivileges = PrivilegeProxy.GetPrivilegesbyModuleIds(moduleIds);
                }
            }

            return featurePrivileges;
        }

        /// <summary>
        /// This method is used to get the roles for tenant by given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public List<Role> GetRolesForTenant(string tenantId)
        {
            // Getting all the roles from the logged in tenant
            List<Role> Roles = new List<Role>();
            try
            {
                Roles = RoleProxy.GetTenantRoleDetails(tenantId);
                if (Roles != null)
                {
                    Roles.RemoveAll(r => r.RoleId.Equals(RoleConstants.ProductAdmin, StringComparison.Ordinal));
                }
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }
            catch (RoleException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }

            // Fetching Global Roles
            List<Role> GlobalRoles = null;
            try
            {
                // Get global roles accessible by the tenant [license]
                GlobalRoles = RoleProxy.GetGlobalRolesByTenant(this.TenantId);

                if (GlobalRoles != null && GlobalRoles.Count > 0)
                {
                    //Binding all the roles
                    Roles.AddRange(GlobalRoles);
                }
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }
            catch (RoleException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
            }
            return Roles.ToList();
        }

        /// <summary>
        /// This method is used to get the active tenant roles.
        /// The consumer for this method is in TenantUserAssociation.
        /// Do Not use this method elsewhere.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetActiveTenantRoles()
        {
            List<Role> tenantRoles = GetRolesForTenant(this.TenantId);

            if (tenantRoles == null || tenantRoles.Count < 1)
            {
                return Json(new { Status = Resources.RolesResource.e_NoRolesAvailable }, JsonRequestBehavior.DenyGet);
            }

            // remove all the global roles
            /* for (int i = 0; i < tenantRoles.Count; i++)
            {
                if (tenantRoles[i].IsGlobal)
                {
                    tenantRoles.RemoveAt(i);
                }
            }
             */
            return Json(tenantRoles, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// This method is used to determines whether [is global role] is true or false for the specified role identifier.
        /// </summary>
        /// <param name="roleId">role identifier.</param>
        /// <returns>
        /// 	<c>true</c> if [is global role] [the specified role id]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsGlobalRole(string roleId)
        {
            return roleId.StartsWith("GR$", StringComparison.Ordinal)
                && !roleId.Equals(RoleConstants.ServiceAdmin, StringComparison.Ordinal);
        }

        #endregion

        #endregion
    }
}
