﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using CelloSaaS.Integration.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Rules.Constants;
using CelloSaaS.Rules.Core;
using CelloSaaS.Rules.ServiceProxies;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.Configuration;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for business rules.
    /// </summary>
    public class RulesController : CelloController
    {
        private const string policyName = "RulesExceptionLogger";
        private string TenantId = TenantContext.GetTenantId("_BusinessRule");
        private string UserId = UserIdentity.UserId;

        /// <summary>
        /// This method is used to view manage business rule page for a specific rule set.
        /// </summary>
        /// <param name="ruleSetCode">rule set code.</param>
        /// <returns></returns>
        public ActionResult Index(string ruleSetCode)
        {
            ViewData["SelectRuleSetCode"] = ruleSetCode; // initially select this rule in the tree
            return View();
        }

        /// <summary>
        /// This method is used to manages the specified rule set code.
        /// </summary>
        /// <param name="ruleSetCode">rule set code.</param>
        /// <param name="screenType">type of the screen.</param>
        /// <returns></returns>
        public ActionResult Manage(string ruleSetCode, string screenType)
        {
            ViewData["RuleName"] = ruleSetCode;
            ViewData["ScreenType"] = screenType;
            ViewData["TenantId"] = this.TenantId;
            ViewData["CanCopyFromParent"] = false;

            try
            {
                RuleSetMetadata ruleMetadata = RuleSetMetadataProxy.GetRuleSetMetadata(ruleSetCode, this.TenantId);
                ViewData["RuleMetadata"] = ruleMetadata;

                if (ruleMetadata != null && string.IsNullOrEmpty(ruleMetadata.TenantId))
                {
                    // provide copy facility only for Global business rules
                    var brSettingValue = CelloSaaS.ServiceProxies.Configuration.ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.TenantId, BRConstants.ModuleConfigurationId);

                    if (!this.TenantId.Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase)
                        && !string.IsNullOrEmpty(brSettingValue)
                        && brSettingValue.Equals(ConfigurationSettingValueConstant.OWN_SETTING))
                    {
                        ViewData["CanCopyFromParent"] = true;
                    }
                }
                else if (ruleMetadata == null)
                {
                    ModelState.AddModelError("Error", Resources.RulesResource.e_RulesNotAvailable);
                }
            }
            catch (ModuleConfigurationSettingException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("", ex.Message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to resets the specified rule set code.
        /// </summary>
        /// <param name="ruleSetCode">rule set code.</param>
        /// <param name="screenType">type of the screen.</param>
        /// <returns></returns>
        public ActionResult Reset(string ruleSetCode, string screenType)
        {
            if (!string.IsNullOrEmpty(ruleSetCode))
            {
                try
                {
                    RuleSetMetadata ruleMetadata = RuleSetMetadataProxy.GetRuleSetMetadata(ruleSetCode, this.TenantId);

                    if (ruleMetadata == null)
                    {
                        TempData["Error"] = Resources.RulesResource.e_RulesNotAvailable;
                    }
                    else
                    {
                        RuleServiceProxy.DeleteRuleset(ruleSetCode, this.TenantId);
                        TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.RulesResource.s_Reset, ruleSetCode);
                    }
                }
                catch (Exception ex)
                {
                    TempData["Error"] = ex.Message;
                }
            }
            else
            {
                TempData["Error"] = "Invalid RuleSetCode provided!";
            }

            return RedirectToAction("Manage", new { ruleSetCode = ruleSetCode, screenType = screenType });
        }

        /// <summary>
        /// This method is used to copy the rule set from parent.
        /// </summary>
        /// <param name="ruleSetCode">rule set code.</param>
        /// <param name="screenType">type of the screen.</param>
        /// <returns></returns>
        public ActionResult CopyRulesetFromParent(string ruleSetCode, string screenType)
        {
            try
            {
                var success = RuleServiceProxy.CopyRulesetFromParent(ruleSetCode, this.TenantId, this.UserId);

                if (success)
                    TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.ConfigurationResource.s_CopyRuleset, ruleSetCode);
                else
                    TempData["Error"] = string.Format(CultureInfo.InvariantCulture, Resources.ConfigurationResource.e_RuleSetCode, ruleSetCode);
            }
            catch (BusinessRuleException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("Manage", new { ruleSetCode, screenType });
        }

        /// <summary>
        /// This method is used to copy all rules from parent.
        /// </summary>
        /// <returns></returns>
        public ActionResult CopyAllRulesFromParent()
        {
            try
            {
                int count = RuleServiceProxy.CopyAllRulesFromParent(this.TenantId, this.UserId);
                TempData["Success"] = string.Format(CultureInfo.InvariantCulture, Resources.ConfigurationResource.s_CopyRuleset, count);
            }
            catch (BusinessRuleException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to gets the rule definition based on the given rule set identifier.
        /// </summary>
        /// <param name="rulesetCode">rule set code.</param>
        /// <returns></returns>
        public PartialViewResult GetRuleDefinition(string rulesetCode)
        {
            if (!string.IsNullOrEmpty(rulesetCode))
            {
                try
                {
                    RuleSetMetadata ruleMetadata = RuleSetMetadataProxy.GetRuleSetMetadata(rulesetCode, this.TenantId);
                    ViewData["RuleMetadata"] = ruleMetadata;

                    if (ruleMetadata != null)
                    {
                        ViewData["RuleName"] = ruleMetadata.Code;
                        ViewData["ScreenType"] = ruleMetadata.ScreenType.ToString();
                        ViewData["TenantId"] = ruleMetadata.TenantId;
                    }
                }
                catch (BusinessRuleException ex)
                {
                    CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to gets the rule activity.
        /// </summary>
        /// <param name="rulesetCode">rule set code.</param>
        /// <returns></returns>
        public PartialViewResult GetRuleActivity(string rulesetCode)
        {
            if (!string.IsNullOrEmpty(rulesetCode))
            {
                ViewData["rulesetCode"] = rulesetCode;
                ViewData["CanAddActivity"] = true;

                try
                {
                    var ruleMetadata = RuleSetMetadataProxy.GetRuleSetMetadata(rulesetCode, this.TenantId);

                    if (ruleMetadata == null)
                    {
                        ModelState.AddModelError("Error", Resources.RulesResource.e_RulesetCode);
                    }
                    else
                    {
                        ViewData["RuleMetadata"] = ruleMetadata;
                        ViewData["CanAddActivity"] = !ruleMetadata.IsFixed || (string.IsNullOrEmpty(ruleMetadata.TenantId) && UserIdentity.IsInRole(RoleConstants.ProductAdmin));

                        Dictionary<string, RuleActivityDefinition> ruleActivityDefs = RuleSetMetadataProxy.GetRuleActivityDefinitions(rulesetCode, this.TenantId);
                        ViewData["RuleActivityDefs"] = ruleActivityDefs;

                        if (ruleActivityDefs != null && ruleActivityDefs.Any(x => x.Value.ActivityType == ActivityType.ServiceCallRuleActivity))
                        {
                            ViewData["ServiceEndpoints"] = CelloServiceEndpointProxy.GetServiceEndpointByTenantId(this.TenantId);
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
                catch (BusinessRuleException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to adds the rule activity.
        /// </summary>
        /// <param name="rulesetCode">rule set code.</param>
        /// <param name="activityType">type of the activity.</param>
        /// <returns></returns>
        public PartialViewResult AddRuleActivity(string rulesetCode, ActivityType activityType)
        {
            ViewData["rulesetCode"] = rulesetCode;
            ViewData["activityType"] = activityType;

            if (activityType == ActivityType.ServiceCallRuleActivity)
            {
                var selectList = new List<SelectListItem>();
                var eps = CelloServiceEndpointProxy.GetServiceEndpointByTenantId(this.TenantId);

                if (eps != null)
                {
                    selectList.AddRange(new SelectList(eps.Values, "Id", "Name"));
                }

                selectList.Insert(0, new SelectListItem { Value = "", Text = "Select" });

                ViewData["ServiceEndpointId"] = selectList;
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to adds the rule activity.
        /// </summary>
        /// <param name="activityDefinition">activity definition.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddRuleActivity(RuleActivityDefinition activityDefinition)
        {
            string error = ValidateActivityDefinition(activityDefinition);

            if (!string.IsNullOrEmpty(error))
            {
                return Json(new { Error = error });
            }

            try
            {
                var ruleMetadata = RuleSetMetadataProxy.GetRuleSetMetadata(activityDefinition.RulesetCode, this.TenantId);

                if (ruleMetadata == null)
                {
                    return Json(new { Error = Resources.RulesResource.e_RulesetCode });
                }

                //if ((ruleMetadata.IsFixed || string.IsNullOrEmpty(ruleMetadata.TenantId)) && !UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                //{
                //    // global rule activity can be added only by product admin
                //    return Json(new { Error = Resources.RulesResource.e_AddActivityAccessDenied });
                //}

                //activityDefinition.TenantId = string.IsNullOrEmpty(ruleMetadata.TenantId) ? string.Empty : this.TenantId;
                activityDefinition.TenantId = this.TenantId;
                activityDefinition.CreatedBy = this.UserId;
                activityDefinition.Status = true;

                RuleSetMetadataProxy.AddRuleActivity(activityDefinition);

                return Json(new { Success = Resources.RulesResource.s_AddActivity });
            }
            catch (ArgumentException ex)
            {
                CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
            catch (BusinessRuleException ex)
            {
                CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This is method is used to validates the activity definition.
        /// </summary>
        /// <param name="activityDefinition">activity definition.</param>
        /// <returns></returns>
        private static string ValidateActivityDefinition(RuleActivityDefinition activityDefinition)
        {
            string errorMessage = null;

            if (activityDefinition == null)
                errorMessage = Resources.RulesResource.e_InvalidInput;

            switch (activityDefinition.ActivityType)
            {
                case ActivityType.XMLRuleActivity:
                    if (string.IsNullOrEmpty(activityDefinition.ServiceOutputXML))
                    {
                        errorMessage = Resources.RulesResource.e_ValidXml;
                    }
                    else
                    {
                        try
                        {
                            var doc = new XmlDocument();
                            doc.LoadXml(activityDefinition.ServiceOutputXML);
                        }
                        catch (XmlException xmlException)
                        {
                            errorMessage = xmlException.Message;
                        }
                    }
                    break;

                case ActivityType.ServiceCallRuleActivity:
                    if (string.IsNullOrEmpty(activityDefinition.ServiceEndpointId))
                    {
                        errorMessage = Resources.RulesResource.e_ServiceEndpoindInvalid;
                    }
                    break;

                case ActivityType.OpenRuleActivity:
                case ActivityType.EntityRuleActivity:
                    if (string.IsNullOrEmpty(activityDefinition.AssemblyType))
                    {
                        errorMessage = Resources.RulesResource.e_AssemblyTypeName;
                    }
                    else if (activityDefinition.AssemblyType.Split(',').Length != 2)
                    {
                        errorMessage = Resources.RulesResource.e_ValueFormat;
                    }
                    break;
            }

            return errorMessage;
        }

        /// <summary>
        /// This method is used to deletes the rule activity.
        /// </summary>
        /// <param name="rulesetCode">rule set code.</param>
        /// <param name="activityId">activity identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteRuleActivity(string rulesetCode, string activityId)
        {
            if (string.IsNullOrEmpty(rulesetCode))
            {
                return Json(new { Error = Resources.RulesResource.e_RulesetCodeEmpty });
            }

            if (string.IsNullOrEmpty(activityId))
            {
                return Json(new { Error = Resources.RulesResource.e_RuleActivityIdEmpty });
            }

            try
            {
                Dictionary<string, RuleActivityDefinition> ruleActivityDefs = RuleSetMetadataProxy.GetRuleActivityDefinitions(rulesetCode, this.TenantId);

                if (ruleActivityDefs == null || !ruleActivityDefs.ContainsKey(activityId))
                {
                    return Json(new { Error = Resources.RulesResource.e_RuleActivityInvalid });
                }

                if (string.IsNullOrEmpty(ruleActivityDefs[activityId].TenantId) && !UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                {
                    // global rule activity can be added only by product admin
                    return Json(new { Error = Resources.RulesResource.e_DeleteRuleActivityAccessDenied });
                }

                RuleSetMetadataProxy.DeleteRuleActivity(activityId, rulesetCode, ruleActivityDefs[activityId].TenantId);

                return Json(new { Success = Resources.RulesResource.s_DeleteRuleActivity });
            }
            catch (BusinessRuleException ex)
            {
                CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to adds the rule set.
        /// </summary>
        /// <param name="ruleSetMetadata">rule set metadata.</param>
        /// <returns></returns>
        public JsonResult AddRuleset(RuleSetMetadata ruleSetMetadata)
        {
            if (ruleSetMetadata == null || string.IsNullOrEmpty(ruleSetMetadata.Code) || string.IsNullOrEmpty(ruleSetMetadata.Name))
            {
                return Json(new { Error = Resources.RulesResource.e_RulesetCodeOrNameEmpty });
            }

            try
            {
                bool isGlobal = (!string.IsNullOrEmpty(Request["isGlobal"]) && Request["isGlobal"] == "on");

                if (isGlobal && !UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                {
                    // global rule can be added only by product admin
                    return Json(new { Error = Resources.RulesResource.e_AddGlobalRuleAccessDenied });
                }

                ruleSetMetadata.TenantId = isGlobal ? null : this.TenantId;
                ruleSetMetadata.CreatedBy = this.UserId;
                ruleSetMetadata.Status = true;
                ruleSetMetadata.Id = RuleSetMetadataProxy.CreateRuleSetMetadata(ruleSetMetadata);

                return Json(new { Success = Resources.RulesResource.s_CreateRule });
            }
            catch (BusinessRuleException ex)
            {
                CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to updates the rule set.
        /// </summary>
        /// <param name="ruleSetMetadata">rule set metadata.</param>
        /// <returns></returns>
        public JsonResult UpdateRuleset(RuleSetMetadata ruleSetMetadata)
        {
            if (ruleSetMetadata == null || string.IsNullOrEmpty(ruleSetMetadata.Code) || string.IsNullOrEmpty(ruleSetMetadata.Name))
            {
                return Json(new { Error = Resources.RulesResource.e_RulesetCodeOrNameEmpty });
            }

            try
            {
                bool isGlobal = string.IsNullOrEmpty(ruleSetMetadata.TenantId);

                if (isGlobal && !UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                {
                    // global rule can be added only by product admin
                    return Json(new { Error = Resources.RulesResource.e_UpdateGlobalRuleAccessDenied });
                }

                // ruleSetMetadata.TenantId = this.TenantId;
                ruleSetMetadata.UpdatedBy = this.UserId;
                ruleSetMetadata.Status = true;
                RuleSetMetadataProxy.UpdateRuleSetMetadata(ruleSetMetadata);

                return Json(ruleSetMetadata);
            }
            catch (BusinessRuleException ex)
            {
                CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to deletes the rule set.
        /// </summary>
        /// <param name="rulesetCode">The rule set code.</param>
        /// <returns></returns>
        public JsonResult DeleteRuleset(string rulesetCode)
        {
            if (string.IsNullOrEmpty(rulesetCode))
            {
                return Json(new { Error = Resources.RulesResource.e_RulesetCodeEmpty });
            }

            try
            {
                var ruleMetadata = RuleSetMetadataProxy.GetRuleSetMetadata(rulesetCode, this.TenantId);

                if (ruleMetadata == null)
                {
                    return Json(new { Error = Resources.RulesResource.e_RuleSet });
                }

                if ((ruleMetadata.IsFixed || string.IsNullOrEmpty(ruleMetadata.TenantId))
                    && !UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                {
                    // global rule can be deleted only by product admin
                    return Json(new { Error = Resources.RulesResource.e_DeleteGlobalRuleDenied });
                }

                RuleSetMetadataProxy.DeleteRuleSetMetadata(rulesetCode, ruleMetadata.TenantId);

                return Json(new { Success = Resources.RulesResource.s_DeleteRuleset });
            }
            catch (BusinessRuleException ex)
            {
                CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to get all rule metadata.
        /// </summary>
        /// <param name="categoryid">category identifier.</param>
        /// <returns></returns>
        public JsonResult GetAllRuleMetadata(string categoryid)
        {
            Dictionary<string, RuleSetMetadata> lstRules = null;

            try
            {
                lstRules = RuleSetMetadataProxy.GetAllRuleSetMetadata(this.TenantId);
            }
            catch (BusinessRuleException ex)
            {
                CelloSaaS.Library.ExceptionService.HandleException(ex, policyName);
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            if (lstRules != null)
            {
                // group by rule category
                var categoryRules = new Dictionary<string, List<RuleSetMetadata>>();

                foreach (var item in lstRules.Values)
                {
                    var category = item.Category ?? "Default";

                    if (!categoryRules.ContainsKey(category))
                    {
                        categoryRules.Add(category, new List<RuleSetMetadata>());
                    }

                    categoryRules[category].Add(item);
                }

                var rslt = new List<object>();

                foreach (var item in categoryRules)
                {
                    var childs = from x in item.Value
                                 let classVal = (x.ScreenType.ToString() + (x.IsFixed ? " fixed" : string.Empty))
                                 select new
                                 {
                                     data = new { title = x.Name, icon = "/Content/images/" + x.ScreenType.ToString() + ".png" },
                                     attr = new { @class = classVal, id = x.Code, title = x.Description, rel = string.IsNullOrEmpty(x.TenantId) },
                                     metadata = new { pkid = x.Id, screentype = x.ScreenType.ToString() },
                                     state = string.Empty,
                                 };

                    rslt.Add(new
                    {
                        data = new { title = item.Key, icon = "/Content/images/category-ico.png" },
                        attr = new { @class = "category", rel = "category", id = item.Key },
                        children = childs,
                        state = "opened"
                    });
                }

                var root = new
                    {
                        data = new { title = "Rulesets", icon = "/Content/images/rules-root-ico.png" },
                        attr = new { @class = "root", rel = "root", id = "rules" },
                        children = rslt,
                        state = "opened"
                    };

                return Json(root, JsonRequestBehavior.AllowGet);
            }


            return Json(null, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to test the business rules via XML inputs.
        /// </summary>
        /// <param name="ruleSetCode">rule set code.</param>
        /// <param name="inputXml">input XML.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult TestRule(string ruleSetCode, string inputXml)
        {
            ViewBag.ruleSetCode = ruleSetCode;
            ViewBag.inputXml = inputXml;

            if (string.IsNullOrEmpty(ruleSetCode))
            {
                ModelState.AddModelError("ruleSetCode", Resources.RulesResource.e_RulesetCode);
            }

            if (this.Request.HttpMethod == "POST")
            {
                if (string.IsNullOrEmpty(inputXml))
                {
                    ModelState.AddModelError("inputXml", Resources.RulesResource.e_ValidXml);
                }
                else
                {
                    try
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(inputXml);
                    }
                    catch (XmlException ex)
                    {
                        ModelState.AddModelError("Error", Resources.RulesResource.e_XmlInvalid + ex.ToString());
                    }
                }
            }

            try
            {
                ViewBag.ruleVariableXml = RuleServiceProxy.GetRuleVariablesXml(ruleSetCode, this.TenantId);

                if (ModelState.IsValid && this.Request.HttpMethod == "POST")
                {
                    // execute the rule
                    ViewBag.outputXml = RuleServiceProxy.ExecuteXmlRule(ruleSetCode, this.TenantId, inputXml);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.ToString());
            }

            return View();
        }
    }
}
