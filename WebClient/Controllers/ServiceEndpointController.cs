﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using CelloSaaS.EncryptionProvider;
using CelloSaaS.Library;
using CelloSaaS.Library.Encryption;
using CelloSaaS.Rules.Core;
using CelloSaaS.Rules.Execution;
using CelloSaaS.View;
using CelloSaaS.Integration;
using CelloSaaS.Integration.ServiceProxies;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for service endpoints.
    /// </summary>
    public class ServiceEndpointController : CelloController
    {
        private const string policyName = "GlobalExceptionLogger";
        private string TenantId = TenantContext.GetTenantId("_ServiceEndpoint");
        private string UserId = UserIdentity.UserId;

        /// <summary>
        /// This method is used to get the web hooks management view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            Dictionary<string, CelloServiceEndpoint> endpoints = null;
            ViewBag.CanAdd = false;
            ViewBag.CanEdit = false;
            ViewBag.CanDelete = false;

            try
            {
                endpoints = CelloServiceEndpointProxy.GetServiceEndpointByTenantId(this.TenantId); // get all service endpoint for the tenant

                var userPrivileges = AccessControlProxy.GetTenantAccessPrivileges(this.UserId, new string[] { PrivilegeConstants.AddEndpoint, PrivilegeConstants.EditEndpoint, PrivilegeConstants.DeleteEndpoint });

                if (userPrivileges != null && userPrivileges.Count > 0)
                {
                    if (userPrivileges.ContainsKey(PrivilegeConstants.AddEndpoint) && userPrivileges[PrivilegeConstants.AddEndpoint].ContainsKey(this.TenantId))
                    {
                        ViewBag.CanAdd = true;
                    }

                    if (userPrivileges.ContainsKey(PrivilegeConstants.EditEndpoint) && userPrivileges[PrivilegeConstants.EditEndpoint].ContainsKey(this.TenantId))
                    {
                        ViewBag.CanEdit = true;
                    }

                    if (userPrivileges.ContainsKey(PrivilegeConstants.DeleteEndpoint) && userPrivileges[PrivilegeConstants.DeleteEndpoint].ContainsKey(this.TenantId))
                    {
                        ViewBag.CanDelete = true;
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (CelloServiceEndpointException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(endpoints);
        }

        /// <summary>
        /// This method is used to Add/Edit service endpoint.
        /// </summary>
        /// <param name="endpointId">endpoint identifier to edit.</param>
        /// <returns></returns>
        public ActionResult Manage(string endpointId)
        {
            CelloServiceEndpoint endpoint = null;

            try
            {
                if (!string.IsNullOrEmpty(endpointId))
                {
                    endpoint = CelloServiceEndpointProxy.GetServiceEndpoint(endpointId, this.TenantId);

                    ViewData["webServiceType"] = (endpoint != null && endpoint.SoapMetadata != null && !string.IsNullOrEmpty(endpoint.SoapMetadata.SoapAction)) ? "SOAP" : "REST";

                    // if sts endpoint url is present then enable the federation checkbox
                    ViewData["EnableFederation"] = endpoint != null && endpoint.SoapMetadata != null && !string.IsNullOrEmpty(endpoint.SoapMetadata.StsEndpointAddress);
                }
                else
                {
                    endpoint = new CelloServiceEndpoint();
                    endpoint.SoapMetadata = new SoapMetadata();
                    endpoint.RestMetadata = new RestMetadata();
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (CelloServiceEndpointException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(endpoint);
        }

        /// <summary>
        /// This method is sued to save the service endpoint metadata.
        /// </summary>
        /// <param name="endpoint">endpoint.</param>
        /// <param name="forms">forms.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Manage(CelloServiceEndpoint endpoint, FormCollection forms)
        {
            string webServiceType  = forms["webServiceType"];
            ViewData["webServiceType"] = webServiceType;

            ValidateEndpointMetadata(endpoint);

            try
            {
                if (ModelState.IsValid)
                {
                    endpoint.TenantId = this.TenantId;

                    if (webServiceType == "SOAP")
                    {
                        endpoint.RestMetadata = null;
                    }
                    else
                    {
                        endpoint.SoapMetadata = null;
                    }

                    if (string.IsNullOrEmpty(endpoint.Id))
                    {
                        endpoint.CreatedBy = this.UserId;
                        endpoint.Status = true;
                        CelloServiceEndpointProxy.AddServiceEndpoint(endpoint);
                        TempData["Success"] = "Service Endpoint details added successfully!";
                    }
                    else
                    {
                        endpoint.UpdatedBy = this.UserId;
                        CelloServiceEndpointProxy.UpdateServiceEndpoint(endpoint);
                        TempData["Success"] = "Service Endpoint details updated successfully!";
                    }

                    return RedirectToAction("Index");
                }
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (CelloServiceEndpointException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(endpoint);
        }

        /// <summary>
        /// This method is used to validates the incoming form endpoint metadata values.
        /// </summary>
        /// <param name="endpoint">endpoint.</param>
        private void ValidateEndpointMetadata(CelloServiceEndpoint endpoint)
        {
            if (string.IsNullOrEmpty(endpoint.Name))
            {
                ModelState.AddModelError("Name", "Endpoint name cannot be empty!");
            }

            if (string.IsNullOrEmpty(endpoint.EndpointAddress))
            {
                ModelState.AddModelError("EndpointAddress", "Endpoint Address cannot be empty!");
            }
            else if (!Util.ValidateURL(endpoint.EndpointAddress))
            {
                ModelState.AddModelError("EndpointAddress", "Enter a valid Endpoint Address!");
            }

            if (ViewData["webServiceType"] !=null && ViewData["webServiceType"].ToString() == "REST" && endpoint.RestMetadata != null)
            {
                // validate REST model
            }

            if (ViewData["webServiceType"] != null && ViewData["webServiceType"].ToString() == "SOAP" && endpoint.SoapMetadata != null)
            {
                var soapMetadata = endpoint.SoapMetadata;

                if (!string.IsNullOrEmpty(soapMetadata.MessageModifierAddress) && !Util.ValidateURL(soapMetadata.MessageModifierAddress))
                {
                    ModelState.AddModelError("SoapMetadata.MessageModifierAddress", "Enter a valid message modifier endpoint address!");
                }

                if (soapMetadata.CredentialType == CredentialType.None)
                {
                    soapMetadata.UserName = soapMetadata.Password = null;
                }

                if (soapMetadata.CredentialType == CredentialType.UserName)
                {
                    if (!string.IsNullOrEmpty(soapMetadata.UserName) && string.IsNullOrEmpty(soapMetadata.Password))
                    {
                        ModelState.AddModelError("SoapMetadata.Password", "Password cannot be null if username is given.");
                    }

                    if (!string.IsNullOrEmpty(soapMetadata.Password) && string.IsNullOrEmpty(soapMetadata.UserName))
                    {
                        ModelState.AddModelError("SoapMetadata.UserName", "UserName cannot be null if password is given.");
                    }
                }

                string enableFederation = this.Request.Form["EnableFederation"];

                ViewData["EnableFederation"] = enableFederation;

                if (!string.IsNullOrEmpty(enableFederation) && enableFederation == "True")
                {
                    if (string.IsNullOrEmpty(soapMetadata.StsEndpointAddress))
                    {
                        ModelState.AddModelError("SoapMetadata.StsEndpointAddress", "STS Endpoint Address cannot be empty!");
                    }
                    else if (!Util.ValidateURL(soapMetadata.StsEndpointAddress))
                    {
                        ModelState.AddModelError("SoapMetadata.StsEndpointAddress", "STS Endpoint Address is not in valid url format!");
                    }

                    if (!string.IsNullOrEmpty(soapMetadata.RealmAddress) && !Util.ValidateURL(soapMetadata.RealmAddress))
                    {
                        ModelState.AddModelError("SoapMetadata.RealmAddress", "Enter a valid Federation Realm Endpoint Address!");
                    }
                }

                if (soapMetadata.CredentialType != CredentialType.None)
                {
                    if (string.IsNullOrEmpty(soapMetadata.ServiceCertificate))
                    {
                        ModelState.AddModelError("SoapMetadata.ServiceCertificate", "Encryption certificate is required!");
                    }
                    else
                    {
                        try
                        {
                            new X509Certificate2(Convert.FromBase64String(soapMetadata.ServiceCertificate));
                        }
                        catch (CryptographicException ex)
                        {
                            ExceptionService.HandleException(ex, policyName);
                            ModelState.AddModelError("SoapMetadata.ServiceCertificate", ex.Message);
                        }
                        catch (FormatException ex)
                        {
                            ExceptionService.HandleException(ex, policyName);
                            ModelState.AddModelError("SoapMetadata.ServiceCertificate", ex.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method is sued to deletes the specified endpoint identifier.
        /// </summary>
        /// <param name="endpointId">endpoint identifier.</param>
        /// <returns></returns>
        public ActionResult Delete(string endpointId)
        {
            try
            {
                if (!string.IsNullOrEmpty(endpointId))
                {
                    CelloServiceEndpointProxy.DeleteServiceEndpoint(endpointId, this.TenantId);
                    TempData["Success"] = "Service Endpoint details deleted successfully!";
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = ex.Message;
            }
            catch (CelloServiceEndpointException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                TempData["Error"] = ex.Message;
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to test the web service.
        /// </summary>
        /// <param name="endpointId">endpoint identifier.</param>
        /// <param name="inputXml">input xml.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult TestService(string endpointId, string inputXml)
        {
            ViewBag.endpointId = endpointId;
            ViewBag.inputXml = inputXml;

            if (string.IsNullOrEmpty(endpointId))
            {
                ModelState.AddModelError("endpointId", "Invalid endpoint identitifer");
            }

            if (this.Request.HttpMethod == "POST")
            {
                if (string.IsNullOrEmpty(inputXml))
                {
                    ModelState.AddModelError("inputXml", "Enter a valid input xml.");
                }
                else
                {
                    try
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(inputXml);
                    }
                    catch (XmlException ex)
                    {
                        ExceptionService.HandleException(ex, policyName);
                        ModelState.AddModelError("Error", "Given xml is not valid! Exception: " + ex.ToString());
                    }
                }
            }

            try
            {
                ViewBag.entity = CelloServiceEndpointProxy.GetServiceEndpoint(endpointId, this.TenantId);

                if (ModelState.IsValid && this.Request.HttpMethod == "POST")
                {
                    ViewBag.OutputXml = CelloServiceEndpointProxy.InvokeServiceById(endpointId, this.TenantId, inputXml);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (CelloServiceEndpointException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", ex.ToString());
            }

            return View();
        }
    }
}
