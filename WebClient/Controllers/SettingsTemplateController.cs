﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model.MasterData;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.ViewManagement;
using CelloSaaS.ServiceProxies.MasterData;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.ViewManagement;
using Resources;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for settings template.
    /// </summary>
    public class SettingsTemplateController : CelloSaaS.View.CelloController
    {
        private string SettingsTemplateTenantId = TenantContext.GetTenantId(new SettingsTemplate().EntityIdentifier);
        private string TenantSettingsTemplateTenantId = TenantContext.GetTenantId(new TenantSettingsTemplate().EntityIdentifier);

        private const string _defaultPolicy = "GlobalExceptionLogger";
        private const string DefaultLogoImagePath = "LogoImageFolder";

        /// <summary>
        /// This method is used to get the manage settings templates view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<SettingsTemplate> settingsTemplateList = new List<SettingsTemplate>();
            try
            {
                var settingsTemplate = SettingsTemplateProxy.GetSettingsTemplatesForTenant(SettingsTemplateTenantId);
                if (settingsTemplate != null)
                {
                    settingsTemplateList = settingsTemplate.Values.ToList();
                }
            }
            catch (ArgumentNullException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_SettingsTemplateLoad);
            }

            if (TempData["Message"] != null && !string.IsNullOrEmpty(TempData["Message"].ToString()))
            {
                ModelState.AddModelError("SettingsTemplateSuccess", TempData["Message"].ToString());
            }

            return View(settingsTemplateList);
        }

        /// <summary>
        /// This method is sued to manages the settings templates based on the given settings template identifier.
        /// </summary>
        /// <param name="settingTemplateId">setting template identifier.</param>
        /// <returns></returns>
        public ActionResult ManageSettingsTemplates(string settingTemplateId)
        {
            SettingsTemplate settingsTemplate = new SettingsTemplate();





            try
            {
                SettingsMetaData settingsMetaData = GetSettingsMetaData();
                if (!string.IsNullOrEmpty(settingTemplateId))
                {
                    Guid tempId;
                    if (!Guid.TryParse(settingTemplateId, out tempId))
                    {
                        return HttpNotFound("The settings template not found");
                    }

                    settingsTemplate = SettingsTemplateProxy.GetSettingsTemplateDetails(settingTemplateId);
                    if (settingsTemplate != null && settingsTemplate.Attributes != null)
                    {
                        settingsTemplate.Attributes.ForEach(x => x.IsSelected = true);
                    }
                    else
                    {
                        return HttpNotFound("The settings template not found");
                    }
                }
                else
                {
                    settingsTemplate.Attributes = (from sm in settingsMetaData.AttributeMetaData.Values
                                                   select new SettingsAttribute()
                                                       {
                                                           AttributeSetId = sm.AttributeSetId,
                                                           AttributeId = sm.AttributeId,
                                                           AttributeValue = string.Empty
                                                       }).ToList();
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AccessDenied);
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (SettingsMetaDataException settingsMetaDataException)
            {
                ExceptionService.HandleException(settingsMetaDataException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AttributeMetaData);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ManageSettingsTemplate);
            }

            SetTemplateDefaultValues(settingsTemplate.Attributes);
            return View(settingsTemplate);
        }

        /// <summary>
        /// This method is used to manages the settings templates.
        /// </summary>
        /// <param name="settingsTemplate">settings template.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageSettingsTemplates(SettingsTemplate settingsTemplate)
        {
            try
            {
                GetSettingsMetaData();

                List<SettingsAttribute> tempSettingsAttribute = new List<SettingsAttribute>();
                settingsTemplate.Attributes.Where(x => x.IsSelected).ToList().ForEach(x => tempSettingsAttribute.Add(x));
                settingsTemplate.Attributes = tempSettingsAttribute;

                ValidateSettingsTemplateDetails(settingsTemplate);

                if (ModelState.IsValid)
                {
                    settingsTemplate.TenantId = !settingsTemplate.IsGlobal ? SettingsTemplateTenantId : string.Empty;
                    settingsTemplate.CreatedBy = UserIdentity.UserId;

                    LogoUpload(settingsTemplate.Attributes, SettingsTemplateTenantId);

                    if (settingsTemplate.Id == null || string.IsNullOrEmpty(settingsTemplate.Id.ToString()))
                    {
                        string templateId = SettingsTemplateProxy.AddSettingsTemplate(settingsTemplate);
                        settingsTemplate.Id = Guid.Parse(templateId);
                    }
                    else
                    {
                        settingsTemplate.UpdatedBy = UserIdentity.UserId;
                        SettingsTemplateProxy.UpdateSettingsTemplate(settingsTemplate);
                    }

                    TempData["Message"] = Resources.SettingsTemplateResource.s_SettingsAttributeSave;
                    return RedirectToAction("Index");
                }
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (DuplicateTemplateName duplicateTemplateName)
            {
                ExceptionService.HandleException(duplicateTemplateName, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_DuplicateTemplateName);
            }
            catch (DuplicateAttributeTemplateException duplicateAttributeException)
            {
                ExceptionService.HandleException(duplicateAttributeException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_DuplicateTemplateAttribute);
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AccessDenied);
            }
            catch (SettingsMetaDataException settingsMetaDataException)
            {
                ExceptionService.HandleException(settingsMetaDataException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AttributeMetaData);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_SettingsTemplateSave);
            }

            SetTemplateDefaultValues(settingsTemplate.Attributes);
            return View(settingsTemplate);
        }

        /// <summary>
        /// This method is used to manage the logo upload.
        /// </summary>
        /// <param name="attributes">list of attributes.</param>
        /// <param name="tenantId">tenant identifier.</param>
        private void LogoUpload(List<SettingsAttribute> attributes, string tenantId)
        {
            var logoAttribute = attributes.Where(a => a.AttributeId.Equals(AttributeConstants.Logo)).SingleOrDefault();

            if (logoAttribute == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(logoAttribute.AttributeId) && Request.Files.Count > 0 &&
                !string.IsNullOrEmpty(Request.Files[0].FileName) && !string.IsNullOrEmpty(logoAttribute.AttributeValue))
            {
                string path = this.Server.MapPath(@"..\" + (ConfigHelper.LogoImageFolderPath ?? DefaultLogoImagePath) + @"\");

                string fileExtension = new FileInfo(Request.Files[0].FileName).Extension;

                string logoFile = tenantId + "_logo" + fileExtension;

                IFileStorageService fileStorageService = (IFileStorageService)ServiceLocator.GetServiceImplementation(typeof(IFileStorageService));
                fileStorageService.StoreFile(path, logoFile, 0);

                logoAttribute.AttributeValue = logoFile;
            }
        }

        /// <summary>
        /// This method is used to manages the tenant settings template based on the settings template identifier.
        /// </summary>
        /// <param name="settingTemplateId">setting template identifier.</param>
        /// <returns></returns>
        public ActionResult ManageTenantSettingsTemplate(string settingTemplateId)
        {
            TenantSettingsTemplate tenantSettingsTemplate = new TenantSettingsTemplate();
            try
            {
                SettingsMetaData settingsMetaData = GetSettingsMetaData();

                if (!string.IsNullOrEmpty(settingTemplateId))
                {
                    tenantSettingsTemplate = SettingsTemplateProxy.GetTenantTemplateDetails(TenantSettingsTemplateTenantId, settingTemplateId);
                }
                else
                {
                    tenantSettingsTemplate.Attributes = (from sm in settingsMetaData.AttributeMetaData.Values
                                                         select new SettingsAttribute()
                                                         {
                                                             AttributeSetId = sm.AttributeSetId,
                                                             AttributeId = sm.AttributeId,
                                                             AttributeValue = null
                                                         }).ToList();
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AccessDenied);
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (SettingsMetaDataException settingsMetaDataException)
            {
                ExceptionService.HandleException(settingsMetaDataException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AttributeMetaData);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ManageTenantSettingsTemplate);
            }

            SetTemplateDefaultValues(tenantSettingsTemplate.Attributes);
            return View(tenantSettingsTemplate);
        }

        /// <summary>
        /// This method is used to manages the tenant settings templates.
        /// </summary>
        /// <param name="tenantSettingsTemplate">settings template.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageTenantSettingsTemplate(TenantSettingsTemplate tenantSettingsTemplate)
        {
            string message = string.Empty;
            try
            {
                ValidateSettingsAttributes(tenantSettingsTemplate.Attributes, tenantSettingsTemplate.SettingsTemplate.TemplateType);

                if (this.ModelState.IsValid)
                {
                    tenantSettingsTemplate.Attributes.Where(x => string.IsNullOrEmpty(x.AttributeValue)).ToList().ForEach(x => x.AttributeValue = string.Empty);

                    LogoUpload(tenantSettingsTemplate.Attributes, TenantSettingsTemplateTenantId);

                    SettingsTemplateProxy.UpdateTenantSettingsTemplate(tenantSettingsTemplate);
                    this.TempData["Success"] = Resources.SettingsTemplateResource.s_TenantSettingsTemplateSave;
                    return this.RedirectToAction("TenantSettingsTemplate");
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_AccessDenied;
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_ParameterEmptyOrNull;
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_TenantSettingsTemplateSave;
            }
            TempData["Error"] = message;
            SetTemplateDefaultValues(tenantSettingsTemplate.Attributes);
            return View(tenantSettingsTemplate);
        }

        /// <summary>
        /// This method is used for tenant settings template.
        /// </summary>
        /// <returns></returns>
        public ActionResult TenantSettingsTemplate()
        {
            if (TempData["Success"] != null && !string.IsNullOrEmpty(TempData["Success"].ToString()))
            {
                ModelState.AddModelError("SettingsTemplateSuccess", TempData["Success"].ToString());
            }
            if (TempData["Error"] != null && !string.IsNullOrEmpty(TempData["Error"].ToString()))
            {
                ModelState.AddModelError("SettingsTemplateError", TempData["Error"].ToString());
            }
            ViewData["TenantId"] = TenantSettingsTemplateTenantId;
            return View();
        }

        /// <summary>
        /// This method is used for tenant settings template list.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult TenantSetingsTemplateList()
        {
            if (TempData["Message"] != null && !string.IsNullOrEmpty(TempData["Message"].ToString()))
            {
                ModelState.AddModelError("SettingsTemplateSuccess", TempData["Message"].ToString());
            }

            ViewData["TenantId"] = TenantSettingsTemplateTenantId;

            Dictionary<string, TenantSettingsTemplate> settingsTemplateList = SettingsTemplateProxy.GetTenantSettingsTemplates(TenantSettingsTemplateTenantId);

            return settingsTemplateList != null ? this.PartialView(settingsTemplateList.Values.ToList()) : this.PartialView(null);
        }

        /// <summary>
        /// This method is used to deletes the tenant settings template based on the template identifier
        /// </summary>
        /// <param name="templateId"> template identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteTenantSettingsTemplate(string templateId)
        {
            string message = string.Empty;
            try
            {
                SettingsTemplateProxy.DeleteTenantSettingsTemplate(TenantSettingsTemplateTenantId, templateId);
                TempData["Success"] = Resources.SettingsTemplateResource.s_TenantSettingsTemplateDelete;
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_ParameterEmptyOrNull;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_AccessDenied;
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                message = Resources.SettingsTemplateResource.e_TenantSettingsTemplateDelete;
            }

            TempData["Error"] = message;
            return RedirectToAction("TenantSettingsTemplate");
        }

        /// <summary>
        /// This method is used to lists the tenant settings template.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult TenantTemplatesList()
        {
            ListAllTemplates();
            return PartialView("~/Views/Shared/TenantTemplates/TenantTemplatesList.ascx");
        }

        /// <summary>
        /// This method is used to get all settings template.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult GetAllTemplates()
        {
            ListAllTemplates(true);
            return PartialView("~/Views/Shared/TenantTemplates/TenantTemplatesList.ascx");
        }

        /// <summary>
        /// This method is used to lists all Settings templates.
        /// </summary>
        /// <returns>collection of all templates.</returns>
        private Dictionary<string, string> ListAllTemplates(bool getAllTemplates = false)
        {
            Dictionary<string, SettingsTemplate> settingsTemplateList = null;

            if (getAllTemplates)
            {
                settingsTemplateList = SettingsTemplateProxy.GetSettingsTemplatesForTenant(UserIdentity.TenantID);
            }
            else
            {
                if (TenantSettingsTemplateTenantId.Equals(UserIdentity.LoggedInUserTenantId))
                {
                    settingsTemplateList = SettingsTemplateProxy.GetAssignableTemplateForTenant(TenantSettingsTemplateTenantId);
                }
                else
                {
                    settingsTemplateList = SettingsTemplateProxy.GetAssignableTemplateForTenantByOtherTenant(TenantSettingsTemplateTenantId, UserIdentity.LoggedInUserTenantId);
                }
            }

            var templatesList = settingsTemplateList != null ? settingsTemplateList.Values.ToList() : null;

            var settingsTemplates = new Dictionary<string, string>();
            settingsTemplates.Add("", Resources.SettingsTemplateResource.ChooseTemplate);

            if (templatesList != null && templatesList.Count > 0)
            {
                foreach (var item in templatesList)
                {
                    settingsTemplates.Add(item.Id.ToString(), item.Name);
                }
            }

            ViewData["settingsTemplateId"] = new SelectList(settingsTemplates, "Key", "Value", null);

            return settingsTemplates;
        }

        /// <summary>
        /// This method is used to adds the tenant settings template.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddTenantSettingsTemplate()
        {
            //var settingsTemplates = ListAllTemplates();

            if (TempData["SettingsTemplateError"] != null && !string.IsNullOrEmpty(TempData["SettingsTemplateError"].ToString()))
            {
                ModelState.AddModelError("SettingsTemplateError", TempData["SettingsTemplateError"].ToString());
            }

            if (TempData["settingsTemplate"] != null && TempData["SettingsTemplateError"] == null)
            {
                var settingTemplate = (SettingsTemplate)TempData["settingsTemplate"];
                SetTemplateDefaultValues(settingTemplate.Attributes);
                ViewData["SettingsAttribute"] = settingTemplate.Attributes.ToDictionary(t => t.AttributeId, t => t);

                //ViewData["settingsTemplateId"] = new SelectList(settingsTemplates, "Key", "Value", settingTemplate.Id);
                return View(TempData["settingsTemplate"]);
            }

            return View();
        }

        /// <summary>
        /// This method is used to get the tenants settings template details based on the settings template identifier.
        /// </summary>
        /// <param name="settingsTemplateId">settings template identifier.</param>
        /// <param name="forTenantProvisioning">forTenantProvisioning (true/false).</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult TenantSettingsTemplateDetails(string settingsTemplateId, bool forTenantProvisioning = false)
        {
            ViewData["tenantProvisioning"] = forTenantProvisioning;

            SettingsTemplate settingsTemplate = new SettingsTemplate();

            try
            {
                SettingsMetaData settingsMetaData = GetSettingsMetaData();

                if (string.IsNullOrEmpty(settingsTemplateId))
                {
                    ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_Select_Template);
                    return PartialView("~/Views/Shared/TenantTemplates/TenantSettingsTemplateDetails.ascx", settingsTemplate);
                }

                if (!string.IsNullOrEmpty(settingsTemplateId))
                {
                    settingsTemplate = SettingsTemplateProxy.GetSettingsTemplateDetails(settingsTemplateId);
                }
                else
                {
                    settingsTemplate.Attributes = (from sm in settingsMetaData.AttributeMetaData.Values
                                                   select new SettingsAttribute()
                                                   {
                                                       AttributeSetId = sm.AttributeSetId,
                                                       AttributeId = sm.AttributeId,
                                                       AttributeValue = null
                                                   }).ToList();
                }

                Dictionary<string, SettingsAttribute> settingAttributes = new Dictionary<string, SettingsAttribute>();
                settingsTemplate.Attributes.ForEach(x => { x.IsSelected = true; settingAttributes.Add(x.AttributeId, x); });
                ViewData["SettingsAttribute"] = settingAttributes;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AccessDenied);
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (SettingsMetaDataException settingsMetaDataException)
            {
                ExceptionService.HandleException(settingsMetaDataException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AttributeMetaData);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ManageTenantSettingsTemplate);
            }

            SetTemplateDefaultValues(settingsTemplate.Attributes);
            return PartialView("~/Views/Shared/TenantTemplates/TenantSettingsTemplateDetails.ascx", settingsTemplate);
        }

        /// <summary>
        /// This method is used to saves the tenant settings templates details.
        /// </summary>
        /// <param name="settingsTemplate">settings template.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveTenantSettingsTemplatesDetails(SettingsTemplate settingsTemplate)
        {
            try
            {
                TenantSettingsTemplate tenantSettingsTemplate = new TenantSettingsTemplate();
                tenantSettingsTemplate.TemplateId = settingsTemplate.Id.ToString();
                tenantSettingsTemplate.TenantId = TenantSettingsTemplateTenantId;
                tenantSettingsTemplate.Attributes = settingsTemplate.Attributes;
                tenantSettingsTemplate.SettingsTemplate = settingsTemplate;
                tenantSettingsTemplate.CreatedBy = UserIdentity.UserId;

                ValidateSettingsAttributes(settingsTemplate.Attributes, settingsTemplate.TemplateType);

                if (ModelState.IsValid)
                {
                    LogoUpload(settingsTemplate.Attributes, TenantSettingsTemplateTenantId);
                    SettingsTemplateProxy.AddTenantSettingsTemplate(tenantSettingsTemplate);
                    TempData["Message"] = Resources.SettingsTemplateResource.s_TenantSettingsTemplateSave;
                    return RedirectToAction("TenantSettingsTemplate");
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AccessDenied);
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (SettingsMetaDataException settingsMetaDataException)
            {
                ExceptionService.HandleException(settingsMetaDataException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AttributeMetaData);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_TenantSettingsTemplateSave);
            }
            catch (TenantSettingsException tenantSettingsException)
            {
                ExceptionService.HandleException(tenantSettingsException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_TenantSettingsTemplateSave);
            }

            Dictionary<string, SettingsAttribute> settingAttributes = new Dictionary<string, SettingsAttribute>();
            settingsTemplate.Attributes.ForEach(x => { x.IsSelected = true; settingAttributes.Add(x.AttributeId, x); });
            ViewData["SettingsAttribute"] = settingAttributes;
            TempData["settingsTemplate"] = settingsTemplate;

            SetTemplateDefaultValues(settingsTemplate.Attributes);

            if (!ModelState.IsValid)
            {
                TempData["SettingAttributes"] = settingsTemplate.Attributes;
                TempData["settingTemplateId"] = settingsTemplate.Id;
                TempData["SettingsTemplateError"] = ModelState["SettingsTemplateError"].Errors[0].ErrorMessage;
            }

            return RedirectToAction("AddTenantSettingsTemplate");
        }

        /// <summary>
        /// This method is used for getting the tenants settings templates details.
        /// </summary>
        /// <returns></returns>
        public ActionResult TenantSettingsTemplatesDetails()
        {
            return View();
        }

        /// <summary>
        /// This method is used for getting the tenant settings templates details.
        /// </summary>
        /// <param name="settingsTemplate">settings template.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TenantSettingsTemplatesDetails(SettingsTemplate settingsTemplate)
        {
            return View();
        }

        /// <summary>
        /// This method is used to views the settings templates based on the settings template identifier.
        /// </summary>
        /// <param name="settingTemplateId">The setting template identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public PartialViewResult ViewSettingsTemplate(string settingTemplateId, string tenantId)
        {
            SettingsTemplate settingsTemplate = new SettingsTemplate();
            ViewData["ImageURL"] = this.Url.Content(@"~/" + (ConfigHelper.LogoImageFolderPath ?? DefaultLogoImagePath) + @"/");

            try
            {
                SettingsMetaData settingsMetaData = GetSettingsMetaData();

                if (!string.IsNullOrEmpty(settingTemplateId))
                {
                    if (string.IsNullOrEmpty(tenantId))
                    {
                        settingsTemplate = SettingsTemplateProxy.GetSettingsTemplateDetails(settingTemplateId);
                    }
                    else
                    {
                        CelloSaaS.Model.SettingsManagement.TenantSettingsTemplate tenantTemplate = SettingsTemplateProxy.GetTenantTemplateDetails(tenantId, settingTemplateId);
                        if (tenantTemplate != null && tenantTemplate.SettingsTemplate != null)
                        {
                            settingsTemplate = tenantTemplate.SettingsTemplate;
                            settingsTemplate.Attributes = tenantTemplate.Attributes;
                        }
                    }
                }
                else
                {
                    settingsTemplate.Attributes = (from sm in settingsMetaData.AttributeMetaData.Values
                                                   select new SettingsAttribute()
                                                   {
                                                       AttributeSetId = sm.AttributeSetId,
                                                       AttributeId = sm.AttributeId,
                                                       AttributeValue = null
                                                   }).ToList();

                    // check a for null
                    if (settingsTemplate.Attributes.Count < 1)
                    {
                        ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_MissingAttributes);
                    }
                }
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_AccessDenied);
            }
            catch (ArgumentNullException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_ParameterEmptyOrNull);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", settingsTemplateException.Message);
            }
            catch (SettingsMetaDataException settingsMetaDataException)
            {
                ExceptionService.HandleException(settingsMetaDataException, _defaultPolicy);
                ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_AttributeSet_Exception);
            }

            return PartialView("~/Views/Shared/TenantTemplates/ViewSettingsTemplate.ascx", settingsTemplate);
        }

        /// <summary>
        /// This method is used to gets the given formatted date.
        /// </summary>
        /// <param name="dateFormat">The date format.</param>
        /// <returns></returns>
        public JsonResult GetGivenFormattedDate(string dateFormat)
        {
            DateTime time = DateTime.Now; // Use current time
            string formattedDate = time.ToString(dateFormat, CultureInfo.InvariantCulture);

            return Json(formattedDate);
        }

        #region Private Methods

        /// <summary>
        /// Gets the settings meta data.
        /// </summary>
        /// <returns></returns>
        private SettingsMetaData GetSettingsMetaData()
        {
            SettingsMetaData settingsMetaData = SettingsMetaDataProxy.GetSettingsMetaData();
            if (settingsMetaData == null || settingsMetaData.AttributeMetaData == null)
            {
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_AttributeMetaData);
            }

            ViewData["SettingsMetaData"] = settingsMetaData;
            return settingsMetaData;
        }

        /// <summary>
        /// Validates the settings template details.
        /// </summary>
        /// <param name="settingsTemplate">The settings template.</param>
        private void ValidateSettingsTemplateDetails(SettingsTemplate settingsTemplate)
        {
            if (settingsTemplate.Name == null || string.IsNullOrEmpty(settingsTemplate.Name.Trim()))
            {
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_SettingsTemplateName);
            }

            if (!string.IsNullOrEmpty(settingsTemplate.Name) && settingsTemplate.Name.Length > 100)
            {
                ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_SettingsTemplateNameMaxLength);
                return;
            }

            if (settingsTemplate.Attributes != null && settingsTemplate.Attributes.Count < 1)
            {
                ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_SettingsTemplateAttribute);
                return;
            }

            ValidateSettingsAttributes(settingsTemplate.Attributes, settingsTemplate.TemplateType);
        }

        /// <summary>
        /// Validates the settings attributes.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        private void ValidateSettingsAttributes(List<SettingsAttribute> attributes, SettingTemplateType type)
        {
            if (attributes == null || attributes.Count < 1)
            {
                ModelState.AddModelError("SettingsTemplateError", Resources.SettingsTemplateResource.e_SettingsTemplateAttribute);
                return;
            }

            if (type.Equals(SettingTemplateType.Customized))
            {
                return;
            }

            foreach (SettingsAttribute attribute in attributes)
            {
                if (attribute.AttributeId.Equals(AttributeConstants.Logo))
                {
                    if (this.Request != null && this.Request.Files.Count < 1 || string.IsNullOrEmpty(this.Request.Files[0].FileName))
                    {
                        if (string.IsNullOrEmpty(attribute.AttributeValue))
                        {
                            this.ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_SettingsTemplateeAttributeValue);
                            break;
                        }
                    }
                    else
                    {
                        attribute.AttributeValue = this.SettingsTemplateTenantId + "_logo.jpg";
                        continue;
                    }
                }

                if (attribute.AttributeId.Equals(AttributeConstants.UserPasswordExpiration))
                {
                    int expiryDays;
                    var isInputValid = int.TryParse(attribute.AttributeValue, out expiryDays);

                    if (!isInputValid)
                    {
                        ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_Invalid_Password_Expiry_Setting);
                        break;
                    }
                    attribute.AttributeValue = expiryDays.ToString(CultureInfo.InvariantCulture);
                }

                if (attribute.AttributeId.Equals(AttributeConstants.MaxPasswordFailureCount))
                {
                    int passwordFailCount;
                    var isInputValid = int.TryParse(attribute.AttributeValue, out passwordFailCount);

                    if (!isInputValid)
                    {
                        ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_Invalid_MaxPasswordFailureCount);
                        break;
                    }
                    attribute.AttributeValue = passwordFailCount.ToString(CultureInfo.InvariantCulture);
                }

                if (attribute.AttributeId.Equals(AttributeConstants.MaxPasswordAnswerFailureCount))
                {
                    int passwordAnswerFailCount;
                    var isInputValid = int.TryParse(attribute.AttributeValue, out passwordAnswerFailCount);

                    if (!isInputValid)
                    {
                        ModelState.AddModelError("SettingsTemplateError", SettingsTemplateResource.e_Invalid_MaxPasswordAnswerFailureCount);
                        break;
                    }
                    attribute.AttributeValue = passwordAnswerFailCount.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// Gets the theme name list.
        /// </summary>
        /// <param name="selectedThemeName">Name of the selected theme.</param>
        /// <returns></returns>
        private List<SelectListItem> GetThemeNameList(string selectedThemeName)
        {
            System.IO.DirectoryInfo appThemeDir = new System.IO.DirectoryInfo(Server.MapPath("~\\App_Themes"));
            System.IO.DirectoryInfo[] appThemeSubDir = appThemeDir.GetDirectories();

            List<SelectListItem> themeList = new List<SelectListItem>();

            //Gets the app_themes sub folders(Theme folders)
            foreach (System.IO.DirectoryInfo themefolder in appThemeSubDir)
            {
                // Removing source safe related directories
                if (themefolder.Name != ".svn")
                {
                    themeList.Add(new SelectListItem
                    {
                        Text = themefolder.Name,
                        Value = themefolder.Name,
                        Selected = (themefolder.Name == selectedThemeName)
                    });
                }
            }

            return themeList;
        }

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <returns></returns>
        private static List<SelectListItem> GetLanguages(string language)
        {
            var languageSelectList = new List<SelectListItem>();
            List<Language> languageList = LanguageProxy.GetAllLanguages();

            foreach (Language lang in languageList)
            {
                languageSelectList.Add(new SelectListItem
                {
                    Text = lang.Name,
                    Value = lang.Code,
                    Selected = lang.Code.Equals(language)
                });
            }

            return languageSelectList;
        }

        /// <summary>
        /// This method is used to get the available Date time format from the appsettings.config.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static List<SelectListItem> GetDateTimeFormats(string date)
        {
            string[] dateFormats = ConfigHelper.AvailableDateTimeFormats.Split(';');

            var dateFormatList = dateFormats.Where(x => !string.IsNullOrEmpty(x)).Select(x => new SelectListItem
            {
                Text = DateTime.Now.ToString(x.Trim()),
                Value = x.Trim(),
                Selected = x.Trim().Equals(date)
            }).ToList();

            return dateFormatList;
        }

        /// <summary>
        /// Sets the template default values.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <param name="themeName">Name of the theme.</param>
        /// <param name="logoFile">The logo file.</param>
        /// <param name="language">The language.</param>
        /// <param name="dateFormat">The date format.</param>
        private void SetTemplateDefaultValues(List<SettingsAttribute> attributes)
        {
            string themeName = ConfigHelper.DefaultTheme;
            string logoFile = ConfigHelper.DefaultLogo;
            string language = string.Empty;
            string dateFormat = ConfigHelper.ConfiguredDateTimeFormat;

            Dictionary<string, SettingsAttribute> settingAttributes = null;
            if (attributes != null)
            {
                settingAttributes = (from a in attributes select new { Key = a.AttributeId, Value = a }).ToDictionary(t => t.Key, t => t.Value);
                if (settingAttributes != null && settingAttributes.Count > 0)
                {
                    if (settingAttributes.ContainsKey(AttributeConstants.Theme))
                    {
                        if (string.IsNullOrEmpty(settingAttributes[AttributeConstants.Theme].AttributeValue))
                        {
                            settingAttributes[AttributeConstants.Theme].AttributeValue = themeName;
                        }
                        else
                        {
                            themeName = settingAttributes[AttributeConstants.Theme].AttributeValue;
                        }
                    }

                    if (settingAttributes.ContainsKey(AttributeConstants.Language))
                    {
                        if (string.IsNullOrEmpty(settingAttributes[AttributeConstants.Language].AttributeValue))
                        {
                            settingAttributes[AttributeConstants.Language].AttributeValue = language;
                        }
                        else
                        {
                            language = settingAttributes[AttributeConstants.Language].AttributeValue;
                        }
                    }

                    if (settingAttributes.ContainsKey(AttributeConstants.Logo))
                    {
                        if (string.IsNullOrEmpty(settingAttributes[AttributeConstants.Logo].AttributeValue))
                        {
                            settingAttributes[AttributeConstants.Logo].AttributeValue = logoFile;
                        }
                        else
                        {
                            logoFile = settingAttributes[AttributeConstants.Logo].AttributeValue;
                        }
                    }

                    if (settingAttributes.ContainsKey(AttributeConstants.DateFormat))
                    {
                        if (string.IsNullOrEmpty(settingAttributes[AttributeConstants.DateFormat].AttributeValue))
                        {
                            settingAttributes[AttributeConstants.DateFormat].AttributeValue = dateFormat;
                        }
                        else
                        {
                            dateFormat = settingAttributes[AttributeConstants.DateFormat].AttributeValue;
                        }
                    }
                }
            }

            ViewData["ImageURL"] = this.Url.Content(@"~/" + (ConfigHelper.LogoImageFolderPath ?? DefaultLogoImagePath) + @"/" + logoFile);

            ViewData["SettingsAttribute"] = settingAttributes;
            ViewData["Themes"] = GetThemeNameList(themeName);
            ViewData["Languages"] = GetLanguages(language);
            ViewData["DateFormats"] = GetDateTimeFormats(dateFormat);
            ViewData["dateFormat"] = dateFormat;
        }

        #endregion Private Methods
    }
}