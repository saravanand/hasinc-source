﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Reporting.DataSources.Model;
using CelloSaaS.Reporting.DataSources.ServiceProxies;
using Resources;

namespace CelloSaaSApplication.Controllers
{
    using System.Transactions;
    using CelloSaaS.ChartBuilder.ServiceContracts;
    using CelloSaaS.QueryBuilderLibrary.ServiceProxies;
    using CelloSaaS.ReportBuilder.ServiceProxies;
    using CelloSaaS.Reporting.DataSources.ServiceContracts;
    using System.Globalization;
    using CelloSaaS.ChartBuilder.ServiceProxies;
    using CelloSaaS.View;

    /// <summary>
    /// This class is responsible for table sources.
    /// </summary>
    public class TableSourcesController : CelloController
    {
        /// <summary>
        /// This method is used to gets the table sources.
        /// </summary>
        /// <param name="viewObjectId">view object identifier.</param>
        /// <param name="tableSourceId">table source identifier.</param>
        /// <returns></returns>
        public PartialViewResult GetTableSources(string viewObjectId, string tableSourceId)
        {
            if (string.IsNullOrEmpty(viewObjectId))
            {
                throw new ArgumentException(TableSourceResources.e_ViewObjectId_Missing);
            }

            ViewData["viewObjectId"] = viewObjectId;
            ViewData["tableSourceId"] = string.IsNullOrEmpty(tableSourceId) ? null : tableSourceId;
            FillTableSources(tableSourceId);
            return this.PartialView();
        }

        /// <summary>
        /// This method is used to get the manage table source view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (TempData["Success"] != null)
            {
                ModelState.AddModelError("Success", TempData["Success"].ToString());
            }

            if (TempData["Error"] != null)
            {
                ModelState.AddModelError("Error", TempData["Error"].ToString());
            }

            var tableSources = TableSourceProxy.GetShallowSources(UserIdentity.TenantID);

            var tableSourcesList = from t in tableSources
                                   select new TableSource { Identifier = t.Key, Name = t.Value };

            return View(tableSourcesList);
        }

        /// <summary>
        /// This method is used to fills the query sources.
        /// </summary>
        /// <param name="queryId">query identifier.</param>
        private void FillQuerySources(string queryId = null)
        {
            var availableQueries = QueryDetailsProxy.GetShallowQueryDetails(UserIdentity.TenantID);

            var queries = new Dictionary<string, string> { { "-1", "--Choose A QuerySource--" } };

            if (availableQueries != null && availableQueries.Count > 0)
            {
                foreach (var availableQuery in availableQueries)
                {
                    queries.Add(availableQuery.Key, availableQuery.Value);
                }
            }

            if (string.IsNullOrEmpty(queryId))
            {
                queryId = "-1";
            }

            ViewData["dataSources"] = new SelectList(queries, "Key", "Value", queryId);
        }

        /// <summary>
        /// This method is used to manages the table sources.
        /// </summary>
        /// <param name="tableSource">table source.</param>
        /// <returns></returns>
        public ActionResult ManageTableSources(TableSource tableSource)
        {
            ViewData["tableSource"] = tableSource;

            ValidateTableSource(tableSource);

            if (tableSource.DataSourceValues != null && !string.IsNullOrEmpty(tableSource.DataSourceValues.SourceContentId))
            {
                this.FillQuerySources(tableSource.DataSourceValues.SourceContentId);
            }
            else
            {
                this.FillQuerySources();
            }

            if (!ModelState.IsValid)
            {
                return this.View();
            }

            TableSource tblSource = tableSource;

            bool status;
            try
            {
                if (string.IsNullOrEmpty(tableSource.Id))
                {
                    tblSource.TenantId = UserIdentity.TenantID;

                    CheckAndAddDataSource(tableSource, tblSource);

                    tblSource.DataSourceValues.UpdatedBy = tblSource.CreatedBy = UserIdentity.UserId;
                    tblSource.DataSourceValues.UpdatedOn = tblSource.CreatedOn = DateTime.Now;
                    tblSource.Status = tblSource.DataSourceValues.Status = true;
                }
                else
                {
                    tblSource = (TableSource)TableSourceProxy.GetSource(tableSource.Id, UserIdentity.TenantID);
                    tblSource.DataSourceValues.UpdatedBy = tblSource.UpdatedBy = UserIdentity.UserId;
                    tblSource.DataSourceValues.UpdatedOn = tblSource.UpdatedOn = DateTime.Now;
                    TryUpdateModel(tblSource);
                }

                if (string.IsNullOrEmpty(tblSource.Id))
                {
                    tblSource.Id = TableSourceProxy.AddSource(tblSource);
                    ModelState.AddModelError("Success", TableSourceResources.m_TableSourceUpdate);
                    TempData["Success"] = TableSourceResources.m_TableSourceSave;
                    return RedirectToAction("Index");
                }

                status = TableSourceProxy.UpdateSource(tblSource) > 0;

                if (!status)
                {
                    this.ModelState.AddModelError("Error", TableSourceResources.e_TableSourceUpdate);
                }
                else
                {
                    this.ModelState.AddModelError("Success", TableSourceResources.m_TableSourceUpdate);
                    TempData["Success"] = TableSourceResources.m_TableSourceSave;
                    return RedirectToAction("Index");
                }
            }
            catch (DuplicateDataSourceNameException duplicateDataSourceNameException)
            {
                ModelState.AddModelError("Error", duplicateDataSourceNameException.Message);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("Error", exception.Message);
            }

            ViewData["tableSource"] = tblSource;

            this.FillQuerySources(tblSource.DataSourceValues.SourceContentId);

            return this.View();
        }

        /// <summary>
        /// This method is used to validates the table source.
        /// </summary>
        /// <param name="tableSource">table source.</param>
        private void ValidateTableSource(TableSource tableSource)
        {
            if (tableSource == null)
            {
                ModelState.AddModelError("InvalidTableSource", TableSourceResources.e_Invalid_TableSource);
                return;
            }

            if (string.IsNullOrEmpty(tableSource.Name))
            {
                ModelState.AddModelError("EmptyTableSourceName", TableSourceResources.e_Invalid_Table_Source_Name);
            }

            if (tableSource.DataSourceValues == null || string.IsNullOrEmpty(tableSource.DataSourceValues.SourceContentId) || tableSource.DataSourceValues.SourceContentId == "-1")
            {
                ModelState.AddModelError("InvalidDataSource", TableSourceResources.e_Invalid_DataSource);
            }

            if (!string.IsNullOrEmpty(tableSource.Name) && tableSource.Name.Length > 50)
            {
                ModelState.AddModelError("LengthySourceName", TableSourceResources.e_Table_Source_Name_lengthy);
            }

            if (!string.IsNullOrEmpty(tableSource.Description) && tableSource.Description.Length > 50)
            {
                ModelState.AddModelError("LengthySourceDesc", TableSourceResources.Table_Source_Description_lengthy);
            }
        }

        /// <summary>
        /// This method is used to checks and add data source.
        /// </summary>
        /// <param name="tableSource">table source.</param>
        /// <param name="tblSource">TBL source.</param>
        private void CheckAndAddDataSource(TableSource tableSource, TableSource tblSource)
        {
            if (tblSource.DataSourceValues == null || string.IsNullOrEmpty(tblSource.DataSourceValues.SourceContentId))
            {
                return;
            }

            var matchedDataSources = DataSourceProxy.GetSourcesByContentId(tblSource.DataSourceValues.SourceContentId, UserIdentity.TenantID);

            if (matchedDataSources != null && matchedDataSources.Count > 0)
            {
                tblSource.DataSourceValues = (DataSource)matchedDataSources.Values.ToList()[0];
                tblSource.DataSourceId = tblSource.DataSourceValues.Id;
            }
            else
            {
                var preBuiltQueries = QueryDetailsProxy.GetShallowQueryDetails(UserIdentity.TenantID);

                if (preBuiltQueries == null || preBuiltQueries.Count < 1)
                {
                    this.ModelState.AddModelError("Error", TableSourceResources.e_Invalid_DataSource);
                    return;
                }

                var ds = new DataSource
                    {
                        SourceContentId = tableSource.DataSourceValues.SourceContentId,
                        SourceTypeId = DataSourceConstants.BuiltInQuerySource,
                        Status = true,
                        TenantId = UserIdentity.TenantID,
                        CreatedBy = UserIdentity.UserId,
                        CreatedOn = DateTime.Now,
                        Name = preBuiltQueries[tableSource.DataSourceValues.SourceContentId],
                        DataSourceContent = null
                    };

                ds.Id = DataSourceProxy.AddSource(ds);
                tblSource.DataSourceId = ds.Id;
                tblSource.DataSourceValues = ds;
            }
        }

        /// <summary>
        /// This method is used to manages the table sources based on the given source identifier.
        /// </summary>
        /// <param name="sourceId">source identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ManageTableSources(string sourceId)
        {
            if (string.IsNullOrEmpty(sourceId))
            {
                FillQuerySources();
                return View();
            }

            try
            {
                var tableSource = (TableSource)TableSourceProxy.GetSource(sourceId, UserIdentity.TenantID);

                if (tableSource == null || string.IsNullOrEmpty(tableSource.Id))
                {
                    ViewData["statusMessage"] = TableSourceResources.e_GetTableSource;
                    FillQuerySources();
                    return View();
                }

                ViewData["tableSource"] = tableSource;

                if (tableSource.DataSourceValues != null && !string.IsNullOrEmpty(tableSource.DataSourceValues.SourceContentId))
                {
                    FillQuerySources(tableSource.DataSourceValues.SourceContentId);
                }
                else
                {
                    FillQuerySources();
                }
            }
            catch (DataSourceException dataSourceException)
            {
                string message = string.Format("{0} : {1}", TableSourceResources.e_GetTableSource, dataSourceException.Message);
                ViewData["statusMessage"] = message;
                ModelState.AddModelError("statusMessage", message);
            }
            catch (ArgumentException argException)
            {
                string message = string.Format("{0} : {1}", TableSourceResources.e_GetTableSource, argException.Message);
                ViewData["statusMessage"] = message;
                ModelState.AddModelError("statusMessage", message);
            }

            return View();
        }

        /// <summary>
        /// This method is used to deletes the source based on the source identifier.
        /// </summary>
        /// <param name="sourceId">source identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteSource(string sourceId)
        {
            try
            {
                if (string.IsNullOrEmpty(sourceId))
                {
                    TempData["Error"] = TableSourceResources.e_InvalidSource;
                    return RedirectToAction("Index");
                }

                Dictionary<string, CelloSaaS.ReportBuilder.Model.Report> existingReports = ReportProxy.GetAllReportsBySourceId(UserIdentity.TenantID, sourceId);

                if (existingReports != null && existingReports.Count > 0)
                {
                    TempData["Error"] = TableSourceResources.e_SourceLinksFound;
                    return RedirectToAction("Index");
                }

                TableSource tableSource = (TableSource)TableSourceProxy.GetSource(sourceId, UserIdentity.TenantID);

                if (tableSource == null)
                {
                    TempData["Error"] = TableSourceResources.e_Invalid_TableSource;
                    return RedirectToAction("Index");
                }

                Dictionary<string, IObjectSource> usedDataSources = null;

                if (tableSource.DataSourceValues != null)
                {
                    usedDataSources = DataSourceProxy.GetSourcesByContentId(tableSource.DataSourceValues.SourceContentId, UserIdentity.TenantID);
                }

                using (TransactionScope tScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    var deletableDataSources = new List<string>();

                    if (usedDataSources != null && usedDataSources.Count == 1)
                    {
                        foreach (var usedDataSource in usedDataSources)
                        {
                            var charts = ChartDetailsProxy.GetChartsBySourceId(usedDataSource.Value.Id, usedDataSource.Value.TenantId);

                            if (!usedDataSource.Value.Id.Equals(tableSource.DataSourceId, StringComparison.OrdinalIgnoreCase)
                                || (charts != null && charts.Count >= 1))
                            {
                                continue;
                            }
                            if (!deletableDataSources.Contains(usedDataSource.Key))
                            {
                                deletableDataSources.Add(usedDataSource.Key);
                            }
                        }
                    }

                    if (TableSourceProxy.DeleteSource(sourceId, UserIdentity.TenantID) > 0)
                    {
                        foreach (var deletableDataSource in deletableDataSources)
                        {
                            DataSourceProxy.DeleteSource(deletableDataSource, UserIdentity.TenantID);
                        }
                        tScope.Complete();
                        TempData["Success"] = TableSourceResources.m_TableSourceDelete;
                        return RedirectToAction("Index");
                    }
                    tScope.Complete();
                }
            }
            catch (DataSourceException dataSourceException)
            {
                TempData["Error"] = dataSourceException.Message;
            }
            catch (ArgumentException argException)
            {
                TempData["Error"] = argException.Message;
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to fills the table sources based on the given table source identifier.
        /// </summary>
        /// <param name="tableSourceId">table source identifier.</param>
        private void FillTableSources(string tableSourceId = null)
        {
            var tableSources = TableSourceProxy.GetShallowSources(UserIdentity.TenantID);

            var tableSourceList = new Dictionary<string, string> { { "-1", "--Choose A Table Source --" } };

            foreach (var tblSource in tableSources)
            {
                tableSourceList.Add(tblSource.Key, tblSource.Value);
            }

            tableSourceId = string.IsNullOrEmpty(tableSourceId) ? "-1" : tableSourceId;

            ViewData["TableSourcesList"] = new SelectList(tableSourceList, "Key", "Value", tableSourceId);
        }
    }
}
