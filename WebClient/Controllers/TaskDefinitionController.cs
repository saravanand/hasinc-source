﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.WorkFlow;
using CelloSaaS.WorkFlow.Service;
using CelloSaaSApplication.Models;
using CelloSaaS.View;
using CelloSaaS.WorkFlow.Model;
using CelloSaaS.WorkFlow.ServiceProxies;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for task definition.
    /// </summary>
    public class TaskDefinitionController : CelloController
    {
        /// <summary>
        /// This method is used to adds the task definition.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="taskDefinitionId">task definition identifier.</param>
        /// <param name="taskType">type of the task.</param>
        /// <returns></returns>
        public JsonResult AddTaskDefinition(string workflowDefinitionId, string taskDefinitionId, string taskType)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && !string.IsNullOrEmpty(taskType))
            {
                //var service = new WorkflowDefinitionService();

                var taskDef = WorkflowDefinitionServiceProxy.CreateTaskDefinition(workflowDefinitionId, taskDefinitionId, ((WFTaskType)Enum.Parse(typeof(WFTaskType), taskType)));

                return Json(new
                {
                    TaskDefinitionId = taskDef.TaskId.ToString(),
                    TaskName = taskDef.TaskName,
                    TaskCode = taskDef.TaskCode,
                    Ordinal = taskDef.Ordinal
                });
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_InvalidData }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to deletes the task definition.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="taskDefinitionId">task definition identifier.</param>
        /// <returns></returns>
        public ActionResult DeleteTaskDefinition(string workflowDefinitionId, string taskDefinitionId)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && !string.IsNullOrEmpty(taskDefinitionId))
            {
                //var service = new WorkflowDefinitionService();
                WorkflowDefinitionServiceProxy.DeleteTaskDefinition(workflowDefinitionId, taskDefinitionId);
                return Json(new { Success = Resources.TaskDefinitionResource.s_DeleteTask }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_InvalidData }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to gets the tasks definitions.
        /// </summary>
        /// <param name="workflowId">The workflow identifier.</param>
        /// <param name="workflowDefinitionId">The workflow definition identifier.</param>
        /// <returns></returns>
        public JsonResult GetTaksDefinitions(string workflowId, string workflowDefinitionId)
        {
            //var service = new WorkflowDefinitionService();
            var wfDef = WorkflowDefinitionServiceProxy.GetWorkflowDefinition(workflowDefinitionId);

            if (wfDef != null)
            {
                var uiModelDict = WorkflowDefinitionServiceProxy.GetWorkflowUIModel(workflowDefinitionId);

                var items = wfDef.WorkflowItemDefinitions.Where(x => x is TaskDefinition).Select(x =>
                {
                    var taskDef = (x as TaskDefinition);
                    var taskDefVM = new TaskDefViewModel
                    {
                        TaskId = taskDef.TaskId.ToString(),
                        TaskCode = taskDef.TaskCode,
                        TaskName = taskDef.TaskName,
                        Ordinal = taskDef.Ordinal,
                        TaskType = GetTaskTypeString(taskDef),
                        UIModel = new WFUIModel(),
                        Conditions = new TaskConditionsViewModel()
                    };

                    if (uiModelDict != null && uiModelDict.ContainsKey(taskDefVM.TaskId))
                    {
                        taskDefVM.UIModel = uiModelDict[taskDefVM.TaskId];
                    }

                    if ((taskDef is AutoTaskDefinition || taskDef is ManualTaskDefinition)
                        && !string.IsNullOrEmpty(taskDef.TypeName) && !string.IsNullOrEmpty(taskDef.AssemblyName))
                    {
                        var activityDetails = new ActivityService().GetActivity(workflowId, taskDef.TypeName, taskDef.AssemblyName);

                        if (activityDetails != null)
                        {
                            taskDefVM.ActivityId = activityDetails.Id.ToString();
                            taskDefVM.ActivityName = activityDetails.Name;
                            taskDefVM.ActivityDescription = activityDetails.Description;
                        }
                    }

                    if (taskDef is ManualTaskDefinition)
                    {
                        taskDefVM.ActorDef = GetActorDefinition(taskDef as ManualTaskDefinition);
                    }

                    FillTaskConditions(taskDef.TaskConditionDefinition as TaskConditionDefinition, taskDefVM.Conditions);

                    return taskDefVM;
                });

                return Json(items);
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_FetchTaskDefinition });
        }

        /// <summary>
        /// This method is used to gets the router definitions.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <returns></returns>
        public JsonResult GetRouterDefinitions(string workflowId, string workflowDefinitionId)
        {
            //var service = new WorkflowDefinitionService();
            var wfDef = WorkflowDefinitionServiceProxy.GetWorkflowDefinition(workflowDefinitionId);

            if (wfDef != null)
            {
                var uiModelDict = WorkflowDefinitionServiceProxy.GetWorkflowUIModel(workflowDefinitionId);

                var items = wfDef.WorkflowItemDefinitions.Where(x => x is TaskRouterDefinition).Select(x =>
                {
                    var router = (x as TaskRouterDefinition);

                    if (router.RoutingTable.RoutingConditions.Count > 0)
                    {
                        var routerVM = new RouterViewModel
                        {
                            RouterId = router.RouterId,
                            RouterName = router.RouterName,
                            IsVisible = router.IsVisible,
                            InputTaskIds = string.Join(",", router.RoutingTable.RoutingConditions[0].InputTaskIds),
                            Conditions = GetRouterConditionExpressionArray(router.RoutingTable.RoutingConditions),
                            UIModel = new WFUIModel(),
                            Type = router.RouterType,
                            TypeString = router.RouterType.ToString()
                        };

                        if (router.RouterType.Equals(RouterType.IfElse))
                        {
                            IfElseRoutingContion ifElseRoutingCondition = (IfElseRoutingContion)router.RoutingTable.RoutingConditions[0];
                            routerVM.OutputTaskIds = string.Join(",", ifElseRoutingCondition.IfNodeOutputIds);
                            routerVM.ElseOutputIds = string.Join(",", ifElseRoutingCondition.ElseNodeOutputIds);
                        }
                        else
                        {
                            routerVM.OutputTaskIds = string.Join(",", router.RoutingTable.RoutingConditions[0].OutputTaskIds);
                        }

                        if (uiModelDict != null && uiModelDict.ContainsKey(routerVM.RouterId.ToString()))
                        {
                            routerVM.UIModel = uiModelDict[routerVM.RouterId.ToString()];
                        }

                        return routerVM;
                    }

                    return new RouterViewModel();
                });

                return Json(items);
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_FetchTaskRouting });
        }

        /// <summary>
        /// This method is used to gets the task type string.
        /// </summary>
        /// <param name="taskDefinition">task definition.</param>
        /// <returns></returns>
        private static string GetTaskTypeString(TaskDefinition taskDefinition)
        {
            if (taskDefinition is StartTaskDefinition)
                return WFTaskType.Start.ToString();
            else if (taskDefinition is CloseTaskDefinition)
                return WFTaskType.Close.ToString();
            else if (taskDefinition is AutoTaskDefinition)
                return WFTaskType.Auto.ToString();
            else if (taskDefinition is ManualTaskDefinition)
                return WFTaskType.Manual.ToString();

            return WFTaskType.Start.ToString();
        }

        /// <summary>
        /// This method is used to gets the manual task definition details.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="taskDefinitionId">task definition identifier.</param>
        /// <returns></returns>
        public JsonResult GetManaulTaskDefinitionDetails(string workflowDefinitionId, string taskDefinitionId)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && !string.IsNullOrEmpty(taskDefinitionId))
            {
                //var service = new WorkflowDefinitionService();
                var wfDef = WorkflowDefinitionServiceProxy.GetWorkflowDefinition(workflowDefinitionId);

                var items = wfDef.WorkflowItemDefinitions
                                    .Where(t => (t is ManualTaskDefinition)
                                                 && ((t as ManualTaskDefinition).TaskId.ToString() == taskDefinitionId));

                if (items != null && items.Count() > 0)
                {
                    var taskDef = items.First() as ManualTaskDefinition;

                    string roles = null;
                    bool isRuntimeCalculated = false;

                    if (taskDef.ActorDefinition is RoleBasedTaskActorDefinition)
                    {
                        var actorDef = taskDef.ActorDefinition as RoleBasedTaskActorDefinition;

                        isRuntimeCalculated = actorDef.IsRuntimeCalculated;
                        roles = string.Join(",", actorDef.Roles);
                    }

                    return Json(new TaskActorDefViewModel
                    {
                        Url = taskDef.Url,
                        Roles = roles,
                        IsRuntimeCalculated = isRuntimeCalculated
                    });
                }
                else
                {
                    return Json(new { Error = Resources.TaskDefinitionResource.e_TaskNotFound });
                }
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to updates the task definition.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identification.</param>
        /// <param name="taskDefVM">task definition identification.</param>
        /// <param name="actorDefVM">task actor definition view model</param>
        /// <param name="conditionsVM">task condition view model.</param>
        /// <returns></returns>
        public JsonResult UpdateTaskDefinition(string workflowDefinitionId,
            TaskDefViewModel taskDefVM,
            TaskActorDefViewModel actorDefVM,
            TaskConditionsViewModel conditionsVM)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId)
                && taskDefVM != null
                && !string.IsNullOrEmpty(taskDefVM.TaskId)
                && !string.IsNullOrEmpty(taskDefVM.TaskType))
            {
                //var service = new WorkflowDefinitionService();
                var result = WorkflowDefinitionServiceProxy.UpdateTaskDefinition(workflowDefinitionId, taskDefVM, actorDefVM, conditionsVM);
                if (result)
                {
                    return Json(new { Success = Resources.TaskDefinitionResource.s_Update });
                }
                else
                {
                    return Json(new { Error = Resources.TaskDefinitionResource.e_InvalidData });
                }
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_InvalidData });
        }

        #region Private Methods to update task conditions

        private static void FillTaskConditions(TaskConditionDefinition taskConditionDefinition, TaskConditionsViewModel taskConditionsViewModel)
        {
            if (taskConditionDefinition != null && taskConditionsViewModel != null)
            {
                taskConditionsViewModel.StartConditions = GetConditionExpressionArray(taskConditionDefinition.StartConditions);
                taskConditionsViewModel.ExpireConditions = GetConditionExpressionArray(taskConditionDefinition.ExpireConditions);
                taskConditionsViewModel.CompleteConditions = GetConditionExpressionArray(taskConditionDefinition.CompleteConditions);
                taskConditionsViewModel.SkipConditions = GetConditionExpressionArray(taskConditionDefinition.SkipConditions);
                taskConditionsViewModel.OverrideConditions = GetConditionExpressionArray(taskConditionDefinition.OverrideConditions);
            }
        }

        private static string[] GetConditionExpressionArray(List<ITaskCondition> taskConditions)
        {
            var lstExpressions = new List<string>();

            if (taskConditions == null || taskConditions.Count == 0)
                return lstExpressions.ToArray();

            taskConditions.ForEach(x =>
            {
                if (x is CodeExpressionCondition)
                {
                    lstExpressions.Add("CodeExpressionCondition:" + (x as CodeExpressionCondition).ConditionExpression);
                }
                else if (x is TaskExpressionCondition)
                {
                    lstExpressions.Add("TaskExpressionCondition:" + (x as TaskExpressionCondition).ConditionExpression);
                }
                else if (x is SPTaskCondition)
                {
                    lstExpressions.Add("SPTaskCondition:" + (x as SPTaskCondition).SpDefinition.SPName);
                }
                else if (x is BusinessRuleExpressionCondition)
                {
                    lstExpressions.Add("BusinessRuleExpressionCondition:" + (x as BusinessRuleExpressionCondition).ConditionExpression);
                }
            });

            return lstExpressions.ToArray();
        }

        private static string[] GetRouterConditionExpressionArray(List<IRoutingCondition> rConditions)
        {
            var lstExpressions = new List<string>();

            if (rConditions == null || rConditions.Count == 0)
                return lstExpressions.ToArray();

            rConditions.ForEach(x =>
            {
                if (x.Condition is CodeExpressionCondition)
                {
                    lstExpressions.Add("CodeExpressionCondition:" + (x.Condition as CodeExpressionCondition).ConditionExpression);
                }
                else if (x.Condition is TaskExpressionCondition)
                {
                    lstExpressions.Add("TaskExpressionCondition:" + (x.Condition as TaskExpressionCondition).ConditionExpression);
                }
                else if (x.Condition is SPTaskCondition)
                {
                    lstExpressions.Add("SPTaskCondition:" + (x.Condition as SPTaskCondition).SpDefinition.SPName);
                }
                else if (x.Condition is BusinessRuleExpressionCondition)
                {
                    lstExpressions.Add("BusinessRuleExpressionCondition:" + (x.Condition as BusinessRuleExpressionCondition).ConditionExpression);
                }
            });

            return lstExpressions.ToArray();
        }

        private static TaskActorDefViewModel GetActorDefinition(ManualTaskDefinition manualTaskDefinition)
        {
            string roles = null;
            string roleNames = null;
            bool isRuntimeCalculated = false;
            string userNames = string.Empty;

            if (manualTaskDefinition.ActorDefinition is RoleBasedTaskActorDefinition)
            {
                var actorDef = manualTaskDefinition.ActorDefinition as RoleBasedTaskActorDefinition;

                isRuntimeCalculated = actorDef.IsRuntimeCalculated;
                roles = string.Join(",", actorDef.Roles);
                roleNames = string.Join(",", actorDef.RoleNames);
                userNames = string.Join(",", actorDef.UserNames);
            }

            return new TaskActorDefViewModel
            {
                Url = manualTaskDefinition.Url,
                Roles = roles,
                RoleNames = roleNames,
                UserNames = userNames,
                IsRuntimeCalculated = isRuntimeCalculated
            };
        }

        #endregion

        #region Activity

        /// <summary>
        /// This method is used to adds the activity.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="taskDefinitionId">task definition identifier.</param>
        /// <param name="activityId">activity identifier.</param>
        /// <returns></returns>
        public JsonResult AddActivity(string workflowId, string workflowDefinitionId, string taskDefinitionId, string activityId)
        {
            if (!string.IsNullOrEmpty(workflowId)
                && !string.IsNullOrEmpty(taskDefinitionId)
                && !string.IsNullOrEmpty(activityId))
            {
                //var service = new ActivityService();
                var activityDetails = ActivityServiceProxy.GetActivity(workflowId, activityId);

                if (activityDetails != null)
                {
                    WorkflowDefinitionServiceProxy.AddActivityDefinition(workflowDefinitionId, taskDefinitionId, activityDetails.ActivityDefinition);
                }

                return Json(new { Status = Resources.TaskDefinitionResource.s_Success });
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to removes the activity.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identification.</param>
        /// <param name="taskDefinitionId">task definition identification.</param>
        /// <param name="activityId">activity identification.</param>
        /// <returns></returns>
        public JsonResult RemoveActivity(string workflowDefinitionId, string taskDefinitionId, string activityId)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId)
                && !string.IsNullOrEmpty(taskDefinitionId)
                && !string.IsNullOrEmpty(activityId))
            {
                WorkflowDefinitionServiceProxy.DeleteActivityDefinition(workflowDefinitionId, taskDefinitionId);
                return Json(new { Status = Resources.TaskDefinitionResource.s_Success });
            }

            return Json(new { Error = Resources.TaskDefinitionResource.e_InvalidData });
        }

        #endregion
    }
}
