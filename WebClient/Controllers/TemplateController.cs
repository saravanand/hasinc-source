﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.ServiceProxies.MasterDataManagement;
using CelloSaaS.Model.MasterDataManagement;
using CelloSaaS.ServiceContracts.MasterDataManagement;
using CelloSaaS.Template.Model;
using CelloSaaS.Template.ServiceProxies;
using CelloSaaS.Model.TenantManagement;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for templates.
    /// </summary>
    public class TemplateController : CelloSaaS.View.CelloController
    {
        //
        // GET: /Template/
        // Logger policy name
        private const string _defaultPolicy = "TemplateExceptionLogger";

        /// <summary>
        /// Success value. This value is used in views
        /// </summary>
        private const string Sussess = "Success";

        /// <summary>
        /// The tenant Identifier.
        /// </summary>
        private string TenantId { get { return TenantContext.GetTenantId("_Template"); } }


        /// <summary>
        /// This methods is used to show the templates details.
        /// </summary>
        /// <returns></returns>
        public ActionResult TemplateDetails(string category = "")
        {
            if (!Util.ValidateIdentifier(category)) category = string.Empty;

            GetTemplateDetails(this.TenantId, category);
            GetTemplateCategories(new TemplateDetail { Category = category });
            if (Request.IsAjaxRequest())
            {
                return PartialView("PartialTemplateDetails");
            }
            return View();
        }

        /// <summary>
        /// This method is used to manages the template based on the template identifier
        /// </summary>
        /// <param name="templateId">template identifier.</param>
        /// <returns></returns>
        public ActionResult ManageTemplate(string templateId, string templateCategory)
        {
            if (!Util.ValidateIdentifier(templateCategory)) templateCategory = string.Empty;

            TemplateDetail template = null;
            if (string.IsNullOrEmpty(templateId))
            {
                template = new TemplateDetail();
                template.Category = templateCategory;
            }
            else
            {
                try
                {
                    templateId = Guid.Parse(templateId).ToString();
                    template = TemplateProxy.GetTemplate(this.TenantId, templateId);
                }
                catch (ArgumentNullException)
                {
                    ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException)
                {
                    ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
                }
                catch (UnauthorizedAccessException)
                {
                    ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_AccessDenied);
                }
                catch (TemplateException)
                {
                    ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_FetchTemplate);
                }
            }
            if (template == null)
            {
                template = new TemplateDetail();
            }
            GetTemplateCategories(template);
            return PartialView("ManageTemplate", template);
        }

        /// <summary>
        ///This methods is used to manages the template.
        /// </summary>
        /// <param name="template">template details.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public object ManageTemplate(TemplateDetail template)
        {
            try
            {
                template.TenantId = this.TenantId;
                template.CreatedBy = UserIdentity.UserId;
                ValidateTemplate(template);
                if (ModelState.IsValid)
                {
                    TemplateProxy.SaveTemplate(template);
                    return Sussess;
                }
            }
            catch (ArgumentNullException)
            {
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException)
            {
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException)
            {
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_AccessDenied);
            }
            catch (TemplateException)
            {
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_SaveTemplate);
            }
            catch (DuplicateTemplateNameException)
            {
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_DuplicateTemplateName);
            }

            GetTemplateCategories(template);
            return PartialView("ManageTemplate", template);
        }

        #region LocaleTemplate Configuration

        /// <summary>
        /// This method is used to show the locale template details.
        /// </summary>
        /// <param name="templateId">notification master identifier.</param>
        /// <param name="category">category.</param>
        /// <returns></returns>
        public ActionResult LocaleTemplateDetails(string templateId, string category)
        {
            Guid result;
            if (string.IsNullOrWhiteSpace(templateId) || !Guid.TryParse(templateId, out result))
            {
                return HttpNotFound();
            }
            
            if (string.IsNullOrWhiteSpace(category) || !Util.ValidateAlphaNumericWithSpace(category))
            {
                category = string.Empty;
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_Category);
            }
            Dictionary<string, LocaleTemplate> localeTemplateList = null;

            try
            {
                TemplateDetail templateDetails = TemplateProxy.GetTemplate(UserIdentity.TenantID, templateId);

                if (templateDetails == null)
                {
                    return HttpNotFound();
                }
                localeTemplateList = TemplateProxy.GetLocaleTemplates(templateDetails.TemplateId);
                ViewData["NotificationName"] = templateDetails.TemplateName;
                ViewData["NotificationTemplateId"] = templateDetails.TemplateId;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_FetchTemplate);

            }

            if (localeTemplateList != null && localeTemplateList.Count > 0)
            {
                ViewData["LocaleTemplateSource"] = localeTemplateList.Values.ToList();
                ViewData["localeTemplate"] = localeTemplateList.Values.ToList()[0];
            }
            ViewData["category"] = category;
            return View();
        }

        /// <summary>
        ///This method is used to adds the locale template.
        /// </summary>
        /// <param name="notificationTemplateId">notification template identifier.</param>
        /// <returns></returns>
        public ActionResult AddLocaleTemplate(string notificationTemplateId)
        {
            LocaleTemplate localeTemplate = new LocaleTemplate();
            localeTemplate.TemplateId = notificationTemplateId;
            GetLocaleIdList("LocaleNames", notificationTemplateId);
            return PartialView("AddLocaleTemplate", localeTemplate);
        }

        /// <summary>
        /// This method is used to adds the locale template details.
        /// </summary>
        /// <param name="localeTemplate">locale template.</param>
        /// <param name="template"> template.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public object AddLocaleTemplateDetails(LocaleTemplate localeTemplate, string template)
        {
            localeTemplate.ContentText = template;
            ValidateLocaleTemplate(localeTemplate);
            localeTemplate.CreatedBy = UserIdentity.UserId;

            try
            {
                if (ModelState.IsValid)
                {
                    TemplateProxy.SaveLocaleTemplate(localeTemplate);
                    ViewData["notificationType"] = "Email";
                    ViewData["NotificationTemplateId"] = localeTemplate.TemplateId;
                    return Sussess;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_SaveTemplate);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_AccessDenied);
            }
            GetLocaleIdList("LocaleNames", localeTemplate.TemplateId);
            return PartialView("AddLocaleTemplate", localeTemplate);
        }

        /// <summary>
        /// This method is used to shows the locale template details.
        /// </summary>
        /// <param name="notificationTemplateId">notification template identifier.</param>
        /// <param name="notificationMasterName">name of the notification master.</param>
        /// <param name="notificationType">type of notification</param>
        /// <returns></returns>
        public ActionResult ShowLocaleTemplateDetails(string notificationTemplateId, string notificationMasterName, string notificationType)
        {
            LocaleTemplateDetailsList(notificationTemplateId, notificationMasterName);
            ViewData["notificationType"] = notificationType;
            if (Request.IsAjaxRequest())
            {
                return PartialView("LocaleTemplate");
            }
            return View("LocaleTemplateDetails");
        }

        /// <summary>
        /// This method is used to edits the locale template.
        /// </summary>
        /// <param name="localeId">locale identifier.</param>
        /// <param name="templateId">template identifier.</param>
        /// <returns></returns>
        public ActionResult EditLocaleTemplate(string localeId, string templateId)
        {
            LocaleTemplate localeTemplate = null;
            try
            {
                localeTemplate = TemplateProxy.GetLocaleTemplateByLocaleId(templateId, localeId);
                ViewData["localeTemplate"] = localeTemplate;
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_AccessDenied);
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_FetchLocaleTemplate);
            }
            GetLocaleIdList("LocaleNames", templateId);
            return PartialView("EditLocaleTemplate", localeTemplate);
        }

        /// <summary>
        /// This method is used edits the locale template.
        /// </summary>
        /// <param name="localeTemplate">locale template.</param>
        /// <param name="template">template.</param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult EditLocaleTemplate(LocaleTemplate localeTemplate, string template)
        {
            localeTemplate.ContentText = template;
            ValidateLocaleTemplate(localeTemplate);
            try
            {
                if (ModelState.IsValid)
                {
                    string result = string.Empty;
                    result = TemplateProxy.SaveLocaleTemplate(localeTemplate);
                    if (!string.IsNullOrEmpty(result))
                    {
                        ModelState.AddModelError("StatusMessage", Resources.TemplateResource.s_SaveLocaleTemplate);
                        return Content(Sussess);
                    }
                    else
                    {
                        ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_SaveLocaletemplate);
                    }

                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_SaveLocaletemplate);
            }
            ViewData["localeTemplate"] = localeTemplate;
            return PartialView("EditLocaleTemplate", localeTemplate);
        }

        /// <summary>
        /// This method is used to removes the locale template.
        /// </summary>
        /// <param name="localeId">locale identifier.</param>
        /// <param name="templateId">template identifier.</param>
        /// <returns></returns>
        public ActionResult RemoveLocaleTemplate(string localeId, string templateId)
        {
            try
            {
                TemplateProxy.DeleteLocaleTemplate(localeId, templateId);
                ModelState.AddModelError("SuccessStatusMessage", Resources.TemplateResource.s_DeleteLocaleTemplate);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_AccessDenied);
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_DeleteLocaleTemplate);
            }

            LocaleTemplateDetailsList(templateId, "");
            return PartialView("LocaleTemplate");

        }

        #endregion

        /// <summary>
        /// This method is used to get the template details.
        /// </summary>
        /// <param name="tenantId">tenant identifier</param>
        /// <param name="category">category</param>
        private void GetTemplateDetails(string tenantId, string category)
        {
            try
            {
                Dictionary<string, TemplateDetail> templateDetails = null;
                if (!string.IsNullOrEmpty(category))
                {
                    templateDetails = TemplateProxy.GetAllTemplateDetailsByCategory(tenantId, category);
                }
                else
                {
                    templateDetails = TemplateProxy.GetAllTemplateDetails(tenantId);
                }
                if (templateDetails != null && templateDetails.Count > 0)
                {
                    ViewData["TemplateDetails"] = templateDetails.Values.ToList();
                }
                ViewData["TemplateCategory"] = category;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_AccessDenied);
            }
            catch (TemplateException exception)
            {
                ExceptionService.HandleException(exception, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_FetchTemplate);
            }
        }

        /// <summary>
        /// This method is used to validates the locale template.
        /// </summary>
        /// <param name="localeTemplate">locale template.</param>
        private void ValidateLocaleTemplate(LocaleTemplate localeTemplate)
        {
            if (string.IsNullOrEmpty(localeTemplate.LocaleId))
            {
                ModelState.AddModelError("LocaleId", "");
                ModelState.AddModelError("StatusMessageSummary", Resources.TemplateResource.e_LocaleIdMandatory);
            }
            if (string.IsNullOrEmpty(localeTemplate.ContentText.ToString()))
            {
                ModelState.AddModelError("Template", "");
                ModelState.AddModelError("StatusMessageSummary", Resources.TemplateResource.e_TemplateText);
            }
            else if (localeTemplate.ContentText.ToString() == "<br>")
            {
                ModelState.AddModelError("Template", "");
                ModelState.AddModelError("StatusMessageSummary", Resources.TemplateResource.e_TemplateText);
            }

        }

        /// <summary>
        /// This method is used to gets the locale identifier list.
        /// </summary>
        /// <param name="masterDataName">name of the master data.</param>
        /// <param name="notificationTemplateId">notification template identifier.</param>
        private void GetLocaleIdList(string masterDataName, string notificationTemplateId)
        {
            string statusMessage = string.Empty;
            Dictionary<string, GenericMasterData> dicMasterDataEntity = null;
            Dictionary<string, LocaleTemplate> localeTemplateList = null;
            try
            {
                localeTemplateList = TemplateProxy.GetLocaleTemplates(notificationTemplateId);
                dicMasterDataEntity = MasterDataServiceProxy.FetchMasterDataList(masterDataName, null, "LocaleNames_LocaleId");
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                statusMessage = Resources.TemplateResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                statusMessage = Resources.TemplateResource.e_ParameterEmptyOrNull;
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                statusMessage = Resources.MasterDataResource.a_MasterDataDetails_Fetch;
            }
            catch (TemplateException templateException)
            {
                ExceptionService.HandleException(templateException, _defaultPolicy);
                statusMessage = Resources.TemplateResource.e_FetchTemplate;
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, _defaultPolicy);
                statusMessage = Resources.MasterDataResource.e_MasterDataDetails_Fetch;
            }

            if (!string.IsNullOrEmpty(statusMessage))
                ModelState.AddModelError("StatusMessage", statusMessage);

            Dictionary<string, string> dictTableLocaleData = new Dictionary<string, string>();

            if (localeTemplateList != null && localeTemplateList.Count > 0)
            {
                foreach (var value in localeTemplateList.Values)
                {
                    dictTableLocaleData.Add(value.LocaleId, value.LocaleName);
                }
            }

            Dictionary<string, string> dictMasterLocaleData = new Dictionary<string, string>();
            if (dicMasterDataEntity != null && dicMasterDataEntity.Count > 0)
            {
                foreach (var entityValues in dicMasterDataEntity.Values)
                {
                    string localeId = entityValues.Properties["LocaleNames_Id"].ToString();
                    if (!dictTableLocaleData.ContainsKey(localeId))
                    {
                        dictMasterLocaleData.Add(entityValues.Properties["LocaleNames_Id"].ToString(), entityValues.Properties["LocaleNames_Name"].ToString());
                    }
                }
            }

            List<SelectListItem> lstLocaleId = new List<SelectListItem>();
            lstLocaleId.Add(new SelectListItem { Value = "", Text = Resources.TemplateResource.select });
            foreach (var Locale in dictMasterLocaleData)
            {
                lstLocaleId.Add(new SelectListItem
                {
                    Value = Locale.Key,
                    Text = Locale.Value
                });
            }
            ViewData["LocaleId"] = lstLocaleId;
        }

        /// <summary>
        /// This method is used to get the locale template details list.
        /// </summary>
        /// <param name="notificationTemplateId">notification template identifier.</param>
        /// <param name="notificationMasterName">name of the notification master.</param>
        private void LocaleTemplateDetailsList(string notificationTemplateId, string notificationMasterName)
        {
            Dictionary<string, LocaleTemplate> localeTemplateList = null;
            try
            {
                localeTemplateList = TemplateProxy.GetLocaleTemplates(notificationTemplateId);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_ParameterEmptyOrNull);
            }

            ViewData["LocaleTemplateSource"] = (localeTemplateList != null && localeTemplateList.Count > 0) ? localeTemplateList.Values.ToList() : null;
            ViewData["NotificationName"] = notificationMasterName;
            ViewData["NotificationTemplateId"] = notificationTemplateId;
        }

        /// <summary>
        /// This method is used to validate the template.
        /// </summary>
        /// <param name="template">template details</param>
        private void ValidateTemplate(TemplateDetail template)
        {
            if (string.IsNullOrEmpty(template.TemplateName))
            {
                ModelState.AddModelError("StatusMessage", Resources.TemplateResource.e_TemplateName);
            }
        }

        /// <summary>
        /// This method is used to gets the template categories.
        /// <param name="template">template details</param>
        /// </summary>
        private void GetTemplateCategories(TemplateDetail template)
        {
            var templateCategories = new Dictionary<string, string>();          
            templateCategories.Add("NotificationTemplate", "Notification Template");
            templateCategories.Add("EventTemplate", "Event Template");

            ViewData["Category"] = new SelectList(templateCategories, "Key", "Value", template.Category ?? string.Empty);

        }

        /// <summary>
        /// This method is used to gets the template categories.
        /// <param name="template">template details</param>
        /// </summary>
        public JsonResult GetCategories(TemplateDetail template)
        {
            var templateCategories = new Dictionary<string, string>();         
            templateCategories.Add("NotificationTemplate", "Notification Template");
            templateCategories.Add("EventTemplate", "Event Template");

            var templateCategoryAndId = templateCategories.Select(t => new
            {
                CategoryId = t.Key,
                CategoryName = t.Value
            });

            return Json(templateCategoryAndId);
        }
    }
}
