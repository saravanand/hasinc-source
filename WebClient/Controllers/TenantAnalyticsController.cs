﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.ServiceProxies.ProductAnalytics;
using CelloSaaS.Model.ProductAnalytics;
using System.Configuration;
using CelloSaaS.Library;
using CelloSaaS.ServiceContracts.ProductAnalytics;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for tenant analytics.
    /// </summary>
    public class TenantAnalyticsController : CelloController
    {
        private static Dictionary<string, Dictionary<int, int>> frequnctSetting = new Dictionary<string, Dictionary<int, int>>();
        private const string DefaultPolicy = "GlobalExceptionLogger";
       
        /// <summary>
        /// This method is used to get the tenant analytics index page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This method is used to get the hot trial search view.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult HotTrialSearch()
        {
            SetFrequence();
            ViewData["ApplicationFreq"] = GetApplicationFrequenceList("70,100");
            return PartialView("HotTrialSearch");
        }

        /// <summary>
        /// This method is used to get the hot trial based on the search condition.
        /// </summary>
        /// <param name="searchCondition">search condition.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="page">page.</param>
        /// <returns></returns>
        public PartialViewResult HotTrial(ProductAnalyticsTrialSearchCondition searchCondition, int pageSize = 10, int page = 1)
        {
            HotTrialSearchResult result = null;
            try
            {
                SetFrequence();

                //var searchCondition = new ProductAnalyticsTrialSearchCondition();
                TryUpdateModel(searchCondition, "HotTrial");

                searchCondition.PageNo = page;
                searchCondition.PageSize = pageSize;

                searchCondition.TrialFromDate = (searchCondition.TrialFromDate == null && searchCondition.TrialToDate == null ? DateTime.Today.AddMonths(-2).Date : searchCondition.TrialFromDate);
                searchCondition.VisitFrequently = string.IsNullOrEmpty(searchCondition.VisitFrequently) ? "70,100" : searchCondition.VisitFrequently;
                ViewData["PageNumber"] = searchCondition.PageNo;
                ViewData["PageSize"] = searchCondition.PageSize;
                ViewData["SearchCondition"] = searchCondition;
                ViewData["ApplicationFreq"] = GetApplicationFrequenceList(searchCondition.VisitFrequently);

                result = ProductAnalyticsProxy.GetHotTrials(searchCondition);


                if (result != null)
                {
                    ViewData["TotalCount"] = result.TotalCount;
                    ViewData["TrialList"] = SetFrequncyText(result.Items);
                }
            }
            catch (FormatException fex)
            {
                ExceptionService.HandleException(fex, DefaultPolicy);
                ModelState.AddModelError("Error", fex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }

            return PartialView("HotTrialResult", result != null ? result.Items : null);
        }

        /// <summary>
        /// This method is used to get the trial needing attention view.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult TrialsNeedingAttentionSearch()
        {

            SetFrequence();
            ViewData["ApplicationFreq"] = GetApplicationFrequenceList("0,40");
            return PartialView("TrialAttentionSearch");
        }

        /// <summary>
        /// This method is used to get the trial need attention view based on the form collection.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="page">page.</param>
        /// <returns></returns>
        public PartialViewResult TrialNeedAttention(FormCollection formCollection, int pageSize = 10, int page = 1)
        {
            HotTrialSearchResult result = null;
            try
            {
                SetFrequence();

                var searchCondition = new ProductAnalyticsTrialSearchCondition();
                TryUpdateModel(searchCondition, "TrialNeedAtt");

                searchCondition.PageNo = page;
                searchCondition.PageSize = pageSize;

                searchCondition.TrialFromDate = (searchCondition.TrialFromDate == null && searchCondition.TrialToDate == null ? DateTime.Today.AddMonths(-2) : searchCondition.TrialFromDate);
                searchCondition.VisitFrequently = string.IsNullOrEmpty(searchCondition.VisitFrequently) ? "0,40" : searchCondition.VisitFrequently;

                ViewData["PageNumber"] = searchCondition.PageNo;
                ViewData["PageSize"] = searchCondition.PageSize;
                ViewData["SearchCondition"] = searchCondition;
                ViewData["ApplicationFreq"] = GetApplicationFrequenceList(searchCondition.VisitFrequently);

                result = ProductAnalyticsProxy.GetTrialsNeedAttention(searchCondition);


                if (result != null)
                {
                    ViewData["TotalCount"] = result.TotalCount;
                    ViewData["TrialList"] = SetFrequncyText(result.Items);
                }

            }
            catch (FormatException fex)
            {
                ExceptionService.HandleException(fex, DefaultPolicy);
                ModelState.AddModelError("Error", fex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            return PartialView("TrialAttentionResult", result != null ? result.Items : null);

        }

        /// <summary>
        /// This method is used for getting the customer needing attention view.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult CustomerNeedingAttentionSearch()
        {
            SetFrequence();
            ViewData["ApplicationFreq"] = GetApplicationFrequenceList("0,40");
            return PartialView("CustomerNeedingAttentionSearch");
        }
        
        /// <summary>
        /// This method is used to get the customer need attention view based on the search condition.
        /// </summary>
        /// <param name="searchCondition">search condition.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="page">page.</param>
        /// <returns></returns>
        public PartialViewResult CustomerNeedAttention(ProductAnalyticsTrialSearchCondition searchCondition, int pageSize = 10, int page = 1)
        {
            HotTrialSearchResult result = null;
            try
            {
                SetFrequence();

                TryUpdateModel(searchCondition, "Customer");

                searchCondition.PageNo = page;
                searchCondition.PageSize = pageSize;

                searchCondition.VisitFrequently = string.IsNullOrEmpty(searchCondition.VisitFrequently) ? "0,40" : searchCondition.VisitFrequently;

                ViewData["PageNumber"] = searchCondition.PageNo;
                ViewData["PageSize"] = searchCondition.PageSize;
                ViewData["SearchCondition"] = searchCondition;
                ViewData["ApplicationFreq"] = GetApplicationFrequenceList(searchCondition.VisitFrequently);

                result = ProductAnalyticsProxy.GetCustomerNeedAttention(searchCondition);

                if (result != null)
                {
                    ViewData["TotalCount"] = result.TotalCount;
                    ViewData["CustomerList"] = SetFrequncyText(result.Items);
                }
            }
            catch (FormatException fex)
            {
                ExceptionService.HandleException(fex, DefaultPolicy);
                ModelState.AddModelError("Error", fex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (ProductAnalyticsException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            if (Request.IsAjaxRequest())
            {
                return PartialView("CustomerNeedingAttentionResult", result != null ? result.Items : null);
            }
            return PartialView("CustomerNeedingAttentionResult", result != null ? result.Items : null);
        }

        /// <summary>
        /// This method is used to get the application frequency list.
        /// </summary>
        /// <param name="selectedValue">selected value.</param>
        /// <returns></returns>
        private List<SelectListItem> GetApplicationFrequenceList(string selectedValue)
        {
            List<SelectListItem> lstApplicationAccessed = new List<SelectListItem>();
            lstApplicationAccessed.Add(new SelectListItem { Value = "-1", Text = "-All-" });

            foreach (var item in frequnctSetting)
            {
                foreach (var inneritem in item.Value)
                {
                    if (selectedValue.Trim() == string.Format("{0},{1}", inneritem.Key, inneritem.Value).Trim())
                        lstApplicationAccessed.Add(new SelectListItem { Value = string.Format("{0},{1}", inneritem.Key, inneritem.Value), Text = item.Key, Selected = true });
                    else
                        lstApplicationAccessed.Add(new SelectListItem { Value = string.Format("{0},{1}", inneritem.Key, inneritem.Value), Text = item.Key });
                }
            }
            return lstApplicationAccessed;
        }

        /// <summary>
        /// This method is used to set the frequency text.
        /// </summary>
        /// <param name="ProductAnalyticsList">list product analytics.</param>
        /// <returns></returns>
        private List<ProductAnalyticsTrial> SetFrequncyText(List<ProductAnalyticsTrial> ProductAnalyticsList)
        {
            foreach (ProductAnalyticsTrial prod in ProductAnalyticsList)
            {
                foreach (var item in frequnctSetting)
                {
                    foreach (var inneritem in item.Value)
                    {

                        if (inneritem.Key == 0)
                        {
                            if (prod.UsedPercentage >= inneritem.Key && prod.UsedPercentage <= inneritem.Value)
                            {
                                prod.visitFrequency = item.Key;
                            }
                        }
                        else if (prod.UsedPercentage > inneritem.Key && prod.UsedPercentage <= inneritem.Value)
                        {
                            prod.visitFrequency = item.Key;
                        }
                        else if (inneritem.Value == 100)
                        {
                            if (prod.UsedPercentage > inneritem.Key)
                            {
                                prod.visitFrequency = item.Key;
                            }
                        }
                    }
                }
            }

            return ProductAnalyticsList;
        }

        /// <summary>
        /// This method is used to set the frequency.
        /// </summary>
        private void SetFrequence()
        {
            if (frequnctSetting.Count > 0) return;

            if (ConfigurationManager.AppSettings["HotTrialFrequency"] != null)
            {
                string[] freqList = ConfigurationManager.AppSettings["HotTrialFrequency"].Split('|');

                foreach (string freq in freqList)
                {
                    string[] namevalue = freq.Split(':');
                    if (namevalue.Length >= 2)
                    {
                        string key = namevalue[0];
                        string value = namevalue[1];

                        string[] FreqIndex = value.Split(',');
                        Dictionary<int, int> item = new Dictionary<int, int>();

                        if (FreqIndex.Length >= 2)
                        {
                            item.Add(Convert.ToInt32(FreqIndex[0]), Convert.ToInt32(FreqIndex[1]));
                        }
                        frequnctSetting.Add(key, item);
                    }
                }

            }
        }
    }
}
