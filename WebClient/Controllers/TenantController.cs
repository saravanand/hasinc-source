using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.EventScheduler.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Library.Utilities;
using CelloSaaS.Model;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.MasterData;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.PartitionManagement.Model;
using CelloSaaS.PartitionManagement.ServiceProxies;
using CelloSaaS.Rules.Core;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.MasterData;
using CelloSaaS.ServiceContracts.Providers;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.ServiceContracts.ViewManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.Configuration;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.MasterData;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.ServiceProxies.ViewManagement;
using CelloSaaS.View;
using CelloSaaSApplication.Models;
using CelloSaaSWebClient.Services;
using CelloSaaSWebClient.Models;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for tenant management.
    /// </summary>
    [HandleError]
    public class TenantController : CelloSaaS.View.CelloController
    {
        private string TenantId { get { return TenantContext.GetTenantId(new TenantDetails().EntityIdentifier); } }

        #region Declaration

        private const string DefaultLogoImagePath = "LogoImageFolder";
        private const string DefaultPolicy = "GlobalExceptionLogger";

        #endregion Declaration

        #region Manage Tenant Relations

        /// <summary>
        /// This method is used to get the tenant management view.
        /// </summary>
        /// <returns></returns>
        public ActionResult TenantRelations()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["parentTenantID"]))
            {
                ViewData["ParentTenantID"] = Request.QueryString["parentTenantID"];
                ViewData["ParentTenantName"] = Request.QueryString["parentTenantName"];
            }
            else
            {
                return RedirectToAction("TenantList");
            }

            string childTenantID = string.Empty, relationTypeID = string.Empty;

            //check for delete request
            if (!string.IsNullOrEmpty(Request.QueryString["deleteTenantRelationId"]))
            {
                TenantRelationProxy.DeleteTenantRelation(Request.QueryString["deleteTenantRelationId"]);
                TempData["statusMsg"] = Resources.TenantResource.m_RelationDeleted;
            }
            //check for edit request
            else if (!string.IsNullOrEmpty(Request.QueryString["editRelationID"]))
            {
                ViewData["RelationID"] = Request.QueryString["editRelationID"];
                ViewData["FromMode"] = "INSERT";

                try
                {
                    var tenantRelationMapping = TenantRelationProxy.GetTenantMappingDetails(Request.QueryString["editRelationID"]);

                    if (tenantRelationMapping != null)
                    {
                        childTenantID = tenantRelationMapping.ChildTenantID;
                        relationTypeID = tenantRelationMapping.TenantRelationTypeID;
                        //relation found so change to edit mode
                        ViewData["ChildTenantID"] = childTenantID;
                        ViewData["FromMode"] = "EDIT";
                    }
                    else
                    {
                        //relation not found
                        TempData["statusMsg"] = Resources.TenantResource.e_RelationNotFound;
                    }
                }
                catch
                {
                    TempData["statusMsg"] = Resources.TenantResource.e_TenantRelationMapping;
                }
            }

            LoadTenantRelations();
            LoadTenantRelationTypes(relationTypeID);
            LoadTenantList(childTenantID);

            return View();
        }

        /// <summary>
        /// This method is used to get the tenant management view based on the given form collection
        /// </summary>
        /// <param name="forms">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TenantRelations(FormCollection forms)
        {
            //get the parent tenat id from the posted data
            var parentTenantID = forms["ParentTenantID"];
            var parentTenantName = forms["ParentTenantName"];
            ViewData["ParentTenantID"] = parentTenantID;
            ViewData["ParentTenantName"] = parentTenantName;

            if (string.IsNullOrEmpty(parentTenantID)
                || !string.IsNullOrEmpty(forms["Cancel.x"]))
            {
                //not found so redirect
                return RedirectToAction("TenantRelations", new { parentTenantID, parentTenantName });
            }


            var childTenantID = forms["TenantList"];
            var relationTypeID = forms["TenantRelationTypes"];

            if (!string.IsNullOrEmpty(forms["RelationID"]))
            {
                ViewData["FromMode"] = "EDIT";
            }
            else
            {
                ViewData["FromMode"] = "INSERT";
            }

            if (!string.IsNullOrEmpty(childTenantID)
                && !string.IsNullOrEmpty(relationTypeID))
            {
                if (childTenantID == "0")
                {
                    ModelState.AddModelError("TenantRelationMessage", Resources.TenantResource.e_TenantMandatory);
                }
                else if (relationTypeID == "0")
                {
                    ModelState.AddModelError("TenantRelationMessage", Resources.TenantResource.e_TenantRelationTypeMandatory);
                }
                else
                {
                    try
                    {
                        if (forms["RelationID"] != null
                            && !string.IsNullOrEmpty(forms["RelationID"])
                            && !string.IsNullOrEmpty(forms["Save.x"]))
                        {
                            //update tenant relation
                            TenantRelationProxy.UpdateTenantRelation(forms["RelationID"], parentTenantID, childTenantID, relationTypeID);
                            TempData["statusMsg"] = "Tenant Relation Updated!";
                        }
                        else
                        {
                            //create new tenant relation
                            TenantRelationProxy.AddTenantRelation(parentTenantID, childTenantID, relationTypeID);
                            TempData["statusMsg"] = "Tenant Relation Added!";
                        }

                        return RedirectToAction("TenantRelations", new { parentTenantID, parentTenantName });
                    }
                    catch (DuplicateTenantRelationException ex)
                    {
                        ModelState.AddModelError("TenantRelationMessage", ex.Message);
                    }
                    catch
                    {
                        ModelState.AddModelError("TenantRelationMessage", Resources.TenantResource.e_TenantRelationMapping);
                    }
                }
            }
            else
            {
                ModelState.AddModelError("TenantRelationMessage", Resources.TenantResource.e_TenantMandatory + " & " + Resources.TenantResource.e_TenantRelationTypeMandatory);
            }


            ViewData["ChildTenantID"] = childTenantID;
            ViewData["RelationID"] = forms["RelationID"];
            LoadTenantRelations();
            LoadTenantRelationTypes(relationTypeID);
            LoadTenantList(childTenantID);

            return View();
        }

        /// <summary>
        /// This method is used to load the tenant relations.
        /// </summary>
        private void LoadTenantRelations()
        {
            var tenantRelations = TenantRelationProxy.GetRelatedTenants((string)ViewData["ParentTenantID"]);

            if (tenantRelations != null)
            {
                ViewData["TenantRelations"] = tenantRelations;
            }
        }

        /// <summary>
        /// This method is used to load the tenant relation types based on the given relation type identifier.
        /// </summary>
        /// <param name="relationTypeID">relation type identifier.</param>
        private void LoadTenantRelationTypes(string relationTypeID)
        {
            var tenantRelationTypes = TenantRelationProxy.GetAllTenantRelationTypes();

            var lstRelationTypes = (from rt in tenantRelationTypes
                                    select new SelectListItem
                                    {
                                        Text = rt.Name,
                                        Value = rt.ID
                                    }).ToList();

            lstRelationTypes.Insert(0, new SelectListItem
            {
                Text = "---Select---",
                Value = "0"
            });

            //find the relation to select
            var temp = lstRelationTypes.Find(rt => rt.Value == relationTypeID);
            if (temp != null)
            {
                temp.Selected = true;
            }
            else
            {
                lstRelationTypes.ElementAt(0).Selected = true;
            }


            ViewData["TenantRelationTypes"] = lstRelationTypes;
        }

        /// <summary>
        /// This method is used to load the tenant list based on the child tenant identifier.
        /// </summary>
        /// <param name="childTenantID">child tenant identifier.</param>
        private void LoadTenantList(string childTenantID)
        {
            var tenantdetails = new List<TenantDetails>();// = TenantRelationProxy.GetChildTenantList((string)ViewData["ParentTenantID"]);

            //form select list of tenants
            var tenantList = (from td in tenantdetails
                              select new SelectListItem
                              {
                                  Text = td.TenantName,
                                  Value = td.TenantCode
                              }).ToList();

            tenantList.Insert(0, new SelectListItem
            {
                Text = "---Select---",
                Value = "0",
            });

            //find the child tenant to select
            var tmp = tenantList.Find(rt => rt.Value == childTenantID);
            if (tmp != null)
            {
                tmp.Selected = true;
            }
            else
            {
                tenantList.ElementAt(0).Selected = true;
            }

            ViewData["TenantList"] = tenantList;
        }

        #endregion

        #region Manage Tax

        /// <summary>
        /// This method is used to add/update tax.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>Add/Edit view of tax</returns>
        public ActionResult ManageTax(Guid? TenantId)
        {
            TaxRate taxRate = null;
            try
            {
                taxRate = TaxProxy.GetTaxRateByTenantId(TenantId ?? null);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_ParameterEmptyOrNull);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_TaxLoaded);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_TaxLicense);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_Access);
                ViewBag.AccessDenied = true;
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", settingsTemplateException.Message);
            }

            ViewData["TaxRate"] = taxRate;
            return View();
        }

        /// <summary>
        /// This method is used to manage the tax.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <param name="tenantTaxRate">tenant tax rate.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManageTax(FormCollection formCollection, List<TenantTaxRate> tenantTaxRate)
        {
            var taxRate = new TaxRate();
            try
            {
                if (tenantTaxRate == null) tenantTaxRate = new List<TenantTaxRate>();
                TryUpdateModel(taxRate, "TaxRate");
                taxRate.TenantId = null;
                taxRate.TenantTaxRates = tenantTaxRate;

                if (ValidateTax(tenantTaxRate))
                {
                    ProcessTax(taxRate);
                    ModelState.AddModelError("TaxSuccessMessage", Resources.TaxResource.e_SuccessMessage);
                }
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TenantResource.e_ParameterEmptyOrNull);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TenantResource.e_TenantLoaded);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TenantResource.e_TenantLicense);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", Resources.TenantResource.e_Access);
                ViewBag.AccessDenied = true;
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, DefaultPolicy);
                ModelState.AddModelError("TaxErrorMessage", settingsTemplateException.Message);
            }

            ViewData["TaxRate"] = taxRate;
            return View();
        }

        /// <summary>
        /// This method is used to process the tax rate.
        /// </summary>
        /// <param name="taxRate">tax rate.</param>
        private void ProcessTax(TaxRate taxRate)
        {
            var prevTaxRate = TaxProxy.GetTaxRateByTenantId(taxRate.TenantId);
            taxRate.TaxRateId = prevTaxRate == null ? Guid.NewGuid() : prevTaxRate.TaxRateId;

            if (taxRate.TenantTaxRates != null)
            {
                foreach (TenantTaxRate ttr in taxRate.TenantTaxRates)
                {
                    ttr.TenantTaxRateId = Guid.NewGuid();
                    ttr.TaxRateId = taxRate.TaxRateId;
                }
            }

            TaxProxy.AddTaxRate(taxRate);
        }

        /// <summary>
        /// This method is used to validate the tax.
        /// </summary>
        /// <param name="tenantTaxRate">list of tenant tax rate.</param>
        /// <returns></returns>
        private bool ValidateTax(List<TenantTaxRate> tenantTaxRate)
        {
            //if (tenantTaxRate == null)
            //{
            //    ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_TaxRateRow);
            //    return false;
            //}

            if (tenantTaxRate != null && tenantTaxRate.Count > 0)
            {
                if (!tenantTaxRate.Any(x => x.TaxOrder == 0))
                {
                    ModelState.AddModelError("TaxErrorMessage", Resources.TenantResource.e_ShouldContaintax);
                    return false;
                }

                int lineItemCount = tenantTaxRate.Count;
                foreach (TenantTaxRate ttr in tenantTaxRate)
                {
                    if (string.IsNullOrWhiteSpace(ttr.Name))
                    {
                        ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_Name);
                    }

                    if (ttr.TaxOrder < 0 || ttr.TaxOrder >= lineItemCount)
                    {
                        if (!string.IsNullOrEmpty(ttr.Name))
                            ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_TaxOrder.Replace("[NAME]", ttr.Name));
                        else
                            ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_InvalidTaxOrder);
                    }

                    if (ttr.Percentage <= 0.0 || ttr.Percentage > 100.0)
                    {
                        if (!string.IsNullOrEmpty(ttr.Name))
                            ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.PercentageZero.Replace("[NAME]", ttr.Name));
                        else
                            ModelState.AddModelError("TaxErrorMessage", Resources.TaxResource.e_InvalidPercentage);
                    }
                }
            }

            return ModelState.IsValid;
        }

        /// <summary>
        /// This method is used to load tax rate based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        private void LoadTaxRate(string tenantId)
        {
            TaxRate taxRate = null;

            if (string.IsNullOrEmpty(tenantId))
            {
                taxRate = TaxProxy.GetTaxRateByTenantId(null);
            }
            else
            {
                taxRate = TaxProxy.GetTaxRateByTenantId(Guid.Parse(tenantId));

                if (taxRate == null)
                    taxRate = TaxProxy.GetTaxRateByTenantId(null);
            }

            ViewData["TaxRate"] = taxRate;
        }

        #endregion Tax

        #region Manage Tenant

        /// <summary>
        /// This method is used to add/update tenant.
        /// </summary>
        /// <param name="tenantId">tenant identifier</param>
        /// <returns>Add/Edit view of tenant</returns>
        public ActionResult ManageTenant(string tenantId)
        {
            Tenant tenant = null;
            TenantDetails parentTenant = null;
            TenantLicense tenantLicense = null;
            string tenantLicensePackageId = string.Empty;
            string parentId = this.TenantId;
            bool isIsvOrReseller = false;

            try
            {
                // Allow this setup only when the tenant is being created 
                if (string.IsNullOrEmpty(tenantId))
                {
                    if (ConfigHelper.EnableExternalAuthentication)
                    {
                        new CelloSaaSWebClient.Models.Helpers().IdentitySetup(this, null, null);
                    }
                    else
                    {
                        ViewBag.Provider = Constants.CelloOpenIdProviderName;
                        ViewBag.AuthenticationType = Constants.CelloOpenIdProviderName;
                    }
                }
                else
                {
                    var tenantSettings = TenantSettingsProxy.GetTenantSettings(tenantId);
                    string providerId = tenantSettings.GetValue(SettingAttributeConstants.IdProviders), authTypeId = tenantSettings.GetValue(SettingAttributeConstants.TenantDefaultAuthenticationType);

                    if (string.IsNullOrEmpty(providerId))
                    {
                        providerId = AppSettingHelper.CelloOpenIdentity;
                        authTypeId = AppSettingHelper.CelloAuthentication;
                        ViewBag.Provider = Constants.CelloOpenIdProviderName;
                        ViewBag.AuthenticationType = Constants.CelloOpenIdProviderName;
                    }
                    else if (ConfigHelper.EnableExternalAuthentication)
                    {
                        providerId = providerId.ToLowerInvariant();
                        authTypeId = authTypeId.ToLowerInvariant();

                        var providers = AuthenticationService.GetAuthenticationProviders() ?? new Dictionary<string, string>();
                        var authTypeCollection = AuthenticationService.GetAuthenticationTypes(providerId) ?? new Dictionary<string, Dictionary<string, string>>();
                        ViewBag.Provider = providers.ContainsKey(providerId)
                                            ? providers[providerId]
                                            : AppSettingHelper.CelloOpenIdentity;

                        ViewBag.AuthenticationType = authTypeCollection.ContainsKey(providerId)
                            ? authTypeCollection[providerId].ContainsKey(authTypeId)
                                ? authTypeCollection[providerId][authTypeId]
                                : Constants.CelloOpenIdProviderName
                            : Constants.CelloOpenIdProviderName;
                    }

                    if (string.IsNullOrEmpty(authTypeId))
                    {
                        authTypeId = AppSettingHelper.CelloAuthentication;
                    }
                }

                var loggedTenantInfo = TenantProxy.GetTenantDetailsByTenantId(this.TenantId, this.TenantId);
                isIsvOrReseller = loggedTenantInfo.Types != null
                            && loggedTenantInfo.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);

                ViewData["IsIsvOrReseller"] = isIsvOrReseller;

                if (string.IsNullOrEmpty(tenantId))
                {
                    parentTenant = loggedTenantInfo.TenantDetails;
                    ViewData["dbType"] = "Shared";
                    ViewData["tenantConnectionString"] = string.Empty;
                    ViewData["FormMode"] = CurrentFormMode.Insert;
                    tenant = new Tenant();
                    tenantLicense = new TenantLicense
                    {
                        ValidityStart = DateTime.Now
                    };
                }
                else
                {
                    ViewData["FormMode"] = CurrentFormMode.Edit;

                    Guid tmp;
                    if (!Guid.TryParse(tenantId, out tmp))
                        return HttpNotFound("Tenant not found!");

                    parentTenant = TenantRelationProxy.GetParentTenant(tenantId);

                    if (parentTenant != null)
                    {
                        parentId = parentTenant.TenantCode;
                    }

                    if (!AccessControlProxy.CheckTenantAccessWithTenantScope(UserIdentity.UserId, PrivilegeConstants.UpdateTenant, parentId, "Tenant", FetchType.Edit, this.TenantId))
                    {
                        throw new UnauthorizedAccessException(Resources.TenantResource.e_EditAccessDenied);
                    }

                    tenant = TenantProxy.GetTenantDetailsByTenantId(tenantId, this.TenantId);

                    if (tenant == null)
                    {
                        return HttpNotFound("Tenant not found!");
                    }
                    else
                    {
                        if (tenant.Address == null)
                        {
                            tenant.Address = new Address();
                        }

                        if (tenant.ContactDetail == null)
                        {
                            tenant.ContactDetail = new ContactDetails();
                        }

                        // get license info
                        tenantLicense = LicenseProxy.GetTenantLicenseHistory(tenantId).First();

                        tenantLicensePackageId = tenantLicense.PackageId;
                        LoadPricePlanList(tenantLicense.PackageId, tenantLicense.PricePlanId);
                    }
                }
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ParameterEmptyOrNull);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantLoaded);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantLicense);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Access);
                ViewBag.AccessDenied = true;
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", settingsTemplateException.Message);
            }

            // Get the list of country and license package.
            LoadCountry((tenant == null || tenant.Address == null) ? string.Empty : tenant.Address.CountryId);
            LoadLicensepackages(string.IsNullOrEmpty(tenantId) ? parentId : tenantId, tenantLicensePackageId, string.IsNullOrEmpty(tenantId));
            LoadTenantLicenseHistory(tenantId);
            LoadTenantTypes(tenant == null ? null : tenant.Types); //get list of tenant types
            LoadCompanySize(tenant != null ? tenant.TenantDetails.CompanySize : null);
            LoadDataPartitionDetails(tenant == null ? null : tenant.TenantDetails.DataPartitionId, isIsvOrReseller, parentTenant != null ? parentTenant.DataPartitionId : null);

            if (isIsvOrReseller)
            {
                LoadTaxRate(tenantId);
            }

            ViewData["TenantDetails"] = tenant;
            ViewData["TenantLicense"] = tenantLicense;

            return View();
        }

        /// <summary>
        /// This method is used to returns list of dataPartition.
        /// </summary>
        /// <param name="dataPartitionID">dataPartition identifier.</param>
        /// <param name="isIsvOrReseller">IsOverReseller.</param>
        /// <param name="parentPartitionId">parent partition identifier.</param>
        private void LoadDataPartitionDetails(string dataPartitionID, bool isIsvOrReseller, string parentPartitionId)
        {
            var dataPartitionDic = DataPartitionProxy.GetAllDataPartition();

            if (dataPartitionDic != null && dataPartitionDic.Count > 0 && dataPartitionDic.Values != null && dataPartitionDic.Values.Count > 0)
            {
                var dataPartitionList = dataPartitionDic.Values.ToList();
                if (!string.IsNullOrEmpty(dataPartitionID))
                {
                    var selected = dataPartitionList.Where(x => x.Id.ToString().Equals(dataPartitionID, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    ViewData["DataPartitionName"] = selected != null ? selected.DataPartitionName : "No DataPartition";
                }

                Guid defaultPartitionSetID = (from c in dataPartitionList
                                              where c.IsDefault.Equals(true)
                                              select c.Id).FirstOrDefault();

                if (!isIsvOrReseller && !string.IsNullOrEmpty(parentPartitionId))
                {
                    ViewData["ParentPartitionId"] = parentPartitionId;
                    dataPartitionList = dataPartitionList.Where(x => x.Id.ToString().Equals(parentPartitionId, StringComparison.OrdinalIgnoreCase)).ToList();
                }

                var items = (from tt in dataPartitionList
                             select new SelectListItem
                             {
                                 Text = tt.DataPartitionName,
                                 Value = tt.Id.ToString(),
                                 Selected = string.IsNullOrWhiteSpace(dataPartitionID) ? tt.Id.Equals(defaultPartitionSetID) : tt.Id.Equals(Guid.Parse(dataPartitionID)),
                             }).ToList<SelectListItem>();

                ViewData["Tenant.TenantDetails.DataPartitionId"] = items;
            }
            else
            {
                var lst = new List<SelectListItem>();
                lst.Add(new SelectListItem { Text = "--Select a partition--", Value = "" });
                ViewData["Tenant.TenantDetails.DataPartitionId"] = lst;
            }
        }

        /// <summary>
        /// This method is used to save tenant details.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <param name="tenantTaxRate">tenant tax rate.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> ManageTenant(FormCollection formCollection, List<TenantTaxRate> tenantTaxRate)
        {
            Tenant tenant = null;
            TenantDetails parentTenant = null;
            TaxRate taxRate = new TaxRate();
            TenantLicense tenantLicense = null, prevLicense = null;
            string tenantCode = formCollection["Tenant.Identifier"] ?? string.Empty;
            string parentId = this.TenantId;
            string userConnString = formCollection["UserConnectionString"];
            bool isIsvOrReseller = false;

            try
            {
                // Allow this setup only when the tenant is being created and to show when there is an error
                if (string.IsNullOrEmpty(tenantCode) && ConfigHelper.EnableExternalAuthentication)
                {
                    new Helpers().IdentitySetup(this, formCollection["TenantIdProvider"], formCollection["UserAuthenticationType"], formCollection["OnPremiseServerUri"]);
                }
                else
                {
                    ViewBag.Provider = Constants.CelloOpenIdProviderName;
                    ViewBag.AuthenticationType = Constants.CelloOpenIdProviderName;
                }

                // during the tenant creation, if no authentiation provider is chosen,
                if (ConfigHelper.EnableExternalAuthentication && string.IsNullOrEmpty(tenantCode) && string.IsNullOrEmpty(formCollection["UserAuthenticationType"]))
                {
                    ModelState.AddModelError("UserAuthenticationType", "");
                    ModelState.AddModelError("TenantErrorMessage", "Choose a valid Authentication Type");
                }

                var loggedTenantInfo = TenantProxy.GetTenantDetailsByTenantId(this.TenantId, this.TenantId);
                isIsvOrReseller = loggedTenantInfo.Types != null && loggedTenantInfo.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);
                ViewData["IsIsvOrReseller"] = isIsvOrReseller;

                if (!string.IsNullOrEmpty(tenantCode))
                {
                    ViewData["FormMode"] = CurrentFormMode.Edit;
                    tenant = TenantProxy.GetTenantInfo(tenantCode);
                    parentTenant = TenantRelationProxy.GetParentTenant(tenantCode);
                    if (parentTenant != null)
                    {
                        parentId = parentTenant.TenantCode;
                    }
                    // Get the License details  for tenant.
                    tenantLicense = LicenseProxy.GetTenantLicenseHistory(tenantCode).First();

                    prevLicense = CelloXmlSerializer.Clone<TenantLicense>(tenantLicense);

                    if (tenant != null && tenant.TenantDetails != null)
                    {
                        if (tenant.TenantDetails.ParentTenant == null)
                        {
                            tenant.TenantDetails.ParentTenant = new TenantDetails() { TenantCode = parentId };
                        }
                    }
                }
                else
                {
                    parentTenant = loggedTenantInfo.TenantDetails;
                    ViewData["FormMode"] = CurrentFormMode.Insert;
                    tenant = new Tenant();
                    tenant.TenantDetails.ParentTenant = new TenantDetails() { TenantCode = parentId };
                    tenantLicense = new TenantLicense();
                }

                string[] selectedTenantTypes = new string[0];

                //check whether the user selected at least one tenant type
                if (formCollection["SelectedTenantTypes"] != null && !string.IsNullOrEmpty(formCollection["SelectedTenantTypes"]))
                {
                    selectedTenantTypes = formCollection["SelectedTenantTypes"].Split(',');
                }
                else
                {
                    ModelState.AddModelError("Tenant.Types", Resources.TenantResource.e_ChooseTenantType);
                }

                TryUpdateModel(tenant, "Tenant");
                TryUpdateModel(tenantLicense, "TenantLicense");
                tenantLicense.TenantId = tenantCode;

                //validate the models
                ValidateTenant(tenant.TenantDetails);
                ValidateLicense(tenantLicense, prevLicense);

                string ldapServerUri = formCollection["OnPremiseServerUri"];
                if (ConfigHelper.EnableExternalAuthentication)
                {
                    if (!string.IsNullOrEmpty(ldapServerUri) && !Util.ValidateURL(ldapServerUri))
                    {
                        ModelState.AddModelError("TenantErrorMessage", "");
                        ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_URL);
                    }
                }

                if (isIsvOrReseller)
                {
                    if (tenantTaxRate != null && tenantTaxRate.Count > 0)
                    {
                        TryUpdateModel(taxRate, "TaxRate");
                        taxRate.TenantTaxRates = tenantTaxRate;
                        ValidateTax(tenantTaxRate);
                    }
                }

                AddressValidation(tenant);

                ContactValidation(tenant);

                if (string.IsNullOrEmpty(tenantCode))
                {
                    ValidateUserAndMembership(tenant.TenantAdminMembershipdetail, tenant.TenantAdminUserdetail);
                }

                if (!string.IsNullOrEmpty(userConnString))
                {
                    string provider = System.Configuration.ConfigurationManager.ConnectionStrings[CelloSaaS.Library.DAL.Constants.CelloSaaSConnectionString].ProviderName;
                    TenantSettingsProxy.ValidateConnectionString(userConnString, provider);
                    userConnString += "~" + provider;
                }

                if (tenant.Types == null)
                {
                    tenant.Types = new List<TenantType>();
                }

                tenant.Types.Clear(); //clear the previous types
                foreach (var type in selectedTenantTypes)
                {
                    //add the newly selected type for updating or adding
                    tenant.Types.Add(new TenantType { ID = type });
                }

                string discountCode = null, discountedPricePlanId = null, licensePackageId = tenantLicense.PackageId;

                ManageTenantDiscountCodes(tenant, tenantLicense, parentId, ref discountCode, ref discountedPricePlanId, licensePackageId);

                if (ModelState.IsValid)
                {
                    // no discount code
                    if (string.IsNullOrEmpty(discountCode))
                    {
                        discountedPricePlanId = tenantLicense.PricePlanId;
                    }
                    else if (!string.IsNullOrEmpty(discountCode) && !string.IsNullOrEmpty(discountedPricePlanId))
                    {
                        tenantLicense.PricePlanId = discountedPricePlanId;
                    }

                    string approvedStatus = Resources.TenantResource.m_WaitingForApproval;

                    if (string.IsNullOrEmpty(tenant.Identifier))
                    {
                        TenantSetting tenantSettings = TenantSettingsProxy.GetTenantSettings(parentId);

                        if (tenantSettings != null && tenantSettings.Setting != null && tenantSettings.Setting.Attributes != null
                            && tenantSettings.Setting.Attributes.ContainsKey(AttributeConstants.AutoApprovalTenantCreation))
                        {
                            if (Convert.ToBoolean(tenantSettings.Setting.Attributes[AttributeConstants.AutoApprovalTenantCreation], CultureInfo.CurrentCulture))
                            {
                                approvedStatus = Resources.TenantResource.m_AutoApproved;
                            }
                        }

                        // Not a Cello OpenId Provider
                        if (ConfigHelper.EnableExternalAuthentication && !formCollection["TenantIdProvider"].Equals(ConfigHelper.CelloOpenIdProvider, StringComparison.OrdinalIgnoreCase))
                        {
                            // set the default password for new user, if the provider is not a Cello IDP
                            tenant.TenantAdminMembershipdetail.Password = ConfigHelper.DefaultUserPassword;
                        }
                        else // a Cello OpenId Provider
                        {
                            tenant.TenantAdminMembershipdetail.IsFirstTimeUser = true;
                        }

                        //tenant.TenantAdminMembershipdetail.Password = ConfigHelper.AutoGenerateUserPassword && !string.IsNullOrWhiteSpace(tenant.TenantAdminMembershipdetail.EmailId) ? string.Empty : ConfigHelper.DefaultUserPassword;
                        tenant.TenantAdminUserdetail.CreatedOn = DateTime.Now;
                        tenant.TenantAdminUserdetail.CreatedBy = UserIdentity.UserId;
                        tenant.TenantAdminMembershipdetail.CreationDate = DateTime.Now;

                        string tenantProvider = formCollection["TenantIdProvider"], authType = null;
                        Dictionary<string, string> providers = null, authenticationTypes = null;
                        if (ConfigHelper.EnableExternalAuthentication)
                        {
                            providers = AuthenticationService.GetAuthenticationProviders();
                            // During tenant creation
                            var authTypeCollection = AuthenticationService.GetAuthenticationTypes(tenantProvider);

                            authenticationTypes = authTypeCollection[tenantProvider];

                            authType = formCollection["UserAuthenticationType"];
                        }

                        tenantCode = tenant.TenantDetails.TenantCode = (!string.IsNullOrEmpty(userConnString))
                            ? TenantProxy.ProvisionTenantWithConnString(tenant, tenantLicense, userConnString)
                            : TenantProxy.ProvisionTenant(tenant, tenantLicense);

                        if (ConfigHelper.EnableExternalAuthentication)
                            await ManageTenantClients(tenant, tenantCode, providers, tenantProvider, authenticationTypes, authType, ldapServerUri);

                        approvedStatus = " " + approvedStatus;

                        InitBillingSettings(tenant, tenantLicense, tenantCode);
                    }
                    else // user usage is moved to metering // if (!tenantLicense.NumberOfUsers.HasValue || ValidateNumberOfUsers(tenantLicense.NumberOfUsers.Value, tenant.TenantDetails.TenantCode))
                    {
                        if (!AccessControlProxy.CheckTenantAccessWithTenantScope(UserIdentity.UserId, PrivilegeConstants.UpdateTenant, parentId, "Tenant", FetchType.Edit, this.TenantId))
                        {
                            throw new UnauthorizedAccessException(Resources.TenantResource.e_EditAccessDenied);
                        }


                        BillingSetting billSetting = null;
                        var generateInvoice = CheckLicenseUpgrade(tenant, tenantLicense, out billSetting);

                        if (ModelState.IsValid)
                        {
                            //Update the tenant information and license details.
                            tenantCode = tenant.TenantDetails.TenantCode = TenantProxy.UpdateTenantInfo(tenant, tenantLicense);
                            approvedStatus = string.Empty;

                            if (generateInvoice && billSetting != null)
                            {
                                // generate bill and debit
                                if (!ConfigHelper.ApplyGlobalBillSettingForPrepaidMode)
                                {
                                    billSetting.TenantId = Guid.Parse(tenant.TenantDetails.TenantCode);
                                    billSetting.ChargeDay = tenantLicense.ValidityStart.Day + (billSetting.ChargeDay - billSetting.StartDay);
                                    billSetting.StartDay = tenantLicense.ValidityStart.Day;
                                    BillingProxy.UpdateBillingSetting(billSetting);
                                }

                                BillingProxy.GenerateInvoice(Guid.Parse(tenant.TenantDetails.TenantCode));
                            }
                        }
                    }

                    if (ModelState.IsValid)
                    {
                        if (isIsvOrReseller)
                        {
                            if (tenantTaxRate != null && tenantTaxRate.Count > 0)
                            {
                                taxRate.TenantId = Guid.Parse(tenantCode);
                                ProcessTax(taxRate);
                            }
                        }

                        TempData["Message"] = Resources.TenantResource.s_TenantSaveSuccess + approvedStatus;
                        return Redirect("TenantList");
                    }
                }
            }
            catch (InvalidEmailException invalidEmailId)
            {
                ExceptionService.HandleException(invalidEmailId, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.ErrorMessage.e_InvalidEmailId);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", argumentException.Message);
            }
            catch (DuplicateTenantCodeException duplicateTenantCode)
            {
                ExceptionService.HandleException(duplicateTenantCode, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCodeExists);
            }
            catch (DuplicateTenantNameException duplicateTenantName)
            {
                ExceptionService.HandleException(duplicateTenantName, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantNameExists);
            }
            catch (DuplicateURLException duplicateURL)
            {
                ExceptionService.HandleException(duplicateURL, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantURLExists);
            }
            catch (DuplicateWebsiteException duplicateWebsite)
            {
                ExceptionService.HandleException(duplicateWebsite, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantWebsiteExists);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCreate);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", ex.Message);
            }
            catch (DuplicateEmailIDException duplictaeEmailIdexception)
            {
                ExceptionService.HandleException(duplictaeEmailIdexception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_DuplicateTenantAdminEmailId);
            }
            catch (InvalidUserNameException invalidUserNameException)
            {
                ExceptionService.HandleException(invalidUserNameException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_UserNameFormate);
            }
            catch (InvalidEmailIDException invalidEmailIdException)
            {
                ExceptionService.HandleException(invalidEmailIdException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_UserNameFormate);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Access);
                ViewBag.AccessDenied = true;
            }
            catch (UserConnectionStringException userConnectionStringException)
            {
                ExceptionService.HandleException(userConnectionStringException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", userConnectionStringException.Message);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                string errorMessage = dataException.InnerException != null ? dataException.InnerException.Message : dataException.Message;
                // do not change to resource, unique extn field error message comes from here
                ModelState.AddModelError("TenantErrorMessage", errorMessage);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", settingsTemplateException.Message);
            }
            catch (BusinessRuleException bex)
            {
                ExceptionService.HandleException(bex, DefaultPolicy);
                if (bex.Data != null)
                {
                    int count = 1;
                    foreach (DictionaryEntry er in bex.Data)
                    {
                        foreach (string error in (IEnumerable<string>)er.Value)
                        {
                            ModelState.AddModelError("Error" + (count++), error);
                        }
                    }
                }
            }
            catch (LicenseLimitException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_UsageLimitExceeded);
            }

            if (tenant.Address == null)
            {
                tenant.Address = new Address();
            }

            if (tenant.ContactDetail == null)
            {
                tenant.ContactDetail = new ContactDetails();
            }

            //Get the list of country and license package.
            LoadCountry((tenant != null && tenant.Address != null && !string.IsNullOrEmpty(tenant.Address.CountryId)) ? tenant.Address.CountryId : null);
            LoadLicensepackages(string.IsNullOrEmpty(tenantCode) ? parentId : tenantCode, tenantLicense.PackageId, string.IsNullOrEmpty(tenantCode));
            LoadTenantLicenseHistory(tenantCode);
            LoadTenantTypes(tenant != null ? tenant.Types : null); //get list of tenant types
            LoadCompanySize(tenant != null ? tenant.TenantDetails.CompanySize : null);
            LoadDataPartitionDetails(tenant == null ? null : tenant.TenantDetails.DataPartitionId, isIsvOrReseller, parentTenant != null ? parentTenant.DataPartitionId : null);
            LoadPricePlanList(tenantLicense.PackageId, tenantLicense.PricePlanId);
            ViewData["TaxRate"] = taxRate;
            ViewData["TenantLicense"] = tenantLicense;
            ViewData["UserConnectionString"] = userConnString;
            ViewData["TenantDetails"] = tenant;

            return View();
        }

        /// <summary>
        /// This method is used to initialize billing settings.
        /// </summary>
        /// <param name="tenant">tenant.</param>
        /// <param name="tenantLicense">tenant license.</param>
        /// <param name="tenantCode">tenant code.</param>
        private static void InitBillingSettings(Tenant tenant, TenantLicense tenantLicense, string tenantCode)
        {
            if (!ConfigHelper.ApplyGlobalBillSettingForPrepaidMode && !string.IsNullOrEmpty(tenantCode) && !string.IsNullOrEmpty(tenantLicense.PricePlanId))
            {
                var gTenantId = Guid.Parse(tenantCode);
                var gParentTenantId = Guid.Parse(tenant.TenantDetails.ParentTenantId);
                var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(tenantLicense.PricePlanId), gParentTenantId);
                var frequency = pricePlan.BillFrequency;
                var globalBillSetting = BillingProxy.GetBillingSetting(frequency, gParentTenantId);

                if (globalBillSetting.Mode == BillCycleMode.Prepaid)
                {
                    var startDay = tenantLicense.TrialEndDate.HasValue ? tenantLicense.TrialEndDate.Value.Day : tenantLicense.ValidityStart.Day;

                    var billSetting = new BillingSetting
                    {
                        Id = Guid.Empty,
                        TenantId = gTenantId,
                        Mode = globalBillSetting.Mode,
                        BillFrequency = frequency,
                        ChargeDay = startDay + (globalBillSetting.ChargeDay - globalBillSetting.StartDay),
                        StartDay = startDay,
                    };
                    BillingProxy.AddBillingSetting(billSetting);
                }
            }
        }

        /// <summary>
        /// This method is used for address validation.
        /// </summary>
        /// <param name="tenant">tenant.</param>
        private void AddressValidation(Tenant tenant)
        {
            if ((!string.IsNullOrEmpty(tenant.Address.Address1) || !string.IsNullOrEmpty(tenant.Address.City) || !string.IsNullOrEmpty(tenant.Address.State)
                || !string.IsNullOrEmpty(tenant.Address.CountryId) || !string.IsNullOrEmpty(tenant.Address.PostalCode)))
            {

                ValidateRegAddress(tenant.Address);
            }
            else
            {
                if (tenant.Address.ExtendedRow != null && tenant.Address.ExtendedRow.ExtendedEntityColumnValues != null && tenant.Address.ExtendedRow.ExtendedEntityColumnValues.Count > 0)
                {
                    foreach (var item in tenant.Address.ExtendedRow.ExtendedEntityColumnValues)
                    {
                        if (!string.IsNullOrEmpty(item.Value.Value))
                        {
                            ValidateRegAddress(tenant.Address);
                            break;
                        }
                    }
                }
                else
                {
                    tenant.Address = null;
                }
            }
        }

        /// <summary>
        /// This method is used for contact validation.
        /// </summary>
        /// <param name="tenant">tenant.</param>
        private void ContactValidation(Tenant tenant)
        {
            if (!(string.IsNullOrEmpty(tenant.ContactDetail.FirstName)) || !string.IsNullOrEmpty(tenant.ContactDetail.LastName) || !string.IsNullOrEmpty(tenant.ContactDetail.Phone)
                || !string.IsNullOrEmpty(tenant.ContactDetail.Email) || !string.IsNullOrEmpty(tenant.ContactDetail.Fax))
            {
                ValidateContactDetails(tenant.ContactDetail);
            }
            else
            {
                if (tenant.ContactDetail.ExtendedRow != null && tenant.ContactDetail.ExtendedRow.ExtendedEntityColumnValues != null && tenant.ContactDetail.ExtendedRow.ExtendedEntityColumnValues.Count > 0)
                {
                    foreach (var item in tenant.ContactDetail.ExtendedRow.ExtendedEntityColumnValues)
                    {
                        if (!string.IsNullOrEmpty(item.Value.Value))
                        {
                            ValidateContactDetails(tenant.ContactDetail);
                            break;
                        }
                    }
                }
                else
                {
                    tenant.ContactDetail = null;
                }
            }
        }

        /// <summary>
        /// This method is used to manage the tenant discount codes.
        /// </summary>
        /// <param name="tenant">tenan.t</param>
        /// <param name="tenantLicense">tenant license.</param>
        /// <param name="parentId">parent identifier.</param>
        /// <param name="discountCode">discount code.</param>
        /// <param name="discountedPricePlanId">discounted price plan identifier.</param>
        /// <param name="licensePackageId">license package identifier.</param>
        private void ManageTenantDiscountCodes(Tenant tenant, TenantLicense tenantLicense, string parentId, ref string discountCode, ref string discountedPricePlanId, string licensePackageId)
        {
            try
            {
                if (tenant.ExtendedRow != null && tenant.ExtendedRow.ExtendedEntityColumnValues != null
                    && tenant.ExtendedRow.ExtendedEntityColumnValues.Count > 0
                    && tenant.ExtendedRow.ExtendedEntityColumnValues.ContainsKey(Constants.DiscountCodeExtnFieldName)
                    && !string.IsNullOrEmpty(tenant.ExtendedRow.ExtendedEntityColumnValues[Constants.DiscountCodeExtnFieldName].Value))
                {
                    discountCode = tenant.ExtendedRow.ExtendedEntityColumnValues[Constants.DiscountCodeExtnFieldName].Value;
                    discountedPricePlanId = TenantExtensions.GetDiscountedPricePlanId(discountCode, licensePackageId, parentId, tenantLicense.PricePlanId);
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("DiscountCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_InvalidDiscountCode);
            }

            if (!string.IsNullOrEmpty(discountCode) && !string.IsNullOrEmpty(licensePackageId) && string.IsNullOrEmpty(discountedPricePlanId))
            {
                ModelState.AddModelError("DiscountCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_InvalidDiscountCode);
            }
        }

        /// <summary>
        /// This method is used to manage the tenant clients.
        /// </summary>
        /// <param name="tenant">tenant.</param>
        /// <param name="tenantCode">tenant code.</param>
        /// <param name="providers">providers.</param>
        /// <param name="tenantProvider">tenant providers.</param>
        /// <param name="authenticationTypes">list of authentication type.</param>
        /// <param name="authType">auth type.</param>
        /// <param name="ldapUri">ldap uri.</param>
        /// <returns></returns>
        private static async Task ManageTenantClients(Tenant tenant, string tenantCode, Dictionary<string, string> providers, string tenantProvider, Dictionary<string, string> authenticationTypes, string authType, string ldapUri)
        {
            await new TenantPostProcessor().DoPostProcessorInsert(tenantCode, tenant.TenantDetails, new[] { tenant });

            // If tenant has no setting, try to get the value from the form collection, else try to use cellopen id provider
            if (providers.ContainsKey(tenantProvider) && !string.IsNullOrEmpty(tenantProvider))
            {
                var tenantSettings = Helpers.ApplyTenantAuthSettings(tenantProvider, authType, ldapUri);
                if (tenantSettings != null && tenantSettings.Count > 0)
                    TenantSettingsProxy.UpdateTenantSettings(new TenantSetting
                    {
                        TenantID = tenantCode,
                        Setting = new Settings
                        {
                            Attributes = tenantSettings
                        }
                    });
            }

            if (authenticationTypes.ContainsKey(authType))
            {
                UserSettingsProxy.UpdateUserSettings(new UserSettings
                {
                    AddedBy = UserIdentity.UserId,
                    Setting = new Settings
                    {
                        Attributes = new Dictionary<string, string>
                                    {
                                        {
                                            SettingAttributeConstants.LoginProvider,authType
                                        }
                                    }
                    },
                    UserId = tenant.TenantAdminUserdetail.Identifier
                });
            }
        }

        /// <summary>
        /// This method is used to check the license upgrade.
        /// </summary>
        /// <param name="tenant">tenant.</param>
        /// <param name="tenantLicense">tenant license.</param>
        /// <param name="billSetting">bill setting.</param>
        /// <returns></returns>
        private bool CheckLicenseUpgrade(Tenant tenant, TenantLicense tenantLicense, out BillingSetting billSetting)
        {
            var gtenantId = Guid.Parse(tenant.TenantDetails.TenantCode);
            string pricePlanId = tenantLicense.PricePlanId;
            string licensePackageId = tenantLicense.PackageId;
            billSetting = null;

            if (string.IsNullOrEmpty(pricePlanId))
                return false;

            var currentLicense = LicenseProxy.GetTenantLicense(tenant.TenantDetails.TenantCode);

            if (currentLicense == null)
            {
                currentLicense = LicenseProxy.GetInActiveTenantLicense(tenant.TenantDetails.TenantCode);
            }

            var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(pricePlanId), Guid.Parse(tenant.TenantDetails.ParentTenantId));
            billSetting = BillingProxy.GetBillingSetting(pricePlan.BillFrequency, gtenantId);

            // package or price plan changed generate bill
            if (!licensePackageId.Equals(currentLicense.PackageId, StringComparison.OrdinalIgnoreCase)
                || currentLicense.PricePlanId != pricePlanId
                || currentLicense.ValidityStart.Date != tenantLicense.ValidityStart.Date)
            {
                var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(licensePackageId);

                bool requirePaymentAccount = tenant.TenantDetails.EnableAutoDebit;

                if (packageDetails.ExpiryDays > 0 && currentLicense.TrialEndDate.HasValue && currentLicense.TrialEndDate.Value > DateTime.Now)
                {
                    // current subscription has trial period
                    var history = LicenseProxy.GetTenantLicenseHistory(tenant.TenantDetails.TenantCode);
                    var firstLicense = history.OrderBy(x => x.ValidityStart).First();

                    var trialEndDate = firstLicense.ValidityStart.AddDays(packageDetails.ExpiryDays);

                    if (trialEndDate > DateTime.Now)
                    {
                        requirePaymentAccount = false; // still has some trial period left
                    }
                }

                if (requirePaymentAccount)
                {
                    // if trial period expires or upgarde to paid plan
                    // if pre paid mode and payment not given then ask to provide payment details before upgrade

                    if (billSetting.Mode == BillCycleMode.Prepaid)
                    {
                        var paymentAccount = PaymentProxy.GetAccountSetting(gtenantId);

                        if (paymentAccount == null || string.IsNullOrEmpty(paymentAccount.StoreKey) || paymentAccount.StoreKey.Split('/').Length != 2 || !paymentAccount.Status)
                        {
                            ModelState.AddModelError("Error", Resources.TenantResource.e_PaymentDetails);
                            return false;
                        }
                        else
                        {
                            return tenantLicense.ValidityStart.Date == DateTime.Today;
                        }
                    }
                }
            }

            if (billSetting.Mode == BillCycleMode.Prepaid && !ConfigHelper.ApplyGlobalBillSettingForPrepaidMode && tenantLicense.TrialEndDate.HasValue && currentLicense.TrialEndDate.HasValue && tenantLicense.TrialEndDate != currentLicense.TrialEndDate)
            {
                billSetting.TenantId = Guid.Parse(tenant.TenantDetails.TenantCode);
                billSetting.ChargeDay = tenantLicense.TrialEndDate.Value.Day + (billSetting.ChargeDay - billSetting.StartDay);
                billSetting.StartDay = tenantLicense.TrialEndDate.Value.Day;
                BillingProxy.UpdateBillingSetting(billSetting);
            }

            return false;
        }

        /// <summary>
        /// This method is used to loads the company size drop down list.
        /// </summary>
        /// <param name="companySize">company size.</param>
        private void LoadCompanySize(string companySize)
        {
            var lstPickupListValues = PickupListProxy.GetAllPickupListValues(PickupListConstants.CompanySizePickupList, ProductAdminConstants.ProductAdminTenantId);
            Dictionary<string, string> companySizeDetails = new Dictionary<string, string>();
            companySizeDetails.Add("", "-Select-");
            if (lstPickupListValues != null && lstPickupListValues.Count > 0)
            {
                lstPickupListValues.ForEach(x => companySizeDetails.Add(x.ItemId, x.ItemName));
            }

            ViewData["Tenant.CompanySize"] = new SelectList(companySizeDetails, "Key", "Value", companySize);
        }

        /// <summary>
        /// This method is used to loads the tenant types into view data as SelectListItem(s).
        /// </summary>
        private void LoadTenantTypes(List<TenantType> lstTypes)
        {
            //get all the tenant types available
            var allTenantTypes = TenantTypesProxy.GetTenantTypes();
            var currTenantType = TenantTypesProxy.GetTenantTypes(this.TenantId);
            var availableTypes = new List<TenantType>();

            if (currTenantType != null)
            {
                if (currTenantType.Any(x => x.ID == TenantTypeConstants.ISV))
                {
                    availableTypes.Add(new TenantType { ID = TenantTypeConstants.SMB, Name = "SMB" });
                    availableTypes.Add(new TenantType { ID = TenantTypeConstants.Enterprise, Name = "Enterprise" });
                    availableTypes.Add(new TenantType { ID = TenantTypeConstants.Reseller, Name = "Reseller" });
                }
                else if (currTenantType.Any(x => x.ID == TenantTypeConstants.Reseller))
                {
                    availableTypes.Add(new TenantType { ID = TenantTypeConstants.SMB, Name = "SMB" });
                    availableTypes.Add(new TenantType { ID = TenantTypeConstants.Enterprise, Name = "Enterprise" });
                }
                else
                {
                    availableTypes.Add(new TenantType { ID = TenantTypeConstants.OrgUnit, Name = "OrgUnit" });
                }
            }

            //get all the tenant type id only
            string[] tenantTypesIDs = new string[1];

            if (lstTypes != null)
            {
                tenantTypesIDs = (from lt in lstTypes select lt.ID).ToArray();
            }

            if (availableTypes.Count > 0)
            {
                //create select list items
                var items = from tt in availableTypes.OrderBy(x => x.Name)
                            select new SelectListItem
                            {
                                Text = tt.Name,
                                Value = tt.ID,
                                Selected = tenantTypesIDs.Contains(tt.ID)
                            };
                //store it in the view data
                ViewData["Tenant.Types"] = items;
            }
            else
            {
                //no tenant types found
                ViewData["Tenant.Types"] = new SelectList(new string[] { "Not Found!" });
            }
        }

        #endregion Manage Tenant

        #region Price plans

        /// <summary>
        /// This method is used to loads the price plan drop down list.
        /// </summary>
        /// <param name="packageId">package identifier.</param>
        /// <param name="pricePlanId">price plan identifier.</param>
        private void LoadPricePlanList(string packageId, string pricePlanId)
        {
            if (!UserIdentity.HasPrivilege(PrivilegeConstants.ViewPricePlan))
            {
                return;
            }

            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "--Choose a plan--", Value = "" });
            ViewData["PricePlanList"] = list;

            if (string.IsNullOrEmpty(packageId))
            {
                return;
            }

            try
            {
                var pricePlans = CelloSaaS.Billing.ServiceProxies.BillingPlanProxy.GetPricePlansByPackageId(Guid.Parse(packageId), Guid.Parse(this.TenantId));

                if (pricePlans != null)
                {
                    foreach (var plan in pricePlans.Values)
                    {
                        list.Add(new SelectListItem
                        {
                            Text = plan.Name,
                            Value = plan.Id.ToString(),
                            Selected = plan.Id.ToString() == pricePlanId
                        });
                    }
                }

                ViewData["PricePlanList"] = list;
            }
            catch (CelloSaaS.Billing.Model.BillingException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
        }

        /// <summary>
        /// This method is Returns the price plans in json format.
        /// </summary>
        /// <param name="packageId">package identifier.</param>
        /// <returns></returns>
        public JsonResult GetPricePlans(string packageId)
        {
            if (string.IsNullOrEmpty(packageId))
            {
                return Json(new { Error = Resources.TenantResource.e_InvalidRequest });
            }

            if (!UserIdentity.HasPrivilege(PrivilegeConstants.ViewPricePlan))
            {
                return Json(null);
            }

            try
            {
                var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(packageId);

                if (packageDetails == null)
                {
                    return Json(null);
                }

                var pricePlans = CelloSaaS.Billing.ServiceProxies.BillingPlanProxy.GetPricePlansByPackageId(Guid.Parse(packageId), Guid.Parse(packageDetails.TenantId));

                var list = new List<SelectListItem>();
                list.Add(new SelectListItem { Text = "--Choose a plan--", Value = "" });

                if (pricePlans != null)
                {
                    foreach (var plan in pricePlans.Values)
                    {
                        list.Add(new SelectListItem
                        {
                            Text = plan.Name,
                            Value = plan.Id.ToString()
                        });
                    }
                }

                return Json(list);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
            catch (CelloSaaS.Billing.Model.BillingException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                return Json(new { Error = ex.Message });
            }
        }

        #endregion Price Plans

        #region Manager Tenant Approval

        /// <summary>
        /// This method is used to approves the tenant based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult ActivateSelfRegisteredTenant(string tenantId)
        {
            if (string.IsNullOrWhiteSpace(tenantId))
            {
                TempData["Error"] = Resources.TenantResource.e_InvalidRequest;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(tenantId, this.TenantId);

                    TenantLicense tenantLicense = LicenseProxy.GetTenantLicense(tenantId);

                    if (tenantDetails == null || tenantLicense == null)
                    {
                        throw new TenantActivationException(Resources.TenantResource.e_InvalidAccount);
                    }

                    if (tenantDetails.TenantDetails.ApprovalStatus == TenantApprovalStatus.APPROVED)
                    {
                        throw new TenantActivationException(Resources.TenantResource.e_AccountActivated);
                    }

                    TenantProxy.UpdateApprovalStatus(tenantId, TenantApprovalStatus.APPROVED);

                    var package = LicenseProxy.GetPackageDetailsByPackageId(tenantLicense.PackageId);

                    tenantLicense.ValidityStart = DateTime.Now;

                    if (package.ExpiryDays > 0)
                    {
                        tenantLicense.TrialEndDate = tenantLicense.ValidityStart.AddDays(package.ExpiryDays);
                    }

                    if (package.IsEvaluation)
                    {
                        tenantLicense.ValidityEnd = tenantLicense.TrialEndDate;
                    }

                    LicenseProxy.UpdateTenantLicense(tenantLicense);

                    TempData["Message"] = string.Format(CultureInfo.InvariantCulture, Resources.TenantResource.s_Activate, tenantDetails.TenantDetails.TenantName);
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    TempData["Error"] = ex.Message;
                }
            }

            return RedirectToAction("TenantList");
        }

        /// <summary>
        /// This method is used to manages the tenant approval.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageTenantApproval()
        {
            GetAllSkipLevelChildTenants();
            return View();
        }

        /// <summary>
        /// This method is used to approves the tenant based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult ApproveTenant(string tenantId)
        {
            if (string.IsNullOrWhiteSpace(tenantId))
            {
                ModelState.AddModelError("Error", Resources.TenantResource.e_InvalidRequest);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    TenantProxy.UpdateApprovalStatus(tenantId, TenantApprovalStatus.APPROVED);
                    ModelState.AddModelError("Success", "Tenant approved successfully!");
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    ModelState.AddModelError("Error", ex.Message);
                }


                GetAllSkipLevelChildTenants();
            }

            return PartialView("PartialManageTenantApproval");
        }

        /// <summary>
        /// This method is used to rejects the tenant based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult RejectTenant(string tenantId, bool redirect = false)
        {
            if (string.IsNullOrWhiteSpace(tenantId))
            {
                ModelState.AddModelError("Error", Resources.TenantResource.e_InvalidRequest);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    TenantProxy.UpdateApprovalStatus(tenantId, TenantApprovalStatus.REJECTED);
                    if (redirect)
                        TempData["Success"] = "Tenant rejected successfully!";
                    else
                        ModelState.AddModelError("Success", "Tenant rejected successfully!");
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    if (redirect)
                        TempData["Error"] = ex.Message;
                    else
                        ModelState.AddModelError("Error", ex.Message);
                }

                if (!redirect)
                    GetAllSkipLevelChildTenants();
            }

            if (redirect)
            {
                return RedirectToAction("TenantList");
            }

            return PartialView("PartialManageTenantApproval");
        }

        /// <summary>
        /// This method is used to gets the immediate child tenant list.
        /// </summary>
        private void GetAllSkipLevelChildTenants()
        {
            try
            {
                Dictionary<string, TenantDetails> tenantList = TenantRelationProxy.GetAllSkipLevelChildTenants(UserIdentity.TenantID);
                if (tenantList != null && tenantList.Count > 0)
                {
                    ViewData["TenantList"] = tenantList.Values.ToList();
                }
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", ex.Message);
            }
        }

        #endregion

        #region TenantList

        /// <summary>
        /// This method is used to view the tenant dashboard.
        /// </summary>
        /// <param name="year">year</param>
        /// <returns></returns>
        public PartialViewResult TenantDashboard(int? year)
        {
            if (!year.HasValue)
            {
                year = DateTime.Now.Year;
            }

            ViewBag.year = year;

            try
            {
                var currentTenant = TenantProxy.GetTenantInfo(this.TenantId);
                ViewBag.currentTenant = currentTenant;

                var unApprovedTenants = TenantProxy.SearchTenantDetails(new TenantDetailsSearchCondition
                {
                    ApprovalStatus = new string[] { TenantApprovalStatus.WAITINGFORAPPROVAL, TenantApprovalStatus.REJECTED },
                    ParentTenantId = Guid.Parse(this.TenantId)
                });

                ViewData["WaitingTenants"] = unApprovedTenants != null ? unApprovedTenants.Items : null;

                //Get the list od tenant and their details.
                var immediateChildTenant = TenantRelationProxy.GetAllImmediateChildTenants(this.TenantId);

                var allChilds = TenantRelationProxy.GetAllChildTenants(this.TenantId);
                ViewBag.AllChildTenants = allChilds;

                if (allChilds != null && allChilds.Count > 0)
                {
                    var allChildids = allChilds.Keys.ToArray();
                    ViewBag.TotalUsers = UserDetailsProxy.GetUserCountByTenantIds(allChildids);
                    ViewBag.TotalOnlineUsers = UserDetailsProxy.GetOnlineUsersCountForTenants(allChildids);
                }

                if (immediateChildTenant != null && immediateChildTenant.Count > 0)
                {
                    var activeTenants = immediateChildTenant.Values.Where(t => t.ApprovalStatus.Equals(TenantApprovalStatus.APPROVED, StringComparison.OrdinalIgnoreCase));

                    var gchildTenantIds = activeTenants.Select(x => Guid.Parse(x.TenantCode)).ToArray();
                    var lstTenants = new ConcurrentBag<TenantViewModel>();

                    var tenantLicenses = LicenseProxy.SearchTenantLicenses(new TenantLicenseSearchCondition
                    {
                        TenantIds = gchildTenantIds
                    });

                    var allPaymentAccounts = PaymentProxy.GetChildAccountSettings(gchildTenantIds) ?? new Dictionary<Guid, PaymentAccountSetting>();

                    foreach (var item in activeTenants)
                    {
                        var tenantId = Guid.Parse(item.TenantCode);
                        var license = tenantLicenses[tenantId];

                        lstTenants.Add(new TenantViewModel
                        {
                            TenantDetails = item,
                            License = license,
                            PaymentAccount = allPaymentAccounts.ContainsKey(tenantId) ? allPaymentAccounts[tenantId] : null
                        });
                    }

                    ViewData["TenantDetails"] = lstTenants;
                }
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_Access);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }

            return PartialView();
        }

        /// <summary>
        /// This method is used to tenant list view.
        /// </summary>
        /// <param name="page">page.</param>
        /// <param name="pageSize">page size.</param>
        /// <param name="searchCondition">search condition.</param>
        /// <returns></returns>
        public ActionResult Index(TenantViewSearch searchCondition, int page = 1, int pageSize = 20)
        {
            if (TempData["Message"] != null && !string.IsNullOrEmpty(TempData["Message"].ToString()))
            {
                ModelState.AddModelError("Success", TempData["Message"].ToString());
            }

            if (TempData["Error"] != null && !string.IsNullOrEmpty(TempData["Error"].ToString()))
            {
                ModelState.AddModelError("Error", TempData["Error"].ToString());
            }

            if (searchCondition == null) searchCondition = new TenantViewSearch();
            searchCondition.PageNo = page;
            searchCondition.PageSize = pageSize;

            ViewBag.searchCondition = searchCondition;
            ViewBag.AllPackages = LicenseProxy.GetAllLicensePackage(this.TenantId);

            LoadTenantDetails(searchCondition);

            if (this.Request.IsAjaxRequest())
            {
                return PartialView("TenantListView");
            }

            return View();
        }

        /// <summary>
        /// This method is used to load tenant, license, plan, usage details.
        /// </summary>
        /// <param name="searchCondition">search condition.</param>
        private void LoadTenantDetails(TenantViewSearch searchCondition)
        {
            try
            {
                var currentTenant = TenantProxy.GetTenantInfo(this.TenantId);
                ViewBag.currentTenant = currentTenant;
                bool isIsvOrReseller = currentTenant.Types != null && currentTenant.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);
                ViewBag.IsIsvOrReseller = isIsvOrReseller;

                var unApprovedTenants = TenantProxy.SearchTenantDetails(new TenantDetailsSearchCondition
                {
                    ApprovalStatus = new string[] { TenantApprovalStatus.WAITINGFORAPPROVAL, TenantApprovalStatus.REJECTED }
                });

                ViewData["WaitingTenants"] = unApprovedTenants != null ? unApprovedTenants.Items : null;

                Dictionary<Guid, double> tenantOverdues = null;
                Guid[] searchTenantIds = null;

                if (searchCondition.HasOverdue.HasValue && isIsvOrReseller)
                {
                    tenantOverdues = BillingProxy.GetTenantOverdues(null); // get only tenants who has overdues

                    if (tenantOverdues != null && tenantOverdues.Count > 0)
                    {
                        searchTenantIds = tenantOverdues.Where(x => searchCondition.HasOverdue.Value ? x.Value > 0 : x.Value == 0).Select(y => y.Key).ToArray();
                    }

                    if (searchCondition.HasOverdue.Value && (searchTenantIds == null || searchTenantIds.Length == 0))
                    {
                        return; // no tenants have overdues
                    }
                }

                Dictionary<Guid, TenantLicense> tenantLicenses = null;
                TenantLicenseStatus? licenseStatus = null;
                Dictionary<Guid, int> lstChildTenantsCount = null;

                if (searchCondition.OnlyActive.HasValue) licenseStatus = TenantLicenseStatus.Active;
                if (searchCondition.OnlyDeactivated.HasValue) licenseStatus = TenantLicenseStatus.Deactivated;
                if (searchCondition.OnlyTrial.HasValue) licenseStatus = TenantLicenseStatus.Trial;

                if ((searchCondition.PackageId.HasValue && searchCondition.PackageId != Guid.Empty)
                    || (licenseStatus.HasValue))
                {
                    tenantLicenses = LicenseProxy.SearchTenantLicenses(new TenantLicenseSearchCondition
                    {
                        PackageIds = (searchCondition.PackageId.HasValue && searchCondition.PackageId != Guid.Empty) ? new Guid[] { searchCondition.PackageId.Value } : null,
                        LicenseStatus = licenseStatus
                    });

                    if (tenantLicenses != null && tenantLicenses.Count > 0)
                    {
                        searchTenantIds = tenantLicenses.Keys.ToArray();
                    }
                }

                if (searchCondition.HasChildTenants.HasValue)
                {
                    lstChildTenantsCount = TenantRelationProxy.GetAllChildTenantsCount(searchTenantIds);

                    if (lstChildTenantsCount != null && lstChildTenantsCount.Count > 0)
                    {
                        searchTenantIds = lstChildTenantsCount.Where(x => searchCondition.HasChildTenants.Value ? x.Value > 0 : x.Value == 0).Select(x => x.Key).ToArray();
                    }

                    if (searchCondition.HasChildTenants.Value && (searchTenantIds == null || searchTenantIds.Length == 0))
                    {
                        return; // no 1st level child tenants found
                    }
                }

                var searchResult = TenantProxy.SearchTenantDetails(new TenantDetailsSearchCondition
                {
                    TenantName = searchCondition.SearchText,
                    IsSelfRegistered = searchCondition.OnlySelfRegistered,
                    CreatedYear = searchCondition.IsNew.HasValue && searchCondition.IsNew.Value ? DateTime.Today.Year : 0,
                    EnableAutoDebit = searchCondition.HasAutoDebit,
                    ApprovalStatus = new string[] { TenantApprovalStatus.APPROVED },
                    TenantIds = searchTenantIds,
                    WithOutPaymentAccount = searchCondition.WithoutPaymentAccount,
                    PageSize = searchCondition.PageSize,
                    PageNo = searchCondition.PageNo,
                    TenantTypeId = searchCondition.TenantType
                });

                if (searchResult == null || searchResult.TotalCount == 0) return;

                var allTenantInfos = searchResult.Items;
                ViewData["TotalCount"] = searchResult.TotalCount;

                string[] childTenantIds = allTenantInfos.Select(x => x.Key).ToArray();
                var gchildTenantIds = childTenantIds.Select(x => Guid.Parse(x)).ToArray();

                if (tenantLicenses == null)
                {
                    tenantLicenses = LicenseProxy.SearchTenantLicenses(new TenantLicenseSearchCondition
                    {
                        TenantIds = gchildTenantIds
                    });
                }

                var lstTenants = new List<TenantViewModel>();
                Dictionary<Guid, PricePlan> pricePlans = null;
                Dictionary<Guid, BillStatistics> ytdBillStats = null;
                Dictionary<Guid, PaymentAccountSetting> allPaymentAccounts = null;

                if (tenantLicenses != null && tenantLicenses.Any(x => !string.IsNullOrEmpty(x.Value.PricePlanId)))
                {
                    var planIds = tenantLicenses.Where(x => !string.IsNullOrEmpty(x.Value.PricePlanId))
                                                .Select(x => Guid.Parse(x.Value.PricePlanId)).ToArray();
                    pricePlans = BillingPlanProxy.GetPricePlans(planIds, Guid.Parse(this.TenantId));
                }

                Dictionary<string, Dictionary<int, int>> userCounts = UserDetailsProxy.GetUserCountByTenantIds(childTenantIds);
                Dictionary<string, int> tenantOnlineUsers = UserDetailsProxy.GetOnlineUsersCountForTenants(childTenantIds);

                var allPackages = LicenseProxy.GetAllPackageDetails();

                var datapartitionList = DataPartitionProxy.GetAllDataPartition();
                var dataPartitionDictionary = datapartitionList != null && datapartitionList.Count > 0 ? datapartitionList.ToDictionary(c => c.Key.ToString(), c => c.Value.DataPartitionName) : null;

                if (lstChildTenantsCount == null)
                {
                    lstChildTenantsCount = TenantRelationProxy.GetAllChildTenantsCount(gchildTenantIds);
                }

                if (isIsvOrReseller)
                {
                    if (tenantOverdues == null)
                    {
                        tenantOverdues = BillingProxy.GetTenantOverdues(gchildTenantIds);
                    }

                    DateTime fromDate = DateTime.ParseExact("01/01/" + DateTime.Now.Year, "MM/d/yyyy", CultureInfo.InvariantCulture);
                    DateTime toDate = DateTime.ParseExact("12/31/" + DateTime.Now.Year, "MM/d/yyyy", CultureInfo.InvariantCulture);

                    allPaymentAccounts = PaymentProxy.GetChildAccountSettings(gchildTenantIds);

                    ytdBillStats = BillingProxy.GetBillStatistics(new BillStatisticsSearchCondition()
                    {
                        TenantIds = gchildTenantIds,
                        FromDate = fromDate,
                        ToDate = toDate
                    });
                }

                foreach (var item in allTenantInfos)
                {
                    var tenantId = item.Key;
                    Guid gTenantId = Guid.Parse(tenantId);

                    if (!tenantLicenses.ContainsKey(gTenantId)) continue;
                    var license = tenantLicenses[gTenantId];

                    var packageDetails = allPackages != null && allPackages.ContainsKey(license.PackageId) ? allPackages[license.PackageId] : null;
                    var paymentAccount = allPaymentAccounts != null && allPaymentAccounts.ContainsKey(gTenantId) ? allPaymentAccounts[gTenantId] : null;
                    var billStats = ytdBillStats != null && ytdBillStats.ContainsKey(gTenantId) ? ytdBillStats[gTenantId] : new BillStatistics();
                    int childTenantCount = lstChildTenantsCount != null && lstChildTenantsCount.ContainsKey(gTenantId) ? lstChildTenantsCount[gTenantId] : 0;
                    int onlineUsers = tenantOnlineUsers != null && tenantOnlineUsers.ContainsKey(tenantId) ? tenantOnlineUsers[tenantId] : 0;
                    var userCount = userCounts != null && userCounts.ContainsKey(tenantId) ? userCounts[tenantId] : null;

                    PricePlan plan = null;

                    if (!string.IsNullOrEmpty(license.PricePlanId) && pricePlans != null)
                    {
                        var id = Guid.Parse(license.PricePlanId);
                        plan = pricePlans.ContainsKey(id) ? pricePlans[id] : null;
                    }

                    string partitionId = item.Value.TenantDetails.DataPartitionId;
                    string partitionName = !string.IsNullOrEmpty(partitionId)
                                            && dataPartitionDictionary != null
                                            && dataPartitionDictionary.ContainsKey(partitionId)
                                            ? dataPartitionDictionary[partitionId] : "NA";

                    string logo = string.Empty;
                    /*
                    var tenantSetting = TenantSettingsProxy.GetTenantSettings(tenantId);
                    if (tenantSetting != null
                        && tenantSetting.Setting != null
                        && tenantSetting.Setting.Attributes != null
                        && tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.Logo))
                    {
                        logo = tenantSetting.Setting.Attributes[AttributeConstants.Logo];
                    }

                    DateTime startDate, endDate;
                    var billFreq = plan != null ? plan.BillFrequency : BillFrequency.Monthly;
                    GetStartAndEndDates(gTenantId, billFreq, out startDate, out endDate);

                    var usageAmounts = MeteringProxy.GetUsageAmount(null, startDate, endDate, tenantId) ?? new Dictionary<string, double>();
                    var childUsageAmounts = MeteringProxy.GetChildTenantsUsageAmount(null, startDate, endDate, tenantId);

                    var lstUsages = GetUsages(packageDetails, usageAmounts);

                    if (searchCondition.HasUsageAlert.HasValue && searchCondition.HasUsageAlert.Value
                         && !lstUsages.Any(x => x.Item5))
                    {
                        continue;
                    }*/

                    lstTenants.Add(new TenantViewModel
                    {
                        Tenant = item.Value,
                        TenantDetails = item.Value.TenantDetails,
                        License = license,
                        Types = item.Value.Types,
                        Plan = plan,
                        PaymentAccount = paymentAccount,
                        UsageAmounts = null, //usageAmounts,
                        ChildUsageAmounts = null, //childUsageAmounts,
                        UsageDetails = null, //lstUsages,
                        PackageDetails = packageDetails,
                        TotalUsers = userCount != null ? (userCount.ContainsKey(1) ? userCount[1] : 0) + (userCount.ContainsKey(0) ? userCount[0] : 0) : 0,
                        OnlineUsers = onlineUsers,
                        TotalChildTenants = childTenantCount,
                        Logo = logo,
                        BillStatistics = billStats,
                        OverallOverdue = tenantOverdues != null && tenantOverdues.ContainsKey(gTenantId) ? tenantOverdues[gTenantId] : 0.0,
                        DataPartitionName = partitionName
                    });
                }

                ViewData["TenantDetails"] = lstTenants;
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_Access);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (BillingException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (PartitionManagementException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
            catch (UserDetailException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantList);
            }
        }

        /// <summary>
        /// This method is used to calculation of usages limit, over usage, alerts.
        /// </summary>
        /// <param name="packageDetails">package details.</param>
        /// <param name="usageAmounts">usage amount.</param>
        /// <returns>collection of usages.</returns>
        private static List<Tuple<string, double, double, double, bool>> GetUsages(PackageDetails packageDetails, Dictionary<string, double> usageAmounts)
        {
            var lstUsages = new List<Tuple<string, double, double, double, bool>>();
            var usageLimits = packageDetails != null && packageDetails.PackageUsageDetails != null ? packageDetails.PackageUsageDetails : new Dictionary<string, PackageUsage>();

            if (usageLimits.Count != usageAmounts.Count)
            {
                foreach (var u in usageLimits)
                {
                    if (!usageAmounts.ContainsKey(u.Value.UsageCode))
                    {
                        usageAmounts.Add(u.Value.UsageCode, 0.0);
                    }
                }
            }

            foreach (var usage in usageLimits.OrderBy(x => x.Value.UsageName))
            {
                double maxLimit = usage.Value.MaximumCapacity ?? 0.0;
                double usageAmt = usageAmounts != null && usageAmounts.ContainsKey(usage.Value.UsageCode) ? usageAmounts[usage.Value.UsageCode] : 0.0;
                double rem = (maxLimit - usageAmt);
                bool crossedThreshold = false;

                if (maxLimit > 0.0 & rem <= (maxLimit * (usage.Value.UsageThreshold / 100.0)))
                {
                    crossedThreshold = true;
                }

                lstUsages.Add(Tuple.Create(usage.Value.UsageName, rem, usageAmt, maxLimit, crossedThreshold));
            }

            return lstUsages;
        }

        /// <summary>
        /// This is method is used to get the tenant list.
        /// </summary>
        /// <returns></returns>
        public ActionResult TenantList()
        {
            return RedirectToAction("Index");
        }

        #endregion TenantList

        #region Activate/Deactivate Tenant

        /// <summary>
        /// This method is used to de-activate the tenant license based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult DeActivateTenant(string tenantId)
        {
            if (string.IsNullOrEmpty(tenantId))
            {
                TempData["Error"] = Resources.TenantResource.e_InvalidTenantId;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(tenantId, this.TenantId);

                    if (tenantDetails == null)
                    {
                        TempData["Error"] = Resources.TenantResource.e_TenantDetailsNotFound;
                    }
                    else
                    {
                        LicenseProxy.DeactivateTenantLicense(tenantId);
                        TempData["Message"] = string.Format(CultureInfo.InvariantCulture, Resources.TenantResource.s_DeActivate, tenantDetails.TenantDetails.TenantName);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    TempData["Error"] = ex.Message;
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// This method is used to activates the tenant license based on the given tenant identifier
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public ActionResult ActivateTenant(string tenantId)
        {
            if (string.IsNullOrEmpty(tenantId))
            {
                TempData["Error"] = Resources.TenantResource.e_InvalidTenantId;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var tenantDetails = TenantProxy.GetTenantDetailsByTenantId(tenantId, this.TenantId);

                    if (tenantDetails == null)
                    {
                        TempData["Error"] = Resources.TenantResource.e_TenantDetailsNotFound;
                    }
                    else
                    {
                        LicenseProxy.ActivateTenantLicense(tenantId);

                        var paymentSetting = PaymentProxy.GetAccountSetting(Guid.Parse(tenantId));

                        if (paymentSetting != null)
                        {
                            var currentLicense = LicenseProxy.GetTenantLicense(tenantId);

                            var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(currentLicense.PricePlanId), Guid.Parse(tenantDetails.TenantDetails.ParentTenantId));

                            var billSetting = BillingProxy.GetBillingSetting(pricePlan.BillFrequency, Guid.Parse(tenantDetails.TenantDetails.ParentTenantId));

                            if (billSetting != null)
                            {
                                // generate bill and debit
                                if (!ConfigHelper.ApplyGlobalBillSettingForPrepaidMode)
                                {
                                    billSetting.TenantId = Guid.Parse(tenantId);
                                    billSetting.ChargeDay = DateTime.Now.Day + (billSetting.ChargeDay - billSetting.StartDay);
                                    billSetting.StartDay = DateTime.Now.Day;
                                    BillingProxy.UpdateBillingSetting(billSetting);
                                }

                                BillingProxy.GenerateInvoice(Guid.Parse(tenantId));
                            }
                        }

                        TempData["Message"] = string.Format(CultureInfo.InvariantCulture, Resources.TenantResource.s_Activate, tenantDetails.TenantDetails.TenantName);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, DefaultPolicy);
                    TempData["Error"] = ex.Message;
                }
            }

            return RedirectToAction("Index");
        }

        #endregion Activate/Deactivate Tenant

        #region TenantSetting

        /// <summary>
        /// This method is used to gets the tenant settings.
        /// </summary>
        /// <returns></returns>
        public ActionResult TenantSettings()
        {
            string themeName = ConfigHelper.DefaultTheme;
            string logoFile = ConfigHelper.DefaultLogo;
            string wcfSecretKey = string.Empty;
            string language = string.Empty;
            string dateFormat = ConfigHelper.ConfiguredDateTimeFormat;
            int expiryDays = 0;
            bool enableProductAnalytics = false;

            try
            {
                var tenantSetting = TenantSettingsProxy.GetTenantSettings(UserIdentity.TenantID);

                if (tenantSetting != null && tenantSetting.Setting != null
                    && tenantSetting.Setting.Attributes != null)
                {
                    if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.DisableProductAnalytics))
                    {
                        bool.TryParse(tenantSetting.Setting.Attributes[AttributeConstants.DisableProductAnalytics], out enableProductAnalytics);
                    }

                    if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.Theme))
                    {
                        themeName = tenantSetting.Setting.Attributes[AttributeConstants.Theme];
                    }

                    if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.Logo))
                    {
                        logoFile = tenantSetting.Setting.Attributes[AttributeConstants.Logo];
                    }

                    if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.UserPasswordExpiration))
                    {
                        Int32.TryParse(tenantSetting.Setting.Attributes[AttributeConstants.UserPasswordExpiration], out expiryDays);
                    }

                    if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.WCFSharedKey))
                    {
                        wcfSecretKey = tenantSetting.Setting.Attributes[AttributeConstants.WCFSharedKey];
                    }

                    if (tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.DateFormat))
                    {
                        dateFormat = tenantSetting.Setting.Attributes[AttributeConstants.DateFormat];
                    }

                    if (tenantSetting.Setting.Attributes.ContainsKey("Language"))
                    {
                        language = tenantSetting.Setting.Attributes["Language"];
                    }
                }
            }
            catch (ViewMetaDataException viewException)
            {
                ExceptionService.HandleException(viewException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantSettingsUpdate);
            }
            catch (ArgumentException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantSettingsUpdate);
            }
            catch (TenantPasswordExpirySettingException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantSettingsUpdate);
            }
            catch (TenantSettingsException settingsException)
            {
                ExceptionService.HandleException(settingsException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantSettingsUpdate);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_Access);
            }

            ViewData["Themes"] = GetThemeNameList(themeName);
            ViewData["Languages"] = GetLanguages(language);
            ViewData["ImageURL"] = this.Url.Content(@"~/" + (ConfigHelper.LogoImageFolderPath ?? DefaultLogoImagePath) + @"/" + logoFile);
            ViewData["expiryDays"] = expiryDays;
            ViewData["HiddenWCFSecretKey"] = ViewData["WCFSecretKey"] = wcfSecretKey;
            ViewData["EnableProductAnalytics"] = enableProductAnalytics;
            ViewData["dateFormat"] = dateFormat;

            return View();
        }

        /// <summary>
        /// This method is used for tenant settings.
        /// </summary>
        /// <param name="viewMetaData">view meta data.</param>
        /// <param name="savedImagePath">saved image data.</param>
        /// <param name="ExpiryDays">expiry days.</param>
        /// <param name="WCFSecretKey">WCF secret key.</param>
        /// <param name="HiddenWCFSecretKey">hidden WCF shared key.</param>
        /// <param name="EnableProductAnalytics">Enable product analytics.</param>
        /// <param name="dateFormat">date format.</param>
        /// <param name="language">language.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TenantSettings([Bind(Prefix = "ViewMetaData")]CelloSaaS.Model.ViewManagement.ViewMetaData viewMetaData,
            string savedImagePath,
            string ExpiryDays,
            string WCFSecretKey,
            string HiddenWCFSecretKey,
            bool EnableProductAnalytics,
            string dateFormat,
            string language)
        {
            ViewData["ImageURL"] = savedImagePath;
            ViewData["ExpiryDays"] = ExpiryDays;
            ViewData["WCFSecretKey"] = WCFSecretKey;
            ViewData["HiddenWCFSecretKey"] = HiddenWCFSecretKey;
            ViewData["EnableProductAnalytics"] = EnableProductAnalytics;
            ViewData["dateFormat"] = dateFormat;

            int expiryDays = 0;
            Dictionary<string, string> attributes = new Dictionary<string, string>();

            try
            {
                // if null set default theme
                viewMetaData.Theme = (string.IsNullOrEmpty(viewMetaData.Theme) || viewMetaData.Theme.Equals("-1")) ? ConfigHelper.DefaultTheme : viewMetaData.Theme;

                ViewData["Themes"] = GetThemeNameList(viewMetaData.Theme);

                ViewData["Languages"] = GetLanguages(language);

                // add theme setting
                attributes.Add(AttributeConstants.Theme, viewMetaData.Theme);

                // add productanalytics settings
                attributes.Add(AttributeConstants.DisableProductAnalytics, EnableProductAnalytics.ToString());

                // add date format
                attributes.Add(AttributeConstants.DateFormat, dateFormat);

                // add language
                if (!string.IsNullOrEmpty(language))
                {
                    attributes.Add("Language", language);
                }

                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase hpf = Request.Files[0] as HttpPostedFileBase;

                    if (hpf.ContentLength > 0)
                    {
                        string path = this.Server.MapPath(@"..\" + (ConfigHelper.LogoImageFolderPath ?? DefaultLogoImagePath) + @"\");
                        string logoImageName = UserIdentity.TenantID + "_logo.jpg";

                        FileStorageProxy.StoreFile(path, logoImageName, 0);

                        // add logo image setting
                        attributes.Add(AttributeConstants.Logo, logoImageName);
                    }
                }

                if (!string.IsNullOrEmpty(ExpiryDays) && int.TryParse(ExpiryDays, out expiryDays))
                {
                    int pastExpDays = TenantPasswordExpirySettingsProxy.GetPasswordExpirationDays(UserIdentity.TenantID);

                    if (pastExpDays != expiryDays)
                    {
                        // expiry date updated
                        TenantPasswordExpirySettingsProxy.SetupPasswordExpiry(expiryDays, UserIdentity.TenantID);
                    }
                }

                if (!string.IsNullOrEmpty(WCFSecretKey) && !WCFSecretKey.Equals(HiddenWCFSecretKey))
                {
                    // add wcf shared secret key setting
                    TenantClientValidatorSettingProxy.SaveClientValidatorSetting(WCFSecretKey, UserIdentity.TenantID);
                    WCFSecretKey = TenantClientValidatorSettingProxy.GetClientValidatorSecretKey(UserIdentity.TenantID);
                    ViewData["WCFSecretKey"] = ViewData["HiddenWCFSecretKey"] = WCFSecretKey;
                }

                var tenantSetting = new CelloSaaS.Model.SettingsManagement.TenantSetting
                {
                    Setting = new CelloSaaS.Model.SettingsManagement.Settings
                    {
                        Attributes = attributes
                    },
                    TenantID = UserIdentity.TenantID
                };

                // update the database
                TenantSettingsProxy.UpdateTenantSettings(tenantSetting);
            }
            catch (ViewMetaDataException viewException)
            {
                ExceptionService.HandleException(viewException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantSettingsUpdate);
            }
            catch (TenantPasswordExpirySettingException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantSettingsUpdate);
            }
            catch (TenantSettingsException settingsException)
            {
                ExceptionService.HandleException(settingsException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_TenantSettingsUpdate);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.TenantResource.e_Access);
            }

            if (ModelState.IsValid)
            {
                ViewData["Success"] = Resources.TenantResource.s_TenantSettingsUpdate;
            }

            return View();
        }

        /// <summary>
        /// This method is used to gets the languages.
        /// </summary>
        /// <param name="language">language.</param>
        /// <returns>collection of language</returns>
        private static List<SelectListItem> GetLanguages(string language)
        {
            var languageSelectList = new List<SelectListItem>();

            List<Language> languageList = LanguageProxy.GetAllLanguages();
            foreach (Language lang in languageList)
            {
                languageSelectList.Add(new SelectListItem
                {
                    Text = lang.Name,
                    Value = lang.Code,
                    Selected = lang.Code.Equals(language)
                });
            }

            return languageSelectList;
        }

        /// <summary>
        /// This method is used to get the list of themes based on the themes available in the themes folder.
        /// </summary>
        /// <param name="selectedThemeName">selected theme name.</param>
        /// <returns>collection of theme name.</returns>
        private List<SelectListItem> GetThemeNameList(string selectedThemeName)
        {
            System.IO.DirectoryInfo appThemeDir = new System.IO.DirectoryInfo(Server.MapPath("~\\App_Themes"));
            System.IO.DirectoryInfo[] appThemeSubDir = appThemeDir.GetDirectories();

            List<SelectListItem> themeList = new List<SelectListItem>();

            //Gets the app_themes sub folders(Theme folders)
            foreach (System.IO.DirectoryInfo themefolder in appThemeSubDir)
            {
                // Removing source safe related directories
                if (themefolder.Name != ".svn")
                {
                    themeList.Add(new SelectListItem
                    {
                        Text = themefolder.Name,
                        Value = themefolder.Name,
                        Selected = (themefolder.Name == selectedThemeName)
                    });
                }
            }

            return themeList;
        }

        #endregion TenantSetting

        #region GetmasterData

        /// <summary>
        /// This method is load the country.
        /// </summary>
        /// <param name="selectedId">selected identifier.</param>
        private void LoadCountry(string selectedId)
        {
            try
            {
                List<Country> lstcountry = CountryProxy.GetAllCountry();
                Country country = new Country { CountryId = "", Name = "--Select Country--" };
                lstcountry.Insert(0, country);
                ViewData["Tenant.Address.CountryId"] = new SelectList((IEnumerable)lstcountry, "CountryId", "Name", selectedId);
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_CountryLoaded);
            }
        }

        /// <summary>
        /// This method is used to get all the license packages.
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="selectedPackageId">package identifier.</param>
        /// <param name="isCreate">isCreate (true/false).</param>
        /// </summary>
        private void LoadLicensepackages(string tenantId, string selectedPackageId, bool isCreate)
        {
            try
            {
                List<LicensePackage> lstLicensePackage = null;
                if (isCreate)
                {
                    // for tenant create load all packages defined by the tenant
                    lstLicensePackage = LicenseProxy.GetAllLicensePackage(tenantId);
                }
                else
                {
                    // for tenant update load upgradable/downgradable packages
                    lstLicensePackage = LicenseProxy.GetAssignableLicensePackages(tenantId);
                }

                if (!string.IsNullOrEmpty(selectedPackageId)
                    && !lstLicensePackage.Any(x => x.LicensepackageId.Equals(selectedPackageId, StringComparison.OrdinalIgnoreCase)))
                {
                    var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(selectedPackageId);
                    if (packageDetails != null)
                    {
                        lstLicensePackage.Add(new LicensePackage
                        {
                            LicensepackageId = selectedPackageId,
                            Name = packageDetails.PackageName,
                            Description = packageDetails.PackageDescription,
                            IntegrateForBilling = packageDetails.IntegrateForBilling,
                            IsEvaluation = packageDetails.IsEvaluation,
                            TenantId = packageDetails.TenantId
                        });
                    }
                }

                ViewData["LicensePackages"] = lstLicensePackage;
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_PackageLoaded);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Access);
            }
        }

        /// <summary>
        /// This method is used to loads tenant license history based on the given tenant code.
        /// </summary>
        /// <param name="tenantCode">tenant code.</param>
        private void LoadTenantLicenseHistory(string tenantCode)
        {
            if (string.IsNullOrEmpty(tenantCode))
            {
                return;
            }

            try
            {
                var tenant = TenantProxy.GetTenantDetailsByTenantId(tenantCode, this.TenantId.ToString());

                if (tenant == null)
                    return;

                var licenseHistory = LicenseProxy.GetTenantLicenseHistory(tenantCode);
                ViewData["LicenseHistory"] = licenseHistory;

                if (licenseHistory != null)
                {
                    var parentTenantId = Guid.Parse(tenant.TenantDetails.ParentTenantId);
                    var pricePlans = new Dictionary<Guid, PricePlan>();

                    foreach (var lic in licenseHistory.Where(x => !string.IsNullOrEmpty(x.PricePlanId)))
                    {
                        if (!pricePlans.ContainsKey(Guid.Parse(lic.PricePlanId)))
                        {
                            var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(lic.PricePlanId), parentTenantId);

                            if (pricePlan != null)
                            {
                                pricePlans.Add(pricePlan.Id, pricePlan);
                            }
                        }
                    }

                    ViewData["PricePlans"] = pricePlans;
                }
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_PackageLoaded);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Access);
            }
        }

        #endregion GetmasterData

        #region Validation

        /// <summary>
        /// This method is to validate the tenant details.
        /// </summary>
        /// <param name="tenantDetails">tenant details.</param>
        private void ValidateTenant(TenantDetails tenantDetails)
        {
            if (string.IsNullOrWhiteSpace(tenantDetails.TenantCodeString))
            {
                ModelState.AddModelError("Tenant.TenantDetails.TenantCodeString", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCode);
            }
            else if (!Util.ValidateAlphaNumeric(tenantDetails.TenantCodeString))
            {
                ModelState.AddModelError("Tenant.TenantDetails.TenantCodeString", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_InvalidTenantCode);
            }
            else
            {
                tenantDetails.TenantCodeString = tenantDetails.TenantCodeString.Trim();
            }

            if (string.IsNullOrWhiteSpace(tenantDetails.TenantName))
            {
                ModelState.AddModelError("Tenant.TenantDetails.TenantName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantName);
            }
            else
            {
                tenantDetails.TenantName = tenantDetails.TenantName.Trim();
            }

            if (!string.IsNullOrWhiteSpace(tenantDetails.Website) && !Util.ValidateURL(tenantDetails.Website))
            {
                ModelState.AddModelError("Tenant.TenantDetails.Website", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Website);
            }
            else if (!string.IsNullOrWhiteSpace(tenantDetails.Website))
            {
                tenantDetails.Website = tenantDetails.Website.ToLowerInvariant();
            }

            if (!string.IsNullOrEmpty(tenantDetails.URL) && !Util.ValidateURL(tenantDetails.URL))
            {
                ModelState.AddModelError("Tenant.TenantDetails.URL", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_URL);
            }
            else if (!string.IsNullOrWhiteSpace(tenantDetails.URL))
            {
                tenantDetails.URL = tenantDetails.URL.ToLowerInvariant();
                if (!tenantDetails.URL.EndsWith(@"/"))
                    tenantDetails.URL += @"/";
            }

            if (string.IsNullOrWhiteSpace(tenantDetails.CompanySize) || tenantDetails.CompanySize == "-1")
            {
                tenantDetails.CompanySize = null;
            }
        }

        /// <summary>
        /// This method is to validate the membership and user details.
        /// </summary>
        /// <param name="membershipDetails">membership details.</param>
        /// <param name="userDetails">user details.</param>
        private void ValidateUserAndMembership(MembershipDetails membershipDetails, User userDetails)
        {
            if (string.IsNullOrWhiteSpace(userDetails.FirstName))
            {
                ModelState.AddModelError("Tenant.TenantAdminUserdetail.FirstName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_UserFirstname);
            }

            if (string.IsNullOrWhiteSpace(userDetails.LastName))
            {
                ModelState.AddModelError("Tenant.TenantAdminUserdetail.LastName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.UserResource.e_LastName);
            }

            if (string.IsNullOrWhiteSpace(membershipDetails.UserName))
            {
                ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.UserName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_UserNameMandatory);
            }

            // Check valid emali or not
            if (!string.IsNullOrWhiteSpace(membershipDetails.EmailId) && !Util.ValidateEmailId(membershipDetails.EmailId))
            {
                ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.EmailId", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantAdminInvalidEmailId);
            }
        }

        /// <summary>
        /// This method is validate the address details.
        /// </summary>
        /// <param name="address">address.</param>
        private void ValidateRegAddress(Address address)
        {
            if (string.IsNullOrWhiteSpace(address.Address1))
            {
                this.ModelState.AddModelError("Tenant.Address.Address1", "");
                this.ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Address);
            }
            if (string.IsNullOrWhiteSpace(address.City))
            {
                ModelState.AddModelError("Tenant.Address.City", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_City);
            }
            if (string.IsNullOrWhiteSpace(address.State))
            {
                ModelState.AddModelError("Tenant.Address.State", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_State);
            }
            if (string.IsNullOrWhiteSpace(address.CountryId))
            {
                ModelState.AddModelError("Tenant.Address.CountryId", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Country);
            }

            if (string.IsNullOrWhiteSpace(address.PostalCode))
            {
                ModelState.AddModelError("Tenant.Address.PostalCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.UserResource.e_Zipcode);
            }
            else if (!Util.ValidateAlphaNumericWithSpace(address.PostalCode))
            {
                ModelState.AddModelError("Tenant.Address.PostalCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.UserResource.e_Zipcode_SpecialChars);
            }
        }

        /// <summary>
        /// This method is to validate contact details.
        /// </summary>
        /// <param name="contactDetails">contact details.</param>
        private void ValidateContactDetails(ContactDetails contactDetails)
        {
            if (string.IsNullOrWhiteSpace(contactDetails.FirstName))
            {
                ModelState.AddModelError("Tenant.ContactDetail.FirstName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_FirstName);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.LastName))
            {
                ModelState.AddModelError("Tenant.ContactDetail.LastName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_LastName);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Phone))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Phone", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Phone);
            }
            else if (!Util.ValidatePhoneNumber(contactDetails.Phone))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Phone", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_PhoneNumberFormat);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Email))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Email", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ContactEmail);
            }
            else if (!Util.ValidateEmailId(contactDetails.Email))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Email", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ContactInvalidEmailId);
            }
        }

        /// <summary>
        /// This method is used to validate tenant license details.
        /// </summary>
        /// <param name="tenantLicense">tenant license.</param>
        /// <param name="prevLicense">previous license.</param>
        /// <returns></returns>
        private void ValidateLicense(TenantLicense tenantLicense, TenantLicense prevLicense)
        {
            if (string.IsNullOrEmpty(tenantLicense.PackageId))
            {
                ModelState.AddModelError("TenantLicense.PackageId", Resources.TenantResource.e_Package);
            }

            if (tenantLicense.NumberOfUsers.HasValue && tenantLicense.NumberOfUsers.Value <= 0)
            {
                this.ModelState.AddModelError("TenantLicense.NumberOfUsers", Resources.TenantResource.e_NumberOfUserFormate);
            }

            if (!string.IsNullOrEmpty(tenantLicense.PackageId))
            {
                var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(tenantLicense.PackageId);

                if (packageDetails == null)
                {
                    ModelState.AddModelError("TenantLicense.PackageId", Resources.TenantResource.e_Package);
                }

                tenantLicense.PackageName = packageDetails.PackageName;
            }

            if (!string.IsNullOrEmpty(tenantLicense.PricePlanId))
            {
                var priceplans = BillingPlanProxy.GetPricePlansByPackageId(Guid.Parse(tenantLicense.PackageId), Guid.Parse(TenantId));

                if (priceplans == null || priceplans.Count < 1 || !priceplans.Any(pp => pp.Value.Id.Equals(Guid.Parse(tenantLicense.PricePlanId))))
                {
                    ModelState.AddModelError("TenantLicense.PricePlanId", Resources.TenantResource.e_Priceplan);
                }
            }

            if (prevLicense != null && !string.IsNullOrEmpty(tenantLicense.TenantLicenseCode)
                && prevLicense.TenantLicenseCode == tenantLicense.TenantLicenseCode
                && prevLicense.PackageId == tenantLicense.PackageId
                && prevLicense.PricePlanId == tenantLicense.PricePlanId)
            {
                // update previous license code
                if (tenantLicense.ValidityStart.Date == prevLicense.ValidityStart.Date)
                {
                    tenantLicense.ValidityStart = prevLicense.ValidityStart;
                }

                if (tenantLicense.ValidityEnd.HasValue && prevLicense.ValidityEnd.HasValue
                    && tenantLicense.ValidityEnd.Value.Date == prevLicense.ValidityEnd.Value.Date)
                {
                    tenantLicense.ValidityEnd = prevLicense.ValidityEnd;
                }
                else if (tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value.ToString("hh:mm tt") == "12:00 AM")
                {
                    tenantLicense.ValidityEnd = tenantLicense.ValidityEnd.Value.AddDays(1).AddSeconds(-1);
                }

                if (tenantLicense.TrialEndDate.HasValue && prevLicense.TrialEndDate.HasValue
                   && tenantLicense.TrialEndDate.Value.Date == prevLicense.TrialEndDate.Value.Date)
                {
                    tenantLicense.TrialEndDate = prevLicense.TrialEndDate;
                }
                else if (tenantLicense.TrialEndDate.HasValue && tenantLicense.TrialEndDate.Value.ToString("hh:mm tt") == "12:00 AM")
                {
                    tenantLicense.TrialEndDate = tenantLicense.TrialEndDate.Value.AddDays(1).AddSeconds(-1);
                }
            }
            else
            {
                // new tenant or upgrade to new plan scenario
                //if (tenantLicense.ValidityStart == DateTime.Today)
                {
                    tenantLicense.ValidityStart = DateTime.Now;
                }

                if (tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value.ToString("hh:mm tt") == "12:00 AM")
                {
                    tenantLicense.ValidityEnd = tenantLicense.ValidityEnd.Value.AddDays(1).AddSeconds(-1);
                }

                if (tenantLicense.TrialEndDate.HasValue && tenantLicense.TrialEndDate.Value.ToString("hh:mm tt") == "12:00 AM")
                {
                    tenantLicense.TrialEndDate = tenantLicense.TrialEndDate.Value.AddDays(1).AddSeconds(-1);
                }
            }

            if (string.IsNullOrEmpty(tenantLicense.TenantLicenseCode) && tenantLicense.ValidityStart < DateTime.Now.Date)
            {
                this.ModelState.AddModelError("TenantLicense.ValidityStart", Resources.TenantResource.e_SubscriptionStartDate);
            }

            if (tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value <= tenantLicense.ValidityStart)
            {
                this.ModelState.AddModelError("TenantLicense.ValidityEnd", Resources.TenantResource.e_SubscriptionEndDate);
            }

            if (tenantLicense.TrialEndDate.HasValue && tenantLicense.TrialEndDate.Value <= tenantLicense.ValidityStart)
            {
                this.ModelState.AddModelError("TenantLicense.TrialEndDate", Resources.TenantResource.e_TrialEndDate);
            }

            if (tenantLicense.TrialEndDate.HasValue && tenantLicense.ValidityEnd.HasValue && tenantLicense.TrialEndDate.Value > tenantLicense.ValidityEnd)
            {
                this.ModelState.AddModelError("TenantLicense.TrialEndDate", Resources.TenantResource.e_TrialEndDateLess);
            }
        }

        #endregion Validation
    }
}