﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.EventScheduler.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Encryption;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.MasterData;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.Rules.Core;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.Configuration;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.MasterData;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.ServiceContracts.ViewManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.Configuration;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.MasterData;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.ServiceProxies.ViewManagement;
using CelloSaaS.View;
using CelloSaaSApplication.Models;
using CelloSaaSWebClient.Models;
using System.Threading.Tasks;
using CelloSaaSWebClient.Services;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for tenant self registration.
    /// </summary>
    public class TenantSelfRegistrationController : CelloController
    {
        private const string DefaultPolicy = "GlobalExceptionLogger";

        private string TenantId = UserIdentity.TenantID;
        private string UserId = UserIdentity.UserId;

        /// <summary>
        /// This method is used to initiate the self-registration process by the tenant.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> TenantSelfRegistration()
        {
            var tenantUri = Request.Url.GetBaseUrl();
            if (!string.IsNullOrEmpty(tenantUri))
            {
                var tenantInfo = TenantProxy.GetTenantDetailsByURL(tenantUri);

                //registration can be done only with a non-tenant specific URI or from the context of the product administrator URI
                if (tenantInfo != null && !tenantInfo.TenantCode.Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase))
                {
                    return new HttpUnauthorizedResult();
                }
            }

            ViewData["Tenant"] = new Tenant();
            LoadLicensepackages(this.TenantId, string.Empty);
            LoadCompanySize(string.Empty, this.TenantId);
            if (ConfigHelper.EnableExternalAuthentication)
                new CelloSaaSWebClient.Models.Helpers().IdentitySetup(this, null, null);
            return View();
        }

        /// <summary>
        /// This method is called to perform the registration process by the tenant.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> TenantSelfRegistration(FormCollection formCollection)
        {
            Tenant tenant = new Tenant();
            if (ConfigHelper.EnableExternalAuthentication)
                new CelloSaaSWebClient.Models.Helpers().IdentitySetup(this, null, null);
            UpdateModel(tenant, "Tenant");
            string licensePackageId = formCollection["PackageId"];
            string pricePlanId = formCollection["PricePlanId"];
            string membershipId = formCollection["MembershipId"];
            string discountCode = formCollection["DiscountCode"];
            string billingFrequency = formCollection["billingFrequency"];

            BillFrequency frequency = (BillFrequency)Enum.Parse(typeof(BillFrequency), billingFrequency);

            ViewData["PackageId"] = licensePackageId;
            ViewData["PricePlanId"] = pricePlanId;
            ViewBag.MembershipId = membershipId;
            ViewBag.DiscountCode = discountCode;

            if (string.IsNullOrEmpty(licensePackageId) || string.IsNullOrEmpty(licensePackageId.Trim()))
            {
                ModelState.AddModelError("LicensePackage", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Package);
            }

            ValidateTenant(tenant);

            #region Tenant Settings
            string idProvider = formCollection["TenantIdProvider"], authType = formCollection["UserAuthenticationType"], ldapServerUri = formCollection["OnPremiseServerUri"];

            if (string.IsNullOrEmpty(idProvider))
                idProvider = ConfigHelper.CelloOpenIdProvider;

            if (string.IsNullOrEmpty(authType))
                authType = System.Configuration.ConfigurationManager.AppSettings["CelloAuthentication"];

            ViewBag.OnPremiseLDAPUri = ldapServerUri;

            if (!string.IsNullOrEmpty(ldapServerUri) && !Util.ValidateURL(ldapServerUri))
            {
                ModelState.AddModelError("TenantErrorMessage", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_URL);
            }

            var tenantSettings = Helpers.ApplyTenantAuthSettings(idProvider, authType, ldapServerUri);
            #endregion

            // there is a discount code and it does not match with the existing plans, then show the error message
            string discountedPricePlanId = null;
            try
            {
                discountedPricePlanId = TenantExtensions.GetDiscountedPricePlanId(discountCode, licensePackageId, TenantId, frequency);
            }
            catch (Exception)
            {
                ModelState.AddModelError("DiscountCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_InvalidDiscountCode);
            }

            if (!string.IsNullOrEmpty(discountCode) && !string.IsNullOrEmpty(licensePackageId) && string.IsNullOrEmpty(discountedPricePlanId))
            {
                ModelState.AddModelError("DiscountCode", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_InvalidDiscountCode);
            }

            if (ModelState.IsValid)
            {
                tenant.Address = null;
                tenant.Types = new List<TenantType>() { new TenantType() { ID = TenantTypeConstants.SMB } };
                tenant.TenantDetails.ParentTenantId = this.TenantId;
                tenant.TenantDetails.IsSelfRegistered = true;
                tenant.TenantDetails.EnableAutoDebit = true;
                tenant.TenantDetails.DataPartitionId = GetDefaultDataPartitionId();
                tenant.TenantDetails.URL = FormTenantUrl(tenant.TenantDetails.TenantCodeString);
                tenant.TenantAdminUserdetail.FirstName = tenant.ContactDetail.FirstName;
                tenant.TenantAdminUserdetail.LastName = tenant.ContactDetail.LastName;
                tenant.TenantAdminMembershipdetail.EmailId = tenant.ContactDetail.Email;

                if (ConfigHelper.EnableUserNameInTenantRegistration)
                    tenant.TenantAdminMembershipdetail.UserName = tenant.TenantAdminMembershipdetail.UserName;

                if (ConfigHelper.SendTenantActivationLink)
                {
                    // if there is an explicit username, do not override that with the email
                    if (string.IsNullOrEmpty(tenant.TenantAdminMembershipdetail.UserName))
                        tenant.TenantAdminMembershipdetail.UserName = tenant.ContactDetail.Email;
                    tenant.TenantDetails.ParentTenantId = this.TenantId;
                    if (idProvider.Equals(ConfigHelper.CelloOpenIdProvider, StringComparison.OrdinalIgnoreCase))
                    {
                        tenant.TenantAdminMembershipdetail.Password = new CelloSaaS.Services.UserManagement.PasswordGenerator().GeneratePassword();
                    }
                    else
                    {
                        tenant.TenantAdminMembershipdetail.Password = ConfigHelper.DefaultUserPassword;
                    }
                    tenant.TenantAdminMembershipdetail.IsFirstTimeUser = idProvider.Equals(ConfigHelper.CelloOpenIdProvider, StringComparison.OrdinalIgnoreCase);
                    tenant.TenantDetails.ApprovalStatus = TenantApprovalStatus.WAITINGFORAPPROVAL;
                }
                else
                {
                    tenant.TenantDetails.ApprovalStatus = TenantApprovalStatus.APPROVED;
                }

                TenantLicense tenantLicense = new TenantLicense()
                {
                    PackageId = licensePackageId,
                    //If no discounted price plans, use the default price plan, else use the discounted price plan id
                    PricePlanId = string.IsNullOrEmpty(discountedPricePlanId)
                                    ? pricePlanId
                                    : discountedPricePlanId,
                    ValidityStart = DateTime.Now
                };

                if (CreateTenant(tenant, tenantLicense, discountCode, tenantSettings))
                {
                    if (ConfigHelper.EnableExternalAuthentication)
                    {
                        var providers = AuthenticationService.GetAuthenticationProviders();
                        // During tenant creation
                        var authTypeCollection = AuthenticationService.GetAuthenticationTypes(idProvider);
                        var authenticationTypes = authTypeCollection[idProvider];
                        await ManageTenantClients(tenant, providers, idProvider, authenticationTypes, authType, ldapServerUri);
                    }
                    return RedirectToAction("RegistrationSuccess");
                }
            }

            LoadLicensepackages(this.TenantId, licensePackageId);
            LoadCompanySize(tenant.TenantDetails.CompanySize, this.TenantId);
            ViewData["Tenant"] = tenant;
            return View();
        }

        /// <summary>
        /// This method is used to returns the default data partition configured.
        /// </summary>
        /// <returns></returns>
        private static string GetDefaultDataPartitionId()
        {
            try
            {
                var dataPartition = CelloSaaS.PartitionManagement.ServiceProxies.DataPartitionProxy.GetDefaultPartitionId();
                return dataPartition == Guid.Empty ? null : dataPartition.ToString();
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
            }

            return null;
        }

        /// <summary>
        /// This method is used to get the form tenant url string based on the given company code.
        /// </summary>
        /// <param name="companyCode">company code.</param>
        /// <returns></returns>
        private string FormTenantUrl(string companyCode)
        {
            var t = this.Request.Url.Host.Split('.');
            var temp = t.Length == 3 ? t.Skip(1) : t;
            string domainPart = string.Join(".", temp);
            var currentUrl = string.Format(CultureInfo.InvariantCulture, "{0}://{1}.{2}", this.Request.Url.Scheme, companyCode, domainPart);
            if (this.Request.Url.Port != 80 && this.Request.Url.Port != 443)
            {
                currentUrl += ":" + this.Request.Url.Port;
            }
            return string.Format(CultureInfo.InvariantCulture, ConfigHelper.TenantUrlFormat ?? (currentUrl.TrimEnd('/') + "/"), companyCode).ToLowerInvariant();
        }

        /// <summary>
        /// This method is used to get the view of tenant registration.
        /// </summary>
        /// <returns></returns>
        public ActionResult RegistrationSuccess()
        {
            if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                TenantContext.Clear();
            }

            return View();
        }

        /// <summary>
        /// This method is used to activates the specified activation key.
        /// </summary>
        /// <param name="activationKey">activation key.</param>
        /// <returns></returns>
        public ActionResult Activate(string activationKey)
        {
            string errorMessage = Resources.TenantSelfRegistrationResource.e_InvalidActivationLink;

            if (string.IsNullOrEmpty(activationKey))
            {
                return HttpNotFound();
            }

            try
            {
                var passwordEncrptionService = ServiceLocator.Resolve<IPasswordEncrptionService>();
                string key = passwordEncrptionService.DecryptPassword(ProductAdminConstants.ProductAdminTenantId, HttpUtility.UrlDecode(activationKey));
                string[] activationDetails = key.Split('~');

                if (activationDetails.Length != 4)
                {
                    ViewData["ErrorMessage"] = errorMessage;
                    return View();
                }

                string tenantId = activationDetails[0];

                var tenant = TenantProxy.GetTenantDetailsByTenantId(tenantId, ProductAdminConstants.ProductAdminTenantId);

                if (tenant != null && tenant.TenantDetails != null)
                {
                    if (tenant.TenantDetails.ApprovalStatus == TenantApprovalStatus.WAITINGFORAPPROVAL)
                    {
                        var license = LicenseProxy.GetTenantLicense(tenantId);

                        if (license != null && !string.IsNullOrEmpty(license.PricePlanId) && !license.TrialEndDate.HasValue)
                        {
                            // no trial tenant, so ask him to provide Payment info if in pre-paid mode
                            var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(license.PricePlanId), Guid.Parse(ProductAdminConstants.ProductAdminTenantId));

                            if (pricePlan == null)
                            {
                                return HttpNotFound();
                            }

                            var setting = BillingProxy.GetBillingSetting(pricePlan.BillFrequency, Guid.Parse(tenantId));

                            if (setting.Mode == BillCycleMode.Prepaid)
                            {
                                SetSecretPrePaidLogonCookie(tenant.TenantDetails.TenantCodeString);
                                Response.Redirect(Url.Action("InviteLogOn", "Account", new { activationKey = activationKey, area = "" }, Request.Url.Scheme));
                                Response.SuppressContent = true;
                                Response.End();
                                return Content("");
                            }
                        }
                    }

                    if (TenantProxy.ActivateTenant(activationKey))
                    {
                        return RedirectToAction("InviteLogOn", "Account", new { activationKey = activationKey, area = "" });
                    }
                }
                else
                {
                    errorMessage = Resources.TenantSelfRegistrationResource.e_InvalidActivationLink;
                }
            }
            catch (ArgumentException argumrntException)
            {
                ExceptionService.HandleException(argumrntException, DefaultPolicy);
                errorMessage = Resources.TenantSelfRegistrationResource.e_ActivateAccount;
            }
            catch (TenantActivationException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = ex.Message;

                if (errorMessage.Contains("Account already activated"))
                {
                    return RedirectToAction("InviteLogOn", "Account", new { activationKey = activationKey, area = "" });
                }
            }
            catch (TenantActivationKeyExpiredException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = Resources.TenantSelfRegistrationResource.e_ActivationKeyExpired;
            }
            catch (InvalidActivationKeyException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = Resources.TenantSelfRegistrationResource.e_InvalidActivationLink;
            }
            catch (TenantException ex)
            {
                ExceptionService.HandleException(ex, DefaultPolicy);
                errorMessage = Resources.TenantSelfRegistrationResource.e_ExceptionActivateAccount;
            }

            ViewData["ErrorMessage"] = errorMessage;
            return View();
        }

        /// <summary>
        /// This method is used to set secret pre paid login cookie based on the given company code.
        /// </summary>
        /// <param name="companyCode"></param>
        private void SetSecretPrePaidLogonCookie(string companyCode)
        {
            Response.Cookies.Add(new HttpCookie("cello_tpl")
            {
                Value = companyCode.Encrypt(),
                HttpOnly = true
            });
        }

        #region Private Methods

        private static async Task ManageTenantClients(Tenant tenant, Dictionary<string, string> providers, string tenantProvider, Dictionary<string, string> authenticationTypes, string authType, string ldapUri)
        {
            await new TenantPostProcessor().DoPostProcessorInsert(tenant.TenantDetails.TenantCode, tenant.TenantDetails, new[] { tenant });

            // If tenant has no setting, try to get the value from the form collection, else try to use cellopen id provider
            if (providers.ContainsKey(tenantProvider) && !string.IsNullOrEmpty(tenantProvider))
            {
                var tenantSettings = Helpers.ApplyTenantAuthSettings(tenantProvider, authType, ldapUri);
                if (tenantSettings != null && tenantSettings.Count > 0)
                    TenantSettingsProxy.UpdateTenantSettings(new TenantSetting
                    {
                        TenantID = tenant.TenantDetails.TenantCode,
                        Setting = new Settings
                        {
                            Attributes = tenantSettings
                        }
                    });
            }

            if (authenticationTypes.ContainsKey(authType))
            {
                UserSettingsProxy.UpdateUserSettings(new UserSettings
                {
                    AddedBy = UserIdentity.UserId,
                    Setting = new Settings
                    {
                        Attributes = new Dictionary<string, string>
                                    {
                                        {
                                            SettingAttributeConstants.LoginProvider,authType
                                        }
                                    }
                    },
                    UserId = tenant.TenantAdminUserdetail.Identifier
                });
            }
        }

        /// <summary>
        /// Creates the tenant.
        /// </summary>
        /// <param name="tenant">The tenant.</param>
        /// <param name="tenantLicense">The tenant license.</param>
        private bool CreateTenant(Tenant tenant, TenantLicense tenantLicense, string discountCode, Dictionary<string, string> tenantSettings = null)
        {
            try
            {
                SettingsTemplate settingsTemplate = SettingsTemplateProxy.GetSettingsTemplateDetails(SettingsTemplateConstants.DefaultCustomTemplate);
                if (settingsTemplate == null || settingsTemplate.Attributes == null || settingsTemplate.Attributes.Count < 1)
                {
                    throw new SettingsTemplateException("Invalid Setting Template");
                }

                TenantSettingsTemplate tenantSettingsTemplate = new TenantSettingsTemplate
                {
                    SettingsTemplate = settingsTemplate,
                    TemplateId = settingsTemplate.Id.ToString(),
                    Attributes = settingsTemplate.Attributes,
                    CreatedBy = UserIdentity.UserId
                };

                if (tenantSettingsTemplate.Attributes != null && tenantSettingsTemplate.Attributes.Count > 0
                    && tenantSettingsTemplate.Attributes.Any(attr => attr.AttributeId.Equals(SettingAttributeConstants.IdProviders))
                    && tenantSettings.ContainsKey(SettingAttributeConstants.IdProviders))
                {
                    var idp = tenantSettingsTemplate.Attributes.Where(at => at.AttributeId.Equals(SettingAttributeConstants.IdProviders)).FirstOrDefault();
                    idp.AttributeValue = tenantSettings[SettingAttributeConstants.IdProviders];
                }

                var tenantId = TenantProxy.ProvisionTenant(tenant, tenantLicense, tenantSettingsTemplate);
                tenant.TenantDetails.TenantCode = tenantId;
                if (!string.IsNullOrEmpty(tenantId) && (tenantSettings != null && tenantSettings.Count > 0))
                {
                    // Update the AuthServer Settings in Tenant Settings
                    TenantSettingsProxy.UpdateTenantSettings(new TenantSetting
                    {
                        TenantID = tenantId,
                        Setting = new Settings
                        {
                            Attributes = tenantSettings
                        }
                    });
                }

                if (!ConfigHelper.ApplyGlobalBillSettingForPrepaidMode && !string.IsNullOrEmpty(tenantId) && !string.IsNullOrEmpty(tenantLicense.PricePlanId))
                {
                    var gTenantId = Guid.Parse(tenantId);
                    var gParentTenantId = Guid.Parse(tenant.TenantDetails.ParentTenantId);
                    var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(tenantLicense.PricePlanId), gParentTenantId);
                    var frequency = pricePlan.BillFrequency;
                    var globalBillSetting = BillingProxy.GetBillingSetting(frequency, gParentTenantId);

                    if (globalBillSetting.Mode == BillCycleMode.Prepaid)
                    {
                        var startDay = tenantLicense.TrialEndDate.HasValue ? tenantLicense.TrialEndDate.Value.Day : tenantLicense.ValidityStart.Day;

                        var billSetting = new BillingSetting
                        {
                            Id = Guid.Empty,
                            TenantId = gTenantId,
                            Mode = globalBillSetting.Mode,
                            BillFrequency = frequency,
                            ChargeDay = startDay + (globalBillSetting.ChargeDay - globalBillSetting.StartDay),
                            StartDay = startDay,
                        };
                        BillingProxy.AddBillingSetting(billSetting);
                    }
                }

                return true;
            }
            catch (InvalidEmailException invalidEmailId)
            {
                ExceptionService.HandleException(invalidEmailId, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.ErrorMessage.e_InvalidEmailId);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ParameterEmptyOrNull);
            }
            catch (DuplicateTenantCodeException duplicateTenantCode)
            {
                ExceptionService.HandleException(duplicateTenantCode, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCodeExists);
            }
            catch (DuplicateTenantNameException duplicateTenantName)
            {
                ExceptionService.HandleException(duplicateTenantName, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantNameExists);
            }
            catch (DuplicateWebsiteException duplicateWebsite)
            {
                ExceptionService.HandleException(duplicateWebsite, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantWebsiteExists);
            }
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCreate);
            }
            catch (DuplicateEmailIDException duplictaeEmailIdexception)
            {
                ExceptionService.HandleException(duplictaeEmailIdexception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_DuplicateTenantAdminEmailId);
            }
            catch (UnauthorizedAccessException exception)
            {
                ExceptionService.HandleException(exception, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Access);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                string errorMessage = dataException.InnerException != null ? dataException.InnerException.Message : dataException.Message;
                // do not change to resource, unique extn field error message comes from here
                ModelState.AddModelError("TenantErrorMessage", errorMessage);
            }
            catch (SettingsTemplateException settingsTemplateException)
            {
                ExceptionService.HandleException(settingsTemplateException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", settingsTemplateException.Message);
            }
            catch (BusinessRuleException bex)
            {
                ExceptionService.HandleException(bex, DefaultPolicy);
                if (bex.Data != null)
                {
                    int count = 1;
                    foreach (DictionaryEntry er in bex.Data)
                    {
                        foreach (string error in (IEnumerable<string>)er.Value)
                        {
                            ModelState.AddModelError("Error" + (count++), error);
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Loads the country.
        /// </summary>
        /// <param name="selectedId">The selected id.</param>
        private void LoadCountry(string selectedId)
        {
            try
            {
                List<Country> lstcountry = CountryProxy.GetAllCountry();
                Country country = new Country { CountryId = "", Name = "--Select Country--" };
                lstcountry.Insert(0, country);
                ViewData["Address.CountryId"] = new SelectList((IEnumerable)lstcountry, "CountryId", "Name", selectedId);
            }
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, DefaultPolicy);
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_CountryLoaded);
            }
        }

        /// <summary>
        /// Loads the licensepackages.
        /// </summary>
        /// <param name="tenantId">The tenant id.</param>
        /// <param name="selectedPackageId">The selected package id.</param>
        private void LoadLicensepackages(string tenantId, string selectedPackageId)
        {
            string parentTenantId = UserIdentity.TenantID;
            var availablePackages = LicenseProxy.GetAllLicensePackage(parentTenantId);
            var lstPickupListValues = PickupListProxy.GetAllPickupListValues(PickupListConstants.SelfRegistrationPackagePickupList, parentTenantId.ToString()) ?? new List<PickupListValue>();
            var orderedPkgPricePlans = new Dictionary<string, List<PricePlan>>();
           
            if (availablePackages != null && availablePackages.Count > 0)
            {
                var packagePricePlans = new Dictionary<string, List<PricePlan>>();
                var pickupIds = lstPickupListValues.Where(x => x.Status == true).Select(x => x.ItemId);
                availablePackages = availablePackages.Where(x => pickupIds.Contains(x.LicensepackageId)).ToList();
                bool pricePlanFlag = false;
                foreach (var package in availablePackages)
                {
                    var plans = BillingPlanProxy.GetPricePlansByPackageId(Guid.Parse(package.LicensepackageId), Guid.Parse(parentTenantId));
                    
                    if (plans != null && plans.Count > 0)
                    {
                        pricePlanFlag = true;
                        packagePricePlans.Add(package.LicensepackageId, plans.Values.ToList());
                    }

                }
                if (!pricePlanFlag)
                    ViewBag.PackageError = Resources.TenantSelfRegistrationResource.e_PackageError;

                orderedPkgPricePlans = packagePricePlans.OrderBy(pp => (pp.Value != null && pp.Value.Count > 0) ? pp.Value[0].Price : 0).ToDictionary(t => t.Key, t => t.Value);
                //ViewBag.PackagePricePlans = packagePricePlans;
                ViewBag.PackagePricePlans = orderedPkgPricePlans;
            }

            if (!string.IsNullOrEmpty(selectedPackageId))
            {
                if (availablePackages == null)
                    availablePackages = new List<LicensePackage>();

                var currentPackage = availablePackages.Where(x => x.LicensepackageId.Equals(selectedPackageId, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                if (currentPackage == null)
                {
                    var packageDetails = LicenseProxy.GetPackageDetailsByPackageId(selectedPackageId);
                    if (packageDetails != null)
                    {
                        availablePackages.Add(new LicensePackage
                        {
                            LicensepackageId = selectedPackageId,
                            Name = packageDetails.PackageName,
                            Description = packageDetails.PackageDescription,
                            IntegrateForBilling = packageDetails.IntegrateForBilling,
                            IsEvaluation = packageDetails.IsEvaluation,
                            TenantId = packageDetails.TenantId
                        });
                    }
                }
            }

            List<LicensePackage> avlPkgs = new List<LicensePackage>();

            if (orderedPkgPricePlans != null && orderedPkgPricePlans.Count > 0)
            {
                foreach (var pricedPkg in orderedPkgPricePlans)
                {
                    LicensePackage licensePkg = availablePackages.Where(ap => ap.LicensepackageId == pricedPkg.Key).FirstOrDefault();
                    if (licensePkg != null)
                    {
                        avlPkgs.Add(licensePkg);
                    }
                }
            }

            ViewBag.AvailablePackages = avlPkgs;

            //ViewBag.AvailablePackages = availablePackages;
        }

        /// <summary>
        /// Loads the size of the company.
        /// </summary>
        /// <param name="companySize">Size of the company.</param>
        /// <param name="tenantId">The tenant id.</param>
        private void LoadCompanySize(string companySize, string tenantId)
        {
            List<PickupListValue> lstPickupListValues = PickupListProxy.GetAllPickupListValues(PickupListConstants.CompanySizePickupList, tenantId);
            Dictionary<string, string> companySizeDetails = new Dictionary<string, string>();
            companySizeDetails.Add("-1", "-Select-");
            if (lstPickupListValues != null && lstPickupListValues.Count > 0)
            {
                lstPickupListValues.ForEach(x => companySizeDetails.Add(x.ItemId, x.ItemName));
            }

            ViewData["CompanySize"] = new SelectList(companySizeDetails, "Key", "Value", companySize);
        }

        #endregion

        #region Validation

        /// <summary>
        /// This method is used to validates the tenant.
        /// </summary>
        /// <param name="tenant">tenant.</param>
        private void ValidateTenant(Tenant tenant)
        {
            ValidateTenantDetails(tenant.TenantDetails);
            ValidateContactDetails(tenant.ContactDetail);

            if (!ConfigHelper.SendTenantActivationLink)
            {
                if (string.IsNullOrWhiteSpace(tenant.TenantAdminMembershipdetail.UserName))
                {
                    ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.UserName", Resources.TenantSelfRegistrationResource.e_UserNameMandatory);
                }
                else if (!Util.ValidateIdentifier(tenant.TenantAdminMembershipdetail.UserName))
                {
                    ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.UserName", Resources.TenantSelfRegistrationResource.e_UserNameInvalid);
                }
                else if (tenant.TenantAdminMembershipdetail.UserName.Length < 4)
                {
                    ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.UserName", Resources.TenantSelfRegistrationResource.e_UserNameLength);
                }

                string confirmPassword = this.Request.Form["ConfirmPassword"];

                if (string.IsNullOrWhiteSpace(tenant.TenantAdminMembershipdetail.Password))
                {
                    ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.Password", Resources.TenantSelfRegistrationResource.e_PasswordMandatory);
                }
                else if (tenant.TenantAdminMembershipdetail.Password != confirmPassword)
                {
                    ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.Password", Resources.TenantSelfRegistrationResource.e_PasswordNotMatch);
                }
                else
                {
                    var status = PasswordValidationProxy.IsPasswordValid(tenant.TenantAdminMembershipdetail.Password);

                    if (!status.IsPasswordValid)
                    {
                        ModelState.AddModelError("Tenant.TenantAdminMembershipdetail.Password", status.Message);
                    }
                }
            }
        }

        /// <summary>
        /// This method is used to validates the contact details.
        /// </summary>
        /// <param name="contactDetails">contact details.</param>
        private void ValidateContactDetails(ContactDetails contactDetails)
        {
            if (string.IsNullOrWhiteSpace(contactDetails.FirstName))
            {
                ModelState.AddModelError("Tenant.ContactDetail.FirstName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_FirstName);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.LastName))
            {
                ModelState.AddModelError("Tenant.ContactDetail.LastName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_LastName);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Phone))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Phone", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_Phone);
            }
            else if (!Util.ValidatePhoneNumber(contactDetails.Phone))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Phone", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantSelfRegistrationResource.e_PhoneNumberInvalid);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Email))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Email", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ContactEmail);
            }
            else if (!Util.ValidateEmailId(contactDetails.Email))
            {
                ModelState.AddModelError("Tenant.ContactDetail.Email", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_ContactInvalidEmailId);
            }

            if (string.IsNullOrWhiteSpace(contactDetails.Fax))
            {
                contactDetails.Fax = contactDetails.Phone;
            }
        }

        /// <summary>
        /// This method is used to validates the tenant details.
        /// </summary>
        /// <param name="tenantDetails">tenant details.</param>
        private void ValidateTenantDetails(TenantDetails tenantDetails)
        {
            if (string.IsNullOrWhiteSpace(tenantDetails.TenantCodeString))
            {
                ModelState.AddModelError("Tenant.TenantDetails.TenantCodeString", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantCode);
            }
            else if (!Util.ValidateAlphaNumeric(tenantDetails.TenantCodeString))
            {
                ModelState.AddModelError("Tenant.TenantDetails.TenantCodeString", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_InvalidTenantCode);
            }

            if (string.IsNullOrWhiteSpace(tenantDetails.TenantName))
            {
                ModelState.AddModelError("Tenant.TenantDetails.TenantName", "");
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantResource.e_TenantName);
            }

            if (string.IsNullOrWhiteSpace(tenantDetails.CompanySize) || tenantDetails.CompanySize.Equals("-1"))
            {
                tenantDetails.CompanySize = null;
            }
        }

        #endregion
    }
}
