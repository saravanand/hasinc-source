﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using System.Globalization;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for tenant user association.
    /// </summary>
    public class TenantUserAssociationController : CelloController
    {
        //
        // GET: /TenantUserAssociation/

        /// <summary>
        /// This method is used to make link between other tenant list.
        /// </summary>
        /// <returns></returns>
        public ActionResult LinkedByOtherTenantsList()
        {
            var loggedInTenantSettings = CelloSaaS.ServiceProxies.SettingsManagement.TenantSettingsProxy.GetTenantSettings(UserIdentity.TenantID);

            if (loggedInTenantSettings == null || loggedInTenantSettings.Setting == null || loggedInTenantSettings.Setting.Attributes == null || loggedInTenantSettings.Setting.Attributes.Count < 1)
            {
                ViewData["sharedUsers"] = false;
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantUserAssociationResource.m_nousersharing);
                return View();
            }

            if (loggedInTenantSettings.Setting.Attributes.ContainsKey(AttributeConstants.ShareUsers) && bool.Parse(loggedInTenantSettings.Setting.Attributes[AttributeConstants.ShareUsers]))
            {
                LinkedTenantsList();
                ViewData["sharedUsers"] = true;
            }
            else
            {
                ViewData["sharedUsers"] = false;
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantUserAssociationResource.m_nousersharing);
            }

            return View();
        }

        /// <summary>
        ///This method is used to get the other tenant user list.
        /// </summary>
        /// <returns></returns>
        public ActionResult OtherTenantUsersList()
        {
            RequstedUsersList();
            return View();
        }

        /// <summary>
        /// This method is used to requests the tenant.
        /// </summary>
        /// <param name="comments">comments.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="status">status.</param>
        /// <param name="linkedId">linked identifier.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RequestTenant(string comments, string tenantId, string status, string linkedId)
        {
            LinkedTenantUsers linkedtenantuser = new LinkedTenantUsers();
            linkedtenantuser.TenantId = tenantId;
            linkedtenantuser.UserId = UserIdentity.UserId;
            linkedtenantuser.CreatedBy = UserIdentity.UserId;
            linkedtenantuser.RequestStatus = RequestStatus.WaitingForTenantApproval;
            linkedtenantuser.Comments = comments;
            linkedtenantuser.RequestedBy = TenantUserAssociationConstants.User;

            try
            {
                if (status == RequestStatus.Rejected.ToString() && !string.IsNullOrEmpty(linkedId))
                {
                    TenantUserAssociationProxy.UpdateLinkedTenantsUserStatus(linkedId, false);
                }
                TenantUserAssociationProxy.LinkOtherTenantsUser(linkedtenantuser);
                ModelState.AddModelError("TenantSuccessMessage", Resources.TenantUserAssociationResource.s_RequestSent);
            }
            catch (Exception)
            {
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantUserAssociationResource.e_RequestSend);
            }

            LinkedTenantsList();
            return View("LinkedByOtherTenantsList");
        }

        /// <summary>
        ///  This method is used to requests the user.
        /// </summary>
        /// <param name="comments">comments.</param>
        /// <param name="userId">user identifier.</param>
        /// <param name="status">status.</param>
        /// <param name="linkedId">linked identifier.</param>
        /// <param name="roles">list of roles.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RequestUser(string comments, string userId, string status, string linkedId, string[] roles)
        {
            LinkedTenantUsers linkedtenantuser = new LinkedTenantUsers
            {
                TenantId = UserIdentity.TenantID,
                UserId = userId,
                CreatedBy = UserIdentity.UserId,
                RequestStatus = RequestStatus.WaitingForUserApproval,
                Comments = comments,
                RequestedBy = TenantUserAssociationConstants.Admin
            };

            try
            {
                if (status.Equals(RequestStatus.Rejected.ToString("F"), StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(linkedId))
                {
                    TenantUserAssociationProxy.UpdateLinkedTenantsUserStatus(linkedId, false);
                }

                // add the privileges to this user.
                TenantUserAssociationProxy.LinkOtherTenantsUser(linkedtenantuser, roles, UserIdentity.TenantID);

                RequstedUsersList();
                return View("OtherTenantUsersList");
            }
            catch (Exception)
            {
                ModelState.AddModelError("TenantErrorMessage", Resources.TenantUserAssociationResource.e_RequestSend);
                return null;
            }
        }

        /// <summary>
        /// This method is used to responds the request.
        /// </summary>
        /// <param name="linkedTenantUsersId">linked tenant users identifier.</param>
        /// <param name="status">status.</param>
        /// <param name="userId">user identifier.</param>
        /// <param name="requestedBy">requested by.</param>
        /// <param name="roles">list of roles.</param>
        /// <returns></returns>
        public ActionResult RespondRequest(string linkedTenantUsersId, string status, string userId, string requestedBy, string[] roles)
        {
            if (string.IsNullOrEmpty(linkedTenantUsersId))
            {
                ModelState.AddModelError("", "Invalid Link Id");
                RequstedUsersList();
                return PartialView("OtherTenantUsers");
            }

            LinkedTenantUsers linkedTenantUsers = TenantUserAssociationProxy.GetLinkedTenantUserDetails(linkedTenantUsersId);

            if (linkedTenantUsers == null || string.IsNullOrEmpty(linkedTenantUsers.TenantId))
            {
                ModelState.AddModelError("", "Invalid Association");
                RequstedUsersList();
                return PartialView("OtherTenantUsers");
            }

            linkedTenantUsers.RequestedBy = requestedBy;
            linkedTenantUsers.UpdatedBy = UserIdentity.UserId;
            if (status.Equals(RequestStatus.Approved.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                linkedTenantUsers.RequestStatus = RequestStatus.Approved;
            }
            else if (status.Equals(RequestStatus.Rejected.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                linkedTenantUsers.RequestStatus = RequestStatus.Rejected;
            }
            else
            {
                RequstedUsersList();
                return PartialView("OtherTenantUsers");
            }

			try
            {
                TenantUserAssociationProxy.UpdateLinkedTenantsUser(linkedTenantUsers, roles, UserIdentity.TenantID);
                ModelState.AddModelError("TenantSuccessMessage", string.Format(CultureInfo.CurrentCulture, Resources.TenantUserAssociationResource.s_successfull, status));
            }
            catch (TenantUserAssociationException)
            {
                ModelState.AddModelError("TenantErrorMessage", string.Format(CultureInfo.CurrentCulture, Resources.TenantUserAssociationResource.e_Response, status));
            }
            catch (LicenseLimitException licenseLimitException)
            {
                ModelState.AddModelError("TenantErrorMessage", licenseLimitException.Message);
            }
            catch (Exception)
            {
                ModelState.AddModelError("TenantErrorMessage", string.Format(CultureInfo.CurrentCulture, Resources.TenantUserAssociationResource.e_Response, status));
            }

            if (!userId.Equals(UserIdentity.UserId, StringComparison.OrdinalIgnoreCase))
            {
                RequstedUsersList();
                return PartialView("OtherTenantUsers");
            }
            else
            {
                LinkedTenantsList();
                return View("RequestByTenant");
            }
        }

        /// <summary>
        /// This method is used to get the linked tenant list.
        /// </summary>
        private void LinkedTenantsList()
        {
            string userId = UserIdentity.UserId;
            try
            {
                Dictionary<string, LinkedTenantUsers> linkedTenantList = TenantUserAssociationProxy.GetAllLinkableTenantDetails(userId);
                if (linkedTenantList.ContainsKey(UserIdentity.TenantID) && string.IsNullOrEmpty(UserIdentity.SessionTenantID))
                {
                    linkedTenantList.Remove(UserIdentity.TenantID);
                }
				
                if (linkedTenantList.ContainsKey(UserIdentity.LoggedInUserTenantId))
                {
                    linkedTenantList.Remove(UserIdentity.LoggedInUserTenantId);
                }

                Dictionary<string, LinkedTenantUsers> requestByAdmin = new Dictionary<string, LinkedTenantUsers>();
                Dictionary<string, LinkedTenantUsers> requestByUser = new Dictionary<string, LinkedTenantUsers>();
                foreach (var linkedTenant in linkedTenantList)
                {
                    if (linkedTenant.Value.RequestedBy == "Admin")
                    {
                        requestByAdmin.Add(linkedTenant.Key, linkedTenant.Value);
                    }
                    else
                    {
                        requestByUser.Add(linkedTenant.Key, linkedTenant.Value);
                    }
                }
                ViewData["LinkedTenants"] = requestByUser.Values.ToList();
                ViewData["RequstByUser"] = requestByUser.Values.ToList();
                ViewData["RequstByAdmin"] = requestByAdmin.Values.ToList();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// This method is used to get the requested user list.
        /// </summary>
        private void RequstedUsersList()
        {
            string tenantId = UserIdentity.TenantID;

            try
            {
                Dictionary<string, LinkedTenantUsers> requestdUsersList = TenantUserAssociationProxy.GetAllLinkedTenantUsers(tenantId);

                Dictionary<string, LinkedTenantUsers> requestByAdmin = new Dictionary<string, LinkedTenantUsers>();
                Dictionary<string, LinkedTenantUsers> requestByUser = new Dictionary<string, LinkedTenantUsers>();

                foreach (var LinkedTenant in requestdUsersList)
                {
                    if (LinkedTenant.Value.RequestedBy == "Admin")
                    {
                        requestByAdmin.Add(LinkedTenant.Key, LinkedTenant.Value);

                    }
                    else
                    {
                        requestByUser.Add(LinkedTenant.Key, LinkedTenant.Value);
                    }
                }

                ViewData["RequstByUser"] = requestByUser.Values.ToList();
                ViewData["RequstByAdmin"] = requestByAdmin.Values.ToList();
            }
            catch (Exception)
            {

            }
        }
    }
}
