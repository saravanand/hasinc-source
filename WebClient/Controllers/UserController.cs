using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.MasterData;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.Rules.Core;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.MasterData;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.ServiceProxies.MasterData;
using CelloSaaS.ServiceProxies.TenantManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using MvcContrib.Pagination;
using CelloSaaS.EventScheduler.ServiceProxies;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.Library.Helpers;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaSWebClient.Services;
using System.Security;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaSWebClient.Models;
using CelloSaaSApplication.Models;
using System.Threading.Tasks;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for user management.
    /// </summary>
    [HandleError]
    public class UserController : CelloSaaS.View.CelloController
    {
        private string TenantId = TenantContext.GetTenantId(new User().EntityIdentifier);
        private const string _defaultPolicy = "GlobalExceptionLogger";

        /// <summary>
        /// This method is used to determines whether the specified tenant identifier is product admin.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns><c>true</c> if [is product admin] [the specified tenant id]; otherwise, <c>false</c>.</returns>
        public bool IsProductAdmin(string tenantId)
        {
            return (ProductAdminConstants.ProductAdminTenantId.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase));
        }

        #region User Lock, Un-Lock and reset password

        /// <summary>
        /// This method is used to un-Locks the lock user accounts.
        /// </summary>
        /// <param name="userIds">list of user identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult UnLockUserAccounts(string[] userIds, string tenantId)
        {
            tenantId = this.TenantId;
            if (Request.IsAjaxRequest() && userIds != null && userIds.Count() > 0
                && !string.IsNullOrEmpty(tenantId))
            {
                try
                {
                    if (AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.UnLockUser, tenantId))
                    {
                        int count = UserDetailsProxy.UnLockUserAccounts(userIds);
                        var message = string.Format(CultureInfo.InvariantCulture, Resources.UserResource.s_UserUnLock, count);
                        return Json(new { Status = message });
                    }
                    else
                    {
                        return Json(new { error = Resources.UserResource.e_UnlockAccessDenied });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UserDetailException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (AccessControlException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
            }

            return Json(new { error = Resources.UserResource.e_UnlockUser });
        }

        /// <summary>
        /// This method is used to locks the user accounts.
        /// </summary>
        /// <param name="userIds">list of user identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult LockUserAccounts(string[] userIds, string tenantId)
        {
            tenantId = this.TenantId;
            if (Request.IsAjaxRequest() && userIds != null && userIds.Count() > 0
                && !string.IsNullOrEmpty(tenantId))
            {
                try
                {
                    if (AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.LockUser, tenantId))
                    {
                        int count = UserDetailsProxy.LockUserAccounts(userIds);
                        var message = string.Format(CultureInfo.InvariantCulture, Resources.UserResource.s_UserLock, count);
                        return Json(new { Status = message });
                    }
                    else
                    {
                        return Json(new { error = Resources.UserResource.e_LockAccessDenied });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UserDetailException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (AccessControlException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
            }

            return Json(new { error = Resources.UserResource.e_LockUser });
        }

        /// <summary>
        /// This method is used to un-Locks the lock user accounts based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult UnLockAllUserAccounts(string tenantId)
        {
            tenantId = this.TenantId;
            if (Request.IsAjaxRequest() && !string.IsNullOrEmpty(tenantId))
            {
                try
                {
                    if (AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.UnLockUser, tenantId))
                    {
                        int count = UserDetailsProxy.UnLockAllUserAccounts(tenantId);
                        var message = string.Format(CultureInfo.InvariantCulture, Resources.UserResource.s_UserUnLock, count);
                        return Json(new { Status = message });
                    }
                    else
                    {
                        return Json(new { error = Resources.UserResource.e_UnlockAllAccessDenied });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UserDetailException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (AccessControlException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
            }

            return Json(new { error = Resources.UserResource.e_UnlockAllUser });
        }

        /// <summary>
        /// This method is used to locks the user accounts based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult LockAllUserAccounts(string tenantId)
        {
            tenantId = this.TenantId;
            if (Request.IsAjaxRequest() && !string.IsNullOrEmpty(tenantId))
            {
                try
                {
                    if (AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.LockUser, tenantId))
                    {
                        int count = UserDetailsProxy.LockAllUserAccounts(tenantId);
                        var message = string.Format(CultureInfo.InvariantCulture, Resources.UserResource.s_UserLock, count);
                        return Json(new { Status = message });
                    }
                    else
                    {
                        return Json(new { error = Resources.UserResource.e_LockAllAccessDenied });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UserDetailException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (AccessControlException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
            }

            return Json(new { error = Resources.UserResource.e_LockAllUser });
        }

        /// <summary>
        /// This method is used to resets the pin for user accounts.
        /// </summary>
        /// <param name="userIds">list of user identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult ResetPINforUserAccounts(string[] userIds, string tenantId)
        {
            tenantId = this.TenantId;
            //string password = "company";
            if (Request.IsAjaxRequest() && userIds != null && userIds.Count() > 0
                && !string.IsNullOrEmpty(tenantId))
            {
                try
                {
                    if (AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.ResetPINUser, tenantId))
                    {
                        int count = UserDetailsProxy.ResetPassword(userIds, (ConfigHelper.EnableExternalAuthentication)
                          ? ConfigHelper.DefaultUserPassword
                          : string.Empty, tenantId);

                        var message = string.Format(CultureInfo.InvariantCulture, Resources.UserResource.s_PinReset, count);
                        return Json(new { Status = message });
                    }
                    else
                    {
                        return Json(new { error = Resources.UserResource.e_ResetPinAccessDenied });
                    }
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UserDetailException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (AccessControlException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
            }

            return Json(new { error = Resources.UserResource.e_ResetPin });
        }

        /// <summary>
        /// This method is used to approve the user accounts.
        /// </summary>
        /// <param name="userIds">list of user identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult ApproveUserAccounts(string[] userIds, string tenantId)
        {
            tenantId = this.TenantId;
            if (Request.IsAjaxRequest() && userIds != null && userIds.Count() > 0 && !string.IsNullOrEmpty(tenantId))
            {
                try
                {
                    if (AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.ApproveUser, tenantId))
                    {
                        int count = UserDetailsProxy.ApproveUsers(userIds);

                        return (count > 0)
                                ? Json(new { Status = string.Format(CultureInfo.InvariantCulture, Resources.UserResource.s_UsersApproved, count) })
                                : Json(new { error = Resources.UserResource.e_UserApproval });
                    }
                    else
                    {
                        return Json(new { error = Resources.UserResource.e_AccessDenied });
                    }
                }
                catch (LicenseLimitException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                    return Json(new { error = Resources.UserResource.e_UserCreateLicenseOver });
                }
                catch (ArgumentNullException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (ArgumentException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (UnauthorizedAccessException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (UserDetailException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (AccessControlException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
            }

            return Json(new { error = Resources.UserResource.e_UserApproval });
        }

        /// <summary>
        /// This method is used to Invite the users that are added via the user management console.
        /// </summary>
        /// <param name="userIds">list of user identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult InviteUsers(string[] userIds, string tenantId)
        {
            tenantId = this.TenantId;
            if (Request.IsAjaxRequest() && userIds != null && userIds.Count() > 0 && !string.IsNullOrEmpty(tenantId))
            {
                try
                {
                    if (AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.ApproveUser, tenantId))
                    {
                        int count = UserDetailsProxy.InviteUsers(userIds);

                        return (count < 0)
                                ? Json(new { error = Resources.UserResource.e_UserInviteFailed })
                                : Json(new { Status = string.Format(CultureInfo.InvariantCulture, Resources.UserResource.s_UsersInvited, count) });
                    }
                    else
                    {
                        return Json(new { error = Resources.UserResource.e_AccessDenied });
                    }
                }
                catch (LicenseLimitException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                    return Json(new { error = Resources.UserResource.e_UserCreateLicenseOver });
                }
                catch (ArgumentNullException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (ArgumentException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (UnauthorizedAccessException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (UserDetailException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
                catch (AccessControlException ex)
                {
                    Task.Run(() => ExceptionService.HandleException(ex, _defaultPolicy));
                }
            }

            return Json(new { error = Resources.UserResource.e_UserInviteFailed });
        }

        #endregion

        #region Manage User Role Public Methods

        /// <summary>
        /// This method is used to manages the user role based on the given user identifier.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <returns></returns>
        public ActionResult ManageUserRole(string userId)
        {

            if (string.IsNullOrEmpty(userId))
            {
                ModelState.AddModelError("Error", Resources.UserResource.e_UserIdMandatory);
            }
            else
            {
                try
                {
                    string tenantId = this.TenantId;
                    Tenant tenant = TenantProxy.GetTenantInfo(tenantId);

                    if (tenant == null)
                    {
                        ModelState.AddModelError("Error", Resources.UserResource.e_TenantNotFound);
                    }
                    else
                    {
                        //  ViewData["CanManageUserRoleTenant"] = IsProductAdmin(tenantId);
                        ViewData["CanManageUserRoleTenant"] = UserIdentity.IsInRole(RoleConstants.TenantAdmin);

                        var userNames = UserDetailsProxy.GetUserNameForAudit(new string[] { userId });
                        if (userNames == null || !userNames.ContainsKey(userId))
                        {
                            ModelState.AddModelError("Error", Resources.UserResource.e_UserNotFound);
                        }
                        else
                        {
                            List<Role> roleList = this.GetRoleList();
                            List<Role> userRoles = new List<Role>();

                            if (roleList != null)
                            {
                                // get user roles
                                string[] roleIds = RoleProxy.GetUserRolesForTenant(userId, this.TenantId);

                                foreach (string roleId in roleIds)
                                {
                                    var found = roleList.Where(r => r.RoleId == roleId);
                                    if (found.Count() > 0)
                                    {
                                        // add to user role list details
                                        userRoles.Add(found.FirstOrDefault());

                                        // remove form the role list
                                        roleList.Remove(found.FirstOrDefault());
                                    }
                                }
                            }

                            if (userRoles.Where(r => r.RoleId.Equals(RoleConstants.ServiceAdmin)).Count() > 0)
                            {
                                ViewData["ServiceList"] = GetServiceList();

                                Dictionary<string, Service> roleServices = LicenseProxy.GetServicesByUserId(userId);
                                ViewData["RoleServices"] = roleServices != null ? roleServices.Keys.ToArray() : null;
                            }

                            // check if he has add user role privilege
                            if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                            {
                                ViewData["CanAddUserRole"] = true;
                            }
                            else
                            {
                                ViewData["CanAddUserRole"] = AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.AddUserRole, tenantId);
                            }


                            ViewData["UserRoleList"] = userRoles;
                            ViewData["RoleList"] = roleList;
                            ViewData["UserName"] = userNames[userId];
                            ViewData["UserId"] = userId;
                            ViewData["TenantId"] = tenantId;
                            ViewData["TenantName"] = tenant.TenantDetails.TenantName;
                        }
                    }
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_ParameterEmptyOrNull);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_ParameterEmptyOrNull);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_AccessDenied);
                }
                catch (UserDetailException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_UserDetails);
                }
                catch (TenantException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_FetchTenantDetails);
                }
                catch (RoleException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_FetchRoleDetails);
                }
                catch (AccessControlException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_CheckTenantAccessPrivilege);
                }
                catch (LicenseException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                    ModelState.AddModelError("Error", Resources.UserResource.e_FetchService);
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("UserRoleTenantDetails");
            }

            return View();
        }

        /// <summary>
        /// This method is used to gets the user role tenant mapping list.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult GetUserRoleTenantMappingList(string userId, string roleId, string tenantId)
        {
            // check for product admin tenant users
            //if (!IsProductAdmin(tenantId))
            //{
            //    return Redirect(Url.Action("AccessDenied", "Home"));
            //}
            tenantId = this.TenantId;
            if (string.IsNullOrEmpty(roleId))
            {
                return Content(Resources.UserResource.e_RoleIdMandatory);
            }

            if (string.IsNullOrEmpty(userId))
            {
                return Content(Resources.UserResource.e_UserIdMandatory);
            }

            if (string.IsNullOrEmpty(tenantId))
            {
                return Content(Resources.UserResource.e_TenantIdMandatory);
            }

            try
            {
                // get tenant details
                Tenant tenantInfo = TenantProxy.GetTenantInfo(tenantId);

                if (tenantInfo == null)
                {
                    return Content(Resources.UserResource.e_TenantNotFound);
                }

                // get role details
                Role role = RoleProxy.GetRoleDetailsByRoleId(roleId);

                if (role == null)
                {
                    return Content(Resources.UserResource.e_RoleNotFound);
                }

                ViewData["UserId"] = userId;
                ViewData["RoleDetial"] = role;

                Dictionary<string, TenantDetails> tenantDetailsList = null;// TenantProxy.GetTenantsForCurrentUser();

                if (UserIdentity.IsInRole(RoleConstants.ProductAdmin) && UserIdentity.TenantID == tenantId)
                {
                    tenantDetailsList = TenantProxy.GetAllActiveTenantDetails();
                }
                else
                {
                    // get tenants whom he has this privilege
                    //string[] privileges = new string[] { PrivilegeConstants.AddUserRole };
                    var childTenantDetails = TenantRelationProxy.GetAllChildTenants(tenantId);
                    tenantDetailsList = childTenantDetails;
                }

                if (!tenantDetailsList.ContainsKey(tenantId))
                {
                    tenantDetailsList.Add(tenantId, tenantInfo.TenantDetails);
                }

                // fetch mapped tenants
                Dictionary<string, TenantDetails> mappedTenantDetails = TenantProxy.GetTenantsByUserRole(userId, roleId, this.TenantId);

                // store the tenant details with true/false for mapped the role to show in the UI
                Dictionary<TenantDetails, bool> mappedTenantList = new Dictionary<TenantDetails, bool>();

                if (tenantDetailsList != null && tenantDetailsList.Count > 0)
                {
                    foreach (TenantDetails tenantDetail in tenantDetailsList.Values)
                    {
                        bool status = false;
                        if (mappedTenantDetails.ContainsKey(tenantDetail.TenantCode))
                        {
                            mappedTenantDetails.Remove(tenantDetail.TenantCode);
                            status = true;
                        }
                        mappedTenantList.Add(tenantDetail, status);
                    }
                }

                // store the remaining tenant ids so the update by other restricted users
                // wont deletes already added tenants
                ViewData["HiddenTenantIds"] = mappedTenantDetails.Keys.ToList();

                var sortedDict = (from mappedTenant in mappedTenantList
                                  orderby mappedTenant.Key.TenantName.ToUpperInvariant() ascending
                                  select mappedTenant).ToDictionary(pair => pair.Key, pair => pair.Value);

                ViewData["TenantList"] = sortedDict;
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Content(Resources.UserResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Content(Resources.UserResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Content(Resources.UserResource.e_AccessDenied);
            }
            catch (TenantException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Content(Resources.UserResource.e_FetchTenantMapping);
            }
            catch (RoleException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Content(Resources.UserResource.e_FetchTenantMapping);
            }

            return PartialView("UserRoleTenantList");
        }

        /// <summary>
        /// This method is used to adds the role to user.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="roleIds">role identifier.</param>
        /// <returns>JsonResult.</returns>
        public JsonResult AddRoleToUser(string userId, string[] roleIds)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { Error = Resources.UserResource.e_UserIdMandatory });
            }

            if (roleIds == null || roleIds.Length == 0)
            {
                return Json(new { Error = Resources.UserResource.e_RoleMandatory });
            }

            try
            {
                RoleProxy.AddUserToRoles(userId, roleIds, this.TenantId);
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_ParameterEmptyOrNull });
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_ParameterEmptyOrNull });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_AccessDenied });
            }
            catch (RoleException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_AddRole });
            }

            return Json(new { Success = Resources.UserResource.s_AddRole });
        }

        /// <summary>
        /// This method is used to removes the role from user.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <returns>JsonResult.</returns>
        public JsonResult RemoveRoleFromUser(string userId, string roleId)
        {
            if (string.IsNullOrEmpty(roleId))
            {
                return Json(new { Error = Resources.UserResource.e_RoleIdMandatory });
            }

            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { Error = Resources.UserResource.e_UserIdMandatory });
            }

            try
            {
                // first, deletes the role mapping for this tenant and user
                // if no further mapping is found, then it delets the user role also
                RoleProxy.DeleteUserRoleAssignedByTenant(userId, new string[] { roleId }, this.TenantId);
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_ParameterEmptyOrNull });
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_ParameterEmptyOrNull });
            }
            catch (UnauthorizedAccessException unauthorisedex)
            {
                ExceptionService.HandleException(unauthorisedex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_AccessDenied });
            }
            catch (RoleException rex)
            {
                ExceptionService.HandleException(rex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_UnselectToDeleteRole });
            }

            return Json(new { Success = Resources.UserResource.s_RemoveUserRole });
        }

        /// <summary>
        /// This method is used to updates the user role tenant mapping.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <param name="tenantIds">list of tenant identifiers.</param>
        /// <returns>JsonResult.</returns>
        public JsonResult UpdateUserRoleTenantMapping(string userId, string roleId, string[] tenantIds)
        {
            if (string.IsNullOrEmpty(roleId))
            {
                return Json(new { Error = Resources.UserResource.e_RoleIdMandatory });
            }

            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { Error = Resources.UserResource.e_UserIdMandatory });
            }

            try
            {
                if (tenantIds == null || tenantIds.Length == 0)
                {
                    // delete all the tenant mapping
                    Dictionary<string, TenantDetails> mappedTenantDetails = TenantProxy.GetTenantsByUserRole(userId, roleId, this.TenantId);
                    if (mappedTenantDetails != null && mappedTenantDetails.Count > 0)
                    {
                        TenantProxy.DeleteUserRoleTenants(userId, roleId, mappedTenantDetails.Keys.ToArray(), this.TenantId);
                    }
                }
                else
                {
                    // now map the tenant ids to the role and user identifier
                    TenantProxy.UpdateUserRoleTenants(userId, roleId, tenantIds, this.TenantId);
                }
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_ParameterEmptyOrNull });
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_UpdateTenantMapping });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_AccessDenied });
            }
            catch (TenantException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_UpdateTenantMapping });
            }

            return Json(new { Success = Resources.UserResource.s_SaveTenantMapping });
        }

        /// <summary>
        /// This method is used to updates the user service mapping.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="serviceIds">list of service identifiers.</param>
        /// <returns>JsonResult.</returns>
        public JsonResult UpdateUserServiceMapping(string userId, string[] serviceIds)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return Json(new { Error = Resources.UserResource.e_UserIdMandatory });
            }

            try
            {
                LicenseProxy.UpdateUserServices(userId, serviceIds);
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_ParameterEmptyOrNull });
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_ParameterEmptyOrNull });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_AccessDenied });
            }
            catch (LicenseException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
                return Json(new { Error = Resources.UserResource.e_UserUpdate });
            }

            return Json(new { Success = Resources.UserResource.s_SaveServiceMapping });
        }

        #region Private Methods

        /// <summary>
        /// This is the get method of Role list.
        /// If user is Service Admin then get roles for his service only.
        /// For other users get the roles by tenant license and intersect with
        /// logged in user roles.
        /// </summary>        
        private List<Role> GetRoleList()
        {
            // check the logged in user is a service admin
            if (UserIdentity.Roles.Contains(RoleConstants.ServiceAdmin))
            {
                return GetServiceAdminRoles();
            }
            // Get global role details
            List<Role> roleGlobalRole = RoleProxy.GetGlobalRolesByTenant(this.TenantId);

            // Remove product admin role
            if (roleGlobalRole != null && roleGlobalRole.Count > 0)
            {
                List<string> lstUserRoles = null;

                // Get the logged in user roles
                if (UserIdentity.Roles != null && UserIdentity.Roles.Count() > 0)
                {
                    lstUserRoles = UserIdentity.Roles.ToList();
                }

                // If user have roles check user have tenant admin or product admin  
                if (lstUserRoles == null || lstUserRoles.Count == 0
                    || !(lstUserRoles.Contains(RoleConstants.ProductAdmin)
                    || lstUserRoles.Contains(RoleConstants.TenantAdmin)))
                {
                    // If user does not have any roles remove 
                    // product admin and tenant admin roles from role list
                    roleGlobalRole.RemoveAll(r => r.RoleId == RoleConstants.TenantAdmin);
                }

                // Get tenant role details
                List<Role> roleTenantRole = RoleProxy.GetTenantRoleDetails(this.TenantId);

                if (roleTenantRole != null && roleTenantRole.Count > 0)
                {
                    roleGlobalRole.AddRange(roleTenantRole);
                }

                // remove product admin role
                roleGlobalRole.RemoveAll(r => r.RoleId == RoleConstants.ProductAdmin);

            }
            return roleGlobalRole;

        }

        /// <summary>
        /// This method is used to gets the service admin roles.
        /// </summary>
        /// <returns></returns>
        private static List<Role> GetServiceAdminRoles()
        {
            // get his services
            Dictionary<string, Service> userServices = LicenseProxy.GetServicesByUserId(UserIdentity.UserId);

            if (userServices != null && userServices.Count > 0)
            {
                // get roles mapped to his services
                Dictionary<string, Role> roleList = RoleProxy.GetRolesByServiceIds(userServices.Keys.ToArray());

                // remove the product & service admin roles
                if (roleList != null && roleList.Count > 0)
                {
                    roleList.Remove(RoleConstants.ProductAdmin);
                    roleList.Remove(RoleConstants.ServiceAdmin);
                }

                return (roleList != null) ? roleList.Values.ToList() : null;
            }

            return null;
        }

        #endregion

        #endregion

        #region User Details Public Methods

        /// <summary>
        /// This method is used to get user details.
        /// </summary>
        /// <param name="page">current page number.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="searchString">search string.</param>
        /// <param name="searchFilter">search filter.</param>
        /// <returns>User List View</returns>
        public ActionResult UserList(int? page, string sortString, string sortDirection, string searchString, string searchFilter, int pageSize = 10)
        {
            string tenantId = this.TenantId;

            // get the paginated users of the tenant
            IPagination<UserDetails> userWithPagination = this.GetUserDetailsList(page, pageSize, sortString, sortDirection, tenantId, searchString, searchFilter);
            try
            {

                if (Request.IsAjaxRequest())
                {
                    return PartialView("PartialUserList", userWithPagination);
                }

                if (TenantUserAssociationProxy.CanLinkWithOtherTenants(tenantId))
                {
                    OtherTenantUserDetails(tenantId);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_ParameterEmptyOrNull);
            }
            catch (TenantUserAssociationException tenantUserAssociationException)
            {
                ExceptionService.HandleException(tenantUserAssociationException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_ParameterEmptyOrNull);
            }

            if (TempData["Success"] != null)
            {
                ModelState.AddModelError("UserSuccessMessage", TempData["Success"].ToString());
            }

            return View(userWithPagination);
        }

        /// <summary>
        /// This method is used to create/update user.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="pageNumber">current page number.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <returns>Add/Edit User Details View</returns>
        public ActionResult ManageUser(string userId, int? pageNumber, string sortString, string sortDirection)
        {
            UserDetails userDetails = new UserDetails();
            ViewData["PageNumber"] = !pageNumber.HasValue ? 1 : (int)pageNumber;
            ViewData["SortString"] = string.IsNullOrEmpty(sortString) ? UserDetailsSortStringConstants.FirstName : sortString;
            ViewData["SortDirection"] = string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(SortExpressionConstants.Ascending) ? SortExpressionConstants.Ascending : SortExpressionConstants.Decending;

            try
            {
                if (string.IsNullOrEmpty(userId))
                {
                    string tenantIdProvider = null;
                    if (ConfigHelper.EnableExternalAuthentication)
                    {
                        var tenantSettings = TenantSettingsProxy.GetTenantSettings(this.TenantId);
                        tenantIdProvider = tenantSettings.GetValue(SettingAttributeConstants.IdProviders);
                    }
                    else
                    {
                        tenantIdProvider = ConfigHelper.CelloOpenIdProvider;
                    }

                    ViewBag.TenantIdProvider = tenantIdProvider;
                    if (ConfigHelper.EnableExternalAuthentication)
                    {
                        new Helpers().IdentitySetup(this, tenantIdProvider, null);
                    }
                }
                else
                {
                    var userSettings = Helpers.GetUserAuthSettings(userId, this.TenantId);

                    if (userSettings != null)
                    {
                        ViewBag.TenantIdProvider = userSettings.Item1;
                        ViewBag.LoginProvider = userSettings.Item2;
                    }
                }

                if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                {
                    // check authorization
                    bool canUpdateUser = AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.UpdateUser, this.TenantId);
                    if (!canUpdateUser)
                    {
                        ModelState.AddModelError("AccessError", Resources.UserResource.e_UpdateUserAccessDenied);
                        return View();
                    }
                }

                if (string.IsNullOrEmpty(userId))
                {
                    ViewData["FormMode"] = CurrentFormMode.Insert;
                    Context.SetPrivilegeContext(PrivilegeConstants.AddUser);
                }
                else
                {
                    Guid tmp;
                    if (!Guid.TryParse(userId, out tmp))
                        return HttpNotFound("User not found!");

                    ViewData["FormMode"] = CurrentFormMode.Edit;
                    Context.SetPrivilegeContext(PrivilegeConstants.UpdateUser);

                    userDetails = UserDetailsProxy.GetUserDetailsByUserId(userId, this.TenantId);

                    if (userDetails == null)
                    {
                        return HttpNotFound("User not found!");
                    }
                    else
                    {
                        if (userDetails.Address == null)
                        {
                            userDetails.Address = new Address();
                        }
                    }
                }

                // Get all country details
                GetCountryDetails((userDetails == null || userDetails.Address == null) ? null : userDetails.Address.CountryId);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch UnauthorizedAccess exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_AccessDenied);
                ViewBag.AccessDenied = true;
            }
            // Catch role exception and show user exception message to user
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserLoad);
            }
            // Catch role exception and show user exception message to user
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_FetchTenantDetails);
            }
            // Catch role exception and show user exception message to user
            catch (AccessControlException accessControlException)
            {
                ExceptionService.HandleException(accessControlException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserLoad);
            }

            ViewData["UserDetails"] = userDetails;
            return View();
        }

        /// <summary>
        /// This method is used to create new user.
        /// </summary> 
        /// <param name="formCollection">form collection details</param>
        /// <returns>Success/Add user details view</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManageUser(FormCollection formCollection)
        {
            string tenantId = this.TenantId;

            ViewData["PageNumber"] = Convert.ToInt32(formCollection["PageNumber"], CultureInfo.InvariantCulture);
            ViewData["SortString"] = formCollection["SortString"];
            ViewData["SortDirection"] = formCollection["SortDirection"];

            // Create object for user details
            UserDetails userDetails = new UserDetails();

            Dictionary<string, string> providers = null;

            if (ConfigHelper.EnableExternalAuthentication)
            {
                providers = AuthenticationService.GetAuthenticationProviders();
            }
            var tenantSettings = TenantSettingsProxy.GetTenantSettings(tenantId);

            var idprovider = tenantSettings.GetValue(SettingAttributeConstants.IdProviders) ?? ConfigHelper.CelloOpenIdProvider;

            var selectedProvider = formCollection["UserAuthenticationType"];

            ViewBag.TenantIdProvider = idprovider;

            try
            {
                if (ConfigHelper.EnableExternalAuthentication)
                    new Helpers().IdentitySetup(this, idprovider, selectedProvider);

                if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                {
                    // check authorization
                    bool canUpdateUser = AccessControlProxy.CheckTenantAccessPrivilege(UserIdentity.UserId, PrivilegeConstants.UpdateUser, this.TenantId);

                    if (!canUpdateUser)
                    {
                        ModelState.AddModelError("AccessError", Resources.UserResource.e_UpdateUserAccessDenied);
                        return View();
                    }
                }

                string userId = formCollection["UserDetails.Identifier"];

                if (!string.IsNullOrEmpty(userId))
                {
                    userDetails = UserDetailsProxy.GetUserDetailsByUserId(userId, tenantId);
                    ViewData["FormMode"] = CurrentFormMode.Edit;
                }
                else
                {
                    ViewData["FormMode"] = CurrentFormMode.Insert;
                }

                if (ConfigHelper.EnableExternalAuthentication)
                {
                    var userSettings = Helpers.GetUserAuthSettings(userId, this.TenantId);
                    if (userSettings != null)
                    {
                        ViewBag.TenantIdProvider = userSettings.Item1;
                        ViewBag.LoginProvider = userSettings.Item2;
                    }
                }
                else
                {
                    ViewBag.TenantIdProvider = CelloSaaS.View.AppSettingHelper.CelloAuthentication;
                    ViewBag.LoginProvider = CelloSaaS.View.AppSettingHelper.CelloAuthentication;
                }

                // Get details from form and fill it in user details object
                TryUpdateModel(userDetails, "UserDetails");

                ValidateUserDetails(userDetails.User);
                ValidateMembership(userDetails.MembershipDetails);
                ValidateAddressDetails(userDetails);

                CheckForSharedUser(userDetails);

                if (ModelState.IsValid)
                {
                    string userDetailsId = string.Empty;

                    if (string.IsNullOrEmpty(userDetails.Identifier))
                    {
                        if (!idprovider.Equals("D1179B78-B86D-4B79-BD4B-615764FF15BB", StringComparison.OrdinalIgnoreCase))
                        {
                            // set the default password for new user, if the provider is not a Cello IDP
                            userDetails.MembershipDetails.Password = ConfigHelper.DefaultUserPassword;
                        }
                        Context.SetPrivilegeContext(PrivilegeConstants.AddUser);

                        userDetails.MembershipDetails.TenantCode = tenantId;
                        userDetails.User.CreatedBy = UserIdentity.UserId;
                        userDetails.User.CreatedOn = DateTime.Now;
                        userDetails.MembershipDetails.CreationDate = DateTime.Now;


                        if (ConfigHelper.EnableExternalAuthentication && providers != null && providers.ContainsKey(idprovider) && providers[idprovider] != Constants.CelloOpenIdProviderName)
                        {
                            userDetails.MembershipDetails.IsFirstTimeUser = false;
                        }
                        else
                        {
                            userDetails.MembershipDetails.IsFirstTimeUser = true;
                        }

                        userDetailsId = UserDetailsProxy.AddUser(userDetails);

                        //UserDetailsProxy.UpdateUser(userDetails);

                        userDetails.Identifier = userDetails.User.UserId = userDetailsId;

                        string userAuthType = formCollection["UserAuthenticationType"];

                        if (!string.IsNullOrEmpty(userAuthType))
                        {
                            UserSettingsProxy.UpdateUserSettings(new UserSettings
                            {
                                AddedBy = UserIdentity.UserId,
                                Setting = new Settings
                                {
                                    Attributes = new Dictionary<string, string>
                                    {
                                        {
                                            SettingAttributeConstants.LoginProvider,userAuthType
                                        }
                                    }
                                },
                                UserId = userDetailsId
                            });
                        }

                        if (string.IsNullOrEmpty(userDetailsId))
                        {
                            ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserCreate);
                        }
                        else
                        {
                            TempData["Success"] = Resources.UserResource.s_AddUser;
                        }
                    }
                    else
                    {
                        Context.SetPrivilegeContext(PrivilegeConstants.UpdateUser);

                        // Call update user method from proxy.
                        userDetails.User.UpdatedBy = UserIdentity.UserId;
                        userDetails.User.UpdatedOn = DateTime.Now;
                        userDetailsId = UserDetailsProxy.UpdateUser(userDetails);

                        if (string.IsNullOrEmpty(userDetailsId))
                        {
                            ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserUpdate);
                        }
                        else
                        {
                            TempData["Success"] = Resources.UserResource.s_UpdateUser;
                        }
                    }

                    if (!string.IsNullOrEmpty(userDetailsId))
                    {
                        // After user details created/updated successfully return to user details list
                        return RedirectToAction("UserList", "User", new { page = (int)ViewData["PageNumber"], sortString = (string)ViewData["SortString"], sortDirection = (string)ViewData["SortDirection"] });
                    }
                }
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch UnauthorizedAccess Exception and show user exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_AccessDenied);
                ViewBag.AccessDenied = true;
            }
            // Catch user detail exception and show user exception message to user
            catch (UserDetailException userException)
            {
                ExceptionService.HandleException(userException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserCreate);
            }
            // Catch user detail exception and show user exception message to user
            catch (AccessControlException userException)
            {
                ExceptionService.HandleException(userException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_CheckTenantAccessPrivilege);
            }
            // Catch user detail exception and show user exception message to user
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_FetchTenantDetails);
            }
            // Catch Invalid Email id exception and show user exception message to user
            catch (InvalidEmailIDException invalidEmailIDException)
            {
                ExceptionService.HandleException(invalidEmailIDException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserName);
            }
            // Catch Invalid User Name exception and show user exception message to user
            catch (InvalidUserNameException invaildUserNameException)
            {
                ExceptionService.HandleException(invaildUserNameException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserName);
            }
            // Catch Duplicate Email Id exception and show user exception message to user
            catch (DuplicateEmailIDException duplicateEmailIDException)
            {
                ExceptionService.HandleException(duplicateEmailIDException, _defaultPolicy);
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.e_DuplicateEmailId);

                if ((string.IsNullOrEmpty(userDetails.User.UserId) || string.IsNullOrEmpty(userDetails.Identifier))
                    && TenantUserAssociationProxy.CanLinkWithOtherTenants(this.TenantId))
                {
                    string otherTenantUserId = UserDetailsProxy.GetUserIdByEmailId(userDetails.MembershipDetails.EmailId);

                    if (!string.IsNullOrEmpty(otherTenantUserId))
                    {
                        ViewData["IsLinkable"] = true;
                    }
                }
            }
            // Catch Duplicate user name exception and show user exception message to user
            catch (DuplicateUserNameException duplicateUserNameException)
            {
                ExceptionService.HandleException(duplicateUserNameException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_DuplicateUserName);
            }
            // Catch license limit exception and show user exception message to user
            catch (LicenseLimitException licenseLimitException)
            {
                ExceptionService.HandleException(licenseLimitException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserCreateLicenseOver);
            }
            // Catch send mail exception and show user exception message to user
            catch (SendMailException sendMailException)
            {
                ExceptionService.HandleException(sendMailException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.s_UserCreate + " " + Resources.UserResource.e_SendMail);
            }
            // Catch data exception and show user exception message to user
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, _defaultPolicy);
                // do not change to resource, unique extn field error message comes from here
                ModelState.AddModelError("UserErrorMessage", dataException.Message);
            }
            catch (BusinessRuleException bex)
            {
                ExceptionService.HandleException(bex, _defaultPolicy);
                if (bex.Data != null)
                {
                    int count = 1;
                    foreach (DictionaryEntry er in bex.Data)
                    {
                        foreach (string error in (IEnumerable<string>)er.Value)
                        {
                            ModelState.AddModelError("Error" + (count++), error);
                        }
                    }
                }
            }

            // Get country details
            GetCountryDetails(userDetails.Address != null ? userDetails.Address.CountryId : null);

            ViewData["UserDetails"] = userDetails;
            return View();
        }

        /// <summary>
        ///This method is used to Checks for shared user.
        /// </summary>
        /// <param name="userDetails">user details.</param>
        private void CheckForSharedUser(UserDetails userDetails)
        {
            if (string.IsNullOrEmpty(userDetails.MembershipDetails.EmailId)
                || (!string.IsNullOrEmpty(userDetails.User.UserId) || !string.IsNullOrEmpty(userDetails.Identifier)))
            {
                return;
            }

            if (!TenantUserAssociationProxy.CanLinkWithOtherTenants(this.TenantId))
            {
                return;
            }

            UserDetails otherTenantUserDetails = null;

            try
            {
                otherTenantUserDetails = UserDetailsProxy.GetUserDetailsByEmailId(userDetails.MembershipDetails.EmailId);
            }
            catch (ArgumentNullException)
            {
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.e_ParameterEmptyOrNull);
                ViewData["IsLinkable"] = false;
                return;
            }
            catch (ArgumentException)
            {
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.e_ParameterEmptyOrNull);
                ViewData["IsLinkable"] = false;
                return;
            }
            catch (TenantUserAssociationException)
            {
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.e_SharingDisabled);
                ViewData["IsLinkable"] = false;
                return;
            }

            if (otherTenantUserDetails == null || otherTenantUserDetails.MembershipDetails == null)
            {
                return;
            }

            if (otherTenantUserDetails != null && otherTenantUserDetails.MembershipDetails != null && !string.IsNullOrEmpty(otherTenantUserDetails.MembershipDetails.TenantCode))
            {
                if (otherTenantUserDetails.MembershipDetails.TenantCode.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase))
                {
                    ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_DuplicateUser);
                    return;
                }
            }

            //gets all the tenantdetails that are linked to this user
            Dictionary<string, LinkedTenantUsers> linkedTenantList = TenantUserAssociationProxy.GetAllLinkableTenantDetails(otherTenantUserDetails.User.UserId);

            if (!linkedTenantList.ContainsKey(this.TenantId))
            {
                return;
            }

            var status = linkedTenantList[this.TenantId].RequestStatus;

            ViewData["IsLinkable"] = false;

            if (status == RequestStatus.WaitingForTenantApproval)
            {
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.e_WaitingForTenant);
            }
            else if (status == RequestStatus.WaitingForUserApproval)
            {
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.e_WaitingForUser);
            }
            else if (status == RequestStatus.Approved)
            {
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.e_DuplicateLinkUser);
            }
            else
            {
                ModelState.AddModelError("DuplicateEmailUserErrorMessage", Resources.UserResource.m_linkableUser);
                ViewData["IsLinkable"] = true;
            }
        }

        /// <summary>
        /// This method is used to validates the user mail.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="emailId">email identifier.</param>
        /// <returns>PartialViewResult.</returns>
        [HttpPost]
        public PartialViewResult ValidateUserMail(string userId, string emailId)
        {
            if (string.IsNullOrEmpty(emailId))
            {
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_emptyEmail);
                return PartialView("~/Views/TenantUserAssociation/UserRequest.ascx");
            }

            if (!string.IsNullOrEmpty(userId))
            {
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_Nochange);
                return PartialView("~/Views/TenantUserAssociation/UserRequest.ascx");
            }

            try
            {
                if (!TenantUserAssociationProxy.CanLinkWithOtherTenants(this.TenantId))
                {
                    return null;
                }

                var other_userDetails = UserDetailsProxy.GetUserDetailsByEmailId(emailId);

                if (other_userDetails == null || other_userDetails.MembershipDetails == null)
                {
                    return null;
                }

                //gets all the tenantdetails that are linked to this user
                Dictionary<string, LinkedTenantUsers> linkedTenantList = TenantUserAssociationProxy.GetAllLinkableTenantDetails(other_userDetails.User.UserId);

                if (!linkedTenantList.ContainsKey(this.TenantId))
                {
                    return null;
                }

                var status = linkedTenantList[this.TenantId].RequestStatus;
                if (status == RequestStatus.Open || status == RequestStatus.Rejected)
                {
                    LinkedTenantUsers linkedTenantUsers = new LinkedTenantUsers();
                    linkedTenantUsers.UserDetails = other_userDetails;
                    if (linkedTenantUsers.UserDetails.MembershipDetails.TenantCode.Equals(this.TenantId, StringComparison.OrdinalIgnoreCase))
                    {
                        ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_DuplicateUser);
                    }
                    else
                    {
                        ViewData["UserName"] = linkedTenantUsers.UserDetails.MembershipDetails.UserName;
                        ViewData["UserId"] = other_userDetails.User.UserId;
                        ViewData["ReqStatus"] = status;
                        ViewData["linkedId"] = linkedTenantList[this.TenantId].Id;

                        var rolesList = new RolesController().GetRolesForTenant(this.TenantId);

                        ViewData["RolesList"] = rolesList;

                        if (rolesList == null || rolesList.Count < 1)
                        {
                            return null;
                        }

                        return PartialView("~/Views/TenantUserAssociation/UserRequest.ascx");
                    }
                }
                else if (status == RequestStatus.WaitingForTenantApproval)
                {
                    ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_WaitingForTenant);
                }
                else if (status == RequestStatus.WaitingForUserApproval)
                {
                    ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_WaitingForUser);
                }
                else
                {
                    ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_DuplicateLinkUser);
                }
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ModelState.AddModelError("UserErrorMessage", unauthorizedAccessException.Message);
            }
            catch (TenantUserAssociationException exception)
            {
                ModelState.AddModelError("UserErrorMessage", exception.Message);
            }
            catch (UserDetailException exception)
            {
                ModelState.AddModelError("UserErrorMessage", exception.Message);
            }
            catch (FaultException exception)
            {
                ModelState.AddModelError("UserErrorMessage", exception.Message);
            }

            return PartialView("~/Views/TenantUserAssociation/UserRequest.ascx");
        }

        /// <summary>
        /// This method is used to deactivate the user.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>User details List view</returns>
        public JsonResult DeleteUser(string userId, string tenantId)
        {
            string error = string.Empty;
            tenantId = this.TenantId;

            try
            {
                // Check user id is valid or not.
                if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(tenantId))
                {
                    // check authorization
                    var canDeleteUser = TenantAccessProxy.CheckTenantAccess(new User().EntityIdentifier, tenantId, CelloSaaS.Model.FetchType.Delete, this.TenantId);
                    if (!canDeleteUser)
                    {
                        error = Resources.UserResource.e_DeactivateAccessDenied;
                    }
                    else
                    {
                        //Deactivate user
                        UserDetailsProxy.DeleteUser(userId);

                        return Json(new { Status = Resources.UserResource.s_UserDelete });
                    }
                }
                else
                {
                    error = Resources.UserResource.e_DeleteUserDetails;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                error = Resources.UserResource.e_AccessDenied;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                error = Resources.UserResource.e_AccessDenied;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                error = Resources.UserResource.e_AccessDenied;
            }
            catch (UserDetailException userDetailsException)
            {
                ExceptionService.HandleException(userDetailsException, _defaultPolicy);
                error = Resources.UserResource.e_UserDelete;
            }

            return Json(new { error = error });
        }

        /// <summary>
        /// This method is used to activates the user.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult ActivateUser(string userId, string tenantId)
        {
            string error = string.Empty;
            tenantId = this.TenantId;

            try
            {
                // Check user id is valid or not.
                if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(tenantId))
                {
                    // check authorization
                    var canActivateUser = TenantAccessProxy.CheckTenantAccess(new User().EntityIdentifier, tenantId, CelloSaaS.Model.FetchType.Delete, this.TenantId) || true;

                    if (!canActivateUser)
                    {
                        error = Resources.UserResource.e_ActivateAccessDenied;
                    }
                    else
                    {
                        //activate user
                        UserDetailsProxy.ActivateUser(userId);
                        return Json(new { Status = Resources.UserResource.s_UserActivate });
                    }
                }
                else
                {
                    error = Resources.UserResource.e_DeleteUserDetails;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                error = Resources.UserResource.e_UserDelete;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                error = Resources.UserResource.e_UserDelete;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                error = Resources.UserResource.e_AccessDenied;
            }
            catch (UserDetailException userDetailsException)
            {
                ExceptionService.HandleException(userDetailsException, _defaultPolicy);
                error = Resources.UserResource.e_UserDelete;
            }
            catch (LicenseLimitException licenseLimitException)
            {
                ExceptionService.HandleException(licenseLimitException, _defaultPolicy);
                error = Resources.UserResource.e_UserCreateLicenseOver;
            }

            return Json(new { error = error });
        }

        /// <summary>
        /// This method is used to activates the user.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        public JsonResult DisAssociateUser(string userId, string tenantId)
        {
            string error = string.Empty;
            tenantId = this.TenantId;

            try
            {
                // Check user id is valid or not.
                if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(tenantId))
                {
                    // check authorization
                    var canActivateUser = TenantAccessProxy.CheckTenantAccess(new User().EntityIdentifier, tenantId, CelloSaaS.Model.FetchType.Delete, this.TenantId) || true;

                    if (!canActivateUser)
                    {
                        error = Resources.UserResource.e_ActivateAccessDenied;
                    }
                    else
                    {
                        //activate user
                        var roleIds = RoleProxy.GetUserRolesForTenant(userId, tenantId);
                        if (roleIds == null || roleIds.Count() > 0)
                        {
                            RoleProxy.DeleteUserRoleAssignedByTenant(userId, roleIds, tenantId);
                            return Json(new { Status = Resources.UserResource.s_UserDelete });
                        }
                        else
                        {
                            error = Resources.AccountResource.e_UserRoles;
                        }
                    }
                }
                else
                {
                    error = Resources.UserResource.e_DeleteUserDetails;
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                error = Resources.UserResource.e_UserDelete;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                error = Resources.UserResource.e_UserDelete;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                error = Resources.UserResource.e_AccessDenied;
            }
            catch (UserDetailException userDetailsException)
            {
                ExceptionService.HandleException(userDetailsException, _defaultPolicy);
                error = Resources.UserResource.e_UserDelete;
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, _defaultPolicy);
                error = Resources.RolesResource.e_RoleDelete;
            }
            catch (LicenseLimitException licenseLimitException)
            {
                ExceptionService.HandleException(licenseLimitException, _defaultPolicy);
                error = Resources.UserResource.e_UserCreateLicenseOver;
            }

            return Json(new { error = error });
        }


        /// <summary>
        ///This method is used shows the Other tenant users details.
        /// </summary>
        /// <returns>ActionResult.</returns>
        public ActionResult OtherTenantUsersDetails()
        {
            string tenantId = UserIdentity.TenantID;
            try
            {
                if (TenantUserAssociationProxy.CanLinkWithOtherTenants(tenantId))
                {
                    OtherTenantUserDetails(tenantId);
                }
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }
            catch (TenantUserAssociationException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }
            catch (UserDetailException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }

            return View();

        }

        #endregion

        #region User Details Private Methods

        /// <summary>
        /// This method is used to gets the mapping service list.
        /// </summary>
        private static IList<Service> GetServiceList()
        {
            Dictionary<string, Service> servicesList = null;

            if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
            {
                servicesList = LicenseProxy.GetAllServices();
            }
            else
            {
                servicesList = LicenseProxy.GetServicesByUserId(UserIdentity.UserId);
            }

            return (servicesList != null) ? servicesList.Values.ToList() : null;
        }

        /// <summary>
        /// This method is used to validate the membership details.
        /// </summary>
        /// <param name="membershipdetails">membership details.</param>        
        /// <returns>True/False</returns>
        private void ValidateMembership(MembershipDetails membershipdetails)
        {
            // Check user name is null or empty.
            if (string.IsNullOrWhiteSpace(membershipdetails.UserName))
            {
                ModelState.AddModelError("UserDetails.MembershipDetails.UserName", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserNameMandatory);
            }

            // Check valid user name or not
            if (!string.IsNullOrWhiteSpace(membershipdetails.EmailId) && !Util.ValidateEmailId(membershipdetails.EmailId))
            {
                ModelState.AddModelError("UserDetails.MembershipDetails.EmailId", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_EmailId);
            }
        }

        /// <summary>
        /// This method is used to validate the user details.
        /// </summary>
        /// <param name="user">user details</param>
        /// <returns>True/False</returns>
        private void ValidateUserDetails(User user)
        {
            // Check user first name is null or empty
            if (string.IsNullOrWhiteSpace(user.FirstName))
            {
                ModelState.AddModelError("UserDetails.User.FirstName", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_FirstName);
            }

            // Check user first name is null or empty
            if (string.IsNullOrWhiteSpace(user.LastName))
            {
                ModelState.AddModelError("UserDetails.User.LastName", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_LastName);
            }
        }

        /// <summary>
        /// This method is used to validate the address details.
        /// </summary>
        /// <param name="user">user address details</param>
        /// <returns>True/False</returns>
        private void ValidateAddressDetails(UserDetails user)
        {
            var address = user.Address;

            // address can be optional
            if (string.IsNullOrWhiteSpace(address.Address1) && string.IsNullOrWhiteSpace(address.City)
                && string.IsNullOrWhiteSpace(address.State) && string.IsNullOrWhiteSpace(address.CountryId)
                && string.IsNullOrWhiteSpace(address.PostalCode))
            {
                user.Address = null;
                return;
            }

            if (string.IsNullOrWhiteSpace(address.Address1))
            {
                ModelState.AddModelError("UserDetails.Address.Address1", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_Address1);
            }
            if (string.IsNullOrWhiteSpace(address.City))
            {
                ModelState.AddModelError("UserDetails.Address.City", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_City);
            }
            if (string.IsNullOrWhiteSpace(address.State))
            {
                ModelState.AddModelError("UserDetails.Address.State", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_State);
            }
            if (string.IsNullOrWhiteSpace(address.CountryId))
            {
                ModelState.AddModelError("UserDetails.Address.CountryId", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_Country);
            }
            if (string.IsNullOrWhiteSpace(address.PostalCode))
            {
                ModelState.AddModelError("UserDetails.Address.PostalCode", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_Zipcode);
            }
            else if (!Util.ValidateAlphaNumericWithSpace(address.PostalCode))
            {
                ModelState.AddModelError("UserDetails.Address.PostalCode", "");
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_Zipcode_SpecialChars);
            }
        }

        /// <summary>
        /// This method is used to get country details.
        /// </summary>
        ///<param name="countryId">country identifier</param>
        private void GetCountryDetails(string countryId)
        {
            try
            {
                // Get all country details
                List<Country> lstcountry = CountryProxy.GetAllCountry();
                Country country = new Country();
                country.CountryId = "";
                country.Name = "--Select Country--";
                lstcountry.Insert(0, country);
                //Assign country as select list into viewdata
                ViewData["UserDetails.Address.CountryId"] = new SelectList((IEnumerable)lstcountry, "CountryId", "Name", countryId);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch master data exception and show exception message to user
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_AccessDenied);
            }
            // Catch master data exception and show exception message to user
            catch (MasterDataException masterDataException)
            {
                ExceptionService.HandleException(masterDataException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_CountryLoad);
            }
        }

        /// <summary>
        /// This method is used to get user details with pagination.
        /// </summary>
        /// <param name="pageNumber">page number.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="searchString">search string.</param>
        /// <param name="searchFilter">search filter.</param>
        /// <returns>User Details with Pagination</returns>
        private IPagination<UserDetails> GetUserDetailsList(
            int? pageNumber,
            int pageSize,
            string sortString,
            string sortDirection,
            string tenantId,
            string searchString,
            string searchFilter)
        {
            ViewData["searchString"] = searchString;
            ViewData["searchFilter"] = searchFilter;

            // check tenant parameter
            if (string.IsNullOrEmpty(tenantId))
            {
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserDetails);
                return null;
            }

            IPagination<UserDetails> userWithPagination = null;

            try
            {
                // get tenant details
                Tenant tenant = TenantProxy.GetTenantInfo(tenantId);

                if (tenant == null)
                {
                    ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_TenantNotFound);
                    return null;
                }

                bool canAddUser = ValidateAndSetUserManagementPrivileges(tenantId, tenant);

                // Create instance for search parameters
                SearchParameters searchParameters = new SearchParameters();

                // Create instance for user search condition
                UserDetailsSearchCondition userDetailsSearchCondition = new UserDetailsSearchCondition();

                // Set Tenant Id
                userDetailsSearchCondition.TenantId = tenantId;

                // search filter
                if (!string.IsNullOrEmpty(searchFilter) && !string.IsNullOrEmpty(searchString))
                {
                    if (searchFilter == "Email")
                    {
                        userDetailsSearchCondition.EmailId = searchString;
                    }
                    else if (searchFilter == "Username")
                    {
                        userDetailsSearchCondition.UserName = searchString;
                    }
                    else
                    {
                        userDetailsSearchCondition.SearchValue = searchString;
                    }
                }

                //If active user details needed then set status as true.
                userDetailsSearchCondition.Status = false;

                //Set Sort string
                userDetailsSearchCondition.SortString = sortString;

                //Set Sort Direction
                userDetailsSearchCondition.SortExpression = string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(SortExpressionConstants.Ascending) ? SortExpressionConstants.Ascending : SortExpressionConstants.Decending;

                //Set Total count is true to get user total records
                userDetailsSearchCondition.SetTotalCount = true;

                //Set starting record number
                userDetailsSearchCondition.RecordStart = (!pageNumber.HasValue || pageNumber.Equals(1) ? 1 : ((int)((pageNumber - 1) * pageSize) + 1));

                //Set ending record number
                userDetailsSearchCondition.RecordEnd = (!pageNumber.HasValue ? pageSize : ((int)pageNumber * pageSize));
                searchParameters.searchCondition = userDetailsSearchCondition;

                // Get user details based on the search condition
                Dictionary<string, UserDetails> baseTenantUsers = UserDetailsProxy.SearchUserDetails(searchParameters);

                if (baseTenantUsers != null && baseTenantUsers.Count > 0)
                {
                    // Convert user details list into IPagination list
                    userWithPagination = baseTenantUsers.Values.ToList().AsPagination(1, pageSize);
                    ViewData["UserDetails"] = userWithPagination.ToList();
                }

                int searchUserCount = 0, totalUserCount = 0;

                Dictionary<bool, int> totCount = UserDetailsProxy.GetUserCountByTenantId(tenantId);
                foreach (var item in totCount)
                {
                    totalUserCount += item.Value;
                }

                if (searchParameters.SearchSummary != null)
                {
                    searchUserCount = searchParameters.SearchSummary.TotalCount;
                }
                else
                {
                    searchUserCount = totalUserCount;
                }

                ViewData["PageNumber"] = !pageNumber.HasValue ? 1 : (int)pageNumber;
                ViewData["SortString"] = string.IsNullOrEmpty(sortString) ? UserDetailsSortStringConstants.FirstName : sortString;
                ViewData["SortDirection"] = string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(SortExpressionConstants.Ascending)
                                            ? SortExpressionConstants.Ascending
                                            : SortExpressionConstants.Decending;

                ViewData["TotalCount"] = searchUserCount;
                ViewData["PageSize"] = pageSize;
                ViewData["TenantId"] = tenantId;
                ViewData["TenantName"] = tenant.TenantDetails.TenantName;
                ViewData["privilege"] = PrivilegeConstants.ViewUser;

                // Get License details for tenant
                TenantLicense tenantLisence = LicenseProxy.GetTenantLicense(tenantId);

                int? licenseUserCount = 0;

                //If license details is not null then get number of users for tenant
                if (tenantLisence != null)
                {
                    licenseUserCount = tenantLisence.NumberOfUsers;
                }

                // Check if user count is less than license user count then add new user else throw exception
                if (canAddUser && (licenseUserCount == null || totalUserCount < licenseUserCount))
                {
                    ViewData["CreateUser"] = true;
                }

                //User Approval
                ViewData["CanApproveUser"] = ConfigHelper.EnableExternalAuthentication && (UserIdentity.IsInRole(RoleConstants.ProductAdmin) || UserIdentity.HasPrivilege(PrivilegeConstants.ApproveUser) && userWithPagination.ToList().Any(u => !u.MembershipDetails.IsApproved));

                //User Invite Link
                ViewData["UserInvite"] = userWithPagination.ToList().Any(u => !u.MembershipDetails.IsApproved) && ConfigHelper.EnableUserInvitation;
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch argument exception and show exception message to user
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            // Catch UnauthorizedAccess exception and show user exception message to user
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_AccessDenied);
            }
            // Catch user detail exception and show user exception message to user
            catch (UserDetailException userException)
            {
                ExceptionService.HandleException(userException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserLoad);
            }
            // Catch user detail exception and show user exception message to user
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_UserLoad);
            }
            // Catch user detail exception and show user exception message to user
            catch (TenantException tenantException)
            {
                ExceptionService.HandleException(tenantException, _defaultPolicy);
                ModelState.AddModelError("UserErrorMessage", Resources.UserResource.e_FetchTenantDetails);
            }
            return userWithPagination;
        }

        /// <summary>
        /// Validates the and set user management privileges.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        /// <param name="tenant">tenant.</param>
        /// <returns></returns>
        private bool ValidateAndSetUserManagementPrivileges(string tenantId, Tenant tenant)
        {
            bool canAddUser = false, canResetPINUser = false;

            bool canViewUser = false, canEditUser = false, canDeleteUser = false, canManageUserRoles = false;
            bool canLockUser = false, canUnLockUser = false, canForcePasswordReset = false;

            if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
            {
                // check access privileges
                string[] privileges = new string[] { 
                        PrivilegeConstants.ViewUser, 
                        PrivilegeConstants.AddUser, 
                        PrivilegeConstants.UpdateUser, 
                        PrivilegeConstants.DeleteUser,
                        PrivilegeConstants.AddUserRole,
                        PrivilegeConstants.LockUser,
                        PrivilegeConstants.UnLockUser,
                        PrivilegeConstants.ResetPINUser,
                        PrivilegeConstants.ForcePasswordExpiry
                    };

                Dictionary<string, Dictionary<string, TenantDetails>> accessPrivileges = AccessControlProxy.GetTenantAccessPrivileges(privileges);
                var editScopeAccess = TenantAccessProxy.CheckTenantAccess(new User().EntityIdentifier, TenantContext.GetTenantId(new User().EntityIdentifier), CelloSaaS.Model.FetchType.Edit, this.TenantId);
                var deleteScopeAccess = TenantAccessProxy.CheckTenantAccess(new User().EntityIdentifier, TenantContext.GetTenantId(new User().EntityIdentifier), CelloSaaS.Model.FetchType.Delete, this.TenantId);

                if (accessPrivileges != null && accessPrivileges.Count > 0)
                {
                    if (accessPrivileges.ContainsKey(PrivilegeConstants.ViewUser))
                    {
                        canViewUser = accessPrivileges[PrivilegeConstants.ViewUser].ContainsKey(tenantId);
                    }
                    if (editScopeAccess)
                    {
                        if (accessPrivileges.ContainsKey(PrivilegeConstants.AddUser))
                        {
                            canAddUser = accessPrivileges[PrivilegeConstants.AddUser].ContainsKey(tenantId);
                        }
                        if (accessPrivileges.ContainsKey(PrivilegeConstants.UpdateUser))
                        {
                            canEditUser = accessPrivileges[PrivilegeConstants.UpdateUser].ContainsKey(tenantId);
                        }

                        if (accessPrivileges.ContainsKey(PrivilegeConstants.AddUserRole))
                        {
                            canManageUserRoles = accessPrivileges[PrivilegeConstants.AddUserRole].ContainsKey(tenantId);
                        }
                        if (accessPrivileges.ContainsKey(PrivilegeConstants.LockUser))
                        {
                            canLockUser = accessPrivileges[PrivilegeConstants.LockUser].ContainsKey(tenantId);
                        }
                        if (accessPrivileges.ContainsKey(PrivilegeConstants.UnLockUser))
                        {
                            canUnLockUser = accessPrivileges[PrivilegeConstants.UnLockUser].ContainsKey(tenantId);
                        }
                        if (accessPrivileges.ContainsKey(PrivilegeConstants.ResetPINUser))
                        {
                            canResetPINUser = accessPrivileges[PrivilegeConstants.ResetPINUser].ContainsKey(tenantId);
                        }
                        if (accessPrivileges.ContainsKey(PrivilegeConstants.ForcePasswordExpiry))
                        {
                            canForcePasswordReset = accessPrivileges[PrivilegeConstants.ForcePasswordExpiry].ContainsKey(tenantId);
                        }
                    }
                    if (deleteScopeAccess)
                    {
                        if (accessPrivileges.ContainsKey(PrivilegeConstants.DeleteUser))
                        {
                            canDeleteUser = accessPrivileges[PrivilegeConstants.DeleteUser].ContainsKey(tenantId);
                        }
                    }
                }
            }
            else
            {
                canViewUser = UserIdentity.HasPrivilege(PrivilegeConstants.ViewUser);
                canEditUser = UserIdentity.HasPrivilege(PrivilegeConstants.UpdateUser);
                canDeleteUser = UserIdentity.HasPrivilege(PrivilegeConstants.DeleteUser);
                canLockUser = UserIdentity.HasPrivilege(PrivilegeConstants.LockUser);
                canUnLockUser = UserIdentity.HasPrivilege(PrivilegeConstants.UnLockUser);
                canResetPINUser = UserIdentity.HasPrivilege(PrivilegeConstants.ResetPINUser);
                canAddUser = UserIdentity.HasPrivilege(PrivilegeConstants.AddUser);
                canManageUserRoles = UserIdentity.HasPrivilege(PrivilegeConstants.AddUserRole);
                canForcePasswordReset = UserIdentity.HasPrivilege(PrivilegeConstants.ForcePasswordExpiry);
            }

            var tenantSettings = UserIdentity.TenantSettings;

            if (tenantSettings == null || tenantSettings.Count < 1)
            {
                var settings = TenantSettingsProxy.GetTenantSettings(this.TenantId);

                if (settings != null && settings.Setting != null)
                    tenantSettings = settings.Setting.Attributes;
            }

            if (tenantSettings != null && tenantSettings.Count > 0 && tenantSettings.ContainsKey(AttributeConstants.IdentityProvider)
                && !string.IsNullOrEmpty(tenantSettings[AttributeConstants.IdentityProvider])
                && !tenantSettings[AttributeConstants.IdentityProvider].Equals(ConfigHelper.CelloOpenIdProvider, StringComparison.OrdinalIgnoreCase))
            {
                canLockUser = false;
                canUnLockUser = false;
                canForcePasswordReset = false;
                canResetPINUser = false;
            }

            ViewData["CanViewUser"] = canViewUser;
            ViewData["CanEditUser"] = canEditUser;
            ViewData["CanDeleteUser"] = canDeleteUser;
            ViewData["CanLockUser"] = canLockUser;
            ViewData["CanUnLockUser"] = canUnLockUser;
            ViewData["CanManageRoles"] = canManageUserRoles;
            ViewData["CanForcePassword"] = canForcePasswordReset;
            ViewData["CanResetPINUser"] = canResetPINUser;

            return canAddUser;
        }

        /// <summary>
        /// To force the password resetting for the users under a given tenant.
        /// </summary>
        /// <param name="userIds">list of user identifiers.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns>
        /// true if forced password reset is successful.
        /// false if forced password reset is NOT successful.
        /// </returns>
        public JsonResult ForcePasswordReset(string[] userIds, string tenantId)
        {
            if (Request.IsAjaxRequest() && userIds != null && userIds.Count() > 0)
            {
                tenantId = string.IsNullOrEmpty(tenantId) ? this.TenantId : tenantId;
                try
                {
                    if (UserIdentity.Privileges.Contains(PrivilegeConstants.ForcePasswordExpiry))
                    {
                        UserDetailsProxy.ForceUserPasswordReset(userIds);
                        return Json(new { Status = Resources.UserResource.s_PasswordForceSuccess });
                    }
                    return this.Json(new { error = Resources.UserResource.e_PasswordForceAccessDenied });
                }
                catch (ArgumentNullException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (ArgumentException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
                catch (UserDetailException ex)
                {
                    ExceptionService.HandleException(ex, _defaultPolicy);
                }
            }

            return Json(new { error = Resources.UserResource.e_PasswordForceFail });
        }

        #endregion

        #region Other Tenant User Details Private Method

        /// <summary>
        /// This method is used to get the other tenant user details based on the given tenant identifier.
        /// </summary>
        /// <param name="tenantId">tenant identifier.</param>
        private void OtherTenantUserDetails(string tenantId)
        {
            try
            {
                ViewData["IsLinked"] = "True";
                Dictionary<string, LinkedTenantUsers> otherTenantUsersList = TenantUserAssociationProxy.GetLinkedTenantUsers(tenantId);
                if (otherTenantUsersList != null && otherTenantUsersList.Count > 0)
                {
                    ViewData["OtherTenantUser"] = otherTenantUsersList.Values.ToList();
                }
            }
            catch (ArgumentNullException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }
            catch (ArgumentException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }
            catch (TenantUserAssociationException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }
            catch (UserDetailException ex)
            {
                ExceptionService.HandleException(ex, _defaultPolicy);
            }
        }

        #endregion
    }
}
