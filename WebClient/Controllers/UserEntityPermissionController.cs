using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using System.Linq;
using CelloSaaS.Library;
using CelloSaaS.View;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceProxies.UserManagement;
using CelloSaaS.ServiceProxies.DataManagement;
using CelloSaaS.Library.Helpers;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.LicenseManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.ServiceContracts.DataManagement;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for user entity permission.
    /// </summary>
    public class UserEntityPermissionController : CelloController
    {
        #region UserEntityPermission

        private const string DefaultPolicy = "GlobalExceptionLogger";

        /// <summary>
        /// This method is used to get the manage user entity permission view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This method is used to get the user entity permission form view.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <returns></returns>
        public PartialViewResult UserEntityPermissionForm(string userId, string entityId, string referenceId, string roleId)
        {
            try
            {
                LoadUserList(userId);
                LoadRoleList(roleId);
                LoadEntityList(entityId);
                LoadReferenceList(entityId, referenceId);

                if (!string.IsNullOrEmpty(entityId) && !string.IsNullOrEmpty(referenceId))
                {
                    UserEntityPermission userEntityPermission =
                        UserEntityPermissionProxy.SearchUserEntityPermissionDetails(
                            new UserEntityPermissionSearchCondition()
                                {
                                    UserId = userId,
                                    RoleId = roleId,
                                    EntityId = entityId,
                                    ReferenceId = referenceId,
                                    Status = true,
                                    TenantId = UserIdentity.TenantID
                                }).Values.Where(uep => !string.IsNullOrEmpty(uep.UserId) ? uep.UserId == userId : true && !string.IsNullOrEmpty(uep.RoleId) ? uep.RoleId == roleId : true && uep.EntityId == entityId && uep.ReferenceId == referenceId).FirstOrDefault();

                    //userEntityPermission =
                    //    UserEntityPermissionProxy.SearchUserEntityPermissionDetails(
                    //        new UserEntityPermissionSearchCondition()
                    //        {
                    //            UserId = userId,
                    //            RoleId = roleId,
                    //            EntityId = entityId,
                    //            ReferenceId = referenceId,
                    //            Status = true,
                    //            TenantId = UserIdentity.TenantID
                    //        }).Values.FirstOrDefault();


                    if (userEntityPermission != null)
                    {
                        ViewData["UserEntityPermissionId"] = userEntityPermission.Id;
                        ViewData["AddPermission"] = userEntityPermission.AddPermission;
                        ViewData["ViewPermission"] = userEntityPermission.ViewPermission;
                        ViewData["EditPermission"] = userEntityPermission.EditPermission;
                        ViewData["DeletePermission"] = userEntityPermission.DeletePermission;
                        //ViewData["OtherPrivileges"] = userEntityPermission.UserEntityOtherPrivileges
                    }
                    else
                    {
                        ViewData["UserEntityPermissionId"] = string.Empty;
                        ViewData["AddPermission"] = false;
                        ViewData["ViewPermission"] = false;
                        ViewData["EditPermission"] = false;
                        ViewData["DeletePermission"] = false;
                        ViewData["OtherPrivileges"] = null;
                    }
                }
            }
            catch (UnauthorizedAccessException unExp)
            {
                ExceptionService.HandleException(unExp, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", unExp.Message.ToString(CultureInfo.InvariantCulture));
            }
            catch (ArgumentNullException argNullExp)
            {
                ExceptionService.HandleException(argNullExp, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", argNullExp.Message.ToString(CultureInfo.InvariantCulture));
            }
            catch (ArgumentException argExp)
            {
                ExceptionService.HandleException(argExp, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", argExp.Message.ToString(CultureInfo.InvariantCulture));
            }
            catch (UserEntityPermissionException userEntityPermissionException)
            {
                ExceptionService.HandleException(userEntityPermissionException, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", userEntityPermissionException.Message.ToString(CultureInfo.InvariantCulture));
            }
            return PartialView();
        }

        /// <summary>
        /// This method is used to get the user entity permission form based on the given form collection.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UserEntityPermissionForm(FormCollection formCollection)
        {
            var userEntityPermission = new UserEntityPermission
                                                            {
                                                                UserId = formCollection["UserList"].ToString(CultureInfo.InvariantCulture),
                                                                RoleId = formCollection["RoleList"].ToString(CultureInfo.InvariantCulture),
                                                                EntityId = formCollection["EntityList"].ToString(CultureInfo.InvariantCulture),
                                                                ReferenceId = formCollection["ReferenceList"].ToString(CultureInfo.InvariantCulture),
                                                                ViewPermission = formCollection["ViewPermission"] != null && (formCollection["ViewPermission"].ToString(CultureInfo.InvariantCulture) == "on" ? true : false),
                                                                AddPermission = formCollection["AddPermission"] != null && (formCollection["AddPermission"].ToString(CultureInfo.InvariantCulture) == "on" ? true : false),
                                                                EditPermission = formCollection["EditPermission"] != null && (formCollection["EditPermission"].ToString(CultureInfo.InvariantCulture) == "on" ? true : false),
                                                                DeletePermission = formCollection["DeletePermission"] != null && (formCollection["DeletePermission"].ToString(CultureInfo.InvariantCulture) == "on" ? true : false),
                                                                TenantId = UserIdentity.TenantID,
                                                                Status = true
                                                            };
            if (!string.IsNullOrEmpty(formCollection["Privileges"]))
            {
                List<string> privileges = formCollection["Privileges"].Split(',').ToList();

                var userEntityOtherPrivileges = new List<UserEntityOtherPrivilege>();

                foreach (string item in privileges)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        UserEntityOtherPrivilege ueop = new UserEntityOtherPrivilege { Privilege = item };
                        userEntityOtherPrivileges.Add(ueop);
                    }
                }
                userEntityPermission.UserEntityOtherPrivileges = userEntityOtherPrivileges;
            }
            else
            {
                userEntityPermission.UserEntityOtherPrivileges = new List<UserEntityOtherPrivilege>();
            }
            if (string.IsNullOrEmpty(formCollection["UserEntityPermissionId"].ToString(CultureInfo.InvariantCulture)))
            {
                userEntityPermission.CreatedBy = UserIdentity.UserId;
                userEntityPermission.CreatedOn = DateTimeHelper.GetDatabaseDateTime();
                UserEntityPermissionProxy.AddUserEntityPermission(userEntityPermission);
            }
            else
            {
                userEntityPermission.Id = formCollection["UserEntityPermissionId"].ToString(CultureInfo.InvariantCulture);
                userEntityPermission.UpdatedBy = UserIdentity.UserId;
                userEntityPermission.UpdatedOn = DateTimeHelper.GetDatabaseDateTime();
                UserEntityPermissionProxy.UpdateUserEntityPermission(userEntityPermission);
            }

            return RedirectToAction("UserEntityPermissionForm", new { userId = userEntityPermission.UserId, entityId = userEntityPermission.EntityId, referenceId = userEntityPermission.ReferenceId, roleId = userEntityPermission.RoleId });
        }

        /// <summary>
        /// This method is used to get the entity permission..
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <returns></returns>
        public JsonResult Load(string userId, string entityId, string referenceId, string roleId)
        {
            string userEntityPermissionId = string.Empty;
            bool addPermission = false;
            bool viewPermission = false;
            bool editPermission = false;
            bool deletePermission = false;
            List<UserEntityOtherPrivilege> UserEntityOtherPrivileges = null;
            try
            {
                if (!string.IsNullOrEmpty(entityId) && !string.IsNullOrEmpty(referenceId))
                {
                    userId = string.IsNullOrEmpty(userId) ? null : userId;
                    entityId = string.IsNullOrEmpty(entityId) ? null : entityId;
                    referenceId = string.IsNullOrEmpty(referenceId) ? null : referenceId;
                    roleId = string.IsNullOrEmpty(roleId) ? null : roleId;

                    UserEntityPermission userEntityPermission =
                        UserEntityPermissionProxy.SearchUserEntityPermissionDetails(
                            new UserEntityPermissionSearchCondition()
                            {
                                UserId = userId,
                                RoleId = roleId,
                                EntityId = entityId,
                                ReferenceId = referenceId,
                                Status = true,
                                TenantId = UserIdentity.TenantID
                            }).Values.Where(uep => uep.UserId == userId && uep.RoleId == roleId && uep.EntityId == entityId && uep.ReferenceId == referenceId).FirstOrDefault();

                    if (userEntityPermission != null)
                    {
                        userEntityPermissionId = userEntityPermission.Id;
                        addPermission = Convert.ToBoolean(userEntityPermission.AddPermission, CultureInfo.InvariantCulture);
                        viewPermission = Convert.ToBoolean(userEntityPermission.ViewPermission, CultureInfo.InvariantCulture);
                        editPermission = Convert.ToBoolean(userEntityPermission.EditPermission, CultureInfo.InvariantCulture);
                        deletePermission = Convert.ToBoolean(userEntityPermission.DeletePermission, CultureInfo.InvariantCulture);
                        UserEntityOtherPrivileges = userEntityPermission.UserEntityOtherPrivileges;
                    }
                }
            }
            catch (UnauthorizedAccessException unExp)
            {
                ExceptionService.HandleException(unExp, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", unExp.Message.ToString(CultureInfo.InvariantCulture));
            }
            catch (ArgumentNullException argNullExp)
            {
                ExceptionService.HandleException(argNullExp, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", argNullExp.Message.ToString(CultureInfo.InvariantCulture));
            }
            catch (ArgumentException argExp)
            {
                ExceptionService.HandleException(argExp, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", argExp.Message.ToString(CultureInfo.InvariantCulture));
            }
            catch (UserEntityPermissionException userEntityPermissionException)
            {
                ExceptionService.HandleException(userEntityPermissionException, DefaultPolicy);
                ModelState.AddModelError("UserEntityPermissionErrorMessage", userEntityPermissionException.Message.ToString(CultureInfo.InvariantCulture));
            }
            return Json(new { userId = userId, entityId = entityId, referenceId = referenceId, roleId = roleId, userEntityPermissionId = userEntityPermissionId, addPermission = addPermission, viewPermission = viewPermission, editPermission = editPermission, deletePermission = deletePermission, UserEntityOtherPrivileges = UserEntityOtherPrivileges });
        }

        /// <summary>
        /// This method is used to load user list based on the given user identifier.
        /// </summary>
        /// <param name="selectedUserId">user identifier.</param>
        private void LoadUserList(string selectedUserId)
        {
            var userList = new Dictionary<string, string>();

            var userDetails = UserDetailsProxy.GetAllUserDetailsByTenantId(UserIdentity.TenantID).Values.ToList().OrderBy(u => u.User.FirstName);

            userList.Add("", "Select User");

            foreach (var userDetail in userDetails)
            {
                userList.Add(userDetail.Identifier, userDetail.User.FirstName + " " + userDetail.User.LastName);
            }

            ViewData["UserList"] = !string.IsNullOrEmpty(selectedUserId)
                                        ? new SelectList(userList, "Key", "Value", selectedUserId)
                                        : new SelectList(userList, "Key", "Value", null);

        }

        /// <summary>
        /// This method is used to load role list based on the given role identifier.
        /// </summary>
        /// <param name="selectedRoleId">role identifier.</param>
        private void LoadRoleList(string selectedRoleId)
        {
            var roleList = new Dictionary<string, string>();

            var roleDetails = RoleList();

            roleList.Add("", "Select Role");

            foreach (var role in roleDetails)
            {
                roleList.Add(role.RoleId, role.RoleName);
            }

            ViewData["RoleList"] = !string.IsNullOrEmpty(selectedRoleId)
                                        ? new SelectList(roleList, "Key", "Value", selectedRoleId)
                                        : new SelectList(roleList, "Key", "Value", null);
            
        }

        /// <summary>
        /// This method is used to get the collection of role list.
        /// </summary>
        /// <returns>collection of role list.</returns>
        private List<Role> RoleList()
        {
            List<Role> roleGlobalRole = null;
            try
            {
                // check the logged in user is a service admin
                if (UserIdentity.IsInRole(RoleConstants.ServiceAdmin))
                {
                    // get his services
                    Dictionary<string, Service> userServices = LicenseProxy.GetServicesByUserId(UserIdentity.UserId);

                    if (userServices != null && userServices.Count > 0)
                    {
                        // get roles mapped to his services
                        Dictionary<string, Role> roleList = RoleProxy.GetRolesByServiceIds(userServices.Keys.ToArray());

                        // remove the product & service admin roles
                        if (roleList != null && roleList.Count > 0)
                        {
                            roleList.Remove(RoleConstants.ProductAdmin);
                            roleList.Remove(RoleConstants.ServiceAdmin);

                            roleGlobalRole = roleList.Values.ToList();
                        }
                        return roleGlobalRole;
                    }
                }

                // Get global role details
                roleGlobalRole = RoleProxy.GetGlobalRolesByTenant(UserIdentity.TenantID);

                // Remove product admin role
                if (roleGlobalRole != null && roleGlobalRole.Count > 0)
                {
                    if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                        roleGlobalRole.RemoveAll(r => r.RoleId.Equals(RoleConstants.ServiceAdmin, StringComparison.Ordinal));

                    // Get tenant role details
                    List<Role> roleTenantRole = RoleProxy.GetAllTenantRoleDetails(UserIdentity.TenantID);

                    if (roleTenantRole != null && roleTenantRole.Count > 0)
                    {
                        roleTenantRole.RemoveAll(r => r.RoleId.Equals(RoleConstants.ProductAdmin, StringComparison.Ordinal));
                        roleGlobalRole.AddRange(roleTenantRole);
                    }
                }
                return roleGlobalRole;
            }
            catch (UnauthorizedAccessException unauthorizedException)
            {
                ExceptionService.HandleException(unauthorizedException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_AccessDenied);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_ParameterEmptyOrNull);
            }
            catch (RoleException roleException)
            {
                ExceptionService.HandleException(roleException, DefaultPolicy);
                ModelState.AddModelError("RoleListMessage", Resources.RolesResource.e_RoleLoad);
            }
            return roleGlobalRole;
        }

        /// <summary>
        /// This method is used to load entity list based on the given entity identifier.
        /// </summary>
        /// <param name="selectedEntityId">entity identifier.</param>
        private void LoadEntityList(string selectedEntityId)
        {
            var entityList = new Dictionary<string, string>();

            var entityDetails = DataManagementProxy.GetAllEntityMetaData(UserIdentity.TenantID);
            entityList.Add("", "Select Entity");
            if (entityDetails != null)
            {
                foreach (var entity in celloEntities)
                {
                    entityDetails.Remove(entity);
                }

                foreach (var entityMetaData in entityDetails)
                {
                    entityList.Add(entityMetaData.Value.EntityIdentifier, entityMetaData.Value.EntityIdentifier);
                }
            }
            ViewData["EntityList"] = !string.IsNullOrEmpty(selectedEntityId)
                                         ? new SelectList(entityList, "Key", "Value", selectedEntityId)
                                         : new SelectList(entityList, "Key", "Value", null);
        }

        /// <summary>
        /// This method is used to load reference list.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="selectedReferenceId">reference identifier.</param>
        private void LoadReferenceList(string entityId, string selectedReferenceId)
        {
            var referenceIds = new Dictionary<string, string>();
            try
            {
                referenceIds.Add("", "Select Reference");

                if (!string.IsNullOrEmpty(entityId))
                {
                    var referenceIdList = DataManagementProxy.GetEntityKeyValuePair(entityId, UserIdentity.TenantID).OrderBy(r => r.Value);
                    foreach (var reference in referenceIdList)
                    {
                        referenceIds.Add(reference.Key, reference.Value);
                    }
                }
            }
            catch (DataManagementException exp)
            {
                ModelState.AddModelError("Error", exp.Message);
            }
            catch (Exception exp)
            {
                ModelState.AddModelError("Error", exp.Message);
            }

            ViewData["ReferenceList"] = !string.IsNullOrEmpty(selectedReferenceId)
                                        ? new SelectList(referenceIds, "Key", "Value", selectedReferenceId)
                                        : new SelectList(referenceIds, "Key", "Value", null);
        }

        /// <summary>
        /// This method is used to get user reference details based on the given entity identifier.
        /// </summary>
        /// <param name="entityId">entity identifier</param>
        /// <returns></returns>
        public JsonResult GetUserReferenceDetails(string entityId)
        {
            Dictionary<string, string> referenceIds = null;
            try
            {
                if (!string.IsNullOrEmpty(entityId))
                {
                    referenceIds = DataManagementProxy.GetEntityKeyValuePair(entityId, UserIdentity.TenantID).OrderBy(r => r.Value).ToDictionary(d => d.Key, d => d.Value);
                }
            }
            catch (DataManagementException exp)
            {
                ModelState.AddModelError("Error", exp.Message);
            }

            return Json(referenceIds);
        }

        /// <summary>
        /// This method is used to get the user entity permission details.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <returns></returns>
        public JsonResult GetUserEntityPermissionDetails(string userId, string entityId, string referenceId, string roleId)
        {
            Dictionary<string, UserEntityPermission> userEntityPermission = null;
            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(entityId) && !string.IsNullOrEmpty(referenceId))
            {
                var userEntityPermissionSearchCondition = new UserEntityPermissionSearchCondition
                {
                    UserId = userId,
                    RoleId = roleId,
                    EntityId = entityId,
                    ReferenceId = referenceId,
                    Status = true
                };
                userEntityPermission = UserEntityPermissionProxy.SearchUserEntityPermissionDetails(userEntityPermissionSearchCondition);
                ViewData["UserEntityPermission"] = userEntityPermission;
            }
            return Json(userEntityPermission);
        }
        
        /// <summary>
        /// This method is used to get the user entity permission details.
        /// </summary>
        /// <param name="userId">user identifier.</param>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <param name="roleId">role identifier.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="page">page.</param>
        /// <param name="pageSize">page size.</param>
        /// <returns></returns>
        public ActionResult UserEntityPermissionDetails(string userId, string entityId, string referenceId, string roleId, string sortString, string sortDirection, int page = 1, int pageSize = 10)
        {
            ViewData["referenceId"] = referenceId;
            ViewData["UserIdentifier"] = userId;
            ViewData["RoleIdentifier"] = roleId;
            ViewData["EntityId"] = entityId;

            ViewData["SortString"] = sortString ?? string.Empty;
            ViewData["SortDirection"] = sortDirection ?? string.Empty;
            ViewData["PageNumber"] = page;
            ViewData["TotalCount"] = 0;
            ViewData["PageSize"] = pageSize;
            try
            {
                var userEntityPermissionSearchCondition =
                    new UserEntityPermissionSearchCondition()
                        {
                            UserId = userId,
                            RoleId = roleId,
                            EntityId = entityId,
                            ReferenceId = referenceId,
                            Status = true,
                            RecordStart = ((page - 1) * pageSize) + 1,
                            RecordEnd = page * pageSize,
                            SetTotalCount = true,
                            TenantId = UserIdentity.TenantID
                        };

                var userEntityPermission = UserEntityPermissionProxy.SearchUserEntityPermissionDetails(userEntityPermissionSearchCondition);

                var userEntityPermissionCount = UserEntityPermissionProxy.SearchUserEntityPermissionDetailsCount(userEntityPermissionSearchCondition);
                ViewData["TotalCount"] = userEntityPermissionCount;
                if (!string.IsNullOrEmpty(userId))
                    ViewData["UserEntityPermission"] = userEntityPermission = userEntityPermission.OrderBy(uep => uep.Value.UserId).ToDictionary(x => x.Key, x => x.Value);
                else if (!string.IsNullOrEmpty(roleId))
                    ViewData["UserEntityPermission"] = userEntityPermission = userEntityPermission.OrderBy(uep => uep.Value.RoleId).ToDictionary(x => x.Key, x => x.Value);
                else
                    ViewData["UserEntityPermission"] = userEntityPermission;

                ViewData["UserDetailsList"] = UserDetailsProxy.GetAllUserDetailsByTenantId(UserIdentity.TenantID);
                var roleList = RoleProxy.GetAllTenantRoleDetails(UserIdentity.TenantID);
                ViewData["RoleDetailsList"] = null;
                if (roleList != null)
                {
                    var roleDetailsList = roleList.ToDictionary(role => role.RoleId, role => role.RoleName);
                    ViewData["RoleDetailsList"] = roleDetailsList;
                }

                if (!string.IsNullOrEmpty(entityId))
                {
                    ViewData["ReferenceListDetails"] = DataManagementProxy.GetEntityKeyValuePair(entityId, UserIdentity.TenantID);
                }
            }
            catch (DataManagementException exp)
            {
                ModelState.AddModelError("Error", exp.Message);
            }

            return PartialView();
        }
        #endregion

        public string[] celloEntities = new string[] { "Tenant", "User", "Address", "ContactDetails" };
    }
}
