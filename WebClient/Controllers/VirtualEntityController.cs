﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using CelloSaaS.Library;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.Model.VirtualEntityManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.DataManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.ViewManagement;
using CelloSaaS.ServiceContracts.VirtualEntityManagement;
using CelloSaaS.ServiceProxies.DataManagement;
using CelloSaaS.ServiceProxies.ViewManagement;
using CelloSaaS.ServiceProxies.VirtualEntityManagement;
using CelloSaaS.View;
using CelloSaaS.Rules.Core;
using System.Collections;
using System.Text;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for virtual entity.
    /// </summary>
    public class VirtualEntityController : CelloController
    {
        private string TenantId = UserIdentity.TenantID;
        private const string DefaultPolicy = "GlobalExceptionLogger";

        /// <summary>
        /// This method is used to loads the entity details.
        /// </summary>
        private void LoadEntityDetails()
        {
            try
            {
                //Get all entity meta data
                var dicEntityMetaData = DataManagementProxy.GetAllEntityMetaData();
                if (dicEntityMetaData != null && dicEntityMetaData.Count > 0)
                {
                    Dictionary<string, string> virtualEntity = new Dictionary<string, string>();
                    int count = 0;
                    foreach (KeyValuePair<string, EntityMetaData> metaData in dicEntityMetaData)
                    {
                        if (!string.IsNullOrEmpty(metaData.Value.TenantId))
                        {
                            IAccessControlService accessControlService = (IAccessControlService)ServiceLocator.GetServiceImplementation(typeof(IAccessControlService));
                            if (accessControlService.CheckPrivilege(EntityPrivilegePrefix.ViewPrefix + metaData.Value.EntityIdentifier))
                            {
                                count = count + 1;
                                virtualEntity.Add(metaData.Value.EntityIdentifier, metaData.Value.EntityName);
                            }
                        }
                    }
                    if (count == 0)
                    {
                        ViewData["EntityList"] = null;
                    }
                    else
                    {
                        ViewData["EntityList"] = virtualEntity;
                    }
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (LicenseException licenseException)
            {
                ExceptionService.HandleException(licenseException, DefaultPolicy);
                ModelState.AddModelError("Error", licenseException.Message);
            }
        }

        /// <summary>
        /// This method is sued to gets the virtual entity data views.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="entityName">name of the entity.</param>
        /// <returns></returns>     
        public ActionResult GetVirtualEntityDataViews(string entityId, string entityName)
        {
            //ModelState.Clear();
            if (TempData["argumentNullException"] != null && !string.IsNullOrEmpty(TempData["argumentNullException"].ToString()))
            {
                ModelState.AddModelError("Error", TempData["argumentNullException"].ToString());
            }
            if (TempData["argumentException"] != null && !string.IsNullOrEmpty(TempData["argumentException"].ToString()))
            {
                ModelState.AddModelError("Error", TempData["argumentException"].ToString());
            }
            if (TempData["virtualEntityException"] != null && !string.IsNullOrEmpty(TempData["virtualEntityException"].ToString()))
            {
                ModelState.AddModelError("Error", TempData["virtualEntityException"].ToString());
            }
            if (TempData["dataManagementException"] != null && !string.IsNullOrEmpty(TempData["dataManagementException"].ToString()))
            {
                ModelState.AddModelError("Error", TempData["dataManagementException"].ToString());
            }
            if (TempData["Identifier"] != null && !string.IsNullOrEmpty(TempData["Identifier"].ToString()))
            {
                ViewData["Identifier"] = TempData["Identifier"];
            }

            //LoadEntityDetails();

            FillViewData(entityId);
            ViewData["EntityName"] = entityName;
            var lstDataView = new List<CelloSaaS.Model.ViewManagement.DataView>();
            try
            {
                //Get all entity meta data
                var dicEntityMetaData = DataManagementProxy.GetAllEntityMetaData();
                if (dicEntityMetaData != null && dicEntityMetaData.Count > 0)
                {
                    var dataViewList = ViewMetaDataProxy.GetDataViews(this.TenantId);

                    if (dataViewList != null & dataViewList.Count > 0)
                    {
                        IAccessControlService accessControlService = (IAccessControlService)ServiceLocator.GetServiceImplementation(typeof(IAccessControlService));
                        if (!accessControlService.CheckPrivilege(EntityPrivilegePrefix.ViewPrefix + entityId))
                        {
                            throw new VirtualEntityException(string.Format(CultureInfo.InvariantCulture, Resources.VirtualEntityResource.e_ViewDenied, entityName));
                        }

                        int count = 0;
                        foreach (CelloSaaS.Model.ViewManagement.DataView dataView in dataViewList)
                        {
                            if (dataView.MainEntity == entityId)
                            {
                                if (dataView.DataViewID.ToLower().Contains("form" + "$" + this.TenantId))
                                {
                                    count = count + 1;
                                    ViewData["FormMode"] = CurrentFormMode.Insert;
                                    FillViewData(entityId);
                                    ViewData["FormDataViewId"] = dataView.DataViewID;
                                    ViewData["FormDataViewName"] = dataView.Name;
                                }
                                else if (dataView.DataViewID.ToLower().Contains("grid" + "$" + this.TenantId))
                                {
                                    count = count + 1;
                                    GetVirtualEntityData(entityId, dataView.DataViewID);
                                    ViewData["GridDataViewId"] = dataView.DataViewID;
                                    ViewData["GridDataViewName"] = dataView.Name;
                                }
                            }
                        }
                        if (count <= 0)
                        {
                            TempData["Info"] = string.Format(CultureInfo.InvariantCulture, Resources.VirtualEntityResource.e_NoDataView, entityName);
                        }
                    }
                    else
                    {
                        TempData["Info"] = string.Format(CultureInfo.InvariantCulture, Resources.VirtualEntityResource.e_NoDataView, entityName);
                    }
                }
                else
                {
                    TempData["Info"] = string.Format(CultureInfo.InvariantCulture, Resources.VirtualEntityResource.e_NoMetaData, entityName);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, DefaultPolicy);
                ModelState.AddModelError("Error", unauthorizedAccessException.Message);
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, DefaultPolicy);
                ModelState.AddModelError("Error", dataManagementException.Message);
            }
            catch (ViewMetaDataException viewMetaDataException)
            {
                ExceptionService.HandleException(viewMetaDataException, DefaultPolicy);
                ModelState.AddModelError("Error", viewMetaDataException.Message);
            }
            catch (VirtualEntityException virtualEntityException)
            {
                ExceptionService.HandleException(virtualEntityException, DefaultPolicy);
                ModelState.AddModelError("Error", virtualEntityException.Message);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                string errorMessage = dataException.InnerException != null ? dataException.InnerException.Message : dataException.Message;
                // do not change to resource, unique extn field error message comes from here
                ModelState.AddModelError("Error", errorMessage);
            }
            if (TempData["FromAndGrid"] != null)
            {
                if (TempData["FormMode"] != null && !string.IsNullOrEmpty(TempData["FormMode"].ToString()))
                {
                    ViewData["FormMode"] = TempData["FormMode"];
                    ViewData["VirtualEntity"] = TempData["VirtualEntity"];
                }
                return View("FormAndGrid");
            }
            else
            {
                return View("VirtualEntity");
            }
        }


        /// <summary>
        /// This method is used to deletes the virtual entity data.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <param name="tenantId">tenant identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteVirtualEntityData(string entityId, string referenceId, string tenantId, string dataViewId)
        {
            TempData["FromAndGrid"] = true;
            EntityMetaData entityMetaData = null;
            try
            {
                Guard.NullOrEmpty("entityId", entityId);
                Guard.NullOrEmpty("referenceId", referenceId);
                Guard.NullOrEmpty("tenantId", tenantId);
                entityMetaData = DataManagementProxy.GetEntityMetaData(entityId, this.TenantId);
                if (entityMetaData == null)
                {
                    TempData["Error"] = string.Format(CultureInfo.InvariantCulture, Resources.VirtualEntityResource.e_NoMetaData, Resources.VirtualEntityResource.thisEntity);
                }
                VirtualEntityProxy.DeleteVirtualEntityData(entityId, referenceId, tenantId);
                TempData["Success"] = string.Format(Resources.VirtualEntityResource.s_VirtualEntity_Deleted_Successfully, entityMetaData.EntityName);
                ViewData["FormMode"] = CurrentFormMode.Insert;
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                TempData["argumentNullException"] = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                TempData["argumentException"] = Resources.DataResource.e_ParameterEmptyOrNull;
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, DefaultPolicy);
                ModelState.AddModelError("Error", unauthorizedAccessException.Message);
            }
            catch (VirtualEntityException virtualEntityException)
            {
                ExceptionService.HandleException(virtualEntityException, DefaultPolicy);
                TempData["virtualEntityException"] = virtualEntityException.Message;
            }
            catch (DataManagementException dataManagementException)
            {
                ExceptionService.HandleException(dataManagementException, DefaultPolicy);
                TempData["dataManagementException"] = dataManagementException.Message;
            }
            return GetVirtualEntityDataViews(entityId, entityMetaData.EntityName);
        }

        /// <summary>
        /// This method is used to manages the specified from collection.
        /// </summary>
        /// <param name="fromCollection">form collection.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Manage(FormCollection fromCollection)
        {
            EntityMetaData entityMetaData = null;
            TempData["FromAndGrid"] = true;
            if (fromCollection["EntityIdentifier"] != null && !string.IsNullOrEmpty(fromCollection["EntityIdentifier"].ToString()) && fromCollection["DataViewId"] != null && !string.IsNullOrEmpty(fromCollection["DataViewId"].ToString()))
            {
                try
                {
                    entityMetaData = DataManagementProxy.GetEntityMetaData(fromCollection["EntityIdentifier"], this.TenantId);
                    if (entityMetaData == null)
                    {
                        TempData["Info"] = string.Format(CultureInfo.InvariantCulture, Resources.VirtualEntityResource.e_NoMetaData, Resources.VirtualEntityResource.EntityData);
                    }
                    VirtualEntity virtualEntity = new VirtualEntity(fromCollection["EntityIdentifier"].ToString());
                    virtualEntity.TenantId = this.TenantId;
                    virtualEntity.ExtendedRow = new ExtendedEntityRow();

                    virtualEntity.Identifier = fromCollection["Identifier"];
                    TryUpdateModel(virtualEntity, virtualEntity.EntityIdentifier);
                    if (ModelState.IsValid)
                    {
                        VirtualEntityProxy.SaveVirtualEntityData(virtualEntity);
                        TempData["Success"] = string.Format(Resources.VirtualEntityResource.s_VirtualEntity_Saved_Successfully, entityMetaData.EntityName);
                        TempData["FormMode"] = CurrentFormMode.Insert;
                        ModelState.Clear();
                    }
                    else
                    {
                        TempData["FormMode"] = CurrentFormMode.Edit;
                        TempData["VirtualEntity"] = virtualEntity;
                        if (fromCollection["Identifier"] != null && !string.IsNullOrEmpty(fromCollection["Identifier"].ToString()))
                        {
                            TempData["Identifier"] = fromCollection["Identifier"];
                        }
                    }
                }
                catch (BusinessRuleException businessRuleException)
                {
                    ExceptionService.HandleException(businessRuleException, DefaultPolicy);
                    if (businessRuleException.Data != null)
                    {
                        int count = 1;
                        StringBuilder errorMessage = new StringBuilder(); ;
                        foreach (DictionaryEntry er in businessRuleException.Data)
                        {
                            foreach (string error in (IEnumerable<string>)er.Value)
                            {
                                errorMessage.Append(error);
                            }
                        }
                        TempData["virtualEntityException"] = errorMessage.ToString();
                    }
                }
                catch (ArgumentException argumentException)
                {
                    ExceptionService.HandleException(argumentException, DefaultPolicy);
                    TempData["argumentException"] = Resources.DataResource.e_ParameterEmptyOrNull;
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    ExceptionService.HandleException(unauthorizedAccessException, DefaultPolicy);
                    ModelState.AddModelError("Error", unauthorizedAccessException.Message);
                }
                catch (VirtualEntityException virtualEntityException)
                {
                    ExceptionService.HandleException(virtualEntityException, DefaultPolicy);
                    TempData["virtualEntityException"] = virtualEntityException.Message;
                }
                catch (DataManagementException dataManagementException)
                {
                    ExceptionService.HandleException(dataManagementException, DefaultPolicy);
                    TempData["dataManagementException"] = dataManagementException.Message;
                }
                catch (DataException dataException)
                {
                    ExceptionService.HandleException(dataException, DefaultPolicy);
                    string errorMessage = dataException.InnerException != null ? dataException.InnerException.Message : dataException.Message;
                    // do not change to resource, unique extn field error message comes from here
                    ModelState.AddModelError("Error", errorMessage);
                }
            }
            return GetVirtualEntityDataViews(fromCollection["EntityIdentifier"], entityMetaData.EntityName);
        }

        /// <summary>
        /// This method is used to manages the virtual entity data.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="referenceId">reference identifier.</param>
        /// <param name="dataViewId">data view identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageVirtualEntityData(string entityId, string referenceId, string dataViewId)
        {
            Guard.NullOrEmpty("entityId", entityId);
            Guard.NullOrEmpty("dataViewId", dataViewId);
            Guard.NullOrEmpty("referenceId", referenceId);

            ViewData["DataViewId"] = dataViewId;
            ViewData["EntityId"] = entityId;
            ViewData["TenantId"] = this.TenantId;
            VirtualEntity virtualEntity = new VirtualEntity(entityId);
            virtualEntity.ExtendedRow = new ExtendedEntityRow();
            try
            {
                GetVirtualEntityData(entityId, dataViewId);
                ViewData["FormMode"] = CurrentFormMode.Edit;
                virtualEntity = VirtualEntityProxy.GetVirtualEntityData(entityId, referenceId, this.TenantId);
            }
            catch (ArgumentNullException argumentNullException)
            {
                ExceptionService.HandleException(argumentNullException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, DefaultPolicy);
                ModelState.AddModelError("Error", Resources.DataResource.e_ParameterEmptyOrNull);
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                ExceptionService.HandleException(unauthorizedAccessException, DefaultPolicy);
                ModelState.AddModelError("Error", unauthorizedAccessException.Message);
            }
            catch (ViewMetaDataException viewMetaDataException)
            {
                ExceptionService.HandleException(viewMetaDataException, DefaultPolicy);
                ModelState.AddModelError("Error", viewMetaDataException.Message);
            }
            catch (VirtualEntityException virtualEntityException)
            {
                ExceptionService.HandleException(virtualEntityException, DefaultPolicy);
                ModelState.AddModelError("Error", virtualEntityException.Message);
            }
            catch (DataException dataException)
            {
                ExceptionService.HandleException(dataException, DefaultPolicy);
                string errorMessage = dataException.InnerException != null ? dataException.InnerException.Message : dataException.Message;
                // do not change to resource, unique extn field error message comes from here
                ModelState.AddModelError("Error", errorMessage);
            }
            return PartialView("FormAndGrid", virtualEntity);
        }



        /// <summary>
        /// This method is used to gets the virtual entity data.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        /// <param name="dataViewId">data view identifier.</param>
        private void GetVirtualEntityData(string entityId, string dataViewId)
        {
            var dataViewList = ViewMetaDataProxy.GetDataViews(this.TenantId);
            if (dataViewList != null)
            {
                foreach (CelloSaaS.Model.ViewManagement.DataView dataView in dataViewList)
                {
                    if (dataView.MainEntity == entityId)
                    {
                        if (dataView.DataViewID.ToLower().Contains("form" + "$" + this.TenantId))
                        {
                            ViewData["FormDataViewId"] = dataView.DataViewID;
                            ViewData["FormDataViewName"] = dataView.Name;
                        }
                        else if (dataView.DataViewID.ToLower().Contains("grid" + "$" + this.TenantId))
                        {
                            ViewData["GridDataViewId"] = dataViewId;
                            ViewData["GridDataViewName"] = dataView.Name;
                        }
                    }
                }
                VirtualEntitySearchCondition condition = new VirtualEntitySearchCondition();
                condition.TenantId = this.TenantId;
                condition.EntityId = entityId;
                FillViewData(entityId);
                ViewData["VirtualEntities"] = (IEnumerable<VirtualEntity>)VirtualEntityProxy.SearchVirtualEntityData(condition).Items.Values.AsEnumerable();
            }
            else
            {
                TempData["Info"] = string.Format(CultureInfo.InvariantCulture, Resources.VirtualEntityResource.e_NoDataView, Resources.VirtualEntityResource.thisEntity);
            }
        }

        /// <summary>
        /// This method is used to fills the view data.
        /// </summary>
        /// <param name="entityId">entity identifier.</param>
        private void FillViewData(string entityId)
        {
            ViewData["EntityId"] = entityId;
            ViewData["TenantId"] = this.TenantId;
        }

        /// <summary>
        /// This method is used to fills the data view.
        /// </summary>
        /// <param name="lstDataView">LST data view.</param>
        /// <param name="dataView">data view.</param>
        private static void FillDataView(List<CelloSaaS.Model.ViewManagement.DataView> lstDataView, CelloSaaS.Model.ViewManagement.DataView dataView)
        {
            CelloSaaS.Model.ViewManagement.DataView metadataView = new CelloSaaS.Model.ViewManagement.DataView();
            metadataView.DataViewID = dataView.DataViewID;
            metadataView.Name = dataView.Name;
            metadataView.TenantId = UserIdentity.TenantID;
            lstDataView.Add(metadataView);
        }
    }
}
