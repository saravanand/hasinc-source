﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.WorkFlow;
using CelloSaaS.WorkFlow.Service;
using CelloSaaS.WorkFlow.ServiceContracts;
using CelloSaaS.WorkFlow.Model;
using CelloSaaS.WorkFlow.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.View;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for workflow dashboard.
    /// </summary>
    public class WFDashboardController : CelloController
    {
        private string TenantId = TenantContext.GetTenantId("_WorkFlow");
        private const string policyName = "WorkFlowExceptionLogger";

        /// <summary>
        /// This method is used to get the workflow dashboard view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<WorkflowModel> workflowModels = null;
            Dictionary<string, string> InstanceStatus = new Dictionary<string, string>();
            InstanceStatus.Add("", "All");
            InstanceStatus.Add("Created", "Created");
            InstanceStatus.Add("Started", "Started");
            InstanceStatus.Add("Completed", "Completed");
            ViewData["WFInstanceStatus"] = new SelectList(InstanceStatus, "Key", "Value", null);
            try
            {
                //var service = new WorkflowService();
                workflowModels = WorkflowServiceProxy.GetWorkflows(this.TenantId);
                ViewData["Workflows"] = workflowModels != null ? workflowModels.Count : 0;
                ViewData["WorkflowName"] = new SelectList(workflowModels, "Id", "Name");
            }
            catch (WorkFlowException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("", ex.Message);
            }

            return View(workflowModels);
        }

        /// <summary>
        /// This method is used to get workflow dashboard based on the given parameters.
        /// </summary>
        /// <param name="WorkflowName">work flow name.</param>
        /// <param name="WfInstanceStatus">work flow instance status.</param>
        /// <param name="MapId">map identifier.</param>
        /// <param name="page">page.</param>
        /// <param name="pageSize">page size.</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult Index(string WorkflowName, string WfInstanceStatus, string MapId, int page = 1, int pageSize = 10)
        {
            ViewData["WorkflowName"] = WorkflowName;
            ViewData["WfInstanceStatus"] = WfInstanceStatus;
            ViewData["MapId"] = MapId;
            ViewData["PageNumber"] = page;
            ViewData["TotalCount"] = 0;
            ViewData["PageSize"] = pageSize;
            List<WorkflowInstance> lstWFInstances = null;


            if (string.IsNullOrEmpty(WorkflowName))
            {
                ModelState.AddModelError("WorkFlowDashBoardError", Resources.WFDashboardResource.e_SelectWorkflow);
            }
            else
            {
                //fetch instances details based on workflow id
                //var wfService = new WorkflowService();
                var searchParam = new WfSearchParameter
                {
                    WFId = WorkflowName,
                    WFInstanceStatus = WfInstanceStatus,
                    MapId = MapId,
                    RecordStart = ((Convert.ToInt32(page) - 1) * pageSize) + 1,
                    RecordEnd = page * pageSize,
                    SetTotalCount = true,
                    TenantId = this.TenantId
                };

                lstWFInstances = WorkflowServiceProxy.SearchWorkFlowInstanceDeep(searchParam);

                if (searchParam.TotalCount != null)
                {
                    ViewData["TotalCount"] = searchParam.TotalCount;
                }
                else
                {
                    ViewData["TotalCount"] = WorkflowServiceProxy.GetTotalCountSearchDetails(searchParam);
                }
            }

            return PartialView("WFDashboardResult", lstWFInstances);
        }

        /// <summary>
        /// This method is used to restarts the step.
        /// </summary>
        /// <param name="wfId">work flow identifier.</param>
        /// <param name="wfInstanceId">work flow instance identifier.</param>
        /// <param name="mapId">map identifier.</param>
        /// <param name="taskCode">task code.</param>
        /// <returns></returns>
        public JsonResult RestartStep(string wfId, string wfInstanceId, string mapId, string taskCode)
        {
            // Validating arguments
            if (!string.IsNullOrEmpty(wfId) && !string.IsNullOrEmpty(wfInstanceId) && !string.IsNullOrEmpty(mapId) && !string.IsNullOrEmpty(taskCode))
            {
                var restartStatus = WorkflowServiceProxy.RestartWorkflowStep(wfId, wfInstanceId, mapId, taskCode, this.TenantId);

                if (restartStatus == true)
                {
                    return Json(new { Success = Resources.WFDashboardResource.s_RestartStep }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Error = Resources.WFDashboardResource.e_RestartStep }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Error = Resources.WFDashboardResource.e_InvalidData }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method used to show the occurred exceptions.
        /// </summary>
        /// <param name="taskRunId">task run identifier.</param>
        /// <returns></returns>
        public ActionResult ShowException(string taskRunId)
        {
            List<string> exceptionDetails = WorkflowServiceProxy.GetWorkflowExceptionDetails(taskRunId);
            ViewData["ExceptionDetails"] = exceptionDetails != null && exceptionDetails.Count > 0 ? exceptionDetails.FirstOrDefault() : string.Empty;
            return PartialView("ShowException");
        }
    }
}
