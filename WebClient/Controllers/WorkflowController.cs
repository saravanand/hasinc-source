﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.WorkFlow.ServiceContracts;
using CelloSaaS.View;
using CelloSaaS.WorkFlow.Model;
using CelloSaaS.Library;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.WorkFlow.ServiceProxies;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for workflow.
    /// </summary>
    public class WorkflowController : CelloController
    {
        private const string policyName = "WorkFlowExceptionLogger";
        private string TenantId = TenantContext.GetTenantId("_WorkFlow");

        /// <summary>
        /// This method is used to get the manage workflow view.
        /// </summary>
        /// <param name="filterId">filter identifier.</param>
        /// <param name="workflowName">workflow name.</param>
        /// <returns></returns>
        public ActionResult Index(string filterId, string workflowName)
        {
            if (!string.IsNullOrEmpty(workflowName))
            {
                try
                {
                    WorkflowModel wfModel = WorkflowServiceProxy.GetWorkflowByName(workflowName, this.TenantId);
                    ViewData["SelectWorkflowId"] = wfModel != null ? wfModel.Id.ToString() : null;
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                }
            }

            return View();
        }

        /// <summary>
        /// This method is used to get all workflow.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllWorkflow()
        {
            //var service = new WorkflowService();
            List<WorkflowModel> workflowModels = WorkflowServiceProxy.GetWorkflows(this.TenantId);

            if (workflowModels != null)
            {
                // group by workflow category
                var categoryWorkflows = new Dictionary<string, List<WorkflowModel>>();

                foreach (var item in workflowModels)
                {
                    var category = string.IsNullOrEmpty(item.CategoryName) ? "Default" : item.CategoryName;

                    if (!categoryWorkflows.ContainsKey(category))
                    {
                        categoryWorkflows.Add(category, new List<WorkflowModel>());
                    }

                    categoryWorkflows[category].Add(item);
                }

                var workflowList = new List<object>();

                foreach (var item in categoryWorkflows)
                {
                    var childs = item.Value.Select(x =>
                       new
                       {
                           data = new { title = x.Name, icon = "/Content/images/workflow-ico.png" },
                           attr = new { id = x.Id.ToString(), title = x.Description, rel = string.IsNullOrEmpty(x.TenantId) },
                           metadata = new { pkid = x.Id.ToString() },
                           state = "",
                       });

                    workflowList.Add(new
                    {
                        data = new { title = item.Key, icon = "/Content/images/category-ico.png" },
                        attr = new { @class = "category", rel = "category", id = item.Key },
                        children = childs,
                        state = "opened"
                    });
                }

                var root = new
                {
                    data = new { title = "Workflow", icon = "/Content/images/workflow-root-ico.png" },
                    attr = new { @class = "root", rel = "root", id = "workflow" },
                    children = workflowList,
                    state = "opened"
                };

                return Json(root, JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to manages the workflow.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <returns></returns>
        public ActionResult ManageWorkflow(string workflowId)
        {
            WorkflowModel workflowModel = new WorkflowModel();
            if (!string.IsNullOrEmpty(workflowId))
            {
                //var workflowService = new WorkflowService();
                workflowModel = WorkflowServiceProxy.GetWorkflow(workflowId, this.TenantId);
            }

            ViewData["IsGlobal"] = this.TenantId.Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase);
            return PartialView(workflowModel);
        }

        /// <summary>
        /// This method is used to adds the workflow.
        /// </summary>
        /// <param name="formCollection">form collection.</param>
        /// <param name="IsGlobal">IsGlobal (true/false)</param>
        /// <returns></returns>        
        [ValidateInput(false)]
        public ActionResult SaveWorkflow(FormCollection formCollection, bool? IsGlobal)
        {
            WorkflowModel workflowModel = new WorkflowModel();
            TryUpdateModel(workflowModel);
            if (string.IsNullOrEmpty(workflowModel.Name))
            {
                ModelState.AddModelError("Name", Resources.WorkflowDesignerResource.e_NameEmpty);
            }

            if (string.IsNullOrEmpty(workflowModel.Description))
            {
                ModelState.AddModelError("Description", Resources.WorkflowDesignerResource.e_DescriptionEmpty);
            }

            ViewBag.IsGlobal = IsGlobal;

            try
            {
                if (ModelState.IsValid)
                {
                    //var workflowService = new WorkflowService();
                    workflowModel.TenantId = IsGlobal != null && (bool)IsGlobal ? string.Empty : this.TenantId;
                    string xmlTransform = string.Empty;
                    if (workflowModel.Id != null && workflowModel.Id != Guid.Empty)
                    {
                        xmlTransform = formCollection["XmlTransform"].ToString();
                        workflowModel.UpdatedBy = UserIdentity.UserId;
                    }
                    else
                    {
                        xmlTransform = formCollection["XmlTransformAdd"].ToString();
                        workflowModel.CreatedBy = UserIdentity.UserId;
                    }

                    workflowModel.WFXmlTransform = xmlTransform;
                    string wfId = WorkflowServiceProxy.SaveWorkflow(workflowModel);
                    workflowModel.Id = Guid.Parse(wfId);
                    return Json(new { Success = "Success" });
                }
            }
            catch (DuplicateWorkflowNameException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", Resources.WorkflowDesignerResource.e_DuplicateName);
            }
            catch (WorkFlowException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("Error", Resources.WorkflowDesignerResource.e_SaveWorkflow);
            }

            return PartialView("ManageWorkflow", workflowModel);
        }

        /// <summary>
        /// This method is used to gets the workflow details.
        /// </summary>
        /// <param name="wfId">workflow identifier.</param>
        /// <returns></returns>
        public PartialViewResult GetWorkflowDetails(string wfId)
        {
            if (string.IsNullOrEmpty(wfId))
            {
                ModelState.AddModelError("Error", "Invalid workflow Id");
            }

            WorkflowModel wfDetails = null;

            if (ModelState.IsValid)
            {
                try
                {
                    wfDetails = WorkflowServiceProxy.GetWorkflow(wfId, this.TenantId, string.Empty);
                    if (wfDetails != null && string.IsNullOrEmpty(wfDetails.TenantId))
                    {
                        ViewData["IsGlobal"] = true;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                }
            }

            return PartialView(wfDetails);
        }


        /// <summary>
        /// This method is used to get activities for specified workflow identifier.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <returns></returns>
        public ActionResult Activities(string workflowId)
        {
            var activites = ActivityServiceProxy.GetActivities(workflowId);
            return PartialView(activites);
        }
    }
}
