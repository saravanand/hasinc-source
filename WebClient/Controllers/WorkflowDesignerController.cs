﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.WorkFlow;
using CelloSaaS.Library;
using CelloSaaSApplication.Models;
using CelloSaaS.WorkFlow.ServiceContracts;
using CelloSaaS.View;
using CelloSaaS.WorkFlow.Model;
using CelloSaaS.Model.Configuration;
using CelloSaaS.ServiceProxies.AccessControlManagement;
using CelloSaaS.ServiceProxies.Configuration;
using CelloSaaS.WorkFlow.ServiceProxies;
using CelloSaaS.WorkFlow.Activities;
using CelloSaaS.Notification.Model;
using CelloSaaS.Notification.ServiceProxies;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for workflow designer.
    /// </summary>
    public class WorkflowDesignerController : CelloController
    {
        public string TenantId = TenantContext.GetTenantId("_WorkFlow");
        public string TenantName = TenantContext.GetTenantName("_WorkFlow");
        public string UserId = UserIdentity.UserId;

        private const string policyName = "WorkFlowExceptionLogger";

        /// <summary>
        /// This method is used to list the workflows found in the system.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string workflowId, string filterId)
        {
            List<WorkflowDefinition> lstActiveWfDefs = null;
            ViewData["FilterId"] = filterId;
            ViewData["WorkflowId"] = workflowId;

            ViewData["CanAdd"] = AccessControlProxy.CheckPermission(WorkFlowPrivilegeConstants.AddWorkflowDesign);
            ViewData["CanEdit"] = AccessControlProxy.CheckPermission(WorkFlowPrivilegeConstants.EditWorkflowDesign);
            ViewData["CanDelete"] = AccessControlProxy.CheckPermission(WorkFlowPrivilegeConstants.DeleteWorkflowDesign);
            ViewData["CanPublish"] = AccessControlProxy.CheckPermission(WorkFlowPrivilegeConstants.PublishWorkflowDesign);

            ViewData["OwnWfDefs"] = null;
            ViewData["DefaultWfDef"] = null;
            ViewData["WorkflowModel"] = null;
            ViewData["Source"] = string.Empty;

            try
            {
                //WorkflowDefinitionService service = new WorkflowDefinitionService();
                WorkflowDefinition defaultWfDef = WorkflowDefinitionServiceProxy.GetDefaultDefinition(workflowId);
                ViewData["DefaultWfDef"] = defaultWfDef;

                var wfSettingValue = ModuleConfigurationSettingProxy.GetModuleConfigurationSettingValue(this.TenantId, ModuleConfigurationConstant.WORKFLOW);

                if (!string.IsNullOrEmpty(wfSettingValue)
                    && wfSettingValue.Equals(ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING))
                {
                    //gets the active definition
                    var activeWfDef = WorkflowDefinitionServiceProxy.GetWorkflowDefinition(workflowId, this.TenantId, filterId);

                    ViewData["ActiveParentWfDef"] = activeWfDef;

                    ViewData["CanAdd"] = false;
                    ViewData["CanPublish"] = false;
                    ViewData["Source"] = ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING;

                    lstActiveWfDefs = WorkflowDefinitionServiceProxy.GetAllWorkflowDefinitionsFromOwnSetting(workflowId, this.TenantId, filterId);
                }
                else
                {
                    ViewData["Source"] = ConfigurationSettingValueConstant.OWN_SETTING;
                    lstActiveWfDefs = WorkflowDefinitionServiceProxy.GetAllWorkflowDefinitions(workflowId, this.TenantId, filterId);
                }

                SetFilterIds(filterId);

                var wfModel = WorkflowServiceProxy.GetWorkflow(workflowId, this.TenantId);
                ViewData["WorkflowModel"] = wfModel;
            }
            catch (WorkFlowException wfex)
            {
                ExceptionService.HandleException(wfex, policyName);
                ModelState.AddModelError("", wfex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("", ex.Message);
            }
            catch (ArgumentException argumentException)
            {
                ExceptionService.HandleException(argumentException, policyName);
                ModelState.AddModelError("Error", Resources.ConfigurationResource.e_ParameterEmptyOrNull);
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("PartialIndex", lstActiveWfDefs);
            }

            return View(lstActiveWfDefs);
        }

        /// <summary>
        /// This method is used to sets the filter identifier.
        /// </summary>
        /// <param name="filterId">filter identifier.</param>
        private void SetFilterIds(string filterId)
        {
            ViewData["FilterIds"] = new List<SelectListItem>() 
            {
                { new SelectListItem(){ Text="1", Value="1", Selected = filterId.Equals("1")} },
                { new SelectListItem(){ Text="2", Value="2", Selected = filterId.Equals("2")} },
                { new SelectListItem(){ Text="3", Value="3", Selected = filterId.Equals("3")} },
                { new SelectListItem(){ Text="4", Value="4", Selected = filterId.Equals("4")} }
            };
        }

        /// <summary>
        /// This method is used to creates the new design.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="isDefault">isDefault (true/false).</param>
        /// <param name="filterId"> filter identifier.</param>
        /// <returns></returns>
        public ActionResult CreateNewDesign(string workflowId, bool isDefault, string filterId)
        {
            if (!string.IsNullOrEmpty(workflowId))
            {
                try
                {
                    //var service = new WorkflowDefinitionService();
                    var wfDef = WorkflowDefinitionServiceProxy.CreateTemporaryWorkflowDefinition(workflowId, filterId, TenantId, UserId, isDefault);
                    return RedirectToAction("ManageDesign", new
                    {
                        workflowId = workflowId,
                        workflowDefinitionId = wfDef.DefinitionId.ToString(),
                        filterId = filterId
                    });
                }
                catch (WorkFlowException wfex)
                {
                    ExceptionService.HandleException(wfex, policyName);
                    ModelState.AddModelError("", wfex.Message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Redirect(this.Request.UrlReferrer.ToString());
        }

        /// <summary>
        /// This method is sued to creates the copy.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <returns></returns>
        public JsonResult CreateCopy(string workflowId, string workflowDefinitionId, string filterId)
        {
            //var service = new WorkflowDefinitionService();
            string newDefId = string.Empty;

            try
            {
                newDefId = WorkflowDefinitionServiceProxy.CopyDefinition(workflowDefinitionId, filterId, TenantId, UserId);
            }
            catch (WorkFlowException wfex)
            {
                ExceptionService.HandleException(wfex, policyName);
                return Json(new { Error = wfex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }

            if (string.IsNullOrEmpty(newDefId))
            {
                return Json(new { Error = Resources.WorkflowDesignerResource.e_DuplicateDraftVersion });
            }

            return Json(new { Status = Resources.WorkflowDesignerResource.s_Copy });
        }

        /// <summary>
        /// This method is used to publishes the specified workflow definition identifier.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <returns></returns>
        public JsonResult Publish(string workflowDefinitionId)
        {
            try
            {
                //WorkflowDefinitionService workflowDefinitionService = new WorkflowDefinitionService();
                WorkflowDefinitionServiceProxy.PublishDefinition(workflowDefinitionId);
                return Json(new { Status = Resources.WorkflowDesignerResource.s_Publish });
            }
            catch (WorkFlowException wfex)
            {
                ExceptionService.HandleException(wfex, policyName);
                return Json(new { Error = wfex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
        }

        /// <summary>
        /// This method is used to manages the design.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="filterId">filter identifier.</param>
        /// <returns></returns>
        public ActionResult ManageDesign(string workflowId, string workflowDefinitionId, string filterId)
        {
            //var service = new WorkflowDefinitionService();
            WorkflowDefinition wfDef = null;
            ViewData["FilterId"] = filterId;
            ViewData["WorkflowId"] = workflowId;
            ViewData["CanEdit"] = false;

            try
            {
                if (!string.IsNullOrEmpty(workflowDefinitionId)
                    && !string.IsNullOrEmpty(workflowId))
                {
                    var wfModel = WorkflowServiceProxy.GetWorkflow(workflowId, TenantId);
                    ViewData["WorkflowModel"] = wfModel;

                    wfDef = WorkflowDefinitionServiceProxy.GetWorkflowDefinitionUnCached(workflowDefinitionId);

                    if (wfDef == null)
                        wfDef = WorkflowDefinitionServiceProxy.GetWorkflowDefinition(workflowDefinitionId);

                    if (wfDef != null)
                        ViewData["FilterId"] = wfDef.FilterId;

                    //var activityService = new ActivityService();
                    ViewData["Activities"] = ActivityServiceProxy.GetActivities(workflowId);

                    if (wfDef != null)
                    {
                        ViewData["CanEdit"] = (UserIdentity.HasPrivilege(WorkFlowPrivilegeConstants.EditWorkflowDesign) ||
                                            UserIdentity.HasPrivilege(WorkFlowPrivilegeConstants.AddWorkflowDesign)) &&
                                             ((wfDef.Version == 1 && (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin)
                                             || (UserIdentity.TenantID.Equals(wfModel.TenantId)))) || (wfDef.Status == WorkflowDefinitionStatus.Draft));
                    }
                }
            }
            catch (WorkFlowException wfex)
            {
                ExceptionService.HandleException(wfex, policyName);
                ModelState.AddModelError("", wfex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }

            return View(wfDef);
        }

        /// <summary>
        /// This method is used to saves the workflow UI model.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="uiModel">UI model.</param>
        /// <returns></returns>
        public JsonResult SaveWorkflowUIModel(string workflowId, string workflowDefinitionId, WFUIModel[] uiModel)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId)
                && !string.IsNullOrEmpty(workflowId)
                && uiModel != null)
            {
                try
                {
                    //var service = new WorkflowDefinitionService();
                    WorkflowDefinitionServiceProxy.SaveWorkflowDefinition(workflowDefinitionId, UserId);

                    if (uiModel != null && uiModel.Length > 0)
                    {
                        var uiModelDict = new WFUIModelDictionary();
                        foreach (var item in uiModel)
                            uiModelDict.Add(item.Id, item);

                        WorkflowDefinitionServiceProxy.SaveWorkflowUIModel(workflowDefinitionId, uiModelDict);
                    }

                    return Json(new { Success = Resources.WorkflowDesignerResource.s_SaveWorkflowUI });
                }
                catch (WorkFlowException wfex)
                {
                    ExceptionService.HandleException(wfex, policyName);
                    return Json(new { Error = wfex.Message });
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    return Json(new { Error = ex.Message });
                }
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to saves the workflow definition.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <returns></returns>
        public JsonResult SaveWorkflowDefinition(string workflowId, string workflowDefinitionId)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId)
              && !string.IsNullOrEmpty(workflowId))
            {
                try
                {
                    //var service = new WorkflowDefinitionService();
                    WorkflowDefinitionServiceProxy.SaveWorkflowDefinition(workflowDefinitionId, UserId);
                    return Json(new { Success = Resources.WorkflowDesignerResource.s_SaveWorkflowDefinition });
                }
                catch (WorkFlowException wfex)
                {
                    ExceptionService.HandleException(wfex, policyName);
                    return Json(new { Error = wfex.Message });
                }
                catch (UnauthorizedAccessException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    return Json(new { Error = ex.Message });
                }
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to deletes the workflow.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="filterId">filter identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteWorkflow(string workflowId, string workflowDefinitionId, string filterId)
        {
            try
            {
                if (!string.IsNullOrEmpty(workflowDefinitionId))
                {
                    //var service = new WorkflowDefinitionService();
                    WorkflowDefinitionServiceProxy.DeleteWorkflowDefinition(workflowDefinitionId);

                    return Json(new { Success = Resources.WorkflowDesignerResource.s_DeleteWFDesign });
                }
            }
            catch (WorkFlowException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }
            catch (UnauthorizedAccessException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                return Json(new { Error = ex.Message });
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        #region Routers & Connections

        /// <summary>
        /// This method is used to adds the router.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="router">router.</param>
        /// <returns></returns>
        public JsonResult AddRouter(string workflowDefinitionId, RouterViewModel router)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && router != null)
            {
                try
                {
                    //var service = new WorkflowDefinitionService();

                    var taskRouter = new TaskRouterDefinition();
                    taskRouter.RouterId = router.RouterId;
                    taskRouter.RouterType = router.Type;

                    if (router.Conditions != null && router.Conditions.Length > 0)
                    {
                        foreach (var cond in router.Conditions)
                        {
                            IRoutingCondition rCondition = null;
                            if (router.Type.Equals(RouterType.IfElse))
                            {
                                IfElseRoutingContion ifElseCondition = new IfElseRoutingContion();
                                if (!string.IsNullOrEmpty(router.OutputTaskIds))
                                {
                                    ifElseCondition.IfNodeOutputIds.AddRange(router.OutputTaskIds.Split(',').Select(x => new Guid(x)));
                                }

                                if (!string.IsNullOrEmpty(router.ElseOutputIds))
                                {
                                    ifElseCondition.ElseNodeOutputIds.AddRange(router.ElseOutputIds.Split(',').Select(x => new Guid(x)));
                                }

                                rCondition = ifElseCondition;
                            }
                            else
                            {
                                rCondition = new RoutingConditions();
                                if (!string.IsNullOrEmpty(router.OutputTaskIds))
                                {
                                    rCondition.OutputTaskIds.AddRange(router.OutputTaskIds.Split(',').Select(x => new Guid(x)));
                                }
                            }

                            if (!string.IsNullOrEmpty(router.InputTaskIds))
                            {
                                rCondition.InputTaskIds.AddRange(router.InputTaskIds.Split(',').Select(x => new Guid(x)));
                            }

                            taskRouter.RoutingTable.RoutingConditions.Add(rCondition);
                        }
                    }

                    var routerName = WorkflowDefinitionServiceProxy.AddRouter(workflowDefinitionId, taskRouter);

                    return Json(new { Success = Resources.WorkflowDesignerResource.s_AddRouter, RouterName = routerName });
                }
                catch (WorkFlowException wfex)
                {
                    ExceptionService.HandleException(wfex, policyName);
                    ModelState.AddModelError("", wfex.Message);
                }
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to updates the router condition.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="routerId">router identifier.</param>
        /// <param name="conditionExpressions">list of condition expression.</param>
        /// <returns></returns>
        public JsonResult UpdateRouterDetails(string workflowDefinitionId, string routerId, string routerName, string[] conditionExpressions)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && !string.IsNullOrEmpty(routerId) && conditionExpressions.Count() > 0)
            {
                //var service = new WorkflowDefinitionService();
                WorkflowDefinitionServiceProxy.UpdateRouterDetails(workflowDefinitionId, routerId, routerName, conditionExpressions);
                return Json(new { Success = Resources.WorkflowDesignerResource.s_UpdateRouterCondition });
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to deletes the router.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="routerId">router identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteRouter(string workflowDefinitionId, string routerId)
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && !string.IsNullOrEmpty(routerId))
            {
                //var service = new WorkflowDefinitionService();
                WorkflowDefinitionServiceProxy.DeleteRouter(workflowDefinitionId, routerId);
                return Json(new { Success = Resources.WorkflowDesignerResource.s_DeleteRouter });
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to adds the router connections.
        /// </summary>
        /// <param name="workflowDefinitionId"> workflow definition identifier.</param>
        /// <param name="routerId">router identifier.</param>
        /// <param name="inputTaskId">input task identifier.</param>
        /// <param name="outputTaskId">output task identifier.</param>
        /// <returns></returns>
        public JsonResult AddRouterConnections(string workflowDefinitionId, string routerId, string inputTaskId, string outputTaskId, bool isVisible, string elseOutputId = "")
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && !string.IsNullOrEmpty(routerId))
            {
                //var service = new WorkflowDefinitionService();
                WorkflowDefinitionServiceProxy.AddRouterConnections(workflowDefinitionId, routerId, inputTaskId, outputTaskId, isVisible, elseOutputId);
                return Json(new { Success = Resources.WorkflowDesignerResource.s_AddRouterConnection });
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to removes the router connections.
        /// </summary>
        /// <param name="workflowDefinitionId">workflow definition identifier.</param>
        /// <param name="routerId">router identifier.</param>
        /// <param name="inputTaskId">input task identifier.</param>
        /// <param name="outputTaskId">output task identifier.</param>
        /// <param name="elseOutputId">elseOutputId</param>
        /// <returns></returns>
        public JsonResult RemoveRouterConnections(string workflowDefinitionId, string routerId, string inputTaskId, string outputTaskId, string elseOutputId = "")
        {
            if (!string.IsNullOrEmpty(workflowDefinitionId) && !string.IsNullOrEmpty(routerId))
            {
                //var service = new WorkflowDefinitionService();
                WorkflowDefinitionServiceProxy.RemoveRouterConnections(workflowDefinitionId, routerId, inputTaskId, outputTaskId, elseOutputId);
                return Json(new { Success = Resources.WorkflowDesignerResource.s_DeleteRouterConnection });
            }

            return Json(new { Error = Resources.WorkflowDesignerResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to gets the activity property details.
        /// </summary>
        /// <param name="wfId"> workflow identifier.</param>
        /// <param name="wfDefId"> workflow definition identifier.</param>
        /// <param name="activityId">activity identifier.</param>
        /// <param name="taskDefId">task definition identifier.</param>
        /// <returns></returns>
        public ActionResult GetActivityPropertyDetails(string wfId, string wfDefId, string activityId, string taskDefId)
        {
            if (string.IsNullOrEmpty(activityId))
            {
                return Json(Resources.WorkflowDesignerResource.e_ActivityNotAvailable);
            }

            //ActivityService activityService = new ActivityService();
            ActivityDetails activityDetails = ActivityServiceProxy.GetActivity(wfId, activityId);
            if (activityDetails == null)
            {
                return Json(Resources.WorkflowDesignerResource.e_ActivityNotAvailable);
            }

            if (activityDetails.KeyValues == null || activityDetails.KeyValues.Count() < 1)
            {
                return Json(Resources.WorkflowDesignerResource.e_PropertiesNotAvailable);
            }

            List<ActivityProperty> properties = new List<ActivityProperty>();
            bool isNotificationActivity = false;
            foreach (string property in activityDetails.KeyValues)
            {
                ActivityProperty activityProperty = new ActivityProperty()
                {
                    Name = property.Split(':')[0].Trim(),
                    DataType = property.Split(':')[1].Trim()
                };

                properties.Add(activityProperty);
                if (activityProperty.Name.Equals(NotificationActivityConstants.NotificationName, StringComparison.OrdinalIgnoreCase))
                {
                    isNotificationActivity = true;
                }
            }

            //WorkflowDefinitionService service = new WorkflowDefinitionService();
            WorkflowDefinition wfDef = WorkflowDefinitionServiceProxy.GetWorkflowDefinition(wfDefId);

            TaskDefinition taskDefinition = (TaskDefinition)wfDef.WorkflowItemDefinitions.Where(x => x is TaskDefinition && (x as TaskDefinition).TaskId.ToString().Equals(taskDefId, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (taskDefinition != null && taskDefinition.ActivityProperties != null && taskDefinition.ActivityProperties.Count > 0)
            {
                properties.Where(x => taskDefinition.ActivityProperties.ContainsKey(x.Name)).ToList().ForEach(y => { y.Value = taskDefinition.ActivityProperties[y.Name].Value; });
            }

            // ViewData["SourceType"] = Enum.GetValues(typeof(SourceType)).Cast<SourceType>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() });
            if (isNotificationActivity)
            {
                string selectedId = properties.Where(x => x.Name.Equals(NotificationActivityConstants.NotificationName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().Value;
                LoadNotificationDetails(selectedId);
            }

            return PartialView("ActivityProperties", properties);
        }

        /// <summary>
        /// This method is used to set the notification detail in view data.
        /// </summary>
        /// <param name="selectedId">selected identifier.</param>
        private void LoadNotificationDetails(string selectedId = null)
        {
            var selectList = new List<SelectListItem>();
            try
            {
                List<NotificationDetails> notificationDetails = NotificationConfigurationProxy.GetNotificationMasterDetailsByTenantId(UserIdentity.TenantID);
                if (notificationDetails != null && notificationDetails.Count > 0)
                {
                    selectList.AddRange(new SelectList(notificationDetails.OrderBy(x => x.NotificationName), "NotificationName", "NotificationName", selectedId));
                }
            }
            catch (WorkFlowException wfException)
            {
                ExceptionService.HandleException(wfException, policyName);
            }

            selectList.Insert(0, new SelectListItem { Value = "", Text = "--Select--" });
            ViewData[NotificationActivityConstants.NotificationName] = selectList;

        }

        #endregion
    }
}
