﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CelloSaaS.View;
using CelloSaaS.Library;
using CelloSaaS.WorkFlow.Model;
using CelloSaaS.WorkFlow.ServiceContracts;
using CelloSaaS.WorkFlow;
using System.Xml;
using CelloSaaS.WorkFlow.ServiceProxies;
using System.Globalization;
using CelloSaaS.Library.Utilities;

namespace CelloSaaSApplication.Controllers
{
    /// <summary>
    /// This class is responsible for workflow instance.
    /// </summary>
    public class WorkflowInstanceController : CelloController
    {
        private string TenantId = TenantContext.GetTenantId("_WorkFlow");
        private const string policyName = "WorkFlowExceptionLogger";

        /// <summary>
        /// This method is used to view workflow instance details.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="filterId">filter identifier.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="page">page number.</param>
        /// <param name="pageSize">size of the page.</param>
        /// <returns></returns>
        public ActionResult ViewWorkflowInstances(string workflowId, string filterId, string sortString, string sortDirection, int page = 1, int pageSize = 10)
        {
            ViewData["WorkflowId"] = workflowId;
            ViewData["FilterId"] = filterId;
            ViewData["SortString"] = sortString ?? string.Empty;
            ViewData["SortDirection"] = sortDirection ?? string.Empty;
            ViewData["PageNumber"] = page;
            ViewData["TotalCount"] = 0;
            ViewData["PageSize"] = pageSize;

            if (string.IsNullOrEmpty(workflowId))
            {
                ModelState.AddModelError("", Resources.WorkFlowInstanceResource.e_InvalidRequest);
            }

            List<WorkflowInstance> lstWFInstances = new List<WorkflowInstance>();
            if (ModelState.IsValid)
            {
                try
                {
                    // fetch instances details based on workflow id                    
                    WfSearchParameter searchParam = new WfSearchParameter
                    {
                        WFId = workflowId,
                        TenantId = this.TenantId,
                        RecordStart = ((Convert.ToInt32(page) - 1) * (int)pageSize) + 1,
                        RecordEnd = page * pageSize,
                        SetTotalCount = true
                    };

                    lstWFInstances = WorkflowServiceProxy.SearchWorkFlowInstanceDeep(searchParam);
                    ViewData["TotalCount"] = searchParam.TotalCount != null ? (int)searchParam.TotalCount : WorkflowServiceProxy.GetTotalCountSearchDetails(searchParam);

                }
                catch (WorkFlowException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("", ex.Message);
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("PartialWfInstances", lstWFInstances);
            }

            return View(lstWFInstances);
        }

        /// <summary>
        /// This method is used to gets the workflow instances.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="searchField">search field.</param>
        /// <param name="searchValue">search value.</param>
        /// <param name="sortString">sort string.</param>
        /// <param name="sortDirection">sort direction.</param>
        /// <param name="page">page number.</param>
        /// <param name="pageSize">size of the page.</param>
        /// <returns></returns>
        public JsonResult GetWorkflowInstances(string workflowId,
            string searchField, string searchValue,
            string sortString, string sortDirection, int page, int pageSize = 50)
        {
            if (!string.IsNullOrEmpty(workflowId))
            {
                // search workflow instances
            }

            return Json(new { Error = Resources.WorkFlowInstanceResource.e_InvalidData });
        }

        /// <summary>
        /// This method is used to views the workflow task instances.
        /// </summary>
        /// <param name="workflowId">workflow identifier.</param>
        /// <param name="filterId">filter identifier.</param>
        /// <param name="workflowInstanceId">workflow instance identifier.</param>
        /// <param name="redirectToPage">redirect to page.</param>
        /// <param name="wfSearchInstId">workflow search instance identifier.</param>
        /// <param name="WFInstanceStatus">workflow instance status.</param>
        /// <param name="mapId">map identifier.</param>
        /// <param name="wFName">workflow name.</param>
        /// <param name="page">page.</param>
        /// <param name="pageSize">page size.</param>
        /// <returns></returns>
        public ActionResult ViewWorkflowTaskInstances(string workflowId, string filterId, string workflowInstanceId,
            int redirectToPage, string wfSearchInstId, string WFInstanceStatus, string mapId, string wFName,
            string page, string pageSize)
        {
            ViewData["WFIStatus"] = string.IsNullOrEmpty(WFInstanceStatus) ? string.Empty : WFInstanceStatus;
            ViewData["WFInstId"] = string.IsNullOrEmpty(wfSearchInstId) ? string.Empty : wfSearchInstId;
            ViewData["MapId"] = string.IsNullOrEmpty(mapId) ? string.Empty : mapId;
            ViewData["WfName"] = string.IsNullOrEmpty(wFName) ? string.Empty : wFName;

            ViewData["PageNumber"] = string.IsNullOrEmpty(page) ? string.Empty : page;
            ViewData["PageSize"] = string.IsNullOrEmpty(pageSize) ? string.Empty : pageSize;

            ViewData["WorkflowId"] = workflowId;
            ViewData["FilterId"] = filterId;
            ViewData["redirectToPage"] = redirectToPage;
            

            if (string.IsNullOrEmpty(workflowId) || string.IsNullOrEmpty(workflowInstanceId))
            {
                ModelState.AddModelError("", Resources.WorkFlowInstanceResource.e_InvalidRequest);
            }


            var lstTaskInstances = new List<WorkflowTaskInstance>();
            if (ModelState.IsValid)
            {
                try
                {
                    //var service = new WorkflowService();
                    lstTaskInstances = WorkflowServiceProxy.SearchWorkFlowTaskInstance(new WfSearchParameter
                    {
                        WFId = workflowId,
                        WFInstanceId = workflowInstanceId,
                        TenantId = this.TenantId
                    });
                }
                catch (WorkFlowException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return View(lstTaskInstances);
        }

        /// <summary>
        /// This method is used to searches the workflow instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SearchWFInstance(string wfInstanceStatus, string wfInstanceId, string mapId, string wfName, int page = 1, int pageSize = 10, bool LoadSearch = false)
        {
            List<WorkflowModel> workflowModels = new List<WorkflowModel>();
            WorkflowModel wfModel = new WorkflowModel();
            wfModel.Name = "All";
            workflowModels.Add(wfModel);

            ViewData["WFInstanceStatus"] = new SelectList(GetWfInstanceStatus(), "Key", "Value", string.IsNullOrEmpty(wfInstanceStatus) ? null : wfInstanceStatus);

            //var service = new WorkflowService();
            workflowModels.AddRange(WorkflowServiceProxy.GetWorkflows(TenantContext.GetTenantId("Workflow")));

            ViewData["WFName"] = new SelectList(workflowModels, "Name", "Name", string.IsNullOrEmpty(wfName) ? null : wfName);
            ViewData["WFInstanceId"] = string.IsNullOrEmpty(wfInstanceId) ? null : wfInstanceId;
            ViewData["MapId"] = string.IsNullOrEmpty(mapId) ? null : mapId;

            ViewData["PageNumber"] = page;
            ViewData["PageSize"] = pageSize;
            ViewData["LoadSearch"] = LoadSearch;


            return View();
        }

        /// <summary>
        /// This method is used to searches the workflow instance.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <param name="page">page.</param>
        /// <param name="pageSize">page size.</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult SearchWFInstance(WfSearchParameter parameter, int page = 1, int pageSize = 10)
        {
            ViewData["PageNumber"] = page;
            ViewData["TotalCount"] = 0;
            ViewData["PageSize"] = pageSize;

            if (parameter.WFName == "All")
            {
                parameter.WFName = "";
            }

            if (!string.IsNullOrEmpty(parameter.TaskCurrentExecutionStatus))
            {
                parameter.TaskCurrentExecutionStatusDetails = new List<string>(parameter.TaskCurrentExecutionStatus.Split(','));
            }

            var lstWFInstances = new List<WorkflowInstance>();
            try
            {
                parameter.RecordStart = ((Convert.ToInt32(page) - 1) * pageSize) + 1;
                parameter.RecordEnd = page * pageSize;
                parameter.SetTotalCount = true;
                parameter.TenantId = this.TenantId;

                //var service = new WorkflowService();
                lstWFInstances = WorkflowServiceProxy.SearchWorkFlowInstanceDeep(parameter);

                if (parameter.TotalCount != null)
                {
                    ViewData["TotalCount"] = parameter.TotalCount;
                }
                else
                {
                    ViewData["TotalCount"] = WorkflowServiceProxy.GetTotalCountSearchDetails(parameter);
                }
            }
            catch (WorkFlowException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("", ex.Message);
            }

            FillSearchParamterInViewData(parameter);

            return PartialView("WorkflowInstanceList", lstWFInstances);
        }

        /// <summary>
        /// This method is used to searches the workflow task instance.
        /// </summary>
        /// <param name="parameter">parameter.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SearchWFTaskInstance(WfSearchParameter parameter)
        {
            var lstWFTaskInstances = new List<WorkflowTaskInstance>();
            List<WorkflowModel> workflowModels = new List<WorkflowModel>();
            WorkflowModel wfModel = new WorkflowModel();
            wfModel.Name = "All";
            workflowModels.Add(wfModel);
            workflowModels.AddRange(WorkflowServiceProxy.GetWorkflows(TenantContext.GetTenantId("Workflow")));
            ViewData["WFName"] = new SelectList(workflowModels, "Name", "Name", null);

            return View(lstWFTaskInstances);
        }

        /// <summary>
        /// This method is used to searches the workflow task instance.
        /// </summary>
        /// <param name="parameter">parameter.</param>
        /// <param name="page">page.</param>
        /// <param name="pageSize">size of the page.</param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult SearchWFTaskInstance(WfSearchParameter parameter, int page = 1, int pageSize = 10)
        {
            ViewData["PageNumber"] = page;
            ViewData["TotalCount"] = 0;
            ViewData["PageSize"] = pageSize;

            if (parameter.WFName == "All")
            {
                parameter.WFName = "";
            }

            parameter.TaskCurrentExecutionStatusDetails = new List<string>();
            if (!string.IsNullOrEmpty(parameter.TaskCurrentExecutionStatus))
            {
                parameter.TaskCurrentExecutionStatusDetails = parameter.TaskCurrentExecutionStatus.Split(',').ToList();
                parameter.TaskCurrentExecutionStatus = string.Empty;
            }

            if (!string.IsNullOrEmpty(parameter.TaskCode))
            {
                parameter.TaskCodes = new List<string>(parameter.TaskCode.Split(','));
                parameter.TaskCode = string.Empty;
            }

            if (!string.IsNullOrEmpty(parameter.MapId))
            {
                parameter.MapIds = new List<string>(parameter.MapId.Split(','));
                parameter.MapId = string.Empty;
            }

            if (!string.IsNullOrEmpty(parameter.ActorId))
            {
                parameter.ActorIds = new List<string>(parameter.ActorId.Split(','));
                parameter.ActorId = string.Empty;
            }

            FillWFTaskInstanceSearchParamterInViewData(parameter);

            var lstWFTaskInstances = new List<WorkflowTaskInstance>();
            try
            {
                parameter.RecordStart = ((Convert.ToInt32(page) - 1) * pageSize) + 1;
                parameter.RecordEnd = page * pageSize;
                parameter.SetTotalCount = true;
                parameter.TenantId = this.TenantId;

                lstWFTaskInstances = WorkflowServiceProxy.SearchWorkFlowTaskInstance(parameter);
                ViewData["TotalCount"] = parameter.TotalCount != null ? (int)parameter.TotalCount : WorkflowServiceProxy.GetTotalCountWFTInstanceSearchDetails(parameter);

                List<WorkflowModel> workflowModels = WorkflowServiceProxy.GetWorkflows(TenantContext.GetTenantId("Workflow"));
                ViewData["workflowModels"] = workflowModels.ToDictionary(w => w.Id.ToString(), w => w.Name);
            }
            catch (WorkFlowException ex)
            {
                ExceptionService.HandleException(ex, policyName);
                ModelState.AddModelError("", ex.Message);
            }

            return PartialView("WorkflowTaskInstanceList", lstWFTaskInstances);
        }

        /// <summary>
        /// This method is used to view the workflow instance tasks based on the given workflow instance identifier.
        /// </summary>
        /// <param name="instanceId">workflow instance identifier.</param>
        /// <returns></returns>
        public PartialViewResult ViewWfInstanceTasks(string instanceId)
        {
            return PartialView();
        }

        /// <summary>
        /// This method is used to executes the task.
        /// </summary>
        /// <param name="wfInstanceId">workflow instance identifier.</param>
        /// <param name="taskCode">task code.</param>
        /// <param name="status">status.</param>
        /// <returns></returns>
        public ActionResult ExecuteTask(string wfInstanceId, string taskCode, string status)
        {
            //WorkflowService wfService = new WorkflowService();
            WorkflowInstance wfInstance = WorkflowServiceProxy.SearchWorkFlowInstanceDeep(new WfSearchParameter() { WFInstanceId = wfInstanceId }).FirstOrDefault();
            TaskDefinition taskDefinition = (TaskDefinition)wfInstance.WorkflowDefinition.WorkflowItemDefinitions.Where(x => x is TaskDefinition && (x as TaskDefinition).TaskCode == taskCode).FirstOrDefault();

            ActivityDetails activity = ActivityServiceProxy.GetActivity(wfInstance.WorkflowDefinition.WfId.ToString(), taskDefinition.TypeName, taskDefinition.AssemblyName);

            GetInputDetails(wfInstance);


            if (wfInstance.CompletedTasks != null)
            {
                var TaskInstance = wfInstance.CompletedTasks.Where(x => x.TaskDefinition.TaskCode.Equals(taskCode)).FirstOrDefault();
                if (TaskInstance != null)
                {
                    ViewData["Outcome"] = string.Join(",", TaskInstance.OutComes);
                }
            }

            ViewBag.Activity = activity;
            ViewBag.status = status;
            ViewBag.wfInstanceId = wfInstanceId;
            ViewBag.taskCode = taskCode;
            ViewBag.taskName = taskDefinition != null ? taskDefinition.TaskName : string.Empty;

            return View();
        }

        /// <summary>
        /// This method is used to executes the task.
        /// </summary>
        /// <param name="wfInstanceId">workflow instance identifier.</param>
        /// <param name="taskCode">task code.</param>
        /// <param name="status">status.</param>
        /// <param name="outcome">outcome.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExecuteTask(string wfInstanceId, string taskCode, string status, string outcome)
        {
            //WorkflowService wfService = new WorkflowService();
            WorkflowInstance wfInstance = WorkflowServiceProxy.SearchWorkFlowInstance(new WfSearchParameter() { WFInstanceId = wfInstanceId }).FirstOrDefault();
            IWorkflowInput input = null;
            if (wfInstance != null)
            {
                input = GetWorkflowInput(wfInstance);
            }

            WorkflowInstanceService instanceService = WorkflowInstanceService.GetInstance(Guid.Parse(wfInstanceId), input);
            WorkflowTaskInstanceService taskInstanceService = instanceService.GetActiveTaskInstanceService(taskCode, new TaskActor() { UserId = UserIdentity.UserId });
            taskInstanceService.ChangeTaskOutcomes(taskInstanceService.TaskInstance.Id.ToString(), new List<string>() { outcome });
            status = taskInstanceService.TaskInstance.AffectedExecutionStatus.LastOrDefault();
            return ExecuteTask(wfInstanceId, taskCode, status);
        }

        /// <summary>
        /// This method is used for actor the workflow task instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult ActorWorkflowTaskInstance()
        {
            //WorkflowService wfService = new WorkflowService();
            List<WorkflowTaskInstance> taskInstanceList = WorkflowServiceProxy.GetAllTaskInstanceForActor(new TaskActor() { UserId = UserIdentity.UserId });
            return View(taskInstanceList);
        }

        #region Private Methods

        /// <summary>
        /// Fills the search parameter in view data.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        private void FillSearchParamterInViewData(WfSearchParameter parameter)
        {
            ViewData["WFIStatus"] = string.IsNullOrEmpty(parameter.WFInstanceStatus) ? string.Empty : parameter.WFInstanceStatus;
            ViewData["WFInstId"] = string.IsNullOrEmpty(parameter.WFInstanceId) ? string.Empty : parameter.WFInstanceId;
            ViewData["MapId"] = string.IsNullOrEmpty(parameter.MapId) ? string.Empty : parameter.MapId;
            ViewData["WfName"] = string.IsNullOrEmpty(parameter.WFName) ? string.Empty : parameter.WFName;
        }

        /// <summary>
        /// Gets the wf instance status.
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> GetWfInstanceStatus()
        {
            Dictionary<string, string> InstanceStatus = new Dictionary<string, string>();
            InstanceStatus.Add("", "All");
            InstanceStatus.Add("Created", "Created");
            InstanceStatus.Add("Started", "Started");
            InstanceStatus.Add("Completed", "Completed");

            return InstanceStatus;
        }

        /// <summary>
        /// Fills the WF task instance search parameter in view data.
        /// </summary>
        /// <param name="parameter">The WF Search parameter.</param>
        private void FillWFTaskInstanceSearchParamterInViewData(WfSearchParameter parameter)
        {
            ViewData["WfName"] = parameter.WFName;

            ViewData["WFTaskCode"] = parameter.TaskCodes != null && parameter.TaskCodes.Count > 0
                                    ? string.Join(",", parameter.TaskCodes) : parameter.TaskCode;


            ViewData["WFActorID"] = parameter.ActorIds != null && parameter.ActorIds.Count > 0
                                    ? string.Join(",", parameter.ActorIds) : parameter.ActorId;


            ViewData["WFMapId"] = parameter.MapIds != null && parameter.MapIds.Count > 0
                                    ? string.Join(",", parameter.MapIds) : parameter.MapId;

            ViewData["WfTaskCurrentExecutionStatus"] = parameter.TaskCurrentExecutionStatusDetails != null && parameter.TaskCurrentExecutionStatusDetails.Count > 0
                                                        ? string.Join(",", parameter.TaskCurrentExecutionStatusDetails) : parameter.TaskCurrentExecutionStatus;

        }

        /// <summary>
        /// Gets the input details.
        /// </summary>
        /// <param name="wfInstance">The wf instance.</param>
        private void GetInputDetails(WorkflowInstance wfInstance)
        {
            if (wfInstance == null)
            {
                return;
            }

            IWorkflowInput wfInput = null;

            string inputXml = string.Empty;
            wfInput = wfInstance.WorkflowInput;
            if (wfInput != null && wfInput is WFXmlInput)
            {
                inputXml = ((WFXmlInput)wfInput).XmlInput;
            }
            else
            {
                wfInput = wfInput != null ? wfInput : GetWorkflowInput(wfInstance);
                string inputString = CelloXmlSerializer.SerializeUsingDataContract(wfInput);
                inputXml = XmlHelper.StripNamespaces(inputString);
            }

            if (!string.IsNullOrEmpty(wfInstance.WorkflowDefinition.WfModel.WFXmlTransform))
            {
                ViewData["InputDetails"] = string.IsNullOrEmpty(inputXml) ? string.Empty : XmlTransformer.Transform(inputXml, wfInstance.WorkflowDefinition.WfModel.WFXmlTransform);

            }
            else
            {
                ViewData["TaskDetails"] = GetTaskDetailsFromXml(inputXml);
            }
        }

        /// <summary>
        /// Gets the task details from XML.
        /// </summary>
        /// <param name="inputXml">The input XML.</param>
        /// <returns></returns>
        private static Dictionary<string, string> GetTaskDetailsFromXml(string inputXml)
        {
            if (string.IsNullOrEmpty(inputXml))
            {
                return null;
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(inputXml);
            XmlNodeList serviceNodes = doc.SelectNodes("//ServiceOutput");
            XmlNodeList chilsNodes = serviceNodes != null && serviceNodes.Count > 0 ? serviceNodes[0].ChildNodes : doc.ChildNodes;
            if (chilsNodes == null || chilsNodes.Count < 1)
            {
                return null;
            }

            Dictionary<string, string> taskDetails = new Dictionary<string, string>();
            XmlNodeList nodes = chilsNodes[0].ChildNodes;
            foreach (XmlNode node in nodes)
            {
                if (!string.IsNullOrEmpty(node.InnerText))
                {
                    taskDetails.Add(node.Name, node.InnerText);
                }
            }

            return taskDetails;
        }

        /// <summary>
        /// This method is used to get the workflow input.
        /// </summary>
        /// <param name="wfInstance">The wf instance.</param>
        /// <returns></returns>
        private static IWorkflowInput GetWorkflowInput(WorkflowInstance wfInstance)
        {
            if (string.IsNullOrEmpty(wfInstance.WorkflowDefinition.WfId.ToString()))
            {
                return null;
            }

            //WorkflowService wfService = new WorkflowService();
            WorkflowModel wfModel = WorkflowServiceProxy.GetWorkflow(wfInstance.WorkflowDefinition.WfId.ToString(), wfInstance.WorkflowDefinition.TenantId);
            if (wfModel == null || string.IsNullOrEmpty(wfModel.WFInputFinderType))
            {
                return null;
            }

            IWorkFlowInputFinder wfFinder = (IWorkFlowInputFinder)Activator.CreateInstance(wfModel.WFInputFinderType.Split(',')[0], wfModel.WFInputFinderType.Split(',')[1]).Unwrap();
            Dictionary<string, IWorkflowInput> inputs = wfFinder.GetMappingObjects(new List<string>() { wfInstance.MapId }, wfModel.Id.ToString());
            CelloSaaS.WorkFlow.Model.WorkflowInput input = (inputs == null || inputs.Count < 1 || !inputs.ContainsKey(wfInstance.MapId)) ? null : (WorkflowInput)inputs[wfInstance.MapId];
            if (input != null)
            {
                input.TenantId = UserIdentity.TenantID;
            }

            return input;
        }

        /// <summary>
        /// Executes the pending task.
        /// </summary>
        /// <param name="taskCode">The task code.</param>
        /// <param name="wfInstanceId">The wf instance id.</param>
        /// <param name="outcome">The outcome.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public ActionResult ExecutePendingTask(string taskCode, string wfInstanceId, string outcome, string status = "Started")
        {
            if (!string.IsNullOrEmpty(taskCode) && !string.IsNullOrEmpty(wfInstanceId))
            {
                try
                {
                    WorkflowInstance wfInstance = WorkflowServiceProxy.SearchWorkFlowInstance(new WfSearchParameter() { WFInstanceId = wfInstanceId }).FirstOrDefault();

                    if (wfInstance != null)
                    {
                        IWorkflowInput input = null;

                        input = GetWorkflowInput(wfInstance);

                        WorkflowInstanceService instanceService = WorkflowInstanceService.GetInstance(Guid.Parse(wfInstanceId), input);

                        WorkflowTaskInstanceService taskInstanceService = instanceService.GetActiveTaskInstanceService(taskCode, new TaskActor() { UserId = UserIdentity.UserId });

                        if (taskInstanceService != null)
                        {
                            taskInstanceService.ChangeTaskOutcomes(taskInstanceService.TaskInstance.Id.ToString(), new List<string>() { outcome });
                            status = taskInstanceService.TaskInstance.AffectedExecutionStatus.LastOrDefault();

                            return RedirectToAction("ExecuteTask", new { wfInstanceId, taskCode, status });
                        }
                        else
                        {
                            TempData["Error"] = string.Format(CultureInfo.InvariantCulture, Resources.WorkFlowInstanceResource.e_TaskNotAssigned, taskCode);
                        }
                    }
                    else
                    {
                        TempData["Error"] = Resources.WorkFlowInstanceResource.e_WorkflowInstanceNotAvailable;
                    }
                }
                catch (WorkFlowException ex)
                {
                    ExceptionService.HandleException(ex, policyName);
                    TempData["Error"] = ex.ToString();
                }
            }
            else
            {
                TempData["Error"] = Resources.WorkFlowInstanceResource.e_InvalidParameters;
            }

            return RedirectToAction("ActorWorkflowTaskInstance");
        }

        #endregion
    }
}
