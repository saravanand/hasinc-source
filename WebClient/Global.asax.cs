﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;
using System.Web.Http;
using System.Web.Optimization;

namespace CelloSaaSApplication
{
    /// <summary>
    /// Mvc Http Application class
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ValueProviderFactories.Factories.Add(new CelloSaaS.View.ValueProviders.JsonFormValueProviderFactory());

            RegisterBinders();
            Configure();
            InitRules();
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            GlobalErrorHandler.HandleError(((HttpApplication)sender).Context, Server.GetLastError(), new CelloSaaSApplication.Controllers.ErrorController());
        }

        /// <summary>
        /// Fluent API configurations of module, features, entities, dataviews, rules
        /// </summary>
        private void Configure()
        {
            // Register CelloSaaS modules - do not edit
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.Notification.NotificationModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.WorkFlow.WorkflowModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.DataBackup.DataBackupModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.Integration.IntegrationModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterEntity<CelloSaaS.WorkFlow.WorkflowEntityConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.Configuration.DBCelloModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterEntity<CelloSaaS.Configuration.DBCelloEntityConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterDataView<CelloSaaS.Configuration.DBCelloDataViewConfigurator>();

            // XML mode configuration
            CelloSaaS.Configuration.CelloConfigurator.RegisterModule<CelloSaaS.Configuration.XmlModuleConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterEntity<CelloSaaS.Configuration.XmlEntityConfigurator>();
            CelloSaaS.Configuration.CelloConfigurator.RegisterDataView<CelloSaaS.Configuration.XmlDataViewConfigurator>();

            // insert your configuration here

            //end insert

            CelloSaaS.Configuration.CelloConfigurator.Configure();

            CelloSaaS.Configuration.DalExtensionsConfiguration.Register<CelloSaaS.Services.DataEventHandlers.CURDEventRasieEventHandler>();
            CelloSaaS.Configuration.MeteringPreAndPostProcessConfiguration.AddPostProcess<CelloSaaS.Billing.Services.GenerateAdjustmentInvoiceMeteringPostProcessor>();
        }

        /// <summary>
        /// Inits the rules.
        /// </summary>
        private void InitRules()
        {
            CelloSaaS.Rules.Execution.RuleConfiguration.Register<CelloSaaS.Configuration.CelloRuleConfiguration>();
            CelloSaaS.Rules.Execution.RuleConfiguration.Configure();
        }

        /// <summary>
        /// Register custom model binders
        /// </summary>
        private void RegisterBinders()
        {
            ModelBinders.Binders.DefaultBinder = new CelloSaaS.View.BusinessModelBinder();
            ModelBinders.Binders.Add(typeof(CelloSaaS.Model.DataManagement.ExtendedEntityRow), new CelloSaaS.View.ExtendedEntityRowBinder());
        }
    }
}