﻿using System;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Configuration;

namespace CelloSaaSApplication.Models
{
    public class CookieData
    {
        public string userName { get; set; }

        public string userKey { get; set; }

        public string[] roles { get; set; }

        public string tenantId { get; set; }

        public string sessiontenantid { get; set; }

        public bool onBehalfOfUser { get; set; }

        public string loggedInUserName { get; set; }

        public string loggedInUserId { get; set; }

        public string[] loggedInUserRoles { get; set; }

        public string[] loggedInUserPrivileges { get; set; }

        public string loggedInUserTenantId { get; set; }

        public string[] sessionRoles { get; set; }
    }

    public static class AuthenticationCookieHelper
    {
        /// <summary>
        /// Prepares the cookie data based on the given inputs
        /// </summary>
        /// <param name="cookieData">The cookie data.</param>
        /// <returns></returns>
        private static string PrepareCookieContentFromCookieData(CookieData cookieData)
        {
            const string userDataSubDelimiter = "~";

            StringBuilder sb = new StringBuilder();
            //1st token
            sb.Append(cookieData.userKey);
            sb.Append("|");

            sb.Append((cookieData.roles != null && cookieData.roles.Length > 0) ? String.Join(userDataSubDelimiter, cookieData.roles) : string.Empty);

            // 2
            sb.Append("|");

            sb.Append(!string.IsNullOrEmpty(cookieData.tenantId) ? cookieData.tenantId.ToString() : string.Empty);
            // 3
            sb.Append("|");

            sb.Append(!string.IsNullOrEmpty(cookieData.sessiontenantid) ? cookieData.sessiontenantid.ToString() : string.Empty);
            // 4
            sb.Append("|");

            sb.Append(!cookieData.onBehalfOfUser ? string.Empty : cookieData.onBehalfOfUser.ToString());
            // 5
            sb.Append("|");

            sb.Append(string.IsNullOrEmpty(cookieData.loggedInUserName) ? string.Empty : cookieData.loggedInUserName);
            // 6
            sb.Append("|");

            sb.Append(string.IsNullOrEmpty(cookieData.loggedInUserId) ? string.Empty : cookieData.loggedInUserId);
            // 7
            sb.Append("|");

            sb.Append((cookieData.loggedInUserRoles != null && cookieData.loggedInUserRoles.Length > 0)
                ? string.Join(userDataSubDelimiter, cookieData.loggedInUserRoles)
                : (cookieData.roles != null && cookieData.roles.Length > 0)
                    ? string.Join(userDataSubDelimiter, cookieData.roles)
                    : string.Empty
                );
            // 8
            sb.Append("|");

            sb.Append((cookieData.loggedInUserPrivileges != null && cookieData.loggedInUserPrivileges.Length > 0) ? string.Join(userDataSubDelimiter, cookieData.loggedInUserPrivileges) : string.Empty);
            // 9
            sb.Append("|");

            sb.Append(string.IsNullOrEmpty(cookieData.loggedInUserTenantId) ? string.Empty : cookieData.loggedInUserTenantId);
            // 10
            sb.Append("|");

            sb.Append(cookieData.sessionRoles == null ? string.Empty : string.Join(userDataSubDelimiter, cookieData.sessionRoles));

            return sb.ToString();
        }

        /// <summary>
        /// Prepares the authentication cookie adn then sends out the HttpCookie
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="userID">The user ID.</param>
        /// <param name="roles">The roles.</param>
        /// <param name="tenantId">The tenant id.</param>
        /// <param name="sessionTenantId">The session tenant id.</param>
        /// <returns></returns>
        public static HttpCookie GetAuthenticationCookie(CookieData cookieData)
        {
            string userData = PrepareCookieContentFromCookieData(cookieData);

            AuthenticationSection section = WebConfigurationManager.GetWebApplicationSection("system.web/authentication") as AuthenticationSection;

            TimeSpan ts = section.Forms.Timeout;
            int timeout = (ts.Minutes != 0) ? timeout = ts.Minutes : 1;

            bool isPersistent = Convert.ToBoolean(HttpContext.Current.Request.Form["isPersistent"] ?? "False");

            if (isPersistent) timeout = 30 * 24 * 60;

            //ticket object is formed based on the above details set. Evry page afer login will use this ticket to get base user data
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, cookieData.userName, DateTime.Now,
                DateTime.Now.AddMinutes(timeout), isPersistent, userData, FormsAuthentication.FormsCookiePath);

            // to encrypt the ticket 
            string encryptedCookieString = FormsAuthentication.Encrypt(ticket);

            // setting the ticket to the cookie.
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedCookieString);
            cookie.HttpOnly = true;
            if (isPersistent)
                cookie.Expires = DateTime.Now.AddYears(1);

            return cookie;
        }

    }
}