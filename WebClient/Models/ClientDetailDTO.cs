﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CelloSaaSWebClient.Models
{
    public class ClientDetailDTO
    {
        public bool Status { get; set; }
        public string LogoUri { get; set; }
        public string Secret { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Uri { get; set; }
        public string Id { get; set; }
        public string ScopeRestrictions { get; set; }
        public string ApplicationTypes { get; set; }
        public string ClientType { get; set; }
        public string SigningKeyType { get; set; }
        public string AccessTokenType { get; set; }
        public string TenantId { get; set; }
        public bool RequireConsent { get; set; }
        public bool AllowRememberConsent { get; set; }
        public string Flows { get; set; }
        public string RedirectUris { get; set; }
        public long IdentityTokenLifetime { get; set; }
        public long AccessTokenLifetime { get; set; }
        public long RefreshTokenLifetime { get; set; }
        public long AuthorizationCodeLifetime { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool PublicAccessAllowed { get; set; }
    }
}