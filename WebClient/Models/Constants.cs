﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CelloSaaSApplication.Models
{
    public static class Constants
    {
        public const string MembershipIdExtnFieldName = "Extn_MembershipId";
        public const string DiscountCodeExtnFieldName = "Extn_DiscountCode";
        public const string DefaultPricePlanPickupListId = "59e6b31a-0e67-4cd2-87dc-1d9103b7c692";
        public const string DiscountCodePickListId = "859bb209-d31d-428d-93c0-7c4c3ed3e7a7";

        #region authorization server constants
        public const string CelloOpenIdProviderName = "CelloOpenId";
        public const string WebApplicationType = "InternalWeb";
        #endregion
    }
}

namespace CelloSaaS.ServiceContracts.SettingsManagement
{
    public static class AuthenticationConstants
    {
        public const string CelloAuthentication = "CelloAuth";
    }

    public static class SettingAttributeConstants
    {
        public const string TenantDefaultAuthenticationType = "DefaultAuthenticationType";
        public const string IdProviders = "IdProviders";
        public const string DefaultScopes = "DefaultScopes";
        public const string LoginProvider = "LoginProvider";
    }

}