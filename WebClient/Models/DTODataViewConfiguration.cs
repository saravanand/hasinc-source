﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CelloSaaS.Configuration;
using CelloSaaS.Model.UserManagement;

namespace WebApplication.Models
{
    public class DTODataViewConfiguration : IDataViewConfigurator
    {
        public void Configure(DataViewConfigExpression dataViewConfiguration)
        {
            dataViewConfiguration.Add<UserGrid, UserDetails>();
        }
    }

    /// <summary>
    /// Class for User Grid
    /// </summary>
    public class UserGrid : CelloDataView<UserDetails>
    {
        /// <summary>
        /// It initializes a new instance of the <see cref="UserGrid" /> class.
        /// </summary>
        public UserGrid()
            : base(700)
        {
            DataViewName = "User Grid";
            MainEntity = "User";
            Description = "User Grid";
            SetFeatures(f =>
                f.Add("ManageUser")
                .Add("ManageAllUser")
            );

            SetColumns(col =>
                col.Add("first_name").WithDisplayName("First Name").WithEntityFieldIdentifier("User_FirstName")
                   .Add("last_name").WithDisplayName("Last Name").WithEntityFieldIdentifier("User_LastName")
                   .Add("username").WithDisplayName("User Name").WithEntityFieldIdentifier("User_Username")
                   .Add("email").WithDisplayName("Email").WithEntityFieldIdentifier("User_EmailId")
                   .Add("description").WithDisplayName("Description").WithEntityFieldIdentifier("User_Description")
                   .Add("address.city").WithDisplayName("City").WithEntityFieldIdentifier("User_City").Hide()
                   .Add("address.state").WithDisplayName("State").WithEntityFieldIdentifier("User_State").Hide()
                   .Add("address.country").WithDisplayName("Country").WithEntityFieldIdentifier("User_Country").Hide()
                   .Add("address.zipcode").WithDisplayName("Zipcode").WithEntityFieldIdentifier("User_Zipcode").Hide()
                   .Add("is_locked").WithDisplayName("Is Locked?").WithEntityFieldIdentifier("User_IsLocked")
                );
        }
    }
}