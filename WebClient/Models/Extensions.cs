﻿using CelloSaaS.AuthServer.Core.Ancilliary;
using CelloSaaS.AuthServer.Client.Extensions;
using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceProxies.Configuration;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Security;

namespace CelloSaaS.View
{
    public static class Extensions
    {
        private static string _celloVersion;
        private static string _publiIP, _lanIP;

        /// <summary>
        /// Returns the cellosaas version string
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static string CelloVersion(this HtmlHelper helper)
        {
            if (_celloVersion == null)
            {
                _celloVersion = typeof(CelloSaaS.Model.TenantManagement.Tenant).Assembly.GetName().Version.ToString();
            }

            return _celloVersion;
        }

        /// <summary>
        /// Returns the current server public IP
        /// </summary>
        /// <returns></returns>
        public static string GetServerPublicIP()
        {
            if (string.IsNullOrEmpty(_publiIP) && HttpContext.Current != null)
            {
                var hosts = System.Net.Dns.GetHostAddresses(HttpContext.Current.Request.Url.Host);

                if (hosts.Any(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork))
                {
                    _publiIP = hosts.First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
                }
                else
                {
                    _publiIP = hosts[0].ToString();
                }
            }

            return _publiIP;
        }

        /// <summary>
        /// Returns the current server LAN IP
        /// </summary>
        /// <returns></returns>
        public static string GetServerLANIP()
        {
            if (string.IsNullOrEmpty(_lanIP) && HttpContext.Current != null)
            {
                _lanIP = "NA";
                var nics = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
                foreach (var adapter in nics)
                {
                    foreach (var x in adapter.GetIPProperties().UnicastAddresses)
                    {
                        if (x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            _lanIP = x.Address.ToString();
                            break;
                        }
                    }

                    if (_lanIP != "NA") { break; }
                }
            }

            return _lanIP;
        }

        /// <summary>
        /// Returns the en culture (4.3 supports only USD transactions).
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static CultureInfo GetBillingCulture()
        {
            string cultureTag = ConfigHelper.DefaultCulture;

            if (!string.IsNullOrEmpty(UserIdentity.TenantID))
            {
                var key = UserIdentity.TenantID + AttributeConstants.CurrencyCulture;
                var cacheItem = HttpContext.Current.Items.Contains(key) ? HttpContext.Current.Items[key] : null;

                if (cacheItem == null)
                {
                    var tenantSetting = TenantSettingsProxy.GetTenantSettings(UserIdentity.TenantID);

                    if (tenantSetting != null && tenantSetting.Setting.Attributes.ContainsKey(AttributeConstants.CurrencyCulture))
                    {
                        cultureTag = tenantSetting.Setting.Attributes[AttributeConstants.CurrencyCulture];
                    }
                }

                if (!HttpContext.Current.Items.Contains(key))
                    HttpContext.Current.Items.Add(key, cultureTag);
            }

            var currentCulture = CultureInfo.GetCultureInfoByIetfLanguageTag(cultureTag);
            var newCulture = new System.Globalization.CultureInfo(currentCulture.Name);
            newCulture.NumberFormat.CurrencyNegativePattern = 1;
            return newCulture;
        }

        /// <summary>
        /// Returns the given double value in tenant setting currency format
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static string ToBillingCurrency(this double value)
        {
            return ToBillingCurrency(value, "C");
        }

        /// <summary>
        /// Returns the given double value in tenant setting currency format
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static string ToBillingCurrency(this double value, string format)
        {
            return value.ToString(format, GetBillingCulture());
        }

        /// <summary>
        /// Returns the given double value in tenant setting currency format
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static string ToBillingCurrency(this decimal value, string format = "C")
        {
            return value.ToString(format, GetBillingCulture());
        }

        /// <summary>
        /// Returns the given double value in tenant setting currency format
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static string ToBillingCurrency(this int value, string format = "C")
        {
            return value.ToString(format, GetBillingCulture());
        }

        /// <summary>
        /// Returns the currency symbol according to current culture
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static string GetCurrencySymbol(this HtmlHelper helper)
        {
            return GetBillingCulture().NumberFormat.CurrencySymbol;
        }

        public static BillCycleMode GetStartAndEndDates(TenantLicense tenantLicense, out DateTime startDate, out DateTime endDate, out DateTime chargeDate)
        {
            if (HttpContext.Current != null && HttpContext.Current.Items.Contains("Cello_Billing_StartDate"))
            {
                startDate = Convert.ToDateTime(HttpContext.Current.Items["Cello_Billing_StartDate"]);
                endDate = Convert.ToDateTime(HttpContext.Current.Items["Cello_Billing_EndDate"]);
                chargeDate = Convert.ToDateTime(HttpContext.Current.Items["Cello_Billing_ChargeDate"]);
                return (BillCycleMode)Enum.Parse(typeof(BillCycleMode), HttpContext.Current.Items["Cello_Billing_Mode"].ToString());
            }

            string tenantId = tenantLicense.TenantId;
            Guid gTenantId = Guid.Parse(tenantId);
            BillingSetting billingSetting = null;

            if (tenantId.Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase))
            {
                billingSetting = BillingProxy.GetBillingSetting(BillFrequency.Monthly, gTenantId);

                startDate = DateTime.Today.Day == 1 ? DateTime.Today : DateTime.Today.AddDays(-(DateTime.Today.Day - 1));
                endDate = DateTime.Now;
                chargeDate = DateTime.Now;
            }
            else
            {
                BillFrequency frequency = BillFrequency.Monthly;

                if (!string.IsNullOrEmpty(tenantLicense.PricePlanId))
                {
                    Guid parentTenantId = Guid.Parse(TenantProxy.GetTenantDetailsByTenantId(tenantId, UserIdentity.TenantID).TenantDetails.ParentTenantId);
                    var pricePlan = BillingPlanProxy.GetPricePlan(Guid.Parse(tenantLicense.PricePlanId), parentTenantId);
                    frequency = pricePlan.BillFrequency;
                }

                var previousInvoice = BillingProxy.GetPreviousInvoice(gTenantId);
                billingSetting = BillingProxy.GetBillingSetting(frequency, gTenantId);

                if (billingSetting.Mode == BillCycleMode.Prepaid && previousInvoice != null)
                {
                    startDate = previousInvoice.StartDate;
                    endDate = previousInvoice.EndDate;
                    chargeDate = startDate;
                }
                else
                {
                    CelloSaaS.Billing.Services.BillingSchedulerHelper.GetStartDateAndEndDate(tenantLicense, billingSetting, out startDate, out endDate, out chargeDate, previousInvoice);
                }
            }

            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items["Cello_Billing_StartDate"] = startDate;
                HttpContext.Current.Items["Cello_Billing_EndDate"] = endDate;
                HttpContext.Current.Items["Cello_Billing_ChargeDate"] = chargeDate;
                HttpContext.Current.Items["Cello_Billing_Mode"] = billingSetting.Mode;
            }

            return billingSetting.Mode;
        }

        public static string PickupList(this HtmlHelper helper, string name, string pickupListId, string pickuplistValueId = null, string firstOption = null, string tenantId = null)
        {
            StringBuilder selectList = new StringBuilder("<select name=\"" + name + "\">");

            if (!string.IsNullOrEmpty(firstOption))
            {
                selectList.Append(string.Format("<option value=\"\">{0}</option>", firstOption));
            }

            try
            {
                var clist = PickupListProxy.GetPickupListValues(pickupListId, tenantId ?? TenantContext.GetTenantId(null));

                if (clist != null && clist.Count > 0)
                {
                    clist.ForEach(p =>
                    {
                        string opt = string.Format("<option value=\"{0}\" {2}>{1}</option>", p.ItemId, p.ItemName, (p.ItemId == pickuplistValueId ? "selected" : ""));
                        selectList.Append(opt);
                    });
                }
            }
            catch { }

            selectList.Append("</select>");

            return selectList.ToString();
        }

        public static string PickupList(this HtmlHelper helper, string name, Type enumType, object value = null, string firstOption = null)
        {
            StringBuilder selectList = new StringBuilder("<select name=\"" + name + "\">");

            if (!string.IsNullOrEmpty(firstOption))
            {
                selectList.Append(string.Format("<option value=\"\">{0}</option>", firstOption));
            }

            var clist = enumType.ToSelectList();

            if (clist != null && clist.Count > 0)
            {
                clist.ForEach(p =>
                {
                    string opt = string.Format("<option value=\"{0}\" {2}>{1}</option>", p.Value, p.Text, (p.Value == value.ToString() ? "selected" : ""));
                    selectList.Append(opt);
                });
            }

            selectList.Append("</select>");

            return selectList.ToString();
        }

        public static IEnumerable Errors(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                return modelState.ToDictionary(kvp => kvp.Key,
                    kvp => kvp.Value.Errors
                                    .Select(e => e.ErrorMessage).ToArray())
                                    .Where(m => m.Value.Count() > 0);
            }
            return null;
        }

        public static string ErrorString(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                var errMsg = new StringBuilder();

                var lst = modelState
                            .SelectMany(kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray())
                            .Where(m => m.Count() > 0).ToList();

                lst.ForEach(x => errMsg.Append(x + ";"));

                return errMsg.ToString();
            }

            return null;
        }
    }

    public static class UriExtensions
    {
        public static string GetBaseUrl(this Uri requestUri)
        {
            if (requestUri == null) return null;
            if (ConfigHelper.UsePortNumberinUri)
                return new UriBuilder(requestUri.Scheme, requestUri.DnsSafeHost, requestUri.Port).ToString().TrimEnd('/') + "/";
            else
                return new UriBuilder(requestUri.Scheme, requestUri.DnsSafeHost).ToString().TrimEnd('/') + "/";
        }
    }

    public static class AppSettingHelper
    {
        static string _authServerUri, _defaultScopes, _authServer, _celloOpenIdentity, _celloAuthenticationType, _callbackSegment;
        static Nullable<bool> _clientRequiresConsent, _rememberConsent, _logServiceCalls, _logPerf;
        static int _cookieTimeout;

        public static string GetCallbackSegments()
        {
            if (!string.IsNullOrEmpty(_callbackSegment))
                return _callbackSegment;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AuthorizationCallBackSegments"]))
                _callbackSegment = ConfigurationManager.AppSettings["AuthorizationCallBackSegments"];
            else
                _callbackSegment = "Account/AuthorizationCallback";

            return _callbackSegment;
        }

        public static int GetCookieTimeout()
        {
            if (_cookieTimeout == 0)
                _cookieTimeout = 60;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SessionTimeout"]))
                _cookieTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["SessionTimeout"]);

            return _cookieTimeout;
        }

        public static string GetAuthServerUri()
        {
            if (string.IsNullOrEmpty(_authServerUri))
                _authServerUri = ConfigurationManager.AppSettings["AuthServerUri"];

            return _authServerUri;
        }

        public static string GetAuthIssuer()
        {
            if (string.IsNullOrEmpty(_authServerUri))
                _authServerUri = ConfigurationManager.AppSettings["AuthIssuer"];
            return _authServerUri;
        }

        public static string DefaultScopes
        {
            get
            {
                if (string.IsNullOrEmpty(_defaultScopes))
                    _defaultScopes = ConfigurationManager.AppSettings["DefaultScopes"];

                return _defaultScopes;
            }
            private set { }
        }

        public static bool IsClientRequiringConsent()
        {
            if (_clientRequiresConsent.HasValue)
                return _clientRequiresConsent.Value;

            _clientRequiresConsent = Convert.ToBoolean(ConfigurationManager.AppSettings["ClientsRequireConsent"]);
            return _clientRequiresConsent.Value;
        }

        public static bool IsClientConsentRememberable()
        {
            if (_rememberConsent.HasValue)
                return _rememberConsent.Value;

            _rememberConsent = Convert.ToBoolean(ConfigurationManager.AppSettings["RememberClientsUserConsent"]);
            return _rememberConsent.Value;
        }

        public static bool LoggingServiceCalls
        {
            get
            {
                if (_logServiceCalls.HasValue) return _logServiceCalls.Value;
                _logServiceCalls = Convert.ToBoolean(ConfigurationManager.AppSettings["LoggingServiceCalls"]);
                return _logServiceCalls.Value;
            }
            private set { }
        }

        public static bool PerformanceLogging
        {
            get
            {
                if (_logPerf.HasValue) return _logPerf.Value;
                _logPerf = Convert.ToBoolean(ConfigurationManager.AppSettings["PerformanceLogging"]);
                return _logPerf.Value;
            }
            private set { }
        }

        public static string CelloOpenIdentity
        {
            get
            {
                if (!string.IsNullOrEmpty(_celloOpenIdentity))
                    return _celloOpenIdentity;

                _celloOpenIdentity = ConfigurationManager.AppSettings["CelloOpenIdentity"];
                return _celloOpenIdentity;
            }
            private set { }
        }

        public static string CelloAuthentication
        {
            get
            {
                if (!string.IsNullOrEmpty(_celloAuthenticationType))
                    return _celloAuthenticationType;

                _celloAuthenticationType = ConfigurationManager.AppSettings["CelloAuthentication"];
                return _celloAuthenticationType;
            }
        }

        public static string SocialIdentity
        {
            get
            {
                if (!string.IsNullOrEmpty(_socialIdentity))
                    return _socialIdentity;

                _socialIdentity = ConfigurationManager.AppSettings["SocialIdentity"];
                return _socialIdentity;
            }
            private set { }
        }

        public static string AuthorizationSettingManagerRoles
        {
            get
            {
                if (!string.IsNullOrEmpty(_authMgrRole))
                    return _authMgrRole;

                _authMgrRole = ConfigurationManager.AppSettings["AuthorizationManagerRoles"];
                return _authMgrRole;
            }
            private set { }
        }


        static string _socialIdentity, _authMgrRole, _ldapId;

        public static string LDAPIdentity
        {
            get
            {
                if (!string.IsNullOrEmpty(_ldapId))
                    return _ldapId;

                _ldapId = ConfigurationManager.AppSettings["LDAPIdentity"];
                return _ldapId;
            }
            private set { }
        }

    }

    public static class AuthServerHelper
    {
        /// <summary>
        /// Gets the state for the given domain name
        /// </summary>
        /// <param name="domainName"></param>
        /// <returns></returns>
        public static string GetState(string domainName)
        {
            return Math.Abs(string.Format(CultureInfo.InvariantCulture, "{0}~{1}", "CelloSaaS", domainName).GetHashCode()).ToString();
        }

        /// <summary>
        /// Validates the state for the given domain name and received state
        /// </summary>
        /// <param name="domainName">The domain name</param>
        /// <param name="receivedState">The received state</param>
        /// <returns>
        /// <c>true</c> if the state is valid
        /// <c>false</c> if the state is invalid or expired
        /// </returns>
        public static bool ValidateState(string domainName, string receivedState)
        {
            if (string.IsNullOrEmpty(receivedState)) return false;
            return Math.Abs(string.Format(CultureInfo.InvariantCulture, "{0}~{1}", "CelloSaaS", domainName).GetHashCode())
                    .ToString().Equals(receivedState, StringComparison.OrdinalIgnoreCase);
        }

        public static void GetClientIdSecrets(ExpandoObject clientDetail, out Guid clientId, out string clientSecret)
        {
            clientId = clientDetail.TryGetStringValue("Id").ToGuid();
            clientSecret = clientDetail.TryGetStringValue("Secret");
        }
    }

    public static class RequestExtensions
    {
        /// <summary>
        /// Sets the tenant id and secret for the client end point authorization
        /// </summary>
        /// <param name="request">The HttpRequestMessage</param>
        /// <param name="clientDetail">The Client Detail object</param>
        public static void SetAdminAuthHeadersForClient(this HttpRequestMessage request)
        {
            if (string.IsNullOrEmpty(UserIdentity.TenantID))
                throw new UnauthorizedAccessException("Tenant not identified");

            string secretValue = null;
            if (UserIdentity.TenantSettings.IsNullOrEmptyDictionary())
            {
                var tenantSettings = TenantSettingsProxy.GetTenantSettings(UserIdentity.TenantID);
                if (tenantSettings != null && tenantSettings.Setting != null && tenantSettings.Setting.Attributes != null && tenantSettings.Setting.Attributes.Count > 0 && !tenantSettings.Setting.Attributes.ContainsKey(AttributeConstants.WCFSharedKey))
                {
                    secretValue = tenantSettings.Setting.Attributes[AttributeConstants.WCFSharedKey];
                }
            }
            else
            {
                secretValue = UserIdentity.TenantSettings[AttributeConstants.WCFSharedKey];
            }

            if (string.IsNullOrEmpty(secretValue))
                throw new UnauthorizedAccessException("Tenant not identified");

            SetAuthHeaders(request, UserIdentity.TenantID, secretValue);

            //string authInfo = string.Format(CultureInfo.InvariantCulture, "{0}:{1}", tenantId.ToString(), secretValue);
            //authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            //request.Headers.Authorization = new AuthenticationHeaderValue("basic", authInfo);

        }

        /// <summary>
        /// Sets the Authorization Headers for the client to invoke the authorization end point to obtain the access and id tokens
        /// </summary>
        /// <param name="request">
        /// The request
        /// <see cref="HttpRequestMessage"/>
        /// </param>
        /// <param name="clientDetail">The client Details</param>
        public static void SetAuthHeadersWithClientId(this HttpRequestMessage request, ExpandoObject clientDetail)
        {
            Guid clientId;
            string clientSecret;
            AuthServerHelper.GetClientIdSecrets(clientDetail, out clientId, out clientSecret);
            request.Headers.Add("client_id", clientId.ToString());
            SetAuthHeaders(request, clientId.ToString(), clientSecret);
        }

        private static void SetAuthHeaders(HttpRequestMessage request, string clientId, string clientSecret)
        {
            string authInfo = string.Format(CultureInfo.InvariantCulture, "{0}:{1}", clientId, clientSecret);
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers.Authorization = new AuthenticationHeaderValue("basic", authInfo);
        }

        /// <summary>
        /// Sets the Authorization Headers for the client to invoke the REST Services
        /// </summary>
        /// <param name="request">
        /// The request
        /// <see cref="HttpRequestMessage"/>
        /// </param>
        /// <param name="clientDetail">The client Details</param>
        public static void SetAuthHeaders(this HttpRequestMessage request, ExpandoObject clientDetail)
        {
            Guid clientId;
            string clientSecret;
            AuthServerHelper.GetClientIdSecrets(clientDetail, out clientId, out clientSecret);

            string authInfo = string.Format(CultureInfo.InvariantCulture, "{0}:{1}", clientId.ToString(), clientSecret);
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers.Authorization = new AuthenticationHeaderValue("basic", authInfo);
        }
    }

    public static class HttpClientExtensions
    {
        public static void AddClientAuthHeader(this HttpClient client, ExpandoObject clientDetail)
        {
            Guid clientId;
            string clientSecret;
            AuthServerHelper.GetClientIdSecrets(clientDetail, out clientId, out clientSecret);

            string authInfo = string.Format(CultureInfo.InvariantCulture, "{0}:{1}", clientId.ToString(), clientSecret);
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
        }

        public static HttpClient SetAdminAuthenticationHeaders(this HttpClient client)
        {
            if (string.IsNullOrEmpty(UserIdentity.TenantID))
                throw new UnauthorizedAccessException("Tenant not identified");

            string secretValue = null;
            if (UserIdentity.TenantSettings.IsNullOrEmptyDictionary())
            {
                var tenantSettings = TenantSettingsProxy.GetTenantSettings(UserIdentity.TenantID);
                if (tenantSettings != null && tenantSettings.Setting != null && tenantSettings.Setting.Attributes != null && tenantSettings.Setting.Attributes.Count > 0 && !tenantSettings.Setting.Attributes.ContainsKey(AttributeConstants.WCFSharedKey))
                {
                    secretValue = tenantSettings.Setting.Attributes[AttributeConstants.WCFSharedKey];
                }
            }
            else
            {
                secretValue = UserIdentity.TenantSettings[AttributeConstants.WCFSharedKey];
            }

            if (string.IsNullOrEmpty(secretValue))
                throw new UnauthorizedAccessException("Tenant not identified");

            string authInfo = string.Format(CultureInfo.InvariantCulture, "{0}:{1}", UserIdentity.TenantID, secretValue);
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("basic", authInfo);

            return client;
        }
    }

    public static class CollectionExtensions
    {
        public static bool IsNullOrEmptyEnumerable<T>(this IEnumerable<T> collection)
        {
            return (collection == null || collection.Count() < 1) ? true : false;
        }

        public static bool IsNullOrEmptyDictionary<T, U>(this IDictionary<T, U> dictionary)
        {
            return (dictionary == null || dictionary.Count < 1) ? true : false;
        }
    }
}

namespace CelloSaaS.Model
{
    public static class CustomExtensions
    {
        public static bool IsNullOrEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        /// <summary>
        /// Returns the guid value of the string
        /// </summary>
        /// <param name="input">The guid as a string value</param>
        /// <returns></returns>
        public static Guid ToGuid(this string input)
        {
            return input.IsNullOrEmpty() ? Guid.Empty : Guid.Parse(input);
        }

        public static bool IsNullOrEmpty<T, U>(this Dictionary<T, U> input)
        {
            return (input == null || input.Count < 1);
        }

        public static bool IsNullOrEmpty<T>(this List<T> input)
        {
            return (input == null || input.Count < 1);
        }

        public static bool Contains(this TenantSetting setting, string keyName)
        {
            return !(setting == null || setting.Setting == null || setting.Setting.Attributes == null
                    || setting.Setting.Attributes.Count < 1 || !setting.Setting.Attributes.ContainsKey(keyName));
        }

        public static string GetValue(this TenantSetting setting, string keyName)
        {
            if (!setting.Contains(keyName)) return null;
            return setting.Setting.Attributes[keyName];
        }

        public static string AsQueryParameters(this Dictionary<string, string> routeValues)
        {
            if (routeValues == null || routeValues.Count < 1) return null;

            // Create query string with all values
            return string.Join("&", routeValues.Keys.Select(key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(routeValues[key]))));
        }

        public static bool IsEmpty(this Guid input)
        {
            if (input == null) return true;
            return input == Guid.Empty;
        }
    }
}