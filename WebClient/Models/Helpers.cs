﻿using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaSWebClient.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using CelloSaaSApplication.Models;
using Newtonsoft.Json;
using CelloSaaS.AuthServer.Client.Models;
using CelloSaaS.View;

namespace CelloSaaSWebClient.Models
{
    public class Helpers
    {
        public void IdProviderSetup(Controller controller, string userId, string tenantId)
        {
            var tenantSettings = TenantSettingsProxy.GetTenantSettings(tenantId);
            var providers = AuthenticationService.GetAuthenticationProviders();

            var celloOpenIdProvider = providers.Where(p => p.Value.Equals(Constants.CelloOpenIdProviderName)).FirstOrDefault();

            // Default will be cello id provider
            controller.ViewBag.IdProvider = celloOpenIdProvider.Value;
            controller.ViewBag.IdProviderId = celloOpenIdProvider.Key;

            if (tenantSettings != null && tenantSettings.Setting != null && tenantSettings.Setting.Attributes != null && tenantSettings.Setting.Attributes.Count > 0 && tenantSettings.Setting.Attributes.ContainsKey(SettingAttributeConstants.IdProviders))
            {
                controller.ViewBag.IdProvider = tenantSettings.Setting.Attributes[SettingAttributeConstants.IdProviders];
                controller.ViewBag.IdProviderId = providers.Where(p => p.Value.Equals(controller.ViewBag.IdProvider as string, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                var authTypeCollection = AuthenticationService.GetAuthenticationTypes(controller.ViewBag.IdProvider as string);

                Dictionary<string, string> authenticationTypes = authTypeCollection[controller.ViewBag.IdProvider as string];

                string selectedProvider = null;

                if (!string.IsNullOrEmpty(userId))
                {
                    var userSettings = UserSettingsProxy.GetUserSettings(userId);
                    if (userSettings != null && userSettings.Setting != null && userSettings.Setting.Attributes != null && userSettings.Setting.Attributes.Count > 0 && userSettings.Setting.Attributes.ContainsKey(SettingAttributeConstants.LoginProvider))
                    {
                        selectedProvider = userSettings.Setting.Attributes[SettingAttributeConstants.LoginProvider];
                    }
                }
                controller.ViewBag.Providers = (authenticationTypes != null && authenticationTypes.Count > 0)
                                    ? new SelectList(authenticationTypes, "Key", "Value", selectedProvider)
                                    : null;
            }
        }

        public void IdentitySetup(Controller controller, string tenantIdProvider, string userAuthenticationType, string ldapUri = null)
        {
            var providers = AuthenticationService.GetAuthenticationProviders() ?? new Dictionary<string, string>();

            var authTypeCollection = AuthenticationService.GetAuthenticationTypes(tenantIdProvider) ?? new Dictionary<string, Dictionary<string, string>>();

            InitViewBinding(controller, providers, authTypeCollection);

            controller.ViewBag.UserAuthenticationTypeOptions = Newtonsoft.Json.JsonConvert.SerializeObject(authTypeCollection);
            controller.ViewBag.TenantIdProvider = new SelectList(providers, "Key", "Value", tenantIdProvider);
            controller.ViewBag.UserAuthType = userAuthenticationType;

            if (!string.IsNullOrEmpty(ldapUri))
                controller.ViewBag.OnPremiseLDAPUri = ldapUri;

            if (!string.IsNullOrEmpty(tenantIdProvider) && authTypeCollection.ContainsKey(tenantIdProvider))
            {
                Dictionary<string, string> authTypes = authTypeCollection[tenantIdProvider];

                if (!string.IsNullOrEmpty(userAuthenticationType) && authTypes.ContainsKey(userAuthenticationType))
                {
                    controller.ViewBag.UserAuthType = authTypes[userAuthenticationType];
                }

                if (string.IsNullOrEmpty(userAuthenticationType) && authTypes.Count > 0)
                {
                    userAuthenticationType = authTypes.Keys.ToArray()[0];
                }

                authTypes.Add("", "--Choose an Authentication Type--");
                controller.ViewBag.UserAuthenticationType = new SelectList(authTypes.OrderBy(o => o.Value).ToDictionary(t => t.Key, t => t.Value), "Key", "Value", userAuthenticationType ?? tenantIdProvider);
            }
            else
            {
                controller.ViewBag.UserAuthenticationType = new SelectList(new Dictionary<string, string>(), "Key", "Value", userAuthenticationType ?? tenantIdProvider);
            }
        }

        private static void InitViewBinding(Controller controller, Dictionary<string, string> providers, Dictionary<string, Dictionary<string, string>> authTypeCollection)
        {
            // For the UI Binding in Cascade fashion
            controller.ViewBag.Providers = Newtonsoft.Json.JsonConvert.SerializeObject(providers.Select(x => new { ProviderId = x.Key, Name = x.Value }).ToList());

            List<dynamic> authNTypes = new List<dynamic>();

            foreach (var providedBasedAuthType in authTypeCollection)
            {
                string providerId = providedBasedAuthType.Key;

                foreach (var authtype in providedBasedAuthType.Value)
                {
                    authNTypes.Add(new
                    {
                        AuthTypeId = authtype.Key,
                        ProviderId = providerId,
                        AuthTypeName = authtype.Value
                    });
                }
            }

            // for the UI binding
            controller.ViewBag.AuthTypes = Newtonsoft.Json.JsonConvert.SerializeObject(authNTypes);
        }

        public static Dictionary<string, string> ApplyTenantAuthSettings(string idProvider, string authType, string ldapServerUri)
        {
            Dictionary<string, string> tenantSettings = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(idProvider) && !string.IsNullOrEmpty(authType))
            {
                tenantSettings.Add(CelloSaaS.AuthServer.Client.Models.AuthServerSettingConstants.IdentityProviders, idProvider);
                tenantSettings.Add(CelloSaaS.AuthServer.Client.Models.AuthServerSettingConstants.DefaultAuthenticationType, authType);

                if (idProvider.Equals("221480d5-4ae4-4c9d-9c1d-025716ff33b0", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(ldapServerUri))
                {
                    ldapServerUri = ldapServerUri.TrimEnd('/');
                    var authSettings = new CelloSaaS.AuthServer.Client.Models.AuthorizationSettings
                    {
                        AuthorizationEndpointUri = ldapServerUri + "/authorize",
                        TokenEndpointUri = ldapServerUri + "/api/token",
                        UserInfoEndpointUri = ldapServerUri + "/api/userinfo",
                    };
                    tenantSettings.Add(AuthServerSettingConstants.AuthorizationSettings, JsonConvert.SerializeObject(authSettings));
                }
            }

            return tenantSettings;
        }

        internal static Tuple<string, string> GetUserAuthSettings(string userId, string tenantId)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(tenantId)) return null;

            var tenantSettings = TenantSettingsProxy.GetTenantSettings(tenantId);

            string tenantIdProvider = null, authType = null;

            if (tenantSettings != null && tenantSettings.Setting != null && tenantSettings.Setting.Attributes != null && tenantSettings.Setting.Attributes.Count > 0 && tenantSettings.Setting.Attributes.ContainsKey(SettingAttributeConstants.IdProviders))
                tenantIdProvider = tenantSettings.Setting.Attributes[SettingAttributeConstants.IdProviders];

            if (string.IsNullOrEmpty(tenantIdProvider)) return null;

            tenantIdProvider = tenantIdProvider.ToLowerInvariant();

            var userSettings = UserSettingsProxy.GetUserSettings(userId);

            if (userSettings != null && userSettings.Setting != null && userSettings.Setting.Attributes != null && userSettings.Setting.Attributes.Count > 0 && userSettings.Setting.Attributes.ContainsKey(SettingAttributeConstants.LoginProvider))
                authType = userSettings.Setting.Attributes[SettingAttributeConstants.LoginProvider];

            if (string.IsNullOrEmpty(authType)) return null;

            authType = authType.ToLowerInvariant();

            var authProviders = AuthenticationService.GetAuthenticationProviders();

            var authTypes = AuthenticationService.GetAuthenticationTypes(tenantIdProvider);

            if (authProviders == null || authProviders.Count < 1 || !authProviders.ContainsKey(tenantIdProvider))
                return null;

            if (authTypes == null || authTypes.Count < 1 || !authTypes.ContainsKey(tenantIdProvider) || !authTypes[tenantIdProvider].ContainsKey(authType))
                return null;

            return new Tuple<string, string>(authProviders[tenantIdProvider], authTypes[tenantIdProvider][authType]);
        }
    }

    public static class AppSettingsHelpers
    {
        private static bool? _isdisabled;

        public static bool DisableSSLCheck
        {
            get
            {
                if (_isdisabled == null && !_isdisabled.HasValue)
                {
                    string appSettingValue = System.Configuration.ConfigurationManager.AppSettings["DisableSSLCertificateCheck"];
                    if (!string.IsNullOrEmpty(appSettingValue))
                        _isdisabled = Convert.ToBoolean(appSettingValue);
                    else
                        _isdisabled = false;
                }
                return _isdisabled.Value;
            }
        }
    }
}
