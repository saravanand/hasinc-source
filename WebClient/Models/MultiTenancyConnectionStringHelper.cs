﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WebApplication.Models
{
    public class MultiTenancyConnectionStringHelper
    {
        [DataContract]
        public class DatabaseConnectionMetadata
        {
            [DataMember]
            [Required]
            public string ServerName { get; set; }
            [DataMember]
            [Required]
            public string InstanceName { get; set; }
            [DataMember]
            [Required]
            public string LoginId { get; set; }
            [DataMember]
            [Required]
            public string Password { get; set; }
        }
    }
}