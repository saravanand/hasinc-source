﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CelloSaaSApplication.Models;
using System.Runtime.Serialization;

namespace CelloSaaSApplication.Models
{
    public class ReportBuilderSearchCondition : CelloSaaS.Library.IJQueryDataTableParamModel
    {
        /// <summary>
        /// Request sequence number sent by DataTable,
        /// same value must be returned in response
        /// </summary>

        public int sEcho
        {
            get;
            set;
        }

        /// <summary>
        /// Text used for filtering
        /// </summary>

        public string sSearch
        {
            get;
            set;
        }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>

        public int? iDisplayLength
        {
            get;
            set;
        }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>

        public int? iDisplayStart
        {
            get;
            set;
        }

        /// <summary>
        /// Number of columns in table
        /// </summary>

        public int iColumns
        {
            get;
            set;
        }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>

        public int iSortingCols
        {
            get;
            set;
        }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>

        public string sColumns
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the report id.
        /// </summary>
        /// <value>
        /// The report id.
        /// </value>

        public string ReportId { get; set; }

        /// <summary>
        /// Gets or sets the report object id.
        /// </summary>
        /// <value>
        /// The report object id.
        /// </value>

        public string ReportObjectId { get; set; }

        /// <summary>
        /// Gets or sets the report object type id.
        /// </summary>
        /// <value>
        /// The report object type id.
        /// </value>

        public string ReportObjectTypeId { get; set; }

        /// <summary>
        /// Gets or sets the report object source id.
        /// </summary>
        /// <value>
        /// The report object source id.
        /// </value>

        public string ReportObjectSourceId { get; set; }

        /// <summary>
        /// Gets or sets the dynamic variables.
        /// </summary>
        /// <value>
        /// The dynamic variables.
        /// </value>
        public List<DynamicVariable> dynamicVariablesList { get; set; }
    }

}