﻿using System;
using System.Collections.Generic;

namespace CelloSaaSApplication.Models
{
    public class ReportViewModel
    {
        public string ReportId { get; set; }

        public string Name { get; set; }

        public List<ReportObjectModel> ReportObjects { get; set; }

        public ReportViewModel()
        {
            this.ReportObjects = new List<ReportObjectModel>();
        }
    }

    public class ReportObjectModel
    {
        public string ReportId { get; set; }

        public string ObjectId { get; set; }

        public string Name { get; set; }

        public string TypeId { get; set; }

        public string SourceId { get; set; }

        public int Ordinal { get; set; }

        public int RowsPerPage { get; set; }

        public List<DynamicVariable> DynamicVariables { get; set; }

        public ReportObjectModel()
        {
            this.DynamicVariables = new List<DynamicVariable>();
        }
    }
}