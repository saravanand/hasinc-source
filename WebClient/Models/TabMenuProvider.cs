﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using CelloSaaS.ServiceProxies.DataManagement;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.Library;
using System.Globalization;

namespace CelloSaaSApplication
{
    public class TabMenuProvider : CelloSaaS.View.IMenuProvider
    {
        #region IMenuProvider Members

        /// <summary>
        /// Pagination image for first link
        /// </summary>
        public const string addOn = "AddOn";

        /// <summary>
        /// Renders menu for given sitemap node collection.
        /// </summary>
        /// <param name="helper">Html helper</param>
        /// <param name="urlHelper">Url helper</param>
        /// <param name="menuCssClass">Menu css class</param>
        /// <param name="selectedMenuItemCssClass">Selected menu item css class</param>
        /// <param name="menuHtmlAttributes">Menu html attributes</param>
        /// <param name="rootNode">Sitemap node collection</param>
        /// <returns>Menu tags</returns>
        public string Render(HtmlHelper helper, UrlHelper urlHelper, string menuCssClass, string selectedMenuItemCssClass, object menuHtmlAttributes, SiteMapNodeCollection rootNode)
        {
            if (menuCssClass == null) menuCssClass = string.Empty;
            if (selectedMenuItemCssClass == null) selectedMenuItemCssClass = string.Empty;

            var sb = new StringBuilder();

            // Render each node. This is to render the main menu
            sb.AppendFormat("<ul class=\"first-level {0}\">", menuCssClass);
            foreach (SiteMapNode mainnode in rootNode[0].ChildNodes)
            {
                var childNodes = mainnode.ChildNodes;
                bool hasChildNodes = childNodes.Count > 0;

                sb.AppendFormat(CultureInfo.InvariantCulture, "<li class=\"{0}\">", (hasChildNodes || mainnode.Title.Equals(addOn)) ? "dropdown-submenu" : string.Empty);

                //Add the Url of the main menu.
                string menuUrl = mainnode.Url;
                bool nofollow = menuUrl.Contains("rel=nofollow");

                if (menuUrl.StartsWith("~"))
                {
                    menuUrl = urlHelper.Content(mainnode.Url);
                }

                if (mainnode.Title.Equals(addOn))
                {
                    AddVirtualEntityMenus(helper, selectedMenuItemCssClass, sb, mainnode);
                }
                else
                {
                    string iconClass = mainnode["iconClass"];

                    sb.AppendFormat("<a href=\"{0}\" {3}>{2}<span>{1}</span></a>",
                        nofollow ? "#" : menuUrl, helper.Encode(mainnode.Title),
                        !string.IsNullOrEmpty(iconClass) ? "<i class=\"" + iconClass + "\"></i>&nbsp;" : string.Empty,
                        hasChildNodes ? "class=\"dropdown-toggle\" data-toggle=\"dropdown\"" : "");
                }

                if (hasChildNodes)
                {
                    sb.AppendFormat("<ul class=\"dropdown-menu second-level {0}\">", selectedMenuItemCssClass);
                    foreach (SiteMapNode subnode in childNodes)
                    {
                        var firstLevelChildNodes = subnode.ChildNodes;
                        hasChildNodes = firstLevelChildNodes.Count > 0;

                        sb.AppendFormat(CultureInfo.InvariantCulture, "<li class=\"{0}\">", hasChildNodes ? "dropdown-submenu" : string.Empty);

                        //Add the Url of the main menu.
                        string submenuUrl = subnode.Url;
                        nofollow = submenuUrl.Contains("rel=nofollow");

                        if (submenuUrl.StartsWith("~"))
                        {
                            submenuUrl = urlHelper.Content(subnode.Url);
                        }

                        string iconClass = subnode["iconClass"];

                        sb.AppendFormat("<a href=\"{0}\" {3}>{2}<span>{1}</span></a>", nofollow ? "#" : submenuUrl, helper.Encode(subnode.Title),
                            !string.IsNullOrEmpty(iconClass) ? "<i class=\"" + iconClass + "\"></i>&nbsp;" : string.Empty,
                            hasChildNodes ? "class=\"dropdown-toggle\" data-toggle=\"dropdown\"" : "");

                        if (hasChildNodes)
                        {
                            sb.AppendFormat("<ul class=\"dropdown-menu third-level {0}\">", selectedMenuItemCssClass);
                            foreach (SiteMapNode firstLevelSubNode in firstLevelChildNodes)
                            {
                                var secondLevelChildNodes = firstLevelSubNode.ChildNodes;
                                hasChildNodes = secondLevelChildNodes.Count > 0;

                                sb.AppendFormat(CultureInfo.InvariantCulture, "<li class=\"{0}\">", hasChildNodes ? "dropdown-submenu" : string.Empty);

                                //Add the Url of the main menu.
                                string firstLevelSubMenuUrl = firstLevelSubNode.Url;
                                nofollow = firstLevelSubMenuUrl.Contains("rel=nofollow");

                                if (firstLevelSubMenuUrl.StartsWith("~"))
                                {
                                    firstLevelSubMenuUrl = urlHelper.Content(firstLevelSubNode.Url);
                                }

                                iconClass = firstLevelSubNode["iconClass"];

                                sb.AppendFormat("<a href=\"{0}\" {3}>{2}<span>{1}</span></a>", nofollow ? "#" : firstLevelSubMenuUrl, helper.Encode(firstLevelSubNode.Title),
                                    !string.IsNullOrEmpty(iconClass) ? "<i class=\"" + iconClass + "\"></i>&nbsp;" : string.Empty,
                                    hasChildNodes ? "class=\"dropdown-toggle\" data-toggle=\"dropdown\"" : "");

                                if (hasChildNodes)
                                {
                                    sb.AppendFormat("<ul class=\"dropdown-menu fourth-level {0}\">", selectedMenuItemCssClass);
                                    foreach (SiteMapNode SecondLevelSubNode in secondLevelChildNodes)
                                    {
                                        sb.Append("<li>");

                                        //Add the Url of the main menu.
                                        string SecondLevelSubMenuUrl = SecondLevelSubNode.Url;
                                        nofollow = SecondLevelSubMenuUrl.Contains("rel=nofollow");

                                        if (SecondLevelSubMenuUrl.StartsWith("~"))
                                        {
                                            SecondLevelSubMenuUrl = urlHelper.Content(SecondLevelSubNode.Url);
                                        }

                                        iconClass = SecondLevelSubNode["iconClass"];

                                        sb.AppendFormat("<a href=\"{0}\">{2}<span>{1}</span></a>", nofollow ? "#" : SecondLevelSubMenuUrl, helper.Encode(SecondLevelSubNode.Title),
                                            !string.IsNullOrEmpty(iconClass) ? "<i class=\"" + iconClass + "\"></i>&nbsp;" : string.Empty);

                                        sb.Append("</li>");
                                    }
                                    sb.AppendLine("</ul>");
                                }
                                sb.Append("</li>");
                            }
                            sb.AppendLine("</ul>");
                        }
                        sb.Append("</li>");
                    }
                    sb.AppendLine("</ul>");
                }
                sb.Append("</li>");
            }

            //End list of sub menu
            sb.AppendLine("</ul>");

            return sb.ToString();
        }

        private static void AddVirtualEntityMenus(HtmlHelper helper, string selectedMenuItemCssClass, StringBuilder sb, SiteMapNode mainnode)
        {
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                var stringBuilder = new StringBuilder();
                var dicEntityMetaData = DataManagementProxy.GetAllEntityMetaData();
                if (dicEntityMetaData != null && dicEntityMetaData.Count > 0)
                {
                    foreach (var metaData in dicEntityMetaData.Values.Where(x => !string.IsNullOrEmpty(x.TenantId)))
                    {
                        if (UserIdentity.HasPrivilege(EntityPrivilegePrefix.ViewPrefix + metaData.EntityIdentifier))
                        {
                            stringBuilder.AppendFormat("<li><a href=\"/VirtualEntity/GetVirtualEntityDataViews?entityId={0}&entityName={1}\"><span>Manage {1}</span></a></li>", metaData.EntityIdentifier, metaData.EntityName);
                        }
                    }
                }
                if (stringBuilder.Length > 0)
                {
                    string iconClass = mainnode["iconClass"];
                    sb.AppendFormat("<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">{1}<span>{0}</span></a>", helper.Encode(mainnode.Title), !string.IsNullOrEmpty(iconClass) ? "<i class=\"" + iconClass + "\"></i>&nbsp;" : string.Empty);
                    sb.AppendFormat("<ul class=\"dropdown-menu second-level {0}\">", selectedMenuItemCssClass);
                    sb.Append(stringBuilder);
                    sb.AppendLine("</ul>");
                }
            }
        }

        #endregion
    }
}