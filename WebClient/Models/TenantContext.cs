﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using CelloSaaS.Library;

public static class TenantContext
{
    public const string TenantContextCookieName = "TenantContextCookie";

    public static string GetTenantId(string entityId)
    {
        var val = GetCookieValueAt(entityId, 1);

        if (!string.IsNullOrEmpty(val))
            return val;

        return UserIdentity.TenantID;
    }

    public static string GetTenantName(string entityId)
    {
        var val = GetCookieValueAt(entityId, 2);
        return val;
    }

    private static string GetCookieValueAt(string entityId, int index)
    {
        var cookie = HttpContext.Current.Request.Cookies[TenantContextCookieName];

        if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
        {
            var entityTenants = cookie.Value.Split('^');

            if (entityTenants.Length > 0)
            {
                foreach (var val in entityTenants)
                {
                    var tmp = val.Split('~');
                    if (tmp.Length == 3 && tmp[0] == entityId)
                    {
                        return tmp[index];
                    }
                }
            }
        }

        return string.Empty;
    }

    public static void Clear()
    {
        var response = HttpContext.Current.Response;
        var aCookie = new HttpCookie(TenantContextCookieName);
        aCookie.Expires = DateTime.Now.AddDays(-365);
        response.Cookies.Add(aCookie);
    }
}
