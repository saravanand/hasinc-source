﻿using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceProxies;
using CelloSaaS.Library;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.DataManagement;
using CelloSaaS.ServiceProxies.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CelloSaaSApplication.Models
{
    internal static class TenantExtensions
    {
        internal static ExtendedEntityRow GetDiscountandMembershipExtensions(this Tenant tenant, string discountCode, string membershipId)
        {
            if (tenant == null || string.IsNullOrEmpty(tenant.TenantDetails.ParentTenantId))
            {
                return null;
            }

            ExtendedEntityRow tenantExtendedRow = null;

            if (tenant.ExtendedRow != null && tenant.ExtendedRow.ExtendedEntityColumnValues != null && tenant.ExtendedRow.ExtendedEntityColumnValues.Count > 0)
            {
                tenantExtendedRow = tenant.ExtendedRow;
            }

            // for existing tenant's => from mysettings
            if (!string.IsNullOrEmpty(tenant.TenantDetails.Identifier) && tenantExtendedRow == null)
            {
                tenantExtendedRow = ServiceLocator.Resolve<IDataManagementService>().
                                GetExtendedEntityRow(tenant.EntityIdentifier, tenant.TenantDetails.ParentTenantId, tenant.TenantDetails.Identifier);
            }

            if (tenantExtendedRow == null)
            {
                tenantExtendedRow = new ExtendedEntityRow()
                {
                    ExtendedEntityColumnValues = new Dictionary<string, ExtendedEntityColumn>(),
                    ReferenceId = tenant.Identifier,
                    EntityIdentifier = tenant.EntityIdentifier
                };
            }

            if (tenantExtendedRow.ExtendedEntityColumnValues == null || tenantExtendedRow.ExtendedEntityColumnValues.Count < 1)
            {
                tenantExtendedRow.ExtendedEntityColumnValues = new Dictionary<string, ExtendedEntityColumn>();
            }

            //2. If there are extended rows, check if membership and discount code are there
            if (!tenantExtendedRow.ExtendedEntityColumnValues.ContainsKey(Constants.DiscountCodeExtnFieldName))
            {
                tenantExtendedRow.ExtendedEntityColumnValues.Add(Constants.DiscountCodeExtnFieldName,
                                                    new ExtendedEntityColumn
                                                    {
                                                        EntityFieldIdentifier = Constants.DiscountCodeExtnFieldName,
                                                        Value = discountCode
                                                    });
            }
            else
            {
                //3. If found, update the values
                tenantExtendedRow.ExtendedEntityColumnValues[Constants.DiscountCodeExtnFieldName].Value = discountCode;
            }

            //2. If there are extended rows, check if membership and discount code are there
            if (!tenantExtendedRow.ExtendedEntityColumnValues.ContainsKey(Constants.MembershipIdExtnFieldName))
            {
                tenantExtendedRow.ExtendedEntityColumnValues.Add(Constants.MembershipIdExtnFieldName,
                                                    new ExtendedEntityColumn
                                                    {
                                                        EntityFieldIdentifier = Constants.MembershipIdExtnFieldName,
                                                        Value = membershipId
                                                    });
            }
            else
            {
                //3. If found, update the values
                tenantExtendedRow.ExtendedEntityColumnValues[Constants.MembershipIdExtnFieldName].Value = membershipId;
            }

            //4. Create new and then send back the extended rows
            return tenantExtendedRow;
        }

        internal static string GetDiscountedPricePlanId(string discountCode, string licensePackageId, string parentTenantId, string newPricePlanId)
        {
            if (string.IsNullOrEmpty(licensePackageId))
            {
                return null;
            }

            List<PickupListValue> lstPickupListValues = PickupListProxy.GetAllPickupListValues(Constants.DiscountCodePickListId, parentTenantId);

            // No discount code mapping is made, the discount code is still invalid
            if (lstPickupListValues == null || lstPickupListValues.Count < 1)
            {
                return null;
            }

            IEnumerable<PickupListValue> matches = lstPickupListValues.Where(lpv => lpv.ItemName.Equals(discountCode, StringComparison.OrdinalIgnoreCase));

            // priceplan Id, if there is a match, False otherwise
            string pricePlanId = (matches != null && matches.Count() > 0) ? matches.FirstOrDefault().ItemId : null;

            if (string.IsNullOrEmpty(pricePlanId))
            {
                return null;
            }

            var pricePlans = BillingPlanProxy.GetPricePlansByPackageId(Guid.Parse(licensePackageId), Guid.Parse(parentTenantId));

            // invalid price plan mapping, return the discount code too as invalid
            // check if there is a match for any id in the discounted price plan
            if (pricePlans == null || pricePlans.Count < 1)
            {
                return null;
            }

            List<Guid> discountedPricePlans = pricePlanId.Contains(',')
                                                ? pricePlanId.Split(',').Select(ppl => Guid.Parse(ppl)).ToList()
                                                : new List<Guid> { Guid.Parse(pricePlanId) };

            if (discountedPricePlans == null || discountedPricePlans.Count < 1)
            {
                return null;
            }

            var matchedPricePlans = pricePlans.Keys.Where(ppl => discountedPricePlans.Contains(ppl));

            if (matchedPricePlans.Count() == 0)
            {
                return null;
            }

            if (!matchedPricePlans.Contains(Guid.Parse(newPricePlanId)))
            {
                return matchedPricePlans.First().ToString();
            }

            // there is a valid priceplan for this license package
            return pricePlanId;
        }

        internal static string GetDiscountedPricePlanId(string discountCode, string licensePackageId, string parentTenantId, BillFrequency frequency)
        {
            if (string.IsNullOrEmpty(licensePackageId))
            {
                return null;
            }

            List<PickupListValue> lstPickupListValues = PickupListProxy.GetAllPickupListValues(Constants.DiscountCodePickListId, parentTenantId);

            // No discount code mapping is made, the discount code is still invalid
            if (lstPickupListValues == null || lstPickupListValues.Count < 1)
            {
                return null;
            }

            IEnumerable<PickupListValue> matches = lstPickupListValues.Where(lpv => lpv.ItemName.Equals(discountCode, StringComparison.OrdinalIgnoreCase));

            // priceplan Id, if there is a match, False otherwise
            string pricePlanId = (matches != null && matches.Count() > 0) ? matches.FirstOrDefault().ItemId : null;

            if (string.IsNullOrEmpty(pricePlanId))
            {
                return null;
            }

            var pricePlans = BillingPlanProxy.GetPricePlansByPackageId(Guid.Parse(licensePackageId), Guid.Parse(parentTenantId));

            // filter the price plans by their frequency [monthly / annual
            pricePlans = pricePlans.Where(pp => pp.Value.BillFrequency == frequency).ToDictionary(t => t.Key, t => t.Value);

            // invalid price plan mapping, return the discount code too as invalid
            // check if there is a match for any id in the discounted price plan
            if (pricePlans == null || pricePlans.Count < 1)
            {
                return null;
            }

            List<Guid> discountedPricePlans = pricePlanId.Contains(',')
                                                ? pricePlanId.Split(',').Select(ppl => Guid.Parse(ppl)).ToList()
                                                : new List<Guid> { Guid.Parse(pricePlanId) };

            if (discountedPricePlans == null || discountedPricePlans.Count < 1)
            {
                return null;
            }

            Guid matchedPricePlans = pricePlans.Keys.Where(ppl => discountedPricePlans.Contains(ppl)).FirstOrDefault();

            pricePlanId = (matchedPricePlans != Guid.Empty) ? matchedPricePlans.ToString() : null;

            // there is a valid priceplan for this license package
            return pricePlanId;
        }
    }
}