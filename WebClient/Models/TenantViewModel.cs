﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CelloSaaS.Billing.Model;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;

namespace CelloSaaSApplication.Models
{
    public class TenantViewModel
    {
        public Tenant Tenant { get; set; }

        public TenantDetails TenantDetails { get; set; }

        public ContactDetails TenantContactDetail { get { return this.Tenant != null ? Tenant.ContactDetail : null; } }

        public TenantLicense License { get; set; }

        public PricePlan Plan { get; set; }

        public int TotalUsers { get; set; }

        public int OnlineUsers { get; set; }

        public int TotalChildTenants { get; set; }

        public BillStatistics BillStatistics { get; set; }

        public string Logo { get; set; }

        public Dictionary<string, double> UsageAmounts { get; set; }

        public PackageDetails PackageDetails { get; set; }

        public Dictionary<string, Dictionary<string, double>> ChildUsageAmounts { get; set; }

        public IEnumerable<TenantType> Types { get; set; }

        public PaymentAccountSetting PaymentAccount { get; set; }

        public string DataPartitionName { get; set; }

        public double OverallOverdue { get; set; }

        public List<Tuple<string, double, double, double, bool>> UsageDetails { get; set; }
    }

    public class TenantViewSearch
    {
        public bool? IsNew { get; set; }

        public bool? OnlyActive { get; set; }

        public bool? OnlyTrial { get; set; }

        public bool? OnlyDeactivated { get; set; }

        public bool? OnlyPaying { get; set; }

        public bool? OnlySelfRegistered { get; set; }

        public bool? HasAutoDebit { get; set; }

        public bool? HasOverdue { get; set; }

        public bool? HasOnlineUsers { get; set; }

        public bool? HasUsageAlert { get; set; }

        public bool? HasChildTenants { get; set; }

        public bool? WithoutPaymentAccount { get; set; }

        public string TenantType { get; set; }

        public int? CreatedYear { get; set; }

        public Guid? PackageId { get; set; }

        public TenantViewSort Sort { get; set; }

        public string SearchText { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }
    }

    public enum TenantViewSort
    {
        CreatedOn,
        Name,        
        Revnue
    }

    public class TenantHierarchyViewModel
    {
        public string name { get; set; }

        public string type { get; set; }

        public int size { get; set; }

        public int position { get; set; }

        public string email { get; set; }

        public List<TenantHierarchyViewModel> children { get; set; }
    }
}