﻿using CelloSaaS.Library;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CelloSaaSWebClient.Services
{
    /// <summary>
    /// The Tenant to User Stratification mapper that is used after the tenant is created to stratify the tenant that 
    /// is created to the corresponding user and the ISV admin user
    /// </summary>
    public class AdminStratificationMapper : CelloSaaS.Services.IPostProcessorProvider
    {
        /// <summary>
        /// Stratifies the tenant after the tenant gets activated
        /// </summary>
        /// <param name="referenceId">The reference identifier</param>
        /// <param name="entity">The tenant entity</param>
        /// <param name="args">The variable arguments</param>
        /// <c>true</c> if the post processing succeeds, <c>false</c> otherwise
        public bool PostProcessorActivate(string referenceId, object entity, params object[] args)
        {
            return DoStratification(referenceId);
        }

        private static void ApproveAdminUser(Tenant tenant)
        {
            if (tenant == null)
                return;
            else
                CelloSaaS.ServiceProxies.UserManagement.UserDetailsProxy.ApproveUsers(new string[] { tenant.TenantAdminUserdetail.UserId });
        }

        private static bool DoStratification(string referenceId, bool removeStratification = false)
        {
            try
            {
                if (string.IsNullOrEmpty(referenceId)) return true;

                if (!removeStratification)
                    ServiceLocator.Resolve<ITenantService>().InsertUserRoleTenantMapping(ProductAdminConstants.ProductAdminUserId, RoleConstants.TenantAdmin, referenceId, ProductAdminConstants.ProductAdminTenantId);
                else
                    ServiceLocator.Resolve<ITenantService>().DeleteUserRoleTenants(ProductAdminConstants.ProductAdminUserId, RoleConstants.TenantAdmin, new string[] { referenceId }, ProductAdminConstants.ProductAdminTenantId);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool PostProcessorDelete(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        /// <summary>
        /// This is used to post process the tenant after getting created and then being approved
        /// </summary>
        /// <param name="referenceId">The reference identifier</param>
        /// <param name="entity">The tenant entity</param>
        /// <param name="args">The variable arguments</param>
        /// <c>true</c> if the post processing succeeds, <c>false</c> otherwise
        public bool PostProcessorInsert(string referenceId, object entity, params object[] args)
        {
            var tenantDetails = entity as TenantDetails;

            if (args.Length > 1)
            {
                ApproveAdminUser(args[1] as Tenant);
            }

            if (tenantDetails != null
                && tenantDetails.ApprovalStatus.Equals(TenantApprovalStatus.WAITINGFORAPPROVAL, StringComparison.OrdinalIgnoreCase))
                return true;

            return DoStratification(referenceId);
        }

        public bool PostProcessorPermanentDelete(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        public bool PostProcessorUpdate(string referenceId, object entity, params object[] args)
        {
            string status = null;

            if (args == null || args.Length < 1) return true;

            foreach (var arg in args)
            {
                if (arg is string)
                {
                    status = arg as string;
                    break;
                }
            }

            if (string.IsNullOrEmpty(status)) return true;

            if (status.Equals(TenantApprovalStatus.APPROVED, StringComparison.OrdinalIgnoreCase))
                return DoStratification(referenceId);
            else if (status.Equals(TenantApprovalStatus.REJECTED, StringComparison.OrdinalIgnoreCase))
                return DoStratification(referenceId, true);

            return true;
        }
    }
}