﻿using CelloSaaS.View;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Net.Http;
using System.Web;
using CelloSaaS.AuthServer.Client.Extensions;
using CelloSaaSWebClient.Models;

namespace CelloSaaSWebClient.Services
{
    /// <summary>
    /// The authentication service that will be used to get the identity and the authentication providers from the Authorization Server
    /// </summary>
    public static class AuthenticationService
    {
        static HttpClient _client;

        static AuthenticationService()
        {
#if DEBUG || TEST || STAGING
            _client = new HttpClient(new WebRequestHandler { ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true });
#else
            _client = new HttpClient();
#endif
            if (AppSettingsHelpers.DisableSSLCheck)
                _client = new HttpClient(new WebRequestHandler { ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true });
        }

        /// <summary>
        /// This method is used to get the various registered authentication providers
        /// </summary>
        /// <returns>Collection of authentication providers </returns>
        public static Dictionary<string, string> GetAuthenticationProviders()
        {
            try
            {
                var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
                    {
                        Path = "authentication/getproviders"
                    }.Uri;

                var response = _client.GetAsync(requestUri.ToString()).Result.EnsureSuccessStatusCode();

                var result = JsonConvert.DeserializeObject<List<ExpandoObject>>(response.Content.ReadAsStringAsync().Result);

                var resultantCollection = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

                if (result == null || result.Count < 1) return resultantCollection;

                foreach (var item in result)
                {
                    resultantCollection.Add(item.TryGetStringValue("Id"), item.TryGetStringValue("Name"));
                }

                return resultantCollection.OrderBy(o => o.Value).ToDictionary(t => t.Key, t => t.Value);
            }
            catch (HttpRequestException httpRequestException)
            {
                CelloSaaS.Library.LogService.Write("AuthServer", httpRequestException);
                return null;
            }
        }

        /// <summary>
        /// This method is used to get the authentication types for the provider id if passed as an argument
        /// </summary>
        /// <param name="providerId">The authentication provider identifier</param>
        /// <returns>Collection of Authentication types grouped by the provider identifiers</returns>
        public static Dictionary<string, Dictionary<string, string>> GetAuthenticationTypes(string providerId)
        {
            try
            {
                var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
                    {
                        Path = !string.IsNullOrEmpty(providerId)
                                ? string.Format(CultureInfo.InvariantCulture, "authentication/gettypes?providerId={0}", providerId)
                                : "authentication/gettypes/"
                    }.Uri;

                var response = _client.GetAsync(HttpUtility.UrlDecode(requestUri.ToString())).Result.EnsureSuccessStatusCode();

                var dataResult = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(response.Content.ReadAsStringAsync().Result);

                var result = new Dictionary<string, Dictionary<string, string>>(StringComparer.OrdinalIgnoreCase);

                if (dataResult == null || dataResult.Count < 1)
                    return dataResult;

                foreach (var dr in dataResult)
                {
                    if (!result.ContainsKey(dr.Key))
                        result.Add(dr.Key, new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase));

                    foreach (var at in dr.Value)
                    {
                        if (!result[dr.Key].ContainsKey(at.Key))
                            result[dr.Key].Add(at.Key, null);
                        result[dr.Key][at.Key] = at.Value;
                    }
                }

                return result;
            }
            catch (HttpRequestException requestException)
            {
                CelloSaaS.Library.LogService.Write("AuthServer", requestException);
                return null;
            }
        }
    }
}