﻿using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using System.Data.Common;

namespace CelloSaaSWebClient.Services
{
    public class ClientRegistrationPostProcessor : IPostProcessProvider
    {
        public void RegisterClients(Tenant tenant)
        {
            RegisterClientAndInitSettings(tenant);
        }

        /// <summary>
        ///  This method is get the global pivckup list values and insert the values for the tenant.
        /// </summary>
        /// <param name="tenantId"> Tenant Identifier</param>
        public void Execute(string tenantId)
        {
            try
            {
                var tenantdetail = TenantProxy.GetTenantDetailsByTenantId(tenantId, UserIdentity.TenantID);

                RegisterClientAndInitSettings(tenantdetail);
            }
            catch (DbException dbexception)
            {
                throw new TenantException("Exception while mapping the default client detail to the Tenant.", dbexception);
            }
        }

        private static void RegisterClientAndInitSettings(Tenant tenantdetail)
        {
            if (tenantdetail == null || tenantdetail.TenantDetails == null || tenantdetail.TenantDetails.TenantCode.IsNullOrEmpty())
                throw new TenantException("unable to fetch the tenant details");

            /*ClientDetail client = new ClientDetail
            {
                Name = tenantdetail.TenantDetails.TenantCodeString + " Client",
                Description = tenantdetail.TenantDetails.TenantCodeString + " Client",
                Secret = tenantdetail.TenantAdminMembershipdetail.Password ?? ConfigHelper.DefaultUserPassword,
                Uri = tenantdetail.TenantDetails.URL,
                RedirectUris = Newtonsoft.Json.JsonConvert.SerializeObject(new List<string> { string.Format("{0}/Authentication/AuthorizationCallBack", tenantdetail.TenantDetails.URL) }),
                RequireConsent = true,
                AllowRememberConsent = true,
                ApplicationTypes = "Web",
                Flows = "Code",
                SigningKeyType = "Default",
                AccessTokenType = "JWT",
                CreatedBy = UserIdentity.UserId.ToGuid(),
                CreatedOn = DateTime.UtcNow,
                Status = true,
                TenantId = tenantdetail.Identifier.ToGuid()
            };
            
            
            var clientId = ClientService.AddAsync(client).Result;

            if (string.IsNullOrEmpty(clientId))
            {
                throw new TenantException("Unable to create a client for this tenant");
            }
            
            if (tenantdetail.ExtendedRow != null && tenantdetail.ExtendedRow.ExtendedEntityColumnValues != null
                && tenantdetail.ExtendedRow.ExtendedEntityColumnValues.Count > 0
                && tenantdetail.ExtendedRow.ExtendedEntityColumnValues.ContainsKey("Extn_IdProvider"))
            {
                ITenantSettingsService settingServcie = ServiceLocator.Resolve<ITenantSettingsService>();
                settingServcie.UpdateTenantSettings(new CelloSaaS.Model.SettingsManagement.TenantSetting
                {
                    Setting = new CelloSaaS.Model.SettingsManagement.Settings
                    {
                        Attributes = new Dictionary<string, string>
                            {
                                {
                                    SettingAttributeConstants.IdProviders,tenantdetail.ExtendedRow.ExtendedEntityColumnValues["Extn_IdProvider"].Value
                                }
                            }
                    }
                });
            }*/
        }
    }
}