﻿using CelloSaaS.View;
using CelloSaaSWebClient.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace CelloSaaSWebClient.Services
{
    /// <summary>
    /// This service is used to manage the Application client's for a given tenant in conjunction with the Authorization Server
    /// </summary>
    public static class ClientService
    {
        private static HttpClient _client;
        public static string ExceptionMessage { get; set; }

        static ClientService()
        {
#if DEBUG || TEST || STAGING
            _client = new HttpClient(new WebRequestHandler { ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true });
#else
            _client = new HttpClient();
#endif

            if (AppSettingsHelpers.DisableSSLCheck)
                _client = new HttpClient(new WebRequestHandler { ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true });

        }

        /// <summary>
        /// This method will get the Client details from the Authorization Server REST service
        /// </summary>
        /// <param name="clientId">The client ID</param>
        /// <returns>The client detail object</returns>
        public static async Task<ExpandoObject> GetClientAsync(string clientId)
        {
            var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
            {
                Path = string.Format(CultureInfo.InvariantCulture, "clients/get/{0}", clientId)
            }.Uri;

            _client.SetAdminAuthenticationHeaders();

            var response = await _client.GetAsync(requestUri.ToString());

            if (!response.IsSuccessStatusCode)
            {
                ExceptionMessage = response.Content.ReadAsStringAsync().Result;
                return null;
            }

            return JsonConvert.DeserializeObject<ExpandoObject>(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// This method is used to add a new client to the Authorization Server via the REST service
        /// </summary>
        /// <param name="clientDetail">The client details</param>
        /// <returns>The identifier for the newly created client</returns>
        public async static Task<string> AddClientAsync(dynamic clientDetail)
        {
            var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
            {
                Path = "clients/add"
            }.Uri;

            _client.SetAdminAuthenticationHeaders();

            var content = new ObjectContent<dynamic>(clientDetail, new JsonMediaTypeFormatter());
            var response = await _client.PutAsync(requestUri.ToString(), content);
            if (!response.IsSuccessStatusCode)
            {
                ExceptionMessage = response.Content.ReadAsStringAsync().Result;
                return null;
            }

            return await response.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Updates the client details in the Authorization Server via the REST Service
        /// </summary>
        /// <param name="clientDetail"></param>
        /// <returns></returns>
        public static async Task<int> UpdateClientAsync(ClientDetailDTO clientDetail)
        {
            var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
            {
                Path = "clients/update"
            }.Uri;

            _client.SetAdminAuthenticationHeaders();
            var response = await _client.PostAsync<ClientDetailDTO>(requestUri.ToString(), clientDetail, new JsonMediaTypeFormatter());

            if (!response.IsSuccessStatusCode)
            {
                ExceptionMessage = response.Content.ReadAsStringAsync().Result;
                return -1;
            }

            return JsonConvert.DeserializeObject<int>(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// This method is used to delete the client in the Authorization Server via the REST service
        /// </summary>
        /// <param name="clientId">The client identifier</param>
        /// <returns>The number of records affected</returns>
        public static async Task<int> DeleteClientAsync(string clientId)
        {
            var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
            {
                Path = string.Format(CultureInfo.InvariantCulture, "clients/delete/{0}", clientId)
            }.Uri;

            _client.SetAdminAuthenticationHeaders();
            var response = await _client.DeleteAsync(requestUri.ToString());

            if (!response.IsSuccessStatusCode)
            {
                ExceptionMessage = response.Content.ReadAsStringAsync().Result;
                return -1;
            }

            return JsonConvert.DeserializeObject<int>(await response.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// This method is used to search for the clients based on the given search particulars from the Authorization Server
        /// </summary>
        /// <param name="tenantId">The tenant identifier</param>
        /// <param name="applicationType">
        /// The type of the application like single tenant or multi-tenant client
        /// <see cref="ApplicationTypes"/>
        /// </param>
        /// <param name="clientType">The type of the client like a web client or a native client</param>
        /// <param name="uri">The Unique resource identifier for the client</param>
        /// <param name="Id">The identifier of the client</param>
        /// <returns>Collection of Client Details</returns>
        public static async Task<List<ExpandoObject>> SearchClientsAsync(Guid tenantId, string applicationType, string clientType = null, string uri = null, string Id = null)
        {
            var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
            {
                Path = string.Format(CultureInfo.InvariantCulture, "clients/clients", uri)
            }.Uri;

            dynamic searchCondition = new System.Dynamic.ExpandoObject();
            searchCondition.Uri = uri;
            if (!string.IsNullOrEmpty(applicationType))
                searchCondition.ApplicationTypes = applicationType;
            if (!String.IsNullOrEmpty(clientType))
                searchCondition.ClientType = clientType;

            if (!string.IsNullOrEmpty(Id)) searchCondition.Id = Guid.Parse(Id);

            if (tenantId != Guid.Empty) searchCondition.TenantId = tenantId;

            var response = await _client.PostAsync(requestUri.ToString(), new ObjectContent<dynamic>(searchCondition, new JsonMediaTypeFormatter()));

            if (!response.IsSuccessStatusCode)
            {
                ExceptionMessage = response.Content.ReadAsStringAsync().Result;
                return null;
            }

            return JsonConvert.DeserializeObject<List<ExpandoObject>>(await response.Content.ReadAsStringAsync());
        }
    }
}