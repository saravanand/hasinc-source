﻿using CelloSaaS.View;
using CelloSaaSWebClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;

namespace CelloSaaSWebClient.Services
{
    public static class ConsentsService
    {
        static HttpClient _client;

        static ConsentsService()
        {
#if DEBUG || TEST || STAGING
            _client = new HttpClient(new WebRequestHandler { ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true });
#else
            _client = new HttpClient();
#endif
            if (AppSettingsHelpers.DisableSSLCheck)
                _client = new HttpClient(new WebRequestHandler { ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true });
        }

        public async static void AddSubjects(List<dynamic> clientSubjects)
        {
            var requestUri = new UriBuilder(AppSettingHelper.GetAuthServerUri())
            {
                Path = "clients/addsubjects"
            }.Uri;

            var request = new HttpRequestMessage(HttpMethod.Get, requestUri.ToString());
            request.SetAdminAuthHeadersForClient();

            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            //var response = await _client.PutAsync<List<dynamic>>(requestUri.ToString(), clientSubjects, new JsonMediaTypeFormatter());
        }
    }
}