﻿using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceProxies.TenantManagement;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CelloSaaSWebClient.Services
{
    /// <summary>
    /// The tenant details post processor, this will be invoked during the process of provisioning a Tenant into the system
    /// </summary>
    public class TenantPostProcessor : CelloSaaS.Services.IPostProcessorProvider
    {
        public bool PostProcessorActivate(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        public bool PostProcessorDelete(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        public bool PostProcessorInsert(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        /// <summary>
        /// This method is used to manage the tenant's clients after the tenant is created
        /// </summary>
        /// <param name="referenceId">The identifier of the tenant that is created</param>
        /// <param name="entity">The tenant entity</param>
        /// <param name="args">Variable arguments</param>
        /// <returns>
        /// <c>true</c> if the post processing succeeds, <c>false</c> otherwise
        /// </returns>
        public async System.Threading.Tasks.Task<bool> DoPostProcessorInsert(string referenceId, object entity, params object[] args)
        {
            var tenantDetails = entity as TenantDetails;

            Tenant tenant = null;
            if (args != null && args.Length > 0)
            {
                tenant = args[0] as Tenant;
            }

            // not required to use 
            if (string.IsNullOrEmpty(tenantDetails.URL))
                return true;

            string tenantUri = null;
            if (!string.IsNullOrEmpty(tenantDetails.URL))
                tenantUri = tenantDetails.URL;
            else
            {
                var uri = new UriBuilder
                {
                    Scheme = HttpContext.Current.Request.Url.Scheme,
                    Host = HttpContext.Current.Request.Url.Host
                };

                if (ConfigHelper.UsePortNumberinUri) uri.Port = HttpContext.Current.Request.Url.Port;

                tenantUri = uri.ToString();
            }

            dynamic cd = new System.Dynamic.ExpandoObject();
            cd.Id = Guid.NewGuid();
            cd.TenantId = Guid.Parse(referenceId);
            cd.Name = string.Format(CultureInfo.InvariantCulture, "{0} client", tenantDetails.TenantName);
            cd.Description = string.Format(CultureInfo.InvariantCulture, "{0} client", tenantDetails.TenantName);
            cd.Secret = ConfigHelper.DefaultUserPassword;
            //cd.Uri = tenantUri;
            cd.LogoUri = null;
            cd.ApplicationTypes = CelloSaaS.AuthServer.Core.Models.ApplicationTypes.InternalWeb.ToString("F");
            cd.ClientType = CelloSaaS.AuthServer.Core.Models.ClientTypes.Web.ToString("F");
            cd.RequireConsent = CelloSaaS.View.AppSettingHelper.IsClientRequiringConsent();
            cd.AllowRememberConsent = CelloSaaS.View.AppSettingHelper.IsClientConsentRememberable();
            cd.Flows = "Code";
            cd.RedirectUris = Newtonsoft.Json.JsonConvert.SerializeObject(new List<string>
            {
                tenantUri.TrimEnd('/')+"/"+CelloSaaS.View.AppSettingHelper.GetCallbackSegments()
            });
            cd.SigningKeyType = "Default";
            cd.IdentityTokenLifetime = 0;
            cd.AccessTokenLifetime = 0;
            cd.RefreshTokenLifetime = 0;
            cd.AuthorizationCodeLifetime = 0;
            cd.ScopeRestrictions = null;
            cd.AccessTokenType = "JWT";
            cd.CreatedBy = UserIdentity.UserId;
            cd.CreatedOn = DateTime.UtcNow;
            cd.Status = true;

            try
            {
                string result = await ClientService.AddClientAsync(cd);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex, "GlobalExceptionLogger");
            }

            return true;
        }

        public bool PostProcessorPermanentDelete(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        public bool PostProcessorUpdate(string referenceId, object entity, params object[] args)
        {
            return true;
        }
    }
}