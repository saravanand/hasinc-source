﻿using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceProxies.SettingsManagement;
using System.Collections.Generic;
using System.Linq;
using CelloSaaS.AuthServer.Client.Extensions;

namespace CelloSaaSWebClient.Services
{
    /// <summary>
    /// This User details post processor, will be invoked at various stages of managing the user details by the Cello UserDetails Service
    /// </summary>
    public class UserPostProcessor : CelloSaaS.Services.IPostProcessorProvider
    {
        public bool PostProcessorActivate(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        public bool PostProcessorDelete(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        /// <summary>
        /// This method is invoked after the user is created by a User Details Service
        /// </summary>
        /// <param name="userId">The user identifier</param>
        /// <param name="userEntity">The Userdetails entity</param>
        /// <returns>
        /// <c>true</c> if this operation succeeds, <c>false</c> otherwise
        /// </returns>
        public bool PostProcessorInsert(string userId, UserDetails userEntity)
        {
            var clientDetails = ClientService.SearchClientsAsync(userEntity.MembershipDetails.TenantCode.ToGuid(), CelloSaaSApplication.Models.Constants.WebApplicationType).Result;

            if (clientDetails.IsNullOrEmpty()) return true;

            var tenantSettings = TenantSettingsProxy.GetTenantSettings(userEntity.MembershipDetails.TenantCode);

            var idprovider = tenantSettings.GetValue(SettingAttributeConstants.IdProviders);

            if (string.IsNullOrEmpty(idprovider)) return true;

            var userAccessibleClients = new Dictionary<string, Dictionary<string, string>>();

            userAccessibleClients.Add(userEntity.MembershipDetails.TenantCode, clientDetails.ToDictionary(cd => cd.TryGetStringValue("Id"), cd => idprovider));

            var userSettingValue = Newtonsoft.Json.JsonConvert.SerializeObject(userAccessibleClients);

            UserSettingsProxy.UpdateUserSettings(new UserSettings
                           {
                               AddedBy = UserIdentity.UserId,
                               Setting = new Settings
                               {
                                   Attributes = new Dictionary<string, string>
                                    {
                                        {
                                            SettingAttributeConstants.LoginProvider,userSettingValue
                                        }
                                    }
                               },
                               UserId = userEntity.Identifier
                           });

            return true;
        }

        public bool PostProcessorInsert(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        public bool PostProcessorPermanentDelete(string referenceId, object entity, params object[] args)
        {
            return true;
        }

        public bool PostProcessorUpdate(string referenceId, object entity, params object[] args)
        {
            return true;
        }
    }
}