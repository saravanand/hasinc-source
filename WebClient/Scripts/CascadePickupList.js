﻿function GetChildValues(parentName, childName, parentId, childId, parentValue, childValue) {
    var parentControl = $('select[name="' + parentName + '"]');
    var operationalTenantId = document.getElementById('OperationalTenantId').value;
    var selectedValue = "";
    if (parentControl != null)
        selectedValue = $('select[name="' + parentName + '"]').val();
    else
        selectedValue = parentValue;

    if (selectedValue != "") {

        var element = $("#loading");
        if (jQuery.contains(document.documentElement, element[0])) {
            $(element).show();
        }

        $.getJSON("/CascadePickupList/GetChildPickupListDetails",
        { parentPickupListId: parentId, parentPickupListValueItemId: selectedValue, childPickupListId: childId, parentTenantId: operationalTenantId },
        function (data) {
            if (data != null && data.length > 0) {
                $('select[name="' + childName + '"] option').remove();
                $('select[name="' + childName + '"]').append($("<option value=''> -- Select -- </option>"));
                for (var detailsCount = 2; detailsCount < data.length; detailsCount += 2) {
                    if (childValue != null && childValue != "" && childValue == data[detailsCount]) {
                        $('select[name="' + childName + '"]').append($("<option value=" + data[detailsCount] + " selected> " + data[detailsCount + 1] + "</option>"));
                    }
                    else {
                        $('select[name="' + childName + '"]').append($("<option value=" + data[detailsCount] + "> " + data[detailsCount + 1] + "</option>"));
                    }
                }
            }
            var loadingElement = $("#loading");
            if (jQuery.contains(document.documentElement, loadingElement[0])) {
                $(loadingElement).hide();
            }
        });
    }
    else {
        $('select[name="' + childName + '"] option').remove();
        $('select[name="' + childName + '"]').append($("<option value=''> -- Select -- </option>"));
    }

    $('select[name="' + childName + '"]').trigger('change');
}

function GetChildInitialValue(parentName, childName, parentId, childId, parentValue, childValue) {
    if (parentName != null && parentName != "" && parentId != null && parentId != "" && childId != null && childId != "") {
        var parentControlValue = $('select[name="' + parentName + '"]').val();

        if ((parentValue != null && parentValue != "") || (parentControlValue != null && parentControlValue != "")) {
            GetChildValues(parentName, childName, parentId, childId, parentValue, childValue);
        }
        else {
            var parentControl = $('select[name="' + parentName + '"]');
            if (parentControl != null && parentControl != "") {
                $('select[name="' + childName + '"] option').remove();
                $('select[name="' + childName + '"]').append($("<option value=''> -- Select -- </option>"));
            }
        }
    }
    else {
        $('select[name="' + childName + '"] option').remove();
        $('select[name="' + childName + '"]').append($("<option value=''> -- Select -- </option>"));
    }

    $('select[name="' + childName + '"]').trigger('change');
}