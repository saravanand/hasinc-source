﻿/* global objects / functions */
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

/// <summary> Object to store the arguments for a function </summary>
function Arg(argid, name, ord, type, value, viewElementType) {
    // GUID generated from the client side
    this.ArgId = argid;
    this.Name = name;
    this.Ordinal = ord;
    this.Type = type;
    this.Value = value;
    this.ViewElementType = viewElementType;
}

/// <summary> To Convert Xml to string format </summary>
function XMLToString(oXML) {
    if (window.ActiveXObject && (new XMLSerializer()) === undefined) {
        return oXML.xml;
    }
    return (new XMLSerializer()).serializeToString(oXML);
}

function MapEnumToInputDataType(enumKey) {
    switch (enumKey) {
        case 0:
            return "Integer";
        case 1:
            return "Decimal";
        case 2:
            return "String";
        case 3:
            return "DateTime";
        case 4:
            return "Bool";
        default:
            return "Variable";
        case 5:
            return "Variable";
    }
}

// gets the currently active function id from the stack
function getActiveFunctionId() {
    var fnId = functionIds.pop();
    functionIds.push(fnId);
    return fnId;
}

/* global variables */

var functionType = "Function";
var columnType = "Column";
var staticType = "Static";
var argumentClass = "Arguments";
var argOrdinalClass = "argOrdinal";
var argName = "argName";
var argIdClass = "argId";

var functions = [];
var functionIds = [];

/* Stacks used for the Expression formation */
var functionStack = [];
var expressionStack = [];

/* New Models for forming the expressions */
// this array contains the collection of eFunctions 
var fnCollection = [];
var argsCollection = [];

// -----------------------------------------
// Collection of functoins for the edit mode
// -----------------------------------------
var editFnCollection = [];
var editExpression = null;

// ---------------------------------------------------------
// The clientside implementation for representing a function
// ---------------------------------------------------------
function eFunction(fnId, fnName, fnArgs, fnValue, retType) {
    // function's GUID
    this.FunctionId = fnId;
    // function name
    this.FunctionName = fnName;
    // collection of arguments
    this.FunctionArgs = fnArgs;
    // Will be used to set the values like operators and parenthesis. This and the function arugments are mutually exclusive
    this.FunctionValue = fnValue;
    // the datatype that this function returns
    this.ReturnType = retType;
}

/// <summary> To represent the argument in a function </summary>
function eArgument(fnId, argId, argName, argType, argValue, viewElementType) {
    // the function to which this arguement belongs to
    this.FunctionId = fnId;
    // argument's GUID
    this.ArgumentId = argId;
    // obtained from the inputs definition from the service
    this.ArgumentName = argName;
    // either of these { static/ column / function }
    this.ArgumentType = argType;
    // the translated value or the static input value in case the above type is static ONLY
    this.ArgumentValue = argValue;
    // to idenfity whether this argument is a column or a static type
    this.ViewElementType = viewElementType;
}

function Expression(name, ordinal, functions, isFreeFlowExpr, exprValue, type, exprFields) {
    // name of the expression
    this.Name = name;
    // the position of the expression among the others
    this.Ordinal = ordinal;
    // the functions that make a part of this experssion
    this.FunctionList = functions;
    // to indicate a free flow expression
    this.IsFreeFlowExpression = isFreeFlowExpr;
    // the value of the expression
    this.ExpressionValue = exprValue;
    // to indicate the datatype of this expr, in case of free flow expr, it will be variant
    this.DataType = type;
    // this contains the fields that are being used in the current expression
    this.ExpressionFields = exprFields;
}

function ExpressionColumnInfo(Expression, Alias, Ordinal, Type, Fields, Visibility) {
    // to uniquely identify this col in the UI
    this.ColumnId = guidGenerator();
    // the translated part of the expression / function..
    this.Expression = Expression;
    // the alias name for this column
    this.Alias = Alias;
    // the position of this expression column in the select list of columns
    this.Ordinal = Ordinal;
    // the return type of this Expression
    this.DataType = Type;
    // stores the fields that form the part of the expression column
    this.ExpressionFields = Fields;
    // to indicate whether this field is visible or not
    this.IsVisible = Visibility;
}

var expressionColumnEntityName = "ExprCol";

// ---------------------------------------------------------
// For the setting of the entity field info object from View
// ---------------------------------------------------------
function EntityFieldInfo(dispname, mappedname, type, isepxr, fieldid, entityname, isvisible, isextn, isref, isidentity, isnullable, maxlen, hasprivilege) {
    this.FieldIdentifier = fieldid;
    this.DisplayName = dispname;
    this.MappedFieldName = mappedname;
    this.EntityName = entityname;
    this.IsExtensionField = isextn;
    this.IsReferenceField = isref;
    this.Type = type;
    this.IsIdentity = isidentity;
    this.IsNullable = isnullable;
    this.MaxLength = maxlen;
    this.IsExpressionColumn = isepxr;
    this.IsVisible = isvisible;
    this.HasPrivilege = hasprivilege;
}

// -----------------------------
// get a function form the stack
// -----------------------------
function getFunctionFromStack(fnId) {

    var collection = getCollectionBasedOnMode();

    for (var i = 0; i < collection.length; i++) {
        if (collection[i].FunctionId == fnId) {
            return collection[i];
        }
    }
}
// ---------------------------------------------
// remove a function from the array of functions
// ---------------------------------------------
function removeFuntionFromStack(fnId) {

    var coll = getCollectionBasedOnMode();

    for (var i = 0; i < coll.length; i++) {
        if (coll[i].FunctionId == fnId) {
            coll.splice(i, 1);
            break;
        }
    }
}

// ---------------------------------------------------------------------------------------------
// Gets which array contains all the function based on the current mode of the expression builder
// ---------------------------------------------------------------------------------------------
function getCollectionBasedOnMode() {
    var collection = fnCollection;

    if (expressionBuilderMode === insertMode) {
        collection = fnCollection;
    }
    else if (expressionBuilderMode === editMode) {
        collection = editFnCollection;
    }
    return collection;
}

/* register all the SQL Server Data Types */
var returnDataTypes = [];
returnDataTypes.push("BigInt");
returnDataTypes.push("Binary");
returnDataTypes.push("Bit");
returnDataTypes.push("Char");
returnDataTypes.push("DateTime");
returnDataTypes.push("Decimal");
returnDataTypes.push("Float");
returnDataTypes.push("Image");
returnDataTypes.push("Int");
returnDataTypes.push("Money");
returnDataTypes.push("NChar");
returnDataTypes.push("NText");
returnDataTypes.push("NVarChar");
returnDataTypes.push("Real");
returnDataTypes.push("UniqueIdentifier");
returnDataTypes.push("SmallDateTime");
returnDataTypes.push("SmallInt");
returnDataTypes.push("SmallMoney");
returnDataTypes.push("Text");
returnDataTypes.push("Timestamp");
returnDataTypes.push("TinyInt");
returnDataTypes.push("VarBinary");
returnDataTypes.push("VarChar");
returnDataTypes.push("Variant");
returnDataTypes.push("Xml");
returnDataTypes.push("Udt");
returnDataTypes.push("Structured");
returnDataTypes.push("Date");
returnDataTypes.push("Time");
returnDataTypes.push("DateTime2");
returnDataTypes.push("DateTimeOffset");

// ------------------------------------------------------------------------------------------------
// returns false, if the item is NOT in the array, else returns the index of the item in the array.
// ------------------------------------------------------------------------------------------------
function contains(array, value) {
    if ($.inArray(value, array) < 0) {
        return false;
    }
    else {
        return $.inArray(value, array);
    }
}
// -------------------------------------------------
// get the named function for the enum integer value
// -------------------------------------------------
function getTypeNameFromKey(key) {
    switch (key) {
        case 0:
            return "BigInt";
        case 1:
            return "Binary";
        case 2:
            return "Bit";
        case 3:
            return "Char";
        case 4:
            return "DateTime";
        case 5:
            return "Decimal";
        case 6:
            return "Float";
        case 7:
            return "Image";
        case 8:
            return "Int";
        case 9:
            return "Money";
        case 10:
            return "NChar";
        case 11:
            return "NText";
        case 12:
            return "NVarChar";
        case 13:
            return "Real";
        case 14:
            return "UniqueIdentifier";
        case 15:
            return "SmallDateTime";
        case 16:
            return "SmallInt";
        case 17:
            return "SmallMoney";
        case 18:
            return "Text";
        case 19:
            return "Timestamp";
        case 20:
            return "TinyInt";
        case 21:
            return "VarBinary";
        case 22:
            return "VarChar";
        case 23:
            return "Variant";
        case 25:
            return "Xml";
        case 29:
            return "Udt";
        case 30:
            return "Structured";
        case 31:
            return "Date";
        case 32:
            return "Time";
        case 33:
            return "DateTime2";
        case 34:
            return "DateTimeOffset";
        default:
            return "";
    }
}

var compatibleOperators = [];

var operators = ['+', '-', '*', '/'];

var openParenthesis = "(";
var closeParenthesis = ")";

// -----------------------------------------
// Maps the format type to a readable string
// -----------------------------------------
function MapFormatTypeEnum(enumValue) {
    switch (enumValue) {
        case 0:
            return "Column";
        case 1:
            return "Static";
        case 2:
            return "Function";
        case 3:
            return "Expression";
        case 4:
            return "Variable";
        default:

    }
}

// -----------------------
// Expression Builder Mode
// -----------------------
var insertMode = "insert";
var editMode = "edit";
var expressionBuilderMode = insertMode;

// ------------------------------------------------------------
// Returns a <input type='hidden' based on the given attributes
// ------------------------------------------------------------
function getHiddenField(fieldid, fieldclass, fieldvalue) {

    var hiddenField = "<input type='hidden' ";

    if (fieldid !== undefined && fieldid != null) {
        hiddenField += " id='" + id + "'";
    }
    if (fieldclass !== undefined && fieldclass != null) {
        hiddenField += " class='" + fieldclass + "'";
    }

    if (fieldvalue !== undefined && fieldvalue != null) {
        hiddenField += " value='" + fieldvalue + "'";
    }
    else {
        hiddenField += " value=''";
    }

    return hiddenField + " />";
}