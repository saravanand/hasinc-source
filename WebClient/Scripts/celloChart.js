﻿/*
* CelloSaaS Reporting Module - Charting functions
* Thirdy Party Chart Library - Kendo UI DataViz
*/
(function ($) {

    var CelloChartPlugin = function (el, options) {
        var _options = $.extend(true, {}, {
            theme: "metro",//"default",
            legend: {
                visible: true,
                position: 'bottom'
            },
            tooltip: {
                visible: true
            },
            seriesDefaults: {
                type: "line",
                stack: false,
                labels: {
                    position: "outsideEnd",
                    align: "column",
                    visible: false
                },
                tooltip: {
                    visible: true
                }
            },
            series: [],
            valueAxis: {
                name: 'Value axis label',
                labels: {
                    visible: true
                },
                majorGridLines: { visible: true }
            },
            categoryAxis: {
                majorGridLines: {
                    visible: true
                }
            },
            seriesClick: onSeriesClick
        }, options || {});

        var _el = el;
        var _data = [];

        var _getdata = function (fnCallback) {
            var url = _options.data_url;
            var queryId = _options.queryid;
            var postdata = { "queryId": queryId };

            $.post(url, postdata, function (data) {
                fnCallback(data);
            });
        };

        var _getPieData = function (_data, cf, vf) {
            var items = [];
            if (_data && _data.length > 0) {
                for (i = 0; i < _data.length; ++i) {
                    items.push({ "category": _data[i][cf], "value": _data[i][vf] });
                }
            }
            return items;
        }

        var _getDataAtIndex = function (indexStr) {
            var index = parseInt(indexStr);
            //console.log(_data[index]);
            return (_data && _data[index]) ? _data[index] : [];
        }

        var updateData = function () {
            var chartType = _options.seriesDefaults.type;

            if (chartType == 'pie' || chartType == 'donut') {
                _options.series[0].data = _getPieData();
            } else {
                if ($.isArray(_options.yaxis_data_index) && _options.yaxis_data_index.length > 1) {
                    for (i = 0; i < _options.yaxis_data_index.length && _options.series[i]; i++) {
                        _options.series[i].data = _getDataAtIndex(_options.yaxis_data_index[i]);
                    }
                } else {
                    _options.series[0].data = _getDataAtIndex(_options.yaxis_data_index);
                }

                if ($.isArray(_options.xaxis_data_index) && _options.xaxis_data_index.length > 1) {
                    for (i = 0; i < _options.xaxis_data_index.length && _options.categoryAxis[i]; i++) {
                        _options.categoryAxis[i].categories = _getDataAtIndex(_options.xaxis_data_index[i]);
                    }
                } else {
                    if ($.isArray(_options.categoryAxis)) {
                        _options.categoryAxis[0].categories = _getDataAtIndex(_options.xaxis_data_index);
                    } else {
                        _options.categoryAxis.categories = _getDataAtIndex(_options.xaxis_data_index);
                    }
                }
            }
        };

        var onSeriesClick = function (e) {
            //console.log(e);
            var dataItem = e.dataItem;

            if (!_options.clickQueryDataField || !dataItem[_options.clickQueryDataField])
                return;

            var val = dataItem[_options.clickQueryDataField];

            var $ael = $(val);
            var href = $ael.attr('href');
            var renderarea = $ael.data('renderarea');
            var rendermode = $ael.data('rendermode');

            if (renderarea == 'Window') {
                window.open(href);
            } else {
                //window.open(href, 'Related Query Window', 'width=700,height=500,toolbar=0,menubar=0,location=0,status=0,scrollbars=1,resizable=1,left=0,top=0');
                previewRelatedQuery($ael);
            }
        };

        this.create = function () {
            if (_options.data == null || _options.data.length == 0) {
                if (_options.data_url) {
                    _getdata(function (d) {
                        this._options.data = d; this.create();
                    });
                    return;
                } else {
                    //console.log('No data available to render chart!');
                    //return;
                }
            } else {
                this._data = _options.data;
            }

            //updateData();

            var chart = _el.data("kendoChart");

            if (chart) {
                chart.destroy();
            }

            _options.seriesClick = onSeriesClick;

            _el.kendoChart(_options);
        };

        this.update = function (options) {
            var chart = _el.data("kendoChart");

            _options = $.extend(true, {}, chart.options, options || {});

            if (options) {
                _options.series = options.series;
                _options.categoryAxis = options.categoryAxis;
                _options.valueAxis = options.valueAxis;
            }

            chart.options = _options;
            //updateData();
            chart.refresh();
        };

        this.updateSeries = function (series) {
            if (!series)
                return;
            var chart = _el.data("kendoChart");
            if (chart && chart.options) {
                chart.options.series = series;
                for (i = 0; i < chart.options.series.length; ++i) {
                    chart.options.series[i].data = series[i].data;
                }
            }
            console.log(chart.options.series);
            chart.refresh();
        };

        this.changeType = function (type) {
            var chart = _el.data("kendoChart");
            if (chart && chart.options && chart.options.seriesDefaults)
                chart.options.seriesDefaults.type = type;
            chart.refresh();
        };

        this.getType = function () {
            return _options ? _options.seriesDefaults.type : 'line';
        };

        this.getOptions = function () {
            return _options;
        };

        this.changeTheme = function (theme) {
            if (theme) {
                _options.theme = theme;
                this.create();
            }
        }

        this.toSvg = function () {
            var chart = _el.data("kendoChart");
            var svg = chart.svg().replace(/&/g, "&amp;");
            return svg;
        };

        this.toPng = function () {
            var svg = this.toSvg();
            var canvas = document.createElement('canvas');
            canvg(canvas, svg);
            var png = canvas.toDataURL("image/png");
            $(canvas).remove();
            return png;
        };

        this.toJpeg = function () {
            var svg = this.toSvg();
            var canvas = document.createElement('canvas');
            canvg(canvas, svg);
            var jpeg = canvas.toDataURL("image/jpeg");
            $(canvas).remove();
            return jpeg;
        };

        this.destroy = function () {
            var chart = _el.data("kendoChart");

            if (chart) {
                chart.destroy();
            }

            _el.data('celloChart', null);
        };
    };

    $.fn.celloChart = function (options) {
        var _that = this;
        return this.each(function () {
            if ($(this).data('celloChart')) return;

            var plugin = new CelloChartPlugin($(this), options);

            $(this).data('celloChart', plugin);

            plugin.create();
        });
    };

})(jQuery);

(function ($) {
    var collapsePlugin = function (el, options) {
        var _el = el;
        var _options = $.extend(true, {},
        {
            imageClass: 'ce-icon',
            expand: {
                css: 'fa fa-caret-down',
                title: 'Click to expand this panel!',
                effect: 'slide',
                direction: 'up',
                speed: 300
            },
            collapse: {
                css: 'fa fa-caret-up',
                title: 'Click to collpase this panel!',
                effect: 'slide',
                direction: 'down',
                speed: 300
            }
        }, options || {});

        this.init = function () {
            _el.on('click', function (e) {
                e.preventDefault();

                var $target = $('#' + $(this).data('target'));
                var state = $(this).data('state') || 'open';
                var expandTitle = $(this).data('expand-title') || _options.expand.title;
                var collapseTitle = $(this).data('collapse-title') || _options.collapse.title;

                var icon = $('.' + _options.imageClass, $(this));

                if (state == 'open') {
                    icon.removeClass(_options.expand.css);
                    icon.addClass(_options.collapse.css);
                    $(this).data('state', 'close');
                    $(this).attr('title', expandTitle);
                    $target.hide(_options.collapse.effect, { direction: _options.collapse.direction }, _options.collapse.speed);
                } else {
                    icon.removeClass(_options.collapse.css);
                    icon.addClass(_options.expand.css);
                    $(this).data('state', 'open');
                    $(this).attr('title', collapseTitle);
                    $target.show(_options.expand.effect, { direction: _options.expand.direction }, _options.expand.speed);
                }
            });
        };
    };

    $.fn.celloCollapse = function (options) {
        var _that = this;
        return this.each(function () {
            if ($(this).data('celloCollapse')) return;
            var plugin = new collapsePlugin($(this), options);
            $(this).data('celloCollapse', plugin);
            plugin.init();
        });
    };
})(jQuery);



function downloadChart(el) {
    var queryId = $(el).data('queryid');
    var target = $(el).data('target');
    var chart = $('#' + (target || ('chart-' + queryId))).data('celloChart');
    var png = chart.toPng();
    //console.log(png);
    //var url = png.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
    //window.open(url);
    $(el).attr('href', png);
    //location.href = url;
    return true;
}

var _getPieData = function (_data, cf, vf) {
    var items = [];
    if (_data && _data.length > 0) {
        for (i = 0; i < _data.length; ++i) {
            items.push({ "category": _data[i][cf], "value": _data[i][vf] });
        }
    }
    return items;
}

function getOptions(queryId, data) {
    var theme = $('#chart_theme-' + queryId).val() || "black";
    var odata = $('#frmChartConfiguration-' + queryId).serializeJSON();

    var options = {
        data: data,
        dataSource: {
            data: data
        },
        xaxis_data_index: odata['xaxis.data'],
        yaxis_data_index: odata['yaxis.data'],
        theme: theme,
        title: {
            text: odata['title'],
            visible: odata['title'] && odata['title'].length > 0
        },
        legend: {
            visible: odata['legend.visible'] == "on",
            position: odata['legend.position']
        },
        seriesDefaults: {
            type: odata['chart.type'] || 'line',
            stack: odata['stack.enable'] == "on",
            labels: {
                position: "outsideEnd",
                background: "transparent",
                align: "column",
                visible: odata['xaxis.label.visible'] == "on"
            },
            tooltip: {
                visible: odata['tooltip.visible'] == "on",
                format: odata['tooltip.format'],
                template: odata['tooltip.template']
            }
        },
        series: [{
            name: odata['yaxis.label'],
            title: { text: odata['yaxis.label'], visible: odata['yaxis.label.visible'] == "on" },
            field: "field" + odata['yaxis.data'],
            labels: {            
                format: odata['yaxis.format'],
                template: odata['yaxis.template']
            },
        }],
        categoryAxis: {
            name: odata['xaxis.label'],
            title: { text: odata['xaxis.label'], visible: odata['xaxis.label.visible'] == "on" },
            field: "field" + odata['xaxis.data'],
            majorGridLines: {
                visible: odata['grid.visible'] == "on"
            }
        },
        valueAxis: {

            name: "field" + odata['yaxis.data'],
            title: { text: odata['yaxis.label'], visible: odata['yaxis.label.visible'] == "on" },
            field: "field" + odata['yaxis.data'],
            labels: {
                format: odata['yaxis.format'],
                template: odata['yaxis.template']
            },
            majorGridLines: {
                visible: odata['grid.visible'] == "on"
            }
        },
        tooltip: {
            visible: odata['tooltip.visible'] == "on",
            format: odata['tooltip.format'],
            template: odata['tooltip.template']
        },
        clickQueryDataField: 'field' + odata['chart.clickquerydatafield']
    };

    if ($.isArray(odata['yaxis.data'])) {
        options.valueAxis = [];
        options.series = [];

        for (i = 0; i < odata['yaxis.data'].length; i++) {

            if(odata['yaxis.data'][i] !=odata['xaxis.data'][i] &&  ValueAxisHasValue(options.valueAxis, odata['yaxis.data'][i]))
            {
                var sopt = {
                    name: odata['yaxis.label'][i],
                    title: { text: odata['yaxis.label'][i], visible: odata['yaxis.label.visible'][i] == "on" },
                    field: "field" + odata['yaxis.data'][i],
                    markers: { type: "square" },

                    labels: {
                        format: odata['yaxis.format'][i],
                        template: odata['yaxis.template'][i]
                    },
                    stack: odata['stack.enable'] == "on"
                };
                if (odata['yaxis.axis.visible'][i] == "on") {

                    var vopt = {
                        name: "field" + odata['yaxis.data'][i],
                        title: { text: odata['yaxis.label'][i], visible: odata['yaxis.label.visible'][i] == "on" },
                        field: "field" + odata['yaxis.data'][i],
                        labels: {
                            format: odata['yaxis.format'][i],
                            template: odata['yaxis.template'][i]
                        },
                        majorGridLines: {
                            visible: odata['grid.visible']
                        }
                    };
                    options.valueAxis.push(vopt);
                    sopt.axis = "field" + odata['yaxis.data'][i];
                }
                options.series.push(sopt);
            }                
        }
    }

    if (options.seriesDefaults.type == 'donut') {
        if (data) {
            if ($.isArray(options.series)) {
                for (j = 0; j < options.series.length; j++) {
                    options.series[j].data = _getPieData(data, options.categoryAxis.field, options.series[j].field);
                    options.series[j].field = null;
                }
            }
            else {
                options.series[0].data = _getPieData(data, options.categoryAxis.field, options.series.field);
                options.series[0].field = null;
            }
            options.dataSource = {};
            options.categoryAxis = null;
            options.valueAxis = null;
        }
    }
    else if (options.seriesDefaults.type == 'pie') {
        if (data) {
            options.series[0].data = _getPieData(data, options.categoryAxis.field, options.valueAxis.field);
            options.dataSource = {};
            options.series[0].field = null;
            options.categoryAxis = null;
            options.valueAxis = null;
        }
    }
    //console.log(options);
    return options;
}


function ValueAxisHasValue(option, value) {
    var status = true;

    if ($.isArray(option)) {

        $.each(option, function (index, arr) {
            if (arr != null && arr.name != undefined && arr.name == "field" + value) {
                status = false;
                return false;
            }
        });
    }
    return status;
}

function AddValueAxis(id, queryId) {
    var scount = $('#' + id + ' .c-series:last').data('seriesid') + 1;
    var seriesDiv = $($('#' + id + ' .c-series.c-static')[0].outerHTML);
    seriesDiv.data('seriesid', scount);
    $('.c-seriesname', seriesDiv).text('series ' + scount);

    $('#' + id + ' .c-container').append(seriesDiv);
    updateCelloChart(queryId);
    return false;
}

function RemoveAxes(el, id, queryId) {
    var scount = $('#' + id + ' .c-series').length - 1;

    if (scount == 0) {
        alert('Should have at-least one series');
        return false;
    }

    $(el).closest('.tiles').parent().remove();
    updateCelloChart(queryId);
    return false;
}

function updateCelloChart(queryId) {
    var chart = $("#chart-" + queryId).data('celloChart');
    if (chart) {
        var d = chart._data;
        chart.update(getOptions(queryId, d));
    }
}

$(function () {
    $(document).on("change", ".chart-configuration", function (e) {
        //console.log(e);
        var elem;
        if (e.srcElement) elem = e.srcElement;
        else if (e.target) elem = e.target
        if (elem != null && elem.tagName == "SELECT") {
            var selectedval = $('option:selected', $(elem)).text();
            var targetid = $(elem).data('target');
            if (targetid) {
                $(elem).parent().parent().find('#' + targetid).val(selectedval)

            }
        }
        else if (elem != null && elem.type == "checkbox") {
            var targetid = $(elem).data('target');
            if (targetid) {
                var selectedval = "off";
                if ($(elem).is(":Checked")) {

                    selectedval = "on";
                }
                $(elem).parent().find('#' + targetid).val(selectedval)
            }
        }

        updateCelloChart($(this).data('queryid'));
    });

});