; (function ($) {
    var settings = {};
    var instance = null;

    $(function () {
        $('div[data-celloworkflow=true]').each(function () {
            $(this).celloworkflow({
                edit: $(this).data('edit'),
                wf_id: $(this).data('wf-id'),
                wf_def_id: $(this).data('wf-def-id'),
                filter_id: $(this).data('filter-id'),
            });
        });
    });

    $.fn.celloworkflow = function (options) {
        var that = this;
        settings = $.extend({
            edit: true,
            wf_id: '',
            wf_def_id: '',
            filter_id: '',
            url: {
                get_task: '/TaskDefinition/GetTaksDefinitions',
                get_router: '/TaskDefinition/GetRouterDefinitions',
                add_router: '/WorkflowDesigner/AddRouterConnections',
            }
        }, options);

        init = function () {
            if (settings.edit) {
                // initialise draggable elements.  note: jsPlumb does not do this by default from version 1.3.4 onwards.
                $(".min-window").draggable({
                    revert: "Invalid",
                    helper: "clone",
                    cursor: "move"
                });

                $(".activity-window").draggable({
                    revert: "Invalid",
                    helper: "clone",
                    cursor: "move"
                });

                that.droppable({
                    accept: ".min-window",
                    drop: function (event, ui) {
                        var type = $(ui.draggable).attr("Id");
                        var id = guidGenerator();
                        var off = ui.helper.position();//ui.position;
                        var top = off.top;
                        var left = off.left - 190;
                        var $newElement = AddNewTaskToDOM(that, id, type, top, left, null, settings.edit);
                        makeSource($newElement);

                        AddTask($newElement.attr("id"), $newElement.data("type"), function (data) {
                            if (!data.Error) {
                                if ($newElement.data("type") != 'Router' && $newElement.data("type") != 'IfElseRouter') {
                                    $newElement.data('taskCode', data.TaskCode);
                                    $newElement.data('taskName', data.TaskName);
                                    $newElement.data('Ordinal', data.Ordinal);
                                    $('span.edit', $newElement).text(data.TaskName);
                                    $newElement.append('<div class="ordinal">' + data.Ordinal + '</div>');
                                } else {
                                    $newElement.data('routerName', data.RouterName);
                                    $('span.edit', $newElement).text(data.RouterName);
                                }
                            }
                        });
                    }
                });
            }

            // setup some defaults for jsPlumb.	
            instance = jsPlumb.getInstance({
                Endpoint: ["Dot", { radius: 1 }],
                HoverPaintStyle: { strokeStyle: "#1e8151", lineWidth: 2 },
                PaintStyle: { lineWidth: 2, strokeStyle: "#5c96bc" },
                ConnectionOverlays: [
                    ["Arrow", {
                        location: 1,
                        id: "arrow",
                        length: 5,
                        width: 8,
                        foldback: 0.8
                    }],
                    ["Label", { label: "", id: "label" }]
                ]
            });

            instance.setRenderMode(jsPlumb.SVG);
        };

        bindEvents = function () {
            // bind a click listener to each connection; the connection is deleted.
            instance.bind("click", function (conn) {
                var routerId = findRouterId(conn);

                if (routerId == null && conn.routerId == null) {
                    return;
                }

                if (routerId == null || routerId == false) {
                    routerId = conn.routerId;
                }

                var inputTaskId, outputTaskId, elseOutputId;

                var nodeType = conn.getParameter("NodeType");

                if (conn.sourceId == routerId) {
                    if (nodeType == 'Else') {
                        elseOutputId = conn.targetId;
                    }
                    else if (nodeType == 'If') {
                        outputTaskId = conn.targetId;
                    }
                    else {
                        outputTaskId = conn.targetId;
                    }
                } else if (conn.targetId == routerId) {
                    inputTaskId = conn.sourceId;
                } else {
                    outputTaskId = conn.targetId;
                    inputTaskId = conn.sourceId;
                }

                RemoveRouterConnections(routerId, inputTaskId, outputTaskId, elseOutputId);

                instance.detach(conn);
            });

            // bind a listener when a new connection is made
            instance.bind("connection", function (conn, e) {
                //conn.connection.getOverlay("label").setLabel(conn.connection.id);
                if (conn.connection.scope == "initialLoad")
                    return;

                var routerId = findRouterId(conn);
                var isVisible = true;

                if (routerId == null) {
                    return false;
                } else if (routerId == false) {
                    // attach the router id to connection object
                    conn.connection.routerId = routerId = guidGenerator();
                    isVisible = false;
                }

                var inputTaskId;
                var outputTaskId;
                var elseOutputId;
                if (conn.sourceId == routerId) {
                    var nodeType = conn.connection.getParameter("NodeType");
                    if (nodeType == 'Else') {
                        elseOutputId = conn.targetId;
                    }
                    else {
                        outputTaskId = conn.targetId;
                    }
                } else if (conn.targetId == routerId) {
                    inputTaskId = conn.sourceId;
                } else {
                    outputTaskId = conn.targetId;
                    inputTaskId = conn.sourceId;
                }

                var psData = {
                    workflowDefinitionId: settings.wf_def_id,
                    routerId: routerId,
                    inputTaskId: inputTaskId,
                    outputTaskId: outputTaskId,
                    isVisible: isVisible,
                    elseOutputId: elseOutputId
                };

                $.post(settings.url.add_router, psData, function (data) {
                    if (data.Error) {
                        console.log('Error while adding router connections. ' + data.Error);
                    }
                });
            });

            instance.bind("beforeDrop", function (conn, e) {
                var routerId = findRouterId(conn);
                if (routerId == null) return false;
                return true;
            });
        };

        loadRouter = function () {
            $.post(settings.url.get_router, { workflowId: settings.wf_id, workflowDefinitionId: settings.wf_def_id }, function (data) {
                var j = 0, i = 0;
                for (i = 0; i < data.length; i++) {
                    if (data[i].RouterId) {
                        if (data[i].IsVisible) {
                            var routerType = 'Router';
                            if (data[i].TypeString == 'IfElse') {
                                routerType = 'IfElseRouter';
                            }
                            var newElement = AddNewTaskToDOM(that, data[i].RouterId, routerType, data[i].UIModel.OffsetTop, data[i].UIModel.OffsetLeft, data[i].RouterName, settings.edit);
                            makeSource(newElement);

                            newElement.data('routerName', data[i].RouterName);
                            newElement.data('routerconditions', data[i].Conditions);

                            if (data[i].UIModel.Width != 0)
                                newElement.width(data[i].UIModel.Width);
                            if (data[i].UIModel.Height != 0)
                                newElement.height(data[i].UIModel.Height);

                            var taskIds = data[i].InputTaskIds.split(',');
                            for (j = 0; j < taskIds.length && $('#' + taskIds[j]).length > 0; ++j) {
                                try {
                                    instance.connect({ target: data[i].RouterId, source: taskIds[j], scope: "initialLoad", anchor: "TopCenter" });
                                } catch (e) { console.log(e); }
                            }

                            taskIds = data[i].OutputTaskIds.split(',');
                            if (data[i].TypeString == 'IfElse') {
                                for (j = 0; j < taskIds.length && $('#' + taskIds[j]).length > 0; ++j) {
                                    try {
                                        instance.connect({ target: taskIds[j], source: data[i].RouterId, scope: "initialLoad", parameters: { "NodeType": "If" }, anchor: "LeftMiddle", connector: "Straight" });
                                    } catch (e) { console.log(e); }
                                }

                                taskIds = data[i].ElseOutputIds.split(',');
                                for (j = 0; j < taskIds.length && $('#' + taskIds[j]).length > 0; ++j) {
                                    try {
                                        instance.connect({ target: taskIds[j], source: data[i].RouterId, scope: "initialLoad", parameters: { "NodeType": "Else" }, anchor: "RightMiddle", connector: "Straight" });
                                    } catch (e) { console.log(e); }
                                }
                            }
                            else {
                                for (j = 0; j < taskIds.length && $('#' + taskIds[j]).length > 0; ++j) {
                                    try {
                                        instance.connect({ target: taskIds[j], source: data[i].RouterId, scope: "initialLoad" });
                                    } catch (e) { console.log(e); }
                                }
                            }
                        } else {
                            try {
                                var connect = instance.connect({ target: data[i].OutputTaskIds, source: data[i].InputTaskIds, scope: "initialLoad" });
                                connect.routerId = data[i].RouterId;
                            } catch (e) {
                                console.log(data[i]);
                                console.log(e);
                            }
                        }
                    }
                }

                if (!settings.edit)
                    $('.w').css('cursor', 'default');
            });
        };

        load = function () {
            $.post(settings.url.get_task, { workflowId: settings.wf_id, workflowDefinitionId: settings.wf_def_id }, function (data) {
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].TaskId;
                    var newElement = AddNewTaskToDOM(that, id, data[i].TaskType, data[i].UIModel.OffsetTop, data[i].UIModel.OffsetLeft, data[i].TaskName, settings.edit);
                    makeSource(newElement);

                    newElement.data('taskCode', data[i].TaskCode);
                    newElement.data('Ordinal', data[i].Ordinal);
                    newElement.append('<div class="ordinal">' + data[i].Ordinal + '</div>');

                    // manual
                    if (data[i].TaskType == 'Manual') {
                        newElement.data('roleIds', data[i].ActorDef.Roles);
                        newElement.data('roleNames', data[i].ActorDef.RoleNames);
                        newElement.data('userNames', data[i].ActorDef.UserNames);
                        newElement.data('stepUrl', data[i].ActorDef.Url);
                        newElement.data('isRuntimeCalculated', data[i].ActorDef.IsRuntimeCalculated);
                    }

                    if (data[i].Conditions) {
                        newElement.data('StartConditions', data[i].Conditions.StartConditions);
                        newElement.data('ExpireConditions', data[i].Conditions.ExpireConditions);
                        newElement.data('CompleteConditions', data[i].Conditions.CompleteConditions);
                        newElement.data('SkipConditions', data[i].Conditions.SkipConditions);
                        newElement.data('OverrideConditions', data[i].Conditions.OverrideConditions);
                    }

                    if (data[i].UIModel.Width != 0)
                        newElement.width(data[i].UIModel.Width);
                    if (data[i].UIModel.Height != 0)
                        newElement.height(data[i].UIModel.Height);

                    if (data[i].ActivityId) {
                        var desc = 'Description: ' + data[i].ActivityDescription;
                        desc += ', Outcomes: ';
                        if (data[i].Outcomes) {
                            desc += data[i].Outcomes.join(',');
                        } else {
                            desc += '---';
                        }

                        AddActivityToDOM(newElement, guidGenerator(), id, data[i].ActivityId, data[i].ActivityName, desc, settings.edit);
                    }
                }
                loadRouter();
            });
        };

        makeSource = function (el) {
            //console.log(el);
            instance.draggable(el);

            // suspend drawing and initialise.
            instance.doWhileSuspended(function () {
                var objType = el.data('type');
                if (objType != "IfElseRouter") {
                    instance.makeSource(el, {
                        filter: ".ep",				// only supported by jquery
                        anchor: "Continuous",
                        deleteEndpointsOnDetach: false,
                        connector: ["StateMachine", { curviness: 0.01 }],
                        connectorStyle: { strokeStyle: "#5c96bc", lineWidth: 2, outlineColor: "transparent", outlineWidth: 4 },
                        maxConnections: -1
                    });
                } else {
                    AddEndpoints(el);
                }

                // make it connectable
                instance.makeTarget(el, {
                    dropOptions: { hoverClass: "dragHover" },
                    anchor: "Continuous"
                });
            });
        };

        init();
        bindEvents();
        load();

        var windows = $(".w", that);

        windows.each(function () {
            makeSource($(this));
        });

        return this; // end of fn.celloworkflow
    }

    $(document).on('click', '.deleteTask', function (event) {
        var id = $(this).attr("id").split("_")[1];

        if ($('#' + id).data('type') == 'Router' || $('#' + id).data('type') == 'IfElseRouter') {
            RemoveRouter(id);
            instance.select({ source: id }).each(function (conn) {
                RemoveConnEndpoints(conn);
            });
            instance.select({ target: id }).each(function (conn) {
                RemoveConnEndpoints(conn);
            });
        } else {
            RemoveTask(id);
            var eps = instance.getEndpoints(id);
            if (eps) {
                for (var e = 0; e < eps.length; e++) {
                    var conn = eps[e].connections;
                    for (var i = 0; i < conn.length; i++) {
                        RemoveRouterConnections(conn[i].routerId, conn[i].sourceId, conn[i].targetId);
                        RemoveConnEndpoints(conn[i]);
                    }
                }
            }
        }

        instance.removeAllEndpoints(id);

        $("#" + id).remove();
    });

    function RemoveConnEndpoints(conn) {
        if (conn && conn.endpoints && conn.endpoints.length > 0) {
            for (var e = 0; conn.endpoints != null && e < conn.endpoints.length; e++) {
                instance.deleteEndpoint(conn.endpoints[e]);
            }
        }
    }

    $(document).on('click', '.deleteActivity', function (event) {
        var id = $(this).attr("id").split("_")[1];
        RemoveActivity($('#' + id).data('taskId'), $('#' + id).data('activityId'));
        $("#" + id).remove();
    });

    function AddEndpoints(parent) {
        var id = $(parent).attr("Id");

        var ifEndpoint = {
            endpoint: ["Rectangle", { radius: 10 }],
            paintStyle: { fillStyle: "#316b31" },
            isSource: true,
            connectorStyle: { strokeStyle: "#5c96bc", lineWidth: 2, outlineColor: "transparent", outlineWidth: 4 },
            HoverPaintStyle: { strokeStyle: "#1e8151", lineWidth: 2 },
            maxConnections: -1,
            deleteEndpointsOnDetach: false,
            parameters: { "NodeType": "If" },
            dropOptions: { hoverClass: "dragHover" }
        };

        var elseEndpoint = {
            endpoint: ["Rectangle", { radius: 10 }],
            paintStyle: { fillStyle: "#FF0000" },
            isSource: true,
            connectorStyle: { strokeStyle: "#5c96bc", lineWidth: 2, outlineColor: "transparent", outlineWidth: 4 },
            HoverPaintStyle: { strokeStyle: "#1e8151", lineWidth: 2 },
            maxConnections: -1,
            deleteEndpointsOnDetach: false,
            parameters: { "NodeType": "Else" },
            dropOptions: { hoverClass: "dragHover" }
        };

        var dropEndpoint = {
            endpoint: ["Rectangle", { radius: 10 }],
            paintStyle: { fillStyle: "#800080" },
            isTarget: true,
            connectorStyle: { strokeStyle: "#5c96bc", lineWidth: 2, outlineColor: "transparent", outlineWidth: 4 },
            HoverPaintStyle: { strokeStyle: "#1e8151", lineWidth: 2 },
            maxConnections: -1,
            deleteEndpointsOnDetach: false,
            parameters: { "NodeType": "IfElseDrop" },
            dropOptions: { hoverClass: "dragHover" }
        };

        instance.addEndpoint(id, { anchor: "LeftMiddle" }, ifEndpoint);
        instance.addEndpoint(id, { anchor: "RightMiddle" }, elseEndpoint);
        instance.addEndpoint(id, { anchor: "TopCenter" }, dropEndpoint);
    }

    function RemoveRouterConnections(routerId, inputTaskId, outputTaskId, elseOutputId) {
        var psData = { workflowDefinitionId: settings.wf_def_id, routerId: routerId, inputTaskId: inputTaskId, outputTaskId: outputTaskId, elseOutputId: elseOutputId };
        $.post("/WorkflowDesigner/RemoveRouterConnections", psData, function (data) {
        });
    }

    function findRouterId(conn) {
        var sourceType = $('#' + conn.sourceId).data('type');
        var targetType = $('#' + conn.targetId).data('type');
        if (conn.sourceId == conn.targetId
            || (sourceType == 'Router' && targetType == 'Router')
            || (sourceType == 'IfElseRouter' && targetType == 'IfElseRouter')) {
            return null;
        } else if (sourceType == 'Router' || sourceType == 'IfElseRouter') {
            return conn.sourceId;
        } else if (targetType == 'Router' || targetType == 'IfElseRouter') {
            return conn.targetId;
        }
        return false;
    }

    function AddNewTaskToDOM(targetElement, id, objType, top, left, name, edit) {
        var newElement = $('<div class="w"></div>');

        newElement.css({ "position": 'absolute' });
        newElement.offset(
            {
                top: top || 10,
                left: left || 10
            });

        newElement.addClass(objType);

        newElement.attr("id", id);
        newElement.data("type", objType);

        if (objType == "Start") {
            name = name || "Start Step";
        }
        else if (objType == "Manual") {
            name = name || "Manual Step";
            newElement.append("<div class='activity-drop'></div>");
        }
        else if (objType == "Auto") {
            name = name || "Auto Step";
            newElement.append("<div class='activity-drop'></div>");
        }
        else if (objType == "Router") {
            name = name || "Router";
        }
        else if (objType == "IfElseRouter") {
            name = name || "If Else Router";
        }
        else if (objType == "Close") {
            name = name || "End Step";
        }

        newElement.append('<span class="edit">' + name + '</span>');

        if (edit)
            newElement.append("<span class='deleteTask' title='Click To Delete!' id=\"imgId_" + id + "\" style=\"float: right;\"><i class='fa fa-trash-o'></i></span>");

        if (objType != "Start" && objType != "Close" && objType != "Router" && objType != "IfElseRouter") {
            newElement.resizable({ maxWidth: 250, maxHeight: 300, minWidth: 100, minHeight: 50, stop: repaintDesign });
            newElement.append("<span class='defineTask' title='Edit Step Details' id=\"define_" + id + "\" style=\"float: right;\"><i class='fa fa-search'></i></span>");
        } else if (objType == 'Router' || objType == 'IfElseRouter') {
            newElement.resizable({ maxWidth: 250, maxHeight: 250, minWidth: 50, minHeight: 50, stop: repaintDesign });
            newElement.append("<span class='defineRouter' title='Edit Routing Conditions' id=\"define_" + id + "\" style=\"float: right;\"><i class='fa fa-search'></i></span>");
        }

        if (objType != "Close") {
            var epStyle = edit && objType != 'IfElseRouter' ? '' : 'display:none;';
            var className = objType == 'IfElseRouter' ? 'epIfElseRouter' : 'ep';
            newElement.append('<div class="' + className + '" style="' + epStyle + '" title="Drag to make connections!" ></div>');
        }

        if (edit) {
            $(".activity-drop", newElement).droppable({
                accept: ".activity-window",
                drop: function (event, ui) {
                    if ($('.activity-w', $(this)).length > 0) {
                        return false;
                    }

                    var activityId = $(ui.draggable).attr("Id");
                    var taskId = $(this).parent().attr('id');
                    var id = guidGenerator();

                    AddActivityToDOM(this, id, taskId, activityId, $(ui.draggable).text(), $(ui.draggable).attr('title'), edit);

                    AddActivity(taskId, activityId);
                }
            });
        }

        targetElement.append(newElement);
        return newElement;
    }

    function repaintDesign(e, ui) {
        instance.repaint($(ui.element));
    }

    function AddActivityToDOM(taskElement, id, taskId, activityId, name, description, edit) {
        $(taskElement).append("<div class='activity-w'><div>");
        var activityElement = $(taskElement).find(".activity-w");

        activityElement.attr("id", id);
        activityElement.data("activityId", activityId);
        activityElement.data('taskId', taskId);
        if (edit)
            activityElement.append("<span title='Click To Delete!' class='deleteActivity' id=\"imgId_" + id + "\" style=\"float: right;\"><i class='fa fa-trash-o'></i></span>");

        activityElement.append(name);
        activityElement.attr('title', description);
    }

    function AddTask(taskId, taskType, callback) {
        var psdata = {};
        if (taskType == 'Router' || taskType == 'IfElseRouter') {
            var type = taskType == 'IfElseRouter' ? 'IfElse' : 'Default';
            psdata = { workflowDefinitionId: settings.wf_def_id, RouterId: taskId, Type: type };
            $.post("/WorkflowDesigner/AddRouter", psdata, function (data) {
                if (callback)
                    callback(data);
            }, 'json');

        } else {
            psdata = { workflowDefinitionId: settings.wf_def_id, taskDefinitionId: taskId, taskType: taskType };
            $.post("/TaskDefinition/AddTaskDefinition", psdata, function (data) {
                if (callback)
                    callback(data);
            }, 'json');
        }
    }

    function RemoveTask(taskId) {
        var psdata = { workflowDefinitionId: settings.wf_def_id, taskDefinitionId: taskId };
        $.post("/TaskDefinition/DeleteTaskDefinition", psdata, function (data) {

        }, 'json');
    }

    function RemoveRouter(routerId) {
        var psdata = { workflowDefinitionId: settings.wf_def_id, routerId: routerId };
        $.post("/WorkflowDesigner/DeleteRouter", psdata, function (data) {

        }, 'json');
    }

    function AddActivity(taskId, activityId) {
        var psdata = { workflowId: settings.wf_id, workflowDefinitionId: settings.wf_def_id, taskDefinitionId: taskId, activityId: activityId };
        $.post("/TaskDefinition/AddActivity", psdata, function (data) {

        }, 'json');
    }

    function RemoveActivity(taskId, activityId) {
        var psdata = { workflowId: settings.wf_id, workflowDefinitionId: settings.wf_def_id, taskDefinitionId: taskId, activityId: activityId };
        $.post("/TaskDefinition/RemoveActivity", psdata, function (data) {

        }, 'json');
    }

    function guidGenerator() {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };
}(jQuery));