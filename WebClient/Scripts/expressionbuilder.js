﻿$(function () {
    // load all the available list of functions during the load time itself
    $.ajax({
        url: "/Expressions/GetAllFunctions/",
        type: "POST",
        success: function (data) {

            if (data == undefined) {
                alert("Exception occured while fetching functions");
                return false;
            }

            $("#baseFunctions option").each(function () {
                $(this).remove();
            });

            var functionsList = $("#baseFunctions");

            //functionsList.append(new Option("Choose A Function", "-1", false, true));

            functionsList.append($("<option />").attr("value", "-1").text("Choose A Function"));

            var result = data;

            if (result.Error !== undefined && result.Error.length > 1) {
                alert(result.Error);
                return false;
            }

            for (var fn in result) {
                //functionsList.append(new Option(result[fn].Name, result[fn].Name, false, false));
                functionsList.append($("<option />").attr("value", result[fn].Name).text(result[fn].Name));
            }

            $("#baseFunctions option[value='-1']").attr("selected", "selected");
        }
    });
});

// ------------------------------------------------------------------
// Get the function markup including the arguments for given function
// ------------------------------------------------------------------
function getFunctionTree(fnId, fnName, args, functionSyntax) {
    //var fnId = getActiveFunctionId();
    var markUp = "<div class='syntax'>" + functionSyntax + "</div><ul id=\"" + fnId + "\" class=\"" + functionType + "\" ><li>" + fnName + " ( ";

    for (var i = 0; i < args.length; i++) {
        markUp += "<ul class=\"" + argumentClass + "\"><li>";
        markUp += getHiddenField(null, argOrdinalClass, args[i].Ordinal);
        markUp += getHiddenField(null, argName, args[i].Name);
        var thisArgId = args[i].ArgId;
        markUp += getHiddenField(null, thisArgId + "_datatype", args[i].Type);
        markUp += getHiddenField(null, argIdClass, thisArgId);
        markUp += "<div id=\"" + thisArgId + "\" class=\"arg" + i + "\">" + GetOptionsMarkUp(fnId, thisArgId, '') + "</div></li></ul>";
        //args[i].Value

        //argsCollection.push(new eArgument(fnId, thisArgId, args[i].Name, staticType, args[i].Value, args[i].ViewElementType || ''));
        argsCollection.push(new eArgument(fnId, thisArgId, args[i].Name, args[i].Type, args[i].Value, args[i].ViewElementType || ''));
    }

    var collection = fnCollection;

    if (expressionBuilderMode == editMode) {
        collection = editFnCollection;
    }

    //if (expressionBuilderMode === insertMode) {
    for (var i = 0; i < collection.length; i++) {
        var fn = collection[i];

        if (fn.FunctionId == fnId) {
            fn.FunctionArgs = argsCollection;
            argsCollection = [];
        }
    }
    //}

    argsCollection = [];
    markUp += "</li><li>)</li></ul>";
    return markUp;
}

// -----------------------------------------------------
// Get the radio button options for each of the function
// -----------------------------------------------------
function GetOptionsMarkUp(functionId, argId, selectedOption) {
    var markUp = "<span>";
    if (selectedOption === staticType) {
        markUp += optionMarkUp(functionId, argId, staticType, true);
    }
    else {
        markUp += optionMarkUp(functionId, argId, staticType, false);
    }

    if (selectedOption === columnType) {
        markUp += optionMarkUp(functionId, argId, columnType, true);
    }
    else {
        markUp += optionMarkUp(functionId, argId, columnType, false);
    }

    if (selectedOption === undefined || selectedOption === functionType) {
        markUp += optionMarkUp(functionId, argId, functionType, true);
    }
    else {
        markUp += optionMarkUp(functionId, argId, functionType, false);
    }

    return markUp + "</span>";
}

// ----------------------------------
// function id, argument id and value
// ----------------------------------
function optionMarkUp(fnId, argId, value, isChecked) {
    var radioName = fnId + "_arg_" + argId + "_option";
    var markUp = "<input name=\"" + radioName + "\" type=\"radio\" ";

    if (isChecked === true) {
        markUp += " checked=\"checked\" ";
    }

    markUp += " value=\"" + value + "\" onclick=\"onOptionChanged('" + fnId + "','" + argId + "','" + value + "')\" />" + value;

    return markUp;
}

// -------------------------------------------------------------
// function id, argument id and type [static / column / function]
// -------------------------------------------------------------
function onOptionChanged(fnId, argId, type, argValue) {
    var fnType = jQuery.trim(type);

    var targetClass = "#" + fnId + " #" + argId;

    $(targetClass).html('');

    var classId = argId + "_datatype";
    var fnDataType = $("." + classId).val();

    var currentArg = getArgumentById(fnId, argId);
    if (currentArg == undefined) {
        alert("Exception Occured while editing the argument for this function.");
        return false;
    }
    currentArg.ArgumentType = fnType;

    switch (fnType) {
        case functionType:
            // get the list of allowed functions based on this argument datatype.
            var optionMarkUp = "";

            $.ajax({
                url: "/Expressions/GetFunctionsByType/",
                type: "POST",
                data: { 'returnType': fnDataType },
                async: false,
                success: function (data) {

                    if (data === undefined) {
                        alert("Exception Occured While getting the functions list");
                        return false;
                    }

                    //var result = JSON.parse(data);

                    var result = data;

                    if (result.Error !== undefined && result.Error.length > 1) {
                        alert(result.Error);
                        return false;
                    }

                    optionMarkUp = "<option value=\"-1\">Choose A Function </option>";

                    for (var prop in result) {
                        optionMarkUp += "<option value=\"" + result[prop].Name + "\"";
                        // mark the value function as the selected one.
                        if (result[prop].Name === argValue) {
                            optionMarkUp += " selected=\"selected\"";
                        }
                        optionMarkUp += ">" + result[prop].Name + "</option>";
                    }
                }
            });

            var functionMarkUp = "<select class=\"fns\" onchange=\"onFunctionChanged(this)\" name=\"" + (fnId + " #" + argId) + "\" >";
            functionMarkUp += optionMarkUp;
            functionMarkUp += "</select>";

            $(targetClass).append(functionMarkUp);
            break;
        case staticType:

            var staticInputMarkUp = '<input autocomplete="off" type="text" class="' + argId + 'staticIp"';

            if (argValue !== undefined && argValue != null) {
                staticInputMarkUp += ' value="' + argValue + '" ';
            }

            staticInputMarkUp += ' />';
            $(targetClass).append(staticInputMarkUp);
            break;
        case columnType:
            var colMarkUp = getColumnsFromSelectClauseFields(fnDataType, argValue);
            $(targetClass).append(colMarkUp);
            break;
        default:
            break;
    }

    var resetMarkUp = GetOptionsMarkUp(fnId, argId, type);
    $(targetClass).append(resetMarkUp);

    var currentFunction = getFunctionFromStack(fnId);

    // this will be false for all the function generated args, else for single expression parts, this is true
    if (expressionBuilderMode === insertMode && currentFunction !== undefined && currentFunction.FunctionArgs !== undefined) {
        for (var i = 0; i < currentFunction.FunctionArgs.length; i++) {
            if (currentFunction.FunctionArgs[i].ArgumentId != argId) {
                continue;
            }

            if (currentFunction.FunctionArgs[i].ArgumentType == functionType) {
                var deleteFunctionId = currentFunction.FunctionArgs[i].ArgumentValue;
                removeFuntionFromStack(deleteFunctionId);
                currentFunction.FunctionArgs[i].ArgumentValue = '';
                currentFunction.FunctionArgs[i].ArgumentType = type;
                break;
            }
            currentFunction.FunctionArgs[i].ArgumentType = type;
        }
    }
}

// ------------------------------------------------------------------------
// Gets the columns from the QueryBuilder based on arg's required data type
// ------------------------------------------------------------------------
function getColumnsFromSelectClauseFields(requiredDataType, selectedColumn) {

    if (_selectClauseFields === undefined || _selectClauseFields.length < 1) {
        return;
    }

    var markUp = "<select class=\"querycolumns\">";

    var matchCount = 0;

    var statusObject = _getColumnsFromFields(_selectClauseFields, requiredDataType, selectedColumn);
    matchCount = statusObject.count;
    markUp += statusObject.content;

    statusObject = _getColumnsFromFields(_selectedSelectClauseFields, requiredDataType, selectedColumn);
    matchCount += statusObject.count;
    markUp += statusObject.content;
    markUp += "</select>";

    if (matchCount < 1) {
        alert("No Suitable Columns Found");
    }

    return markUp;
}

// -------------------------------------------------------------------
// get the columns for the options from the given collection of fields
// -------------------------------------------------------------------
function _getColumnsFromFields(collection, requiredDataType, selectedColumn) {

    var matchCount = 0;
    var markUp = '';

    for (var i = 0; i < collection.length; i++) {

        if (collection[i].IsExpressionColumn === true || collection[i].IsExtensionField) {
            continue;
        }

        if (collection[i].Type.toLowerCase() === requiredDataType.toLowerCase()) {
            // add the option elements for each of the matching fields here
            matchCount++;
            var entityId = collection[i].EntityName;
            var val = entityId + '.' + collection[i].MappedFieldName;
            var name = entityId + '.' + collection[i].DisplayName;

            markUp += "<option value=\"" + val + "\" title=\"" + name + "\"";

            if (selectedColumn !== undefined && val.toLowerCase() === selectedColumn.toLowerCase()) {
                markUp += " selected=\"selected\" ";
            }

            markUp += " >" + name + "</option>";
        }
    }

    return { 'count': matchCount, 'content': markUp };
}

// ------------------------------------
// delete the expression column by name
// ------------------------------------
function DeleteExpressionColumn(expressionColumnAlias) {

    if (expressionColumnAlias === undefined || expressionColumnAlias.length < 1) {
        alert("The column definition is Invalid");
        return false;
    }

    if (_isInSelectedFilterClause(expressionColumnAlias)) {
        alert("This Expression Column is consumed in filter clause. Remove from filter clause to continue deletion");
        return false;
    }

    if (expressionStack === undefined && expressionStack.length < 1) {
        alert("No Expression found");
        return false;
    }

    var result = confirm("Confirm Deletion of " + expressionColumnAlias);

    if (result !== true) {
        return false;
    }

    var i = 0;

    if (_expressionColumns !== undefined && _expressionColumns.length > 0) {
        for (i = 0; i < _expressionColumns.length; i++) {
            if (_expressionColumns[i].Alias.toLowerCase() !== expressionColumnAlias.toLowerCase()) {
                continue;
            }
            _expressionColumns.splice(i, 1);
            break;
        }
    }

    if (_expressionFields !== undefined && _expressionFields.length > 0) {
        for (i = 0; i < _expressionFields.length; i++) {
            if (_expressionFields[i].DisplayName.toLowerCase() !== expressionColumnAlias.toLowerCase()) {
                continue;
            }
            _expressionFields.splice(i, 1);
            break;
        }
    }

    _removeFunctionFromUI(expressionColumnAlias)

    for (i = 0; i < expressionStack.length; i++) {
        if (expressionStack[i].Name.toLowerCase() !== expressionColumnAlias.toLowerCase()) {
            continue;
        }
        expressionStack.splice(i, 1);
        break;
    }

    _removeFromColumnListing(expressionColumnAlias);

    //alert("Expression: " + expressionColumnAlias + " removed successfully");

    $('#previewExpressionDetails').modal({ show: false });
    $('#previewExpressionDetails').modal('hide');
}

// ------------------------------------------------------------------------
// Removes an expression column from the list displayed above select clause
// ------------------------------------------------------------------------
function _removeFromColumnListing(aliasName) {
    $("#exprColsList .expressioncolumn").each(function () {
        if (jQuery.trim($(this).html()) === 'ExprCol.' + aliasName) {
            var tdparent = $(this).parent();
            var thisRow = $(tdparent).parent();
            $(thisRow).remove();
        }
    });
}

// ----------------------------------------------------------
// Removes the function / field based on the given alias name
// ----------------------------------------------------------
function _removeFunctionFromUI(expressionColumnAlias) {

    _removeField(_selectClauseFields, expressionColumnAlias);
    _removeElementFromUI("selectClauseFields", expressionColumnAlias);

    _removeField(_selectedSelectClauseFields, expressionColumnAlias);
    _removeElementFromUI("selectedselectClauseFields", expressionColumnAlias);

    _removeField(_filterClauseFields, expressionColumnAlias);
    _removeElementFromUI("conditionField", expressionColumnAlias);

    _removeField(_selectedFilterClauseFields, expressionColumnAlias);

    _removeField(_orderByClauseFields, expressionColumnAlias);
    _removeElementFromUI("orderByFields", expressionColumnAlias);

    _removeField(_selectedOrderByClauseFields, expressionColumnAlias);
    _removeElementFromUI("selectedorderByFields", expressionColumnAlias, true);

    _removeField(_groupByClauseFields, expressionColumnAlias);
    _removeElementFromUI("groupByFields", expressionColumnAlias);

    _removeField(_selectedGroupByClauseFields, expressionColumnAlias);
    _removeElementFromUI("selectedgroupByFields", expressionColumnAlias);
}

// ------------------------------
// Remove the element from the UI
// ------------------------------
function _removeElementFromUI(elementId, valueText, isOrderBySelected) {
    valueText = expressionColumnEntityName + '.' + valueText;
    $("#" + elementId + " option").each(function () {
        if ($(this).text() === valueText) {
            $(this).remove();
        }
        else if (isOrderBySelected) {
            if ($(this).text().indexOf(valueText) >= 0) {
                $(this).remove();
            }
        }
    });
}

// ----------------------------------------------
// Removes the function from the given collection
// ----------------------------------------------
function _removeField(collection, displayName) {
    for (var i = 0; i < collection.length; i++) {
        if (collection[i].DisplayName.toLowerCase() !== displayName.toLowerCase()) {
            continue;
        }
        collection.splice(i, 1);
        break;
    }
}

// ----------------------------------
// edit the expression column by name
// ----------------------------------
function EditExpressionColumn(expressionColumnAlias) {

    if (expressionColumnAlias === undefined || expressionColumnAlias.length < 1) {
        alert("The column definition is Invalid");
        return false;
    }

    if (_isInSelectedFilterClause(expressionColumnAlias)) {
        alert("This Expression Column is consumed in filter clause. Remove from filter clause to continue editing");
        return false;
    }

    if (expressionColumnAlias === undefined) {
        alert("Invalid Column");
        return false;
    }

    if (expressionStack === undefined && expressionStack.length < 1) {
        alert("No Expression found");
        return false;
    }

    expressionBuilderMode = editMode;

    $("#exprSandbox").html('');

    var editFunction = undefined;

    for (var i = 0; i < expressionStack.length; i++) {
        if (expressionStack[i].Name.toLowerCase() !== expressionColumnAlias.toLowerCase()) {
            continue;
        }

        $("#expressionName").val(expressionColumnAlias);
        $("#expressionName").attr("readonly", "readonly");

        editExpression = expressionStack[i];

        if (!IsProductAdmin() && editExpression.IsFreeFlowExpression) {
            alert("You donot have privileges to edit this expression. Please contact your Product Administrator");
            return false;
        }

        if (editExpression.IsFreeFlowExpression) {
            //enableFreeFlowExpression();
            $("input[name=expressionOption]").each(function () {
                if ($(this).val() === "freeflow") {
                    $(this).prop("checked", true);
                }
                else {
                    $(this).prop("checked", false);
                }
            });
        }
        else if (!editExpression.IsFreeFlowExpression) {
            $("input[name=expressionOption]").each(function () {
                if ($(this).val() === "build") {
                    $(this).prop("checked", true);
                }
                else {
                    $(this).prop("checked", false);
                }
            });
        }
        break;
    }

    if (editExpression !== undefined && !editExpression.IsFreeFlowExpression) {
        editFunction = expressionStack[i].FunctionList;
        editFnCollection = editFunction;
        for (var j = 0; j < editFunction.length; j++) {
            var fnName = editFunction[j].FunctionName;
            GenerateFunction(fnName, "exprSandbox", editFunction[j]);
            break;
        }
        var rootFunctionId = getRootFunctionId();

        _iterateAndDisplayArguments(rootFunctionId);
    }
    else {
        $(".expressionName").val(editExpression.Name);

        if (typeof (editExpression.DataType) === "number") {
            editExpression.DataType = getTypeNameFromKey(editExpression.DataType);
        }

        enableFreeFlowExpression(editExpression.ExpressionValue, editExpression.DataType);
    }
    _addToFields();
    ShowExpressionColumn();
}

// -------------------------------------------------------------------------------
// Checks whether this alias name is available in any selected filter clause field
// returns true if found , false otherwise
// -------------------------------------------------------------------------------
function _isInSelectedFilterClause(aliasName) {
    if (aliasName === undefined || aliasName.length < 1 || _selectedFilterClauseFields === undefined || _selectedFilterClauseFields.length < 1) {
        return;
    }

    for (var f = 0; f < _selectedFilterClauseFields.length; f++) {
        if (_selectedFilterClauseFields[f].DisplayName === aliasName && _selectedFilterClauseFields[f].IsExpressionColumn === true) {
            return true;
        }
    }
    return false;
}

// ---------------------------------------------------------
// To iterate and replace arguments with appropriate content
// ---------------------------------------------------------
function _iterateAndDisplayArguments(rootFunctionId) {

    if (rootFunctionId == null) {
        return;
    }

    //rootFunctionId = "ul#" + rootFunctionId;

    var currentFunction = getFunctionByFunctionId(rootFunctionId);

    for (var i = 0; i < currentFunction.FunctionArgs.length; i++) {
        var currentArgId = currentFunction.FunctionArgs[i].ArgumentId;

        var currentArgumentType = _getCurrentArgumentType(currentArgId);

        if (currentArgumentType !== null && currentArgumentType === functionType) {
            var parentElement = $("div#" + currentArgId).parent();
            var argFunction = _getArgumentsFunction(currentFunction.FunctionArgs[i].ArgumentValue);

            if (argFunction !== undefined) {
                onOptionChanged(rootFunctionId, currentArgId, currentArgumentType, argFunction.FunctionName);

                onFunctionChanged($("#" + currentArgId + " .fns"), argFunction);
                //GenerateFunction(argFunction.FunctionName, currentArgId, argFunction);

                _iterateAndDisplayArguments(argFunction.FunctionId);
            }
        }
    }
}

// -----------------------------------------------------
// Gets the function that is pointed to by this argument
// -----------------------------------------------------
function _getArgumentsFunction(argValue) {
    for (var i = 0; i < editFnCollection.length; i++) {
        if (editFnCollection[i].FunctionId === argValue) {
            return editFnCollection[i];
        }
    }
    return undefined;
}

// -------------------------------------------------------------------------------
// Gets the current argument type, like static / function / expression from the UI
// -------------------------------------------------------------------------------
function _getCurrentArgumentType(currentArgId) {
    var argType = null;
    $("div#" + currentArgId + " input[type='radio']").each(function () {
        if ($(this).is(":checked")) {
            argType = $(this).val();
        }
    });
    return argType;
}

// ------------------------------------------------------
// Get the function in the list for the given function id
// ------------------------------------------------------
function getFunctionByFunctionId(fnId) {

    if (editFnCollection == undefined || editFnCollection.length < 1) {
        return null;
    }

    for (var i = 0; i < editFnCollection.length; i++) {
        if (editFnCollection[i].FunctionId.toLowerCase() === fnId.toLowerCase()) {
            return editFnCollection[i];
        }
    }
}

// ----------------------------------
// Get the first function in the list
// ----------------------------------
function getRootFunctionId() {
    if (editFnCollection === undefined || editFnCollection.length < 1) {
        return null;
    }

    return editFnCollection[0].FunctionId;
}

function ResetArgument(argId) {
    var markUp = GetOptionsMarkUp(argId);
    $(".arg" + argId).html('');
    $(".arg" + argId).html(markUp);
}

// ------------------------------------------------------------
// Triggered whenever a function is changed in its dropdownlist
// ------------------------------------------------------------
function onFunctionChanged(sourceElement, argFunction) {

    //var elementName = $(sourceElement).attr("name");

    //var selectedFunctionName = $("select[name=" + elementName + "] :selected").val();

    var selectedFunctionName = sourceElement[sourceElement.selectedIndex].value;

    if (selectedFunctionName === "-1") {
        alert("Choose a valid function to continue");
        return false;
    }

    var targetElement = $(sourceElement).attr("name");

    if (argFunction !== undefined) {
        GenerateFunction(selectedFunctionName, targetElement, argFunction);
    }
    else {
        GenerateFunction(selectedFunctionName, targetElement, undefined);
    }

    if (targetElement !== "exprSandbox") {
        var idCollection = targetElement.split(' ');

        var fnId = idCollection[0];

        var argId = $("#" + targetElement).attr("id");

        if (expressionBuilderMode === editMode) {
            var changingArgument = getArgumentById(fnId, argId);
            changingArgument.ArgumentType = functionType;
            changingArgument.ArgumentValue = getActiveFunctionId();
        }

        var resetMarkUp = GetOptionsMarkUp(fnId, argId, functionType);
        $("#" + targetElement).append(resetMarkUp);
    }
}

// ---------------------------------------------
// Generate the function markup and related data
// ---------------------------------------------
function GenerateFunction(functionName, targetElementId, currentFunction) {
    var args = [];
    $.ajax({
        url: "/Expressions/GetFunctionDefinition/",
        data: {
            'functionName': functionName
        },
        type: "POST",
        async: false,
        success: function (result) {
            if (result === null || result === undefined) {
                alert("The function definition is not valid for the chosen function");
                return false;
            }

            if (result != undefined && result.Error !== undefined && result.Error.length < 1) {
                alert("Unable to fetch the chosen function.");
                return false;
            }

            if (result.length < 1) {
                alert("Exception Occured while fetching the function");
                return false;
            }

            //result = JSON.parse(result);

            if (currentFunction !== undefined && currentFunction.FunctionId !== undefined) {
                result.InstanceId = currentFunction.FunctionId;
            }

            if (result.InstanceId === undefined) {
                result.InstanceId = guidGenerator();
            }

            var fnId = result.InstanceId;

            /* initialize the function ui markup and its related objects */
            var functionDefinition = result;

            var fnInputsDefinition = functionDefinition.InputDefinition;

            for (var definition in fnInputsDefinition) {

                var dataType = getTypeNameFromKey(fnInputsDefinition[definition].DataType);

                var argValue = fnInputsDefinition[definition].Value;

                var argId = guidGenerator();

                if (fnInputsDefinition[definition].InputId != undefined) {
                    argId = fnInputsDefinition[definition].InputId;
                }

                var argViewType = MapFormatTypeEnum(fnInputsDefinition[definition].FormatType);

                if (currentFunction !== undefined && currentFunction.FunctionArgs !== undefined && currentFunction.FunctionArgs.length > 0) {
                    var editArg = getArgumentForEdit(currentFunction, fnInputsDefinition[definition].Name);
                    argValue = editArg.ArgumentValue;
                    argId = editArg.ArgumentId;
                    argViewType = editArg.ViewElementType;
                }

                args.push(new Arg(argId, fnInputsDefinition[definition].Name, fnInputsDefinition[definition].Ordinal, dataType, argValue, argViewType));
            }

            functions.push(functionDefinition);
            functionIds.push(fnId);

            if (expressionBuilderMode === insertMode) {
                fnCollection.push(new eFunction(fnId, functionDefinition.Name, null, null, functionDefinition.OutputDataType));
            } else if (expressionBuilderMode === editMode) {
                // if the current function and this function are different, then add to the collection, else continue without adding
                if (currentFunction === undefined) {
                    editFnCollection.push(new eFunction(fnId, functionDefinition.Name, null, null, functionDefinition.OutputDataType));
                }
                else if (currentFunction !== undefined && currentFunction.FunctionId.toLowerCase() !== fnId.toLowerCase()) {
                    editFnCollection.push(new eFunction(fnId, functionDefinition.Name, null, null, functionDefinition.OutputDataType));
                }
            }

            $("#" + targetElementId).html(getFunctionTree(fnId, functionDefinition.Name, args, functionDefinition.FunctionSyntax));

            $("#" + targetElementId).treeview({
                collapsed: false,
                animated: "medium"
            });

            for (var i = 0; i < args.length; i++) {
                if (args[i].ViewElementType !== undefined && args[i].ViewElementType !== staticType && args[i].ViewElementType !== columnType && args[i].ViewElementType !== functionType) {
                    args[i].ViewElementType = staticType;
                }
                onOptionChanged(fnId, args[i].ArgId, args[i].ViewElementType, args[i].Value);
            }

            /* update the existing function's arg with the pointer to this function, as this argument is a function instead */
            if (targetElementId !== 'exprSandbox' && expressionBuilderMode === insertMode) {
                var tmpIds = targetElementId.split(' #');

                var targetFnId = tmpIds[0];
                var targetArgId = tmpIds[1];

                var selectedFn = getFunctionFromStack(targetFnId);

                if (selectedFn.FunctionArgs != null) {
                    var fnArgs = selectedFn.FunctionArgs;
                    for (var i = 0; i < fnArgs.length; i++) {
                        if (fnArgs[i].ArgumentId !== targetArgId) {
                            continue;
                        }
                        fnArgs[i].ArgumentType = functionType;
                        fnArgs[i].ArgumentValue = fnId;
                    }
                }
            }
        }
    });
}

// -----------------------------------------------------
// Gets the argument in the given function for edit mode
// -----------------------------------------------------
function getArgumentForEdit(editFunction, argName) {

    if (editFunction === undefined || argName === undefined || editFunction.FunctionArgs.length < 1) {
        return;
    }

    for (var i = 0; i < editFunction.FunctionArgs.length; i++) {
        if (editFunction.FunctionArgs[i].ArgumentName.toLowerCase() === argName.toLowerCase()) {
            return editFunction.FunctionArgs[i];
        }
    }
}

// ------------------------------------------------------
// Get argument Based on the Function and the Argument id
// ------------------------------------------------------
function getArgumentById(functionId, argumentId) {
    if (functionId === undefined || argumentId === undefined) {
        return;
    }

    var collection = fnCollection;

    if (expressionBuilderMode === editMode) {
        collection = editFnCollection;
    }

    for (var i = 0; i < collection.length; i++) {
        if (collection[i].FunctionId !== functionId) {
            continue;
        }

        var fnArgs = collection[i].FunctionArgs;

        for (var j = 0; j < fnArgs.length; j++) {
            if (fnArgs[j].ArgumentId === argumentId) {
                return fnArgs[j];
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------
// Adds the EntityFieldInfo object to the expression fields collection for save / view operation
// ---------------------------------------------------------------------------------------------
function _addColumnToExpressionFields(value) {

    if (value == undefined) {
        return;
    }

    var i = 0;

    for (i = 0; i < _selectClauseFields.length; i++) {
        var fieldValue = _selectClauseFields[i].EntityName + "." + _selectClauseFields[i].MappedFieldName;

        if (fieldValue.toLowerCase() === value.toLowerCase()) {
            _expressionFields.push(_selectClauseFields[i]);
            break;
        }
    }

    // in case the column is from the selected select clause field, remove the aggr fn property and use that for the expression fields object
    for (i = 0; i < _selectedSelectClauseFields.length; i++) {
        var fieldValue = _selectedSelectClauseFields[i].EntityName + "." + _selectedSelectClauseFields[i].MappedFieldName;

        if (fieldValue.toLowerCase() === value.toLowerCase()) {
            var field = jQuery.extend(true, {}, _selectedSelectClauseFields[i]);
            delete field.AggregateFunction;
            //_expressionFields.push(_selectedSelectClauseFields[i]);
            _expressionFields.push(field);
            break;
        }
    }
}

/**
Post the values to Server for translation for each expression
*/
function SendData() {

    var aliasName = jQuery.trim($("#expressionName").val());

    if (aliasName.length < 1) {
        alert("Provide a Name for this expression");
        return false;
    }

    if (aliasName.length > 120) {
        alert("Too many characters in the expression column name");
        return false;
    }

    if (!IsFreeFlowExpression() && $("#baseFunctions :selected").val() === "-1") {
        alert("No Function is Selected");
        return false;
    }

    var l = 0;

    // check whether the alias name already exists
    if (expressionBuilderMode === insertMode && _expressionColumns.length > 0) {
        for (l = 0; l < _expressionColumns.length; l++) {
            if (_expressionColumns[l].Alias.toLowerCase() == aliasName.toLowerCase()) {
                alert("The Alias Name already exists");
                return false;
            }
        }
    }

    _expressionFields = [];

    if (!IsFreeFlowExpression()) {
        var fillStatus = FillArgumentValues();

        if (fillStatus === -1) {
            //alert("Invalid function argument data.");
            return false;
        }
    }

    // the expr builder is in edit mode and then it has functions and NOT free flow expressions
    if ((expressionBuilderMode === editMode && !IsFreeFlowExpression()) && (editFnCollection === undefined || editFnCollection.length < 1)) {
        alert("The Expression is not formed correctly");
        return false;
    }

    var expression = null;

    if (expressionBuilderMode === insertMode) {
        var exprValue = undefined;
        if (IsFreeFlowExpression()) {
            exprValue = jQuery.trim($("#exprSandbox .freeflowexpr").val());

            if (exprValue.length < 1) {
                alert("Invalid or Empty FreeFlow Expression");
                return false;
            }

            var retType = $("#exprSandbox .returnType :selected").val();

            //expression = new Expression(aliasName, expressionStack.length, fnCollection, true, exprValue, getTypeNameFromKey(23), _expressionFields);
            expression = new Expression(aliasName, expressionStack.length, fnCollection, true, exprValue, retType, _expressionFields);
        }
        else {
            expression = new Expression(aliasName, expressionStack.length, fnCollection, false, exprValue, undefined, _expressionFields);
        }
        expressionStack.push(expression);
    }
    else if (expressionBuilderMode === editMode) {
        var editExpression = _getExpressionByAliasName(aliasName);
        if (editExpression === undefined) {
            alert("Exception Occured while editing the function.");
            return false;
        }
        //expression = new Expression(aliasName, expressionStack.length, editFnCollection);
        editExpression.Name = aliasName;
        var exprValue = undefined;

        if (IsFreeFlowExpression()) {
            exprValue = $("#exprSandbox .freeflowexpr").val();
            editExpression.DataType = $("#exprSandbox .returnType :selected").val();
        }
        else {
            editExpression.FunctionList = editFnCollection;
        }
        editExpression.ExpressionValue = exprValue;
        editExpression.ExpressionFields = _expressionFields;
    }

    _expressionFields = [];

    $.ajax({
        url: "/Expressions/ManageExpressions/",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(expressionStack),
        success: function (data) {

            if (data === undefined) {
                alert("Exception occured while saving the data");
                return false;
            }

            //var result = JSON.parse(data);

            var result = data;

            if (result.Error !== undefined && result.Error.length > 0) {
                alert(result.Error);
                return false;
            }

            fnCollection = [];
            editFnCollection = [];
            argsCollection = [];
            functions = [];
            functionIds = [];
            _expressionColumns = [];

            for (var i = 0; i < result.length; i++) {

                var visibilityStatus = _checkExprFieldConstituentVisibility(result[i].exprFields);

                _expressionColumns.push(new ExpressionColumnInfo(result[i].TranslatedSQL, result[i].eName, result[i].eOrdinal, getTypeNameFromKey(result[i].exprType), result[i].exprFields, visibilityStatus));
                if (!result[i].isFreeFlowExpression) {
                    var updatableExpr = _getExpressionByAliasName(result[i].eName);
                    updatableExpr.ExpressionValue = result[i].TranslatedSQL;
                }
            }

            _addToFields();
            _updateExistingFields();
            addToExpressionsClause();

            //alert("Expression is saved successfully.");
            $('#previewExpressionDetails').modal({ show: false });
            $('#previewExpressionDetails').modal('hide');
        }
    });
}

// ----------------------------------------------------------------------------------------
// among the given set of fields, if a field has visibility to be false, this returns false
// ----------------------------------------------------------------------------------------
function _checkExprFieldConstituentVisibility(expressionFields) {
    if (expressionFields == null || expressionFields == undefined) {
        return true;
    }
    for (var cf = 0; cf < expressionFields.length; cf++) {
        if (!expressionFields[cf].IsVisible) {
            return false;
        }
    }
    return true;
}

// ----------------------------------------------
// returns true if product admin, false otherwise
// ----------------------------------------------
function IsProductAdmin() {
    if ($("input[name=IsProductAdmin]").length > 0) {
        if ($("input[name=IsProductAdmin]").val().toLowerCase() == 'true') {
            return true;
        }
    }
    return false;
}

// ------------------------------------------------------------------
// Returns true if the experssion is a free flow one, false otherwise
// ------------------------------------------------------------------
function IsFreeFlowExpression() {
    if ($("input[name=expressionOption]").length > 0) {
        if ($("input[name=expressionOption]:checked").val() !== undefined && $("input[name=expressionOption]:checked").val().toLowerCase() === 'freeflow') {
            return true;
        }
    }
    return false;
}

// To show a text area to represent the free flow expression input
function enableFreeFlowExpression(expression, returnType) {
    $("#exprSandbox").html('');
    if (expression !== undefined) {
        $("#exprSandbox").append("<textarea class='freeflowexpr' style='height:200px; width:100%'>" + expression + "</textarea>");
    }
    else {
        $("#exprSandbox").append("<textarea class='freeflowexpr' style='height:200px; width:100%'></textarea>");
    }

    $("#exprSandbox").append("<span>Return DataType <span style='color:red'>*</span></span>");
    $("#exprSandbox").append(_showReturnTypeOptions(returnType));
    $("#exprSandbox").append("<br /><br /><span style='color:red'>* Donot Use Aggregate functions as part of the free flow expressions</span>");
    $("#baseFunctions").parent().parent().fadeOut("fast");
}

// -------------------------------------------------------------
// Get the Return Types markup based on the selected return type
// -------------------------------------------------------------
function _showReturnTypeOptions(selectedOption) {
    var markUp = "<select class='returnType'>";

    for (var i = 0; i < returnDataTypes.length; i++) {
        markUp += "<option value='" + returnDataTypes[i].toLowerCase() + "'";
        if (selectedOption !== undefined && selectedOption !== null && returnDataTypes[i].toLowerCase() === selectedOption.toLowerCase()) {
            markUp += " selected='selected' ";
        }
        markUp += ">" + returnDataTypes[i] + "</option>";
    }
    markUp += "</select>";

    return markUp;
}

// To hide the text area to represent the free flow expression input
function disableFreeFlowExpression() {
    $("#exprSandbox").html('');
    $("#baseFunctions").parent().parent().fadeIn("fast");
}

// ----------------------------------------------------------------------
// Get the expression from the expressionStack using the given alias Name
// ----------------------------------------------------------------------
function _getExpressionByAliasName(aliasName) {

    if (aliasName === undefined || aliasName.length < 1) {
        alert("Invalid Expression Name");
        return false;
    }

    for (var e = 0; e < expressionStack.length; e++) {
        if (expressionStack[e].Name.toLowerCase() === aliasName.toLowerCase()) {
            return expressionStack[e];
        }
    }
    return undefined;
}

// -------------------------------------------------------------
// populate the expressions clause in the UI above select clause
// -------------------------------------------------------------
function addToExpressionsClause() {
    var exprListMarkUp = getExpressionListMarkUp();
    $("#exprColsList tbody").html(exprListMarkUp);
    //$("#expressionColList .exprColsList tbody").html(exprListMarkUp);
    $("#expressionColList").fadeIn("slow");

    _getNonSelectedFields();
}

// -------------------------------------------------------
// Get the markup for the expression columns clause for UI
// -------------------------------------------------------
function getExpressionListMarkUp() {
    if (_expressionColumns === undefined || _expressionColumns.length < 1) {
        return undefined;
    }
    var optionsElementMarkUp = "<tr><td colspan='2'><b>Name</b></td><td><b>Expression</b></td><td><b>Edit</b></td><td><b>Delete</b></td></tr>";

    if (_expressionColumns.length < 1) {
        optionsElementMarkUp = "<tr><td colspan='5'>No Expression Columns Found </td></tr>";
        return optionsElementMarkUp;
    }

    for (var e = 0; e < _expressionColumns.length; e++) {
        var elementMarkUp = "<tr><td colspan='2' class='row4' style='padding:0px 0px 0px 10px'>";
        elementMarkUp += "<span style='color:#836823' class='expressioncolumn' name='" + _expressionColumns[e].Alias + "'>";
        elementMarkUp += expressionColumnEntityName + "." + _expressionColumns[e].Alias + "</span> </td>";
        elementMarkUp += "<td>" + _expressionColumns[e].Expression + "</td>";
        elementMarkUp += "<td  class='row5' style='padding:0px 0px 0px 10px'><span><img src='../../App_Themes/CelloSkin/edit_icon.gif' alt='Edit' onClick=\"EditExpressionColumn('" + _expressionColumns[e].Alias + "')\" /></span></td>";
        elementMarkUp += "<td  class='row5' style='padding:0px 0px 0px 10px'><span><img src='../../App_Themes/CelloSkin/remove_but.gif' alt='Delete' onClick=\"DeleteExpressionColumn('" + _expressionColumns[e].Alias + "')\" /></span></td>";
        elementMarkUp += "</tr>";

        optionsElementMarkUp += elementMarkUp;
    }
    return optionsElementMarkUp;
}

// adds the unconsumed expression column to the select clause
function _getNonSelectedFields() {

    var unusedFields = [];

    for (var e = 0; e < _expressionColumns.length; e++) {
        if (!checkIfFieldExists(_expressionColumns[e].Alias, _selectedSelectClauseFields) && !checkIfFieldExists(_expressionColumns[e].Alias, _selectClauseFields)) {
            var field = new EntityFieldInfo(_expressionColumns[e].Alias, _expressionColumns[e].Expression, _expressionColumns[e].DataType, true, undefined, expressionColumnEntityName, true);
            _selectClauseFields.push(field);
            _orderByClauseFields.push(field);
            _filterClauseFields.push(field);
        }
    }

    _populateControls("#selectClauseFields", _selectClauseFields);
    _populateControls("#orderByFields", _orderByClauseFields);
    _populateControls("#conditionField", _filterClauseFields, 'Filter');
}

// --------------------------------------------------
// Update the existing fields with the translated sql
// --------------------------------------------------
function _updateExistingFields() {

    if (_expressionColumns === undefined || _expressionColumns.length < 1) {
        return;
    }

    _updateField(_selectClauseFields, _expressionColumns);
    _updateField(_selectedSelectClauseFields, _expressionColumns);
    _updateField(_filterClauseFields, _expressionColumns);
    _updateField(_selectedFilterClauseFields, _expressionColumns);
    _updateField(_orderByClauseFields, _expressionColumns);
    _updateField(_selectedOrderByClauseFields, _expressionColumns);
    _updateField(_groupByClauseFields, _expressionColumns);
    _updateField(_selectedGroupByClauseFields, _expressionColumns);
}

// ----------------------------------------------
// Updates the function from the given collection
// ----------------------------------------------
function _updateField(collection, expressionCols) {
    for (var a = 0; a < expressionCols.length; a++) {
        for (var i = 0; i < collection.length; i++) {
            if (collection[i].DisplayName.toLowerCase() === expressionCols[a].Alias.toLowerCase() && collection[i].IsExpressionColumn === true) {
                collection[i].MappedFieldName = expressionCols[a].Expression;
                collection[i].IsVisible = expressionCols[a].IsVisible;
                collection[i].Type = expressionCols[a].DataType;
                break;
            }
            continue;
        }
    }
}

// --------------------------------------------------------------------
// Fill the values for each argument & make ready for sending to server
// --------------------------------------------------------------------
function FillArgumentValues() {

    var collection = fnCollection;

    if (expressionBuilderMode === editMode) {
        collection = editFnCollection;
    }

    for (var i = 0; i < collection.length; i++) {

        if (collection[i] == null || collection[i].FunctionArgs == null || collection[i].FunctionArgs == undefined) {
            continue;
        }

        if ($("#" + collection[i].FunctionId).length < 1) {
            collection.splice(i, 1);
            i--;
            continue;
        }

        var fnArgs = collection[i].FunctionArgs;

        // will be accessed for non-operator and non-parenthesis functions
        for (var j = 0; j < fnArgs.length; j++) {
            var elementId;

            switch (fnArgs[j].ArgumentType) {
                case functionType:
                    fnArgs[j].ViewElementType = functionType;
                    continue;
                case columnType:
                    var tempArgId = "#" + fnArgs[j].ArgumentId + " .querycolumns :selected";
                    var argumentValue = $(tempArgId).val();
                    _addColumnToExpressionFields(argumentValue);
                    fnArgs[j].ArgumentValue = argumentValue;
                    fnArgs[j].ViewElementType = columnType;
                    break;
                case staticType:
                    var tempArgId = "#" + fnArgs[j].ArgumentId + " ." + fnArgs[j].ArgumentId + "staticIp";

                    var argStaticValue = jQuery.trim($(tempArgId).val());

                    if (argStaticValue === undefined || argStaticValue.length < 1) {
                        alert("Provide a valid value for the argument");
                        return -1;
                    }

                    fnArgs[j].ArgumentValue = argStaticValue;
                    fnArgs[j].ViewElementType = staticType;
                    break;
                default:
                    fnArgs[j].ArgumentValue = null;
                    fnArgs[j].ViewElementType = staticType;
                    break;
            }
        }
    }
}

/**
Add the newly formed expression column to the UI elements like select, group by, order by and filter clauses
*/
function _addToFields() {

    var _exprFields = [];

    for (var i = 0; i < _expressionColumns.length; i++) {

        var exprField = {
            'FieldIdentifier': _expressionColumns[i].FieldIdentifier,
            'DisplayName': _expressionColumns[i].Alias,
            'MappedFieldName': _expressionColumns[i].Expression,
            'EntityName': expressionColumnEntityName,
            'Type': _expressionColumns[i].DataType,
            'IsExpressionColumn': true,
            'IsVisible': _expressionColumns[i].IsVisible,
            'HasPrivilege': true
        };

        if (checkIfFieldExists(exprField.DisplayName, _selectedSelectClauseFields) !== true) {
            if (checkIfFieldExists(exprField.DisplayName, _selectClauseFields) !== true) {
                _selectClauseFields.push(exprField);
            }
        }

        if (checkIfFieldExists(exprField.DisplayName, _orderByClauseFields) !== true) {
            _orderByClauseFields.push(exprField);
        }

        if (checkIfFieldExists(exprField.DisplayName, _filterClauseFields) !== true) {
            _filterClauseFields.push(exprField);
        }

        if (checkIfFieldExists(exprField.DisplayName, _exprFields) !== true) {
            _exprFields.push(exprField);
        }
    }

    _populateControls("#selectClauseFields", _selectClauseFields, '');
    _populateControls("#orderByFields", _selectClauseFields, '');
    _populateControls("#conditionField", _filterClauseFields, 'Filter');
}

// -------------------------------------------------------------------------------------
// checks whether the field represented by the alias name exists in the given collection
// -------------------------------------------------------------------------------------
function checkIfFieldExists(aliasName, fields) {
    for (var i = 0; i < fields.length; i++) {
        if (fields[i].DisplayName === aliasName) {
            return true;
        }
    }
    return false;
}

// -------------------------------------------------------
// checks for a duplicate name for given expression column
// -------------------------------------------------------
function CheckDuplicateAlias(exprCol) {
    for (var i = 0; i < _expressionColumns.length; i++) {
        if (_expressionColumns[i].Alias == exprCol.Alias && _expressionColumns[i].ColumnId == exprCol.ColumnId) {
            continue;
        }
        else if (_expressionColumns[i].Alias == exprCol.Alias && _expressionColumns[i].ColumnId != exprCol.ColumnId) {
            return i;
        }
    }
    return -1;
}