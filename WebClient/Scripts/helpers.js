﻿$(function () {
    if ($('#loading').length > 0) {
        $(document).ajaxStart(function () {
            $('#loading').show();
            $('#loading').css('visibility', 'visible');
        }).ajaxStop(function () {
            $('#loading').hide();
            $('#loading').css('visibility', 'hidden');
        });
    }
});

// missing array prototypes for IE
if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fun) {
        var len = this.length;
        if (typeof fun != "function")
            throw new TypeError();

        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this)
                fun.call(thisp, this[i], i, this);
        }
    };
}

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt) {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
          this[from] === elt)
                return from;
        }
        return -1;
    };
}

// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};


function ShowErrorMsg(errorMsg) {
    var html = '<div class="alert alert-danger"><span>The workflow definiton contains the following errors:</span><ul>';
    var msg = errorMsg.split('.');

    for (var i = 0; i < msg.length && msg[i]; i++) {
        html += '<li>' + msg[i] + '.</li>';
    }

    html += '</ul></div>';

    $('#statusMessage').html(html);
    $('#statusMessage').fadeIn();
}

function ShowSuccessMsg(successMsg) {
    var html = '<div class="alert alert-success">' + successMsg + '</div>';
    $('#statusMessage').html(html);
    $('#statusMessage').fadeIn();
}


// form data to JSON object
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$.fn.serializeJSON = function () {
    var json = {};
    jQuery.map(jQuery(this).serializeArray(), function (n, i) {
        var _ = n.name.indexOf('[');
        if (_ > -1) {
            var o = json;
            _name = n.name.replace(/\]/gi, '').split('[');
            for (var i = 0, len = _name.length; i < len; i++) {
                if (i == len - 1) {
                    if (o[_name[i]]) {
                        if (typeof o[_name[i]] == 'string') {
                            o[_name[i]] = [o[_name[i]]];
                        }
                        o[_name[i]].push(n.value);
                    }
                    else o[_name[i]] = n.value || '';
                }
                else o = o[_name[i]] = o[_name[i]] || {};
            }
        }
        else {
            if (json[n.name] !== undefined) {
                if (!json[n.name].push) {
                    json[n.name] = [json[n.name]];
                }
                json[n.name].push(n.value || '');
            }
            else json[n.name] = n.value || '';
        }
    });
    return json;
};

function ShowStatus(status, msg) {
    var html = '<div class="alert alert-' + (status || 'success') + '">' + msg + '</div>';
    $('#statusMessage').html(html).fadeIn();
}

function ShowError(msg) {
    ShowStatus('danger', msg);
}

function ShowSuccess(msg) {
    ShowStatus('success', msg);
}

var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
      , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
      , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
      , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (obj, table, name) {
        if (!table.nodeType) table = document.getElementById(table);
        var out = table.innerHTML;
        if ($.fn.dataTable.fnIsDataTable(table)) {
            out = getFullDataTableData(table);
        }
        var ctx = { worksheet: name || 'Worksheet', table: out };
        //window.location.href = uri + base64(format(template, ctx))
        obj.href = uri + base64(format(template, ctx));
        obj.download = name + '.xls';
        return true;
    }
})();

function getFullDataTableData(tbl) {
    var data = '<thead>';
    data += tbl.tHead.innerHTML;
    data += '</thead>';
    data += '<tbody>';
    $.each($(tbl).dataTable().fnGetData(), function (i, o) {
        data += '<tr>';
        jQuery.each(o, function (j, d) {
            data += '<td>' + d + '</td>';
        });
        data += '</tr>';
    });
    data += '</tbody>';
    return data;
}