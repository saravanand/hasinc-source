﻿$(function () {

    $("#clientTenants").select2(
        {
            width: "100%"
        });

    $("#clientTenants").on("change", function () {
        var chosenTenantId = $(this).find("option:selected").val();

        if (chosenTenantId == "" || chosenTenantId.length < 1) {
            alert("Please choose a valid tenant");
            return;
        }

        ListClients(chosenTenantId);
        $(".clientDetail").html('').hide();
    });
    Init();


});

function addClient() {
    $.ajax({
        method: "get",
        url: addUri,
        data: {
            tenantId: $("#clientTenants").find("option:selected").val()
        }
    }).done(function (result) {
        $(".clientDetail").html(result).show();
        $("#loading").hide();
    }).fail(function (result) {
        $(".alert-danger").html("Error occured while adding the client details, try again later");
        $(".alert-danger").show();
    });
}



function doEdit(id, tenantId, appType) {

    $.ajax({
        method: "post",
        url: editUri,
        data: {
            'Id': id,
            'TenantId': tenantId,
            'ApplicationType': appType
        }
    }).done(function (result) {
        $(".clientDetail").html(result).show();
        $("#loading").hide();
    }).fail(function (result) {
        $(".alert-danger").html("Error occured while editing the client details, try again later");
        $(".alert-danger").show();
        setTimeout($(".alert-danger"), 5000);
    });
}

function editclient(element) {
    if (element == undefined) {
        return;
    }

    var id = $(element).data("id");
    var tenantId = $(element).data("tenantid");
    doEdit(id, tenantId);
}

function deleteClient(id, tenantId) {

    $.ajax({
        method: "post",
        url: removeUri,
        data: {
            'Id': id,
            'TenantId': tenantId,
            'ApplicationType': appType
        }
    }).done(function (result) {
        $(".clientDetail").html(result).show();
        $("#loading").hide();
    }).fail(function (result) {
        $(".alert-danger").html("Error occured while removing the client details, try again later");
        $(".alert-danger").show();
    });
}

function Init() {
    var chosenTenantId = $("#clientTenants").find("option:selected").val();
    if (chosenTenantId.length < 1 || chosenTenantId == undefined) {
        chosenTenantId = defaultTenantId;
    }
    ListClients(chosenTenantId);
}

function ListClients(tenantId) {
    $.ajax({
        method: "post",
        url: listUri,
        data: {
            'tenantId': tenantId
        }
    }).done(function (result) {
        $(".clientListing").html(result);
        $("#loading").hide();
    }).fail(function (result) {
        $(".alert-danger").html("Error occured while reading the client details, try again later");
        $(".alert-danger").show();
    }).complete(function () {
        $('.clientListing .grid-title').html('<h4>Client Details for :' + $("#clientTenants").find("option:selected").text() + '</h4>');
        $("select").select2({ width: "100%" });
    });
}

function cancelSave() {
    alert("Data Not Saved");
    $(".clientDetail").html('').show();

}