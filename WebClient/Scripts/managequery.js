﻿// Moved from inner scope to here
var _selectClauseFields = [];
var _selectedFilterClauseFields = [];
var _globalVariables = [];
var _expressionColumns = [];
var _expressionFields = [];
var _orderByClauseFields = [];
var _filterClauseFields = [];
var _selectedSelectClauseFields = [];
var _groupByClauseFields = [];
var _selectedGroupByClauseFields = [];
var _selectedOrderByClauseFields = [];
var dynamicVariables = [];

// -------------------------------------------------------------------
// gets the mapping form int to string for the filterclausefield types
// -------------------------------------------------------------------
function getValueTypeMapping(value) {
    switch (value) {
        case 1:
            return "Column";
        case 2:
            return "GlobalVariable";
        case 3:
            return "DynamicVariable";
        case 0:
        default:
            return "Static";
    }
}

/* Start of ExpressionBuilder Code */
// -------------------------------------
// Show the Modal for Adding Expressions
// -------------------------------------
function ShowExpressionColumn(mode) {

    if ($("#moduleDDL :selected").val() === "-1") {
        alert("No Module is Selected. Choose a valid module");
        return false;
    }

    if ($("#entityDDL :selected").val() === "-1") {
        alert("No Primary Entity is Selected. Choose a Primary Entity");
        return false;
    }
    $('#previewExpressionDetails .modal-body').html($(".exprcolumns"));
    $('#previewExpressionDetails').modal('show');

    if (mode !== undefined && mode === insertMode) {
        expressionBuilderMode = insertMode;
        editExpression = null;

        $(".exprcolumns input[name=expressionOption]").each(function () {
            if ($(this).val() === "build") {
                $(this).prop("checked", true);
            }
            else {
                $(this).prop("checked", false);
            }
        });
    }

    if (expressionBuilderMode === insertMode) {
        $("#expressionName").prop("readonly", false);
        $(".ManageExprCols").html("Add")
        $("#exprSandbox").html('');
        if ($(".exprcolumns #expressionName").length > 0) {
            $("#expressionName").val('');
        }
        $(".exprcolumns .fns").val("-1").attr('selected', true);
    }

    $("#baseFunctions").parent().parent().show();

    if ((expressionBuilderMode === editMode || expressionBuilderMode === insertMode) && IsFreeFlowExpression()) {
        $("#baseFunctions").parent().parent().hide();
    }

    if (expressionBuilderMode === editMode && !IsFreeFlowExpression()) {
        var fnName = editFnCollection[0].FunctionName;
        $(".exprcolumns .fns").val(fnName).attr('selected', true);
    }

    // change the text of the button to update from add
    if (expressionBuilderMode === editMode && mode !== insertMode) {
        $(".ManageExprCols").html('Update');
    }
    $(".exprcolumns .fns").css("color", "black");
}

// ----------------------------------------------------
// Remove the Edit Expression Button from the UI
// ----------------------------------------------------
function onBaseFieldClick() {

    $(".expressionControls .ExpressionControls").each(function () {
        $(this).remove();
    });
}
/* End of ExpressionBuilder Code */

function _populateControls(selectListId, fields, clauseType) {
    var html = '';

    if (clauseType == 'Select') {
        $('#selectedFieldsTable tbody').empty();
    } else if (clauseType == 'Grp') {
        //$('#groupClause div.chkFieldDiv').remove();
        $('#groupClause #groupClauseTable tbody tr').remove();
    } else if (clauseType == 'Ord') {
        $("#selectedFieldsTable select.direction").attr('disabled', 'disabled');
    }

    for (i = 0; i < fields.length; i++) {
        fields[i].sortid = i;
        var entityId = fields[i].EntityName;
        var fieldId = entityId + '.' + fields[i].MappedFieldName;
        var name = entityId + '.' + fields[i].MappedFieldName;

        if (fields[i].IsExtensionField) {
            name = entityId + '.' + fields[i].MappedFieldName.split('~')[2];
        }

        if (clauseType == 'Select') {
            if (fields[i].AggregateFunction != undefined && fields[i].AggregateFunction != 'NONE') {
                name = fields[i].AggregateFunction + '(' + name + ')';
            }
        } else if (clauseType == 'OrderBy') {
            // don't change the name
            //name = entityId + '.' + fields[i].DisplayName + ' [' + fields[i].SortDirection + ']';
        } else if (clauseType == 'GroupBy') {
            // don't change the name
        } else if (clauseType == 'Filter') {
            if (i == 0) {
                html += '<option value="-1">--Select a field--</option>';
            }
        }

        html += '<option value="' + fieldId + '" title="' + name + '">' + name + '</option>';

        if (clauseType == 'Select') {
            var isSortAsc = fields[i].SortDirection == "ASC";
            var isSortDesc = fields[i].SortDirection == "DESC";

            var orderHtml = '<select name="' + fieldId + '" class="direction" data-fieldid="' + fieldId + '">';
            orderHtml += '<option value="NONE">--None--</option>';
            orderHtml += '<option value="ASC" ' + (isSortAsc ? 'Selected="selected"' : '') + '>Ascending</option>';
            orderHtml += '<option value="DESC" ' + (isSortDesc ? 'Selected="selected"' : '') + '>Descending</option>';
            orderHtml += '</select>';

            var fieldRow = '<tr id="' + fields[i].sortid + '" data-fieldid="' + fieldId + '">';
            fieldRow += '<td class="dragHandle halign"></td>';
            fieldRow += '<td>' + name + '</td>';
            fieldRow += '<td><input type="text" value="' + fields[i].DisplayName + '" name="' + fieldId + '"/></td>';
            fieldRow += '<td class="halign">' + orderHtml + '</td>';
            fieldRow += '<td class="halign"><input type="checkbox" name="' + fieldId + '" class="fieldVisibility" ' + (fields[i].IsVisible ? 'checked="checked"' : '') + 'data-fieldid="' + fieldId + '"/></td>';
            fieldRow += '<td class="halign"><a href="#" title="Remove this field!" class="removeField" data-fieldid="' + fieldId + '"><i class="fa fa-trash-o"></i></a></td>';
            fieldRow += '</tr>';

            $('#selectedFieldsTable tbody').append(fieldRow);

            $('#selectedFieldsTable').tableDnD({
                onDragClass: "highlight",
                dragHandle: ".dragHandle",
                onDrop: function (table, row) {
                    var rows = table.tBodies[0].rows;

                    for (var i = 0; i < rows.length; i++) {
                        var fid = $(rows[i]).data('fieldid');

                        for (j = 0; j < _selectedSelectClauseFields.length; ++j) {
                            var _fid = _selectedSelectClauseFields[j].EntityName + '.' + _selectedSelectClauseFields[j].MappedFieldName;

                            if (fid == _fid) {
                                _selectedSelectClauseFields[j].sortid = i;
                                break;
                            }
                        }
                    }

                    // sort the selected select fields clause by sortid
                    _selectedSelectClauseFields.sort(function (a, b) {
                        return a.sortid > b.sortid;
                    });
                    //console.log(_selectedSelectClauseFields);
                }
            });
        } else if (clauseType == 'Grp') {
            /*var chkHtml = '<div class="chkFieldDiv">';
            chkHtml += '<input type="checkbox" class="groupByField" name="' + fieldId + '" data-fieldid="' + fieldId + '"/>';
            chkHtml += '<label>' + fieldId + '</label>';
            chkHtml += '</div>';
            $('#groupClause').append(chkHtml);*/

            var rowHtml = '<tr id="' + fields[i].sortid + '" data-fieldid="' + fieldId + '">';
            rowHtml += '<td>';
            rowHtml += '<input id="grpField' + fieldId + '" type="checkbox" class="groupByField" name="' + fieldId + '" data-fieldid="' + fieldId + '"/>';
            rowHtml += '<label for="grpField' + fieldId + '">' + fieldId + '</label>';
            rowHtml += '</td>';
            rowHtml += '</tr>';

            $('#groupClause #groupClauseTable tbody').append(rowHtml);

            $('#groupClauseTable').tableDnD({
                onDragClass: "highlight",
                onDrop: function (table, row) {
                    var rows = table.tBodies[0].rows;

                    for (var i = 0; i < rows.length; i++) {
                        var fid = $(rows[i]).data('fieldid');

                        for (j = 0; j < _selectedGroupByClauseFields.length; ++j) {
                            var _fid = _selectedGroupByClauseFields[j].EntityName + '.' + _selectedGroupByClauseFields[j].MappedFieldName;

                            if (fid == _fid) {
                                _selectedGroupByClauseFields[j].sortid = i;
                                break;
                            }
                        }
                    }

                    // sort the selected select fields clause by sortid
                    _selectedGroupByClauseFields.sort(function (a, b) {
                        return a.sortid > b.sortid;
                    });
                }
            });

        } else if (clauseType == 'Ord') {
            $("#selectedFieldsTable select[name='" + fieldId + "'].direction").attr('disabled', null);
        } else if (clauseType == 'OrderBy') {
            $("#selectedFieldsTable select[name='" + fieldId + "'].direction").val(fields[i].SortDirection);
        } else if (clauseType == 'GroupBy') {
            $("#groupClause input[name='" + fieldId + "']").attr('checked', 'checked');
        }
    }

    if (fields.length == 0 && clauseType == 'Filter') {
        html += '<option value="-1">--No field available--</option>';
    }

    $(selectListId).html(html);
    $(selectListId).trigger('change');
    $(selectListId).trigger('liszt:updated');
}

/* for ajax request re-directs */
$("document").ready(function () {

    (function loadAllGlobalVariables() {
        // get the list of global variables.
        $.ajax({
            url: "/QueryBuilder/GetAllGlobalVariables",
            type: "POST",
            data: { "queryType": $("#QueryType").val() },
            success: function (result) {
                if (!result) {
                    alert("Exception Occured while fetching the Global Variables");
                }

                if (result.Error) {
                    alert(result.Error);
                    return false;
                }
                else {
                    for (var prop in result) {
                        if (result.hasOwnProperty(prop) && !_globalVariables.hasOwnProperty(prop)) {
                            _globalVariables.push(result[prop]);
                        }
                    }
                }
            }
        });
    })();

    // DynamicVariable's click handler
    $("#DynamicVariable").click(function () {
        if ($("#DynamicVariable").is(":checked")) {
            $("#AutoPopulateFields").prop("checked", false);
            $("#GlobalVariable").prop("checked", false);
            $("#ColumnVariable").prop("checked", false);

            $("#conditionValueDiv").html('@<input type="text" autocomplete="off" name="conditionValue" id="conditionValue" style="width: 160px; margin-top:12px">');

            $("#conditionOperator option").each(function () {
                if ($(this).val() === "Is" || $(this).val() === "In" || $(this).val() === "Between") {
                    $(this).remove();
                }
            });
        }
        else {
            loadConditionValue();
        }
    });

    // GlobalVariable's click handler
    $("#GlobalVariable").click(function () {
        if ($("#GlobalVariable").is(":checked")) {

            $("#AutoPopulateFields").prop("checked", false);
            $("#DynamicVariable").prop("checked", false);
            $("#ColumnVariable").prop("checked", false);

            $("#conditionValueDiv").html('');

            if (_globalVariables !== undefined && _globalVariables.length > 0) {
                $("#conditionValueDiv").append($("<select/>").attr({ "id": "globalVariablesList" }, { "class": "globalVariablesList" }));
                for (var i = 0; i < _globalVariables.length; i++) {
                    $('#conditionValueDiv #globalVariablesList').append(
                        $("<option></option>")
                        .attr("value", _globalVariables[i].VariableName)
                        .text(_globalVariables[i].Name)
                        );
                }
                $("#conditionValueDiv").append("<script>$('#conditionValueDiv select').attr('style', 'width:75%;');$('#conditionValueDiv select').select2(); </script>");
            }
        } else {
            loadConditionValue();
        }
    });
});

window.currentQueryInfo = null;

/* Filter condition Bracket Additions - START */
var openbracket = "(";
var closebracket = ")";
var conjunctionAND = "AND";
var conjunctionOR = "OR";
var openbracketcount = 0;
var closebracketcount = 0;
var filtercount = 0;

/* new addition STARTS */
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}
/* new addition ENDS */

/* filterString  object declaration */
var FilterString = function (postn, condval, fieldId) {
    this.conditionId = guidGenerator();
    this.position = postn;
    this.value = condval;
    this.FieldIdentifier = fieldId;
    this.toString = function () { return this.value; }
}

var whereconditions = [];

// donot display removable option for the conjunctions.
function getFilterMarkup(id, type) {
    var content = "";
    switch (type) {
        case conjunctionAND:
        case conjunctionOR:
            content = "<div class='bracketmgmt' id ='" + id + "'>&nbsp;" + type + " &nbsp;</div>";
            break;
        default:
            content = "<div class='bracketmgmt' id ='" + id + "'>&nbsp;" + type + " <a class='pl' onclick=\"removeParenthesis('" + id + "')\"><i class=\"fa fa-trash-o\"></i>";
            content += "</a>&nbsp;</div>";
            break;
    }

    return content;
}

function UpdateRemovableFilterConditions() {
    $('#filterClauseDiv .fCondDelete a').each(function () {
        $(this).click(function () {

            var fieldid = $(this).attr('rel').split('~')[0];
            var fieldvalue = $(this).attr('rel').split('~')[1];

            for (var i = 0; i < _selectedFilterClauseFields.length; ++i) {
                var id = _selectedFilterClauseFields[i].EntityName + '.' + _selectedFilterClauseFields[i].MappedFieldName;
                if (id == fieldid && _selectedFilterClauseFields[i].Value == fieldvalue) {
                    /* filter clause bracket code - STARTS */
                    var chosenField = _selectedFilterClauseFields[i];
                    var fieldIndex = chosenField.FieldIdentifier;
                    /* filter clause bracket code - ENDS */

                    _selectedFilterClauseFields.remove(i);

                    /* filter clause bracket code - STARTS */
                    removeFilterFieldFromStringForm(fieldIndex);
                    /* filter clause bracket code - ENDS */

                    break;
                }
            }

            // remvoe that condition div from DOM
            //$(this).parent().parent().remove();
            PopulateFilterClauseDiv(); // repopulate the div
            return false;
        });
    });
}

function getFilterConditionMarkUp(conditionid, conditionFieldId) {
    for (var i = 0; i < _selectedFilterClauseFields.length; ++i) {
        if (_selectedFilterClauseFields[i].FieldIdentifier != conditionFieldId) {
            continue;
        }

        var field = _selectedFilterClauseFields[i];
        var fieldId = field.EntityName + '.' + field.MappedFieldName;

        var cond = '<div class="fCondition" id="' + conditionid + '">';
        cond += '<div class="fCondDelete"><a onclick="removeParenthesis(\'' + conditionid + '\')" title="Delete this condition.">';
        cond += '<i class="fa fa-trash-o"></i></a></div></a></div>';

        if (field.Operator == 'Between') {
            cond += field.EntityName + '.' + field.DisplayName + " is greater than " + field.Value.split(',')[0];
            cond += " and lesser than " + field.Value.split(',')[1];
        }
        else if (field.Operator == 'Is') {
            cond += field.EntityName + '.' + field.DisplayName + " " + field.OperatorDisplayValue + " " + field.Value;
        }
        else {
            cond += field.EntityName + '.' + field.DisplayName + " is " + field.OperatorDisplayValue + " " + field.Value;
        }

        cond += '</div>';
        return cond;
    }
}

function getSelectedFilterClauseItem() {

    var cond = "<ul id=\"tree\">";

    var content = "";

    for (var i = 0; i < whereconditions.length; i++) {
        var tempfs = whereconditions[i];

        switch (tempfs.value) {
            case openbracket:
                cond += "<li><ul>";
            case conjunctionAND:
            case conjunctionOR:
                content = getFilterMarkup(tempfs.conditionId, tempfs.value);
                cond += "<li>" + content + "</li>";
                break;
            case closebracket:
                content = getFilterMarkup(tempfs.conditionId, tempfs.value);
                cond += "<li>" + content + "</li>";
                cond += "</ul></li>";
                break;
            default:
                content = getFilterConditionMarkUp(tempfs.conditionId, tempfs.value);
                //testMarkUp += "<li>" + content + "</li>";
                cond += "<li>" + content + "</li>";
                break;
        }
    }
    cond += "</ul>";
    //testMarkUp += "</ul>";
    //console.log(testMarkUp);

    return cond;
}

function PopulateFilterClauseDiv() {
    /* new change STARTS */
    var cond = $(".fConditionRow");

    if (cond.length < 1) {
        cond = '<div class="fConditionRow" style="display:block">';
        cond += getSelectedFilterClauseItem();
        cond += '</div>';
        //$('#filterClauseDiv .fConditionRow').html("");
        $('#filterClauseDiv').html(cond);
    }
    else {
        var content = getSelectedFilterClauseItem();
        $('#filterClauseDiv .fConditionRow').html("");
        $('#filterClauseDiv .fConditionRow').html(content);
    }
    /* new change ENDS */
    $("#tree").treeview({
        collapsed: false,
        animated: "medium",
        //control: "#sidetreecontrol",
        persist: "location"
    });

    $('#filterClauseDiv').show('fast');
    UpdateRemovableFilterConditions();
}

function removeParenthesis(elementId) {

    if (elementId === undefined || elementId.length < 1) {
        return;
    }

    try {
        for (var i = 0; i < whereconditions.length; i++) {
            var tmpfs = whereconditions[i];

            if (tmpfs.conditionId != elementId) {
                continue;
            }
            if (tmpfs.value === openbracket) {
                openbracketcount = openbracketcount - 1;
            }

            if (tmpfs.value === closebracket) {
                closebracketcount = closebracketcount - 1;
            }

            for (var j = 0; j < _selectedFilterClauseFields.length; j++) {
                if (tmpfs.FieldIdentifier != _selectedFilterClauseFields[j].FieldIdentifier) {
                    continue;
                }
                _selectedFilterClauseFields.splice(j, 1);
                break;
            }

            whereconditions.splice(i, 1);
            var delid = "#" + tmpfs.conditionId;
            $(delid).remove();
            break;
        }
        removeFilterFieldFromStringForm();
    }
    catch (e) {
        alert(e);
        return false;
    }

    if ($("#filterClauseDiv") !== undefined && $("#filterClauseDiv .fConditionRow") !== undefined) {
        if ($("#filterClauseDiv .fConditionRow .bracketmgmt:first-child") !== undefined) {
            if (jQuery.trim($("#filterClauseDiv .fConditionRow .bracketmgmt:first-child").text()) === conjunctionAND
            || jQuery.trim($("#filterClauseDiv .fConditionRow .bracketmgmt:first-child").text()) === conjunctionOR) {
                $("#filterClauseDiv .fConditionRow .bracketmgmt:first-child").text('');
            }
        }
    }
    PopulateFilterClauseDiv();
}

function removeFilterFieldFromStringForm() {
    // remove the prefix conjunctions or the close brackets
    if (whereconditions.length > 0 && (whereconditions[0].value === conjunctionAND || whereconditions[0].value === conjunctionOR || whereconditions[0].value === closebracket)) {
        whereconditions.splice(0, 1);
    }

    // do the validation for the correctness of the formation of parenthesis
    validateFilterClauses(whereconditions, 0);

    if (whereconditions.length < 1) {
        return;
    }

    // remove the orphaned conjunctions after field removal.
    var conjunctionPosition = whereconditions.length - 1;
    if (whereconditions[conjunctionPosition].value === conjunctionAND || whereconditions[conjunctionPosition].value === conjunctionOR) {
        var tempfs = whereconditions[conjunctionPosition];
        $("#" + tempfs.conditionId).remove();
        whereconditions.splice(conjunctionPosition, 1);
    }
}

function validateFilterClauses(whereconditions, startvalue) {
    for (var i = startvalue; i < whereconditions.length - 1; i++) {
        var stv = whereconditions[i];
        var env = whereconditions[i + 1];
        var tempfs;

        var reValuate = false;

        if (stv.value === openbracket && env.value === closebracket) {
            tempfs = whereconditions[i + 1];
            whereconditions.splice(i + 1, 1);
            $("#" + tempfs.conditionId).remove();
            tempfs = whereconditions[i];
            whereconditions.splice(i, 1);
            $("#" + tempfs.conditionId).remove();
            reValuate = true;
        }

        if (stv.value === openbracket && (env.value === conjunctionAND || env.value === conjunctionOR)) {
            tempfs = whereconditions[i + 1];
            whereconditions.splice(i + 1, 1);
            $("#" + tempfs.conditionId).remove();
            reValuate = true;
        }

        if ((stv.value === conjunctionAND || stv.value === conjunctionOR) && env.value === closebracket) {
            tempfs = whereconditions[i];
            whereconditions.splice(i, 1);
            $("#" + tempfs.conditionId).remove();
            reValuate = true;
        }

        if ((env.value === conjunctionAND || env.value === conjunctionOR) && (stv.value === conjunctionOR || stv.value === conjunctionAND)) {
            tempfs = whereconditions[i + 1];
            whereconditions.splice(i + 1, 1);
            $("#" + tempfs.conditionId).remove();
            reValuate = true;
        }

        if (stv.value === closebracket && env.value === openbracket) {
            alert("The parenthesis formation in the filter clause is invalid. Consider revising the filter clause");
            return false;
        }

        if (reValuate) {
            if ((i + 1) < whereconditions.length) {
                return validateFilterClauses(whereconditions, 0);
            }
        }

        if ((i + 1) < whereconditions.length) {
            return validateFilterClauses(whereconditions, i + 1);
        }
    }
}

function loadConditionValue() {
    var id = $('#conditionField').val();
    var field = GetFilterClauseField(id);

    var valueHtml = '<input type="text"  name="conditionValue" id="conditionValue" style="width: 160px;margin-top:5px" />';

    switch (field.Type.toLowerCase()) {
        case 'datetime':
        case 'time':
        case 'date':
        case 'timestamp':
            valueHtml += "<script>$('#conditionValue').datepicker();</script>";
            break;
    }
    $('#conditionValueDiv').html(valueHtml);
}

function GetFilterClauseField(fieldId) {
    for (var i = 0; i < _filterClauseFields.length; ++i) {
        var id = _filterClauseFields[i].EntityName + '.' + _filterClauseFields[i].MappedFieldName;
        if (id == fieldId) {
            var temp = eval('(' + JSON.stringify(_filterClauseFields[i]) + ')');
            return temp;
        }
    }

    return null;
}

/* Filter condition validation variables - STARTS */
var previndx = 0;
var prevval = "";

var currindx = 0;
var currval = "";
/* Filter condition validation variables - ENDS */

/* Filter condition Bracket Additions - ENDS */

// sort the select box control
$.fn.sort_select_box = function () {
    var my_options = $("#" + this.attr('id') + ' option');
    my_options.sort(function (a, b) {
        if (a.text > b.text) return 1;
        else if (a.text < b.text) return -1;
        else return 0;
    });

    //replace with sorted my_options;
    $(this).empty().append(my_options);

    // clearing any selections
    $("#" + this.attr('id') + " option").attr('selected', false);
}

//pass the options variable to the function
var celloQueryBuilder = function (options) {
    //Set the default values, use comma to separate the settings
    var defaults = {
        formMode: 'Insert',
        modulesDropDownListId: '#moduleDDL',
        entitiesDropDownListId: '#entityDDL',
        relatedEntitiesDisplayId: '#relatedEntitiesDiv',
        selectClauseFields: '#selectClauseFields',
        orderByClauseFields: '#orderByFields',
        groupByClauseFields: '#groupByFields',
        selectedselectClauseFields: '#selectedselectClauseFields',
        selectedorderByClauseFields: '#selectedorderByFields',
        selectedgroupByClauseFields: '#selectedgroupByFields',
        filterClauseFields: '#conditionField',
        btnRightSelectClause: '#btnRightSelectClause',
        btnLeftSelectClause: '#btnLeftSelectClause',
        btnRightGroupClause: '#btnRightGroupClause',
        btnLeftGroupClause: '#btnLeftGroupClause',
        btnRightOrderByClause: '#btnRightOrderByClause',
        btnLeftOrderByClause: '#btnLeftOrderByClause',
        entityLoadURL: '',
        fieldsLoadURL: '',
        relatedEntitiesLoadURL: '',
        executeQueryURL: '',
        fetchQueryDetailsURL: '',
        saveQueryURL: ''
    };

    // merge the passed and default settings options
    var options = jQuery.extend(defaults, options);

    var SelectClause = 'SelectClause';
    var GroupByClause = 'GroupByClause';
    var OrderByClause = 'OrderByClause';
    var FilterClause = 'FilterClause';

    var SelectedSelectClause = 'SelectedSelectClause';
    var SelectedGroupByClause = 'SelectedGroupByClause';
    var SelectedOrderByClause = 'SelectedOrderByClause';

    var _relatedEntitiesCache = {};
    var _editParentEntityIds = [];
    var _editChildEntityIds = [];
    //var _selectClauseFields = []; // Moved to outerscope
    // var _groupByClauseFields = []; // Moved to outerscope
    //var _orderByClauseFields = []; // Moved to outerscope
    //var _filterClauseFields = []; // Moved to outerscope

    //var _selectedSelectClauseFields = []; // Moded to outerscope
    //var _selectedGroupByClauseFields = []; // Moved to outerscope
    //var _selectedOrderByClauseFields = []; // Moved to outerscope
    //var _selectedFilterClauseFields = []; // Moved to outerscope

    var editModeLoaded = false;
    //var _savedQueryInfo = null;

    // remove the fields form the cache which has EntityName equals to given entityId
    function RemoveEntity(cache, entityId) {
        if (!entityId) {
            cache.splice(0, cache.length);
            return;
        }

        for (var i = 0; i < cache.length; ++i) {
            if (cache[i].EntityName == entityId) {
                cache.remove(i);
                --i;
            }
        }
    }

    // resets the caches 
    function ResetCache(entityId) {
        RemoveEntity(_selectClauseFields, entityId);
        RemoveEntity(_groupByClauseFields, entityId);
        RemoveEntity(_orderByClauseFields, entityId);
        RemoveEntity(_filterClauseFields, entityId);

        RemoveEntity(_selectedSelectClauseFields, entityId);
        RemoveEntity(_selectedGroupByClauseFields, entityId);
        RemoveEntity(_selectedOrderByClauseFields, entityId);
        RemoveEntity(_selectedFilterClauseFields, entityId);

        _editParentEntityIds = [];
        _editChildEntityIds = [];

        if (!entityId) {
            $('#filterClauseDiv').html('');
            $('#filterClauseDiv').hide('fast');
        }

        _populateControls(options.selectClauseFields, _selectClauseFields, '');
        _populateControls(options.groupByClauseFields, _groupByClauseFields, 'Grp');
        _populateControls(options.orderByClauseFields, _orderByClauseFields, 'Ord');
        _populateControls(options.filterClauseFields, _filterClauseFields, 'Filter');

        _populateControls(options.selectedselectClauseFields, _selectedSelectClauseFields, 'Select');
        _populateControls(options.selectedgroupByClauseFields, _selectedGroupByClauseFields, 'GroupBy');
        _populateControls(options.selectedorderByClauseFields, _selectedOrderByClauseFields, 'OrderBy');
    }

    function FromFieldInfoCacheFromModel(model, clause) {
        var fields = [];
        var aggFunEnumMapping = ['NONE', 'COUNT', 'SUM', 'AVG', 'MIN', 'MAX', 'VAR', 'STDEV'];
        var sortDirEnumMapping = ['ASC', 'DESC'];
        var conjEnumMapping = ['And', 'Or'];
        var filterOpEnumMapping = ['Equals', 'NotEquals', 'IsNull', 'IsNotNull', 'GreaterThan', 'LesserThan', 'GreaterThanAndEqualTo', 'LesserThanAndEqualTo', 'Like', 'In', 'Is', 'Between'];
        var filterOpDisplayNameMapping = ['Equals To', 'Not Equals To', 'Is Null', 'Is Not Null', 'Greater Than', 'Lesser Than', 'Greater Than And EqualTo', 'Lesser Than And EqualTo', 'Like', 'In', 'Is', 'Between'];

        for (var i = 0; i < model.length; ++i) {
            var f = [];

            // ---------------------
            // set the default entity name for the expression columns
            // ---------------------
            if (model[i].FieldInfo !== undefined && model[i].FieldInfo.IsExpressionColumn) {
                model[i].FieldInfo.EntityName = expressionColumnEntityName;
            }

            switch (clause) {
                case SelectClause:
                    f = model[i].FieldInfo;
                    f.AggregateFunction = aggFunEnumMapping[model[i].AggregateFunction];
                    break;
                case GroupByClause:
                    f = model[i];
                    break;
                case OrderByClause:
                    f = model[i].FieldInfo;
                    f.SortDirection = sortDirEnumMapping[model[i].SortDirection];
                    break;
                case FilterClause:
                    f = model[i].FieldInfo;
                    f.Conjuction = conjEnumMapping[model[i].Conjuction];
                    f.Operator = filterOpEnumMapping[model[i].Operator];
                    f.OperatorDisplayValue = filterOpDisplayNameMapping[model[i].Operator];
                    f.Value = model[i].Value;
                    f.ValueLabel = model[i].ValueLabel;
                    f.ValueType = getValueTypeMapping(model[i].ValueType);
                    break;
            }
            fields.push(f);
        }
        return fields;
    }

    function loadTenantFieldForEntity(entityId) {
        $.ajax({
            url: '/QueryBuilder/GetPrimaryEntityTenantField',
            type: "POST",
            data: { 'entityId': entityId },
            async: false,
            success: function (data) {
                var tenantFields = [];

                if (data !== undefined) {
                    _selectClauseFields.push(data);
                }
                _populateControls("#selectClauseFields", _selectClauseFields, '');
                MergeFields(_selectClauseFields, data, true);
            }
        });
    }

    function loadPrimaryKey(entityId) {
        $.ajax({
            url: '/QueryBuilder/GetPrimaryEntityPrimaryKey',
            type: "POST",
            data: { 'entityId': entityId },
            async: false,
            success: function (data) {
                _populateControls("#selectedselectClauseFields", data, '');
                MergeFields(_selectedSelectClauseFields, data, true);
            }
        });
    }

    // loads the fields for the given entity identifier
    function loadFields(entityId, loadentityId) {

        var queryid = undefined;

        if (jQuery.trim($("#queryId").val()).length > 0) {
            queryid = jQuery.trim($("#queryId").val());
        }

        var data = {};
        data.entityId = loadentityId;

        if (queryid !== undefined) {
            data.queryId = queryid;
        }

        $.post(options.fieldsLoadURL, data, function (data) {
            if (data.Error) {
                alert('Load Entity Fields: ' + data.Error);
            }
            else {
                if (entityId != loadentityId && data != null && data.length > 0) {
                    for (var i = 0; i < data.length; ++i) {
                        data[i].EntityName = entityId;
                    }
                }

                if (options.formMode == 'Edit' && editModeLoaded) {
                    RemoveFields(data, _selectedSelectClauseFields);
                    //RemoveFields(data, _selectedFilterClauseFields);
                }

                populateFields(SelectClause, entityId, data);
                populateFields(FilterClause, entityId, data);
                populateFields(FilterClause, entityId, _selectedSelectClauseFields);

                if (options.formMode == 'Edit' && !editModeLoaded) {
                    // edit mode
                    var queryId = $('#queryId').val();

                    // fetch the query details
                    $.post(options.fetchQueryDetailsURL, { queryId: queryId }, function (data) {
                        //data = JSON.parse(data);
                        if (data.Error) {
                            alert(data.Error);
                        } else {
                            //_savedQueryInfo = data;

                            var items = FromFieldInfoCacheFromModel(data.SelectClauseFields, SelectClause);
                            RemoveFields(_selectClauseFields, items);
                            populateFields(SelectedSelectClause, null, items);

                            // ---------------------------------------------------
                            // Add the expression fields to the filter clause also
                            // ---------------------------------------------------
                            var exprFields = [];
                            for (var sf = 0; sf < data.SelectClauseFields.length; sf++) {
                                if (data.SelectClauseFields[sf].FieldInfo.IsExpressionColumn) {
                                    exprFields.push(data.SelectClauseFields[sf].FieldInfo);
                                }
                            }
                            populateFields(FilterClause, '', exprFields);
                            populateFields(OrderByClause, '', exprFields);

                            // --------------------------
                            // Filter clause addition END
                            // --------------------------

                            var temp = _orderByClauseFields;

                            items = FromFieldInfoCacheFromModel(data.GroupClauseFields, GroupByClause);
                            RemoveFields(_groupByClauseFields, items);
                            populateFields(SelectedGroupByClause, null, items);

                            _orderByClauseFields = temp;

                            items = FromFieldInfoCacheFromModel(data.OrderClauseFields, OrderByClause);
                            RemoveFields(_orderByClauseFields, items);
                            populateFields(SelectedOrderByClause, null, items);

                            _selectedFilterClauseFields = FromFieldInfoCacheFromModel(data.FilterClauseFields, FilterClause);

                            _editParentEntityIds = data.ParentEntityIdentifiers;
                            _editChildEntityIds = data.ChildEntityIdentifiers;

                            loadRelatedEntities($(options.modulesDropDownListId).val(), $(options.entitiesDropDownListId).val(), function () {
                                editModeLoaded = true;
                            });

                            var filterPattern = data.WhereCondition;

                            if (filterPattern) {
                                var pattern = filterPattern.split('~');

                                var maxFilterCount = 0;

                                for (var i = 0; i < pattern.length; i++) {
                                    whereconditions.push(new FilterString(pattern[i], pattern[i], pattern[i]));

                                    if (pattern[i] !== openbracket && pattern[i] !== closebracket && pattern[i] !== conjunctionAND && pattern[i] !== conjunctionOR) {
                                        maxFilterCount = pattern[i];
                                    }
                                }

                                filtercount = parseInt(maxFilterCount) + 1;
                            }

                            // ---------------------------
                            // Load the Expression Columns 
                            // ---------------------------
                            if (data.ExpressionColumns != undefined && data.ExpressionColumns.length > 0) {
                                for (var i = 0; i < data.ExpressionColumns.length; i++) {
                                    _expressionColumns.push(new ExpressionColumnInfo(data.ExpressionColumns[i].Expression, data.ExpressionColumns[i].Alias, data.ExpressionColumns[i].Ordinal, getTypeNameFromKey(data.ExpressionColumns[i].DataType), data.ExpressionColumns[i].ExpressionFields));
                                }
                            }

                            // ---------------------------
                            // Load the Expression Fields
                            // ---------------------------
                            if (data.ExpressionFields != undefined && data.ExpressionFields.length > 0) {
                                for (var v = 0; v < data.ExpressionFields.length; v++) {
                                    _expressionFields.push(data.ExpressionFields[v]);
                                }
                            }

                            expressionStack = data.Expressions || [];

                            for (var exp = 0; exp < expressionStack.length; exp++) {
                                expressionStack[exp].DataType = getTypeNameFromKey(expressionStack[exp].DataType);
                            }

                            PopulateFilterClauseDiv();
                            // ----------------------------------------------------------------
                            // Load the expression columns for editing when the query is loaded
                            // ----------------------------------------------------------------
                            addToExpressionsClause();
                            //editModeLoaded = true;
                        }
                    });
                }

                $('#conditionField').trigger('change');

                if ($("#QueryType").length > 0 && $("#QueryType").val() === "DatascopeQuery") {
                    loadPrimaryKey(entityId);
                    loadTenantFieldForEntity(entityId);
                    //$("#btnRightSelectClause").off("click");
                    $(".moveicons").parent().off("click");
                    $("#selectClauseFields").attr("disabled", "disabled");
                    $("#groupByFields").attr("disabled", "disabled");
                    $("#orderByFields").attr("disabled", "disabled");
                }
                else {
                    $(".moveicons").parent().on("click");
                }
            }
        }, 'json');
    }

    function RemoveFields(cache, fields) {
        for (var i = 0; i < cache.length; ++i) {
            if (contains(fields, cache[i])) {
                cache.remove(i);
                --i;
            }
        }
    }

    function Sort(cache) {
        cache.sort(function (a, b) {
            var l = a.EntityName + '.' + a.DisplayName;
            var r = b.EntityName + '.' + b.DisplayName;
            if (l > r) return 1;
            else if (l < r) return -1;
            else return 0;
        });
    }

    function populateFields(clause, entityId, data) {
        switch (clause) {
            case SelectClause:
                MergeFields(_selectClauseFields, data, true);

                if (_selectedSelectClauseFields.length == 0) {
                    _selectedGroupByClauseFields = [];
                } else {
                    RemoveFields(_selectedGroupByClauseFields, data);
                }

                Sort(_selectClauseFields);

                _populateControls(options.selectClauseFields, _selectClauseFields, '');
                _populateControls(options.selectedselectClauseFields, _selectedSelectClauseFields, 'Select');

                _groupByClauseFields = [];
                var temp = _selectedSelectClauseFields.slice();
                RemoveFields(temp, _selectedGroupByClauseFields);
                populateFields(GroupByClause, null, temp);
                break;
            case GroupByClause:
                MergeFields(_groupByClauseFields, data, false);

                _orderByClauseFields = [];
                if (_selectedGroupByClauseFields.length == 0) {
                    var temp = _selectClauseFields.slice();
                    MergeFields(temp, _selectedSelectClauseFields, false);
                    RemoveFields(temp, _selectedOrderByClauseFields);
                    populateFields(OrderByClause, null, temp);
                } else {
                    var temp = _selectedGroupByClauseFields.slice();
                    RemoveFields(temp, _selectedOrderByClauseFields);
                    populateFields(OrderByClause, null, temp);
                }

                var mergedGroupFields = $.merge($.merge([], _groupByClauseFields), _selectedGroupByClauseFields);
                Sort(mergedGroupFields);

                _populateControls(options.groupByClauseFields, mergedGroupFields, 'Grp');
                _populateControls(options.selectedgroupByClauseFields, _selectedGroupByClauseFields, 'GroupBy');
                break;
            case OrderByClause:
                MergeFields(_orderByClauseFields, data, false);
                UnionFields(_orderByClauseFields, _selectedOrderByClauseFields);

                var mergedOrderFields = $.merge($.merge([], _orderByClauseFields), _selectedOrderByClauseFields);
                //Sort(mergedOrderFields);

                _populateControls(options.orderByClauseFields, mergedOrderFields, 'Ord');
                _populateControls(options.selectedorderByClauseFields, _selectedOrderByClauseFields, 'OrderBy');
                break;
            case FilterClause:
                MergeFields(_filterClauseFields, data, false);

                //Sort(_filterClauseFields);

                _populateControls(options.filterClauseFields, _filterClauseFields, 'Filter');
                break;
            case SelectedSelectClause:
                MergeFields(_selectedSelectClauseFields, data, true);

                _populateControls(options.selectClauseFields, _selectClauseFields, '');
                _populateControls(options.selectedselectClauseFields, _selectedSelectClauseFields, 'Select');

                _groupByClauseFields = [];
                var temp = _selectedSelectClauseFields.slice();
                RemoveFields(temp, _selectedGroupByClauseFields);
                MergeFields(_groupByClauseFields, temp, false);
                UnionFields(_groupByClauseFields, _selectedGroupByClauseFields);

                CheckGroupByFields();

                var mergedGroupFields = $.merge($.merge([], _groupByClauseFields), _selectedGroupByClauseFields);
                Sort(mergedGroupFields);

                _populateControls(options.groupByClauseFields, mergedGroupFields, 'Grp');
                _populateControls(options.selectedgroupByClauseFields, _selectedGroupByClauseFields, 'GroupBy');
                break;
            case SelectedGroupByClause:
                MergeFields(_selectedGroupByClauseFields, data, false);

                var mergedGroupFields = $.merge($.merge([], _groupByClauseFields), _selectedGroupByClauseFields);
                Sort(mergedGroupFields);

                _populateControls(options.groupByClauseFields, mergedGroupFields, 'Grp');
                _populateControls(options.selectedgroupByClauseFields, _selectedGroupByClauseFields, 'GroupBy');

                _orderByClauseFields = [];
                MergeFields(_orderByClauseFields, _selectedGroupByClauseFields.slice(), false);
                UpdatedOrderByFields();
                populateFields(OrderByClause, null, []);
                break;
            case SelectedOrderByClause:
                MergeFields(_selectedOrderByClauseFields, data, false);

                var mergedOrderFields = $.merge($.merge([], _orderByClauseFields), _selectedOrderByClauseFields);
                //Sort(mergedOrderFields);

                _populateControls(options.orderByClauseFields, mergedOrderFields, 'Ord');
                _populateControls(options.selectedorderByClauseFields, _selectedOrderByClauseFields, 'OrderBy');
                break;
        }
    }

    function CheckGroupByFields() {
        var temp = [];
        var hasAggFun = false;
        for (var i = 0; i < _selectedSelectClauseFields.length; ++i) {
            if (_selectedSelectClauseFields[i].AggregateFunction == 'NONE') {
                temp.push(_selectedSelectClauseFields[i]);
            } else {
                hasAggFun = true;
            }
        }

        if (hasAggFun) {
            RemoveFields(_groupByClauseFields, temp);
            populateFields(SelectedGroupByClause, null, temp);
        }
    }

    function UpdatedOrderByFields() {
        for (var i = 0; i < _selectedOrderByClauseFields.length; ++i) {
            if (!contains(_orderByClauseFields, _selectedOrderByClauseFields[i])) {
                _selectedOrderByClauseFields.remove(i);
                --i;
            } else {
                RemoveFields(_orderByClauseFields, [_selectedOrderByClauseFields[i]]);
            }
        }
    }

    function MergeFields(cache, fields, includeExtn) {
        for (var i = 0; i < fields.length; ++i) {
            if (fields[i].IsExtensionField && !includeExtn) {
                continue;
            }
            if (!contains(cache, fields[i])) {
                cache.push(fields[i]);
            }
        }
    }

    function contains(cache, field) {
        for (var i = 0; i < cache.length; ++i) {
            if ((cache[i].IsExpressionColumn && field.IsExpressionColumn)) {
                if (cache[i].DisplayName === field.DisplayName && cache[i].MappedFieldName === field.MappedFieldName) {
                    return true;
                }
            }
            else {
                if (cache[i].EntityName == field.EntityName && cache[i].MappedFieldName == field.MappedFieldName) {
                    return true;
                }
            }
        }
        return false;
    }

    function UnionFields(source, destination) {
        var temp = [];
        for (var i = 0; i < destination.length; ++i) {
            if (contains(source, destination[i])) {
                temp.push(destination[i]);
            }
        }
        destination = temp;
    }

    function loadRelatedEntities(moduleId, entityId, callbackFun) {
        // load the related entites
        $.post(options.relatedEntitiesLoadURL, { moduleId: moduleId, entityId: entityId }, function (data) {
            var phtml = '';
            var chtml = '';

            if (data.Error) {
                data = [];
            }

            var editEntityIds = _editChildEntityIds.concat(_editParentEntityIds);
            var editEntityModel = [];

            // construct parent entities
            for (i = 0; i < data.length; i++) {
                var chkbox = '<div class="entityDiv"><input type="Checkbox" rel="' + data[i].EntityIdentifier + '" name="' + data[i].SchemaTableName + '"';

                if (options.formMode == 'Edit' && !editModeLoaded && editEntityIds.indexOf(data[i].SchemaTableName) != -1) {
                    chkbox += ' checked="checked" ';
                    editEntityModel.push(data[i]);
                }

                chkbox += ' id="relEntity' + data[i].EntityIdentifier + '" />';
                chkbox += '<label for="relEntity' + data[i].EntityIdentifier + '" title="' + data[i].SchemaTableName + '">' + data[i].SchemaTableName + '</label>';
                chkbox += '</div>';

                if (data[i].TypeName == 'ParentEntity') {
                    phtml += chkbox;
                } else if (data[i].TypeName == 'ChildEntity') {
                    chtml += chkbox;
                }
            }

            if (phtml == '') {
                phtml = 'No Parent Entities Found!';
            }

            if (chtml == '') {
                chtml = 'No Child Entities Found!';
            }

            $('#ParentEntitiesTable').html(phtml);
            $('#ChildEntitiesTable').html(chtml);

            // show the div
            $(options.relatedEntitiesDisplayId).show();

            // load the edit mode fields
            if (options.formMode == 'Edit') {
                if (callbackFun != undefined) {
                    callbackFun(); // set edit mode loaded to true
                }
                for (var i = 0; i < editEntityModel.length; ++i) {
                    loadFields(editEntityModel[i].SchemaTableName, editEntityModel[i].EntityIdentifier);
                }
            }

            // bind the checkbox for click event to load the field infos
            $('input[type=checkbox]', options.relatedEntitiesDisplayId).each(function () {
                $(this).click(function () {
                    var loadentityId = $(this).attr('rel');
                    var entityId = $(this).attr('name');

                    if ($(this).is(':checked')) {
                        // load fields
                        loadFields(entityId, loadentityId);
                    } else {
                        // unload fields
                        ResetCache(entityId);
                    }
                });
            });
        }, 'json');
    }

    function UpdateFieldInfo(field, clause) {
        if (clause == SelectedOrderByClause) {
            field.SortDirection = $('#SortDirection').val();
        } else if (clause == SelectedSelectClause) {
            var aggName = $('#AggregateFunction').val();
            if (aggName != '-1') {
                field.AggregateFunction = aggName;
            } else {
                field.AggregateFunction = 'NONE';
            }
        }
    }

    function _populate(items, cache, clause) {
        var includeFields = [];
        for (var i = 0; i < cache.length; ++i) {
            var id = cache[i].EntityName + '.' + cache[i].MappedFieldName;
            if (items.indexOf(id) != -1) {
                UpdateFieldInfo(cache[i], clause);
                includeFields.push(cache[i]);
                cache.remove(i);
                --i;
            }
        }
        populateFields(clause, null, includeFields);
    }

    function getAllFields(cache, clause) {
        var selectFields = [];
        for (var s = 0; s < cache.length; ++s) {
            var info = {};
            info.FieldInfo = cache[s];
            if (clause == SelectClause) {
                info.AggregateFunction = cache[s].AggregateFunction;
            } else if (clause == OrderByClause) {
                info.SortDirection = cache[s].SortDirection;
            } else if (clause == GroupByClause) {
                info = cache[s];
            } else if (clause == FilterClause) {
                if (whereconditions.length > 0) {
                    info.Operator = cache[s].Operator;
                    info.Value = cache[s].Value;
                    info.Conjuction = cache[s].Conjuction;
                    info.ValueLabel = cache[s].ValueLabel;
                    info.ValueType = cache[s].ValueType;
                }
                else {
                    continue;
                }
            }
            selectFields.push(info);
        }
        return selectFields;
    }

    function PopulateFilterClauseDiv() {
        var cond = $(".fConditionRow");

        if (cond.length < 1) {
            cond = '<div class="fConditionRow" style="display:block">';
            cond += getSelectedFilterClauseItem();
            cond += '</div>';
            $('#filterClauseDiv').html(cond);
        }
        else {
            var content = getSelectedFilterClauseItem();
            $('#filterClauseDiv .fConditionRow').html("");
            $('#filterClauseDiv .fConditionRow').html(content);
        }

        $("#tree").treeview({
            collapsed: false,
            animated: "medium",
            persist: "location"
        });

        $('#filterClauseDiv').show('fast');
        UpdateRemovableFilterConditions();
    }

    function getSelectedFilterClauseItem() {
        var treeControls = "";

        var cond = treeControls + "<ul id=\"tree\" style=\"list-style-type:none;\">";

        var content = "";

        for (var i = 0; i < whereconditions.length; i++) {
            var tempfs = whereconditions[i];

            switch (tempfs.value) {
                case openbracket:
                    cond += "<li><ul>";
                case conjunctionAND:
                case conjunctionOR:
                    content = getFilterMarkup(tempfs.conditionId, tempfs.value);
                    cond += "<li>" + content + "</li>";
                    break;
                case closebracket:
                    content = getFilterMarkup(tempfs.conditionId, tempfs.value);
                    cond += "<li>" + content + "</li>";
                    cond += "</ul></li>";
                    break;
                default:
                    content = getFilterConditionMarkUp(tempfs.conditionId, tempfs.value);
                    cond += "<li>" + content + "</li>";
                    break;
            }
        }
        cond += "</ul>";

        return cond;
    }

    function getFilterConditionMarkUp(conditionid, conditionFieldId) {
        for (var i = 0; i < _selectedFilterClauseFields.length; ++i) {
            if (_selectedFilterClauseFields[i].FieldIdentifier != conditionFieldId) {
                continue;
            }

            var field = _selectedFilterClauseFields[i];
            var fieldId = field.EntityName + '.' + field.MappedFieldName;

            var cond = '<div class="fCondition" id="' + conditionid + '">';
            cond += '<div class="fCondDelete"><a onclick="removeParenthesis(\'' + conditionid + '\')" title="Delete this condition.">';
            cond += '<i class="fa fa-trash-o"></i></a></div>';

            var fieldvalue = field.Value;

            if (field.ValueType === "GlobalVariable" || field.ValueType === "DynamicVariable") {
                fieldvalue = field.ValueLabel;
            }

            if (field.Operator == 'Between') {
                cond += field.EntityName + '.' + field.DisplayName + " is greater than " + fieldvalue.split(',')[0];
                cond += " and lesser than " + fieldvalue.split(',')[1];
            }
            else if (field.Operator == 'Is') {
                cond += field.EntityName + '.' + field.DisplayName + " " + field.OperatorDisplayValue + " " + fieldvalue;
            }
            else {
                cond += field.EntityName + '.' + field.DisplayName + " is " + field.OperatorDisplayValue + " " + fieldvalue;
            }

            cond += '</div>';
            return cond;
        }
    }

    function UpdateRemovableFilterConditions() {
        $('#filterClauseDiv .fCondDelete a').each(function () {
            $(this).click(function () {

                var fieldid = $(this).attr('rel').split('~')[0];
                var fieldvalue = $(this).attr('rel').split('~')[1];

                for (var i = 0; i < _selectedFilterClauseFields.length; ++i) {
                    var id = _selectedFilterClauseFields[i].EntityName + '.' + _selectedFilterClauseFields[i].MappedFieldName;
                    if (id == fieldid && _selectedFilterClauseFields[i].Value == fieldvalue) {
                        /* filter clause bracket code - STARTS */
                        var chosenField = _selectedFilterClauseFields[i];
                        var fieldIndex = chosenField.FieldIdentifier;
                        /* filter clause bracket code - ENDS */

                        _selectedFilterClauseFields.remove(i);

                        /* filter clause bracket code - STARTS */
                        removeFilterFieldFromStringForm(fieldIndex);
                        /* filter clause bracket code - ENDS */

                        break;
                    }
                }

                // remvoe that condition div from DOM
                //$(this).parent().parent().remove();
                PopulateFilterClauseDiv(); // repopulate the div
                return false;
            });
        });
    }

    function FormQueryInfoModel(entityId) {
        var moduleId = $(options.modulesDropDownListId).val();
        var parentEntityIds = [];
        var childEntityIds = [];
        $('input[type=checkbox]', '#ParentEntitiesTable').each(function () {
            if ($(this).is(':checked')) {
                parentEntityIds.push($(this).attr('name'));
            }
        });

        $('input[type=checkbox]', '#ChildEntitiesTable').each(function () {
            if ($(this).is(':checked')) {
                childEntityIds.push($(this).attr('name'));
            }
        });

        CheckGroupByFields();

        var alias;

        if ($("#primaryEntityAlias").length > 0) {
            alias = $("#primaryEntityAlias").val();
        }
        else {
            alias = entityId;
        }

        var thisQueryInfo = {
            'ModuleId': moduleId,
            'PrimaryEntityIdentifier': entityId,
            'PrimaryEntityAlias': alias,
            'ParentEntityIdentifiers': parentEntityIds,
            'ChildEntityIdentifiers': childEntityIds,
            'SelectClauseFields': getAllFields(_selectedSelectClauseFields, SelectClause),
            'GroupClauseFields': getAllFields(_selectedGroupByClauseFields, GroupByClause),
            'OrderClauseFields': getAllFields(_selectedOrderByClauseFields, OrderByClause),
            'FilterClauseFields': getAllFields(_selectedFilterClauseFields, FilterClause),
            'WhereCondition': whereconditions.join("~"),
            'ExpressionColumns': _expressionColumns,
            'ExpressionFields': _expressionFields
        };

        // for paging
        window.currentQueryInfo = thisQueryInfo;

        return thisQueryInfo;
    }

    var lastestQueryDetails = {};

    return {
        _export: function (type) {
            if (lastestQueryDetails == null || lastestQueryDetails == undefined) {
                return false;
            }

            var temp = {
                exportType: type,
                queryId: lastestQueryDetails.queryId,
                queryName: lastestQueryDetails.queryName,
                queryDescription: lastestQueryDetails.queryDesc || "",
                queryInfo: lastestQueryDetails.queryInfo
            };

            temp.queryInfo.Expressions = expressionStack;

            $("#JsonData").val(JSON.stringify(temp));
            $("#exportForm").submit();
        },

        init: function () {
            var o = options;
            $('#saveForm').submit(function () {
                var queryName = $('#queryName').val();
                var queryId = $('#queryId').val();
                var queryDesc = $('#queryDescription').val();
                var renderMode = $('#renderMode').val();
                var entityId = $(o.entitiesDropDownListId).val();

                if (entityId == '-1') {
                    alert('Please choose a enity & form a valid query for Saving!');
                    return false;
                }

                if (_selectedSelectClauseFields.length == 0) {
                    alert('Please atleast select a field in select clause to save the query.');
                    return false;
                }

                for (i = 0; i < _selectedSelectClauseFields.length; ++i) {
                    if (_selectedSelectClauseFields[i].DisplayName == '') {
                        alert('Display name cannot be empty!');
                        return false;
                    }

                    for (j = i + 1; j < _selectedSelectClauseFields.length; ++j) {
                        if (_selectedSelectClauseFields[i].DisplayName == _selectedSelectClauseFields[j].DisplayName) {
                            alert('Display name should not contain duplicates "' + _selectedSelectClauseFields[i].DisplayName + '"');
                            return false;
                        }
                    }
                }

                if (queryName == '') {
                    alert('Enter a valid Query Name to save!');
                    return false;
                }

                if ($('#saveForm').data('inProgress')) {
                    return false;
                }
                $('#saveForm').data('inProgress', true);
                setTimeout(function () { $('#saveForm').data('inProgress', false); }, 10000);

                var queryType = $("#QueryType").val();

                var queryInfo = FormQueryInfoModel(entityId);
                lastestQueryDetails = {
                    'queryId': queryId,
                    'queryName': queryName,
                    'queryDescription': queryDesc,
                    'renderMode': renderMode,
                    'queryInfo': queryInfo,
                    'IsGlobal': $(".globalQuery").is(":checked"),
                    'QueryType': queryType
                };

                lastestQueryDetails.queryInfo.Expressions = expressionStack;
                $.ajax({
                    url: o.saveQueryURL,
                    data: JSON.stringify(lastestQueryDetails),
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'html',
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data.Error) {
                            $('#executeQueryDiv').html('<div class="alert alert-danger">' + data.Error + '</div>');
                            $('#qbTabs #previewTabLink').tab('show');
                            //alert(data.Error);
                            return false;
                        } else {
                            //alert('Query Saved Successfully!');
                            window.location.href = '/QueryBuilder/Index';
                        }
                        $('#saveForm').data('inProgress', false);
                    },
                    error: function (xhr) {
                        $('#executeQueryDiv').html('<div class="alert alert-danger">Error While Saving Query. Please try again after some time!</div>');
                        $('#qbTabs #previewTabLink').tab('show');
                    },
                    complete: function () {

                    }
                });

                return false;
            });

            $(document).on('click', '.updateDynamicVariableValues', function (e) {
                UpdateDynamicVariableValues();
            });

            // -----------------------------------------
            // fill the values for the dynamic variables
            // -----------------------------------------
            function UpdateDynamicVariableValues() {
                dynamicVariables = [];

                var canExecute = true;

                $(".dynamicVariables .dynVariableValue").each(function () {
                    var key = $(this).attr("name");
                    // get the field here and then set the value and then continue with the regular post back.
                    var field = _getFilterClauseFieldByKey(key);

                    if (field == null || field == undefined) {
                        alert("No matching field with the dynamic variable is found");
                        return false;
                    }

                    var value = jQuery.trim($(this).val());

                    if (value.length < 1) {
                        alert("Provide a valid value for " + $(this).attr("id"));
                        canExecute = false;
                        return false;
                    }

                    field.Value = value;

                    var dynamicVariable = {
                        'VariableId': guidGenerator(),
                        'Name': $(this).attr("id"),
                        'Value': value
                    };

                    dynamicVariables.push(dynamicVariable);

                });

                if (canExecute) {
                    _ExecuteQuery();
                    $('#previewDynamicvarDetails').modal({ show: false });
                    $('#previewDynamicvarDetails').modal('hide');
                }
                else {
                    return false;
                }
            }

            // -----------------------------------------------------------------
            // Get the filter clause field based on the key for dynamic variable
            // -----------------------------------------------------------------
            function _getFilterClauseFieldByKey(key) {
                if (_selectedFilterClauseFields == null || _selectedFilterClauseFields == undefined || _selectedFilterClauseFields.length < 1) {
                    return undefined;
                }

                for (var i = 0; i < _selectedFilterClauseFields.length; i++) {

                    if (_selectedFilterClauseFields[i].FieldIdentifier.toString() !== key) {
                        continue;
                    }

                    return _selectedFilterClauseFields[i];
                }
            }

            // --------------------------------------
            // show the dynamic variable in the popup
            // --------------------------------------
            function ManageDynamicVariables() {
                if (_selectedFilterClauseFields == null || _selectedFilterClauseFields == undefined || _selectedFilterClauseFields.length < 1) {
                    return true;
                }

                var dynVarMarkUp = "";

                for (var sfc = 0; sfc < _selectedFilterClauseFields.length; sfc++) {

                    var elementMarkUp = "";
                    var key = '';

                    if (_selectedFilterClauseFields[sfc].ValueType === "DynamicVariable") {
                        elementMarkUp = "<tr>";
                        /*if (_selectedFilterClauseFields[sfc].IsExpressionColumn) {
                        key = _selectedFilterClauseFields[sfc].DisplayName+'~'+_selectedFilterClauseFields[sfc].MappedFieldName;
                        }
                        else{
                        key = _selectedFilterClauseFields[sfc].EntityName+'~'+_selectedFilterClauseFields[sfc].MappedFieldName;
                        }*/
                        key = _selectedFilterClauseFields[sfc].FieldIdentifier;

                        // this key is used to find the field based on the key and the name of the dynamic variable.
                        elementMarkUp += "<td class='row4'>" + _selectedFilterClauseFields[sfc].ValueLabel + " <input type='hidden' class='key' value='" + key + "' /> </td>";

                        var value = _selectedFilterClauseFields[sfc].Value || '';

                        elementMarkUp += "<td class='row5'> <input type='text' id='" + _selectedFilterClauseFields[sfc].ValueLabel + "' class='dynVariableValue' name='" + key + "' value='" + value + "' /></td>";
                        elementMarkUp += "</tr>";
                    }
                    dynVarMarkUp += elementMarkUp;
                }

                if (dynVarMarkUp.length < 1) {
                    return true;
                }

                $(".dynamicVariables table tbody").html(dynVarMarkUp);

                $('#previewDynamicvarDetails .modal-body').html($(".dynamicVariables"));
                $('#previewDynamicvarDetails .modal-title').html("Manage Dynamic Variables");
                $('#previewDynamicvarDetails').modal('show');
            }

            $('#btnExecuteQuery1, #btnExecuteQuery').click(function () {
                if (ManageDynamicVariables()) {
                    _ExecuteQuery();
                }
            });

            // ------------------
            // Executes the Query
            // ------------------
            function _ExecuteQuery() {
                var queryName = $('#queryName').val();
                var queryId = $('#queryId').val();
                var queryDesc = $('#queryDescription').val();
                var renderMode = $('#renderMode').val();
                var entityId = $(o.entitiesDropDownListId).val();

                if (entityId == '-1') {
                    alert('Please choose a enity & form a valid query for execution!');
                    return false;
                }

                if (_selectedSelectClauseFields.length == 0) {
                    alert('Please atleast select a field in select clause to execute the query.');
                    return false;
                }

                for (i = 0; i < _selectedSelectClauseFields.length; ++i) {
                    if (_selectedSelectClauseFields[i].DisplayName == '') {
                        alert('Display name cannot be empty!');
                        return false;
                    }

                    for (j = i + 1; j < _selectedSelectClauseFields.length; ++j) {
                        if (_selectedSelectClauseFields[i].DisplayName == _selectedSelectClauseFields[j].DisplayName) {
                            alert('Display name should not contain duplicates "' + _selectedSelectClauseFields[i].DisplayName + '"');
                            return false;
                        }
                    }
                }

                if (openbracketcount != closebracketcount) {
                    alert('Please note that the filter clause has invalid parenthesis. Consider revising');
                }

                // check the dynamic variable and get the values from the popup
                // Not needed as the state maintenance is difficult here
                //ManageDynamicVariables();

                if ($(this).data('inProgress')) {
                    return false;
                }

                $('#btnExecuteQuery1, #btnExecuteQuery').data('inProgress', true);
                setTimeout(function () { $('#btnExecuteQuery1, #btnExecuteQuery').data('inProgress', false); }, 10000);

                var queryInfo = FormQueryInfoModel(entityId);

                var isglobalQuery = false;

                if ($(".globalQuery").length > 0) {
                    isglobalQuery = $(".globalQuery").is(":checked");
                }

                lastestQueryDetails = {
                    'queryId': queryId,
                    'queryName': queryName,
                    'queryDescription': queryDesc,
                    'renderMode': renderMode,
                    'queryInfo': queryInfo,
                    'isglobal': isglobalQuery,
                    'queryType': $("#QueryType").val()
                };

                window.currentQueryInfo = lastestQueryDetails;

                $.ajax({
                    url: o.executeQueryURL,
                    data: JSON.stringify(lastestQueryDetails),
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'html',
                    success: function (data) {
                        $('#executeQueryDiv').html(data);

                        $('#btnExecuteQuery1, #btnExecuteQuery').data('inProgress', false);

                        $('#qbTabs #previewTabLink').tab('show');
                    },
                    error: function (xhr) {
                        $('#executeQueryDiv').html('<div class="alert alert-danger">Error While Executing Query. Please try again after some time!</div>');
                        $('#qbTabs #previewTabLink').tab('show');
                    },
                    complete: function () {

                    }
                });
                return false;
            }

            $(o.selectClauseFields).change(function () {
                var selectFields = $(this).val();
                var html = '<option value="-1">--Select Aggregate Function--</option>';

                if (selectFields === null) {
                    return;
                }

                if (selectFields !== undefined && selectFields.length == 1) {
                    var entityId = selectFields[0].split('.')[0];

                    // check data type and load valid aggegrate function
                    for (var i = 0; i < _selectClauseFields.length; ++i) {
                        if (_selectClauseFields[i].IsExtensionField) {
                            continue;
                        }

                        var mappedId = selectFields[0].split('.')[1];

                        if (_selectClauseFields[i].IsExpressionColumn) {
                            mappedId = selectFields[0];
                            mappedId = mappedId.replace(entityId + '.', '');
                        }

                        var id = _selectClauseFields[i].MappedFieldName;

                        if (id == mappedId) {
                            var type = _selectClauseFields[i].Type.toLowerCase();
                            if (type == 'int' || type == 'float'
                                    || type == 'double' || type == 'smallint'
                                    || type == 'bigint' || type == 'short') {

                                html += '<option value="COUNT">Count</option>';
                                html += '<option value="SUM">Sum</option>';
                                html += '<option value="AVG">Average</option>';
                                html += '<option value="MAX">Maximum</option>';
                                html += '<option value="MIN">Minimum</option>';
                                html += '<option value="VAR">Variance</option>';
                                html += '<option value="STDEV">Standard Devation</option>';
                            } else {
                                html += '<option value="COUNT">Count</option>';
                            }
                            break;
                        }
                    }
                } else {
                    var isextension = false;
                    for (var i = 0; i < selectFields.length; ++i) {
                        var entityId = selectFields[i].split('.')[0];
                        // check for extension field
                        for (var j = 0; j < _selectClauseFields.length; ++j) {
                            var id = entityId + '.' + _selectClauseFields[j].MappedFieldName;
                            if (id == selectFields[i] && _selectClauseFields[j].IsExtensionField) {
                                isextension = true;
                                break;
                            }
                        }
                        if (isextension) break;
                    }

                    if (!isextension) {
                        // show only count aggegrate function
                        html += '<option value="COUNT">Count</option>';
                    }
                }

                $('#AggregateFunction').html(html);
                $('#AggregateFunction').trigger('change');
                $('#AggregateFunction').trigger('liszt:updated');
            });

            $(o.modulesDropDownListId).change(function () {
                var moduleId = $(this).val();

                if (moduleId != '-1') {
                    // hide the related entities div
                    $(options.relatedEntitiesDisplayId).hide();
                    ResetCache(null);

                    // load the entities
                    $.post(o.entityLoadURL, { moduleId: moduleId }, function (data) {
                        var html = '';
                        if (data.Error) {
                            html = '<option value="-1">--' + data.Error + '--</option>';
                        } else {
                            html = '<option value="-1">--Select a entity--</option>';
                            for (i = 0; i < data.length; i++) {
                                if (o.formMode == 'Edit' && data[i] == $('#primaryEntityId').val()) {
                                    html += '<option selected>';
                                } else {
                                    html += '<option>';
                                }

                                html += data[i] + '</option>';
                            }
                        }
                        $(o.entitiesDropDownListId).html(html);

                        $(o.entitiesDropDownListId).trigger('change');
                        $(o.entitiesDropDownListId).trigger('liszt:updated');
                    }, 'json');
                } else {
                    var html = '<option value="-1">--Select a entity--</option>';
                    $(o.entitiesDropDownListId).html(html);
                    $(o.entitiesDropDownListId).trigger('change');
                    $(o.entitiesDropDownListId).trigger('liszt:updated');
                }
            });

            $(o.entitiesDropDownListId).change(function () {
                // load the related entities relationships
                // load primary entity fields into available fields, order by fields and filter fields
                var entityId = $(this).val();
                var moduleId = $(o.modulesDropDownListId).val();
                //if (o.formMode == 'Insert') {
                ResetCache(null);
                //}
                if (entityId != '-1') {
                    loadRelatedEntities(moduleId, entityId);
                    loadFields(entityId, entityId);
                    // now load the primary entity field data as a selected field in the select fields
                    $(".expressionControls").fadeIn("slow");
                    if ($("#primaryEntityAlias").length > 0) {
                        $("#primaryEntityAlias").val(entityId);
                    }
                } else {
                    $(options.relatedEntitiesDisplayId).hide();
                    $("#primaryEntityAlias").val('');
                }
            });

            /*Add select field*/
            $(o.btnRightSelectClause).click(function () {
                var items = $(o.selectClauseFields).val();
                if (items == '' || items == null) return false;
                _populate(items, _selectClauseFields, SelectedSelectClause);
                onBaseFieldClick();
                return false;
            });

            /*Remove select field*/
            $(document).on('click', '#selectedFieldsTable a.removeField', function (e) {
                e.preventDefault();
                var fieldId = $(this).data('fieldid');
                if (fieldId && fieldId.length > 0) {
                    _populate([fieldId], _selectedSelectClauseFields, SelectClause);
                }
                return false;
            });

            /*Field display name*/
            $(document).on('blur', '#selectedFieldsTable input[type=text]', function (e) {
                e.preventDefault();
                var fieldId = $(this).attr('name');
                var displayName = $(this).val();

                if (!displayName || displayName == '') {
                    alert('Enter a valid display name!');
                    $(this).focus();
                    return;
                }

                if (fieldId && fieldId.length > 0) {
                    for (i = 0; i < _selectedSelectClauseFields.length; i++) {
                        var id = _selectedSelectClauseFields[i].EntityName + '.' + _selectedSelectClauseFields[i].MappedFieldName;

                        if (id == fieldId) {
                            _selectedSelectClauseFields[i].DisplayName = displayName;
                            return;
                        }
                    }
                }
                return false;
            });

            /*Select field visibility*/
            $(document).on('change', '#selectedFieldsTable input[type=checkbox].fieldVisibility', function (e) {
                e.preventDefault();
                var fieldId = $(this).data('fieldid');
                var isvisible = $(this).is(':checked');

                if (fieldId && fieldId.length > 0) {
                    for (i = 0; i < _selectedSelectClauseFields.length; i++) {
                        var id = _selectedSelectClauseFields[i].EntityName + '.' + _selectedSelectClauseFields[i].MappedFieldName;

                        if (id == fieldId) {
                            _selectedSelectClauseFields[i].IsVisible = isvisible;
                            return;
                        }
                    }
                }
                return false;
            });

            /*Group by fields*/
            $(document).on('change', '#groupClause input[type=checkbox].groupByField', function (e) {
                e.preventDefault();
                var fieldId = $(this).data('fieldid');
                var checked = $(this).is(':checked');
                if (fieldId && fieldId.length > 0) {
                    if (checked) {
                        _populate([fieldId], _groupByClauseFields, SelectedGroupByClause);
                    } else {
                        _populate([fieldId], _selectedGroupByClauseFields, GroupByClause);
                    }
                }
                return false;
            });

            /*Order by fields*/
            $(document).on('change', '#selectedFieldsTable select.direction', function (e) {
                e.preventDefault();
                var fieldId = $(this).data('fieldid');
                var direction = $(this).val();

                if (fieldId && fieldId.length > 0) {
                    if (direction != "NONE") {
                        _populate([fieldId], _orderByClauseFields, SelectedOrderByClause);

                        for (i = 0; i < _selectedOrderByClauseFields.length; ++i) {
                            var id = _selectedOrderByClauseFields[i].EntityName + '.' + _selectedOrderByClauseFields[i].MappedFieldName;
                            if (fieldId == id) {
                                _selectedOrderByClauseFields[i].SortDirection = direction;
                                break;
                            }
                        }
                    } else {
                        _populate([fieldId], _selectedOrderByClauseFields, OrderByClause);
                    }
                }
                return false;
            });

            $('#conditionField').change(function () {
                var id = $(this).val();

                var field = GetFilterClauseField(id);

                if (field !== null && field.IsExpressionColumn === true) {
                    $(".autofillcontrols").fadeOut("slow");
                }
                else {
                    $(".autofillcontrols").fadeIn("slow");
                }

                if (field != null) {
                    var operatorHtml = '';
                    switch (field.Type.toLowerCase()) {
                        case 'bit':
                        case 'bool':
                        case 'boolean':
                            operatorHtml = '<option value="Equals">Equals To</option>';
                            if (field.IsNullable) {
                                operatorHtml += '<option value="Is">Is</option>';
                            }
                            break;
                        case 'int':
                        case 'smallint':
                        case 'decimal':
                        case 'variant':
                            operatorHtml = GetAllFilterOperatorHtml();
                            operatorHtml += GetAdditionalFilterOperatorHtml();
                            break;
                        case 'float':
                        case 'double':
                            operatorHtml = GetAllFilterOperatorHtml();
                            operatorHtml += GetAdditionalFilterOperatorHtml();
                            break;
                        case 'uniqueidentifier':
                            operatorHtml = GetAllFilterOperatorHtml();
                            break;
                        case 'varchar':
                        case 'nvarchar':
                        case 'char':
                            operatorHtml = GetAllFilterOperatorHtml();
                            operatorHtml += GetAdditionalFilterOperatorHtml();
                            break;
                        case 'datetime':
                        case 'time':
                        case 'date':
                        case 'timestamp':
                            operatorHtml = GetAllFilterOperatorHtml();
                            operatorHtml += GetAdditionalFilterOperatorHtml();
                            break;
                    }
                    $("#GlobalVariable").prop("checked", false);
                    $("#DynamicVariable").prop("checked", false);
                    $("#ColumnVariable").prop("checked", false);
                    $('#conditionOperator').html(operatorHtml);
                    $('#conditionOperator').trigger('change');
                    $('#conditionOperator').trigger('liszt:updated');
                }
            });

            $('#conditionOperator').change(function () {
                var op = $(this).val();
                var id = $('#conditionField').val();
                var field = GetFilterClauseField(id);

                if (field == null) return;
                $("#AutoPopulateFields").prop("checked", false);
                $(".autofillcontrols").fadeIn("slow");
                $("#GlobalVariable").prop("checked", false);
                $(".dynVariableControls").fadeIn("slow");
                $(".columnValues").fadeIn("slow");
                $("#DynamicVariable").prop("checked", false);
                $("#ColumnVariable").prop("checked", false);
                switch (op) {
                    case 'Equals':
                    case 'NotEquals':
                    case 'GreaterThan':
                    case 'LesserThan':
                    case 'GreaterThanAndEqualTo':
                    case 'LesserThanAndEqualTo':
                        $(".gvControls").fadeIn("slow");
                        break;
                    case 'Like':
                        $(".gvControls").fadeOut("slow");
                        $(".columnValues").fadeOut("slow");
                        break;
                    case 'In':
                        $(".dynVariableControls").fadeOut("slow");
                        $(".columnValues").fadeOut("slow");
                        $(".gvControls").fadeOut("slow");
                        break;
                    case 'Is':
                    case 'Between':
                        $(".dynVariableControls").fadeOut("slow");
                        $(".columnValues").fadeOut("slow");
                        $(".gvControls").fadeOut("slow");
                        $(".autofillcontrols").fadeOut("slow");
                        break;
                    default:
                        $(".gvControls").fadeOut("slow");
                        break;


                }

                var valueHtml = '<input type="text" name="conditionValue" id="conditionValue" style="width: 160px;margin-top:5px" />';
                switch (op) {
                    case 'Equals':
                    case 'NotEquals':
                    case 'Like':
                    case 'GreaterThan':
                    case 'LesserThan':
                    case 'GreaterThanAndEqualTo':
                    case 'LesserThanAndEqualTo':
                        if (field.Type.toLowerCase() == 'bit') {
                            valueHtml = '<select name="conditionValue" id="conditionValue">';
                            valueHtml += '<option value="True">True</option>';
                            valueHtml += '<option value="False">False</option>';
                            valueHtml += '</select>';
                            valueHtml += "<script>$('#conditionValueDiv select').select2();</script>";
                        } else {
                            valueHtml = '<input type="text" name="conditionValue" id="conditionValue" style="width: 160px;margin-top:5px" />';
                        }
                        break;
                    case 'Is':
                        valueHtml = '<select name="conditionValue" id="conditionValue">';
                        valueHtml += '<option value="Null">NULL</option>';
                        valueHtml += '<option value="Not Null">NOT NULL</option>';
                        valueHtml += '</select>';
                        valueHtml += "<script>$('#conditionValueDiv select').select2();</script>";
                        break;
                    case 'In':
                        break;
                    case 'Between':
                        valueHtml = '<div>FROM: <input type="text" name="conditionValue" id="conditionValue" style="width: 160px;margin-top:5px" /></div>';
                        valueHtml += '<div> TO: <input type="text" name="conditionValue" id="conditionValue1" style="width: 160px;margin-top:5px" /></div>';
                        break;
                }

                switch (field.Type.toLowerCase()) {
                    case 'datetime':
                    case 'time':
                    case 'date':
                    case 'timestamp':
                        valueHtml += "<script>$('#conditionValue').datepicker();";
                        if (op == 'Between') {
                            valueHtml += "$('#conditionValue1').datepicker();";
                        }
                        valueHtml += '</script>';
                        break;
                }

                $('#conditionValueDiv').html(valueHtml);
                $("#AutoPopulateFields").prop("checked", false);
                $('#AutoPopulateFields').trigger('change');
            });

            function GetAllFilterOperatorHtml() {
                var html = '<option value="Equals">Equals To</option>';
                html += '<option value="NotEquals">Not Equals To</option>';
                html += '<option value="Is">Is</option>';
                html += '<option value="In">In</option>';
                html += '<option value="Like" >Like</option>';
                return html;
            }

            function GetAdditionalFilterOperatorHtml() {
                var html = '<option value="GreaterThan">Greater Than</option>';
                html += '<option value="LesserThan">Lesser Than</option>';
                html += '<option value="GreaterThanAndEqualTo">Greater Than EqualTo</option>';
                html += '<option value="LesserThanAndEqualTo">Lesser Than EqualTo</option>';
                html += '<option value="Between">Between</option>';
                return html;
            }

            /* Newly added for Bracket Support - STARTS */

            $("#openparenthesis").click(function () {

                var fs = new FilterString(filtercount, openbracket, -1);
                var tempfs = whereconditions[whereconditions.length - 1];

                try {
                    if (tempfs) {
                        if (tempfs.value === openbracket || (tempfs.value === conjunctionAND || tempfs.value === conjunctionOR)) {
                            whereconditions.push(fs);
                            openbracketcount = openbracketcount + 1;

                            filtercount = filtercount + 1;
                        }
                        else {
                            throw new SyntaxError("Consider choosing a conjunction operator or a field prior to this parenthesis.");
                        }
                    } else {
                        whereconditions.push(fs);
                        filtercount = filtercount + 1;
                        openbracketcount = openbracketcount + 1;
                    }
                } catch (e) {
                    alert(e);
                    return false;
                }

                // generate the div tag for display
                var cond = $(".fConditionRow");

                var content = getFilterMarkup(fs.conditionId, fs.value);

                if (cond.length < 1) {
                    cond = '<div class="fConditionRow" style="display:block">';
                    cond += content
                }
                else {
                    cond.append(content);
                }

                $('#filterClauseDiv').html(cond);
                $('#filterClauseDiv').css("display", "block");

                $("#tree").treeview({
                    collapsed: false,
                    animated: "medium",
                    //control: "#sidetreecontrol",
                    persist: "location"
                });
            });

            $("#closeparenthesis").click(function () {

                try {
                    if (closebracketcount + 1 > openbracketcount) {
                        throw new SyntaxError("There is no matching open parenthesis. Consider revising the use of parenthesis");
                    }

                    var tempfs = whereconditions[whereconditions.length - 1];

                    if (tempfs.value === openbracket || tempfs.value === conjunctionAND || tempfs.value === conjunctionOR) {
                        throw new SyntaxError("There is no matching field to close this parenthesis. Consider revising the use of parenthesis");
                    }

                    var fs = new FilterString(filtercount, closebracket, -1);
                    whereconditions.push(fs);

                    var cond = $(".fConditionRow");

                    var content = getFilterMarkup(fs.conditionId, fs.value);

                    cond.append(content);

                    $('#filterClauseDiv').html(cond);
                    $('#filterClauseDiv').css("display", "block");

                    filtercount = filtercount + 1;
                    closebracketcount++;
                } catch (e) {
                    alert(e);
                }
                $("#tree").treeview({
                    collapsed: false,
                    animated: "medium",
                    //control: "#sidetreecontrol",
                    persist: "location"
                });
            });

            $("#conjunctionand").click(function () {
                var tempfs = whereconditions[whereconditions.length - 1];
                var cond = $(".fConditionRow");

                try {
                    if (whereconditions.length <= 0) {
                        throw new SyntaxError("The Conjunction can not be added after the open parenthesis or added at the start of the filter condition");
                    }
                    if (tempfs) {
                        if (tempfs.value === openbracket || tempfs.value === conjunctionAND || tempfs.value === conjunctionOR) {
                            throw new SyntaxError("The Conjunction can not be added after the open parenthesis or added at the start of the filter condition");
                        } else {
                            if (cond.length < 1) {
                                throw new SyntaxError("Please add a field prior to the this conjunction operator");
                            }

                            var fs = new FilterString(filtercount, conjunctionAND, -1);

                            whereconditions.push(fs);
                            filtercount = filtercount + 1;
                        }
                    } else {
                        whereconditions.push(new FilterString(filtercount, conjunctionAND, -1));
                        filtercount = filtercount + 1;
                    }
                } catch (e) {
                    alert(e);
                    return false;
                }
                var content = getFilterMarkup(fs.conditionId, fs.value);

                cond.append(content);

                $('#filterClauseDiv').html(cond);
                $('#filterClauseDiv').css("display", "block");

                $("#tree").treeview({
                    collapsed: false,
                    animated: "medium",
                    //control: "#sidetreecontrol",
                    persist: "location"
                });
            });

            $("#conjunctionor").click(function () {
                var tempfs = whereconditions[whereconditions.length - 1];
                try {
                    if (whereconditions.length <= 0) {
                        throw new SyntaxError("The Conjunction can not be added after the open parenthesis or added at the start of the filter condition");
                    }

                    var fs = new FilterString(filtercount, conjunctionOR, -1);

                    if (tempfs) {
                        if (tempfs.value === openbracket || tempfs.value === conjunctionAND || tempfs.value === conjunctionOR) {
                            throw new SyntaxError("The Conjunction can not be added after the open parenthesis or added at the start of the filter condition");
                        }
                        else {
                            whereconditions.push(fs);
                            filtercount = filtercount + 1;
                        }
                    }
                    else {
                        whereconditions.push(fs);
                        filtercount = filtercount + 1;
                    }
                } catch (e) {
                    alert(e);
                    return false;
                }
                var cond = $(".fConditionRow");

                var content = getFilterMarkup(fs.conditionId, fs.value);

                cond.append(content);

                $('#filterClauseDiv').html(cond);
                $('#filterClauseDiv').css("display", "block");

                $("#tree").treeview({
                    collapsed: false,
                    animated: "medium",
                    //control: "#sidetreecontrol",
                    persist: "location"
                });
            });

            /* New additions ENDS */

            $('#btnAddFilterCondidtion').click(function () {
                var conditionValue = '';
                var valueType;

                if ($('#conditionValue').length < 1 && $("#globalVariablesList").length > 0) {
                    conditionValue = $("#globalVariablesList :selected").val();
                    valueType = 'GlobalVariable';
                }
                else if ($("#DynamicVariable").is(":checked")) {
                    valueType = 'DynamicVariable';
                    var value = $('#conditionValue').val();

                    if (value == '') {
                        alert("Please enter the dynamic variable name");
                        return false;
                    }
                    else if ($('#conditionValue').val().indexOf(' ') > -1) {
                        alert("Name should not contains space.");
                        return false;
                    }
                }
                else if ($("#ColumnVariable").is(":checked")) {
                    valueType = "Column";

                    if ($(".filterColumns :selected").val() === "-1") {
                        alert("Choose a valid column");
                        return false;
                    }
                    conditionValue = $(".filterColumns :selected").val();
                }
                else {
                    conditionValue = $('#conditionValue').val();
                    valueType = 'Static';
                }

                var conditionFieldId = $('#conditionField').val();

                if (conditionFieldId == '-1') {
                    alert('Please choose a valid field');
                    return false;
                }

                var field = GetFilterClauseField(conditionFieldId);

                if (field == null) {
                    alert('Should Not Happen');
                    return false;
                }

                field.Conjuction = $('#conjuctionOperator').val();
                field.OperatorDisplayValue = $('#conditionOperator option:selected').text();
                field.Operator = $('#conditionOperator').val();
                field.ValueType = valueType;

                /* filter condition bracket code - STARTS */
                field.FieldIdentifier = filtercount;
                /* filter condition bracket code - ENDS */

                $('#filterClauseDiv').show();

                switch (field.Type.toLowerCase()) {
                    default:
                    case 'varchar':
                    case 'nvarchar':
                    case 'char':
                    case 'datetime':
                    case 'time':
                    case 'date':
                    case 'timestamp':
                    case 'bit':
                        {
                            if (field.Operator == "Is" || field.Operator == "In") {
                                conditionValue = conditionValue.trim();
                            }
                            else {
                                conditionValue = "'" + conditionValue + "'";
                            }
                            break;
                        }
                    case 'int':
                    case 'decimal':
                    case 'float':
                    case 'double':
                    case 'smallint':
                        conditionValue = conditionValue;
                        break;
                }

                if (field.Operator == 'Between') {
                    field.Value = conditionValue + ",'" + $('#conditionValue1').val() + "'";
                }
                else if (field.Operator == 'In') {
                    conditionValue = conditionValue.replace(/,\s+/ig, ',');
                    if (conditionValue[conditionValue.length - 1] == ',') {
                        conditionValue = conditionValue.substring(0, conditionValue.length - 1);
                    }
                    field.Value = "'" + conditionValue.split(',').join("','") + "'";
                }
                else {
                    field.Value = conditionValue;
                }


                /* filter condition bracket code - STARTS */
                try {
                    var tempfs = whereconditions[whereconditions.length - 1];

                    if (tempfs) {
                        if ((tempfs.value === conjunctionAND || tempfs.value === conjunctionOR) || tempfs.value === openbracket) {
                            whereconditions.push(new FilterString(filtercount, filtercount, field.FieldIdentifier));
                            filtercount = filtercount + 1;
                        }
                        else {
                            throw new SyntaxError("No valid condition or an opening parenthesis is found");
                        }
                    }
                    else {
                        whereconditions.push(new FilterString(filtercount, filtercount, field.FieldIdentifier));
                        filtercount = filtercount + 1;
                    }
                } catch (e) {
                    alert(e);
                    return false;
                }

                /* filter condition bracket code - ENDS */

                switch (valueType) {
                    case "DynamicVariable":
                        field.ValueLabel = "@" + $("#conditionValue").val();
                        field.Value = '';
                        break;
                    case "GlobalVariable":
                        field.ValueLabel = field.Value;
                        field.Value = '';
                        break;
                    case "Column":
                        var selectedField = $(".filterColumns :selected").val();
                        field.Value = selectedField.replace("ExprCol.", '');
                        break;
                }

                _selectedFilterClauseFields.push(field);

                PopulateFilterClauseDiv();

                //$("#conditionField [value='-1']").attr("selected", "selected");

                if ($("#ColumnVariable").is(":checked")) {
                    $("#ColumnVariable").attr("checked", false);
                }
                $("#conditionValueDiv").html('<input type="text" name="conditionValue" id="conditionValue" style="width: 160px; margin-top:12px">');

                return false;
            });

            $("#ColumnVariable").click(function () {
                if (!$(this).is(':checked')) {
                    loadConditionValue();
                    return;
                }

                var filterColumnSelect = $("<select />").attr({ "class": "filterColumns" });

                if ($("#conditionOperator :selected").val() === "Like" || $("#conditionOperator :selected").val() === "In" || $("#conditionOperator :selected").val() === "Is") {
                    for (var i = 0; i < _filterClauseFields.length; i++) {
                        if (_filterClauseFields[i].IsExpressionColumn) {
                            continue;
                        }

                        var optionValue = _filterClauseFields[i].EntityName + "." + _filterClauseFields[i].MappedFieldName;
                        var optionText = _filterClauseFields[i].EntityName + "." + _filterClauseFields[i].DisplayName;
                        var columnOption = $("<option />").attr({ "value": optionValue }).text(optionText);

                        filterColumnSelect.append(columnOption);
                        $("#conditionValueDiv").html(filterColumnSelect);
                    }
                }
                else {
                    var conditionFields = $("<select class='filterColumns'>");
                    conditionFields.append($("#conditionField").clone().html())
                    $("#conditionValueDiv").html(conditionFields);
                }

                $("#AutoPopulateFields").prop("checked", false);
                $("#GlobalVariable").prop("checked", false);
                $("#DynamicVariable").prop("checked", false);

                var id = $("#conditionField").val();
                var field = GetFilterClauseField(id);

                var basefieldType = field.Type;
                // remove the unwanted type of columns from the options
                $(".filterColumns option").each(function () {
                    id = $(this).val();
                    field = GetFilterClauseField(id);

                    if ((field !== null && field !== undefined) && field.Type.toLowerCase() !== basefieldType.toLowerCase()) {
                        $(this).remove();
                    }
                });
                $("#conditionValueDiv").append("<script>$('#conditionValueDiv select').attr('style', 'width:75%;');$('#conditionValueDiv select').select2(); </script>")
            });

            $('#AutoPopulateFields').click(function () {
                if ($(this).is(':checked')) {

                    $("#GlobalVariable").prop("checked", false);
                    $("#DynamicVariable").prop("checked", false);
                    $("#ColumnVariable").prop("checked", false);
                    $("#conditionValueDiv").html('<input type="text" name="conditionValue" id="conditionValue" style="width: 160px; margin-top:12px">');

                    var isMultiple = $('#conditionOperator').val() == 'In' ? true : false;

                    $('#conditionValue').autocomplete('/QueryBuilder/GetFieldValue', {
                        mustMatch: true,
                        delay: 800,
                        minChars: 0,
                        multiple: isMultiple,
                        inputFocus: false,
                        clickFire: false,
                        contentType: 'application/json',
                        dataType: 'json',
                        extraParams: {
                            'fieldInfo': function () {
                                var fieldId = $('#conditionField').val();
                                if (fieldId == '-1') return null;
                                var fieldInfo = GetFilterClauseField(fieldId);
                                return fieldInfo;
                            }
                        },
                        parse: function (data) {
                            var parsed = [];
                            var rows = data || [];
                            for (var i = 0; i < rows.length; i++) {
                                var row = $.trim(rows[i]);
                                if (row) {
                                    parsed[parsed.length] = {
                                        data: row,
                                        value: row,
                                        result: row
                                    };
                                }
                            }
                            return parsed;
                        },
                        formatItem: function (item) { return item; }
                    }).result(function (event, item) {
                        if (item == 'No Values Found!') {
                            $('#conditionValue').val('');
                        }
                    });
                } else {
                    $("#conditionValue").unautocomplete();
                    loadConditionValue();
                }
            });

            $(o.modulesDropDownListId).trigger('change');
        }
    }
};