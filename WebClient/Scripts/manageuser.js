﻿function SubmitUserDetailsForm() {
    $('#userDetailsFrom').submit();
}

function addPopup() {   
    $("#userRequestModel").modal('show');
}

function ValidateUserEmail() {
    var email = $("#UserDetails_MembershipDetails_EmailId").val();
    var userId = $("#UserDetails_Identifier").val();

    $.ajax({
        url: validateUrl,
        type: "POST",
        data: {
            'emailId': email,
            'userId': userId
        },
        success: function (data) {
            if (data === null || data.length < 1) {
                $(".error").html(linkErrorMsg);              
                return false;
            }

            $(".addUserPopup").html(data);

            if ($(".addUserPopup #IsErrorView").val().toLowerCase() === "true") {               
                $("#userRequestModel").modal('hide');
                if ($(".userMessage .alert-danger").length > 0) {
                    $(".userMessage .alert-danger").html(data);
                    return false;
                }
                else {
                    var errorMarkUp = $("<div />").attr("class", "alert alert-danger");
                    errorMarkUp.html(data);
                    $(".userMessage").append(errorMarkUp);
                    if ($(".info").is(":visible")) {
                        $(".info").hide();
                    }
                    return false;
                }
            };          

            $("#userRequestModel").modal('show');
        },
        error: function (xhr, err) {
            alert(err);
            $(".addUserPopup").dialog("close");
            alert(linkException);
        }
    });
}

function linkUser() {

    var comments = $("#Comments").val();
    var userId = $("#RequestUserId").val();
    var linkedId = $("#RequestlinkId").val();
    var status = $("#RequestStatus").val();
    var requestData = undefined;
    var assignedRoles = [];

    var assignedCount = 0;

    $("._userroles").each(function () {
        if ($(this).is(":checked")) {
            assignedRoles.push($(this).attr("id"));
            assignedCount++;
        };
    });

    if (assignedCount === 0) {
        alert(minRole);
        return false;
    }

    if (comments == null || comments.toString() == "") {
        alert(commentMandatoryMessage);
        return false;
    }

    requestData = {
        'comments': comments,
        'userId': userId,
        'linkedId': linkedId,
        'status': status,
        'roles': assignedRoles
    };

    ProcessRequest(requestData);
}

function ProcessRequest(requestData) {

    $.ajax({
        url: requestLink,
        contentType: "application/json; charset:utf-8",
        data: JSON.stringify(requestData),
        type: "POST",
        success: function (data) {           
            $("#userRequestModel").modal('hide');
            if (data === undefined) {
                alert("Exception Occured while handling the request.");
                return false;
            }

            window.location = otherTenantUserListLink;
        }
    });
}
