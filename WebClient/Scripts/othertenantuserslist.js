﻿var filterTable = null, filterTable1 = null;

$(document).ready(function () {
    jQDataTable();
    jQDataTableAdmin();
    $('#tenantTableSearchText').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            DoSearch();
            e.preventDefault();
        }
    });
    $('#tenantUserTableSearchText').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            DoUserSearch();
            e.preventDefault();
        }
    });
});

function jQDataTable() {
    filterTable = $('table#dataListOtherTenantUsers').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "aaSorting": [[3, "desc"]],
        "bInfo": true,
        "bAutoWidth": false,
        "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2]}]
    });
}

function jQDataTableAdmin() {
    filterTable1 = $('table#dataListRequestByAdmin').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "aaSorting": [[3, "desc"]],
        "bInfo": true,
        "bAutoWidth": false
    });
}

function DoUserSearch() {
    var searchText = $('#tenantUserTableSearchText').val();
    if (searchText == 'Search') {
        searchText = '';
    }
    filterTable.fnFilter(searchText);
    $("div.success,div.error").remove();
    return false;
}

function DoSearch() {
    var searchText = $('#tenantTableSearchText').val();
    if (searchText == 'Search') {
        searchText = '';
    }
    filterTable1.fnFilter(searchText);
    $("div.success,div.error").remove();
    return false;
}

function GetTenantRoles(linkedTenantUsersId, userId, requestedBy) {
    $.ajax({
        url: "/Roles/GetActiveTenantRoles",
        type: "POST",
        success: function (data) {

            var applyButtonMarkUp = "<td colspan='2'><div class='button' style='font-style: normal;'> <a  onclick='MapUserToRoles()'><span>Apply</span></a></div>";

            var contentDiv = "<div class='tenantRoles'> <div class='heading_container'><h1>Map the following Roles to the User</h1></div>"; //<div style='float:right'>" + applyButtonMarkUp + "</div>";

            contentDiv += "<input type='hidden' id='linkedTenantUsersId' value='" + linkedTenantUsersId + "' />";
            contentDiv += "<input type='hidden' id='userId' value='" + userId + "' />";
            contentDiv += "<input type='hidden' id='requestedBy' value='" + requestedBy + "' />";

            var table = "<table class='celloTable details_container_table'><thead><tr> <th>Role</th><th>Status</th> </tr></thead><tbody>";

            for (var i = 0; i < data.length; i++) {
                var currentRow = "<tr><td>" + data[i].RoleName + "</td>";
                currentRow += "<td><input class='_userroles' type='checkBox' name='" + data[i].RoleName + "' id='" + data[i].RoleId + "' /></td>";
                currentRow += "</tr>";
                table += currentRow;
            }

            table += "</tbody> <tfoot> <tr> </td>" + applyButtonMarkUp + "</tr></tfoot> </table>";

            contentDiv += table;

            if ($("#OtherTenantUser .tenantRoles").length < 1) {
                $("#OtherTenantUser").append(contentDiv);
            } else {
                $("#OtherTenantUser .tenantRoles").remove();
            }


            $(".tenantRoles").dialog({
                height: 500,
                width: 500,
                title: dialogtitle,
                modal: "true"
            });

            $(".tenantRoles").dialog("open");
        },
        error:function (xhr, ajaxOptions, thrownError){
            ShowError(approvalError);
        }  
    });
}

function MapUserToRoles() {
    var assignedRoles = [];

    var assignedCount = 0;

    $("._userroles").each(function () {
        if ($(this).is(":checked")) {
            assignedRoles.push($(this).attr("id"));
            assignedCount++;
        };
    });

    if (assignedCount === 0) {
        alert("Please select atleast one role to the user");
        return false;
    }

    var linkedTenantUsersId = $("#linkedTenantUsersId").val();
    var userId = $("#userId").val();
    var requestedBy = $("#requestedBy").val();

    ProcessRequest(linkedTenantUsersId, userId, requestedBy, assignedRoles);
}

function ProcessRequest(linkedTenantUsersId, userId, requestedBy, assignedRoles) {

    status = approvedStatusMessage;
    var requestData = {
        'linkedTenantUsersId': linkedTenantUsersId,
        'status': status,
        'userId': userId,
        'requestedBy': requestedBy,
        'roles': assignedRoles
    };

    $.ajax({
        url: respondUserRequestUrl,
        contentType: "application/json; charset:utf-8",
        data: JSON.stringify(requestData),
        type: "POST",
        success: function (data) {
            $(".tenantRoles").dialog("close");
            loadLinkedTenantDetails(data);
        }
    });
}

function approveRequest(linkedTenantUsersId, userId, requestedBy) {
    GetTenantRoles(linkedTenantUsersId, userId, requestedBy);
    return false;
}

function rejectRequest(linkedTenantUsersId, userId, requestedBy) {
    if (confirm(rejectMessage)) {
        status = rejectedStatus;
        $.ajax({
            url: respondUserRequestUrl,
            data:
            {
                'linkedTenantUsersId': linkedTenantUsersId,
                'status': status,
                'userId': userId,
                'requestedBy': requestedBy
            },
            type: "POST",
            success: function (data) {
                loadLinkedTenantDetails(data);
            }
        });
    }
}

function loadLinkedTenantDetails(data) {
    $("#OtherTenantUser").html(data);
    jQDataTable();
}

function loadApproveUserRequest(data) {
    window.location = window.location;
    $("#RequestedByTeanat").html(data);
    jQDataTable();
}