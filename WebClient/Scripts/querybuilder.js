﻿var currentQueryInfo = null;
var dynamicVariables = [];
var currentMode = "View";

var qb = {
    _export: function (type, queryId) {
        $('#exportType').val(type);

        currentMode = "Export";

        executeQuery(queryId, '');
    }
};

function addQuery() {
    var chosenQueryType = 1;

    if ($("#DataScopeQuery").is(":checked")) {
        chosenQueryType = 2;
    }

    $("#addQueryForm").append($("<input />").attr({ "type": "hidden", "name": "QueryType" }).val(chosenQueryType));
    $("#addQueryForm").submit();
}

var localQueryTable = null, globalQueryTable = null;

function DoGlobalQuerySearch() {
    var searchText = $('#globsearchText').val();
    if (searchText == 'Search') {
        searchText = '';
    }
    globalQueryTable.fnFilter(searchText);
    $("div.success,div.error").remove();
    return false;
}

function DoLocalQuerySearch() {
    var searchText = $('#locsearchText').val();
    if (searchText == 'Search') {
        searchText = '';
    }
    localQueryTable.fnFilter(searchText);
    $("div.success,div.error").remove();
    return false;
}

$(function () {

    var nonSortIndexArray = [];

    var noSortColumns = $("#globalQueryList th.noSortCol");
    if (noSortColumns.length > 0) {
        noSortColumns.each(function () {
            var indexVal = noSortColumns.parent("tr").children().index(this);
            nonSortIndexArray.push(indexVal);
        });
    }

    globalQueryTable = $('#globalQueryList').dataTable({
        "bFilter": false,
        "bInfo": true,
        "sPaginationType": "full_numbers",
        "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": nonSortIndexArray}]
    });

    $('#globalQueryList th:nth-child(3)').css("text-align", "center");
    $('#globalQueryList th:nth-child(4)').css("text-align", "center");
    $('#globalQueryList th:nth-child(5)').css("text-align", "center");

    var noSortColumns = $("#localQueryList th.noSortCol");
    if (noSortColumns.length > 0) {
        noSortColumns.each(function () {
            var indexVal = noSortColumns.parent("tr").children().index(this);
            nonSortIndexArray.push(indexVal);
        });
    }

    localQueryTable = $('#localQueryList').dataTable({
        "bFilter": false,
        "bInfo": true,
        "sPaginationType": "full_numbers",
        "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": nonSortIndexArray}]
    });

    $('#localQueryList th:nth-child(3)').css("text-align", "center");
    $('#localQueryList th:nth-child(4)').css("text-align", "center");
    $('#localQueryList th:nth-child(5)').css("text-align", "center");

    $(".copyQueries").click(function () {

        window.location.href = queryCopyURL;

    });
    
    $('#executeQuery').click(function () {
        var queryName = $('#QueryList option:selected').text();
        var queryId = $('#QueryList').val();
        executeQuery(queryId, queryName);
    });

    $('#editQuery').click(function () {
        if (queryId == '-1') return false;
        var queryId = $('#QueryList').val();
        // goto edit page
        window.location = manageQueryURL + '?queryId=' + queryId;
    });

    $('#deleteQuery').click(function () {
        var queryName = $('#QueryList option:selected').text();
        var queryId = $('#QueryList').val();
        deleteQuery(queryId, queryName);
    });
});

function deleteQuery(queryId, queryName) {
    if (queryId == '-1') {
        return false;
    }
    if (confirm('Are you sure you want to delete "' + queryName + '"?')) {
        // delete the query
        window.location = deleteQueryURL + '?queryId=' + queryId;
    }
}

// --------------------------------------
// show the dynamic variable in the popup
// --------------------------------------
function ManageDynamicVariables() {
    if (dynamicVariables == undefined || dynamicVariables == null || dynamicVariables.length < 1) {
        return true;
    }

    var dynVarMarkUp = "";

    for (var i = 0; i < dynamicVariables.length; i++) {

        var dynamicVarible = dynamicVariables[i];
        var elementMarkUp = "<tr>";
        var key = dynamicVarible.VariableId;

        // this key is used to find the field based on the key and the name of the dynamic variable.
        elementMarkUp += "<td class='row4'>" + dynamicVarible.Name + " <input type='hidden' class='key' value='" + key + "' /> </td>";

        var value = dynamicVarible.Value || '';

        elementMarkUp += "<td class='row5'> <input type='text' class='dynVariableValue' name='" + key + "' value='" + value + "' /></td>";
        elementMarkUp += "</tr>";
        dynVarMarkUp += elementMarkUp;
    }

    $(".dynamicVariables table tbody").html(dynVarMarkUp);

    if ($(".ui-dialog-title").length > 0) {
        $(".ui-dialog-title").html('Manage Dynamic Variables');
    }

    $(".dynamicVariables").dialog({
        modal: true,
        height: 600,
        width: 800
        //closeOnEscape: false
    });

    $(".dynamicVariables").dialog("open");

    $(".ui-dialog-title").html('Manage Dynamic Variables');
}

$(".updateDynamicVariableValues").click(function () {
    UpdateDynamicVariableValues();
});

// -----------------------------------------
// fill the values for the dynamic variables
// -----------------------------------------
function UpdateDynamicVariableValues() {
    var canExecute = true;
    $(".dynamicVariables .dynVariableValue").each(function () {
        var key = $(this).attr("name");
        // get the field here and then set the value and then continue with the regular post back.
        var field = _getDynamicVariableByKey(key);

        if (field == null || field == undefined) {
            alert("No matching field with the dynamic variable is found");
            return false;
        }

        var value = jQuery.trim($(this).val());

        if (value.length < 1) {
            alert("Provide a valid value for " + field.Name);
            canExecute = false;
            return false;
        }

        field.Value = value;
    });

    if (canExecute && currentMode === "View") {
        $(".dynamicVariables").dialog("close");
        _ExecuteQuery($("#queryId").val());
    }
    else if (canExecute && currentMode === "Export") {
        if (dynamicVariables !== undefined && dynamicVariables.length > 0) {

            for (var i = 0; i < dynamicVariables.length; i++) {
                $('form').append($("<input />").attr({ "type": "hidden", "name": "dynamicVariables[" + i + "].VariableId", "Value": guidGenerator() }));
                $('form').append($("<input />").attr({ "type": "hidden", "name": "dynamicVariables[" + i + "].Name", "Value": dynamicVariables[i].Name }));
                $('form').append($("<input />").attr({ "type": "hidden", "name": "dynamicVariables[" + i + "].Value", "Value": dynamicVariables[i].Value }));
            }
        }

        if ($("form input[name='QueryList']").length < 1) {
            $('form').append($("<input />").attr({ "type": "hidden", "name": "QueryList", "Value": $("#queryId").val() }));
        }
        else {
            $("form input[name='QueryList']").val($("#queryId").val());
        }

        $('form').submit();
        $(".dynamicVariables").dialog("close");
    }
    else {
        return false;
    }
}

function _getDynamicVariableByKey(key) {
    if (dynamicVariables == undefined || dynamicVariables == null || dynamicVariables.length < 1) {
        alert("Exception Occured while handling the dynamicvariables");
        return false;
    }
    for (var j = 0; j < dynamicVariables.length; j++) {
        if (dynamicVariables[j].VariableId.toLowerCase() === key.toLowerCase()) {
            return dynamicVariables[j];
        }
    }
}

function executeQuery(queryId, queryName, executeMode) {

    if ($("body #queryId").length < 1) {
        $("body").append($("<input />").attr({ "type": "hidden", "id": "queryId", "value": queryId }));
    } else {
        $("body #queryId").val(queryId);
    }

    if (queryId == '-1') {
        return false;
    }

    // get the list of dynamic variables
    $.ajax({
        url: dynamicVariablesURL,
        data: { 'queryId': queryId },
        type: "POST",
        success: function (data) {
            if (data === undefined) {
                alert("Exception occured while fetching the Dynamic variables");
            }

            //var result = JSON.parse(data);
            var result = data;

            if (result.Error !== undefined) {

                if (executeMode == undefined) {
                    executeMode = currentMode;
                }
                else {
                    currentMode = executeMode;
                }

                if (currentMode == undefined || currentMode === "View") {
                    _ExecuteQuery(queryId);
                }
                if (currentMode === "Export") {
                    if ($("form input[name='QueryList']").length < 1) {
                        $('form').append($("<input />").attr({ "type": "hidden", "name": "QueryList", "Value": queryId }));
                    }
                    else {
                        $("form input[name='QueryList']").val(queryId);
                    }
                    $("form").submit();
                    currentMode = "View";
                    return true;
                }

                return false;
            }
            dynamicVariables = result;

            if (ManageDynamicVariables()) {
                if (currentMode == undefined || currentMode === "View") {
                    _ExecuteQuery(queryId);
                }

                if (currentMode === "Export") {
                    return true;
                }
            }
        }
    });
    return false;
}

function _ExecuteQuery(queryId) {
    var temp = {
        'queryId': queryId,
        'dynamicVariables': dynamicVariables
    };

    $.ajax({
        url: savedQueryExecutionURL,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(temp),
        success: function (data) {
            $('#executeQueryDiv').html(data);
            $('#executeQueryDiv').show();
        }
    });
}

