﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Collections.Generic;
using System.Linq;
using Owin;
using System.Web;
using Microsoft.Owin.Logging;
using CelloSaaS.Owin.Security;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication.Provider;
using CelloSaaS.View;
using CelloSaaS.AuthServer.Client.Extensions;
using CelloSaaS.Model;
using CelloSaaS.ServiceProxies.SettingsManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;
using System.Security.Principal;
using System.Security.Claims;
using CelloSaaS.Library;

[assembly: OwinStartupAttribute(typeof(CelloSaaSWebClient.Startup))]
namespace CelloSaaSWebClient
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            System.IdentityModel.Tokens.JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                CookieName = "FormAuthentication",
                LoginPath = new PathString("/Account/Logon"),
                SlidingExpiration = true,
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            var authOptions = new GenericOAuth2AuthenticationOptions(AuthenticationConstants.CelloAuthentication)
            {
                SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie,
                AccessType = AuthenticationConstants.CelloAuthentication,
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive,
                AuthorizationEndpointUri = AppSettingHelper.GetAuthServerUri() + "authorize",
                TokenEndpointUri = AppSettingHelper.GetAuthServerUri() + "api/token",
                UserInfoEndpointUri = AppSettingHelper.GetAuthServerUri() + "api/userinfo",
                Scope = new List<string> { "all" },
                Provider = new GenericOAuth2AuthenticationProvider
                {
                    InitRequestFromAuthenticationProperties = EndpointUriTypes => true,
                    OnMapContextToClaims = async context =>
                    {
                        List<Claim> additionalClaims = new List<Claim>();
                        if (!string.IsNullOrEmpty(context.Id))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.UserId, context.Id));
                        }
                        if (!string.IsNullOrEmpty(context.ClientId))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.ClientId, context.ClientId));
                        }
                        if (!string.IsNullOrEmpty(context.TenantId))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.TenantId, context.TenantId));
                            additionalClaims.Add(new Claim(CelloClaimTypes.LoggedInTenantId, context.TenantId));
                        }
                        if (!string.IsNullOrEmpty(context.Scope))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.Scopes, context.Scope));
                        }
                        if (!string.IsNullOrEmpty(context.Profile))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.Profile, context.Profile));
                        }

                        if (!string.IsNullOrEmpty(context.Role))
                        {
                            additionalClaims.Add(new Claim(ClaimTypes.Role, string.Join(",", context.Role)));
                            additionalClaims.Add(new Claim(CelloClaimTypes.LoggedInUserRoles, string.Join(",", context.Role)));
                        }
                        if (context.ExpiresIn != null && context.ExpiresIn.HasValue && context.ExpiresIn.Value.TotalSeconds > 0)
                        {
                            additionalClaims.Add(new Claim(ClaimTypes.Expiration, context.ExpiresIn.Value.TotalSeconds.ToString()));
                        }
                        return additionalClaims;
                    }
                }
            };

            app.Map(new PathString("/Account/LogOn"), loginApp =>
           {
               loginApp.UseGenericOAuth2Authentication(authOptions);
           });
        }
    }
}