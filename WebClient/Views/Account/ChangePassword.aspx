﻿<%@ Page Language="C#" Title="<%$ Resources:Title %>" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="changePasswordHead" ContentPlaceHolderID="head" runat="server">
    <title><%=this.GetLocalResourceObject("Title")%></title>
</asp:Content>
<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row-fluid pd-25">
        <% using (Html.BeginForm())
           { %>
        <section class="panel red">
            <header class="panel-heading">
                <h4><%=this.GetGlobalResourceObject("General","ChangePassword") %></h4>
            </header>
            <div class="panel-body">
                <%if (TempData["Success"] != null && !string.IsNullOrEmpty(TempData["Success"].ToString()))
                  { %>
                <div class="alert alert-success">
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                    <%: TempData["Success"].ToString()%>
                </div>
                <%} %>
                <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
                  { %>
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                    <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
                </div>
                <%} %>
                <div class="form-container">
                    <div class="form-group <%=Html.ValidationMessage("currentPassword","*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%=this.GetLocalResourceObject("CurrentPassword")%></label>
                        <%= Html.Password("currentPassword", string.Empty, new {  })%>
                        <%= Html.CelloValidationMessage("currentPassword","*") %>
                    </div>
                    <div class="form-group <%=Html.ValidationMessage("newPassword","*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%=this.GetLocalResourceObject("NewPassword")%></label>
                        <%= Html.Password("newPassword", "", new { maxlength = 128 })%>
                        <%= Html.CelloValidationMessage("newPassword","*") %>
                    </div>
                    <div class="form-group <%=Html.ValidationMessage("confirmPassword","*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%=this.GetLocalResourceObject("ConfirmPassword")%></label>
                        <%= Html.Password("confirmPassword", "", new { maxlength = 128 })%>
                        <%= Html.CelloValidationMessage("confirmPassword","*") %>
                    </div>
                </div>
                <div class="pull-right">
                    <a class="btn btn-info" href="#" onclick="$('form').submit()" title="<%=this.GetLocalResourceObject("Updatenewpassword")%>">
                        <%=this.GetGlobalResourceObject("General","Submit") %></a>
                </div>
            </div>
        </section>
        <% } %>
    </div>
</asp:Content>
