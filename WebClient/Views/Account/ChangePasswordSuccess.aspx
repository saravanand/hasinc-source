<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title><%=this.GetLocalResourceObject("Title") %></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm())
       { %>
    <table border="0" cellspacing="0" cellpadding="0" class="breadcrum">
        <tr>
            <td>
                <ul>
                    <li>
                        <%=this.GetLocalResourceObject("ChangePassword")%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" class="table-main">
        <tr>
            <td>
                <%=this.GetLocalResourceObject("ChangePasswordSuccess")%>
            </td>
        </tr>
    </table>
    <% } %>
</asp:Content>
