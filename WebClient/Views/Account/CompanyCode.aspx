<%@ Page Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master" Inherits="System.Web.Mvc.ViewPage"
    Title="<%$ Resources:title %>" %>

<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="login-box-plain" style="min-height: 0px; margin-top: 45%;">
        <form action="<%=Url.Action("LogOn") %>" method="get">
            <div class="form-group">
                <label>
                    <%=this.GetLocalResourceObject("PleaseentertheCompany") %>
                </label>
                 <i class="fa fa-cloud"></i>
                <%: Html.TextBox("companyCode", null, new { style="width:100%;" })%>
            </div>
            <div class="form-actions">
                <button class="btn btn-primary btn-block" type="submit"><%=this.GetGlobalResourceObject("General", "Submit") %></button>
            </div>
        </form>
    </div>
    <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
       { %>
    <div class="alert alert-danger">
        <%=Html.CelloValidationSummary()%>
    </div>
    <% } %>
    <script type="text/javascript">
        $(function () {
            $('form').submit(function () {
                var companyCode = $('input[name=companyCode]').val();
                if (companyCode == '' || companyCode.length < 2) {
                    alert('<%=this.GetLocalResourceObject("CompanyCode") %>');
                    $('input[name=companyCode]').select();
                    return false;
                }
                return true;
            });
            $('input[name=companyCode]').select();
        });
    </script>
</asp:Content>
