﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<% if (ViewData["TenantCodeString"] != null)
   { %>
<input type="hidden" name="companyCode" value="<%: ViewData["TenantCodeString"]%>" />
<% } %>
<input type="hidden" name="step" value="1" />
<div class="row">
    <div class="form-container col-md-12">
        <% if (ViewData["TenantCodeString"] == null)
           { %>
        <div class="form-group">
            <label><%=this.GetLocalResourceObject("Companycode") %></label>
            <i class="fa fa-cloud"></i>
            <input name="companyCode" type="text" value="<%:ViewData["companyCode"] %>" />
        </div>
        <% } %>
        <div class="form-group">
            <label><%=this.GetLocalResourceObject("Username")%></label>
            <i class="fa fa-user"></i>
            <input name="userName" type="text" value="<%:ViewData["userName"] %>" />
        </div>
        <div class="form-group col-md-8 col-md-offset-3">
            <a class="btn btn-default" href="<%=Url.Action("LogOn","Account") %>"><%=this.GetGlobalResourceObject("General","Cancel")%></a>
            <button class="btn btn-success" type="submit"><%=this.GetGlobalResourceObject("General","Submit") %></button>
        </div>
    </div>
</div>
<script>
    $(function () {
        if ($('input[name=companyCode]').val().length > 0) {
            $('input[name=userName]').focus().select();
        } else {
            $('input[name=companyCode]').focus().select();
        }
    });
</script>
