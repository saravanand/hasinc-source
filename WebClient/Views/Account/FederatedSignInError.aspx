﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="heading_container">
        <h1>
            <%=this.GetLocalResourceObject("error")%></h1>
    </div>
    <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
       { %>
    <div class="error">
        <%=Html.CelloValidationSummary()%>
    </div>
    <%} %>
    <div class="clearfix"></div>
    <div class="center form-container">
        <div class="heading">
            <%=this.GetLocalResourceObject("Claims")%>
        </div>
        <div style="padding: 10px;">
            <ul>
                <% var claims = ((Microsoft.IdentityModel.Claims.IClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identities[0].Claims;

                   foreach (var item in claims)
                   {
                       Response.Write("<li>" + item.ClaimType + " : " + item.Value + "</li>");
                   }
                %>
            </ul>
        </div>
        <div style="margin:10px 30px;">
            <div class="button">
                <%= Html.ActionLink(this.GetLocalResourceObject("clickhere").ToString(), "LogOn", "Account", new { werror = "werror" }, null)%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
