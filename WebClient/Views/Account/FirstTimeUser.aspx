﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select').select2();
            $('input[name=password]').focus().select();
        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="login-box-plain">
        <h2><i class="fa fa-thumbs-up"></i>&nbsp;<%: this.GetLocalResourceObject("Title").ToString() %></h2>
        <br />
        <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary()%>
        </div>
        <% } %>
        <% using (Html.BeginForm())
           { %>
        <div class="form-container">
            <div class="form-group">
                <label><%: this.GetLocalResourceObject("CompanyCode").ToString() %></label>
                <% if (ViewData["companyCode"] != null)
                   {%>
                <br />
                <%:ViewData["companyCode"]%>
                <input type="hidden" name="companyCode" value="<%:ViewData["companyCode"]%>" />
                <%
                   }
                   else
                   {%>
                <input type="text" name="companyCode" value="<%:ViewData["companyCode"]%>" />
                <% } %>
            </div>
            <div class="form-group">
                <label><%: this.GetLocalResourceObject("UserName").ToString() %></label>
                <br />
                <%: ViewData["userName"] %>
                <input type="hidden" name="userName" value="<%: ViewData["userName"] %>" />
            </div>
            <div class="form-group">
                <label><%: this.GetLocalResourceObject("NewPassword").ToString() %></label>
                <i class="fa fa-key"></i>
                <input type="password" name="password" id="newPassword" value="" />
            </div>
            <div class="form-group">
                <label><%: this.GetLocalResourceObject("ConfirmPassword").ToString() %></label>
                <i class="fa fa-key"></i>
                <input type="password" name="confirmPassword" id="confirmPassword" value="" />
            </div>
            <div class="form-group">
                <label><%: this.GetLocalResourceObject("SecurityQuestion").ToString() %></label>
                <select name="securityQuestion" style="width: 100%">
                    <option>
                        <%: this.GetLocalResourceObject("PetName")%></option>
                    <option>
                        <%: this.GetLocalResourceObject("AnniversaryDate") %></option>
                    <option>
                        <%: this.GetLocalResourceObject("BirthCity") %></option>
                    <option>
                        <%: this.GetLocalResourceObject("MiddleName_Father") %></option>
                    <option>
                        <%: this.GetLocalResourceObject("MiddleName_Spouse") %></option>
                    <option>
                        <%: this.GetLocalResourceObject("MiddleName_Child") %></option>
                    <option>
                        <%: this.GetLocalResourceObject("LastName_Friend") %></option>
                    <option>
                        <%: this.GetLocalResourceObject("HighSchoolCity") %></option>
                    <option>
                        <%: this.GetLocalResourceObject("CarMake") %></option>
                </select>
            </div>
            <div class="form-group">
                <label><%: this.GetLocalResourceObject("SecurityQuestionAnswer").ToString() %></label>
                <i class="fa fa-lock"></i>
                <input type="text" name="securityAnswer" value="" autocomplete="off" />
            </div>
            <div class="form-group text-center">
                <button class="btn btn-success w100" type="submit"><%=this.GetGlobalResourceObject("General","Submit") %></button>
            </div>
        </div>
        <% } %>
    </div>
</asp:Content>
