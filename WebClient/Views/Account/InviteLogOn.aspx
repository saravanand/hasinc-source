﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Html.ValidationSummary() != null)
       { %>
    <div class="alert alert-danger">
        <%:Html.ValidationSummary() %>
    </div>
    <% }%>
</asp:Content>