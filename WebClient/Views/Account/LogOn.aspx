﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master" Inherits="System.Web.Mvc.ViewPage"
    Title="<%$ Resources:title %>" %>

<asp:Content ID="loginHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            if ($('#companyCode').val() == '') {
                $('#companyCode').focus();
                $('#companyCode').select();
            }
        });

        function SubmitForm() {
            $('form').submit();
        }
    </script>
</asp:Content>
<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="login-box-plain">
        <h2 class="text-center"><%=this.GetLocalResourceObject("CompanyCode") %></h2>
        <div class="divide-20"></div>
        <% if (Html.ValidationSummary() != null)
           { %>
        <div id="errorMsg" class="alert alert-danger">
            <%:Html.ValidationSummary() %>
        </div>
        <% }%>
        <% using (Html.BeginForm())
           { %>
        <div class="form-group <%=Html.ValidationMessage("companyCode") !=null ? "has-error" : "" %>">
            <label for="companyCode"><%=this.GetLocalResourceObject("CompanyCode") %></label>
            <i class="fa fa-cloud"></i>
            <%=Html.TextBox("companyCode",null,new{@class="form-control",placeholder="company", required="required"}) %>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary btn-block" type="submit"><%=this.GetGlobalResourceObject("General", "Submit") %></button>
        </div>
        <% } %>
        <div class="divide-20"></div>
        <div class="login-helpers">
            <%=this.GetLocalResourceObject("m_NoAccount") %>
            <a href="<%:Url.Action("TenantSelfRegistration","TenantSelfRegistration") %>"><%=this.GetLocalResourceObject("lbl_Registernow") %></a>
        </div>
    </div>

</asp:Content>
