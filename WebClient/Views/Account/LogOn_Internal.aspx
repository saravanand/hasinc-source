﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master" Inherits="System.Web.Mvc.ViewPage"
    Title="<%$ Resources:title %>" %>

<asp:Content ID="loginHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            if ($('#companyCode').val() == '') {
                $('#companyCode').focus();
                $('#companyCode').select();
            } else if ($('#username').val() == '') {
                $('#username').focus();
                $('#username').select();
            } else if ($('#password').val() == '') {
                $('#password').focus();
            } else {
                $('#username').select();
                $('#username').focus();
            }
        });

        function SubmitForm() {
            $('form').submit();
        }
    </script>
</asp:Content>
<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="login-box-plain">
        <h2 class="bigintro"><%=this.GetLocalResourceObject("Login") %></h2>
        <div class="divide-20"></div>
        <% if (Html.ValidationSummary() != null)
           { %>
        <div id="errorMsg" class="alert alert-danger">
            <%:Html.ValidationSummary() %>
        </div>
        <% }%>
        <% using (Html.BeginForm())
           { %>
        <% if (ViewData["TenantCodeString"] != null)
           { %>
        <input type="hidden" name="companyCode" value="<%:ViewData["TenantCodeString"] %>" />
        <% }
           else
           { %>
        <div class="form-group <%=Html.ValidationMessage("companyCode") !=null ? "has-error" : "" %>">
            <label for="companyCode"><%=this.GetLocalResourceObject("CompanyCode") %></label>
            <i class="fa fa-cloud"></i>
            <%=Html.TextBox("companyCode",null,new{@class="form-control",placeholder="company"}) %>
        </div>
        <% } %>
        <div class="form-group <%=Html.ValidationMessage("username") !=null ? "has-error" : "" %>">
            <label for="username"><%=this.GetLocalResourceObject("UserName") %></label>
            <i class="fa fa-user"></i>
            <%=Html.TextBox("username",null,new{@class="form-control",placeholder="john.smith"}) %>
        </div>
        <div class="form-group <%=Html.ValidationMessage("password") !=null ? "has-error" : "" %>">
            <label for="password"><%=this.GetLocalResourceObject("Password") %></label>
            <i class="fa fa-lock"></i>
            <%=Html.Password("password",null,new{@class="form-control",placeholder="password"}) %>
        </div>
        <div class="form-actions">
            <label class="checkbox">
                <input class="uniform" name="isPersistent" id="isPersistent" type="checkbox" value="true"><%=this.GetLocalResourceObject("m_Keepmeloggedin") %>
            </label>
            <button class="btn btn-primary btn-block" type="submit"><%=this.GetGlobalResourceObject("General", "Submit") %></button>
        </div>
        <% } %>
        <div class="divide-20"></div>
        <div class="login-helpers">
            <a href="<%:Url.Action("ResetPassword","Account") %>" title="<%=this.GetLocalResourceObject("t_Resetyourpassword") %>">
                <%=this.GetLocalResourceObject("ForgetPassword") %></a>
            <br>
            <%=this.GetLocalResourceObject("m_NoAccount") %>
            <a href="<%:Url.Action("TenantSelfRegistration","TenantSelfRegistration") %>"><%=this.GetLocalResourceObject("lbl_Registernow") %></a>
        </div>
    </div>
</asp:Content>