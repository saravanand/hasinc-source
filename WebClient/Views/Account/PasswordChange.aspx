﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="login-box-plain">
        <h2>
            <% if (Convert.ToBoolean(ViewData["passExpired"]))
               {
            %>
            <%:this.GetLocalResourceObject("ResetExpiredPassword").ToString()%>
            <%
               }
               else
               {
            %>
            <%:this.GetLocalResourceObject("Title").ToString()%>
            <% } %>
        </h2>
        <% if (Convert.ToBoolean(ViewData["Status"]))
           {
        %>
        <script type="text/javascript">
            var count = 5;
            setTimeout(redirect, 1000);

            function redirect() {
                $('#sec').text(count);
                count--;
                if (count <= 0) {
                    document.location = '/Account/LogOn';
                } else {
                    setTimeout(redirect, 1000);
                }
            }
        </script>
        <div class="alert alert-success" style="margin: 20px;">
            <%= Html.ValidationMessage("Success") %>.
        <%=this.GetLocalResourceObject("Thispage")%>
            <span id="sec"></span>&nbsp;<%=this.GetLocalResourceObject("seconds")%>
            <a class="btn btn-info" href="<%=Url.Action("LogOn","Account") %>" title="<%=this.GetLocalResourceObject("lbl_LogOnPage")%>"><%=this.GetLocalResourceObject("Click")%></a>
        </div>
        <%
           }
           else
           {%>
        <% using (Html.BeginForm("PasswordChange", "Account", FormMethod.Post, new { @class = "passwordChangeForm" }))
           { %>
        <% if (!Convert.ToBoolean(ViewData["Status"]) && !string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger" style="display: block; margin: 30px auto;">
            <%=Html.CelloValidationSummary(this.GetLocalResourceObject("summary").ToString())%>
        </div>
        <% } %>
        <div class="form-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="UserName">
                            <%:GetLocalResourceObject("UserName").ToString()%>
                        </label>
                        <br />
                        <%:ViewData["userName"].ToString()%>
                        <%:Html.Hidden("userName",ViewData["userName"].ToString())%>
                        <%:Html.Hidden("companyCode",ViewData["companyCode"].ToString())%>
                    </div>
                    <div class="form-group">
                        <label class="mandatory" for="currentPassword">
                            <%:GetLocalResourceObject("CurrentPassword").ToString()%>
                        </label>
                        <%=Html.Password("currentPassword", ViewData["oldPassword"])%>
                    </div>
                    <div class="form-group">
                        <label class="mandatory" for="newPassword">
                            <%:GetLocalResourceObject("NewPassword").ToString()%>
                        </label>
                        <%=Html.Password("newPassword", "", new { maxlength = 128 })%>
                    </div>
                    <div class="form-group">
                        <label class="mandatory" for="confirmPassword">
                            <%:GetLocalResourceObject("ConfirmPassword").ToString()%>
                        </label>
                        <%=Html.Password("confirmPassword", "", new { maxlength = 128 })%>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-3">
                        <a class="btn btn-default" href="<%=Url.Action("LogOn","Account") %>"><%=this.GetGlobalResourceObject("General","Cancel")%></a>
                        <button class="btn btn-success" type="submit"><%=this.GetGlobalResourceObject("General","Submit") %></button>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () { $('input[name=currentPassword]').focus(); });
    </script>
</asp:Content>
