﻿<%@ Page Title="<%$ Resources:title %>" Language="C#" MasterPageFile="~/Views/Shared/LogOn.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="login-box-plain">
        <% if (ViewData["updateSuccess"] != null)
           { %>
        <h2><i class="fa fa-thumbs-up"></i>&nbsp;<%=this.GetLocalResourceObject("h_ResetPasswordSuccess")%></h2>
        <script type="text/javascript">
            var count = 8;
            setTimeout(redirect, 1000);

            function redirect() {
                $('#sec').text(count);
                count--;
                if (count <= 0) {
                    document.location = '/Account/LogOn';
                } else {
                    setTimeout(redirect, 1000);
                }
            }
        </script>
        <br />
        <br />
        <p>
            <span class="label label-info">
                <%: ViewData["updateSuccess"]%></span> <%: this.GetLocalResourceObject("automaticallyredirected")%> <span id="sec"></span>&nbsp;<%: this.GetLocalResourceObject("seconds")%>
            <a href="<%=Url.Action("LogOn","Account") %>" title="<%=this.GetLocalResourceObject("lbl_LogOnPage")%>"><%: this.GetLocalResourceObject("click")%></a>
        </p>
        <% }
           else
           {
               using (Html.BeginForm())
               {%>
        <h2><%: this.GetLocalResourceObject("title") %>&nbsp;<%: ViewData["Step"] %></h2>
        <br />
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="alert alert-danger">
            <%= Html.ValidationSummary()%>
        </div>
        <% } %>
        <% 
                   switch (Convert.ToInt32(this.ViewData["Step"]))
                   {
                       case 1:
                           this.Html.RenderPartial("CompanyCodeUserNameForm");
                           break;
                       case 2:
                           this.Html.RenderPartial("SecurityQuestionAnswerForm");
                           break;
                       case 3:
                           this.Html.RenderPartial("NewPasswordForm");
                           break;
                   } %>
        <% } %>
    </div>
    <% } %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
