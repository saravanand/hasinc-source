﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="row">
    <div class="form-container col-md-12">
        <% if (ViewData["TenantCodeString"] == null)
           { %>
        <div class="form-group">
            <label><%=this.GetLocalResourceObject("Companycode") %>:</label>
            <br />
            <%:ViewData["companyCode"]%>
        </div>
        <% } %>
        <div class="form-group">
            <label><%: this.GetLocalResourceObject("Username")%>:</label>
            <br />
            <%: ViewData["userName"] %>
            <input type="hidden" name="userName" value="<%: ViewData["userName"] %>" />
        </div>
        <div class="form-group">
            <label><%: this.GetLocalResourceObject("SecurityQuestion")%>:</label>
            <br />
            <%: ViewData["securityQuestion"] %>
            <input type="hidden" name="securityQuestion" value="<%: ViewData["securityQuestion"] %>" />
        </div>
        <div class="form-group">
            <label><%: this.GetLocalResourceObject("Answerquestion")%>:</label>
            <i class="fa fa-lock"></i>
            <input type="text" name="securityAnswer" autocomplete="off" />
        </div>
        <div class="col-md-8 col-md-offset-3">
            <a class="btn btn-default" href="<%=Url.Action("LogOn","Account") %>"><%=this.GetGlobalResourceObject("General","Cancel")%></a>
            <button class="btn btn-success" type="submit"><%=this.GetGlobalResourceObject("General","Submit") %></button>

            <input type="hidden" name="step" value="2" />
            <input type="hidden" name="companyCode" value="<%: ViewData["companyCode"] %>" />
            <input type="hidden" name="membershipId" value="<%: ViewData["membershipId"] %>" />
            <input type="hidden" name="tenantCode" value="<%: ViewData["tenantCode"] %>" />
        </div>
    </div>
</div>
<script>
    $(function () { $('input[name=securityAnswer]').focus().select(); });
</script>