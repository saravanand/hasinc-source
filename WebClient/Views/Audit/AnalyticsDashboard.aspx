﻿<%@ Page Title="<%$Resources:T_AnalyticsDashboard%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <h3><%:this.GetLocalResourceObject("h_AnalyticsDashboard") %></h3>
    </div>
    <div class="row-fluid pd-25" id="dashboard-container">
        <div class="row content-box">
            <div class="col-md-6 form-container">
                <form class="form-inline" method="post" action="<%=Url.Action("AnalyticsDashboard") %>">
                    <div class="form-group">
                        <label for="from">From</label>
                        <%=Html.TextBox("from", ((DateTime)ViewData["fromDate"]).ToShortDateString(), new { @class="datetime" })%>
                    </div>
                    <div class="form-group">
                        <label for="to">To</label>
                        <%=Html.TextBox("to", ((DateTime)ViewData["toDate"]).ToShortDateString(), new { @class="datetime" }) %>
                    </div>
                    <div class="form-actions pull-right">
                        <br />
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i>&nbsp;Search</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </form>
            </div>
            <div class="ledgend col-md-6 text-right">
                <label><%:this.GetLocalResourceObject("lbl_UsageCategory") %></label>
                <div class="ledgend-box" style="background-color: rgb(225,177,58)"></div>
                <%:this.GetLocalResourceObject("lbl_Lowusage") %>
                <div class="ledgend-box" style="background-color: rgb(191,0,108)"></div>
                <%:this.GetLocalResourceObject("lbl_Mediumusage") %>
                <div class="ledgend-box" style="background-color: rgb(105,0,162)"></div>
                <%:this.GetLocalResourceObject("lbl_Heavyusage") %>
            </div>
        </div>
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (Html.ValidationMessage("Error") == null)
           { %>
        <div class="row-fluid" id="tenant-usage-stats-div">
            <% Html.RenderAction("TenantUsageStats", new { from = ViewBag.fromDate, to = ViewBag.toDate }); %>
        </div>
        <div class="row-fluid" id="module-usage-stats-div">
            <% Html.RenderAction("ModuleUsageStats", new { from = ViewBag.fromDate, to = ViewBag.toDate }); %>
        </div>
        <div class="row-fluid" id="user-usage-stats-div">
            <% Html.RenderAction("UserUsageStats", new { from = ViewBag.fromDate, to = ViewBag.toDate }); %>
        </div>
        <% } %>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select').select2();
            $('input.datetime').datepicker();
        });
    </script>
    <script src="/Scripts/d3.v3.min.js"></script>
    <style>
        .ledgend-box {
            width: 12px;
            height: 12px;
            border: 1px solid #eee;
            display: inline-block;
            margin-left: 20px;
        }

        .circle {
            stroke: black;
            stroke-width: 2px;
            stroke-opacity: .1;
            transition: all .4s ease;
        }

            .circle.active {
                stroke-width: 4px;
                stroke-opacity: .2;
            }

        .text {
            font-size: .9em;
            opacity: .5;
        }

        .tooltip {
            padding: .5em .8em;
            background: rgb(255,255,255);
            box-shadow: 0 0 .3em silver;
            font-size: 1.2em;
            border-radius: .5em;
            position: absolute;
            z-index: -9999;
            min-width: 100px;
        }

        .tooltip {
            opacity: 0;
        }

            .tooltip.active {
                z-index: 1 !important;
                opacity: 1;
            }

        .labels {
            font-size: 1.05em;
            padding: .3em 0 0;
        }

        .count {
            font-weight: bold;
            font-size: 1.1em;
        }

            .count i {
                font-weight: normal;
                font-size: .7em;
                font-style: normal;
            }

            .count.single {
                line-height: 1.4;
                margin-top: .2em;
                font-size: 1.6em;
            }

                .count.single i {
                    display: block;
                    color: #999;
                    font-size: 12px;
                }
    </style>
</asp:Content>
