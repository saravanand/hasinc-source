﻿<%@ Page Title="<%$ Resources:T_BillingAudits %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.Billing.Model.BillingAudit>>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%:this.GetLocalResourceObject("T_BillingAudits") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-title">
                <div class="input-group">
                    <input id="searchText" name="searchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                    </span>
                </div>
            </div>
            <div class="grid-body">
                <% if (this.Model != null && this.Model.Count() > 0)
                   {
                       Html.Grid(this.Model).Columns(
                        column =>
                        {
                            column.For(col => string.IsNullOrEmpty(col.InvoiceNo) ? "-" : string.Format("<a target='_blank' href='{1}' title='" + this.GetLocalResourceObject("t_ViewInvoice").ToString() + "'>{0}</a>", col.InvoiceNo, Url.Action("PreviewInvoice", "Billing", new { invoiceId = col.InvoiceId, tenantId = col.TenantId }))).Named(this.GetLocalResourceObject("lbl_InvoiceNo").ToString()).DoNotEncode();
                            column.For(col => col.InvoiceDate.ToUIDateString()).Named(this.GetLocalResourceObject("lbl_Date").ToString());
                            column.For(col => col.BillFrequency.ToString()).Named(this.GetLocalResourceObject("lbl_BillFrequency").ToString());
                            column.For(col => col.Duration.HasValue ? (1000 * col.Duration.Value).ToString("F2") : "-").Named(this.GetLocalResourceObject("lbl_Duration").ToString());
                            column.For(col => col.BillingAuditStatus.ToString().ToUpperInvariant()).Named(this.GetLocalResourceObject("lbl_Status").ToString());
                            column.For(col => col.ExceptionDetails ?? "--").Attributes(@style => "width:150px;text-wrap:wrap;").HeaderAttributes(@style => "width:150px;").Named(this.GetLocalResourceObject("lbl_ExceptionDetails").ToString());
                        }).Attributes(id => "billingAuditTable", @class => "celloTable").Render();
                   }
                   else
                   { %>
                <div class="alert alert-info">
                    <%=this.GetGlobalResourceObject("General","NoData") %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        function jQDataTable() {
            filterTable = $('table#billingAuditTable').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        }

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            return false;
        }

        $(function () {
            jQDataTable();
        });
    </script>
</asp:Content>
