﻿<%@ Page Title="Browser Statistics" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Model.ProductAnalytics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var tenantlist = ViewBag.TenantSelectList as List<SelectListItem> ?? new List<SelectListItem>();
        var result = ViewBag.Result as Dictionary<string, ProductAnalytics>;
        var browserStats = ViewBag.Stats as IEnumerable<Newtonsoft.Json.Linq.JObject>;
        var countryStats = ViewBag.CountryStats as Dictionary<string, Tuple<CelloSaaS.Util.Library.IpMapResponse, int>>;
        IOrderedEnumerable<IGrouping<string, Newtonsoft.Json.Linq.JObject>> platforms = null;
        string platformdata = string.Empty;
    %>
    <div id="loading"></div>
    <div class="page-title">
        <h3>Browser Statistics</h3>
        <% if (tenantlist.Count > 0)
           { %>
        <div class="pull-right">
            Tenant: <%=Html.DropDownList("tenantId", tenantlist, new { id="browser-stats-tenantId", style="width:180px;" })%>
        </div>
        <% } %>
    </div>
    <div class="row-fluid pd-25" id="dashboard-container">
        <div class="row content-box">
            <div class="col-md-6 form-container">
                <form class="form-inline" method="post" action="<%=Url.Action("BrowserStats") %>">
                    <div class="form-group">
                        <label for="from">From</label>
                        <%=Html.TextBox("from", ((DateTime)ViewData["fromDate"]).ToShortDateString(), new { @class="datetime" })%>
                    </div>
                    <div class="form-group">
                        <label for="to">To</label>
                        <%=Html.TextBox("to", ((DateTime)ViewData["toDate"]).ToShortDateString(), new { @class="datetime" }) %>
                    </div>
                    <div class="form-actions pull-right">
                        <br />
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i>&nbsp;Search</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </form>
            </div>
        </div>
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (Html.ValidationMessage("Error") == null)
           { %>
        <% if (browserStats == null || browserStats.Count() == 0)
           { %>
        <div class="alert alert-info"><%:this.GetGlobalResourceObject("General","m_NoData") %></div>
        <% }
           else
           {
               platforms = browserStats.GroupBy(x => x["Platform"].ToString()).OrderBy(x => x.Key);
               var bgrps = browserStats.GroupBy(x => new { Browser = x["Browser"].ToString(), Version = x["Version"].ToString() }).OrderBy(x => x.Key.Browser).ThenBy(x => x.Key.Version);
               var dataList = bgrps.Select(x => new { category = x.Key.Browser + "/" + x.Key.Version, value = x.Count() });
               var data = Newtonsoft.Json.JsonConvert.SerializeObject(dataList);
               platformdata = Newtonsoft.Json.JsonConvert.SerializeObject(platforms.Select(x => new { category = x.Key, value = x.Count() }));
        %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4>Browser Statistics</h4>
                <div class="pull-right">
                    <a download="preview.png" data-target="browser_chart_area" class="btn-sm btn btn-warning"
                        href="#" onclick="javascript:downloadChart(this);"
                        title="Click to download this chart as image!"><i class="fa fa-cloud-download"></i>&nbsp;Download</a>
                </div>
            </header>
            <div class="panel-body">
                <%:Html.Hidden("browser_chartData", data) %>
                <div class="row">
                    <div class="col-md-7">
                        <div id="browser_chart_area"></div>
                    </div>
                    <div class="col-md-4 pull-right">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Browser</th>
                                    <th>Version</th>
                                    <th>Hits</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% foreach (var item in bgrps)
                                   { 
                                %>
                                <tr>
                                    <td><%:item.Key.Browser %></td>
                                    <td><%:item.Key.Version%></td>
                                    <td><%:item.Count()%></td>
                                </tr>
                                <% } %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <% } %>
        <% if (platforms != null && platforms.Count() > 0)
           { %>
        <section class="panel blue">
            <header class="panel-heading">
                <h4>Platform Statistics</h4>
                <div class="pull-right">
                    <a download="preview.png" data-target="platform_chart_area" class="btn btn-sm btn-warning"
                        href="#" onclick="javascript:downloadChart(this);"
                        title="Click to download this chart as image!"><i class="fa fa-cloud-download"></i>&nbsp;Download</a>
                </div>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Platform</th>
                                    <th>Hits</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% foreach (var item in platforms)
                                   { 
                                %>
                                <tr>
                                    <td><%:item.Key%></td>
                                    <td><%:item.Count()%></td>
                                </tr>
                                <% } %>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7 pull-right">
                        <div id="platform_chart_area"></div>
                        <%:Html.Hidden("platform_chartData", platformdata) %>
                    </div>
                </div>
            </div>
        </section>
        <% } %>
        <% if (countryStats != null && countryStats.Count > 0)
           {
               var maxHits = countryStats.Max(x => x.Value.Item2);
               var minHits = maxHits * 0.2;
               var mediumHits = maxHits * 0.7;
               var bdata = new List<dynamic>();
        %>
        <section class="panel red">
            <header class="panel-heading">
                <h4>Visitor Location</h4>
                <div class="pull-right">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-sm btn-default active" id="btnMap">
                            <input type="radio" name="options" checked="checked">Map
                        </label>
                        <label class="btn btn-sm btn-default" id="btnTable">
                            <input type="radio" name="options">Table
                        </label>
                    </div>
                    <a class="btn-sm btn btn-warning" href="#" onclick="tableToExcel(this, 'tblGeoLocation','Visitor Location');" title="Click to download in excel format!"><i class="fa fa-cloud-download"></i>&nbsp;Download</a>
                </div>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12" id="geolocation-map">
                        <div id="geolocation-container" style="height: 700px; position: relative;"></div>
                    </div>
                    <div class="col-md-12" id="geolocation-table-data">
                        <table class="celloTable" id="tblGeoLocation">
                            <thead>
                                <tr>
                                    <th>Country</th>
                                    <th>IpAddress</th>
                                    <th>Hits</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
               var isoMap = new Dictionary<string, string> { 
{"AF", "AFG"},
{"AX", "ALA"},
{"AL", "ALB"},
{"DZ", "DZA"},
{"AS", "ASM"},
{"AD", "AND"},
{"AO", "AGO"},
{"AI", "AIA"},
{"AQ", "ATA"},
{"AG", "ATG"},
{"AR", "ARG"},
{"AM", "ARM"},
{"AW", "ABW"},
{"AU", "AUS"},
{"AT", "AUT"},
{"AZ", "AZE"},
{"BS", "BHS"},
{"BH", "BHR"},
{"BD", "BGD"},
{"BB", "BRB"},
{"BY", "BLR"},
{"BE", "BEL"},
{"BZ", "BLZ"},
{"BJ", "BEN"},
{"BM", "BMU"},
{"BT", "BTN"},
{"BO", "BOL"},
{"BA", "BIH"},
{"BW", "BWA"},
{"BV", "BVT"},
{"BR", "BRA"},
{"VG", "VGB"},
{"IO", "IOT"},
{"BN", "BRN"},
{"BG", "BGR"},
{"BF", "BFA"},
{"BI", "BDI"},
{"KH", "KHM"},
{"CM", "CMR"},
{"CA", "CAN"},
{"CV", "CPV"},
{"KY", "CYM"},
{"CF", "CAF"},
{"TD", "TCD"},
{"CL", "CHL"},
{"CN", "CHN"},
{"HK", "HKG"},
{"MO", "MAC"},
{"CX", "CXR"},
{"CC", "CCK"},
{"CO", "COL"},
{"KM", "COM"},
{"CG", "COG"},
{"CD", "COD"},
{"CK", "COK"},
{"CR", "CRI"},
{"CI", "CIV"},
{"HR", "HRV"},
{"CU", "CUB"},
{"CY", "CYP"},
{"CZ", "CZE"},
{"DK", "DNK"},
{"DJ", "DJI"},
{"DM", "DMA"},
{"DO", "DOM"},
{"EC", "ECU"},
{"EG", "EGY"},
{"SV", "SLV"},
{"GQ", "GNQ"},
{"ER", "ERI"},
{"EE", "EST"},
{"ET", "ETH"},
{"FK", "FLK"},
{"FO", "FRO"},
{"FJ", "FJI"},
{"FI", "FIN"},
{"FR", "FRA"},
{"GF", "GUF"},
{"PF", "PYF"},
{"TF", "ATF"},
{"GA", "GAB"},
{"GM", "GMB"},
{"GE", "GEO"},
{"DE", "DEU"},
{"GH", "GHA"},
{"GI", "GIB"},
{"GR", "GRC"},
{"GL", "GRL"},
{"GD", "GRD"},
{"GP", "GLP"},
{"GU", "GUM"},
{"GT", "GTM"},
{"GG", "GGY"},
{"GN", "GIN"},
{"GW", "GNB"},
{"GY", "GUY"},
{"HT", "HTI"},
{"HM", "HMD"},
{"VA", "VAT"},
{"HN", "HND"},
{"HU", "HUN"},
{"IS", "ISL"},
{"IN", "IND"},
{"ID", "IDN"},
{"IR", "IRN"},
{"IQ", "IRQ"},
{"IE", "IRL"},
{"IM", "IMN"},
{"IL", "ISR"},
{"IT", "ITA"},
{"JM", "JAM"},
{"JP", "JPN"},
{"JE", "JEY"},
{"JO", "JOR"},
{"KZ", "KAZ"},
{"KE", "KEN"},
{"KI", "KIR"},
{"KP", "PRK"},
{"KR", "KOR"},
{"KW", "KWT"},
{"KG", "KGZ"},
{"LA", "LAO"},
{"LV", "LVA"},
{"LB", "LBN"},
{"LS", "LSO"},
{"LR", "LBR"},
{"LY", "LBY"},
{"LI", "LIE"},
{"LT", "LTU"},
{"LU", "LUX"},
{"MK", "MKD"},
{"MG", "MDG"},
{"MW", "MWI"},
{"MY", "MYS"},
{"MV", "MDV"},
{"ML", "MLI"},
{"MT", "MLT"},
{"MH", "MHL"},
{"MQ", "MTQ"},
{"MR", "MRT"},
{"MU", "MUS"},
{"YT", "MYT"},
{"MX", "MEX"},
{"FM", "FSM"},
{"MD", "MDA"},
{"MC", "MCO"},
{"MN", "MNG"},
{"ME", "MNE"},
{"MS", "MSR"},
{"MA", "MAR"},
{"MZ", "MOZ"},
{"MM", "MMR"},
{"NA", "NAM"},
{"NR", "NRU"},
{"NP", "NPL"},
{"NL", "NLD"},
{"AN", "ANT"},
{"NC", "NCL"},
{"NZ", "NZL"},
{"NI", "NIC"},
{"NE", "NER"},
{"NG", "NGA"},
{"NU", "NIU"},
{"NF", "NFK"},
{"MP", "MNP"},
{"NO", "NOR"},
{"OM", "OMN"},
{"PK", "PAK"},
{"PW", "PLW"},
{"PS", "PSE"},
{"PA", "PAN"},
{"PG", "PNG"},
{"PY", "PRY"},
{"PE", "PER"},
{"PH", "PHL"},
{"PN", "PCN"},
{"PL", "POL"},
{"PT", "PRT"},
{"PR", "PRI"},
{"QA", "QAT"},
{"RE", "REU"},
{"RO", "ROU"},
{"RU", "RUS"},
{"RW", "RWA"},
{"BL", "BLM"},
{"SH", "SHN"},
{"KN", "KNA"},
{"LC", "LCA"},
{"MF", "MAF"},
{"PM", "SPM"},
{"VC", "VCT"},
{"WS", "WSM"},
{"SM", "SMR"},
{"ST", "STP"},
{"SA", "SAU"},
{"SN", "SEN"},
{"RS", "SRB"},
{"SC", "SYC"},
{"SL", "SLE"},
{"SG", "SGP"},
{"SK", "SVK"},
{"SI", "SVN"},
{"SB", "SLB"},
{"SO", "SOM"},
{"ZA", "ZAF"},
{"GS", "SGS"},
{"SS", "SSD"},
{"ES", "ESP"},
{"LK", "LKA"},
{"SD", "SDN"},
{"SR", "SUR"},
{"SJ", "SJM"},
{"SZ", "SWZ"},
{"SE", "SWE"},
{"CH", "CHE"},
{"SY", "SYR"},
{"TW", "TWN"},
{"TJ", "TJK"},
{"TZ", "TZA"},
{"TH", "THA"},
{"TL", "TLS"},
{"TG", "TGO"},
{"TK", "TKL"},
{"TO", "TON"},
{"TT", "TTO"},
{"TN", "TUN"},
{"TR", "TUR"},
{"TM", "TKM"},
{"TC", "TCA"},
{"TV", "TUV"},
{"UG", "UGA"},
{"UA", "UKR"},
{"AE", "ARE"},
{"GB", "GBR"},
{"US", "USA"},
{"UM", "UMI"},
{"UY", "URY"},
{"UZ", "UZB"},
{"VU", "VUT"},
{"VE", "VEN"},
{"VN", "VNM"},
{"VI", "VIR"},
{"WF", "WLF"},
{"EH", "ESH"},
{"YE", "YEM"},
{"ZM", "ZMB"},
{"ZW", "ZWE"}
               };
               var gdata = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
               var rand = new Random();
               foreach (var item in countryStats)
               {
                   var ipAddress = item.Key;
                   var isoCode = "UNKNOWN";// "IND";
                   double latitude = 0.0, longitude = 0.0;

                   if (item.Value.Item1 != null)
                   {
                       isoCode = item.Value.Item1.Country.IsoCode;

                       if (isoMap.ContainsKey(isoCode))
                           isoCode = isoMap[isoCode];

                       if (item.Value.Item1.Location != null)
                       {
                           latitude = item.Value.Item1.Location.Latitude.HasValue ? item.Value.Item1.Location.Latitude.Value : 0.0;
                           longitude = item.Value.Item1.Location.Longitude.HasValue ? item.Value.Item1.Location.Longitude.Value : 0.0;
                       }
                   }
                   else
                   {
                       // latitude = rand.Next(10, 20); //testing
                       // longitude = rand.Next(50, 100);
                   }

                   string fillKey = "HIGH";
                   int hits = item.Value.Item2;

                   if (gdata.ContainsKey(isoCode)) hits += (gdata[isoCode] as dynamic).hits;

                   if (hits > minHits && hits < mediumHits)
                   {
                       fillKey = "MEDIUM";
                   }
                   else if (hits < minHits)
                   {
                       fillKey = "LOW";
                   }

                   if (gdata.ContainsKey(isoCode))
                   {
                       gdata[isoCode] = new { hits = hits, fillKey = fillKey };
                   }
                   else
                   {
                       gdata.Add(isoCode, new { hits = hits, fillKey = fillKey });
                   }  
                                %>
                                <tr class="<%:fillKey %>">
                                    <td><%:isoCode %></td>
                                    <td><%:ipAddress%></td>
                                    <td><%:item.Value.Item2%></td>
                                </tr>
                                <% 
                   if (latitude > 0.0 && longitude > 0.0)
                   {
                       var radius = 3.5;
                       bdata.Add(new
                       {
                           name = ipAddress,
                           country = isoCode,
                           city = item.Value.Item1.City != null ? item.Value.Item1.City.Name : "NA",
                           fillKey = "bubble", //fillKey,
                           radius = radius,
                           latitude = latitude,
                           longitude = longitude,
                           ip = ipAddress,
                           hits = item.Value.Item2
                       });
                   }
               }               
                                %>
                            </tbody>
                        </table>
                    </div>
                    <% var geloc_data = Newtonsoft.Json.JsonConvert.SerializeObject(gdata);
                       var bubbles_data = Newtonsoft.Json.JsonConvert.SerializeObject(bdata); 
                    %>
                    <%:Html.Hidden("gelocation_chartData", geloc_data) %>
                    <%:Html.Hidden("bubbles_data", bubbles_data) %>
                </div>
            </div>
        </section>
        <% } %>
        <script type="text/javascript">
            $(function () {
                $('#tblGeoLocation').dataTable();

                $('#browser-stats-tenantId').click(function () {
                    var tenantId = $(this).val();
                    $('#browser-status-div').load('<%:Url.Action("BrowserStats")%>', { "tenantId": tenantId }, function (html) {
                        $('#browser-stats-tenantId').select2();
                    });
                });

                if ($('#browser_chartData').length > 0) {
                    var browser_data = JSON.parse($('#browser_chartData').val());

                    function createBrowserChart() {
                        var options = {
                            title: {
                                visible: true
                            },
                            dataSource: { data: browser_data },
                            legend: {
                                visible: true,
                                position: 'bottom'
                            },
                            seriesDefaults: {
                                type: 'pie',
                                stack: false,
                                labels: {
                                    template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                                    position: "outsideEnd",
                                    visible: true,
                                    background: "transparent",
                                    align: "column"
                                },
                                tooltip: {
                                    visible: true
                                }
                            },
                            series: [{
                                type: 'pie',
                                data: browser_data
                            }],
                            tooltip: {
                                visible: true,
                                template: "#= category # - #= kendo.format('{0:P}', percentage) #"
                            }
                        };

                        $('#browser_chart_area').celloChart(options);
                    }

                    setTimeout(function () {
                        try {
                            createBrowserChart();
                        } catch (e) { console.log(e); }
                    }, 100);
                }

                if ($('#platform_chartData').length > 0) {
                    var platform_data = JSON.parse($('#platform_chartData').val());

                    function createPlatformChart() {
                        var options = {
                            title: {
                                visible: true
                            },
                            dataSource: { data: platform_data },
                            legend: {
                                visible: true,
                                position: 'bottom'
                            },
                            seriesDefaults: {
                                type: 'pie',
                                stack: false,
                                labels: {
                                    template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                                    position: "outsideEnd",
                                    visible: true,
                                    background: "transparent",
                                    align: "column"
                                },
                                tooltip: {
                                    visible: true
                                }
                            },
                            series: [{
                                type: 'pie',
                                data: platform_data
                            }],
                            tooltip: {
                                visible: true,
                                template: "#= category # - #= kendo.format('{0:P}', percentage) #"
                            }
                        };

                        $('#platform_chart_area').celloChart(options);
                    }

                    setTimeout(function () {
                        try {
                            createPlatformChart();
                        } catch (e) { console.log(e); }
                    }, 100);
                }
            });
        </script>
        <script>
            if ($('#gelocation_chartData').length > 0) {
                var gdata = JSON.parse($('#gelocation_chartData').val());

                var map = new Datamap({
                    element: document.getElementById('geolocation-container'),
                    scope: 'world',
                    projection: 'mercator', //'equirectangular',
                    fills: {
                        HIGH: 'rgb(105, 0, 162)',
                        LOW: 'rgb(225, 177, 58)',
                        MEDIUM: 'rgb(191, 0, 108)',
                        UNKNOWN: 'rgb(0,0,0)',
                        bubble: '#0fa0fa',
                        defaultFill: '#ABDDA4',
                        //defaultFill: '#306596'  //any hex, color name or rgb/rgba value
                    },
                    data: gdata,
                    geographyConfig: {
                        hideAntarctica: true,
                        borderWidth: 1,
                        borderColor: '#FDFDFD',
                        popupTemplate: function (geo, data) {
                            var html = '<div class="hoverinfo"><strong>';
                            html += geo.properties.name;
                            html += '</strong>';
                            if (data) {
                                html += '<br/> Hits: <b>' + data.hits + '</b>';
                            }
                            html += '</div>';
                            return html;
                        }
                    }
                });

                var bdata = JSON.parse($('#bubbles_data').val());
                if (bdata && bdata.length > 0) {
                    map.bubbles(bdata, {
                        popupTemplate: function (geo, data) {
                            return '<div class="hoverinfo">City: <b>' + data.city + '</b><br/>IP: <b>' + data.ip + '</b><br/>Hits: <b>' + data.hits + '</b></div>';
                        }
                    });
                }
            }
        </script>
        <% } %>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>
    <script src="/Scripts/d3.v3.min.js"></script>
    <script src="/Scripts/topojson.v1.min.js"></script>
    <script src="/Scripts/datamaps.world.min.js"></script>
    <script>
        $(function () {
            $('#browser-stats-tenantId').select2();
            $('input.datetime').datepicker();

            $('#browser-stats-tenantId').change(function () {
                window.location = '<%:Url.Action("BrowserStats")%>' + '?tenantId=' + $(this).val();
                return false;
            });

            $('#geolocation-table-data').hide();

            $('#btnMap').click(function () {
                console.log(this);
                $('#geolocation-map').show();
                $('#geolocation-table-data').hide();
            });

            $('#btnTable').click(function () {
                console.log(this);
                $('#geolocation-map').hide();
                $('#geolocation-table-data').show();
            });
        });
    </script>
</asp:Content>
