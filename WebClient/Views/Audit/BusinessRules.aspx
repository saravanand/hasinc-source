﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.Rules.Utils.LogEntry>>" %>

<%@ Import Namespace="CelloSaaS.Rules.Utils" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container">
        <div class="heading_container">
            <h1>
                 <%:this.GetLocalResourceObject("Title") %></h1>
            <div class="inner_hold" style="float: right">
                <div class="green_but">
                    <a href="<%=Url.Action("ClearRulesEntries") %>" title="<%:this.GetGlobalResourceObject("General", "ClearEntries") %>"><span><%:this.GetGlobalResourceObject("General", "Clear") %></span>
                    </a>
                </div>
            </div>
        </div>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
           { %>
        <div class="error">
            <%=Html.CelloValidationMessage("Error", new { @class = "exception" })%>
        </div>
        <%} %>
        <%if (Model != null)
          { %>
        <table style="width: 100%">
            <tbody>
                <%  int i = 0;
                    string cssClass = "";
                    string prefix = "";
                    string prevPrefix = "";

                    string msg = "";

                    foreach (var entry in Model)
                    {

                        if (!string.IsNullOrEmpty(entry.CssClass))
                            cssClass = entry.CssClass;
                        else
                        {
                            if (i % 2 == 0)
                                cssClass = "even";
                            else
                                cssClass = "odd";
                        }

                        prefix = entry.GetMessagePrefix();

                        if (prefix != prevPrefix)
                        {
                            msg = prefix + " " + entry.GetDatePrefix();
                            prevPrefix = prefix;
                        }
                        else
                        {
                            msg = entry.GetDatePrefix();
                        }

                        msg += " " + entry.Message;
                %>
                <tr <%= "class='" + cssClass +"'" %>>
                    <td>
                        <%= msg%>
                    </td>
                </tr>
                <% i++;
                } %>
            </tbody>
        </table>
        <%} %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tr.even td
        {
            background: #e5ecf9;
        }
        
        tr.odd td
        {
            font-weight: normal;
        }
        
        .error, .warn, .info, .perf, .init
        {
            padding: 0.8em;
            margin-bottom: 1em;
            border: 2px solid #ddd;
        }
        .warn
        {
            background: #fbe3e4;
            color: #8a1f11;
            border-color: #fbc2c4;
        }
        .info
        {
            background: #fff6bf;
            color: #514721;
            border-color: #ffd324;
        }
        .perf
        {
            background: #e6efc2;
            color: #264409;
            border-color: #c6d880;
        }
        .init
        {
            background: #d5edf8;
            color: #205791;
            border-color: #92cae4;
        }
        
        .error
        {
            background: #ff0000;
            color: White;
            font-weight: bolder;
        }
    </style>
</asp:Content>
