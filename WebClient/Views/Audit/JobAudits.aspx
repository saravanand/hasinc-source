﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.EventScheduler.Model.JobAudit>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("Title") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="col-md-3 pd-0">
            <section class="panel purple">
                <header class="panel-heading">
                    <h4><%:this.GetGlobalResourceObject("General", "Filter") %></h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <% using (Html.BeginForm("JobAudits", "Audit", FormMethod.Post, new { id = "searchForm" }))
                           { %>
                        <div class="form-group">
                            <%=this.GetLocalResourceObject("lbl_Events")%>
                            <%: Html.DropDownList("eventId", (IEnumerable<SelectListItem>)ViewData["Events"], new { style = "width:100%;" })%>
                        </div>
                        <div class="form-group">
                            <%=this.GetLocalResourceObject("lbl_JobName")%>
                            <%: Html.TextBox("jobName", ViewData["jobName"])%>
                        </div>
                        <div class="form-group">
                            <%=this.GetLocalResourceObject("lbl_StartDate")%>
                            <%: Html.TextBox("startDate", "", new { @class = "datetime" })%>
                        </div>
                        <div class="form-group">
                            <%=this.GetLocalResourceObject("lbl_EndDate")%>
                            <%: Html.TextBox("endDate", "", new { @class = "datetime" })%>
                        </div>
                        <div class="form-group">
                            <%=this.GetLocalResourceObject("lbl_Executionstatus")%>
                            <select name="executionStatus" id="executionStatus" style="width: 100%">
                                <option value="">All</option>
                                <option value="true">Success</option>
                                <option value="false">Failure</option>
                            </select>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-info" href="#" onclick="$('form').submit()" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                                <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%></a>
                            <a class="btn btn-default" href="#" onclick="javascript:formReset();" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                                <%:this.GetGlobalResourceObject("General", "Reset")%></a>
                        </div>
                        <% } %>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-9">
            <div class="grid simple horizontal green">
                <div class="grid-title no-border-side">
                    <h4><%:this.GetLocalResourceObject("h_Searchresult") %></h4>
                </div>
                <div class="grid-body no-border">
                    <div id="divResult">
                        <% Html.RenderPartial("JobAuditsGrid"); %>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalExceptionDetails" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 700px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;</button>
                        <h3></h3>
                    </div>
                    <div class="modal-body" style="white-space: pre-wrap; overflow: auto;">
                    </div>
                    <div class="modal-footer">
                        <div class="button">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><%:this.GetGlobalResourceObject("General", "Close") %></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function formReset() {
            $('#searchForm')[0].reset();
            $('select').trigger('change');
            $('#searchForm').trigger('click');
            $("#startDate").datepicker("option", "maxDate", new Date());
            $("#endDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#startDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#endDate").datepicker("option", "maxDate", new Date());
        }

        $(function () {
            $('select').select2();

            $('#startDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#endDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#searchForm').submit(function () {
                if ($('#eventId').val() == '') {
                    alert('<%=this.GetLocalResourceObject("AlertEvent")%>');
                    return false;
                }

                var fromDate = new Date($('#startDate').val());
                var toDate = new Date($('#endDate').val());

                if (fromDate && toDate) {
                    if (fromDate > toDate) {
                        alert('<%=this.GetLocalResourceObject("AlertMessageForDate")%>');
                        return false;
                    }
                }

                var postdata = $(this).serialize();

                var pageSize = $('input[type=hidden][name=pager_pageSize]').val();
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                postdata += "&pageSize=" + pageSize;
                postdata += "&sortString=" + (sortString || '');
                postdata += "&sortDirection=" + (sortDirection || '');

                $.post($(this).attr('action'), postdata, function (data) {
                    $('#divResult').hide().html(data).slideDown();
                }).error(function () {
                    $('#divResult').hide().html('<div class="error"><%=this.GetLocalResourceObject("ErrorMessage")%></div>').slideDown();
                });

                return false;
            });

            $(document).on('click', '#divGrid a.view-Exception', function () {
                var Id = $(this).attr('divId');
                var message = $('#div' + Id).text();
                $('.modal-header h3').html("<%:this.GetLocalResourceObject("t_ExceptionDetails") %>  ");
                if (message == undefined || message.trim() == '') {
                    message = '<%:this.GetLocalResourceObject("e_ExceptionsNotAvailable") %>';
                }
                $('#modalExceptionDetails .modal-body').html(message);
                $('#modalExceptionDetails').modal('show');
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                var queryString = '?pageSize=' + $(this).val();
                queryString += '&sortString=' + (sortString || '');
                queryString += '&sortDirection=' + (sortDirection || '');

                queryString += '&' + $('#searchForm').serialize();

                $.post($('#searchForm').attr('action') + queryString, null, function (data) {
                    $('#divResult').hide().html(data).slideDown();
                }).error(function () {
                    $('#divResult').html('<div class="error"><%=this.GetLocalResourceObject("ErrorMessage")%>.</div>').slideDown();
                });

                $('div.success, div.error').remove();
            });
        });
    </script>
</asp:Content>
