﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.EventScheduler.Model.JobAudit>>" %>
<% Html.RenderPartial("StatusMessage"); %>
<% if (this.Model != null && this.Model.Count() > 0)
   {
       var rv = new
       {
           eventId = ViewBag.eventId,
           PageSize = ViewBag.PageSize,
           SortString = ViewBag.SortString,
           SortDirection = ViewBag.SortDirection,
           executionStatus = ViewBag.executionStatus,
           startDate = ViewBag.startDate,
           endDate = ViewBag.endDate,
           jobName = ViewBag.jobName
       };

       Html.Grid(this.Model).Columns(
        column =>
        {
            column.For(col => col.JobName).Named(this.GetLocalResourceObject("lbl_JobName").ToString());
            column.For(col => string.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0:d}", col.StartTime.ToUIDateTimeString())).Named(this.GetLocalResourceObject("lbl_StartTime").ToString());
            column.For(col => string.Format(System.Globalization.CultureInfo.CurrentUICulture, "{0}", (int)((col.EndTime - col.StartTime).TotalMilliseconds))).Named(this.GetLocalResourceObject("lbl_Duration").ToString());
            column.For(col => col.ExecutionStatus ? "<i class='fa fa-check'></i>" : "<i class='fa fa-ban'></i>").Named(this.GetLocalResourceObject("lbl_Status").ToString()).DoNotEncode();
            column.For(col => col.ExceptionDetails != null ? string.Format("<div id='divGrid'>{0}<div id='div{1}' style='display:none'>{2}</div></div>", string.Format("<a href='#' class='view-Exception red' divId='{0}' title='" + this.GetLocalResourceObject("Exception") + "'> <i class='fa fa-exclamation-circle' style='color:red'></i>" + this.GetLocalResourceObject("Exception") + " </a>", col.JobAuditId), col.JobAuditId, col.ExceptionDetails) : "--").Attributes(@style => "width:150px;text-wrap:wrap;").HeaderAttributes(@style => "width:150px;").Named(this.GetLocalResourceObject("lbl_Executionstatus").ToString()).DoNotEncode();
        }).Attributes(id => "jobAuditTable", @class => "celloTable").Render();

       Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }, rv)
               .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
               .Render();
   }
   else
   { %>
<div class="alert alert-info">
    <%:this.GetGlobalResourceObject("General", "m_NoData") %>
</div>
<% } %>