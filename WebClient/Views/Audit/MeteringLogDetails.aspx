﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="MvcContrib.Pagination" %>
<%@ Import Namespace="CelloSaaS.View" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("UsageDetails") %>" title="<%:this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%:this.GetLocalResourceObject("lbl_Header")%> -
            <span class="semi-bold"><%=ViewData["UsageCode"] %>  <%:this.GetLocalResourceObject("lbl_Usage") %></span>

        </h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetGlobalResourceObject("General", "Filter") %></h4>
            </header>
            <div class="panel-body">
                <% using (Html.BeginForm("MeteringLogDetails", "Audit", FormMethod.Post, new { id = "searchForm" }))
                   {
                %>
                <div class="row form-container">
                    <div class="col-md-6">
                        <div class="search_holder">
                            <%= Html.Hidden("TenantId", ViewData["TenantId"])%>
                            <%= Html.Hidden("UsageCode", ViewData["UsageCode"])%>
                            <%:this.GetLocalResourceObject("From")%>
                            <%= Html.TextBox("StartDate", "", new { placeholder=Html.JQueryDateFormat(), title = this.GetLocalResourceObject("FromPlaceHolder"), @class = "datetime" })%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="search_holder">
                            <%:this.GetLocalResourceObject("To")%>
                            <%= Html.TextBox("EndDate", "", new { placeholder=Html.JQueryDateFormat(), title = this.GetLocalResourceObject("ToPlaceHolder"), @class = "datetime" })%>
                        </div>
                    </div>
                    <div class="col-md-6 pull-right">
                        <div class=" pull-right">
                            <br />
                            <a class="btn btn-info" href="#" onclick="$('form').submit()" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                                <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%></a>
                            <a class="btn btn-default" href="#" onclick="javascript:formReset();" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                                <%:this.GetGlobalResourceObject("General", "Reset")%></a>
                        </div>
                    </div>
                </div>
                <% } %>
            </div>
        </section>
        <br />
        <div class="grid simple">
            <div class="grid-body no-border pdm-0">
                <div id="divResult">
                    <% Html.RenderPartial("MeteringLogGrid"); %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cnt2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#StartDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
             });

            $('#EndDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#searchForm')[0].reset();

            $('#searchForm').submit(function () {
                var fromDate = new Date($('#StartDate').val());
                var toDate = new Date($('#EndDate').val());

                if (fromDate && toDate) {
                    if (fromDate > toDate) {
                        alert('<%: this.GetLocalResourceObject("e_FromDate")%>');
                        return false;
                    }
                }

                var postdata = $(this).serialize();

                var pageSize = $('input[type=hidden][name=pager_pageSize]').val();
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                postdata += "&pageSize=" + pageSize;
                postdata += "&sortString=" + (sortString || '');
                postdata += "&sortDirection=" + (sortDirection || '');

                $.post($(this).attr('action'), postdata, function (data) {
                    $('#divResult').hide().html(data).slideDown();
                }).error(function () {
                    $('#divResult').hide().html('<div class="alert alert-danger"><%: this.GetLocalResourceObject("e_Search")%></div>').slideDown();
                });

                return false;
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                var queryString = '?pageSize=' + $(this).val();
                queryString += '&sortString=' + (sortString || '');
                queryString += '&sortDirection=' + (sortDirection || '');

                queryString += '&' + $('#searchForm').serialize();

                $.post('<%=Url.Action("MeteringLogDetails")%>' + queryString, null, function (data) {
                    $('#divResult').fadeOut().html(data).slideDown();
                }).error(function () {
                    $('#divResult').html('<div class="error"><%: this.GetLocalResourceObject("e_FetchMeteringLog")%></div>').fadeIn();
                });

                $('div.alert').remove();
            });
        });

        function formReset() {
            $('#searchForm')[0].reset();
            $("#StartDate").datepicker("option", "maxDate", new Date());
            $("#EndDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#StartDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#EndDate").datepicker("option", "maxDate", new Date());
        }
    </script>
</asp:Content>
