﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<% Html.RenderPartial("StatusMessage"); %>
<%  bool test = Convert.ToBoolean(this.Request["test"] ?? "False");
    MeteringSearchCondition condition = (ViewBag.Condition as MeteringSearchCondition) ?? new MeteringSearchCondition();

    string sortDirection = (string)ViewData["SortDirection"];
    string sortString = (string)ViewData["SortString"];
    int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
    int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
    int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;

    var routeValues = new
        {
            UsageCode = condition.UsageCode,
            TenantId = condition.TenantId,
            StartDate = condition.StartDate,
            EndDate = condition.EndDate
        };

    var loglist = ViewData["UsageDetailsList"] as IEnumerable<MeteringLog>;
    var usageAmounts = ViewData["TotalUsageAmount"] as Dictionary<string, double> ?? new Dictionary<string, double>();

    if (loglist != null && loglist.Count() > 0)
    {
       var userList = ViewBag.UserList as Dictionary<string, string> ?? new Dictionary<string, string>();
%>
<% if (condition.StartDate != DateTime.MinValue && condition.EndDate != DateTime.MinValue)
   { %>
<div class="content-box">
    Total Usage Amount during
    <%=string.Format(" {0} - {1}",condition.StartDate.ToUIDateString(),condition.EndDate.ToUIDateString()) %>
    : <strong>
        <%:usageAmounts.ContainsKey(condition.UsageCode) ? usageAmounts[condition.UsageCode].ToString("F2") : "0.0"%></strong>
</div>
<% } %>
<%
        
   Html.Grid(loglist).Columns(col =>
   {
       col.For(x => !string.IsNullOrEmpty(x.LoggedBy) && userList.ContainsKey(x.LoggedBy)
           ? userList[x.LoggedBy]
           : x.LoggedBy).Named(this.GetLocalResourceObject("lbl_UserName").ToString()).DoNotEncode();
       col.For(x => x.LoggedDate.ToUIDateTimeString()).Named(this.GetLocalResourceObject("lbl_LoggedDate").ToString());
       col.For(x => x.Amount).Named(this.GetLocalResourceObject("lbl_UsageAmount").ToString()).Attributes(style => "text-align:right;padding-right:10px;").HeaderAttributes(style => "text-align:right;");
       col.For(x => string.Format("<a href='{0}' title='"+this.GetLocalResourceObject("t_Deletethislogentry").ToString()+"'><i class='fa fa-trash-o'></i></a>", Url.Action("DeleteMeterLog", "Billing", new { id = x.Id })))
           .Named(this.GetGlobalResourceObject("General", "Delete").ToString()).Attributes(@class => "halign").HeaderAttributes(@class => "halign").DoNotEncode().Visible(test);
   }).Attributes(id => "dataList", @class => "celloTable").Render();

   Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }, routeValues)
       .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
       .Render();
    }
    else
    { %>
<div class="alert alert-info">
    <%:this.GetGlobalResourceObject("General","m_NoData") %>
</div>
<% } %>
