﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.Model.ProductAnalytics.ProductAnalytics>>" %>
<%@ Import Namespace="CelloSaaS.Model.ProductAnalytics" %>

<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    string sortDirection = (string)ViewData["SortDirection"];
    string sortString = (string)ViewData["SortString"];
    var search = ViewData["search"] != null ? ViewData["search"] as ProductAnalyticsSearchCondition : new ProductAnalyticsSearchCondition();

    int pageNumber = 0, totalCount = 0, pageSize = 0;
    decimal averageResponseTime = 0;
    decimal averagePageSize = 0;


    if (ViewData.ContainsKey("PageNumber") && ViewData["PageNumber"] != null)
    {
        pageNumber = (int)ViewData["PageNumber"];
    }

    if (ViewData.ContainsKey("TotalCount") && ViewData["TotalCount"] != null)
    {
        totalCount = (int)ViewData["TotalCount"];
    }

    if (ViewData.ContainsKey("PageSize") && ViewData["PageSize"] != null)
    {
        pageSize = (int)ViewData["PageSize"];
    }
%>
<% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
   { %>
<div class="alert alert-danger">
    <%=Html.CelloValidationMessage("Error", new { @class = "exception" })%>
</div>
<%} %>
<% if (this.Model != null && this.Model.Count() > 0)
   { %>
<div class="row">
    <div class="col-md-3">
        <div class="tiles orange">
            <div class="tiles-body">
                <div class="tiles-title"><%:this.GetLocalResourceObject("TotalHits") %></div>
                <div class="heading">
                    <span class="animate-number" data-animation-duration="1200" data-value="<%= totalCount%>">
                        <%= totalCount %></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tiles red">
            <div class="tiles-body">
                <div class="tiles-title"><%:this.GetLocalResourceObject("AverageResponseTime") %></div>
                <div class="heading">
                    <span class="animate-number" data-animation-duration="1200" data-value="<%= search.AvgResponseTime.ToString("0.##") %>">
                        <%= search.AvgResponseTime.ToString("0.##")  %></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="tiles purple">
            <div class="tiles-body">
                <div class="tiles-title"><%:this.GetLocalResourceObject("AveragePageSize") %></div>
                <div class="heading">
                    <span class="animate-number" data-animation-duration="1200" data-value="<%= search.AvgPageSize.ToString("0.##")%>">
                        <%= search.AvgPageSize.ToString("0.##") %></span>
                </div>
            </div>
        </div>
    </div>
</div>
<br />
<div class="grid simple horizontal red">
    <div class="grid-body no-border pdm-0">
        <div style="overflow: auto;">
            <%
       Html.Grid(this.Model).Columns(
           column =>
           {
               column.For(c => string.Format("<div class='dropdown btn action-button'><a class='dropdown-toggle' role='button' data-toggle='dropdown'><span class='fa-stack'><i class='fa fa-square-o fa-stack-2x'></i><i class='fa fa-caret-down fa-stack-1x'></i></span></a><ul class='dropdown-menu' role='menu' style='z-index:1000;text-align: left;'><li>{0}</li><div id='divid{2}' style='display:none'>{1}</div> <li>{3}</li><div id='div{2}' style='display:none'>{4}</div></ul></div>", string.Format("<a href='#' class='view-browserDetails red' divId='{0}' title='" + this.GetLocalResourceObject("BrowserDetails") + "'> <i class='fa fa-desktop'></i>  " + this.GetLocalResourceObject("BrowserDetails") + "</a>", c.Id), HttpUtility.HtmlEncode(c.BrowserDetails), c.Id, string.Format("<a href='#' class='view-Exception red' divId='{0}' title='" + this.GetLocalResourceObject("Exception") + "'> <i class='fa fa-warning-circle'></i>   " + this.GetLocalResourceObject("Exception") + " </a>", c.Id), c.ExceptionDetails))
                   .Named(this.GetLocalResourceObject("Action").ToString())
                   .HeaderAttributes(@class => "text-center")
                   .Attributes(@class => "text-center")
                   .DoNotEncode();
               column.For(col => col.TenantName).Named(this.GetLocalResourceObject("TenantName").ToString()).Visible(search.TenantId == "-1");
               column.For(col => col.UserName).Named(this.GetLocalResourceObject("UserName").ToString()).Visible(search.TenantId == "-1");
               //column.For(col => string.Format("{0}/ {1}", col.ControllerName, col.ActionName)).Named(this.GetLocalResourceObject("Action").ToString());
               column.For(col => col.TimeStamp.ToUIDateTimeString()).Named(this.GetLocalResourceObject("TimeStamp").ToString());
               column.For(col => HttpUtility.UrlDecode(col.CurrentUrl).Replace("/", "/ ").Replace("&", "& ").Replace("?", "? "))
                   .Named(this.GetLocalResourceObject("Url").ToString()).DoNotEncode()
                   .Attributes(@style => "width:100px");
               column.For(col => col.IPAddress).Named(this.GetLocalResourceObject("IPAddress").ToString());
               column.For(col => (col.ResponseLength / 1024)).Named(this.GetLocalResourceObject("PageSize").ToString())
                   .HeaderAttributes(@class => "text-right")
                   .Attributes(@class => "text-right"); 
               column.For(col => col.CreatedOn.Subtract(col.TimeStamp).TotalSeconds).Named(this.GetLocalResourceObject("ResponseTime").ToString())
                   .HeaderAttributes(@class => "text-right")
                   .Attributes(@class => "text-right"); 
               column.For(col => col.ResponseStatusCode == 404 || col.ResponseStatusCode == 500 || !string.IsNullOrEmpty(col.ExceptionDetails) ? "<i class='fa fa-ban'></i>" : "<i class='fa fa-check'></i>")
                   .Named(this.GetLocalResourceObject("Status").ToString()).DoNotEncode()
                   .HeaderAttributes(@class => "text-center")
                   .Attributes(@class => "text-center");
           }).Attributes(id => "dataList", @class => "celloTable").Render();
           
       var rval = new { TenantId = search.TenantId, FromDate = search.FromDate, ToDate = search.ToDate, ResponseTime = search.ResponseTime, ResponseLength = search.ResponseLength, HasError = search.HasError, searchField = this.Request["searchField"], searchValue = this.Request["searchValue"] };
       Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementId = "loading", LoadingElementDuration = 500 }, rval)
           .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
           .Render();
            %>
        </div>
    </div>
</div>
<% }
   else
   { %>
<div class="alert alert-info">
    <%:this.GetGlobalResourceObject("General","m_NoData") %>
</div>
<% } %>
