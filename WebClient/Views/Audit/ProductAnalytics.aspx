﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.Model.ProductAnalytics.ProductAnalytics>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("Title") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row-fluid">
            <% Html.RenderPartial("ProductUsageChart"); %>
        </div>
        <% if (this.Model != null && this.Model.Count() > 0)
           { %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetGlobalResourceObject("General", "Filter") %></h4>
            </header>
            <div class="panel-body">
                <form id="frmSearch" name="frmSearch" method="post" action="<%=Url.Action("ProductAnalytics") %>">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("TenantId") %>
                                    <%= Html.DropDownList("TenantId", (IEnumerable<SelectListItem>)ViewData["TenantList"], new { style = "width:100%;" })%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("HasError") %>
                                    <br />
                                    <%=Html.CheckBox("HasError", false)%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("FromDate") %>
                                    <%=Html.TextBox("FromDate", "", new { @class = "datetime" })%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("ToDate") %>
                                    <%=Html.TextBox("ToDate","", new { @class = "datetime" })%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("ResponseTime") %>
                                    <%=Html.TextBox("ResponseTime", "", new { style = "width:100%;", @maxlength = "5"})%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("ResponseLength") %>
                                    <%=Html.TextBox("ResponseLength", "", new { style = "width:100%;", @maxlength = "5"})%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <a class="btn btn-info" href="#" id="searchButton" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                                        <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%></a>
                                    <a class="btn btn-default" href="#" id="btnReset" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                                        <%:this.GetGlobalResourceObject("General", "Reset")%></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <% } %>
        <div id="divGrid">
            <% Html.RenderPartial("PartialProductAnalytics", this.Model);  %>
        </div>
    </div>
    <div class="modal fade" id="modalBrowserDetails" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 700px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h3></h3>
                </div>
                <div class="modal-body" style="white-space: pre-wrap; overflow: auto;">
                </div>
                <div class="modal-footer">
                    <div class="button">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%:this.GetGlobalResourceObject("General", "Close") %></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#FromDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#ToDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#ResponseTime').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });

            $('#ResponseLength').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });

            $('#TenantId').select2();

            $(document).on('change', 'select[name=pageSize]', function () {
                var searchField = $('#searchField option:selected').val();
                var searchValue = $('#searchValue').val();
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                sortString = sortString == undefined ? '' : sortString;
                sortDirection = sortDirection == undefined ? '' : sortDirection;

                var postdata = $('#frmSearch').serialize();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                postdata += '&searchField=' + searchField;
                postdata += '&searchValue=' + searchValue;
                postdata += '&sortString=' + sortString;
                postdata += '&sortDirection=' + sortDirection;

                $.post('<%=Url.Action("ProductAnalytics","Audit") %>', postdata, function (data) {
                    $('#divGrid').html(data);
                });
            });

            $('#searchButton').click(function () {
                var searchField = $('#searchField option:selected').val();
                var searchValue = $('#searchValue').val();
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                sortString = sortString == undefined ? '' : sortString;
                sortDirection = sortDirection == undefined ? '' : sortDirection;

                var postdata = $('#frmSearch').serialize();
                postdata += '&page=' + $('input[type=hidden][name=pager_pageNumber]').val();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                postdata += '&searchField=' + searchField;
                postdata += '&searchValue=' + searchValue;
                postdata += '&sortString=' + sortString;
                postdata += '&sortDirection=' + sortDirection;

                $.post('<%=Url.Action("ProductAnalytics","Audit") %>', postdata, function (data) {
                    $('#divGrid').html(data);
                });
                return false;
            });

            $('#btnReset').click(function () {
                $('#frmSearch')[0].reset();
                $('#TenantId').trigger('change');
                $('#searchButton').trigger('click');
            });

            $(document).on('click', '#divGrid a.view-Exception', function () {
                var Id = $(this).attr('divId');
                var message = $('#div' + Id).text();
                $('.modal-header h3').html("<%:this.GetLocalResourceObject("t_ExceptionDetails") %>  ");
                if (message == undefined || message.trim() == '') {
                    message = '<%:this.GetLocalResourceObject("e_ExceptionsNotAvailable") %>';
                }
                $('#modalBrowserDetails .modal-body').html(message);
                $('#modalBrowserDetails').modal('show');
            });

            $(document).on('click', '#divGrid a.view-browserDetails', function () {
                var Id = $(this).attr('divId');
                var message = $('#divid' + Id).text();
                $('.modal-header h3').html("<%:this.GetLocalResourceObject("t_BrowserDetails") %>  ");
                $('#modalBrowserDetails .modal-body').html(message);
                $('#modalBrowserDetails').modal('show');
            });

            $('.dropdown-toggle').dropdown();
        });
    </script>
    <style type="text/css">
        .lbrk {
            white-space: pre; /* CSS 2.0 */
            white-space: pre-wrap; /* CSS 2.1 */
            white-space: pre-line; /* CSS 3.0 */
            white-space: -pre-wrap; /* Opera 4-6 */
            white-space: -o-pre-wrap; /* Opera 7 */
            white-space: -moz-pre-wrap; /* Mozilla */
            white-space: -hp-pre-wrap; /* HP Printers */
            word-wrap: break-word; /* IE 5+ */
            width: 350px;
            float: left;
        }
    </style>    
    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>
</asp:Content>
