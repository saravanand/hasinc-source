﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    int interval = 15;
    if (ViewBag.pu_interval != null)
    {
        interval = (int)ViewBag.pu_interval;
    }
%>
<div class="grid simple horizontal purple">
    <div class="grid-title no-border">
        <div class="row">
            <div class="col-md-5">
                <h4>Application Load</h4>
                <a download="appload.png" data-target="pusg_chart_area"
                    href="#" onclick="javascript:downloadChart(this);"
                    title="Click to download this chart as image!"><i class="fa fa-cloud-download"></i></a>
            </div>
            <div class="col-md-7">
                <div class="pull-right actions">
                    From:<%=Html.TextBox("pu_from_date", "", new { @class="datetime", style="width:75px;" })%>
                    To:<%=Html.TextBox("pu_to_date", "", new { @class="datetime", style="width:75px;" }) %>
                    <%=Html.Hidden("pu_interval", interval) %>
                    <button id="btn_pu_search" class="btn btn-xs btn-default" type="button"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body no-border" style="min-height: 380px; padding: 0px;">
        <div id="pusg_chart_area"></div>
        <script>
            $(function () {
                var dateFormat = '<%=Html.JQueryDateFormat()%>';
                $('input#pu_from_date,input#pu_to_date').datepicker({ "format": dateFormat });
                $('button#btn_pu_search').click(createChart);

                function createChart() {
                    
                    var fromDate = $('input#pu_from_date').val();
                    var toDate = $('input#pu_to_date').val();
                    var interval = $('input#pu_interval').val();
                    $('#pusg_chart_area').empty();

                    $.post('<%=Url.Action("ProductUsageData", "Audit")%>', { "fromDate": fromDate, "toDate": toDate, "interval": interval }, function (stats) {

                        if (!stats || !stats.data || stats.data.length == 0) {
                            $('#pusg_chart_area').html('<div class="alert alert-info">No data available!</div>');
                            return;
                        }

                        $('input#pu_from_date').val(stats.fromDate);
                        $('input#pu_to_date').val(stats.toDate);
                        $('input#pu_interval').val(stats.interval);

                        var title_text = 'Application Load between ' + stats.fromDate + ' - ' + stats.toDate;

                        var sdata = stats.data;

                        var step = sdata.length < 8 ? 1 : parseInt(sdata.length / 8);

                        var options = {
                            title: {
                                text: title_text,
                                visible: true
                            },
                            dataSource: { data: sdata },
                            legend: {
                                visible: true,
                                position: 'bottom'
                            },
                            series: [{
                                type: "line",
                                style: "smooth",
                                field: "value",
                                categoryField: "date",
                                markers: {
                                    visible: sdata.length <= 2
                                },
                                border: {
                                    width: 1  
                                },
                                margin: 0,
                                width: 2,
                            }],
                            categoryAxis: {
                                field: "date",
                                //baseUnit: "days",
                                labels: {
                                    rotation: -60,
                                    step: step,
                                },
                                majorGridLines: {
                                    visible: false
                                },
                                title: {
                                    text: "Date"
                                },
                            },
                            valueAxis: {
                                title: {
                                    text: "Hits"
                                },
                                majorGridLines: {
                                    visible: true,
                                    width: 1,
                                    dashType: "dash",
                                }
                            },
                            tooltip: {
                                visible: true,
                                //template: "#= dataItem.date # - #= dataItem.value # hits"
                                template: "#= dataItem.value # hits"
                            }
                        };

                        if ($('#pusg_chart_area').data('celloChart')) $('#pusg_chart_area').data('celloChart').destroy();
                        $('#pusg_chart_area').celloChart(options);

                        setTimeout(createChart, 1 * 60 * 1000); // every 10 minute

                    }, 'json').error(function (msg) {
                        console.log(msg);
                        $('#pusg_chart_area').html('<div class="alert alert-danger">Error while loading data. Try again later!</div>');
                        setTimeout(createChart, 1 * 60 * 1000); // every 10 minute
                    });
                }

                createChart();
            });
        </script>
    </div>
</div>
