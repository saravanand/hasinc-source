﻿<%@ Page Title="<%$Resources:UsageAuditsTitle%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("UsageAuditsTitle")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-body no-border pdm-0">
                <% if (ViewData["UsageDetailsList"] != null && ((IEnumerable<CelloSaaS.Model.LicenseManagement.UsageDetail>)ViewData["UsageDetailsList"]).Count() > 0)
                   {
                       Dictionary<string, double?> totalUsageList = null;
                       if (ViewBag.TotalUsageList != null)
                       {
                           totalUsageList = ViewBag.TotalUsageList as Dictionary<string, double?>;
                       }
                       Html.Grid(((IEnumerable<CelloSaaS.Model.LicenseManagement.UsageDetail>)ViewData["UsageDetailsList"])).Columns(col =>
                        {
                            col.For(x => x.UsageName).Named(this.GetLocalResourceObject("lbl_UsageName").ToString());
                            col.For(x => x.ModuleCode).Named(this.GetLocalResourceObject("lbl_Module").ToString());
                            col.For(x => x.Amount).Named(this.GetLocalResourceObject("lbl_CurrentAmount").ToString()).Attributes(style => "text-align:right;").HeaderAttributes(style => "text-align:right;");
                            col.For(x => (totalUsageList != null && totalUsageList.ContainsKey(x.UsageCode))
                                            ? ((totalUsageList[x.UsageCode].HasValue && totalUsageList[x.UsageCode].Value > 0) ? (totalUsageList[x.UsageCode].Value - x.Amount).ToString() : "Unlimited") : "NA").Named(this.GetLocalResourceObject("lbl_RemainingUsage").ToString()).Attributes(style => "text-align:right;").HeaderAttributes(style => "text-align:right;");
                            col.For(x => "<a id='MeteringLog' href='MeteringLogDetails?tenantId=" + x.TenantId + "&usageCode=" + x.UsageCode + "' title='" + string.Format(this.GetLocalResourceObject("lbl_ViewDetails").ToString(), x.UsageName) + "'><i class='fa fa-search'></i></a>").Named(this.GetLocalResourceObject("lbl_ViewDetails").ToString()).Attributes(@class => "halign", style => "width:130px;").HeaderAttributes(@class => "halign").DoNotEncode();
                        }).Attributes(id => "dataList", @class => "celloTable").Render();
                   }
                   else
                   { %>
                <div class="alert alert-info">
                    <%:this.GetGlobalResourceObject("General","m_NoData") %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
