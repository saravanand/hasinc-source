﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    var model = ViewBag.Stats as Dictionary<string, Dictionary<DateTime, int>>;
    var userNames = ViewBag.UserNames as Dictionary<string, string> ?? new Dictionary<string, string>();
    var tenantlist = ViewBag.TenantSelectList as List<SelectListItem> ?? new List<SelectListItem>();
    tenantlist.RemoveAll(x => string.IsNullOrEmpty(x.Value)); // remove all option
%>
<section class="panel red">
    <header class="panel-heading">
        <h4><%:this.GetLocalResourceObject("h_UserUsageStatistics") %></h4>
        <% if (tenantlist.Count > 0)
           { %>
        <div class="pull-right">
            <%=Html.DropDownList("tenantId", tenantlist, new { id="user-usage-tenantId", style="width:180px;" })%>
            <a class="btn-sm btn btn-warning" href="#" onclick="tableToExcel(this, 'tblUserUsageStatistics','Tenant Usage Statistics');" title="Click to download in excel format!"><i class="fa fa-cloud-download"></i>&nbsp;Download</a>
        </div>
        <% } %>
    </header>
    <div class="panel-body" style="position: relative;">
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (model == null || model.Count == 0)
           { %>
        <div class="alert alert-info"><%:this.GetGlobalResourceObject("General","m_NoData") %></div>
        <% }
           else
           {
               var dataList = model.Select(x => new { labels = userNames.ContainsKey(x.Key) ? userNames[x.Key] : x.Key, value = x.Value.Sum(c => c.Value) });
               var data = Newtonsoft.Json.JsonConvert.SerializeObject(dataList);
        %>
        <input type="hidden" value='<%=data%>' id="usr_data" />
        <div class="row">
            <div class="col-md-3">
                <div style="height: 380px; overflow-y: auto;" id="user-usage-text-data">
                    <table class="table table-condensed" id="tblUserUsageStatistics">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th class="text-right">Total Hits</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in dataList.OrderByDescending(x => x.value))
                               { %>
                            <tr>
                                <td><%=item.labels %></td>
                                <td class="text-right"><%=item.value%></td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-9">
                <div id="user-usage-stats-content">
                </div>
                <div id="user-usage-tooltip" class="tooltip"></div>
            </div>
        </div>
        <script type="text/javascript">
            var UserUsageGraph = function () {

                // version 0.0.4
                var Eventer = function () { if (!(this instanceof Eventer)) return new Eventer; this.publish = function (c, d) { topics = b(c), topics.forEach(function (b) { "object" == typeof a[b] && a[b].forEach(function (a) { a.apply(this, d || []) }) }) }, this.subscribe = function (b, c) { var d = [].concat(c); return d.forEach(function (c) { a[b] || (a[b] = []), a[b].push(c) }), [b, c] }, this.unsubscribe = function (b, c) { a[b] && a[b].forEach(function (d, e) { d == c && a[b].splice(e, 1) }) }, this.queue = function () { return a }, this.on = this.subscribe, this.off = this.unsubscribe, this.trigger = this.publish; var a = {}, b = function (a) { return "string" == typeof a ? a.split(" ") : a }; return this };

                var eventer = new Eventer;
                var margin = 10;
                var width = 700 - margin * 2,
                    height = 220 * 2 - margin * 2;

                // Setup
                var svg = d3.select('#user-usage-stats-content')
                    .append('svg')
                        .attr('width', width + margin)
                        .attr('height', height + margin)
                        .append('g')
                            .attr('transform', 'translate(' + [margin / 2, margin / 2].join(',') + ')')

                svg = d3.select('#user-usage-stats-content svg g')

                // Guts        
                var self = this;
                this.e = new Eventer;

                this.init = function () {
                    this.e.subscribe('load', [this.getData, this.listen]);
                    this.e.subscribe('load:data', [this.canvas.setup]);
                    this.e.subscribe('draw', [this.canvas.draw]);

                    this.e.publish('load');
                };

                this.listen = function () {

                };

                this.getData = function () {
                    self.data = JSON.parse($('#usr_data').val());
                    self.e.publish('load:data', [self.data]);
                };

                this.canvas = {

                    setup: function (data) {
                        self.size = d3.max(data, function (d) { return d.value });

                        self.force = d3.layout.force()
                            .nodes(data);

                        self.colors = {
                            medium: d3.rgb(191, 0, 108),
                            dark: d3.rgb(105, 0, 162),
                            light: d3.rgb(225, 177, 58)
                        };

                        self.e.publish('draw', [data]);
                    },

                    draw: function (data, filter) {

                        self.force
                            .nodes(data)
                            .charge(function (d) { return -d.value / 2 })
                            .size([width, height / 2])
                            .on('tick', function (e) {

                                var center = {
                                    x: width / 2,
                                    y: height / 3
                                },
                                    centerx;

                                self.data.forEach(function (o, i) {

                                    if ((filter || {}).value) {
                                        // Move sides apart
                                        if (filter.years) {
                                            centerx = self.years.indexOf(o.date.getFullYear().toString()) / (self.years.length - 2.7);
                                        } else if (filter_test.call(filter, o)) {
                                            centerx = 1 / 3;
                                        } else {
                                            centerx = 1.6;
                                        }
                                    } else {
                                        // Move back together
                                        centerx = 1;
                                    }
                                    o.x += (center.x * centerx - o.x) * e.alpha * 0.04;
                                    o.y += (center.y - o.y) * e.alpha * 0.04;
                                });

                                d3.selectAll('.circle')
                                    .attr("cx", function (d) { return d.x; })
                                    .attr("cy", function (d) { return d.y; });
                            })
                            .start();

                        var circles = svg.selectAll('.circle')
                            .data(data)

                        circles.enter()
                            .append('circle')
                            .attr('class', 'circle')
                            .attr('r', 0)
                            .attr('fill', function (d) { return color(d) })
                            .call(self.force.drag)

                        circles
                            .transition()
                            .duration(1100)
                            .attr('fill', function (d) {
                                return color(d)
                            })
                            .attr('r', function (d) { return d.value / self.size * 30 + 2 })

                        circles.on('mouseenter', function (d) {
                            d3.select(this)
                                .attr('class', 'circle active')

                            d3.select('#user-usage-tooltip')
                                .attr('class', 'tooltip active')
                                .html([
                                    '<p class="labels">', d.labels, '</p>',
                                    '<p class="count single">', d.value, ' <i>hit', (d.value > 1 ? 's' : ''), '</i></p>'
                                ].join(''))
                                .attr('style', [
                                    'left:', (d.x + 50), 'px;',
                                    'top:', (d.y - 90), 'px;',
                                ].join(''))
                        });

                        circles.on('mouseleave', function (d) {

                            d3.select('#user-usage-tooltip')
                                .attr('class', 'tooltip')
                                .attr('style', '');

                            d3.selectAll('.circle.active')
                                .attr('class', 'circle')
                        });
                    }
                };

                var color = function (d) {
                    var val = parseInt(d.value);
                    var max = self.size;
                    var min = parseInt(max * 0.2);
                    var medium = parseInt(max * 0.7);
                    if (val < min) {
                        return self.colors.light;
                    } else if (val >= min && val < medium) {
                        return self.colors.medium;
                    } else if (val >= medium) {
                        return self.colors.dark;
                    }
                    return self.colors.light;
                };

                this.init.apply(this, arguments);
            };

            var usrgraph = new UserUsageGraph;
        </script>
        <% } %>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#user-usage-tenantId').click(function () {
                var tenantId = $(this).val();
                var fromDate = $('input#from').val();
                var toDate = $('input#to').val();
                $('#user-usage-stats-div').load('<%:Url.Action("UserUsageStats")%>', { "tenantId": tenantId, "from": fromDate, "to": toDate }, function (html) {
                    $('#user-usage-tenantId').select2();
                });
            });
        });
    </script>
</section>
