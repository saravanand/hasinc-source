﻿<%@ Page Title="<%$ Resources:t_PayPalAccountActivated %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center w70 form-container">
        <div class="heading"><i class="fa fa-check"></i>&nbsp;<%: this.GetLocalResourceObject("s_PreapprovalActivated") %> </div>
        <div class="form-content">
            <% Html.RenderPartial("StatusMessage"); %>
            <% if (ViewBag.Success != null && (bool)ViewBag.Success)
               { %>
            <p style="font-size: 20px; padding: 0 0 20px 0;">
                 <%: this.GetLocalResourceObject("s_ActivatePayPal") %> 
            </p>
            <% } %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
