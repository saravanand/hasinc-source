﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Billing.ServiceProxies" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>
<%
    var curInvoice = ViewData["CurrentInvoice"] as Invoice;
    var invoiceResult = ViewData["InvoiceSearchResult"] as InvoiceSearchResult;
    var invoiceList = invoiceResult != null && invoiceResult.Items != null ? invoiceResult.Items : new Dictionary<Guid, Invoice>();

    if (curInvoice != null && !invoiceList.ContainsKey(curInvoice.Id))
    {
        invoiceList.Add(curInvoice.Id, curInvoice);
    }

    var yearInvoices = new Dictionary<int, IEnumerable<dynamic>>();
    string[] monthNames = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames;

    if (invoiceList.Count > 0)
    {
        var minyear = invoiceList.Values.Min(x => x.InvoiceDate.Year);
        var maxyear = invoiceList.Values.Max(x => x.InvoiceDate.Year);

        for (int year = minyear; year <= maxyear; ++year)
        {
            var lst = new List<dynamic>();

            foreach (var month in monthNames)
            {
                if (string.IsNullOrWhiteSpace(month)) continue;
                var i = invoiceList.Values.Where(x => x.InvoiceType == InvoiceType.Regular && x.InvoiceDate.Year == year
                                     && CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(x.InvoiceDate.Month) == month)
                                     .Select(x => new
                                     {
                                         Date = x.EndDate.ToUIDateString(),
                                         Id = x.Id,
                                         InvoiceNo = x.InvoiceNo,
                                         Status = x.InvoiceStatus.ToString(),
                                         Amount = x.Amount,
                                         PaidAmount = x.PaidAmount
                                     }).FirstOrDefault();
                lst.Add(i);
            }

            yearInvoices.Add(year, lst);
            yearInvoices[year] = lst;
        }
    }

    string yearSeriesJsonData = Newtonsoft.Json.JsonConvert.SerializeObject(yearInvoices.Values, Newtonsoft.Json.Formatting.None);
    string yearCategoryJsonData = Newtonsoft.Json.JsonConvert.SerializeObject(yearInvoices.Keys, Newtonsoft.Json.Formatting.None);
    string monthNamesJsonData = Newtonsoft.Json.JsonConvert.SerializeObject(monthNames, Newtonsoft.Json.Formatting.None);
%>
<div id="chartConfiguration">
    <section class="panel indigo">
        <header class="panel-heading">
            <h4><%: this.GetLocalResourceObject("BillReports")%></h4>
            <div class="pull-right">
                <%:Html.Label(this.GetLocalResourceObject("lbl_Type").ToString())%>
                <select name="chartType" style="width: 100px;">
                    <option value="line"><%: this.GetLocalResourceObject("lbl_Line")%></option>
                    <option value="column"><%: this.GetLocalResourceObject("lbl_Column")%></option>
                    <option value="bar"><%: this.GetLocalResourceObject("lbl_Bar")%></option>
                    <option value="area"><%: this.GetLocalResourceObject("lbl_Area")%></option>
                </select>
            </div>
        </header>
        <div class="panel-body">
            <% if (yearInvoices == null || yearInvoices.Count == 0)
               { %>
            <div class="alert alert-info">
                <%: this.GetLocalResourceObject("e_Nodata")%>
            </div>
            <% }
               else
               { %>
            <div class="filterBox">
                <div class="row controls">
                    <div class="col-md-2">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_Years")%></label>
                    </div>
                    <% for (int i = 0; i < yearInvoices.Keys.Count; ++i)
                       { %>
                    <div class="col-md-2">
                        <input type="checkbox" class="chkSeries" checked="checked" value="<%:i%>" />
                        <%:yearInvoices.ElementAt(i).Key%>
                    </div>
                    <% } %>
                </div>
            </div>
            <div class="chartContainer">
                <div id="chartDiv">
                </div>
            </div>
            <% } %>
        </div>
    </section>
</div>
<script type="text/javascript">
    var yearSeriesData = <%=yearSeriesJsonData %>;
    var yearCategoryData = <%=yearCategoryJsonData %>;
    var monthCategoryData = <%=monthNamesJsonData %>;
</script>
<script type="text/javascript">
    $(function () {
        $('#chartConfiguration').on('change', 'select, input.chkSeries', function () {
            createChart();
        });

        function createChart() {
            var options = {
                title: {
                    text: '<%: this.GetLocalResourceObject("titl_Billcharges")%>'
                },
                legend: {
                    visible: true,
                    position: 'bottom'
                },
                seriesDefaults: {
                    type: $('#chartConfiguration select[name=chartType]').val() || 'line',
                    stack: false,
                    labels: {
                        visible: false
                    },
                    tooltip: {
                        visible: true
                    }
                },
                series: [],
                categoryAxis: {
                    categories: monthCategoryData,
                    labels: { rotation: 0 },
                    title: { text: '<%: this.GetLocalResourceObject("titl_BillMonths")%>', visible: true },
                    majorGridLines: {
                        visible: false
                    }
                },
                valueAxis: {
                    name: 'Amount',
                    title: { text: '<%: this.GetLocalResourceObject("titl_BillAmount")%>', visible: false },
                    field: "Amount",
                    labels: { template: "#= kendo.toString(value,'c') #" }
                },
                tooltip: {
                    visible: true,
                    template: "<div style='text-align:left;'>#= category #, #= series.name # : <b>#= kendo.toString(value,'c') #</b><br/>No: #= dataItem.InvoiceNo #<br/>Date: #= dataItem.Date #<br/>Status: #= dataItem.Status #<br/>Paid: #= kendo.toString(dataItem.PaidAmount,'c')#</div>"
                }
            };

            var selectedSeries = [];
            $('#chartConfiguration input.chkSeries:checked').each(function () {
                selectedSeries.push(parseInt($(this).val()));
            });

            for (i = 0; i < selectedSeries.length; ++i) {
                var idx = selectedSeries[i];
                options.series.push({
                    data: yearSeriesData[idx],
                    name: yearCategoryData[idx].toString(),
                    field: "Amount"
                });
            }

            var chart = $('#chartDiv').data('celloChart');

            if (chart)
                chart.destroy();

            $('#chartDiv').celloChart(options);
        }

        setTimeout(function () {
            try {
                if (yearSeriesData && yearSeriesData.length > 0) {
                    createChart();
                }
            } catch (e) { console.log(e); }
        }, 100);
    });
</script>
