﻿<%@ Page Title="<%$ Resources:t_ManageBillingSettings %>"  Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("h_CustomizeBillingPeriods") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
           { %>
        <% var billingSetting = ViewData["BillingSetting"] as BillingSetting;
           if (billingSetting != null && !UserIdentity.IsInRole(RoleConstants.ProductAdmin))
           { 
        %>
        <table class="details_container_table">
            <tbody>
                <tr>
                    <td>
                        <label>
                            <%: this.GetLocalResourceObject("lbl_BillFrequency") %></label>
                    </td>
                    <td>
                        <%=billingSetting.BillFrequency.ToString() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <%: this.GetLocalResourceObject("lbl_StartDay") %></label>
                    </td>
                    <td>
                        <%=billingSetting.StartDay %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <%: this.GetLocalResourceObject("lbl_ChargeDay") %></label>
                    </td>
                    <td>
                        <%=billingSetting.ChargeDay %>
                    </td>
                </tr>
            </tbody>
        </table>
        <%
           }
           else
           { %>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("e_BillingSettings") %>
        </div>
        <% } %>
        <% } %>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-3">
                        <h4><%: this.GetLocalResourceObject("h_BillingPeriods") %></h4>
                    </div>
                    <%  var billSettingList = ViewData["BillingSettingsList"] as List<BillingSetting>;
                        var frequencySelectList = typeof(BillFrequency).ToSelectList();
                        if (UserIdentity.IsInRole(RoleConstants.ProductAdmin) && (billSettingList == null || (billSettingList.Count != frequencySelectList.Count)))
                        { %>
                    <div class="col-md-2 pull-right">
                        <div class="pull-right">
                            <a class="btn btn-success" href="#" id="btnAddBillingSetting" title="<%:this.GetLocalResourceObject("t_Addnewsetting")%>"><i class="fa fa-plus"></i>
                                &nbsp;<%=this.GetGlobalResourceObject("General","Add") %></a>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
            <div class="grid-body">
                <% if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
                   {
                       if (billSettingList != null && billSettingList.Count > 0)
                       {
                           foreach (var bset in billSettingList)
                           {
                               frequencySelectList.RemoveAll(x => x.Text == bset.BillFrequency.ToString());
                           }

                           Html.Grid<BillingSetting>(billSettingList).Columns(column =>
                              {
                                  column.For(x => x.Mode.ToString()).Named(this.GetLocalResourceObject("lbl_BillMode").ToString());
                                  column.For(x => x.BillFrequency.ToString()).Named(this.GetLocalResourceObject("lbl_BillFrequency").ToString());
                                  column.For(x => x.StartDay).Named(this.GetLocalResourceObject("lbl_StartDay").ToString());
                                  column.For(x => x.ChargeDay).Named(this.GetLocalResourceObject("lbl_ChargeDay").ToString());
                                  column.For(x => string.Format("<a class='editglobalbillsetting' data-id='{0}' data-startday='{1}' data-chargeday='{2}' data-billfrequency='{3}' data-mode='{4}' href='#'><i class='fa fa-pencil'></i></a>",
                                      x.Id, x.StartDay, x.ChargeDay, x.BillFrequency, x.Mode))
                                      .Attributes(@class => "halign").HeaderAttributes(@class => "halign")
                                      .Named(this.GetGlobalResourceObject("General", "Edit").ToString()).DoNotEncode();
                              }).Attributes(@class => "celloTable").Render();
                       }
                %>
            </div>
        </div>
        <div class="modal fade" id="manageBillingSettting" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><%:this.GetLocalResourceObject("h_ManageBillingSetting") %></h4>
                    </div>
                    <div class="modal-body">
                        <form id="frmBillingSetting" name="frmBillingSetting" action="<%=Url.Action("ManageBillSettings") %>">
                            <div class="form-container">
                                <div class="form-group">
                                    <label>
                                        <%: this.GetLocalResourceObject("lbl_BillFrequency") %></label>
                                    <%=Html.DropDownList("BillFrequency", frequencySelectList, new { style="width:100%;" })%>
                                    <div class="frequency"></div>
                                    <input type="hidden" name="Id" />
                                </div>
                                <div class="form-group">
                                    <label>
                                        <%: this.GetLocalResourceObject("lbl_BillMode") %></label>
                                    <%=Html.DropDownList("Mode", typeof(BillCycleMode).ToSelectList(), new { style="width:100%;" })%>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <%: this.GetLocalResourceObject("lbl_StartDay") %></label>
                                    <input type="number" name="StartDay" />
                                </div>
                                <div class="form-group">
                                    <label>
                                        <%: this.GetLocalResourceObject("lbl_ChargeDay") %></label>
                                    <input type="number" name="ChargeDay" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnSave" title="<%: this.GetLocalResourceObject("t_Save") %>" class="btn btn-primary">
                            <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %>
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close")%></button>
                    </div>
                </div>
            </div>
        </div>
        <%
                   } 
        %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#manageBillingSettting').modal({ show: false });

            $('#btnAddBillingSetting').click(function () {
                $('#manageBillingSettting input').val('');
                $('#manageBillingSettting .frequency').empty();
                $('#manageBillingSettting select[name="BillFrequency"]').removeClass('hide');
                $('#manageBillingSettting').modal('show');
                return false;
            });

            $(document).on('click', 'a.editglobalbillsetting', function (e) {
                var id = $(this).data('id');
                var billfrequency = $(this).data('billfrequency');
                var startday = $(this).data('startday');
                var chargeday = $(this).data('chargeday');
                var mode = $(this).data('mode');

                $('#manageBillingSettting input[name="Id"]').val(id);
                $('#manageBillingSettting .frequency').text(billfrequency);
                $('#manageBillingSettting input[name="StartDay"]').val(startday);
                $('#manageBillingSettting input[name="ChargeDay"]').val(chargeday);
                $('#manageBillingSettting select[name="Mode"]').val(mode);
                $('#manageBillingSettting select[name="BillFrequency"]').addClass('hide');
                $('#manageBillingSettting').modal('show');
                return false;
            });

            $(document).on('click', '#btnSave', function (e) {
                var postdata = $('form#frmBillingSetting').serializeJSON();
                $.ajax({
                    url: $('form#frmBillingSetting').attr('action'),
                    dataType: 'json',
                    contentType: 'application/json',
                    type: 'POST',
                    data: JSON.stringify({ setting: postdata }),
                    success: function (data) {
                        if (data.Success) {
                            alert(data.Success);
                            $('#manageBillingSettting').modal('hide');
                            window.location.reload();
                        } else {
                            alert(data.Error || '<%: this.GetLocalResourceObject("e_manageSetting")%>');
                        }
                    }
                });
                return false;
            });
        });
    </script>
</asp:Content>
