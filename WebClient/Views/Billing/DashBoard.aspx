﻿<%@ Page Title="<%$ Resources:t_BillingDashboard %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        HtmlHelper.ClientValidationEnabled = false;
        HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
    %>
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("h_BillingDashboard") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
           { %>
        <div class="alert alert-info">
            <%=Html.CelloValidationMessage("Error")%>
        </div>
        <%}
           else
           { %>
        <div id="totalRevenuesDiv">
            <% Html.RenderAction("TotalRevenues"); %>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div id="topRevenuesTenantsDiv">
                    <% Html.RenderAction("TopRevenueTenants"); %>
                </div>
            </div>
            <div class="col-md-6">
                <div id="divTenantBillStatistics">
                    <% Html.RenderAction("RevenuesByTenants"); %>
                </div>
                <div id="divTenantRevenuesTrends">
                    <% Html.RenderAction("TenantRevenuesTrends"); %>
                </div>
            </div>
        </div>
        <%} %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>
</asp:Content>
