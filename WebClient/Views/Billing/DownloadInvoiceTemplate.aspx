﻿<%@ Page Title="Download Invoice" Language="C#" Inherits="System.Web.Mvc.ViewPage<CelloSaaS.Billing.Model.Invoice>" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico" />
    <title>Download Invoice</title>
    <style type="text/css">
        #header {
            display: none;
        }

        #Nav {
            display: none;
        }

        .footer_container {
            display: none;
        }

        .content_area {
            border: none !important;
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            border-radius: 0;
        }
    </style>
    <% foreach (var css in Html.GetThemeCssUrls())
       { %>
    <link rel="stylesheet" type="text/css" href="<%=css %>" />
    <% } %>
</head>
<body>
    <% Html.RenderPartial("PartialInvoiceTemplate", Model); %>
</body>
</html>
