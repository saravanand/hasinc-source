﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<% Html.RenderPartial("StatusMessage"); %>
<%
    var audits = ViewData["NotifyAudits"] as CelloSaaS.Notification.Model.NotificationAuditSearchResult;
%>
<% if (audits != null && audits.Result != null && audits.Result.Count > 0)
   { %>
<table style="width: 100%;">
    <tbody>
        <% foreach (var item in audits.Result)
           {
               var dest = ((CelloSaaS.Notification.Model.Dispatch.EmailDestination)item.NotificationDestination);
               var content = ((CelloSaaS.Notification.Model.Content.TextMessageContent)item.NotificationContent);
        %>
        <tr>
            <td style="white-space: nowrap;">
                <div>
                    <h4><%:item.NotificationDetails.NotificationName %>
                        <small><%: this.GetLocalResourceObject("lbl_Senton") %><i><%:item.CreatedOn.ToUIDateTimeString()%></i>
                            <% if (item.NotificationStatus == CelloSaaS.Notification.Model.NotificationStatus.Success)
                               { %>
                            <i class="fa fa-check-circle text-success" title="<%: this.GetLocalResourceObject("t_Successfullysent") %>"></i>
                            <% }
                               else if (item.NotificationStatus == CelloSaaS.Notification.Model.NotificationStatus.Failure)
                               { %>
                            <i class="fa fa-exclamation-circle text-danger" title="<%: this.GetLocalResourceObject("t_Failure") %>"></i>
                            <% } %>
                        </small>
                    </h4>
                </div>
                <div><span style="width: 50px; display: inline-block;"><%: this.GetLocalResourceObject("lbl_To") %></span>: <%:dest.RecipientAddress %></div>
                <div><span style="width: 50px; display: inline-block;"><%: this.GetLocalResourceObject("lbl_From") %></span>: <%:dest.SenderAddress %></div>
                <div><span style="width: 50px; display: inline-block;"><%: this.GetLocalResourceObject("lbl_Subject") %></span>: <%:content.Subject %></div>
                <div style="border-bottom: 1px solid #ccc; padding-bottom: 6px; margin-bottom: 10px; margin-top: 5px; white-space: normal;">
                    <%=content.ProcessedContent %>
                </div>
            </td>
        </tr>
        <% } %>
    </tbody>
</table>
<% }
   else
   {
%>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("e_InvoiceNotNotified") %>    
</div>
<% } %>