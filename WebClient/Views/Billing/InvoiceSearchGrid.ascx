﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Dictionary<Guid, CelloSaaS.Billing.Model.Invoice>>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<% Html.RenderPartial("StatusMessage"); %>
<% 
    var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails> ?? new Dictionary<string, TenantDetails>();
%>
<% 
    if (Model != null && Model.Count > 0)
    {
        var searchCondition = ViewData["SearchCondition"] as InvoiceSearchCondition ?? new InvoiceSearchCondition();
        var viewTenantId = ViewData["viewTenantId"] as string;
        int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
        int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;
        int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
        var ajaxOpt = new AjaxOptions { UpdateTargetId = "invoiceDiv", LoadingElementId = "loading", LoadingElementDuration = 300 };

        var routeValues = new
        {
            viewTenantId = viewTenantId,
            FromInvoiceDate = searchCondition.FromInvoiceDate,
            ToInvoiceDate = searchCondition.ToInvoiceDate,
            BillFrequency = searchCondition.BillFrequency,
            InvoiceStatus = searchCondition.InvoiceStatus,
            SortField = searchCondition.SortField,
            SortDirection = searchCondition.SortDirection
        };

        Html.Grid<Invoice>(Model.Values).Columns(column =>
                {
                    column.For(x => x.InvoiceStatus != InvoiceStatus.UnPaid ? string.Empty :
                        string.Format("<input type='checkbox' name='chkSelect' value='{0}' data-tenantid='{1}' data-invoiceid='{0}' />", x.Id, x.TenantId))
                        .Attributes(@class => "halign").HeaderAttributes(@class => "halign",style=>"min-width:25px;")
                        .Named(" ")
                        .DoNotEncode();
                    column.For(x => string.Format("<a href='{1}' title='" + this.GetLocalResourceObject("t_Clicktodownload") +"'>{0}</a>", x.InvoiceNo, Url.Action("DownloadInvoice", new { invoiceId = x.Id, tenantId = x.TenantId }))).Named(this.GetLocalResourceObject("lbl_InvoiceNo").ToString()).DoNotEncode();
                    column.For(x => tenantList.ContainsKey(x.TenantId.ToString()) ? tenantList[x.TenantId.ToString()].TenantName : "").Named(this.GetLocalResourceObject("lbl_TenantName").ToString());
                    column.For(x => x.InvoiceDate.ToUIDateString()).Named(this.GetLocalResourceObject("lbl_InvoiceDate").ToString());
                    column.For(x => x.StartDate.ToUIDateString()).Named(this.GetLocalResourceObject("lbl_BillPeriodStart").ToString());
                    column.For(x => x.EndDate.ToUIDateString()).Named(this.GetLocalResourceObject("lbl_BillPeriodEnd").ToString());
                    column.For(x => x.Amount.ToBillingCurrency()).Named(this.GetLocalResourceObject("lbl_Amount").ToString());
                    column.For(x => x.PaidAmount.ToBillingCurrency()).Named(this.GetLocalResourceObject("lbl_Paid").ToString());
                    //column.For(x => x.DueDate.ToUIDateString()).Named("Due Date");
                    column.For(x => x.InvoiceStatus.ToString().ToUpperInvariant()).Named(this.GetLocalResourceObject("lbl_Status").ToString());
                    column.For(x => string.Format("<a href='#' class='view-email-audit' data-invoiceid='{0}' data-invoiceno='{1}' data-tenantid='{2}' data-parenttenantid='{3}' title='" + this.GetLocalResourceObject("t_ViewEmailAudits") + "'><i class='fa fa-envelope'></i></a>", x.Id, x.InvoiceNo, x.TenantId, x.ParentTenantId))
                        .Named(this.GetLocalResourceObject("lbl_EmailAudit").ToString())
                        .Attributes(@class => "halign").HeaderAttributes(@class => "halign")
                        .DoNotEncode();
                    column.For(x => string.Format("<a href='{0}' title='" + this.GetLocalResourceObject("t_ViewInvoiceDetails") + "'><i class='fa fa-search'></i></a>", Url.Action("PreviewInvoice", new { invoiceId = x.Id, tenantId = x.TenantId })))
                        .Named(this.GetLocalResourceObject("lbl_View").ToString())
                        .Attributes(@class => "halign").HeaderAttributes(@class => "halign")
                        .DoNotEncode();
                })
                .RowAttributes(x=> new Dictionary<string,object>() { {"class", (x.Item.InvoiceStatus == InvoiceStatus.UnPaid ? "danger text-danger" : "")} })
                .Attributes(id => "tblInvoiceSearch", @class => "celloTable")
                .Render();

        Ajax.CelloPager(ajaxOpt, routeValues).SetTotalCount(totalCount).SetPageNumber(pageNumber).SetPageSize(pageSize)
          .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
          .Render();
    }
    else
    { %>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("e_NoInvoiceAvailable") %>
</div>
<% } %>