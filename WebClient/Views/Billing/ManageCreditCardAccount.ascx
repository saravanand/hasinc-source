﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% var account = ViewBag.AccountDetails as CreditCardAccount ?? new CreditCardAccount { CreditCardInfo = new CreditCardInfo { Address = new ShippingAddress() } }; %>
<% if (Html.ValidationSummary() != null)
   {
%>
<div class="alert alert-warning">
    <%=Html.ValidationSummary() %>
</div>
<% }
   else
   { %>
<% 
       var expireMonths = new SelectList(Enumerable.Range(1, 12), account.CreditCardInfo.ExpireMonth);
       var expireYears = new SelectList(Enumerable.Range(DateTime.Now.Year, 20), account.CreditCardInfo.ExpireYear);

       var action = ViewData["form_action"] ?? Url.Action("ManageCreditCardAccount");
%>
<form name="frmCreditCardAccount" id="frmCreditCardAccount" method="post" action="<%=action %>">
    <% if (ViewData["token"] != null)
       { %>
    <input type="hidden" name="Token" value="<%=ViewData["token"] %>" />
    <p>
        <%=this.GetLocalResourceObject("info_redirect") %>
    </p>
    <% }
       else
       { %>
    <h3><%: this.GetLocalResourceObject("h_CreditCardDetails") %></h3>
    <table class="details_container_table">
        <colgroup>
            <col style="width: 200px;">
            <col style="width: auto;">
        </colgroup>
        <tbody>
            <tr>
                <td>
                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_Email") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.Email", account.Email, new { maxlength = "255" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.Email", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mandatory">
                        <%: this.GetLocalResourceObject("lbl_Name") %>
                    </label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.First", account.CreditCardInfo.Address.First, new { maxlength ="50", style="width:95px;" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.Address.First", "*")%>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.Last", account.CreditCardInfo.Address.Last, new { maxlength = "50", style = "width:95px;" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.Address.Last", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mandatory">
                        <%: this.GetLocalResourceObject("lbl_CardNumber") %>
                    </label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.CardNumber", account.CreditCardInfo.CardNumber, new { maxlength="16", autocomplete="off" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.CardNumber", "*")%>
                    <div class="supported-credit-cards">
                        <img src="/Content/images/american-express.png" title="<%: this.GetLocalResourceObject("t_AmericanExpress") %>" alt="American Express" />
                        <img src="/Content/images/discover.png" title="<%: this.GetLocalResourceObject("t_Discover") %>" alt="Discover" />
                        <img src="/Content/images/master-card.png" title="<%: this.GetLocalResourceObject("t_MasterCard") %>" alt="MasterCard" />
                        <img src="/Content/images/maestro.png" title="<%: this.GetLocalResourceObject("t_Maestro") %>" alt="Maestro" />
                        <img src="/Content/images/visa.png" title="<%: this.GetLocalResourceObject("t_Visa") %>" alt="Visa" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mandatory">
                        <%: this.GetLocalResourceObject("lbl_Expiration") %></label>
                </td>
                <td>
                    <%=Html.DropDownList("CreditCardAccount.CreditCardInfo.ExpireMonth", expireMonths ,new { style = "width:60px;" })%> -
                        <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.ExpireMonth", "*")%>
                    <%=Html.DropDownList("CreditCardAccount.CreditCardInfo.ExpireYear", expireYears,new { style = "width:80px;" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.ExpireYear", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mandatory">
                        <%: this.GetLocalResourceObject("lbl_CCV") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.CCV", account.CreditCardInfo.CCV, new { maxlength = "4", style = "width:75px;", autocomplete = "off" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.CCV", "*")%>
                    <div class="supported-credit-cards">
                        <img src="/Content/images/card-back.png" title="<%: this.GetLocalResourceObject("t_CreditCardBackView") %>" alt="CCV" />
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <h3><%: this.GetLocalResourceObject("lbl_BillingAddress") %></h3>
    <table class="details_container_table">
        <colgroup>
            <col style="width: 200px;">
            <col style="width: auto;">
        </colgroup>
        <tbody>

            <tr>
                <td>
                    <label>
                        <%: this.GetLocalResourceObject("lbl_City") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.City", account.CreditCardInfo.Address.City, new { maxlength ="50" })%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <%: this.GetLocalResourceObject("lbl_State") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.State", account.CreditCardInfo.Address.State, new { maxlength ="50" })%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <%: this.GetLocalResourceObject("lbl_Country") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.Country", account.CreditCardInfo.Address.Country, new { maxlength ="50" })%>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mandatory">
                        <%: this.GetLocalResourceObject("lbl_Street") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.Street", account.CreditCardInfo.Address.Street, new { maxlength ="255" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.Address.Street", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="mandatory">
                        <%: this.GetLocalResourceObject("lbl_Zip") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.Zip", account.CreditCardInfo.Address.Zip, new { maxlength ="5" })%>
                    <%=Html.ValidationMessage("CreditCardAccount.CreditCardInfo.Address.Zip", "*")%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <%: this.GetLocalResourceObject("lbl_Phone") %></label>
                </td>
                <td>
                    <%=Html.TextBox("CreditCardAccount.CreditCardInfo.Address.Phone", account.CreditCardInfo.Address.Phone, new { maxlength ="15" })%>
                    <div><%: this.GetLocalResourceObject("lbl_eg") %></div>
                </td>
            </tr>
        </tbody>
    </table>
    <% } %>
</form>
<% } %>