﻿<%@ Page Title="<%$ Resources:t_EditInvoice %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Billing.Model.Invoice>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var tenant = ViewData["TenantDetails"] as Tenant;
        var parentTenant = ViewData["ParentTenant"] as Tenant ?? new Tenant();
        string logo = ViewData["ParentTenantLogo"] as string;
    %>
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("ManageChildBills", new { viewTenantId=this.Model!=null ? this.Model.TenantId.ToString() : ""}) %>"
            title="<%: this.GetLocalResourceObject("t_Cancelchanges") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3><%: this.GetLocalResourceObject("h_EditInvoice") %></h3>
        <div class="pull-right">
            <% if (this.Model != null)
               { %>
            <a class="btn btn-success" href="#" onclick="javascript:$('#frmInvoiceLineItem').submit();" title="<%: this.GetLocalResourceObject("t_Savethechanges") %>">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <% } %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (this.Model == null)
           { %>
        <div class="alert alert-danger">
            <%: this.GetLocalResourceObject("e_InvoiceDetails") %>
        </div>
        <% }
           else
           {
        %>
        <div class="col-md-10 col-md-offset-1">
            <div class="grid simple">
                <div class="grid-body no-border invoice-body">
                    <div class="invoiceContainer" id="invoiceContainer">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="width: 48%; vertical-align: top;">
                                        <div class="row">
                                            <div style="padding: 6px;">
                                                <div style="line-height: 23px;">
                                                    <div>
                                                        <%=Html.Logo(parentTenant.TenantDetails.TenantCode) %>
                                                        <h2 style="font-size: 2em; margin-top: 5px; margin-bottom: 20px;">
                                                            <%=parentTenant.TenantDetails.TenantName %></h2>
                                                    </div>
                                                    <div style="margin-left: 6px;">
                                                        <% if (parentTenant.Address != null)
                                                           { %>
                                                        <div>
                                                            <%=parentTenant.Address.Address1 %>
                                                            <%=parentTenant.Address.Address2 %>
                                                        </div>
                                                        <div>
                                                            <%=parentTenant.Address.State%>
                                                        </div>
                                                        <div>
                                                            <%=parentTenant.Address.CountryName%> - <%=parentTenant.Address.PostalCode%>
                                                        </div>
                                                        <% } %>
                                                        <br />
                                                        <% if (parentTenant.ContactDetail != null)
                                                           { %>
                                                        <div>
                                                            <i class="fa fa-phone"></i>
                                                            <span>
                                                                <%=parentTenant.ContactDetail.Phone%></span>
                                                        </div>
                                                        <div>
                                                            <i class="fa fa-envelope"></i>
                                                            <span>
                                                                <%=parentTenant.ContactDetail.Email%></span>
                                                        </div>
                                                        <% } %>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <div class="row">
                                            <div style="padding: 6px; text-align: right;">
                                                <h2 style="font-size: 2.1em; margin-bottom: 20px;"><%: this.GetLocalResourceObject("lbl_INVOICE") %></h2>
                                                <div class="date"><%=Model.InvoiceDate.ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat()) %></div>
                                                <div class="invoiceno"><%: this.GetLocalResourceObject("lbl_Invoiceno") %><b><%=Model.InvoiceNo %></b></div>
                                                <div class="dateperiod"><%: this.GetLocalResourceObject("lbl_Period") %> <%=Model.StartDate.ToUIDateString() %> - <%=Model.EndDate.ToUIDateString() %></div>
                                                <div class="clientname" style="margin: 20px 0;">
                                                    <h2 style="font-size: 2em; padding: 0; margin: 0; margin-bottom: 5px;">
                                                        <%=tenant.TenantDetails.TenantName %></h2>
                                                </div>
                                                <% if (tenant.Address != null)
                                                   { %>
                                                <div>
                                                    <%=tenant.Address.Address1 %>
                                                    <%=tenant.Address.Address2 %>
                                                </div>
                                                <div>
                                                    <%=tenant.Address.State%>
                                                </div>
                                                <div>
                                                    <%=tenant.Address.CountryName%> - <%=tenant.Address.PostalCode%>
                                                </div>
                                                <% } %><br />
                                                <% if (tenant.ContactDetail != null)
                                                   { %>
                                                <div>
                                                    <i class="fa fa-phone"></i>
                                                    <span>
                                                        <%=tenant.ContactDetail.Phone%></span>
                                                </div>
                                                <div>
                                                    <i class="fa fa-envelope"></i>
                                                    <span>
                                                        <%=tenant.ContactDetail.Email%></span>
                                                </div>
                                                <% } %>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <form name="frmInvoiceLineItem" id="frmInvoiceLineItem" method="post" action="">
                            <div class="lineItemsDiv form-container" style="margin-top: 15px;">
                                <%=Html.Hidden("InvoiceId",Model.Id) %>
                                <%=Html.Hidden("TenantId", Model.TenantId)%>
                                <% var lstTaxItems = Model.LineItems.Where(x => x.Type == "Tax"); %>
                                <% var lstAdjustmentItems = Model.LineItems.Where(x => x.Type == "Adjustment"); %>
                                <% 
                                   int cnt = 0;
                                   var cntdic = Model.LineItems.Where(x => x.IsUserAdded).ToDictionary(x => x.Id, y => cnt++);
                                   Html.Grid(Model.LineItems.Where(x => x.Type != "Tax" && x.Type != "Adjustment").OrderBy(x => x.IsUserAdded)).Columns(col =>
                                   {
                                       col.For(x => x.IsUserAdded
                                           ?
                                       string.Format("<a href='#' class='dellineitem'><i class='fa fa-trash-o'></i></a>"
                                           + "<input type='hidden' class='lineitemid' name='lineItems[{1}].Id' value='{0}'/>"
                                           + "<input type='hidden' name='lineItems[{1}].InvoiceId' value='{2}'/>"
                                           + "<input type='hidden' name='lineItems[{1}].TenantId' value='{3}'/>", x.Id, cntdic[x.Id], x.InvoiceId, x.TenantId)
                                       : string.Empty).Named(string.Empty).Attributes(@class => "halign", style => "width:30px;").DoNotEncode();
                                       col.For(x => x.IsUserAdded
                                           ? string.Format("<input type='text' name='lineItems[{1}].Name' value='{0}'/>", x.Name, cntdic[x.Id])
                                           : x.Name).Named(this.GetLocalResourceObject("lbl_ItemName").ToString()).Attributes(@class => "valign").DoNotEncode();
                                       col.For(x => x.IsUserAdded
                                           ? string.Format("<textarea name='lineItems[{1}].Description' class='lidescription'>{0}</textarea>", x.Description, cntdic[x.Id])
                                           : x.Description).Named(this.GetLocalResourceObject("lbl_Description").ToString()).Attributes(@class => "valign").DoNotEncode();
                                       col.For(x => x.IsUserAdded
                                           ? string.Format("<input type='text' style='text-align:right;' class='liamount' name='lineItems[{1}].Amount' value='{0}'/>", x.Amount.ToString("F2"), cntdic[x.Id])
                                           : string.Format("{0}<span>{1}</span>", CelloSaaS.View.Extensions.GetBillingCulture().NumberFormat.CurrencySymbol, x.Amount.ToString("F2"))).Named(this.GetLocalResourceObject("lbl_Amount").ToString()).Attributes(@class => "amount valign tright").HeaderAttributes(@class => "tright", style => "min-width:210px;").DoNotEncode();
                                   }).Attributes(@class => "table table-condensed", id => "tableLineItem").Render();
                                %>
                                <table class="table table-condensed">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <% if (Model.InvoiceStatus == InvoiceStatus.Current)
                                                   { %>
                                                <a href="#" id="btnAddLineItem"><i class="fa fa-plus-square"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_AddLineItem") %></a>
                                                <% } %>
                                            </td>
                                            <td></td>
                                            <td class="tright">
                                                <strong><%: this.GetLocalResourceObject("lbl_SubTotal") %></strong>
                                            </td>
                                            <td class="tright" style="width: 150px;">
                                                <strong>
                                                    <%:CelloSaaS.View.Extensions.GetCurrencySymbol(this.Html) %>
                                                </strong>
                                                <strong class="billamount">
                                                    <%=Model.Subtotal.ToString("F2") %>
                                                </strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <% if (lstTaxItems.Count() > 0)
                               { %>
                            <div class="taxesDiv" style="margin-top: 15px;">
                                <h3 class="blueHeading"><%: this.GetLocalResourceObject("h_Taxes") %></h3>
                                <% 
                                   Html.Grid(lstTaxItems).Columns(col =>
                                   {
                                       col.For(x => x.Name)
                                           .Named(this.GetLocalResourceObject("lbl_ItemName").ToString()).HeaderAttributes(@style => "text-align:left;min-width:220px;width:auto;");
                                       col.For(x => x.Description)
                                           .Named(this.GetLocalResourceObject("lbl_Description").ToString()).HeaderAttributes(@style => "text-align:left;min-width:300px;");
                                       col.For(x => x.Amount.ToBillingCurrency())
                                           .Named(this.GetLocalResourceObject("lbl_Amount").ToString()).HeaderAttributes(style => "text-align:right;min-width:140px;")
                                           .Attributes(style => "text-align:right;");
                                   }).Attributes(@style => "width: 100%;line-height: 23px;").Render();
                                %>
                            </div>
                            <% } %>
                            <% if (lstAdjustmentItems.Count() > 0)
                               { %>
                            <div class="adjustmentsDiv" style="margin-top: 15px;">
                                <h3 class="blueHeading"><%: this.GetLocalResourceObject("lbl_Adjustments") %></h3>
                                <% 
                                   Html.Grid(lstAdjustmentItems).Columns(col =>
                                   {
                                       col.For(x => x.Name)
                                           .Named(this.GetLocalResourceObject("lbl_ItemName").ToString()).HeaderAttributes(@style => "text-align:left;min-width:220px;width:auto;");
                                       col.For(x => x.Description)
                                           .Named(this.GetLocalResourceObject("lbl_Description").ToString()).HeaderAttributes(@style => "text-align:left;min-width:300px;");
                                       col.For(x => x.Amount.ToBillingCurrency())
                                           .Named(this.GetLocalResourceObject("lbl_Amount").ToString()).HeaderAttributes(style => "text-align:right;min-width:140px;")
                                           .Attributes(style => "text-align:right;");
                                   }).Attributes(@style => "width: 100%;line-height: 23px;").Render();
                                %>
                            </div>
                            <% } %>
                            <table style="width: 100%; line-height: 23px;">
                                <tbody>
                                    <% if (lstTaxItems.Count() > 0)
                                       { %>
                                    <tr>
                                        <td style="text-align: right" colspan="4">
                                            <strong><%: this.GetLocalResourceObject("lbl_TaxTotal") %></strong>
                                        </td>
                                        <td style="width: 140px; text-align: right;">
                                            <%:Html.GetCurrencySymbol() %>
                                            <strong id="taxAmount">
                                                <%=Model.TaxAmount.ToString("F2") %>
                                            </strong>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <% if (lstAdjustmentItems.Count() > 0)
                                       { %>
                                    <tr>
                                        <td style="text-align: right" colspan="4">
                                            <strong><%: this.GetLocalResourceObject("lbl_AdjTotal") %></strong>
                                        </td>
                                        <td style="width: 140px; text-align: right;">
                                            <strong>
                                                <%=Model.AdjustmentAmount.ToBillingCurrency() %>
                                            </strong>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <tr>
                                        <td style="text-align: right" colspan="4">
                                            <strong><%: this.GetLocalResourceObject("lbl_NetAmount") %></strong>
                                        </td>
                                        <td style="width: 140px; text-align: right;">
                                            <strong>
                                                <%:CelloSaaS.View.Extensions.GetCurrencySymbol(this.Html) %>
                                            </strong>
                                            <strong id="netAmount">
                                                <%=Model.Amount.ToString("F2") %>
                                            </strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <% if (Model.InvoiceStatus != InvoiceStatus.Current)
                               { %>
                            <div id="paymentsDiv" style="margin-top: 15px;">
                                <div class="blueHeading">
                                    <h3 style="display: inline-block;"><%: this.GetLocalResourceObject("lbl_Payments") %></h3>
                                    <% if (Model.Amount > Model.PaidAmount)
                                       { %>
                                    <div style="display: inline-block;">
                                        <a href="#" class="btn btn-xs btn-primary" id="btnAddPayment" title="<%: this.GetLocalResourceObject("t_AddnewPayment") %>"><i class="fa fa-plus"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_AddPayment") %> </a>
                                    </div>
                                    <% } %>
                                </div>
                                <% if (Model.Payments != null && Model.Payments.Count > 0)
                                   { %>
                                <%
                                       Html.Grid(Model.Payments).Columns(col =>
                                       {
                                           col.For(x => string.Format("<a href='#' class='delpayment' data-id='{0}' data-invoiceid='{1}' data-tenantid='{2}'><i class='fa fa-trash-o'></i></a>", x.Id, x.InvoiceId, x.TenantId))
                                               .Named(string.Empty).Attributes(@class => "halign", style => "width:30px;").DoNotEncode();
                                           col.For(x => x.PaymentMode.ToString()).Named(this.GetLocalResourceObject("lbl_Mode").ToString()).Attributes(style => "width:160px;");
                                           col.For(x => x.PaymentDate.ToUIDateString()).Named(this.GetLocalResourceObject("lbl_Date").ToString()).Attributes(style => "width:210px;");
                                           col.For(x => x.PaidAmount.ToBillingCurrency())
                                               .Named(this.GetLocalResourceObject("lbl_PiadAmount").ToString())
                                               .Attributes(@class => "tright", style => "width:180px;")
                                               .HeaderAttributes(@class => "tright", style => "width:180px;");
                                           col.For(x => x.Notes ?? "--").Named(this.GetLocalResourceObject("lbl_Notes").ToString());
                                       }).Attributes(@class => "table table-condensed", id => "tblPayments").Render();
                                %>
                                <% }
                                   else
                                   { %>
                                <div class="alert alert-info">
                                    <%: this.GetLocalResourceObject("e_Payments") %>
                                </div>
                                <% } %>
                            </div>
                            <% } %>
                            <div id="notesDiv" style="margin-top: 15px;">
                                <div class="blueHeading">
                                    <h3><%: this.GetLocalResourceObject("lbl_Notes") %></h3>
                                </div>
                                <div class="from-container">
                                    <div class="form-group">
                                        <%=Html.TextArea("Memo", Model.Memo, new { rows="5", cols="25", @class="form-control" })%>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="invoicePaymentModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;</button>
                                    <h4 class="modal-title"><%: this.GetLocalResourceObject("h_AddNewPayment") %></h4>
                                </div>
                                <div class="modal-body">
                                    <form id="frmPayment" name="frmPayment" action="<%=Url.Action("ManageInvoice") %>">
                                        <%=Html.Hidden("InvoiceId",Model.Id) %>
                                        <%=Html.Hidden("TenantId", Model.TenantId)%>
                                        <%=Html.Hidden("ParentTenantId", Model.ParentTenantId)%>
                                        <table style="width: 100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label>
                                                            <%: this.GetLocalResourceObject("lbl_PaymentMode") %></label>
                                                    </td>
                                                    <td>
                                                        <%=Html.DropDownList("PaymentMode", typeof(PaymentMode).ToSelectList())%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>
                                                            <%: this.GetLocalResourceObject("lbl_PaidDate") %></label>
                                                    </td>
                                                    <td>
                                                        <%=Html.TextBox("PaymentDate", "", new { @class = "datetime" })%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>
                                                            <%: this.GetLocalResourceObject("lbl_PiadAmount") %></label>
                                                    </td>
                                                    <td>
                                                        <%=Html.TextBox("PaidAmount") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label>
                                                            <%: this.GetLocalResourceObject("lbl_Notes") %>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <%=Html.TextArea("Notes") %>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close")%></button>
                                    <button type="button" id="btnSave" title="<%: this.GetLocalResourceObject("t_AddThisPayment") %>" class="btn btn-primary">
                                        <i class="fa fa-money"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_Pay") %>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .invoiceContainer h1 {
            text-align: center;
            display: block;
            margin: 5px auto;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('input.datetime').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#paymentModal').modal({ show: false });

            $('#btnAddLineItem').click(function (e) {
                var cnt = $('#tableLineItem input.lineitemid').length;
                var trHtml = '<tr>';
                trHtml += '<td class="halign" style="width:30px;"><a href="#" class="dellineitem"><i class="fa fa-trash-o"></i></a>';
                trHtml += '<input type="hidden" class="lineitemid" name="lineItems[' + cnt + '].Id" value="' + guidGenerator() + '"/>';
                trHtml += '<input type="hidden" name="lineItems[' + cnt + '].InvoiceId" value="00000000-0000-0000-0000-000000000000"/>';
                trHtml += '<input type="hidden" name="lineItems[' + cnt + '].TenantId" value="00000000-0000-0000-0000-000000000000"/>';
                trHtml += '</td>';
                trHtml += '<td class="name valign"><input type="text" name="lineItems[' + cnt + '].Name" /></td>';
                trHtml += "<td class='desc valign'><textarea name='lineItems[" + cnt + "].Description' class='lidescription'></textarea></td>";
                trHtml += '<td class="amount tright valign"><input type="text" style="text-align:right;" class="liamount" name="lineItems[' + cnt + '].Amount" value="0"/></td>';
                trHtml += '</tr>';

                $('#tableLineItem tbody tr:last').after($(trHtml));
                return false;
            });

            $('#btnAddPayment').click(function (e) {
                $('#frmPayment')[0].reset();
                $('#paymentModal').modal('show');
                return false;
            });

            $('#btnSave').click(function (e) {
                var payment = $('#frmPayment').serializeJSON();

                $.ajax({
                    url: '/Billing/AddInvoicePayment',
                    dataType: 'json',
                    contentType: 'application/json',
                    type: 'POST',
                    data: JSON.stringify({
                        invoiceId: payment.InvoiceId,
                        tenantId: payment.TenantId,
                        payment: payment
                    }),
                    success: function (data) {
                        if (data.Success) {
                            $('#paymentModal').modal('hide');
                            alert(data.Success);
                            window.location = '<%=Url.Action("ManageChildBills", new { viewTenantId=this.Model!=null ? this.Model.TenantId.ToString() : ""}) %>';
                        } else {
                            alert(data.Error.replace(';', '\n') || '<%: this.GetLocalResourceObject("e_AddPayments") %>');
                        }
                    },
                    complete: function () {

                    }
                });
                return false;
            });

            $('#btnCancel').click(function (e) {
                $('#paymentModal').modal('hide');
                return false;
            });

            $('#tblPayments a.delpayment').click(function (e) {
                if (confirm('<%: this.GetLocalResourceObject("q_DeletePayments") %>')) {
                    var postdata = {
                        invoiceId: $(this).data('invoiceid'),
                        tenantId: $(this).data('tenantid'),
                        paymentId: $(this).data('id')
                    };
                    $.post('/Billing/DeleteInvoicePayment', postdata, function (data) {
                        if (data.Success) {
                            alert(data.Success);
                            window.location = '<%=Url.Action("ManageChildBills", new { viewTenantId=this.Model!=null ? this.Model.TenantId.ToString() : ""}) %>';
                        } else {
                            alert(data.Error || '<%: this.GetLocalResourceObject("e_DeletePayment") %>');
                        }
                    });
                }
                return false;
            });

            function calNetAmount() {
                var amt = 0.0;
                $('#tableLineItem input.liamount').each(function () {
                    amt += parseFloat($(this).val());
                });

                $('#tableLineItem td.amount span').each(function () {
                    amt += parseFloat($(this).text());
                });

                $('.billamount').text(amt.toFixed(2));

                // calc tax amount and update net amount
                var tamt = parseFloat($('#taxAmount').text() || '0.0');
                var namt = tamt + amt;
                $('#netAmount').text(namt.toFixed(2));
            }

            $(document).on('blur', '#tableLineItem input.liamount', function () {
                calNetAmount();
            });

            $(document).on('click', '#tableLineItem a.dellineitem', function () {
                var tr = $(this).closest('tr');
                tr.remove();
                calNetAmount();
                return false;
            });
        });

        function guidGenerator() {
            var S4 = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };
            return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
        }
    </script>
</asp:Content>
