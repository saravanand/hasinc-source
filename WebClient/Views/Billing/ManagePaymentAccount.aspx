﻿<%@ Page Title="<%$ Resources:t_ManagePaymentAccount %>" Language="C#" MasterPageFile="~/Views/Shared/Payment.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Billing.Model.Invoice>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div id="bcontent">
        <%
            var accountDetails = ViewBag.AccountDetails as PaymentAccount;
        %>
        <div class="page-title">
            <h3><%: this.GetLocalResourceObject("h_PaymentAccount") %></h3>
            <% if (accountDetails != null)
               { %>
            <div class="pull-right">
                <a class="btn btn-danger" href="<%=Url.Action("DeletePaymentAccount") %>" id="btnDeletePaymentAccount" title="<%: this.GetLocalResourceObject("t_Delete") %>"><%=this.GetGlobalResourceObject("General","Delete") %></a>
            </div>
            <% } %>
        </div>
        <div class="row-fluid pd-25">
            <% Html.RenderPartial("StatusMessage"); %>
            <%
                var invoice = ViewData["InvoiceDetails"] as Invoice;
                if (invoice != null)
                {
            %>
            <div class="alert alert-success">
                <p>
                    <%:string.Format(this.GetLocalResourceObject("lbl_BillGenerated").ToString(),invoice.InvoiceNo,invoice.StartDate.ToUIDateTimeString(),invoice.EndDate.ToUIDateTimeString(),invoice.Amount.ToBillingCurrency(),(invoice.InvoiceStatus == InvoiceStatus.Paid ? this.GetLocalResourceObject("lbl_andpaid") : "" )) %>
                </p>
                <p>
                    <%:string.Format(this.GetLocalResourceObject("lbl_GoToMyBills").ToString(),"<a href='"+Url.Action("MyBills")+"' title='"+this.GetLocalResourceObject("t_Gotomybillspage")+"'>"+this.GetLocalResourceObject("lbl_mybills")+"</a>")%>
            </div>
            <% } %>
            <% var tenant = ViewBag.TenantInfo as Tenant;
               // if no payment account details found, then check for trial days
               if (accountDetails == null && tenant != null && tenant.TenantDetails.EnableAutoDebit)
               {
                   var tenantLicense = LicenseProxy.GetTenantLicense(UserIdentity.TenantID);
                   if (tenantLicense != null
                       && tenantLicense.TrialEndDate.HasValue
                       && tenantLicense.TrialEndDate.Value < DateTime.Now)
                   {
            %>
            <div class="alert alert-info">
                <p><%: this.GetLocalResourceObject("e_Trialexperied") %> </p>
            </div>
            <% 
                   }
                   else
                   { 
            %>
            <div class="alert alert-info">
                <p><%:this.GetLocalResourceObject("i_PaymentDetails") %></p>
            </div>
            <% 
                   }
               } 
            %>
            <% if (accountDetails is PayPalAccount)
               {
                   var payPalAccount = accountDetails as PayPalAccount;
            %>
            <section class="panel blue">
                <header class="panel-heading">
                    <h4><%: this.GetLocalResourceObject("h_PayPalAccountDetails") %></h4>
                </header>
                <div class="panel-body">
                    <table class="details_container_table" style="width: 100%;">
                        <colgroup>
                            <col style="width: 200px;">
                            <col style="width: auto;">
                        </colgroup>
                        <tbody>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Email") %> 
                                </td>
                                <td>
                                    <%:payPalAccount.Email %>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_CurrencyCode") %>
                                </td>
                                <td>
                                    <%:payPalAccount.CurrencyCode %>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_PreapprovalKey") %>
                                </td>
                                <td>
                                    <%:payPalAccount.StoreKey%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Status") %> 
                                </td>
                                <td>
                                    <%:payPalAccount.Status ? "Active" : "Deactivated" %>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <% if (payPalAccount.PreapprovalDetails != null)
                       {
                           string redirectUrl = string.Format("{0}_ap-preapproval&preapprovalkey={1}", ConfigurationManager.AppSettings["PAYPAL_REDIRECT_URL"], payPalAccount.PreApprovalKey);
                    %>
                    <h4><%: this.GetLocalResourceObject("h_PreapprovalDetails") %> </h4>
                    <table class="details_container_table" style="width: 100%;">
                        <colgroup>
                            <col style="width: 200px;">
                            <col style="width: auto;">
                        </colgroup>
                        <tbody>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Approved") %>
                                </td>
                                <td>
                                    <%:payPalAccount.PreapprovalDetails.Approved %>
                                    <%=payPalAccount.PreapprovalDetails.Approved.HasValue && !payPalAccount.PreapprovalDetails.Approved.Value ? "<div style='display:inline-block;margin-left:10px;'><i class='fa fa-check'></i><a href='"+ redirectUrl +"' title='"+this.GetLocalResourceObject("lbl_Clickheretoapprove").ToString()+"'>"+this.GetLocalResourceObject("lbl_Approve").ToString()+"</a></div>" : "" %>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("h_MaxNumberOfPayments") %> 
                                </td>
                                <td>
                                    <%:payPalAccount.PreapprovalDetails.MaxNumberOfPayments.HasValue ? payPalAccount.PreapprovalDetails.MaxNumberOfPayments.Value.ToString() : "-"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_MaxAmountPerPayment") %>
                                </td>
                                <td>
                                    <%:payPalAccount.PreapprovalDetails.MaxAmountPerPayment.HasValue ? payPalAccount.PreapprovalDetails.MaxAmountPerPayment.Value.ToBillingCurrency(): "-"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_MaxTotalAmount") %>
                                </td>
                                <td>
                                    <%:payPalAccount.PreapprovalDetails.MaxTotalAmountOfAllPayments.HasValue ? payPalAccount.PreapprovalDetails.MaxTotalAmountOfAllPayments.Value.ToBillingCurrency() : "-"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_StartingDate") %>
                                </td>
                                <td>
                                    <%:DateTime.Parse(payPalAccount.PreapprovalDetails.StartingDate).ToUIDateTimeString()%>
                                </td>
                            </tr>
                            <% if (!string.IsNullOrEmpty(payPalAccount.PreapprovalDetails.EndingDate))
                               { %>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_EndingDate") %>
                                </td>
                                <td>
                                    <% var endDate = DateTime.Parse(payPalAccount.PreapprovalDetails.EndingDate); %>
                                    <%:endDate.ToUIDateTimeString()%>
                                    <% if (endDate <= DateTime.Now)
                                       { %>
                                    <div class="actions">
                                        <div class="button">
                                            <a href="#" data-email="<%:payPalAccount.Email %>" id="btnRenewPreapproval" title="<%: this.GetLocalResourceObject("t_Renewpreapproval") %>"><%: this.GetLocalResourceObject("lbl_Renew") %></a>
                                        </div>
                                    </div>
                                    <% } %>
                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>
            </section>
            <% } %>
            <% }
               else if (accountDetails is CreditCardAccount)
               {
                   var cardAccount = accountDetails as CreditCardAccount;
            %>
            <% if (cardAccount == null || cardAccount.CreditCardInfo == null)
               { %>
            <div class="alert alert-danger"><%: this.GetLocalResourceObject("e_FetchCreditCard") %></div>
            <% }
               else
               { %>
            <section class="panel indigo">
                <header class="panel-heading">
                    <h4><%: this.GetLocalResourceObject("h_CreditCardDetails") %></h4>
                </header>
                <div class="panel-body">
                    <table class="details_container_table" style="width: 100%;">
                        <colgroup>
                            <col style="width: 200px;">
                            <col style="width: auto;">
                        </colgroup>
                        <tbody>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Email") %> 
                                </td>
                                <td>
                                    <%:cardAccount.Email%>
                                </td>
                            </tr>
                            <% if (cardAccount.CreditCardInfo.Address != null && !string.IsNullOrEmpty(cardAccount.CreditCardInfo.Address.First))
                               { %>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Name") %> 
                                </td>
                                <td>
                                    <%:cardAccount.CreditCardInfo.Address.First%>
                                    <%:cardAccount.CreditCardInfo.Address.Last%>
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_CurrencyCode") %>
                                </td>
                                <td>
                                    <%:cardAccount.CurrencyCode%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Status") %> 
                                </td>
                                <td>
                                    <%:cardAccount.Status ? "Active" : "Deactivated"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_CardNumber") %>
                                </td>
                                <td>
                                    <%:cardAccount.CreditCardInfo.CardNumber%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_CardExpiration") %>
                                </td>
                                <td><%: this.GetLocalResourceObject("lbl_XXXXX") %>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <% if (cardAccount.CreditCardInfo.Address != null)
                       { %>
                    <h4><%: this.GetLocalResourceObject("lbl_AddressDetails") %></h4>
                    <table class="details_container_table" style="width: 100%;">
                        <colgroup>
                            <col style="width: 200px;">
                            <col style="width: auto;">
                        </colgroup>
                        <tbody>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Street") %> 
                                </td>
                                <td>
                                    <%:!string.IsNullOrEmpty(cardAccount.CreditCardInfo.Address.Street) ?cardAccount.CreditCardInfo.Address.Street : "Not Available!"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_City") %> 
                                </td>
                                <td>
                                    <%:!string.IsNullOrEmpty(cardAccount.CreditCardInfo.Address.City) ?cardAccount.CreditCardInfo.Address.City : "Not Available!"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_State") %> 
                                </td>
                                <td>
                                    <%:!string.IsNullOrEmpty(cardAccount.CreditCardInfo.Address.State) ?cardAccount.CreditCardInfo.Address.State :"Not Available!"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Country") %> 
                                </td>
                                <td>
                                    <%:!string.IsNullOrEmpty(cardAccount.CreditCardInfo.Address.Country) ?cardAccount.CreditCardInfo.Address.Country : "Not Available!"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Zip") %> 
                                </td>
                                <td>
                                    <%:!string.IsNullOrEmpty(cardAccount.CreditCardInfo.Address.Zip) ? cardAccount.CreditCardInfo.Address.Zip : "Not Available!"%>
                                </td>
                            </tr>
                            <tr>
                                <td><%: this.GetLocalResourceObject("lbl_Phone") %> 
                                </td>
                                <td>
                                    <%:!string.IsNullOrEmpty(cardAccount.CreditCardInfo.Address.Phone) ? cardAccount.CreditCardInfo.Address.Phone : "Not Available!"%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <% } %>
                </div>
            </section>
            <% } %>
            <% }
               else if (!UserIdentity.IsInRole(RoleConstants.ProductAdmin))
               { %>
            <% if (ViewBag.PayPalApiAccount == null && ViewBag.AuthorizeNetApiAccount == null)
               { %>
            <div class="alert alert-danger"><%: this.GetLocalResourceObject("e_PaymentSetup") %></div>
            <% }
               else
               { %>
            <section class="panel purple">
                <header class="panel-heading">
                    <h4>
                        <%: this.GetLocalResourceObject("h_SetupPaymentAccount") %>
                    </h4>
                </header>
                <div class="panel-body">
                    <table class="details_container_table" style="width: 100%;">
                        <colgroup>
                            <col style="width: 200px;">
                            <col style="width: auto;">
                        </colgroup>
                        <tbody>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_AccountType") %></label>
                                </td>
                                <td>
                                    <% if (ViewBag.PayPalApiAccount != null)
                                       { %>
                                    <input type="radio" name="accountType" value="PayPal" checked="checked" />
                                    <%: this.GetLocalResourceObject("lbl_PayPal") %>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <% } %>
                                    <% if (ViewBag.AuthorizeNetApiAccount != null)
                                       { %>
                                    <input type="radio" name="accountType" value="CreditCard" <%: (ViewBag.PayPalApiAccount == null ? "checked=\"checked\"" : "") %> />
                                    <%: this.GetLocalResourceObject("lbl_CreditCard") %>
                                    <% } %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_PayPalEmail") %></label>
                                </td>
                                <td>
                                    <%=Html.TextBox("Email") %>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="divCreditCardForm">
                                        <p>Clicking on submit you will be redirected to Authorize.NET site where you can create credit card details! On completion click "Return" button to return to this site.</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a class="btn btn-info" href="#" id="btnSubmit" title="<%: this.GetLocalResourceObject("t_ClikctoContinue") %>"><%=this.GetGlobalResourceObject("General","Submit") %></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            <% } %>
            <% }
               else if (Html.ValidationMessage("Error") != null)
               { %>
            <div class="alert alert-warning">
                <%: this.GetLocalResourceObject("e_managePaymentAccount") %>
            </div>
            <% }
               else if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
               { %>
            <div class="alert alert-warning">
                <%: this.GetLocalResourceObject("i_ISVTenant") %>
            </div>
            <% } %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#btnSubmit').attr('disabled', 'disabled');
            $('#divCreditCardForm').hide();

            $('#btnSubmit').click(function () {
                $('#btnSubmit').attr('disabled', 'disabled');

                var type = $('input[name=accountType]:checked').val();
                var email = $('input[name=Email]').val();

                if (type == 'PayPal') {
                    if (!email || email == '') {
                        alert(' <%: this.GetLocalResourceObject("i_ValidEmail") %>');
                        $('input[name=Email]').focus();
                        $('#btnSubmit').attr('disabled', null);
                        return false;
                    }

                    $('#bcontent').load('/Billing/PayPalPreapproval?email=' + email, function (html) {
                        $('#bcontent').hide().html(html).fadeIn();
                        $('#btnSubmit').attr('disabled', null);
                    });
                } else {
                    if ($('input[name=Token]').length > 0) {
                        $('#frmCreditCardAccount').submit();
                        return true;
                    } else {
                        $.post('/Billing/ManageCreditCardAccount', {}, function (data) {
                            $('#divCreditCardForm').html(data).show();
                            if ($('input[name=Token]').length > 0) {
                                $('#btnSubmit').trigger('click');
                            } else {
                                $('#btnSubmit').attr('disabled', null);
                            }
                        });
                    }
                }

                return false;
            });

            $('input[name=accountType]').click(function () {
                changeType();
            });

            function changeType() {
                var type = $('input[name=accountType]:checked').val();
                if (type == 'PayPal') {
                    $('#divCreditCardForm').hide();
                    $('input[name=Email]').closest('tr').show();
                    $('#btnSubmit').attr('disabled', null);
                } else {
                    $('#divCreditCardForm').show();
                    $('input[name=Email]').closest('tr').hide();
                    $('#btnSubmit').attr('disabled', null);
                }
            }

            changeType();

            $('#btnDeletePaymentAccount').click(function () {
                if (confirm('<%: this.GetLocalResourceObject("q_DeletePayments") %>')) {
                    return true;
                }
                return false;
            });

            $('#btnRenewPreapproval').click(function () {
                var email = $(this).data('email');
                $('#bcontent').load('/Billing/PayPalPreapproval?email=' + email, function (html) {
                    $('#bcontent').hide().html(html).fadeIn();
                });
                return false;
            });
        });
    </script>
</asp:Content>
