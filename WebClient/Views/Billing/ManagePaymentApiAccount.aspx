﻿<%@ Page Title="<%$ Resources:t_ManagePaymentAPIAccount %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Billing.Model.Invoice>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <%
        var payPalAccount = ViewBag.payPalAccount as PaymentApiAccount ?? new PaymentApiAccount();
        var creditCardAccount = ViewBag.creditCardAccount as PaymentApiAccount ?? new PaymentApiAccount();
    %>
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("h_PaymentAPIAccounts") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="alert alert-danger">
            <%:Html.ValidationSummary() %>
        </div>
        <% } %>
        <% if (ViewBag.Success != null)
           { %>
        <div class="alert alert-success">
            <%:ViewBag.Success %>
        </div>
        <% } %>
        <div class="panel indigo">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("lbl_PayPal") %>
                    <%--<img src="<%=Url.Content("~/Content/images/paypal.png") %>" style="width: 32px; margin-right: 5px;" alt="" />--%>
                </h4>
            </header>
            <section class="panel-body form-container">
                <form name="frmPaypalAccount" id="frmPaypalAccount" action="<%=Url.Action("ManagePaymentApiAccount") %>" role="form" method="post">
                    <table class="details_container_table">
                        <tbody>
                            <tr>
                                <td style="width: 200px;">
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_PayPalEmail") %></label>
                                </td>
                                <td>
                                    <%=Html.Hidden("prefix", "PayPal")%>
                                    <%=Html.Hidden("PayPal.Id", payPalAccount.Id)%>
                                    <%=Html.Hidden("PayPal.AccountType", PaymentAccountType.PayPal)%>
                                    <%=Html.TextBox("PayPal.Email", payPalAccount.Email, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("PayPal.Email","*") %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_ApplicationId") %></label>
                                </td>
                                <td>
                                    <%=Html.TextBox("PayPal.AppId", payPalAccount.AppId, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("PayPal.AppId", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_APIUserId") %></label>
                                </td>
                                <td>
                                    <%=Html.TextBox("PayPal.UserName", payPalAccount.UserName, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("PayPal.UserName", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_APIPassword") %></label>
                                </td>
                                <td>
                                    <%=Html.TextBox("PayPal.Password", payPalAccount.Password, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("PayPal.Password", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_APISignature") %></label>
                                </td>
                                <td>
                                    <%=Html.TextBox("PayPal.Signature", payPalAccount.Signature, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("PayPal.Signature", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_Mode") %></label>
                                </td>
                                <td>
                                    <select name="PayPal.Mode" style="width: 100%;">
                                        <option <%:payPalAccount.Mode ? "" : "selected" %> value="false"><%: this.GetLocalResourceObject("lbl_Sandbox") %></option>
                                        <option <%:payPalAccount.Mode ? "selected" : "" %> value="true"><%: this.GetLocalResourceObject("lbl_Live") %></option>
                                    </select>
                                    <%=Html.ValidationMessage("PayPal.Mode", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a class="btn btn-success" href="#frmPaypalAccount" onclick="$('#frmPaypalAccount').submit();" title="<%=this.GetGlobalResourceObject("General","Save") %>"><%=this.GetGlobalResourceObject("General","Save") %></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </section>
        </div>
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("h_AuthorizeNET") %>
                    <%--<img src="/Content/images/american-express.png" title="American Express" alt="American Express" />
                    <img src="/Content/images/discover.png" title="Discover" alt="Discover" />
                    <img src="/Content/images/master-card.png" title="MasterCard" alt="MasterCard" />
                    <img src="/Content/images/maestro.png" title="Maestro" alt="Maestro" />
                    <img src="/Content/images/visa.png" title="Visa" alt="Visa" />--%>
                </h4>
            </header>
            <div class="panel-body form-container">
                <form name="frmCreditCardAccount" id="frmCreditCardAccount" action="<%=Url.Action("ManagePaymentApiAccount") %>" role="form" method="post">
                    <table class="details_container_table">
                        <tbody>
                            <tr>
                                <td style="width: 200px;">
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_AuthorizeNETEmail") %></label>
                                </td>
                                <td>
                                    <%=Html.Hidden("prefix","CreditCard") %>
                                    <%=Html.Hidden("CreditCard.Id", creditCardAccount.Id)%>
                                    <%=Html.Hidden("CreditCard.AccountType", PaymentAccountType.CreditCard)%>
                                    <%=Html.TextBox("CreditCard.Email", creditCardAccount.Email, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("CreditCard.Email", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_APILoginId") %></label>
                                </td>
                                <td>
                                    <%=Html.TextBox("CreditCard.UserName", creditCardAccount.UserName, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("CreditCard.UserName", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_TransactionKey") %></label>
                                </td>
                                <td>
                                    <%=Html.TextBox("CreditCard.Password", creditCardAccount.Password, new { style = "width:450px;" })%>
                                    <%=Html.ValidationMessage("CreditCard.Password", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="mandatory"><%: this.GetLocalResourceObject("lbl_Mode") %></label>
                                </td>
                                <td>
                                    <select name="CreditCard.Mode" style="width: 100%;">
                                        <option <%:creditCardAccount.Mode ? "" : "selected" %> value="false"><%: this.GetLocalResourceObject("lbl_Test") %></option>
                                        <option <%:creditCardAccount.Mode ? "selected" : "" %> value="true"><%: this.GetLocalResourceObject("lbl_Live") %></option>
                                    </select>
                                    <%=Html.ValidationMessage("CreditCard.Mode", "*")%>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a class="btn btn-success" href="#frmCreditCardAccount" onclick="$('#frmCreditCardAccount').submit();" title="<%=this.GetGlobalResourceObject("General","Save") %>"><%=this.GetGlobalResourceObject("General","Save") %></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </section>
    </div>
    <style>
        .form-group label, .form-container label {
            display: block;
            text-align:right;
            margin-right:5px;
        }
    </style>
</asp:Content>
