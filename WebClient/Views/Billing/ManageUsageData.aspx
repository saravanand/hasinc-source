﻿<%@ Page Title="Insert Test Usage Data" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="heading_container">
        <h1>Insert Test Usage Data</h1>
    </div>
    <%
        string usageCode = ViewData["usageCode"] as string;
        var usages = ViewData["Usages"] as List<Usage> ?? new List<Usage>();
        usages.Add(new Usage { UsageCode = "", UsageName = "--Select--" });
        var lstUsages = new SelectList(usages, "UsageCode", "UsageName", usageCode);
    %>
    <div class="clearfix"></div>
    <% Html.RenderPartial("StatusMessage"); %>
    <div class="form-list">
        <%using (Html.BeginForm())
          { %>
        <table class="details_container_table" style="width: auto;">
            <tbody>
                <tr>
                    <td style="width: 200px;">
                        <label>Usage Code</label></td>
                    <td style="width: 230px;"><%=Html.DropDownList("usageCode",lstUsages,new{style="width:208px;"}) %></td>
                </tr>
                <tr>
                    <td>
                        <label>Amount</label></td>
                    <td><%=Html.TextBox("amount") %></td>
                </tr>
                <tr>
                    <td>
                        <label>Usage Date</label></td>
                    <td><%=Html.TextBox("usageDate", ((DateTime)ViewData["usageDate"]).ToUIDateString(), new { @class="datetime"})%></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="actions">
                            <div class="button">
                                <a href="#" onclick="$('form').submit();">Submit</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <% } %>
    </div>
    <div class="clearfix"></div>
    <div class="grid-part">
        <div class="heading_container" id="lblUsageCode">
        </div>
        <div id="divResult">
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select#usageCode').select2();
            $('input.datetime').datepicker({ format: '<%=HtmlExtensions.JQueryDateFormat(this.Html)%>' });

            $('select#usageCode').change(function () {
                var usageCode = $(this).val();
                $('#lblUsageCode').html("<h1>Metering Log details for '<i>" + usageCode + "</i>'</h1>");
                var data = { UsageCode: usageCode };
                $('#meteringLogDiv').hide();
                $.ajax({
                    url: '<%=Url.Action("MeteringLogDetails", "Audit", new { test= "True" })%>',
                    type: 'POST',
                    data: data,
                    complete: function (xhr) {
                        $('#divResult').html(xhr.responseText).fadeIn();
                    }
                });
            });

            $('select#usageCode').trigger('change');
        });
    </script>
</asp:Content>
