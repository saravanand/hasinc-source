﻿<%@ Page Title="<%$ Resources:t_ManageBills %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Billing.Model.Invoice>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <% if (this.Request.Path.Contains("MyBills") && UserIdentity.TenantID.Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase))
       { %>
    <div class="alert alert-info">
        <%: this.GetLocalResourceObject("e_MyBills") %>
    </div>
    <% }
       else
       { %>
    <%
           var viewChildBills = ViewData["viewChildBills"] != null ? (bool)ViewData["viewChildBills"] : false;
           var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails>;
           var viewTenantId = ViewData["viewTenantId"] as string;
    %>
    <div class="page-title">
        <h3>
            <%:viewChildBills ? this.GetLocalResourceObject("lbl_ManageChildBills").ToString() : this.GetLocalResourceObject("lbl_MyBills").ToString() %></h3>
    </div>
    <div class="row-fluid pd-25">
        <%
           if (tenantList != null && tenantList.Count > 0)
           {
               var tenantSelectList = tenantList.Values.OrderBy(x => x.TenantName).Select(x => new SelectListItem { Text = x.TenantName, Value = x.TenantCode }).ToList();
        %>
        <div class="content-box row">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 0px;">
                    <div class="search_holder">
                        <label style="float: left; line-height: 30px; margin-right: 10px;">
                            <%: this.GetLocalResourceObject("lbl_Tenant") %></label>
                        <div class="input-group">
                            <%=Html.DropDownList("viewTenantId", tenantSelectList, new { style="width:100%;" })%>
                            <span class="input-group-btn">
                                <a class="btn btn-info" href="#" id="btnSearch" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                                    <i class='fa fa-search'></i>&nbsp;<%:this.GetGlobalResourceObject("General","View") %></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
        <div id="invoiceDiv">
            <% if (viewChildBills && (tenantList == null || tenantList.Count == 0))
               { %>
            <div class="alert alert-info"><%: this.GetLocalResourceObject("e_Tenant") %></div>
            <% }
               else
               { %>
            <% Html.RenderPartial("PartialCurrentInvoice", Model); %>
            <% } %>
        </div>
        <script type="text/javascript">
            var canTrigger = <%=string.IsNullOrEmpty(viewTenantId).ToString().ToLower() %>;
        </script>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript" async></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript" async></script>
    <script type="text/javascript">
        $(function () {
            $('#btnSearch').click(function (e) {
                var viewTenantId = $('select[name=viewTenantId]').val();
                $.post('/Billing/ManageChildBills', { viewTenantId: viewTenantId }, function (data) {
                    $('#invoiceDiv').html(data);
                });
                return false;
            });

            $('select[name=viewTenantId]').select2();
            //$('select[name=viewTenantId]').trigger('change');
            if (canTrigger)
                $('#btnSearch').trigger('click');


            $('#tenantLicenseHistory').modal({ show: false });

            $(document).on('click', '#viewHistory', function () {
                $('#tenantLicenseHistory').modal('show');
                return false;
            });
        });
    </script>
</asp:Content>
