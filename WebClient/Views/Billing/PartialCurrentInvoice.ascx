﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Billing.Model.Invoice>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Billing.ServiceProxies" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>
<% 
    var invoice = Model;
    var tenant = ViewData["TenantDetails"] as Tenant ?? new Tenant { TenantDetails = new TenantDetails() };
    var tenantId = string.IsNullOrEmpty(tenant.TenantDetails.TenantCode) ? Guid.Empty : Guid.Parse(tenant.TenantDetails.TenantCode);
    var billStatistics = ViewData["BillStatistics"] as BillStatistics;
    var licenseHistory = ViewData["LicenseHistory"] as List<TenantLicense>;
    var tenantLicense = ViewData["TenantLicense"] as TenantLicense;
    var pricePlans = ViewData["PricePlans"] as Dictionary<Guid, PricePlan>;
    var chargeDate = ViewData["chargeDate"] != null ? (DateTime)ViewData["chargeDate"] : DateTime.MinValue;
    var mode = ViewData["BillCycleMode"] != null ? (BillCycleMode)ViewData["BillCycleMode"] : BillCycleMode.Postpaid;
    int totalBills = 0;
    double totalAmount = 0.0, totalPaid = 0.0, overdue = 0.0;

    string currentPackageName = tenantLicense != null ? tenantLicense.PackageName : "-";
    string currentPricePlanName = (tenantLicense != null && !string.IsNullOrEmpty(tenantLicense.PricePlanId)
                                    && pricePlans != null
                                    && pricePlans.ContainsKey(Guid.Parse(tenantLicense.PricePlanId)))
                                    ? pricePlans[Guid.Parse(tenantLicense.PricePlanId)].Name : "-";
    string subscriptionPeriod = tenantLicense != null ? string.Format("{0}{1}", tenantLicense.ValidityStart.ToUIDateTimeString(), tenantLicense.ValidityEnd.HasValue ? " - " + tenantLicense.ValidityEnd.Value.ToUIDateTimeString() : string.Empty) : "-";

    if (tenantLicense != null && tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value < DateTime.Now)
    {
        currentPackageName += "&nbsp;&nbsp;<i title='" + this.GetLocalResourceObject("e_Subscription").ToString() + "' class='text-danger fa fa-bell'></i>";
    }

    if (billStatistics != null)
    {
        totalBills = billStatistics.TotalBills;
        totalAmount = billStatistics.TotalAmount;
        totalPaid = billStatistics.TotalPaid;
        overdue = billStatistics.Overdue;
    }

    var canEdit = UserIdentity.HasPrivilege(PrivilegeConstants.EditInvoice) && !tenantId.ToString().Equals(UserIdentity.TenantID, StringComparison.OrdinalIgnoreCase);
    canEdit = canEdit && chargeDate != DateTime.Today;
    ViewBag.canEdit = canEdit;
%>
<% Html.RenderPartial("StatusMessage"); %>
<section class="panel purple">
    <header class="panel-heading">
        <h4><%: this.GetLocalResourceObject("lbl_AccountSummary") %>
        </h4>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label><%: this.GetLocalResourceObject("lbl_TenantName") %></label>
                    </div>
                    <div class="col-md-8">
                        <%:tenant.TenantDetails.TenantName%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label><%: this.GetLocalResourceObject("lbl_Email") %></label>
                    </div>
                    <div class="col-md-8">
                        <% if (tenant.ContactDetail != null && !string.IsNullOrEmpty(tenant.ContactDetail.Email))
                           {
                        %>
                        <i class='fa fa-envelope'></i>&nbsp;<a href="mailto:<%:tenant.ContactDetail.Email %>"><%:tenant.ContactDetail.Email %></a>
                        <% }
                           else
                           { %>
                        NA
                        <% } %>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label><%: this.GetLocalResourceObject("lbl_Phone") %></label>
                    </div>
                    <div class="col-md-8">
                        <%=tenant.ContactDetail != null && !string.IsNullOrEmpty(tenant.ContactDetail.Phone) ? "<i class='fa fa-phone'></i>&nbsp;" + tenant.ContactDetail.Phone : "NA"%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_Package") %></label>
                        <% if (licenseHistory != null && licenseHistory.Count > 0)
                           {
                        %>
                        <a style="font-size: 9px;" href="#" id="viewHistory"><i class="fa fa-search"></i>
                            <%: this.GetLocalResourceObject("lbl_ViewHistory") %> </a>
                        <% } %>
                    </div>
                    <div class="col-md-8">
                        <%=currentPackageName%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_PricePlan") %></label>
                    </div>
                    <div class="col-md-8">
                        <%:currentPricePlanName%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_SubscriptionPeriod") %></label>
                    </div>
                    <div class="col-md-8">
                        <%:subscriptionPeriod %>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_TotalBills") %></label>
                    </div>
                    <div class="col-md-8">
                        <%:totalBills%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_TotalAmount") %></label>
                    </div>
                    <div class="col-md-8">
                        <strong class="text-info">
                            <%:totalAmount.ToBillingCurrency()%></strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_TotalPaid") %></label>
                    </div>
                    <div class="col-md-8">
                        <%:totalPaid.ToBillingCurrency()%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_OverdueAmount") %></label>
                    </div>
                    <div class="col-md-8">
                        <strong class="text-danger">
                            <%:overdue.ToBillingCurrency()%></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<% if (invoice != null)
   { %>
<section class="panel red">
    <header class="panel-heading">
        <h4>
            <%: mode == BillCycleMode.Prepaid && DateTime.Today < invoice.StartDate.Date ? this.GetLocalResourceObject("h_NextBillStatement") : this.GetLocalResourceObject("h_CurrentBillStatement") %>            
        </h4>
        <div class="pull-right">
            <div class="btn-group">
                <% if (canEdit)
                   { %>
                <a class="btn btn-sm btn-success" href="<%=Url.Action("ManageInvoice", new { invoiceId = invoice.Id, tenantId = invoice.TenantId }) %>"
                    title="<%: this.GetLocalResourceObject("t_Editthisinvoice") %>"><i class="fa fa-pencil"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Edit") %></a>
                <% } %>
                <a class="btn btn-sm btn-info" href="<%=Url.Action("PreviewInvoice", new { invoiceId = invoice.Id, tenantId = invoice.TenantId }) %>"
                    title="<%: this.GetLocalResourceObject("t_ViewThisinvoice") %>"><i class="fa fa-eye"></i>&nbsp;<%=this.GetGlobalResourceObject("General","View") %> </a>
                <a class="btn btn-sm btn-warning" target="_blank" href="<%=Url.Action("DownloadInvoice", new { invoiceId = invoice.Id, tenantId = invoice.TenantId }) %>"
                    title="<%: this.GetLocalResourceObject("t_Download") %>"><i class="fa fa-download"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_Download") %></a>
            </div>
        </div>
    </header>
    <div class="panel-body">
        <table class="table table-condensed">
            <thead>
                <tr class="subHeading">
                    <%-- <th><%: this.GetLocalResourceObject("lbl_PreviousBalance") %>
                    </th>--%>
                    <th><%: this.GetLocalResourceObject("lbl_InvoiceNo") %>
                    </th>
                    <th><%: this.GetLocalResourceObject("lbl_BillPeriod") %> 
                    </th>
                    <th><%: this.GetLocalResourceObject("lbl_InvoiceDate") %>
                    </th>
                    <th><%: this.GetLocalResourceObject("lbl_DueDate") %> 
                    </th>
                    <th class="tright"><%: this.GetLocalResourceObject("lbl_BillAmount") %> 
                    </th>
                    <%--<th class="tright"><%: this.GetLocalResourceObject("lbl_TotalAmountPayable") %>
                    </th>--%>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <%-- <td>
                        <%=invoice.PendingBalance.ToBillingCurrency() %>
                    </td>--%>
                    <td>
                        <%=invoice.InvoiceNo %>
                    </td>
                    <td>
                        <%=invoice.StartDate.ToUIDateString() %>
                -
                <%=invoice.EndDate.ToUIDateString() %>
                    </td>
                    <td>
                        <%= invoice.InvoiceDate.ToUIDateString()%>
                    </td>
                    <td>
                        <%=invoice.DueDate.ToUIDateString() %>
                    </td>
                    <td class="tright">
                        <strong><%=invoice.Amount.ToBillingCurrency() %></strong>
                    </td>
                    <%--<td class="tright">
                        <strong>
                            <%=invoice.NetAmount.ToBillingCurrency() %></strong>
                    </td>--%>
                </tr>
            </tbody>
        </table>
    </div>
</section>
<% }
   else
   { %>
<div class="alert alert-info">
    <% if (Convert.ToBoolean(ViewData["NoBillingPlan"] ?? "False"))
       { %>
    <%: this.GetLocalResourceObject("e_PricingPlan") %>
    <% } if (Convert.ToBoolean(ViewData["TenantDeactivated"] ?? "False"))
       { %>
    <%:this.GetLocalResourceObject("e_LicenseDeactivated") %>
    <% }
       else if (chargeDate != DateTime.MinValue)
       { %>
    <%: this.GetLocalResourceObject("m_CurrentCharges") %> (<%:chargeDate.ToUIDateTimeString() %>)
    <% }
       else
       { %>
    <%: this.GetLocalResourceObject("e_CurrentCharges") %>
    <% } %>
</div>
<% } %>
<div class="row">
    <div class="col-md-6">
        <% if (invoice != null && invoice.LineItems != null && invoice.LineItems.Count > 0)
           { %>
        <section class="panel green">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("h_CurrentBillChargeDetails") %></h4>
            </header>
            <div class="panel-body">
                <% Html.Grid(invoice.LineItems).Columns(col =>
               {
                   col.For(x => x.Name).Named(this.GetLocalResourceObject("lbl_ItemName").ToString());
                   col.For(x => x.Description).Named(this.GetLocalResourceObject("lbl_Description").ToString());
                   col.For(x => x.Amount.ToBillingCurrency()).Named(this.GetLocalResourceObject("lbl_Amount").ToString()).Attributes(@class => "tright").HeaderAttributes(@class => "tright");
               }).Attributes(@class => "table table-condensed").Render();
                %>
            </div>
        </section>
        <% } %>
    </div>
    <div class="col-md-6">
        <section class="panel blue">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("h_PreviousBills") %></h4>
            </header>
            <div class="panel-body">
                <% Html.RenderPartial("PreviousInvoices"); %>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <% Html.RenderPartial("BillStatistics"); %>
    </div>
    <div class="col-md-6">
        <section class="panel green" id="usageChartConfiguration">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("lbl_UsageStatistics") %></h4>
            </header>
            <div class="panel-body">
                <% Html.RenderPartial("~/Views/Dashboard/UsageAlert.ascx"); %>
            </div>
        </section>
    </div>
</div>
<% Html.RenderPartial("PartialLicenseHistory", licenseHistory); %>
