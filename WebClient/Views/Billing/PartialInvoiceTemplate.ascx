﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Billing.Model.Invoice>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%
    var tenant = ViewData["TenantDetails"] as Tenant;
    var parentTenant = ViewData["ParentTenant"] as Tenant ?? new Tenant();
    string logo = ViewData["ParentTenantLogo"] as string;
%>
<div class="col-md-10 col-md-offset-1">
    <div class="grid simple">
        <div class="grid-body no-border invoice-body">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td style="width: 48%; vertical-align: top;">
                            <div style="padding: 6px;">
                                <div class="row">
                                    <div style="line-height: 23px;">
                                        <div>
                                            <%=Html.Logo(parentTenant.TenantDetails.TenantCode) %>
                                            <h2 style="font-size: 2em; margin-top: 5px; margin-bottom: 20px;">
                                                <%=parentTenant.TenantDetails.TenantName %></h2>
                                        </div>
                                        <div style="margin-left: 6px;">
                                            <% if (parentTenant.Address != null)
                                               { %>
                                            <div>
                                                <%=parentTenant.Address.Address1 %>
                                                <%=parentTenant.Address.Address2 %>
                                            </div>
                                            <div>
                                                <%=parentTenant.Address.State%>
                                            </div>
                                            <div>
                                                <%=parentTenant.Address.CountryName%> - <%=parentTenant.Address.PostalCode%>
                                            </div>
                                            <% } %>
                                            <br />
                                            <% if (parentTenant.ContactDetail != null)
                                               { %>
                                            <div>
                                                <i class="fa fa-phone"></i>
                                                <span>
                                                    <%=parentTenant.ContactDetail.Phone%></span>
                                            </div>
                                            <div>
                                                <i class="fa fa-envelope"></i>
                                                <span>
                                                    <%=parentTenant.ContactDetail.Email%></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td style="vertical-align: top;">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <h2 style="font-size: 2.1em; margin-bottom: 20px;"><%: this.GetLocalResourceObject("lbl_INVOICE") %></h2>
                                    <div class="date"><%=Model.InvoiceDate.ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat()) %></div>
                                    <div class="invoiceno"><%: this.GetLocalResourceObject("lbl_InvoiceNo") %>&nbsp;<b><%=Model.InvoiceNo %></b></div>
                                    <div class="dateperiod"><%: this.GetLocalResourceObject("lbl_Period") %>&nbsp;<%=Model.StartDate.ToUIDateString() %> - <%=Model.EndDate.ToUIDateString() %></div>
                                    <div class="invoice-status"><%=Model.InvoiceStatus != InvoiceStatus.Current ? "Status:&nbsp;" + Model.InvoiceStatus.ToString() : string.Empty %></div>
                                    <div class="clientname" style="margin: 20px 0;">
                                        <h2 style="font-size: 2em; padding: 0; margin: 0; margin-bottom: 5px;">
                                            <%=tenant.TenantDetails.TenantName %></h2>
                                    </div>
                                    <% if (tenant.Address != null)
                                       { %>
                                    <div>
                                        <%=tenant.Address.Address1 %>
                                        <%=tenant.Address.Address2 %>
                                    </div>
                                    <div>
                                        <%=tenant.Address.State%>
                                    </div>
                                    <div>
                                        <%=tenant.Address.CountryName%> - <%=tenant.Address.PostalCode%>
                                    </div>
                                    <% } %><br />
                                    <% if (tenant.ContactDetail != null)
                                       { %>
                                    <div>
                                        <i class="fa fa-phone"></i>
                                        <span>
                                            <%=tenant.ContactDetail.Phone%></span>
                                    </div>
                                    <div>
                                        <i class="fa fa-envelope"></i>
                                        <span>
                                            <%=tenant.ContactDetail.Email%></span>
                                    </div>
                                    <% } %>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="lineItemsDiv" style="margin-top: 15px;">
                <% 
                    var lstTaxItems = Model.LineItems.Where(x => x.Type == "Tax");
                    var lstAdjustmentItems = Model.LineItems.Where(x => x.Type == "Adjustment");
                    bool hasUsage = false;
                    var startDate = Model.StartDate.ToUIDateString();
                    var endDate = Model.EndDate.ToUIDateString();
                    foreach (var item in Model.LineItems.Where(x => !string.IsNullOrEmpty(x.Usage) && x.Usage.Contains(':')))
                    {
                        hasUsage = true;
                        var usages = item.Usage.Split(';');
                        item.Usage = string.Empty;
                        foreach (var u in usages)
                        {
                            var v = u.Split(':');
                            var code = v[0].Trim();
                            var amt = v[1].Trim();
                            if (v.Length == 4)
                            {
                                startDate = Convert.ToDateTime(v[2], System.Globalization.CultureInfo.CurrentCulture).ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat());
                                endDate = Convert.ToDateTime(v[3], System.Globalization.CultureInfo.CurrentCulture).AddDays(1).ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat());
                                // startDate = v[2];
                                // endDate = DateTime.Parse(v[3]).AddDays(1).ToShortDateString();
                                
                            }
                            item.Usage += string.Format("<a target='_blank' href='{2}' title='" + this.GetLocalResourceObject("t_ViewUsageLog").ToString() + "'>{0}</a>: {1}<br/>",
                            code,
                            amt,
                            Url.Action("MeteringLogDetails", "Audit",
                            new
                            {
                                UsageCode = code,
                                TenantId = item.TenantId,
                                StartDate = startDate,
                                EndDate = endDate
                            }));
                        }
                    }

                    Html.Grid(Model.LineItems.Where(x => x.Type != "Tax" && x.Type != "Adjustment").OrderBy(x => x.IsUserAdded)).Columns(col =>
                    {
                        col.For(x => x.Name)
                            .Named(this.GetLocalResourceObject("lbl_ItemName").ToString())
                            .Attributes(@style => "vertical-align:top;")
                            .HeaderAttributes(@style => "text-align:left;min-width:220px;width:auto;");
                        col.For(x => string.Format("{0}{1}{2}", x.Description, !string.IsNullOrEmpty(x.Usage) ? "<br/>Usage:<br/>" : "", hasUsage ? x.Usage : ""))
                            .Named(this.GetLocalResourceObject("lbl_Description").ToString())
                            .Attributes(@style => "vertical-align:top;")
                            .HeaderAttributes(@style => "text-align:left;min-width:300px;")
                            .DoNotEncode();
                        col.For(x => x.Amount.ToBillingCurrency())
                            .Named(this.GetLocalResourceObject("lbl_Amount").ToString())
                            .HeaderAttributes(style => "text-align:right;min-width:140px;")
                            .Attributes(style => "text-align:right;vertical-align:top;");
                    }).Attributes(@style => "width: 100%;line-height: 23px;").Render();
                %>
                <table style="width: 100%; line-height: 23px;">
                    <tbody>
                        <tr>
                            <td style="text-align: right" colspan="4">
                                <strong><%: (lstAdjustmentItems.Count() > 0 || lstTaxItems.Count() > 0) ? this.GetLocalResourceObject("lbl_SubTotal") : this.GetLocalResourceObject("lbl_Total") %></strong>
                            </td>
                            <td style="width: 140px; text-align: right;">
                                <strong>
                                    <%=Model.Subtotal.ToBillingCurrency() %>
                                </strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <% if (lstTaxItems.Count() > 0)
               { %>
            <div class="taxesDiv" style="margin-top: 15px;">
                <h3 class="blueHeading"><%: this.GetLocalResourceObject("lbl_Taxes") %></h3>
                <% Html.Grid(lstTaxItems).Columns(col =>
                       {
                           col.For(x => x.Name)
                               .Named(this.GetLocalResourceObject("lbl_ItemName").ToString()).HeaderAttributes(@style => "text-align:left;min-width:220px;width:auto;");
                           col.For(x => x.Description)
                               .Named(this.GetLocalResourceObject("lbl_Description").ToString()).HeaderAttributes(@style => "text-align:left;min-width:300px;");
                           col.For(x => x.Amount.ToBillingCurrency())
                               .Named(this.GetLocalResourceObject("lbl_Amount").ToString()).HeaderAttributes(style => "text-align:right;min-width:140px;")
                               .Attributes(style => "text-align:right;");
                       }).Attributes(@style => "width: 100%;line-height: 23px;").Render();
                %>
            </div>
            <% }%>
            <% if (lstAdjustmentItems.Count() > 0)
               { %>
            <div class="adjustmentsDiv" style="margin-top: 15px;">
                <h3 class="blueHeading"><%: this.GetLocalResourceObject("lbl_Adjustments") %></h3>
                <% Html.Grid(lstAdjustmentItems).Columns(col =>
                       {
                           col.For(x => x.Name)
                               .Named(this.GetLocalResourceObject("lbl_ItemName").ToString()).HeaderAttributes(@style => "text-align:left;min-width:220px;width:auto;");
                           col.For(x => x.Description ?? "-")
                               .Named(this.GetLocalResourceObject("lbl_Description").ToString()).HeaderAttributes(@style => "text-align:left;min-width:300px;");
                           col.For(x => x.Amount.ToBillingCurrency())
                               .Named(this.GetLocalResourceObject("lbl_Amount").ToString()).HeaderAttributes(style => "text-align:right;min-width:140px;")
                               .Attributes(style => "text-align:right;");
                       }).Attributes(@style => "width: 100%;line-height: 23px;").Render();
                %>
            </div>
            <% } %>
            <table style="width: 100%; line-height: 23px;">
                <tbody>
                    <% if (lstTaxItems.Count() > 0)
                       { %>
                    <tr>
                        <td style="text-align: right" colspan="4">
                            <strong><%: this.GetLocalResourceObject("lbl_TaxTotal") %></strong>
                        </td>
                        <td style="width: 140px; text-align: right;">
                            <strong>
                                <%=Model.TaxAmount.ToBillingCurrency() %>
                            </strong>
                        </td>
                    </tr>
                    <% } %>
                    <% if (lstAdjustmentItems.Count() > 0)
                       { %>
                    <tr>
                        <td style="text-align: right" colspan="4">
                            <strong><%: this.GetLocalResourceObject("lbl_AdjTotal") %></strong>
                        </td>
                        <td style="width: 140px; text-align: right;">
                            <strong>
                                <%=Model.AdjustmentAmount.ToBillingCurrency() %>
                            </strong>
                        </td>
                    </tr>
                    <% } %>
                    <% if (lstTaxItems.Count() > 0 || lstAdjustmentItems.Count() > 0)
                       { %>
                    <tr>
                        <td style="text-align: right" colspan="4">
                            <strong><%: this.GetLocalResourceObject("lbl_Total") %></strong>
                        </td>
                        <td style="width: 140px; text-align: right;">
                            <strong>
                                <%=Model.Amount.ToBillingCurrency() %>
                            </strong>
                        </td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
            <div id="paymentsDiv" style="margin-top: 15px;">
                <div class="blueHeading">
                    <h3><%: this.GetLocalResourceObject("lbl_Payments") %></h3>
                </div>
                <% if (Model.Payments != null && Model.Payments.Count > 0)
                   { %>
                <%
                       Html.Grid(Model.Payments).Columns(col =>
                       {
                           col.For(x => x.PaymentMode.ToString()).Named(this.GetLocalResourceObject("lbl_Mode").ToString()).HeaderAttributes(style => "width:160px;");
                           col.For(x => x.PaymentDate.ToUIDateString()).Named(this.GetLocalResourceObject("lbl_Date").ToString()).HeaderAttributes(style => "width:210px;");
                           col.For(x => x.PaidAmount.ToBillingCurrency())
                               .Named(this.GetLocalResourceObject("lbl_PaidAmount").ToString())
                               .Attributes(@class => "tright")
                               .HeaderAttributes(@class => "tright", style => "width:180px;");
                           col.For(x => x.Notes ?? "--").Named(this.GetLocalResourceObject("lbl_Notes").ToString()).HeaderAttributes(style => "min-width:200px;");
                       }).Attributes(@class => "table table-condensed", id => "tblPayments").Render();
                %>
                <% }
                   else
                   { %>
                <div class="info">
                    <%: this.GetLocalResourceObject("e_Payments") %>
                </div>
                <% } %>
            </div>
            <div id="notesDiv" style="margin-top: 15px;">
                <div class="blueHeading">
                    <h3><%: this.GetLocalResourceObject("lbl_Notes") %></h3>
                </div>
                <div style="padding: 10px;">
                    <%=string.IsNullOrEmpty(Model.Memo) ? "--" : Model.Memo%>
                </div>
            </div>
        </div>
    </div>
</div>
