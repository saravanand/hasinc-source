﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    var accountDetails = ViewBag.AccountDetails as PaymentAccount;
    var preapprovalResponse = ViewBag.PreapprovalResponse as PreapprovalResponse;
%>
<div class="clearfix">
</div>
<div class="center w70 form-container">
    <div class="heading"><%: this.GetLocalResourceObject("lbl_PayPalPreapproval") %></div>
    <div class="form-content">
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (preapprovalResponse != null)
           { %>
        <p style="font-size: 20px; padding: 0 0 20px 0;">
          <%: this.GetLocalResourceObject("m_PreApprovalKey") %> <b><%:preapprovalResponse.PreapprovalKey %>)</b>.
      <%: this.GetLocalResourceObject("m_ApproveRequest") %>  
        </p>
        <div style="margin: 10px auto;text-align:center;">
            <div class="button">
                <a href="<%:preapprovalResponse.ReturnUrl %>" id="btnApprove"><%: this.GetLocalResourceObject("lbl_GotoPayPal") %></a>
            </div>
        </div>
        <p>
            <%: this.GetLocalResourceObject("m_ClickButton") %>            
        </p>
        <% }
           else
           { %>
        <%--<div class="error">
            We have encountered an error while making pre-approval request. Please verify the
        email address or try again later!
        </div>--%>
        <div style="margin:0 auto;text-align:center;">
            <div class="button">
                <a href="<%=Url.Action("ManagePaymentAccount") %>" id="btnCancelPreapproval"><%=this.GetGlobalResourceObject("General","Back") %></a>
            </div>
        </div>
        <% } %>
    </div>
</div>
