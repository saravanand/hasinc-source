﻿<%@ Page Title="<%$ Resources:t_PaymentHistory %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Billing.Model.Invoice>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <%
        var showTenantIdColumn = ViewData["ShowTenantIdColumn"] != null ? (bool)ViewData["ShowTenantIdColumn"] : false;
        var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails> ?? new Dictionary<string, TenantDetails>();
        var searchCondition = ViewData["SearchCondition"] as PaymentAuditSearchCondition ?? new PaymentAuditSearchCondition();

        var tenantSelectList = tenantList.Values.OrderBy(x => x.TenantName).Select(x => new SelectListItem { Text = x.TenantName, Value = x.TenantCode }).ToList();
        tenantSelectList.Insert(0, new SelectListItem { Text = "All", Value = "" });

        var accountTypeSelectList = typeof(PaymentAccountType).ToSelectList(addEmpty: true);

        var statusSelectList = new List<SelectListItem>();
        statusSelectList.Add(new SelectListItem { Text = "ALL", Value = "" });
        statusSelectList.Add(new SelectListItem { Text = "COMPLETED", Value = "COMPLETED" });
        statusSelectList.Add(new SelectListItem { Text = "FAILURE", Value = "FAILURE" });
        statusSelectList.Add(new SelectListItem { Text = "ERROR", Value = "ERROR" });
    %>
    <div class="page-title">
        <h3>
            <%:showTenantIdColumn ? this.GetLocalResourceObject("h_Child").ToString() : this.GetLocalResourceObject("h_My").ToString() %><%: this.GetLocalResourceObject("h_PaymentHistory") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetLocalResourceObject("lbl_Filter") %></h4>
            </header>
            <div class="panel-body form-container">
                <form id="frmPaymentAuditSearch" name="frmPaymentAuditSearch" action="">
                    <div class="row">
                        <% if (showTenantIdColumn)
                           { %>
                        <div class="col-md-6">
                            <div class="search_holder">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_Tenant") %>
                                </label>
                                <%=Html.DropDownList("TenantIds", tenantSelectList, new { style="width:100%" })%>
                            </div>
                        </div>
                        <% }
                           else
                           { %>
                        <%=Html.Hidden("TenantIds", UserIdentity.TenantID)%>
                        <% } %>
                        <div class="col-md-6">
                            <div class="search_holder">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_Status") %></label>
                                <%=Html.DropDownList("Status", statusSelectList, new { style="width:100%" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="search_holder">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_TransactionId") %></label>
                                <%=Html.TextBox("TransactionId", searchCondition.TransactionId)%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="search_holder">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_InvoiceNo") %></label>
                                <%=Html.TextBox("InvoiceNo", searchCondition.InvoiceNo)%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="search_holder">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_FromDate") %></label>
                                <%=Html.TextBox("FromDate", searchCondition.FromDate.HasValue ? searchCondition.FromDate.Value.ToUIDateString() : "", new { @class = "datetime" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="search_holder">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_ToDate") %></label>
                                <%=Html.TextBox("ToDate", searchCondition.ToDate.HasValue ? searchCondition.ToDate.Value.ToUIDateString() : "", new { @class = "datetime" })%>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="pull-right">
                        <button type="submit" class="btn btn-info" id="btnSearch" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                            <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%>
                        </button>
                        <button type="button" class="btn btn-default" id="btnReset" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                            <%:this.GetGlobalResourceObject("General", "Reset")%></button>
                    </div>
                </form>
            </div>
        </section>
        <div class="grid simple">
            <div class="grid-body">
                <div id="divGrid">
                    <% Html.RenderPartial("PaymentAuditsGrid"); %>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalErroDisplay" tabindex="-1" role="dialog" aria-labelledby="modalErroDisplay" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Payment Details</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><%:this.GetGlobalResourceObject("General", "Close")%></button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#frmPaymentAuditSearch').submit(function (e) {
                var postdata = $('#frmPaymentAuditSearch').serialize();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                $.post($('#frmPaymentAuditSearch').attr('action'), postdata, function (data) {
                    $('#divGrid').hide().html(data).fadeIn();
                });
                return false;
            });

            $('#btnReset').click(function () {
                $('#frmPaymentAuditSearch')[0].reset();
                $('select[name=TenantIds],select[name=Status],select[name=AccountType]').trigger('change');
                $('#btnSearch').trigger('click');
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var postdata = $('#frmPaymentAuditSearch').serialize();
                postdata += '&pageSize=' + $(this).val();
                $.post($('#frmPaymentAuditSearch').attr('action'), postdata, function (data) {
                    $('#divGrid').hide().html(data).fadeIn();
                });
            });

            $(document).on('click', '#tblPaymentAudits a.perror', function () {
                var msg = $('.err', $(this).parent()).html();
                $('#modalErroDisplay .modal-body').html(msg || 'Details not available!');
                $('#modalErroDisplay').modal('show');
            });

            $('#modalErroDisplay').modal({ show: false });

            $('select[name=TenantIds]').select2();
            $('select[name=AccountType]').select2();
            $('select[name=Status]').select2();

            $('input.datetime').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });
        });
    </script>
    <style type="text/css">
        .search_holder label {
            display: inline-block;
            width: 100px;
        }

        .search_holder select {
            width: 220px;
        }
    </style>
</asp:Content>
