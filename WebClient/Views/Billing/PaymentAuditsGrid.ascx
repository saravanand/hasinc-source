﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    var audits = ViewBag.PaymentAudits as Dictionary<Guid, PaymentAudit> ?? new Dictionary<Guid, PaymentAudit>();
    var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails>; 
%>
<% Html.RenderPartial("StatusMessage"); %>
<div class="grid-part" id="paymentsAuditGridDiv">
    <% if (audits != null && audits.Count > 0)
       { %>
    <%
           var showTenantIdColumn = ViewData["ShowTenantIdColumn"] != null ? (bool)ViewData["ShowTenantIdColumn"] : false;
           var searchCondition = ViewData["SearchCondition"] as PaymentAuditSearchCondition ?? new PaymentAuditSearchCondition();
           int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
           int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;
           int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
           var ajaxOpt = new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementId = "loading", LoadingElementDuration = 300 };

           var routeValues = new
           {
               TenantIds = searchCondition.TenantIds.Length > 1 ? string.Empty : string.Join(",", searchCondition.TenantIds),
               FromDate = searchCondition.FromDate,
               ToDate = searchCondition.ToDate,
               AccountType = searchCondition.AccountType,
               Status = searchCondition.Status,
               TransactionId = searchCondition.TransactionId,
               InvoiceNo = searchCondition.InvoiceNo
           };

           Html.Grid(audits.Values).Columns(col =>
           {
               col.For(x => string.Format("<a target='_blank' href='{0}' title='" + this.GetLocalResourceObject("t_ClickToView").ToString() + "'>{1}</a>", Url.Action("PreviewInvoice", new { invoiceId = x.InvoiceId, tenantId = x.TenantId }), x.InvoiceNo)).Named(this.GetLocalResourceObject("lbl_InvoiceNo").ToString()).DoNotEncode();
               col.For(x => tenantList.ContainsKey(x.TenantId.ToString()) ? tenantList[x.TenantId.ToString()].TenantName : "")
                   .Named(this.GetLocalResourceObject("lbl_TenantName").ToString())
                   .Attributes(style => "min-width:120px;")
                   .Visible(showTenantIdColumn)
                   .DoNotEncode();
               col.For(x => x.AccountType.ToString()).Named(this.GetLocalResourceObject("lbl_Type").ToString());
               col.For(x => x.TransactionId ?? "-").Named(this.GetLocalResourceObject("lbl_TransactionId").ToString()).Attributes(style => "min-width:150px;").DoNotEncode();
               col.For(x => x.TransactionDate.ToUIDateTimeString()).Named(this.GetLocalResourceObject("lbl_Date").ToString()).Attributes(style => "min-width:90px;").DoNotEncode();
               col.For(x => x.Amount.ToBillingCurrency()).Named(this.GetLocalResourceObject("lbl_Amount").ToString()).Attributes(style => "min-width:80px;text-align:right;").HeaderAttributes(style => "text-align:right;").DoNotEncode(); ;
               col.For(x => x.Attempt).HeaderAttributes(@class => "text-right").Attributes(@class => "text-right").Named(this.GetLocalResourceObject("lbl_Attempt").ToString());
               col.For(x => string.Format("<div title='{1}'><a href='#' class='perror' title='Click to view the details!'>{0}</a><div class='err' style='display:none;'>{1}</div></div>",
                   x.Status,
                   x.Details)).Named(this.GetLocalResourceObject("lbl_Status").ToString()).DoNotEncode();
           })
           .RowAttributes(x => new Dictionary<string, object>() { { "class", (x.Item.Status == "ERROR" ? "danger text-danger" : "") } })
           .Attributes(id => "tblPaymentAudits", @class => "celloTable").Render();
    %>
    <%
           Ajax.CelloPager(ajaxOpt, routeValues).SetTotalCount(totalCount).SetPageNumber(pageNumber).SetPageSize(pageSize)
           .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
           .Render();
    %>
    <% }
       else
       { %>
    <div class="alert alert-info">
        <%: this.GetLocalResourceObject("e_PaymentAudits") %>
    </div>
    <% } %>
</div>
