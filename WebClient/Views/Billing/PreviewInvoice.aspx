﻿<%@ Page Title="<%$ Resources:t_PreviewInvoice %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Billing.Model.Invoice>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% var refAction = this.Request.UrlReferrer != null ? this.Request.UrlReferrer.AbsolutePath : string.Empty;
       var canEdit = UserIdentity.HasPrivilege(PrivilegeConstants.EditInvoice)
          && (Model != null && this.Model.InvoiceStatus == InvoiceStatus.Current && !Model.TenantId.ToString().Equals(UserIdentity.TenantID, StringComparison.OrdinalIgnoreCase)); %>
    <div id="loading"></div>
    <div class="page-title">
        <% if (refAction == "/Billing/MyBills")
           { %>
        <a href="<%=Url.Action("MyBills") %>"
            title="<%: this.GetLocalResourceObject("t_GoBackToMyBills") %>">
            <i class="icon-custom-left"></i></a>
        <% }
           else
           { %>
        <a href="<%=Url.Action("ManageChildBills", new { viewTenantId=this.Model!=null ? this.Model.TenantId.ToString() : ""}) %>"
            title="<%: this.GetLocalResourceObject("t_GoBacktochildbills") %> "><i class="icon-custom-left"></i></a>
        <% } %>
        <h3><%: this.GetLocalResourceObject("h_PreviewInvoice") %> </h3>
        <div class="pull-right">
            <% if (this.Model != null)
               { %>
            <% if (canEdit)
               { %>
            <a class="btn btn-success" href="<%=Url.Action("ManageInvoice", new { invoiceId = Model.Id, tenantId = Model.TenantId }) %>"
                title="<%: this.GetLocalResourceObject("t_EditInvoice") %>"><i class="fa fa-pencil"></i>&nbsp; <%=this.GetGlobalResourceObject("General","Edit") %></a>
            <% } %>
            <a class="btn btn-info" href="#btnPrint" id="btnPrint" title="<%: this.GetLocalResourceObject("t_PrintThisInvoice") %>"><i class="fa fa-print"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_Print") %></a>
            <a class="btn btn-default" href="<%=Url.Action("DownloadInvoice", new { invoiceId = Model.Id, tenantId = Model.TenantId }) %>"
                id="btnDownload" title="<%: this.GetLocalResourceObject("lbl_InvoiceDownLoad") %>"><i class="fa fa-download"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_Download") %></a>
            <% } %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (this.Model == null)
           { %>
        <div class="alert alert-danger">
            <%: this.GetLocalResourceObject("e_InvoiceDetails") %>
        </div>
        <%
           }
           else
           {
        %>
        <div class="invoiceContainer" id="invoiceContainer">
            <% Html.RenderPartial("PartialInvoiceTemplate", Model); %>
        </div>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#btnPrint').click(function (e) {
                var style = '<style>';
                style += 'table { width:100%; }';
                style += 'table th { text-align:left; }';
                style += '.tright { text-align:right; }';
                style += '.halign { text-align:center; }';
                style += '.clear, .clearfix { clear:both; }';
                style += 'h1 { text-align: center;display: block;margin: 5px auto;}';
                style += '</style>';
                var html = $('.invoiceContainer').html();

                var w = window.open('', 'Print Invoice');
                w.document.write(html);

                /*
                var headHTML = w.document.getElementsByTagName('head')[0].innerHTML;
                headHTML += '<link type="text/css" rel="stylesheet" href="/App_Themes/CelloSkin/style.css">';
                w.document.getElementsByTagName('head')[0].innerHTML = headHTML;
                */

                w.document.write(style);
                w.print();
                return false;
            });
        });
    </script>
</asp:Content>
