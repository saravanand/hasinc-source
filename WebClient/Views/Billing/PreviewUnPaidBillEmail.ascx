﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<% Html.RenderPartial("StatusMessage"); %>
<% if (ViewData["ToAddress"] != null)
   { %>
<table style="width: 100%; margin: 0 auto;">
    <tbody>
        <tr>
            <td style="vertical-align: top;"><%: this.GetLocalResourceObject("lbl_To") %> To:</td>
            <td><%=ViewData["ToAddress"] %></td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><%: this.GetLocalResourceObject("lbl_Subject") %> </td>
            <td><%=ViewData["Subject"] %></td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><%: this.GetLocalResourceObject("lbl_Body") %> </td>
       <td></td>
        </tr><tr>     <td colspan="2"><%=ViewData["BodyContent"] %></td></tr>
    </tbody>
</table>
<% } %>