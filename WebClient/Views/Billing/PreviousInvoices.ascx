﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<% 
    var invoiceResult = ViewData["InvoiceSearchResult"] as InvoiceSearchResult ?? new InvoiceSearchResult();
    var invoiceList = invoiceResult.Items;
%>
<% 
    if (invoiceList != null && invoiceList.Count > 0)
    {
        var tenantId = invoiceList.First().Value.TenantId.ToString();
        var canEdit = UserIdentity.HasPrivilege(PrivilegeConstants.EditInvoice) && !tenantId.Equals(UserIdentity.TenantID, StringComparison.OrdinalIgnoreCase);
        
        Html.Grid<Invoice>(invoiceList.Values).Columns(column =>
            {
                column.For(x => string.Format("<a href='{1}' title='" + this.GetLocalResourceObject("t_ClickToDownLoad").ToString() + "'>{0}</a>", x.InvoiceNo, Url.Action("DownloadInvoice", new { invoiceId = x.Id, tenantId = x.TenantId }))).Named(this.GetLocalResourceObject("lbl_InvoiceNo").ToString()).DoNotEncode();
                column.For(x => string.Format("{0} - {1}", x.StartDate.ToUIDateString(), x.EndDate.ToUIDateString())).Named(this.GetLocalResourceObject("lbl_BillPeriod").ToString());
                //column.For(x => x.InvoiceDate.ToUIDateString()).Named("Invoice Date");
                //column.For(x => x.StartDate.ToUIDateString()).Named("Bill Period Start");
                //column.For(x => x.EndDate.ToUIDateString()).Named("Bill Period End");
                column.For(x => x.Amount.ToBillingCurrency()).Named(this.GetLocalResourceObject("lbl_Amount").ToString())
                    .Attributes(@class => "tright").HeaderAttributes(@class => "tright");
                //column.For(x => x.NetAmount.ToBillingCurrency()).Named("Net Amount");
                column.For(x => x.PaidAmount.ToBillingCurrency()).Named(this.GetLocalResourceObject("lbl_Paid").ToString())
                    .Attributes(@class => "tright").HeaderAttributes(@class => "tright");
                //column.For(x => x.DueDate.ToUIDateString()).Named("Due Date");
                column.For(x => x.NetAmount > x.PaidAmount ? string.Format("<a href='{0}' title='" + this.GetLocalResourceObject("t_ClickToEdit").ToString() + "'><i class='fa fa-pencil'></i></a>", Url.Action("ManageInvoice", new { invoiceId = x.Id, tenantId = x.TenantId })) : "-")
                    .Named(this.GetGlobalResourceObject("General","Edit").ToString()).Visible(canEdit)
                    .Attributes(@class => "halign").HeaderAttributes(@class => "halign")
                    .DoNotEncode();
                column.For(x => string.Format("<a href='{0}' title='" + this.GetLocalResourceObject("t_ClickToView").ToString() + "'><i class='fa fa-eye'></i></a>", Url.Action("PreviewInvoice", new { invoiceId = x.Id, tenantId = x.TenantId })))
                    .Named(this.GetGlobalResourceObject("General","View").ToString())
                    .Attributes(@class => "halign").HeaderAttributes(@class => "halign")
                    .DoNotEncode();
            })
            .RowAttributes(x => x.Item.Amount > x.Item.PaidAmount
                ? new Dictionary<string, object>() { { "class", "danger" } } : new Dictionary<string, object>())
            .Attributes(@class => "table table-condensed", id => "tblPrevInvoices").Render();
%>
<script type="text/javascript">
    $(function () {
        $('#tblPrevInvoices').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": false,
            "bInfo": true
        });
    });
</script>
<%
    }
    else
    { %>
<div class="">
    <%: this.GetLocalResourceObject("e_PreviousBills") %>  
</div>
<% } %>
