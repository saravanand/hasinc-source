﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%
    var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails> ?? new Dictionary<string, TenantDetails>();
    var bsList = ViewData["BillStatistics"] as Dictionary<Guid, BillStatistics> ?? new Dictionary<Guid, BillStatistics>();
    var selectedyear = (int?)ViewData["year"];

    var model = bsList.ToDictionary(x => x.Key.ToString(), y => y.Value);
    var gtenantList = tenantList.ToDictionary(k => Guid.Parse(k.Key), v => v.Value);

    var condition = ViewData["SearchCondition"] as BillStatisticsSearchCondition ?? new BillStatisticsSearchCondition();

    var totalRevenues = model.Values.OrderBy(x => gtenantList.ContainsKey(x.TenantId) ? gtenantList[x.TenantId].TenantName : x.TenantId.ToString());

    var yrs = Enumerable.Range(DateTime.Now.Year - 15, 16).Reverse().Select(x => x.ToString()).ToList();
    yrs.Insert(0, "Total");
    var lstYears = new SelectList(yrs, selectedyear.HasValue ? selectedyear.Value.ToString() : "Total");
%>
<div class="grid simple horizontal green">
    <div class="grid-title no-border">
        <div class="row">
            <div class="col-md-6">
                <h4><%: this.GetLocalResourceObject("h_Revenuebytenants") %></h4>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <span title="<%: this.GetLocalResourceObject("t_Clicktochangethefinicialyear") %>">
                        <%=Html.DropDownList("revnue_by_tenants_year", lstYears, new { style="width:80px;" })%>
                    </span>
                    <a href="#" onclick="tableToExcel(this,'tblRevenueByTenants','Revenue by Tenants');" title="<%: this.GetLocalResourceObject("t_ClickToDownLoad") %>"><i class="fa fa-cloud-download"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body no-border">
        <div class="filterBox">
            <form action="" method="post" name="frmFilterBillStatisticsGrid" id="frmFilterBillStatisticsGrid">
                <label>
                    <%: this.GetLocalResourceObject("lbl_ShowOnlyOverdue") %></label>
                <input type="checkbox" id="chkOnlyOverdue" name="OnlyOverdue" <%=condition.OnlyOverdue ? "checked=checked" : "" %>
                    value="true" />
                <div class="clear clearfix">
                </div>
            </form>
        </div>
        <div class="grid-part">
            <% if (model != null && model.Count > 0)
               { %>
            <%
                   Html.Grid(totalRevenues).Columns(col =>
                   {
                       col.For(x => gtenantList.ContainsKey(x.TenantId) ? gtenantList[x.TenantId].TenantName : "-")
                           .Named(this.GetLocalResourceObject("lbl_TenantName").ToString());
                       col.For(x => x.TotalBills)
                           .Named(this.GetLocalResourceObject("lbl_Bills").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
                       col.For(x => x.TotalAmount.ToBillingCurrency())
                           .Named(this.GetLocalResourceObject("lbl_Amount").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
                       col.For(x => x.TotalPaid.ToBillingCurrency())
                           .Named(this.GetLocalResourceObject("lbl_Paid").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
                       col.For(x => x.Overdue.ToBillingCurrency())
                           .Named(this.GetLocalResourceObject("lbl_Overdue").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
                       col.For(x => string.Format("<a target='_blank' href='{0}' title='View \"{1}\" bills!'><i class='fa fa-search'></i></a>", Url.Action("ManageChildBills", "Billing", new { viewTenantId = x.TenantId }, Request.Url.Scheme), gtenantList.ContainsKey(x.TenantId) ? gtenantList[x.TenantId].TenantName : "-"))
                           .Named(this.GetLocalResourceObject("lbl_Action").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                   })
                   .RowAttributes(x => Math.Round(x.Item.Overdue, 2) > 0.00
                       ? new Dictionary<string, object>() { { "class", "text-danger danger" } } : new Dictionary<string, object>())
                   .Attributes(@class => "table table-condensed", id => "tblRevenueByTenants").Render();
            %>
            <% }
               else
               { %>
            <div class="alert alert-info">
                <%: this.GetLocalResourceObject("e_NoData") %>
            </div>
            <% } %>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#revnue_by_tenants_year').select2();
        $('#revnue_by_tenants_year').change(function () {
            var val = $(this).val();
            var data = {};

            if (val != 'Total') {
                var year = parseInt($(this).val());
                data = { "year": year };
            }

            $('#divTenantBillStatistics').load('<%:Url.Action("RevenuesByTenants")%>', data);
            return false;
        });

        $('#tblRevenueByTenants').dataTable();

        $('#chkOnlyOverdue').click(function () {
            var postdata = $('#frmFilterBillStatisticsGrid').serialize();
            postdata = postdata + '&year=' + $('#revnue_by_tenants_year').val();
            $.post('<%:Url.Action("RevenuesByTenants")%>', postdata, function (html) {
                $('#divTenantBillStatistics').html(html);
            });
            return false;
        });
    });
</script>
