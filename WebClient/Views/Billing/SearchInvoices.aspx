﻿<%@ Page Title="<%$ Resources:t_SearchBills %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Notification.Model" %>
<%@ Import Namespace="CelloSaaS.Template.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("h_SearchBills") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <%
            var invoiceList = ViewData["InvoiceList"] as Dictionary<Guid, Invoice>;
            var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails> ?? new Dictionary<string, TenantDetails>();
            var notificationList = ViewData["NotificationList"] as List<NotificationDetails> ?? new List<NotificationDetails>();
            var searchCondition = ViewData["SearchCondition"] as InvoiceSearchCondition ?? new InvoiceSearchCondition();

            var tenantSelectList = tenantList.Values.OrderBy(x => x.TenantName).Select(x => new SelectListItem { Text = x.TenantName, Value = x.TenantCode }).ToList();
            tenantSelectList.Insert(0, new SelectListItem { Text = "All", Value = "-1" });

            var invoiceStatusSelectList = typeof(InvoiceStatus).ToSelectList(addEmpty: true);
            invoiceStatusSelectList.RemoveAt(1);
        %>
        <form id="frmInvoiceSearch" name="frmInvoiceSearch" method="post" action="<%=Url.Action("SearchInvoices") %>">
            <section class="panel purple">
                <header class="panel-heading">
                    <h4><%: this.GetLocalResourceObject("lbl_Filter") %></h4>
                </header>
                <div class="panel-body">
                    <div class="row form-container">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_Tenant") %>
                                </label>
                                <%=Html.DropDownList("viewTenantId", tenantSelectList, new { style="width:100%;" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><%: this.GetLocalResourceObject("lbl_Status") %></label>
                                <%=Html.DropDownList("InvoiceStatus", invoiceStatusSelectList, new { style="width:100%;" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_FromDate") %></label>
                                <%=Html.TextBox("FromInvoiceDate", searchCondition.FromInvoiceDate.HasValue ? searchCondition.FromInvoiceDate.Value.ToUIDateString() : "", new { @class = "datetime" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_ToDate") %></label>
                                <%=Html.TextBox("ToInvoiceDate", searchCondition.ToInvoiceDate.HasValue ? searchCondition.ToInvoiceDate.Value.ToUIDateString() : "", new { @class = "datetime" })%>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-info" href="#" onclick="$('#frmInvoiceSearch').submit()" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                            <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%></a>
                        <a class="btn btn-default" href="#" id="btnResetInvoiceSearch" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                            <%:this.GetGlobalResourceObject("General", "Reset")%></a>
                    </div>
                </div>
            </section>
        </form>
        <%
            if (tenantList != null && tenantList.Count > 0)
            {
                var notificationSelectList = notificationList.OrderBy(x => x.NotificationName).Select(x => new SelectListItem { Text = x.NotificationName, Value = x.NotificationId }).ToList();
                notificationSelectList.Insert(0, new SelectListItem { Text = this.GetLocalResourceObject("t_Selectnotification").ToString(), Value = "" });
        %>
        <div class="grid simple">
            <div class="grid-body form-container">
                <% if (invoiceList != null && invoiceList.Count > 0)
                   { %>
                <div class="content-box row">
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label style="float: left; line-height: 30px; margin-right: 10px;">
                                <%: this.GetLocalResourceObject("lbl_Notification") %></label>
                            <div class="input-group">
                                <%=Html.DropDownList("notificationId", notificationSelectList, new { style="width:100%;" })%>
                                <span class="input-group-btn">
                                    <a class="btn btn-info" href="#" id="btnEmail" title="<%: this.GetLocalResourceObject("lbl_ClickSendemail") %>">
                                        <i class='fa fa-envelope'></i>&nbsp;<%: this.GetLocalResourceObject("lbl_Email") %></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <div id="invoiceDiv">
                    <% Html.RenderPartial("InvoiceSearchGrid", invoiceList); %>
                </div>
            </div>
        </div>
        <% }
            else
            { %>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("e_NoTenants") %>
        </div>
        <% } %>
        <div class="modal fade" id="previewBillEmail" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="overflow:scroll;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><%: this.GetLocalResourceObject("lbl_PreviewEmail") %></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%:this.GetGlobalResourceObject("General", "Close")%></button>
                        <button type="button" id="btnSendEmail" title="<%: this.GetLocalResourceObject("t_Save") %>" class="btn btn-primary">
                            <i class="fa fa-envelope"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_Send") %>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalEmailAudits" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><%: this.GetLocalResourceObject("lbl_EmailAudits") %></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%:this.GetGlobalResourceObject("General", "Close")%></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="headContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $(document).on('click', '#invoiceDiv a.view-email-audit', function () {
                var invoiceId = $(this).data('invoiceid');
                var invoiceNo = $(this).data('invoiceno');
                var tenantId = $(this).data('parenttenantid');
                var notificationId = $('select#notificationId').val();
                var data = { notificationId: notificationId, invoiceId: invoiceId, invoiceNo: invoiceNo, tenantId: tenantId };
                $.get('/Billing/GetEmailAudits', data, function (html) {
                    $('#modalEmailAudits .modal-body').html(html);
                    $('#modalEmailAudits').modal('show');
                });
            });

            $('#btnResetInvoiceSearch').click(function () {
                $('#frmInvoiceSearch')[0].reset();
                $('select[name=viewTenantId],select[name=InvoiceStatus],select[name=BillFrequency],select[name=notificationId],select[name=templateId]').trigger('change');
                $('#frmInvoiceSearch').submit();
            });

            $('#frmInvoiceSearch').submit(function (e) {
                var postdata = $('#frmInvoiceSearch').serialize();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                $.post('/Billing/SearchInvoices', postdata, function (data) {
                    $('#invoiceDiv').hide().html(data).fadeIn();
                });
                return false;
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var postdata = $('#frmInvoiceSearch').serialize();
                postdata += '&pageSize=' + $(this).val();

                $.post('/Billing/SearchInvoices', postdata, function (data) {
                    $('#invoiceDiv').hide().html(data).fadeIn();
                });
            });

            $('#btnEmail').click(function () {
                var notificationId = $('select[name=notificationId]').val();

                if (!notificationId) {
                    alert('Select a Notification!');
                    return false;
                }

                var invoiceIds = [];
                var tenantIds = [];

                $('#tblInvoiceSearch input[type=checkbox][name=chkSelect]:checked').each(function () {
                    invoiceIds.push($(this).data('invoiceid'));
                    tenantIds.push($(this).data('tenantid'));
                });

                if (invoiceIds.length == 0) {
                    alert('Select at-least one bill!');
                    return false;
                }

                var postdata = { notificationId: notificationId, invoiceIds: invoiceIds, tenantIds: tenantIds };

                $.ajax({
                    url: '/Billing/PreviewUnPaidBillEmail',
                    data: JSON.stringify(postdata),
                    type: 'POST',
                    contentType: 'application/json',
                    success: function (html) {
                        $('#previewBillEmail .modal-body').html(html);
                        $('#previewBillEmail').modal('show');
                    }
                });

                return false;
            });

            $('#btnSendEmail').click(function () {
                $('#previewBillEmail').modal('hide');

                var notificationId = $('select[name=notificationId]').val();

                if (!notificationId) {
                    alert('Select a Notification!');
                    return false;
                }

                var invoiceIds = [];
                var tenantIds = [];

                $('#tblInvoiceSearch input[type=checkbox][name=chkSelect]:checked').each(function () {
                    invoiceIds.push($(this).data('invoiceid'));
                    tenantIds.push($(this).data('tenantid'));
                });

                if (invoiceIds.length == 0) {
                    alert('Select at-least one bill!');
                    return false;
                }

                var postdata = { notificationId: notificationId, invoiceIds: invoiceIds, tenantIds: tenantIds };

                $.ajax({
                    url: '/Billing/SendUnPaidBillEmail',
                    data: JSON.stringify(postdata),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST',
                    success: function (data) {
                        if (data.Error) {
                            var error = '';
                            var errs = data.Error.split(';');
                            for (i = 0; i < errs.length; ++i)
                                error += errs[i] + '\n';
                            alert(error);
                        } else {
                            alert('Selected bills are notified successfully!');
                            $('#tblInvoiceSearch input[type=checkbox][name=chkSelect]:checked').each(function () {
                                $(this).attr('checked', null);
                            });
                        }
                    }
                });

                return false;
            });

            $('#btnCancel').click(function () { $('#previewBillEmail').modal('hide'); return false; });
            $('#btnEmailAuditClose').click(function () { $('#modalEmailAudits').modal('hide'); return false; });

            $('#previewBillEmail').modal({ show: false });
            $('#modalEmailAudits').modal({ show: false });

            $('select[name=viewTenantId]').select2();
            $('select[name=viewTenantId]').trigger('change');

            $('select[name=InvoiceStatus],select[name=BillFrequency],select[name=notificationId],select[name=templateId]').select2();

            $('input.datetime').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });
        });
    </script>
</asp:Content>
