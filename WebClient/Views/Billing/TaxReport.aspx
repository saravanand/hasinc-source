﻿<%@ Page Title="Tax Reports" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <h3>Tax Reports</h3>
    </div>
    <div class="row-fluid pd-25" id="dashboard-container">
        <%
            var model = ViewBag.Stats as Dictionary<Guid, CelloSaaS.Billing.Model.TaxReport>;
            var tenants = ViewBag.TenantList as Dictionary<string, CelloSaaS.Model.TenantManagement.TenantDetails> ?? new Dictionary<string, CelloSaaS.Model.TenantManagement.TenantDetails>();
            var tenantNames = tenants.ToDictionary(x => Guid.Parse(x.Key), y => y.Value.TenantName);
            var currMonth = ViewBag.currMonth ?? DateTime.Now.Month;
            var currYear = ViewBag.currYear ?? DateTime.Now.Year;
            var selectMonths = new List<SelectListItem>();
            for (int i = 1; i <= 12; ++i)
            {
                selectMonths.Add(new SelectListItem
                {
                    Text = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i),
                    Value = i.ToString(),
                    Selected = i == currMonth
                });
            }
        %>
        <div class="content-box row">
            <div class="col-md-6">
                <form method="post">
                    <div class="form-group" style="margin-bottom: 0px;">
                        <div class="search_holder">
                            <label style="float: left; line-height: 30px; margin-right: 10px;">
                                Select a month:</label>
                            <div class="input-group">
                                <%=Html.DropDownList("month", selectMonths, new { style="width:100%;" })%>
                                <span class="input-group-btn">
                                    <a class="btn btn-info" href="#" id="btnSearch" onclick="$('form').submit();" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                                        <i class='fa fa-search'></i>&nbsp;<%:this.GetGlobalResourceObject("General","View") %></a>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <% if (model == null || model.Count == 0)
           { %>
        <div class="alert alert-info">No data available!</div>
        <% }
           else
           {
               var revnueData = ViewBag.Revenues as Dictionary<DateTime, Tuple<int, double, double, double>> ?? new Dictionary<DateTime, Tuple<int, double, double, double>>();

               int totalBills = revnueData.Where(x => x.Value != null).Sum(x => x.Value.Item1);
               double totalAmount = revnueData.Where(x => x.Value != null).Sum(x => x.Value.Item2);
               double totalPaid = revnueData.Where(x => x.Value != null).Sum(x => x.Value.Item3);
               double totalOverdue = Math.Round(totalAmount - totalPaid, CelloSaaS.Billing.Model.BillingConstants.RoundDecimals);

               var uniqueTaxItems = model.SelectMany(x => x.Value.Taxes.Select(u => u.Key)).Distinct();
               var totalTaxes = uniqueTaxItems.ToDictionary(x => x, y => 0.0);

               foreach (var item in model.Where(x => x.Value.InvoiceDate.Month == currMonth && x.Value.InvoiceDate.Year == currYear))
               {
                   foreach (var ti in item.Value.Taxes)
                   {
                       totalTaxes[ti.Key] += ti.Value;
                   }
               }

               var total = totalTaxes.Sum(x => x.Value);
        %>
        <div class="row">
            <div class="col-md-3">
                <div class="tiles purple">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            Total Bills
                        </div>
                        <div class="heading">
                            <%:totalBills %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles blue">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            Total Revenue
                        </div>
                        <div class="heading" title="<%=totalAmount.ToBillingCurrency() %>">
                            <%=(totalAmount%1==0)?totalAmount.ToBillingCurrency("C0"):totalAmount.ToBillingCurrency("C2") %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles indigo">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            Total Paid
                        </div>
                        <div class="heading" title="<%=totalPaid.ToBillingCurrency() %>">
                            <%=(totalPaid%1==0)?totalPaid.ToBillingCurrency("C0"):totalPaid.ToBillingCurrency("C2") %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles lt-red">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            Overdue
                        </div>
                        <div class="heading" title="<%=totalOverdue.ToBillingCurrency() %>">
                            <%=(totalOverdue%1==0)?totalOverdue.ToBillingCurrency("C0") :totalOverdue.ToBillingCurrency("C2")%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-3">
                <div class="tiles red">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            Total Tax
                        </div>
                        <div class="heading">
                            <div class="heading" title="<%=total.ToBillingCurrency() %>">
                                <%=total.ToBillingCurrency("C0") %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%  var colors = new string[] { "green", "orange", "lt-green", "purple", "indigo" };
                int cnt = 1;
                foreach (var tt in totalTaxes)
                {
                    string color = colors[cnt];
                    cnt++;
                    if (cnt >= colors.Length)
                    {
                        cnt = 0;
            %>
        </div>
        <div class="row" style="margin-top: 15px;">
            <% 
                    }
            %>
            <div class="col-md-3">
                <div class="tiles <%: color %>">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: tt.Key %>
                        </div>
                        <div class="heading">
                            <div class="heading" title="<%=tt.Value.ToBillingCurrency() %>">
                                <%=tt.Value.ToBillingCurrency("C0") %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
        </div>
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-12">
                <div class="grid simple horizontal red">
                    <div class="grid-title">
                        <h4 id="taxReportTitle">Tax Report for <strong><%:System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currMonth) %>, <%:currYear %></strong></h4>
                        <div class="pull-right">
                            <a href="#" class="btn btn-info " onclick="printTaxReport(this);" title="Print"><i class="fa fa-print"></i>&nbsp;Print</a>
                            <a href="#" class="btn btn-warning" onclick="tableToExcel(this, 'taxReportTenant','Tax Report for <%:System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currMonth) %>, <%:currYear %>');" title="Download in excel format"><i class="fa fa-cloud-download"></i>&nbsp;Download</a>
                        </div>
                    </div>
                    <div class="grid-body" style="padding: 0px">
                        <table class="celloTable" id="taxReportTenant">
                            <thead>
                                <tr>
                                    <th>Tenant Name</th>
                                    <% foreach (var item in uniqueTaxItems)
                                       { %>
                                    <th class="text-right"><%:item %></th>
                                    <% } %>
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% foreach (var item in model)
                                   {
                                       var t = item.Value.Taxes.Sum(x => x.Value);
                                %>
                                <tr>
                                    <td><%=tenantNames.ContainsKey(item.Key) ? tenantNames[item.Key] : item.Key.ToString() %></td>
                                    <% foreach (var ti in uniqueTaxItems)
                                       {
                                           if (!item.Value.Taxes.ContainsKey(ti)) continue;
                                    %>
                                    <td class="text-right"><%:item.Value.Taxes[ti].ToString("F2") %></td>
                                    <% } %>
                                    <td class="text-right"><%:t.ToString("F2") %></td>
                                </tr>
                                <% } %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <script>
        var orgTaxReportTable = null;
        $(function () {
            $('select').select2();
            orgTaxReportTable = document.getElementById('taxReportTenant').outerHTML;
            $('#taxReportTenant').dataTable();
        });

        function printTaxReport() {
            var style = '<style>';
            style += 'table { width:100%; }';
            style += 'table th { text-align:left; }';
            style += '.text-right { text-align:right; }';
            style += 'h1, h4 { text-align: center;display: block;margin: 5px auto;}';
            style += '</style>';

            var title = document.getElementById('taxReportTitle').outerHTML;
            var w = window.open('', title);

            w.document.write(style);
            w.document.write(title);
            w.document.write(orgTaxReportTable);

            w.print();
            return false;
        }
    </script>
</asp:Content>
