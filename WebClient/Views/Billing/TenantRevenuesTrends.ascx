﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%
    var selectedTenantIds = ViewData["selectedTenantIds"] as string[] ?? new string[1];
    var childTenants = ViewData["TenantList"] as Dictionary<string, TenantDetails> ?? new Dictionary<string, TenantDetails>();
    var bsList = ViewData["BillStatistics"] as Dictionary<string, Dictionary<Guid, BillStatistics>> ?? new Dictionary<string, Dictionary<Guid, BillStatistics>>();

    var tenantList = childTenants.Select(x => x.Value).Where(x => selectedTenantIds.Contains(x.TenantCode)).OrderBy(x => x.TenantName);
    var years = bsList.Keys;
    var model = new List<List<double?>>();

    foreach (var t in tenantList)
    {
        var tid = Guid.Parse(t.TenantCode);
        var amts = new List<double?>();
        foreach (var y in years)
        {
            if (bsList.ContainsKey(y))
            {
                if (bsList[y].ContainsKey(tid))
                {
                    amts.Add(Math.Round(bsList[y][tid].TotalAmount, 2));
                }
                else
                {
                    amts.Add(null);
                }
            }
        }
        model.Add(amts);
    }

    int selectedMinYear = ViewBag.minYear;
    int selectedMaxYear = ViewBag.maxYear;
%>
<form action="" method="post" name="frmFilterBillStatisticsChart" id="frmFilterBillStatisticsChart">
    <div class="grid simple horizontal purple" id="chartConfiguration">
        <div class="grid-title no-border-side">
            <div class="row">
                <div class="col-md-6">
                    <h4><%: this.GetLocalResourceObject("h_TenantRevenuesTrend") %></h4>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <a href="#" onclick="tableToExcel(this,'tblTenantRevenuesTrends','Tenant Revenues Trend');" title="<%: this.GetLocalResourceObject("t_Clicktodownloadinexcelformat") %>"><i class="fa fa-cloud-download"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-body no-border">
            <% if (childTenants.Count > 0)
               { %>
            <div class="row">
                <div class="col-md-4">
                    <label style="width: 50px;">
                        <%: this.GetLocalResourceObject("lbl_Period") %></label>
                    <div id="period-text" style="display: inline-block; font-weight: bold;">
                        <%:selectedMinYear %> - <%:selectedMaxYear %>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="slider success">
                        <input id="chartYear-slider-range" type="text" data-slider-selection="after" class="slider-element" value="" data-slider-min="<%:DateTime.Today.Year - 15 %>" data-slider-max="<%:DateTime.Today.Year%>" data-slider-step="1" data-slider-value="[<%:selectedMinYear %>,<%:selectedMaxYear %>]" style="width: 206px;">
                    </div>
                </div>
                <div class="col-md-2">
                    <%=Html.Hidden("minYear") %>
                    <%=Html.Hidden("maxYear") %>
                    <div class="pull-right">
                        <a class="btn btn-info btn-xs" href="#" id="btnChartApply" title="<%: this.GetLocalResourceObject("t_Applythisfilter") %>"><%: this.GetLocalResourceObject("lbl_Apply") %> </a>
                    </div>
                </div>
            </div>
            <div id="tenantRevenueGridContainer" class="gridContainer" style="overflow-x: auto; overflow-y: auto;">
                <table id="tblTenantRevenuesTrends" class="table table-condensed">
                    <thead>
                        <tr>
                            <th><%: this.GetLocalResourceObject("lbl_Tenant") %>
                            </th>
                            <% foreach (var year in years)
                               { %>
                            <th>
                                <%:year %>
                            </th>
                            <% } %>
                        </tr>
                    </thead>
                    <tbody>
                        <% int cnt = 0;
                           foreach (var t in tenantList)
                           {
                               var amts = model[cnt++];
                        %>
                        <tr>
                            <td style="min-width: 120px;">
                                <b>
                                    <%:t.TenantName %></b>
                            </td>
                            <% foreach (var amt in amts)
                               { %>
                            <td class="tright">
                                <%:amt.HasValue ? amt.Value.ToBillingCurrency() : "-" %>
                            </td>
                            <% } %>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
            <% }
               else
               { %>
            <div class="info">
                <%: this.GetLocalResourceObject("e_Childtenants") %>
            </div>
            <% } %>
        </div>
    </div>
</form>
<script type="text/javascript">
    var minDate = <%:DateTime.Today.Year - 15 %>;
    var maxDate = <%: DateTime.Today.Year %>;
    var selectedMinYear = <%:selectedMinYear %>;
    var selectedMaxYear = <%: selectedMaxYear %>;
</script>
<script type="text/javascript">
    $(function () {
        $('#tblTenantRevenuesTrends').dataTable();
        var s = $('#chartYear-slider-range').slider();  
        $('#btnChartApply').click(function () {
            var val = s[0].value.split(',');
            var min=val[0],max=val[1];
            $('input#minYear').val(min);
            $('input#maxYear').val(max);
            var postdata = $('#frmFilterBillStatisticsChart').serialize();
            $.post('/Billing/TenantRevenuesTrends', postdata, function (html) {
                $('#divTenantRevenuesTrends').html(html);
            });
            return false;
        });
    });
</script>
