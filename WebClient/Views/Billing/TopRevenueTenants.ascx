﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%  
    bool hideTable = (bool)ViewBag.hideTable;
    var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails> ?? new Dictionary<string, TenantDetails>();
    var model = ViewData["TotalStats"] as Dictionary<Guid, BillStatistics> ?? new Dictionary<Guid, BillStatistics>();
    var totalRevenues = ViewData["TotalRevenue"] as Dictionary<DateTime, Tuple<int, double, double, double>> ?? new Dictionary<DateTime, Tuple<int, double, double, double>>();
    var fromDate = (DateTime?)ViewData["fromDate"];
    var toDate = (DateTime?)ViewData["toDate"];
    var selectedyear = (int?)ViewData["year"];

    int overallBills = totalRevenues.Sum(x => x.Value.Item1);
    double overallTotal = totalRevenues.Sum(x => x.Value.Item2);
    double overallPaid = totalRevenues.Sum(x => x.Value.Item3);
    double overallTax = totalRevenues.Sum(x => x.Value.Item4);
    double overallOverdue = Math.Round(overallTotal - overallPaid, BillingConstants.RoundDecimals);

    var tmp = model.Values.Select(x => new
    {
        category = tenantList.ContainsKey(x.TenantId.ToString()) ? tenantList[x.TenantId.ToString()].TenantName : "-",
        value = overallTotal == 0.0 ? 0 : Math.Round((1 - (overallTotal - x.TotalAmount) / overallTotal) * 100, BillingConstants.RoundDecimals)
    }).ToList();

    var rem = Math.Round(100.0 - tmp.Sum(x => x.value), BillingConstants.RoundDecimals);
    if (rem > 0.0)
    {
        tmp.Add(new { category = "Others", value = rem });
    }

    string chartData = Newtonsoft.Json.JsonConvert.SerializeObject(tmp, Newtonsoft.Json.Formatting.None);
    string title = string.Format(this.GetLocalResourceObject("lbl_TopRevenueTenants").ToString(), ViewData["topCount"]);

    var yrs = Enumerable.Range(DateTime.Now.Year - 15, 16).Reverse().Select(x => x.ToString()).ToList();
    yrs.Insert(0, "Total");
    var lstYears = new SelectList(yrs, selectedyear.HasValue ? selectedyear.Value.ToString() : "Total");
%>
<div class="grid simple horizontal blue">
    <div class="grid-title no-border-side">
        <div class="row">
            <div class="col-md-6">
                <h4><%=title %></h4>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <% if (!hideTable)
                       { %>
                    <span title="<%: this.GetLocalResourceObject("t_Click") %>">
                        <%=Html.DropDownList("top_revenue_year", lstYears, new { style="width:80px;" })%>
                    </span>
                    <a href="#" onclick="tableToExcel(this, 'tblTopRevenueTenants','Top 5 Revenue Tenants');" title="<%: this.GetLocalResourceObject("t_ClickDownLoad") %>"><i class="fa fa-cloud-download"></i></a>
                    <% } %>
                    <a class="reload" href="javascript:;"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body no-border">
        <% if (!hideTable)
           { %>
        <% if (model.Count > 0)
           { %>
        <%
               Html.Grid(model.Values).Columns(col =>
               {
                   col.For(x => tenantList.ContainsKey(x.TenantId.ToString()) ? tenantList[x.TenantId.ToString()].TenantName : "-")
                       .Named(this.GetLocalResourceObject("lbl_TenantName").ToString());
                   col.For(x => x.TotalBills)
                       .Named(this.GetLocalResourceObject("lbl_Bills").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
                   col.For(x => x.TotalAmount.ToBillingCurrency())
                       .Named(this.GetLocalResourceObject("lbl_Amount").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
                   col.For(x => x.TotalPaid.ToBillingCurrency())
                       .Named(this.GetLocalResourceObject("lbl_Paid").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
                   col.For(x => x.Overdue.ToBillingCurrency())
                       .Named(this.GetLocalResourceObject("lbl_Overdue").ToString()).HeaderAttributes(@class => "tright").Attributes(@class => "tright");
               })
               .RowAttributes(x => Math.Round(x.Item.Overdue, 2) > 0.00
                   ? new Dictionary<string, object>() { { "class", "text-danger danger" } } : new Dictionary<string, object>())
               .Attributes(@class => "table table-condensed", id => "tblTopRevenueTenants").Render();
        %>
        <% }
           else
           { %>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("e_NotEnoughData") %>
        </div>
        <% } %>
        <% } %>
        <% if (model.Count > 0)
           { %>
        <%:Html.Hidden("top_revenue_chartData", chartData) %>
        <div id="top_revenue_chart_area"></div>
        <% }
           else if (hideTable)
           { %>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("e_NotEnoughData") %>
        </div>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        if ($('#top_revenue_year').length > 0) {
            $('#top_revenue_year').select2();
            $('#top_revenue_year').change(function () {
                var val = $(this).val();
                var data = {};

                if (val != 'Total') {
                    var year = parseInt($(this).val());
                    data = { "year": year };
                }

                $('#topRevenuesTenantsDiv').load('<%:Url.Action("TopRevenueTenants")%>', data);
                return false;
            });
        }

        if ($('#top_revenue_chartData').length > 0) {
            var top_rev_data = JSON.parse($('#top_revenue_chartData').val());

            function topRevenuecreateChart() {
                var options = {
                    title: {
                        text: '<%:title%>',
                        visible: true
                    },
                    legend: {
                        visible: true,
                        position: 'bottom'
                    },
                    seriesDefaults: {
                        type: 'pie',
                        stack: false,
                        labels: {
                            template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                            position: "outsideEnd",
                            visible: true,
                            background: "transparent",
                            align: "column"
                        },
                        tooltip: {
                            visible: true
                        }
                    },
                    series: [{
                        type: 'pie',
                        data: top_rev_data
                    }],
                    tooltip: {
                        visible: true,
                        template: "#= category # - #= kendo.format('{0:P}', percentage) #"
                    }
                };

                var chart = $('#top_revenue_chart_area').data('celloChart');

                if (chart)
                    chart.destroy();

                $('#top_revenue_chart_area').celloChart(options);
            }

            setTimeout(function () {
                try {
                    topRevenuecreateChart();
                } catch (e) { console.log(e); }
            }, 100);
        }
    });
</script>
