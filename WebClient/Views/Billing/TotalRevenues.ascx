﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%
    bool hideChart = (bool)ViewBag.hideChart;
    var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails> ?? new Dictionary<string, TenantDetails>();
    var revnueData = ViewData["RevenueData"] as Dictionary<DateTime, Tuple<int, double, double, double>> ?? new Dictionary<DateTime, Tuple<int, double, double, double>>();
    var fromDate = (DateTime)ViewData["fromDate"];
    var toDate = (DateTime)ViewData["toDate"];
    var selectedyear = (int?)ViewData["year"];
    bool hasData = revnueData.Count > 0;

    for (var d = fromDate; d < toDate; d = d.AddMonths(1))
    {
        if (!revnueData.ContainsKey(d))
        {
            revnueData.Add(d, null);
        }
    }

    string title = string.Format(this.GetLocalResourceObject("m_Revenueduring").ToString() + " {0} - {1}", fromDate.ToString("MMM,yyyy"), toDate.ToString("MMM,yyyy"));

    var tmp = revnueData.OrderBy(x => x.Key).Select(x => new
    {
        date = x.Key.ToString("MMM"),
        bills = x.Value == null ? null : x.Value.Item1.ToString(),
        amount = x.Value == null ? null : x.Value.Item2.ToString(),
        paid = x.Value == null ? null : x.Value.Item3.ToString(),
        tax = x.Value == null ? null : x.Value.Item4.ToString(),
        overdue = x.Value == null ? null : Math.Round(x.Value.Item2 - x.Value.Item3, BillingConstants.RoundDecimals).ToString()
    });

    int totalBills = revnueData.Where(x => x.Value != null).Sum(x => x.Value.Item1);
    double totalAmount = revnueData.Where(x => x.Value != null).Sum(x => x.Value.Item2);
    double totalPaid = revnueData.Where(x => x.Value != null).Sum(x => x.Value.Item3);
    double totalTax = revnueData.Where(x => x.Value != null).Sum(x => x.Value.Item4);
    double totalOverdue = Math.Round(totalAmount - totalPaid, BillingConstants.RoundDecimals);
    string chartData = Newtonsoft.Json.JsonConvert.SerializeObject(tmp, Newtonsoft.Json.Formatting.None);

    var lstYears = new SelectList(Enumerable.Range(DateTime.Now.Year - 15, 16).Reverse(), selectedyear);
%>
<section class="panel green" id="totalRevenuesDiv">
    <header class="panel-heading">
        <h4><%:this.GetLocalResourceObject("h_RevenueStatistics")%></h4>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="tiles purple">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_FiscalYear") %>
                        </div>
                        <div class="heading" title="Click to change the finicial year!">
                            <%=hideChart ? selectedyear.ToString() : Html.DropDownList("tr_year", lstYears, new { style="width:120px;" }).ToString()%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tiles lt-green">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_TotalBills") %>
                        </div>
                        <div class="heading">
                            <%:totalBills %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tiles indigo">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_TotalRevenue") %>
                        </div>
                        <div class="heading" title="<%=totalAmount.ToBillingCurrency() %>">
                            <%:(totalAmount %1 ==0) ? totalAmount.ToBillingCurrency("C0") : totalAmount.ToBillingCurrency("C2") %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-4">
                <div class="tiles lt-blue">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_TotalTax") %>
                        </div>
                        <div class="heading" title="<%=totalTax.ToBillingCurrency() %>">
                            <%= (totalTax %1 ==0)?totalTax.ToBillingCurrency("C0"):totalTax.ToBillingCurrency("C2") %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tiles orange">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_TotalPaid") %>
                        </div>
                        <div class="heading" title="<%=totalPaid.ToBillingCurrency() %>">
                            <%=(totalPaid%1==0)?totalPaid.ToBillingCurrency("C0"):totalPaid.ToBillingCurrency("C2") %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tiles red">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_Overdue") %>
                        </div>
                        <div class="heading" title="<%=totalOverdue.ToBillingCurrency() %>">
                            <%=(totalOverdue%1==0)?totalOverdue.ToBillingCurrency("C0"):totalOverdue.ToBillingCurrency("C2") %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <% if (hasData && !hideChart)
           { %>
        <div id="totalRevnuesChart" class="chart-container" style="position: relative; background: #fff; margin: 10px auto;">
            <%:Html.Hidden("tr_chartData", chartData) %>
            <div style="height: 240px; text-align: center; margin: 0 auto;">
                <div id="tr_chart_area" style="margin-top: 0px; height: 205px;"></div>
            </div>
        </div>
        <% }
           else if (!hideChart)
           { %>

        <% } %>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        if ($('#tr_year').length > 0) {
            $('#tr_year').select2();
            $('#tr_year').change(function () {
                var year = parseInt($(this).val());
                var data = { 'year': year };
                $('#totalRevenuesDiv').load('<%:Url.Action("TotalRevenues")%>', data);
                return false;
            });
        }

        if ($('#tr_chartData').length > 0) {
            var data = JSON.parse($('#tr_chartData').val());

            function createTotalRevnueChart() {
                var options = {
                    title: {
                        text: '<%:title %>',
                        visible: true
                    },
                    dataSource: {
                        data: data
                    },
                    legend: {
                        visible: false,
                        position: 'bottom'
                    },
                    seriesDefaults: {
                        type: 'column',
                        stack: false,
                        labels: {
                            visible: false
                        },
                        tooltip: {
                            visible: true
                        }
                    },
                    series: [{
                        name: 'Revenue Date',
                        field: "amount"
                    }],
                    categoryAxis: {
                        field: 'date',
                        majorGridLines: {
                            visible: false
                        }
                    },
                    valueAxis: {
                        field: "amount",
                        labels: {
                            template: "#= kendo.toString(value,'c') #",
                            step: 2
                        },
                        line: {
                            visible: false
                        }
                    },
                    tooltip: {
                        visible: true,
                        template: "<div style='text-align:left;'>Date: #= category # <br/>Bills: #= kendo.toString(dataItem.bills)#<br/>Revnue: <span class='text-success'><b>#= kendo.toString(value,'c') #</b></span><br/>Tax: <span class='text-info'>#= kendo.toString(dataItem.tax,'c') #</span><br/>Paid: <span class='text-warning'>#= kendo.toString(dataItem.paid,'c') #</span><br/>Overdue: <span class='text-danger'>#= kendo.toString(dataItem.overdue,'c') #</span></div>"
                    }
                };

                var chart = $('#tr_chart_area').data('celloChart');

                if (chart)
                    chart.destroy();

                $('#tr_chart_area').celloChart(options);
            }

            setTimeout(function () {
                try {
                    createTotalRevnueChart();
                } catch (e) { console.log(e); }
            }, 100);
        }
    });
</script>
