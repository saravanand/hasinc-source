﻿<%@ Page Title="<%$ Resources:RefreshCache %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("RefreshCache") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple horizontal red">
            <div class="grid-title">
                <h4><%: this.GetLocalResourceObject("h_RefreshCacheRegions") %></h4>
            </div>
            <div class="grid-body">
                <%
                    if (ViewData["Info"] != null && !string.IsNullOrEmpty(ViewData["Info"].ToString()))
                    {
                %>
                <div class="alert alert-success">
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                    <%: ViewData["Info"].ToString() %>
                </div>
                <%                
                    }
                %>
                <div class="row">
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageDataViewCache")%></legend>
                            <%using (Html.BeginForm("RefreshDataViewCache", "Cache", FormMethod.Post, new { @id = "RefreshDataViewCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RDataViewCache")%>" onclick="$('#RefreshDataViewCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("DataViewCache")%></span></a>
                            </div>
                            <%} %>
                            <%using (Html.BeginForm("RefreshDataViewFieldDefaultValuesCache", "Cache", FormMethod.Post, new { @id = "RefreshDataViewFieldCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RDataViewFieldDefaultValuesCache")%>"
                                    onclick="$('#RefreshDataViewFieldCache').submit()"><span>
                                        <%: this.GetLocalResourceObject("DataViewFieldCache")%></span></a>
                            </div>
                            <%} %>
                            <%using (Html.BeginForm("RefreshDataViewMetaDataCache", "Cache", FormMethod.Post, new { @id = "RefreshDataViewMetaDataCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RDataViewMetaDataCache")%>" onclick="$('#RefreshDataViewMetaDataCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("DataViewMetaDataCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageTenantCache")%>
                            </legend>
                            <%using (Html.BeginForm("RefreshTenantLicenseCache", "Cache", FormMethod.Post, new { @id = "RefreshTenantLicenseCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RTenantLicenseCache")%>" onclick="$('#RefreshTenantLicenseCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("TenantLicenseCache")%></span></a>
                            </div>
                            <%} %>
                            <%using (Html.BeginForm("RefreshTenantSettingsCache", "Cache", FormMethod.Post, new { @id = "RefreshTenantSettingsCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RTenantSettingsCache")%>" onclick="$('#RefreshTenantSettingsCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("TenantSettingsCache")%></span></a>
                            </div>
                            <%} %>
                            <%using (Html.BeginForm("RefreshTenantCache", "Cache", FormMethod.Post, new { @id = "RefreshTenantCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RTenantCache")%>" onclick="$('#RefreshTenantCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("TenantCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageDataScopeCache")%></legend>
                            <%using (Html.BeginForm("RefreshDataScopeCache", "Cache", FormMethod.Post, new { @id = "RefreshDataScopeCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RDataScopeCache") %>" onclick="$('#RefreshDataScopeCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("DataScopeCache")%></span></a>
                            </div>
                            <%} %>
                            <%using (Html.BeginForm("RefreshEntityBasedDataScopeCache", "Cache", FormMethod.Post, new { @id = "RefreshEntityBasedDataScopeCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("REntityBasedDataScopeCache")%>"
                                    onclick="$('#RefreshEntityBasedDataScopeCache').submit()"><span>
                                        <%: this.GetLocalResourceObject("EntityBasedDataScopeCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageEntityFieldsCache")%>
                            </legend>
                            <%using (Html.BeginForm("RefreshExtendedFieldsCache", "Cache", FormMethod.Post, new { @id = "RefreshExtendedFieldsCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RExtendedFieldsCache")%>" onclick="$('#RefreshExtendedFieldsCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("ExtendedFieldsCache")%></span></a>
                            </div>
                            <%} %>
                            <%using (Html.BeginForm("RefreshBaseFieldsCache", "Cache", FormMethod.Post, new { @id = "RefreshBaseFieldsCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RBaseFieldsCache")%>" onclick="$('#RefreshBaseFieldsCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("BaseFieldsCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManagePrivilegeCache")%></legend>
                            <%using (Html.BeginForm("RefreshPrivilegeCache", "Cache", FormMethod.Post, new { @id = "RefreshPrivilegeCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RPrivilegeCache")%>" onclick="$('#RefreshPrivilegeCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("PrivilegeCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManagePackageCache")%></legend>
                            <%using (Html.BeginForm("RefreshPackageCache", "Cache", FormMethod.Post, new { @id = "RefreshPackageCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RPackageCache")%>" onclick="$('#RefreshPackageCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("PackageCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageEntityMetaDataCache")%>
                            </legend>
                            <%using (Html.BeginForm("RefreshEntityMetaDataCache", "Cache", FormMethod.Post, new { @id = "RefreshEntityMetaDataCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("REntityMetaDataCache")%>" onclick="$('#RefreshEntityMetaDataCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("EntityMetaDataCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageGlobalDefaultValuesCache")%></legend>
                            <%using (Html.BeginForm("RefreshGlobalDefaultValuesCache", "Cache", FormMethod.Post, new { @id = "RefreshGlobalDefaultValuesCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RGlobalDefaultValuesCache")%>"
                                    onclick="$('#RefreshGlobalDefaultValuesCache').submit()"><span>
                                        <%: this.GetLocalResourceObject("GlobalDefaultValuesCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageGlobalFieldsCache")%>
                            </legend>
                            <%using (Html.BeginForm("RefreshGlobalFieldsCache", "Cache", FormMethod.Post, new { @id = "RefreshGlobalFieldsCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RGlobalFieldsCache")%>" onclick="$('#RefreshGlobalFieldsCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("GlobalFieldsCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageWorkflowCache")%>
                            </legend>
                            <%using (Html.BeginForm("RefreshWorkflowCache", "Cache", FormMethod.Post, new { @id = "RefreshWorkflowCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RefreshWorkflowCache")%>" onclick="$('#RefreshWorkflowCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("WorkflowCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageRuleCache")%></legend>
                            <%using (Html.BeginForm("RefreshRuleCache", "Cache", FormMethod.Post, new { @id = "RefreshRuleCache" }))
                              { %>
                            <div class="green_but">
                                <a class="btn btn-info btn-block" href="#" title="<%: this.GetLocalResourceObject("RRuleCache")%>" onclick="$('#RefreshRuleCache').submit()">
                                    <span>
                                        <%: this.GetLocalResourceObject("BusinessRuleCache")%></span></a>
                            </div>
                            <%} %>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        <fieldset class="cacheFieldSet">
                            <legend>
                                <%: this.GetLocalResourceObject("ManageAllCache")%></legend>
                            <%using (Html.BeginForm("RefreshAllCache", "Cache", FormMethod.Post, new { @id = "RefreshAllCache" }))
                              { %>
                            <a class="btn btn-danger btn-block" href="#" title="<%: this.GetLocalResourceObject("RAllCache")%>" onclick="$('#RefreshAllCache').submit()">
                                <span>
                                    <%: this.GetLocalResourceObject("RAllCache")%></span></a>
                            <%} %>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        legend {
            font-size: 14px !important;
            font-weight: 600;
        }
        form {
            margin-bottom:10px;
        }
    </style>
</asp:Content>
