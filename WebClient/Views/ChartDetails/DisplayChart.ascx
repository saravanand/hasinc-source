﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%--<%using (Html.BeginForm("ChartDisplay", "Chart", FormMethod.Post, new { id = "ChartDisplay" }))
%>--%>
<%
    if (Html.ValidationSummary() != null)
    {
%>
<div class="error">
    <%: Html.ValidationSummary() %>
</div>
<%
    }
%>
<%
    if (ViewData["ChartObject"] != null)
    {
        System.Web.UI.DataVisualization.Charting.Chart chart = (System.Web.UI.DataVisualization.Charting.Chart)ViewData["ChartObject"];
%>
<div id="Chart" style="text-align: center">
    <div style="height: auto; clear: both">
        <%
        if (chart != null)
        {
            try
            {
                HtmlTextWriter writer = new HtmlTextWriter(Page.Response.Output);
                chart.RenderType = RenderType.ImageTag;
                chart.RenderControl(writer);
            }
            catch (InvalidOperationException invalidOperationException)
            {
        %>
        <div class="error">
            <%: invalidOperationException.Message %>
        </div>
    </div>
    <div style="clear: both">
    </div>
    <%  
            }
            catch (Exception exception)
            {
    %>
    <div class="error">
        <%: exception.Message %>
    </div>
</div>
<div style="clear: both">
</div>
<%
            }
        }
        else
        {
%>
<h3>
    <%: this.GetLocalResourceObject("NoChartToDisplay") %>
</h3>
<%
        }
%>
<%}%>
</div> 