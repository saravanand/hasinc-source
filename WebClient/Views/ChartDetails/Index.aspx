﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.ChartBuilder.Model.ChartDetails>>" %>

<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.ChartBuilder.ServiceContracts.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <%if (ViewData["Success"] != null && !string.IsNullOrEmpty(ViewData["Success"].ToString()))
          { %>
        <div class="alert alert-success">
            <%= ViewData["Success"].ToString()%>
        </div>
        <%}
          if (Html.ValidationSummary() != null)
          {
        %>
        <div class="alert alert-danger">
            <%: Html.ValidationSummary() %>
        </div>
        <%
          }
          if (ViewData["Error"] != null && !string.IsNullOrEmpty(ViewData["Error"].ToString()))
          {	
        %>
        <div class="alert alert-danger">
            <%: ViewData["Error"].ToString() %>
        </div>
        <%
          }
        %>
        <div class="chartList grid simple" id="chartList">
            <% if (this.Model != null && this.Model.Count() > 0)
               {
            %>
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
            <div class="grid-body">
                <%
                    if (this.Model != null && this.Model.Count() > 0)
                    {
                %>
                <%                    
                        Html.Grid(this.Model).Columns(column =>
                        {
                            column.For(c => c.ChartName).Named(this.GetLocalResourceObject("lbl_ChartName").ToString());
                            // column.For(c => UserIdentity.HasPrivilege(ChartPrivilegeConstants.EditChart) ? "<a href='/ChartDetails/ManageCharts?chartId=" + c.ChartId + "'><i class='fa fa-edit'></i></a>" : "-").Named("Edit").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign");
                            column.For(c => UserIdentity.HasPrivilege(ChartPrivilegeConstants.ViewChart) ? "<a href='#' class='PreviewChart' data-chartId='" + c.ChartId + "'><i class='fa fa-bar-chart-o'></i></a>" : "-").Named(this.GetLocalResourceObject("lbl_Preview").ToString()).DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign", style => "width:100px;");
                            column.For(c => UserIdentity.HasPrivilege(ChartPrivilegeConstants.DeleteChart) ? "<a href='/ChartDetails/DeleteChart?chartId=" + c.ChartId + "'><i class='fa fa-trash-o'></i></a>" : "-").Named(this.GetGlobalResourceObject("General","Delete").ToString()).DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign", style => "width:100px;");
                        }).Attributes(id => "chartTable", @class => "celloTable").Render();
                %>
                <%
                    }
                    else
                    {
                %>
                <div class="alert alert-info">
                    <%: this.GetLocalResourceObject("i_NoChart")%>
                </div>
                <% } %>
            </div>
        </div>
        <div id="previewChartResult" style="display: none;">
            <section class="panel purple">
                <header class="panel-heading">
                    <h4>
                        <i class="fa fa-picture-o"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_Preview")%></h4>
                    <div class="actions pull-right">
                        <a href="#" class="btn btn-sm btn-default" id="btnBack" onclick="javascript:goBack(this);" title="<%: this.GetLocalResourceObject("t_Gobacktolisting")%>">
                            <i class="fa fa-arrow-left"></i>&nbsp;<%:this.GetGlobalResourceObject("General","Back") %></a>
                        <a href="#" class="btn btn-sm btn-default" id="btnRefresh" onclick="javascript:doRefreshChart(this);" title="<%: this.GetLocalResourceObject("t_Redrawthechart")%>">
                            <i class="fa fa-refresh"></i>&nbsp;Refresh</a>
                        <a download="preview.png" data-target="chart"
                            href="#" class="btn btn-sm btn-info" id="btnDownloadChart" onclick="javascript:downloadChart(this);"
                            title="<%: this.GetLocalResourceObject("t_Downloadasimage")%>"><i class="fa fa-cloud-download"></i>&nbsp;<%:this.GetGlobalResourceObject("General","Download") %></a>
                    </div>
                </header>
                <div class="chart-wrapper panel-body">
                    <div id="chart">
                    </div>
                </div>
            </section>
        </div>
        <div class="chartDynamicParamters" style="display: none">
            <% Html.RenderPartial("~/Views/QueryBuilder/ManageDynamicVariables.ascx"); %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" href="/Content/QueryBuilderStyles.css" />
    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>
    <script type="text/javascript">
        function doRefreshChart() {
            var chartId = $('#btnRefresh').data('chartid');
            loadChart(chartId, false);
        }

        function goBack(src) {
            $('#previewChartResult').fadeOut('normal', function () {
                $('#chartList').show('slide', { direction: 'left' }, 300, function () {
                    var c = $("#chart").data('celloChart');
                    if (c) {
                        c.destroy();
                    }
                    $("#chart").empty();
                });
            });

            return false;
        }

        function getChOpts(data, odata) {
            var options = {
                data: data,
                dataSource: {
                    data: data
                },
                theme: odata.theme,
                title: {
                    text: odata.title.text
                },
                legend: {
                    visible: odata['legend']['visible'],
                    position: odata['legend']['position']
                },
                seriesDefaults: {
                    type: odata['seriesDefaults']['type'] || 'line',
                    stack: odata['seriesDefaults']['stack'] == "true",
                    labels: {
                        position: "outsideEnd",
                        align: "column",
                        visible: true
                    },
                    tooltip: {
                        visible: odata['seriesDefaults']['tooltip']['visible'],
                        format: odata['seriesDefaults']['tooltip']['format'],
                        template: odata['seriesDefaults']['tooltip']['template']
                    }
                },
                categoryAxis: odata.categoryAxis,
                valueAxis: odata.valueAxis,
                tooltip: {
                    visible: odata['tooltip']['visible'] == "on",
                    format: odata['tooltip']['format'],
                    template: odata['tooltip']['template']
                },
                clickQueryDataField: odata['clickQueryDataField']
            };

            if ($.isArray(odata.series)) {
                options.series = [];
                for (i = 0; i < odata.series.length; i++) {
                    var sopt = {
                        name: odata.series[i].name,
                        title: { text: odata.series[i].title.text },
                        field: odata.series[i].field,
                        labels: {
                            visible: odata.series[i].labels.visible,
                            format: odata.series[i].labels.format,
                            template: odata.series[i].labels.template
                        },
                        stack: odata.series[i].stack
                    };
                    options.series.push(sopt);
                }
            }

            if (options.seriesDefaults.type == 'pie' || options.seriesDefaults.type == 'donut') {
                if (data) {
                    options.series[0].data = _getPieData(data, options.categoryAxis.field, options.valueAxis.field);
                    options.dataSource = {};
                    options.series[0].field = null;
                    options.categoryAxis = null;
                    options.valueAxis = null;
                }
            }

            return options;
        }

        function loadChart(chartId, animate) {
            $.post('/ChartDetails/GetPreviewChartDetails/', { chartId: chartId }, function (data) {
                if (!data || data.Error) {
                    alert((data && data.Error) ? data.Error : '<%: this.GetLocalResourceObject("e_PreviewChart")%>');
                    return;
                }

                if (!data.data) {
                    alert('<%: this.GetLocalResourceObject("e_ChartData")%>');
                    return;
                }

                var chartData = JSON.parse(data.data);
                var opts = {};
                if (data.details.ChartOptions) {
                    opts = JSON.parse(data.details.ChartOptions);
                }

                var options = getChOpts(chartData, opts);
                //var options = getOptions(null, chartData, opts);
                console.log(options);

                $('#btnRefresh').data('chartid', chartId);

                if (animate) {
                    $('#chartList').hide('slide', { direction: 'left' }, 300, function () {
                        $('#previewChartResult').show('slide', { direction: 'right' }, 300, function () {
                            var c = $("#chart").data('celloChart');
                            if (c) {
                                c.destroy();
                            }
                            $("#chart").celloChart(options);
                        });
                    });
                } else {
                    var c = $("#chart").data('celloChart');
                    if (c) {
                        c.destroy();
                    }
                    $("#chart").celloChart(options);
                }
            });
        }

        $(function () {
            $("a.PreviewChart").click(function (e) {
                e.preventDefault();
                var chartId = $(this).data('chartid');
                loadChart(chartId, true);
                return false;
            });
        });
    </script>
    <script type="text/javascript">

        var thisDataTable = null;

        $(document).ready(function () {
            thisDataTable = $('#chartTable').dataTable({
                "bFilter": true,
                "bSort": true,
                "bInfo": true
            });

            $('#searchText').keyup(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    e.stopPropagation();
                    DoSearch();
                } else if (e.keyCode == 27) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(this).val('');
                    DoSearch();
                }
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            thisDataTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
</asp:Content>
