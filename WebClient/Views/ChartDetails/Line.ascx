﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.ChartBuilder.ServiceContracts.Constants" %>
<table style="width: 100%;" class="celloTable details_container_table">
    <tbody>
        <tr>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("lbl_Labels").ToString())%>
            </td>
            <td class="row5">
                <%: Html.DropDownList(ChartPropertyConstants.PointLabels)%>
            </td>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("lbl_Color").ToString())%>
            </td>
            <td class="row5">
                <%
                    if (ViewData[ChartPropertyConstants.Color] == null)
                    {
                        ViewData[ChartPropertyConstants.Color] = "#abcdef";
                    }
                %>
                <%:Html.TextBox(ChartPropertyConstants.Color, ViewData[ChartPropertyConstants.Color].ToString(), new { @class = "simple_color_custom" })%>
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">
    $('.simple_color_custom').simpleColor({
        cellWidth: 9,
        cellHeight: 9,
        border: '1px solid #333333',
        buttonClass: 'button',
        defaultColor: '#abcdef',
        displayColorCode: true
    });

    $(".simpleColorSelectButton button").click(function () {
        $('.simple_color_custom').val($(".simpleColorDisplay").html());
    });
</script>
