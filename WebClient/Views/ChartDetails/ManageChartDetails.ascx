﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.ChartBuilder.ServiceContracts.Constants" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<% string reportObjectId = string.Empty;

   if (ViewData["reportObjectId"] != null)
   {
       reportObjectId = ViewData["reportObjectId"].ToString();
   }
%>
<div class="chartmanagement">
    <%
        if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DuplicateNameException")))
        {	
    %>
    <div class="error">
        <%: Html.ValidationMessage("DuplicateNameException")%>
    </div>
</div>
<%  
        }
        if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
        {	
%>
<div class="error">
    <%: Html.ValidationMessage("Error")%>
</div>
<%  
        }
%>
<%
    using (Html.BeginForm("ManageCharts", "ChartDetails", FormMethod.Post, new { id = "chartsForm" }))
    {
%>
<div id="chartmessages" style="color: red; margin-left: 25px; display: none">
    <div class="error">
    </div>
</div>
<div style="clear: both">
</div>
<div style="border: 1px solid #B5CBD3;">
    <h4>
        <%: this.GetLocalResourceObject("Title") %>
    </h4>
    <%
        string currentFormMode = CurrentFormMode.Insert.ToString();
        if (ViewData["formMode"] != null && !string.IsNullOrEmpty(ViewData["formMode"].ToString()))
        {
            currentFormMode = ViewData["formMode"].ToString();
        }

        CelloSaaS.ChartBuilder.Model.ChartDetails chartDetail = null;

        if (ViewData["chartDetails"] != null)
        {
            chartDetail = (CelloSaaS.ChartBuilder.Model.ChartDetails)ViewData["chartDetails"];
        }
    %>
    <%: Html.Hidden("formMode",currentFormMode) %>
    <%: Html.Hidden("showPreview") %>
    <table style="width: 100%;" class="chartDetailsTable celloTable details_container_table"
        cellpadding="5px" cellspacing="1px">
        <tr>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("lbl_ChartTitle").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <% string chartId = null;
                   if (chartDetail != null && !string.IsNullOrEmpty(chartDetail.ChartId))
                   {
                       chartId = chartDetail.ChartId;
                   } %>
                <%: Html.Hidden("ChartDetails_ChartId",chartId)%>
                <% if (chartDetail != null && !string.IsNullOrEmpty(chartDetail.ChartName))
                   {%>
                <%: Html.TextBox("ChartDetails_Name", chartDetail.ChartName)%>
                <%: Html.ValidationMessage("InvalidName","*") %>
                <% }
                   else
                   {%>
                <%: Html.TextBox("ChartDetails_Name", null)%>
                <%: Html.ValidationMessage("InvalidName","*") %>
                <% } %>
            </td>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("lbl_ChartType").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%: Html.DropDownList(ChartPropertyConstants.ChartType) %>
                <%: Html.ValidationMessage("InvalidChartType", "*") %>
            </td>
        </tr>
        <tr>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("lbl_ChartSeriesType").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%: Html.DropDownList("SeriesChartTypes", null, new { @onchange = "FillChartProperties()" })%>
                <%: Html.ValidationMessage("InvalidSeriesType", "*")%>
            </td>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("lbl_PickADataSource").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%: Html.DropDownList("ChartDataSources",null, new{@class="ChartDataSources",@onChange="OnChartDataSourceChange('"+reportObjectId+"')"})%>
                <%: Html.ValidationMessage("InvalidChartSource", "*")%>
            </td>
        </tr>
    </table>
</div>
<div class="sourceColumns">
    <h4>
        <%: this.GetLocalResourceObject("ManageChartDataSourceColumns")%>
        <a class="customizeColumns" style="float: right">
            <img src="../../App_Themes/CelloSkin/ico-collapse.gif" alt="Expand" />
        </a>
    </h4>
    <table style="width: 100%;" cellpadding="5px" cellspacing="1px" class="celloTable details_container_table">
        <tr>
            <td class="row4">
                <%:Html.Label(this.GetLocalResourceObject("lbl_XAxisColumns").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%:Html.DropDownList("XAxisColumns")%>
                <%: Html.ValidationMessage("InvalidXAxisColumns","*") %>
                <%
                   string xaxisId = string.Empty;
                   if (chartDetail != null && !string.IsNullOrEmpty(chartDetail.ChartsAxisFields[ChartAxesConstants.XAxis].AxisId))
                   {
                       xaxisId = chartDetail.ChartsAxisFields[ChartAxesConstants.XAxis].AxisId;
                   }
                %>
                <%: Html.Hidden("XChartAxisFields_Id",xaxisId) %>
            </td>
            <td class="row4">
                <%:Html.Label(this.GetLocalResourceObject("lbl_YAxisColumns").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%:Html.DropDownList("YAxisColumns")%>
                <%: Html.ValidationMessage("InvalidYAxisColumns","*") %>
                <%
                   string yaxisId = string.Empty;
                   if (chartDetail != null && !string.IsNullOrEmpty(chartDetail.ChartsAxisFields[ChartAxesConstants.YAxis].AxisId))
                   {
                       yaxisId = chartDetail.ChartsAxisFields[ChartAxesConstants.YAxis].AxisId;
                   }
                %>
                <%: Html.Hidden("YChartAxisFields_Id",yaxisId) %>
            </td>
        </tr>
        <tr>
            <td class="row4">
                <%:Html.Label(this.GetLocalResourceObject("lbl_XAxisTitle").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%
                   string xaxisTitle = null, yaxistitle = null;

                   if (chartDetail != null
                       && !string.IsNullOrEmpty(chartDetail.ChartsAxisFields[ChartAxesConstants.XAxis].Title))
                   {
                       xaxisTitle = chartDetail.ChartsAxisFields[ChartAxesConstants.XAxis].Title;
                   }
                   if (chartDetail != null
                      && !string.IsNullOrEmpty(chartDetail.ChartsAxisFields[ChartAxesConstants.YAxis].Title))
                   {
                       yaxistitle = chartDetail.ChartsAxisFields[ChartAxesConstants.YAxis].Title;
                   }           
                %>
                <%:Html.TextBox("XAxisTitle",xaxisTitle) %>
                <%: Html.ValidationMessage("InvalidXAxisTitle","*") %>
            </td>
            <td class="row4">
                <%:Html.Label(this.GetLocalResourceObject("lbl_YAxisTitle").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%:Html.TextBox("YAxisTitle",yaxistitle) %>
                <%: Html.ValidationMessage("InvalidYAxisTitle", "*")%>
            </td>
        </tr>
    </table>
</div>
<div class="customizeChart">
    <h4>
        <%: this.GetLocalResourceObject("CustomizeChart")%>
        <a class="chartCustomization" style="float: right">
            <img src="../../App_Themes/CelloSkin/ico-collapse.gif" alt="collapse" />
        </a>
    </h4>
    <table style="width: 100%;" cellpadding="5px" cellspacing="1px" class="celloTable details_container_table">
        <tr>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("ChartSeriesName").ToString())%>
            </td>
            <td class="row5">
                <%
                   string seriesName = string.Empty;
                   if (chartDetail != null && chartDetail.ChartsProperties.ContainsKey(ChartPropertyConstants.SeriesName) && !string.IsNullOrEmpty(chartDetail.ChartsProperties[ChartPropertyConstants.SeriesName].PropertyValue))
                   {
                       seriesName = chartDetail.ChartsProperties[ChartPropertyConstants.SeriesName].PropertyValue;
                   }
                %>
                <%: Html.TextBox(ChartPropertyConstants.SeriesName, seriesName, new { @title = this.GetLocalResourceObject("SeriesNameTitle").ToString() })%>
            </td>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("TitleFontSizePixel").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
            </td>
            <td class="row5">
                <%
                   string fontSize = "10";
                   if (chartDetail != null && chartDetail.ChartsProperties != null && chartDetail.ChartsProperties.ContainsKey(ChartPropertyConstants.FontSize))
                   {
                       fontSize = chartDetail.ChartsProperties[ChartPropertyConstants.FontSize].PropertyValue.Split('~')[0];
                   }
                %>
                <%:Html.TextBox(ChartPropertyConstants.FontSize, fontSize, new { @title=this.GetLocalResourceObject("FontSizeTitle").ToString() })%>
                <%: Html.ValidationMessage("InvalidFontSize", "*")%>
            </td>
            <tr>
                <td class="row4">
                    <%: Html.Label(this.GetLocalResourceObject("ChartWidthPixel").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                </td>
                <td class="row5">
                    <%
                   string chartWidth = "500";

                   if (chartDetail != null && chartDetail.ChartsProperties.ContainsKey(ChartPropertyConstants.Width) && chartDetail.ChartsProperties[ChartPropertyConstants.Width] != null)
                   {
                       chartWidth = chartDetail.ChartsProperties[ChartPropertyConstants.Width].PropertyValue.Split('~')[0];
                   }
                    %>
                    <span style="float: left">
                        <%:Html.TextBox(ChartPropertyConstants.Width, chartWidth, new { @title = this.GetLocalResourceObject("ChartWidthTitle").ToString() })%>
                        <%: Html.ValidationMessage("InvalidWidth","*") %>
                    </span>
                    <input type="hidden" value="Pixel" name="WidthUnitSpecs" class="WidthUnitSpecs" />
                </td>
                <td class="row4">
                    <%: Html.Label(this.GetLocalResourceObject("ChartHeightPixel").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                </td>
                <td class="row5">
                    <%
                   string chartHeight = null;

                   if (chartDetail != null && chartDetail.ChartsProperties.ContainsKey(ChartPropertyConstants.Height) && chartDetail.ChartsProperties[ChartPropertyConstants.Height] != null)
                   {
                       chartHeight = chartDetail.ChartsProperties[ChartPropertyConstants.Height].PropertyValue.Split('~')[0];
                   }
                   double chartHt = 500.0d;

                   if (!string.IsNullOrEmpty(chartHeight) && !string.IsNullOrEmpty(fontSize))
                   {
                       chartHt = double.Parse(chartHeight) - double.Parse(fontSize);
                   }
                    %>
                    <span style="float: left">
                        <%:Html.TextBox(ChartPropertyConstants.Height, chartHt.ToString(), new { @title = this.GetLocalResourceObject("ChartHeightTitle").ToString() })%>
                        <%: Html.ValidationMessage("InvalidHeight", "*")%>
                    </span>
                    <input type="hidden" value="Pixel" name="HeightUnitSpecs" class="HeightUnitSpecs" />
                </td>
            </tr>
    </table>
    <div class="loadingproperties" style="display: none; text-align: center">
        <img src="<%: this.ResolveClientUrl("../../Content/ajax-loader.gif") %>" alt="loading" />
    </div>
    <div class="chartProperties">
        <% 
        if (chartDetail != null && !string.IsNullOrEmpty(chartDetail.ChartType))
        {
            switch (chartDetail.ChartType)
            {
                case "Bar":
                case "Column":
        %>
        <% Html.RenderPartial("BarColumn", ViewData);%>
        <%
           break;
                case "Line":
        %>
        <% Html.RenderPartial("Line", ViewData);%>
        <%
           break;
                case "Pie":
        %>
        <% Html.RenderPartial("Pie", ViewData);%>
        <%
           break;
                case "Point":
        %>
        <% Html.RenderPartial("Point", ViewData);%>
        <%
           break;
                default: break;
            }
        } %>
    </div>
    <div class="row" style="height: 30px">
        <div class="controls" style="height: 50px">
            <%
        if (CelloSaaS.Library.UserIdentity.HasPrivilege(ChartPrivilegeConstants.ViewChart))
        {
            %>
            <div class="button" style="float: left">
                <a title="<%: this.GetLocalResourceObject("PreviewChartTitle") %>" id="viewChart"><span>
                    <%:this.GetGlobalResourceObject("General", "View")%></span> </a>
            </div>
            <%
        }
        if (CelloSaaS.Library.UserIdentity.HasPrivilege(ChartPrivilegeConstants.EditChart) || CelloSaaS.Library.UserIdentity.HasPrivilege(ChartPrivilegeConstants.AddChart))
        {
            %>
            <div class="button" style="float: left">
                <a title="<%: this.GetLocalResourceObject("PreviewChartTitle") %>" id="saveChart"><span>
                    <%:this.GetGlobalResourceObject("General", "Save")%></span> </a>
            </div>
            <%
        }%>
        </div>
    </div>
</div>
<div class="chartDynamicParamters" style="display: none">
    <% Html.RenderPartial("~/Views/QueryBuilder/ManageDynamicVariables.ascx"); %>
</div>
<% } %>
<style type="text/css">
    .ChartDataSources
    {
        width: 150px;
    }
</style>
<script type="text/javascript">
    $(".chartDetailsTable input[type='text']").tooltip();
</script>
