﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content-admin">
        <div class="heading_container">
            <h1>
                <%: this.GetLocalResourceObject("Title") %>
            </h1>
            <div class="inner_hold">
                <div class="green_but" style="float: right">
                    <a href="<%: Url.Action("Index","ChartDetails") %>"><span><%:this.GetGlobalResourceObject("General","Back") %> </span> </a>
                </div>
            </div>
        </div>
        <%
            if (Html.ValidationSummary() != null)
            {
        %>
        <div class="error">
            <%: Html.ValidationSummary() %>
        </div>
        <%
            }
        %>
        <% Html.RenderPartial("ManageChartDetails"); %>
        <% ViewData["LinkedTo"] = "ChartDataSources"; %>
        <% Html.RenderPartial("DisplayChart"); %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%: Url.Content("../../Content/chartbuilder.css")%>" rel="stylesheet"
        type="text/css" />
    <script src="<%: Url.Content("../../Scripts/managedatasource.js")%>" type="text/javascript"></script>
    <script src="<%: Url.Content("../../Scripts/ManageCharts.js")%>" type="text/javascript"></script>
    <script src="<%: Url.Content("../../Scripts/jquery.simple-color.js")%>" type="text/javascript"></script>
    <script src="<%: Url.Content("../../Scripts/jquery.tooltip.min.js")%>" type="text/javascript"></script>
    <style type="text/css">
        .chartDetailsTable a
        {
            cursor: pointer;
        }
        .simpleColorDisplay
        {
            float: left;
            font-family: Helvetica;
        }
    </style>
</asp:Content>
