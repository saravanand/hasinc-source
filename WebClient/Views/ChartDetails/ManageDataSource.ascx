﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Reporting.DataSources.Model.ViewModel" %>
<%
    string reportObjectId = string.Empty;
    if (ViewData["reportObjectId"] != null)
    {
        reportObjectId = ViewData["reportObjectId"].ToString();
    }
%>
<div id="<%:reportObjectId %>" class="manageDataSource" style="display: none">
    <%
        SourceContentViewModel ds = null;
        if (ViewData["sourceContentModel"] != null)
        {
            ds = (SourceContentViewModel)ViewData["sourceContentModel"];
        }
    %>
    <table style="width: 500px;" cellpadding="10px" cellspacing="0px">
        <thead>
            <tr>
                <th colspan="4" class="dataSourceHeading">
                    <%: this.GetLocalResourceObject("ManageDataSource")%>
                    <div style="float: right">
                        <a href="#" class="dataSource_close"><%: this.GetGlobalResourceObject("General", "Close")%></a>
                    </div>
                </th>
            </tr>
        </thead>
        <tr>
            <td>
                <%:Html.Label(this.GetLocalResourceObject("Name").ToString()) %>
            </td>
            <td>
                <%
                    string dataSrcId = string.Empty;
                    if (ds != null && !string.IsNullOrEmpty(ds.DataSource_Id))
                    {
                        dataSrcId = ds.DataSource_Id;
                    }
                    else if (ViewData["dataSrcId"] != null)
                    {
                        dataSrcId = ViewData["dataSrcId"].ToString();
                    }

                    string dataSrcName = string.Empty;
                    if (ds != null && !string.IsNullOrEmpty(ds.DataSource_Name))
                    {
                        dataSrcName = ds.DataSource_Name;
                    }
                %>
                <%: Html.Hidden("DataSource_Id", dataSrcId, new { @class = "DataSource_Id" })%>
                <%: Html.TextBox("DataSource_Name", dataSrcName, new { @class = "DataSource_Name" })%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.Label(this.GetLocalResourceObject("SourceContentType").ToString())%>
            </td>
            <td>
                <%: Html.DropDownList("SourceContentTypes", null, new { @class = "SourceContentTypes", @onChange="onSourceContentChange('"+reportObjectId+"')"
                })%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.Label(this.GetLocalResourceObject("SourceContent").ToString())%>
            </td>
            <td colspan="1" align="left" class="sourceTypes">
                <%
                    string srcContentId = string.Empty;

                    if (ds != null && !string.IsNullOrEmpty(ds.SourceContent_Id))
                    {
                        srcContentId = ds.SourceContent_Id;
                    }

                    string sourceContent = string.Empty;

                    if (ds != null && !string.IsNullOrEmpty(ds.SourceContent))
                    {
                        sourceContent = ds.SourceContent;
                    }
            
                %>
                <%: Html.Hidden("SourceContent_Id", srcContentId, new { @class = "SourceContent_Id" })%>
                <%
                    if (ViewData["isBuiltInQuery"] != null && Convert.ToBoolean(ViewData["isBuiltInQuery"].ToString()))
                    {%>
                <%: Html.DropDownList("BuiltInQuerySources", null, new { @class = "BuiltInQuerySources",@style="display:block" })%>
                <%}
                    else
                    {
                %>
                <%: Html.TextArea("SourceContent", sourceContent, new { @class = "SourceContent" })%>
                <%} %>
            </td>
        </tr>
        <tr class="queryParameters" style="display: none">
            <td colspan="2" align="center">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" class="controls">
                <%
                    var linkedTo = "";

                    if (ViewData["LinkedTo"] != null)
                    {
                        linkedTo = ViewData["LinkedTo"].ToString();
                    }
                %>
                <div class="button" style="float: right">
                    <a id="ManageDataSource" onclick="onDataSourceSave('<%:linkedTo %>','<%:reportObjectId %>')">
                        <%--<img src="../../App_Themes/CelloSkin/action-save.gif" alt="Save" title="save" />--%>
                        <span><%: this.GetGlobalResourceObject("General","Save") %></span> </a>
                </div>
                <div class="linkButton">
                    <a id="FetchParams" class="FetchParams" onclick="onSourceParametersFetch('<%:reportObjectId %>')">
                        <%: this.GetLocalResourceObject("FetchParameters") %></a>
                </div>
            </td>
        </tr>
    </table>
</div>
