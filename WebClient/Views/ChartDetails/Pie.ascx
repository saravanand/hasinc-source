﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.ChartBuilder.ServiceContracts.Constants" %>
<table style="width: 100%;" class="celloTable details_container_table">
    <tbody>
        <tr>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("Legends").ToString())%>
            </td>
            <td class="row5">
                <%: Html.DropDownList(ChartPropertyConstants.PieDoughnutLegendOptions)%>
            </td>
            <td class="row4">
                <%: Html.Label(this.GetLocalResourceObject("PieDrawingStyles").ToString())%>
            </td>
            <td class="row5">
                <%: Html.DropDownList( ChartPropertyConstants.PieDrawingStyle)%>
            </td>
        </tr>
    </tbody>
</table>
