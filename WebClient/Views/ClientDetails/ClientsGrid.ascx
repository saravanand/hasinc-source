﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Collections.Generic.List<CelloSaaSWebClient.Models.ClientDetailDTO>>" %>
<%@ Import Namespace="CelloSaaSWebClient.Models" %>
<%
    if (Model == null || Model.Count < 1)
    {
%>
<div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <span>No Clients found</span>
</div>
<%
    }
%>
<div class="row-fluid">
    <div class="grid-title">
    </div>
    <div class="clientsGrid grid-body">
        <div class="celloTableWrapper">
            <%
    
                bool isDeletable = UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin) && false;

                Html.Grid(Model).Columns(
                column =>
                {
                    column.For(col => col.Name).Named("Name").Attributes(width => "200px");
                    column.For(col => col.Description).Named("Description").Attributes(width => "200px");
                    column.For(col => col.Uri).Named("Url").Attributes(width => "200px");
                    column.For(col => col.RequireConsent ? "Yes" : "No").Named("Requires Consent");
                    column.For(col => col.ScopeRestrictions).Named("Scope Restrictions");
                    column.For(col => col.ApplicationTypes).Named("Application Type");
                    column.For(col => col.AccessTokenType).Named("Access Token Type");
                    column.For(col => col.PublicAccessAllowed ? "Yes" : "No").Named("Is Public Access Allowed");
                    column.For(col => "<a data-id='" + col.Id + "' data-type='" + col.ApplicationTypes + "' class='edit' title='Edit'><i class='fa fa-pencil fa-fw'></i></a>").Named("Edit").DoNotEncode();
                    column.For(col => "<a href='javascript:void()' data-id='" + col.Id + "' data-type='" + col.ApplicationTypes + "' class='delete' title='Delete'><i class='fa fa-trash-o fa-fw'></i></a>").Named("Delete").Visible(isDeletable).DoNotEncode();
                }).Attributes(new Dictionary<string, object> { { "class", "celloTable" } }).Render();
            %>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $(".clientsGrid a.edit").on("click", function () {
            var id = $(this).data("id");
            var appType = $(this).data("type");
            var tenantId = $("#clientTenants :selected").val();

            doEdit(id, tenantId, appType);
        });

        $(".clientsGrid a.delete").on("click", function () {
            var id = $(this).data("id");
            var appType = $(this).data("type");
            var tenantId = $("#clientTenants :selected").val();
            deleteClient(id, tenantId, appType);
        });
    });
</script>
