﻿<%@ Page Title="Client Management" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="clientHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var editUri = '<%:Url.Action("ManageClients","ClientDetails")%>';
        var defaultTenantId = '<%:CelloSaaS.Library.UserIdentity.TenantID%>';
        var listUri = '<%:Url.Action("ListAsync","ClientDetails")%>';
        var addUri = '<%: Url.Action("ManageClient","ClientDetails")%>';
        var removeUri = '<%: Url.Action("RemoveClient","ClientDetails") %>';
    </script>
    <script type="text/javascript" src="/Scripts/manageclients.js?123456">       
    </script>
</asp:Content>
<asp:Content ID="clientContent" ContentPlaceHolderID="MainContent" runat="server">
    <%
        Html.EnableUnobtrusiveJavaScript(false);
        Html.EnableClientValidation(false);
    %>
    <div id="loading"></div>
    <div class="page-title">
        <h3>Manage Clients</h3>
        <%
            if (CelloSaaS.Library.UserIdentity.IsInRole(RoleConstants.ProductAdmin))
            {
        %>
        <div class="pull-right">
            <a href="javascript:void();" onclick="addClient()" class="btn btn-success"><i class="fa fa-plus"></i>Add Client Details</a>
        </div>
        <%
            }
        %>
    </div>
    <div class="row-fluid pd-25">
        <div class="alert alert-danger" style="display: none">
             <a href="#" class="close" data-dismiss="alert">&times;</a>
            <%= Html.ValidationSummary() %>
        </div>
        <div class="row-fluid">
            <section class="panel purple">
                <header class="panel-heading">
                    <h4>Client Details</h4>
                </header>
                <div class="grid-title">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="grid-title">
                                <h4>Choose A Tenant</h4>
                            </div>
                            <%=Html.DropDownList("ClientDetail.TenantId", null, new { @id = "clientTenants" }) %>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="clientListing grid simple">
        </div>
        <div class="clientDetail" id="clientDetail">
            <%--<% Html.RenderPartial("ManageClients", null); %>--%>
        </div>
    </div>
</asp:Content>
