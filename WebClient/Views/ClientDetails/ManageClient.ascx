﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaSWebClient.Models.ClientDetailDTO>" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%
    Html.EnableUnobtrusiveJavaScript(false);
    Html.EnableClientValidation(false);

    CurrentFormMode currentMode = new CurrentFormMode();
    if (ViewBag.FormMode != null)
    {
        currentMode = (CurrentFormMode)ViewBag.FormMode;
    }
%>

<div class="page-title">
    <h3>
        <% if (currentMode == CurrentFormMode.Insert)
           {
               ViewBag.Title = this.GetGlobalResourceObject("ClientDetailsResource", "AddClient");
           }
           else if (currentMode == CurrentFormMode.Edit)
           {
               ViewBag.Title = this.GetGlobalResourceObject("ClientDetailsResource", "UpdateClient");
           }
           else
           {
               ViewBag.Title = "Manage Clients";
           }
        %>
        <%:ViewBag.Title %>
    </h3>
    <div class="pull-right">
        <button type="button" onclick="SaveClient()" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;Save</button>
        <a href="javascript:cancelSave()" class="btn btn-default">Cancel</a>
    </div>
</div>
<div>
    <%
        var options = new AjaxOptions { HttpMethod = "post", InsertionMode = InsertionMode.Replace, UpdateTargetId = "clientDetail" };

        using (Ajax.BeginForm("Save", "ClientDetails", options, new { @id = "clientdetailsform" }))
        {
    %>
    <div class="pd-25">
        <%
            if (Html.ValidationMessage("Success") != null && !string.IsNullOrEmpty(Html.ValidationMessage("Success").ToHtmlString()))
            {
        %>
        <div id="statusMessage" style="display: block">
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <%:Html.ValidationMessage("Success")%>
                <script type="text/javascript">
                    setTimeout(window.location.reload(), 2000);
                </script>
            </div>
        </div>
        <%              }
            else if (Html.ValidationMessage("Error") != null && !string.IsNullOrEmpty(Html.ValidationMessage("Error").ToHtmlString()))
            {%>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <%:Html.ValidationMessage("Error")%>
        </div>
        <% }
            else if (Html.ValidationSummary() != null && !string.IsNullOrEmpty(Html.ValidationSummary().ToHtmlString()))
            {%>
        <div class="alert alert-danger">
            <%:Html.ValidationSummary()%>
        </div>
        <% } %>
        <div class="row-fluid">
            <section class="panel green">
                <header class="panel-heading">
                    <h4>Client Details for "<%:Model.Name%>"</h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("Name")%>
                                <%:Html.Hidden("Id", !string.IsNullOrEmpty(Model.Id) ? Model.Id.ToString() : Guid.Empty.ToString())%>
                                <%:Html.Hidden("TenantId", !string.IsNullOrEmpty(Model.TenantId) ? Model.TenantId : string.Empty)%>
                                <%:Html.TextBox("Name", Model.Name, new { required = "required" })%>
                            </div>
                            <div class="col-md-6">
                                <%:Html.Label("Description")%>
                                <%:Html.TextBox("Description", Model.Description)%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("Secret")%>
                                <span style="color: red">*</span>
                                <%:Html.Password("Secret", Model.Secret, new { autocomplete = "off", required="required" })%>
                            </div>
                            <div class="col-md-6">
                                <%:Html.Label("Client Type")%>
                                <%:Html.DropDownList("ClientType")%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("Uri")%>
                                <%:Html.TextBox("Uri", Model.Uri)%>
                            </div>
                            <div class="col-md-6">
                                <%:Html.Label("LogoUri")%>
                                <%:Html.TextBox("LogoUri", Model.LogoUri)%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("RequireConsent")%>
                                <%if (Model.RequireConsent)
                                  {%>
                                <input id="RequireConsent" type="checkbox" name="RequireConsent" checked="checked" value="true" />
                                <%}
                                  else
                                  {%>
                                <input id="RequireConsent" type="checkbox" name="RequireConsent" value="false" />
                                <%}%>
                            </div>

                            <div class="col-md-6">
                                <%:Html.Label("AllowRememberConsent")%>
                                <%if (Model.AllowRememberConsent)
                                  {
                                %>
                                <input id="AllowRememberConsent" type="checkbox" name="AllowRememberConsent" checked="checked" value="true" />
                                <%}
                                  else
                                  {%>
                                <input id="AllowRememberConsent" type="checkbox" name="AllowRememberConsent" value="false" />
                                <%}%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("Flows")%>
                                <%:Html.TextBox("Flows", Model.Flows, new { required = "required" })%>
                            </div>

                            <div class="col-md-6">
                                <%:Html.Label("RedirectUris")%>
                                <%:Html.TextArea("RedirectUris", Model.RedirectUris,new { required = "required" })%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("SigningKeyType")%>
                                <%:Html.DropDownList("SigningKeyType")%>
                            </div>

                            <div class="col-md-6">
                                <%:Html.Label("ApplicationTypes")%>
                                <%:Html.DropDownList("ApplicationTypes")%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("IdentityTokenLifetime")%>
                                <%:Html.TextBox("IdentityTokenLifetime", Model.IdentityTokenLifetime)%>
                            </div>

                            <div class="col-md-6">
                                <%:Html.Label("AccessTokenLifetime")%>
                                <%:Html.TextBox("AccessTokenLifetime", Model.AccessTokenLifetime)%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("RefreshTokenLifetime")%>
                                <%:Html.TextBox("RefreshTokenLifetime", Model.RefreshTokenLifetime)%>
                            </div>

                            <div class="col-md-6">
                                <%:Html.Label("AuthorizationCodeLifetime")%>
                                <%:Html.TextBox("AuthorizationCodeLifetime", Model.AuthorizationCodeLifetime)%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("ScopeRestrictions")%>
                                <%:Html.TextBox("ScopeRestrictions", Model.ScopeRestrictions)%>
                            </div>

                            <div class="col-md-6">
                                <%:Html.Label("AccessTokenType")%>
                                <%:Html.DropDownList("AccessTokenType", Model.AccessTokenType)%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <%:Html.Label("Is Publicly Accessible")%>
                                <%: Html.Hidden("PublicAccessAllowed") %>
                                <%if (Model.PublicAccessAllowed)
                                  {%>
                                <input id="publicAccess" type="checkbox" name="publicAccess" checked="checked" value="true" />
                                <%}
                                  else
                                  {%>
                                <input id="publicAccess" type="checkbox" name="publicAccess" value="false" />
                                <%}%>
                            </div>
                        </div>
                    </div>
            </section>
        </div>
    </div>
    <div class="form-actions">
        <div class="pull-right">
            <button type="button" onclick="SaveClient()" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;Save</button>
            <a href="javascript:cancelSave()" class="btn btn-default">Cancel</a>
        </div>
    </div>
    <%}
    %>
</div>
<script type="text/javascript">
    $(function () {
        $("select").select2({ width: "100%" });

    });

    function SaveClient() {
        if ($("#RequireConsent").is(":checked")) {
            $("#RequireConsent").val(true);
        }
        else {
            $("#RequireConsent").val(false);
        }

        if ($("#AllowRememberConsent").is(":checked")) {
            $("#AllowRememberConsent").val(true);
        }
        else {
            $("#AllowRememberConsent").val(false);
        }

        if ($("#publicAccess").is(":checked")) {
            $("#PublicAccessAllowed").val(true);
        }
        else {
            $("#PublicAccessAllowed").val(false);
        }

        $("#clientdetailsform").submit();

    }

</script>
