<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.Configuration.PickupList>" %>
<% 
    using (Html.BeginForm("AddPickupList", "Configuration", FormMethod.Post, new { id = "AddPickupListPage" }))
    {
%>
<%
        HtmlHelper.ClientValidationEnabled = false;
        HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel green">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("AddPickupList")%></h4>
    </header>
    <div class="panel-body">
        <%if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
          { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <%}%>
        <div class="form-container">
            <div class="form-group <%= Html.ValidationMessage("Name", "*") !=null ? "has-error" : ""%>">
                <label class="mandatory"><%: this.GetLocalResourceObject("Name") %></label>
                <%= Html.TextBox("Name", Model.Name, new { maxlength = 100,required="required" })%>
                <%= Html.ValidationMessage("Name", "*")%>
            </div>
            <div class="pull-right">                
                <a class="btn btn-success" href="#" onclick="$('form#AddPickupListPage').submit()" title="<%= this.GetGlobalResourceObject("General","Save") %>">
                    <i class="fa fa-save"></i>&nbsp;<%= this.GetGlobalResourceObject("General","Save") %></a>
                <a class="btn btn-default" href="#" title="<%= this.GetGlobalResourceObject("General","Cancel") %>" onclick="javascript:btnCancel()">
                    <%= this.GetGlobalResourceObject("General","Cancel") %></a>
            </div>
        </div>
        <script>
            $(function () {
                $('form#AddPickupListPage').submit(function () {
                    AddPickupListDetails();
                    return false;
                });
            });
        </script>
    </div>
</section>
<% } %>
