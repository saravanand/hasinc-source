<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.Configuration.PickupListValue>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% using (Html.BeginForm("AddPickupListValue", "Configuration", FormMethod.Post, new { id = "AddPickupListValuePage" }))
   {
%>
<section class="panel green">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("AddPickupListValue")%></h4>
    </header>
    <div class="panel-body">
        <%if (Html.ValidationSummary() != null)
          { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <%}%>
        <div class="form-container">
            <div class="form-group <%= Html.ValidationMessage("ItemId", "*") !=null ? "has-error" : ""%>">
                <label class="mandatory">
                    <%: this.GetLocalResourceObject("Value") %>
                </label>
                <%= Html.Hidden("PickupListID",Model.PickupListId) %>
                <%= Html.TextBox("ItemId", Model.ItemId, new { maxlength = 50,required="required" })%>
                <%= Html.CelloValidationMessage("ItemId", "*")%>
            </div>
            <div class="form-group <%= Html.ValidationMessage("ItemName", "*") !=null ? "has-error" : ""%>">
                <label class="mandatory">
                    <%: this.GetLocalResourceObject("Name") %>
                </label>
                <%= Html.TextBox("ItemName", Model.ItemName, new { maxlength = 50 ,required="required"})%>
                <%= Html.CelloValidationMessage("ItemName", "*")%>
            </div>
            <div class="form-group">
                <label>
                    <%: this.GetLocalResourceObject("Description") %>
                </label>
                <%= Html.TextArea("Description", Model.Description, new { onKeyDown = "limitText(this,1000)", onKeyUp = "limitText(this,1000)" })%>
            </div>
            <%                     
       if (ViewData["RelationshipDetailss"] != null && (((List<CelloSaaS.Model.Configuration.RelationshipMetaData>)ViewData["RelationshipDetailss"]).Count > 0) && ViewData["PickupListDetails"] != null && ((Dictionary<string, SelectList>)ViewData["PickupListDetails"]).Count > 0)
       {
           Dictionary<string, SelectList> selectListDetails = (Dictionary<string, SelectList>)ViewData["PickupListDetails"];
           SelectList pickupListValueSelectList = null;
           int count = 0;
           foreach (CelloSaaS.Model.Configuration.RelationshipMetaData relationship in (List<CelloSaaS.Model.Configuration.RelationshipMetaData>)ViewData["RelationshipDetailss"])
           {
               pickupListValueSelectList = selectListDetails[relationship.Id];
               if (pickupListValueSelectList != null)
               {
                   count++;
                   if (count % 2 == 0)
                   {%>
            <%}                  
            %>
            <div class="form-group <%= Html.ValidationMessage("Val_"+relationship.Id, "*") !=null ? "has-error" : ""%>">
                <label class="mandatory">
                    <%= relationship.TargetName %></label>
                <%=Html.DropDownList(relationship.Id, pickupListValueSelectList, "-- Select " + relationship.TargetName + " --")%>
                <%= Html.CelloValidationMessage("Val_"+relationship.Id, "*")%>
            </div>
            <% }
           }
       } %>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="#" onclick="javascript:AddPickupListValueDetails()" title="<%= this.GetGlobalResourceObject("General","Save") %>">
                <i class="fa fa-save"></i>&nbsp;<%= this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="#" title="<%= this.GetGlobalResourceObject("General","Cancel") %>" onclick="javascript:btnCancel()">
                <%= this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('#AddPickupListValuePage select').each(function () {
            $(this).attr('style', 'width:100%;');
        });
        $('#AddPickupListValuePage select').select2();
    });
</script>
<% } %>
