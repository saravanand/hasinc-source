<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.Configuration.RelationshipMetaData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("AddRelationshipMetaData", "Configuration"))
       {%>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("AddRelation") %></h3>
        <div class="pull-right">
            <%=Html.CelloSubmitButton("<i class=\"fa fa-save\"></i>&nbsp;" + this.GetGlobalResourceObject("General","Save")) %>
            <%=Html.CelloButton(Url.Action("RelationshipDetailsList"), this.GetGlobalResourceObject("General","Cancel")) %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("AddRelationshipSuccessMessage")))
           { %>
        <div class="alert alert-success">
            <%=Html.CelloValidationMessage("AddRelationshipSuccessMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <%} %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4>Pickuplists</h4>
            </header>
            <div class="panel-body">
                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="mandatory">
                                    <%= Html.Hidden("Id", Model.Id)%>
                                    <%= Html.Hidden("TenantId",Model.TenantId)%>
                                    <input type="hidden" name="TargetType" id="TargetType" value="PickupList" />
                                    <%: this.GetLocalResourceObject("Child") %>
                                </label>
                                <%= Html.DropDownList("SourceId", null, new { style = "width:100%;" })%>
                                <%= Html.CelloValidationMessage("SourceId", "*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("Parent") %>
                                </label>
                                <%= Html.DropDownList("TargetId",null,"--Select Parent--", new { style = "width:100%;" })%>
                                <%= Html.Hidden("hfTargetId",Model.TargetId) %>
                                <%= Html.CelloValidationMessage("TargetId", "*")%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <% } %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            var sourceId = $("#SourceId").val();
            if (sourceId != null && sourceId != "")
                GetTargetValues();
            $("#SourceId").change(function () {
                GetTargetValues();
            });
            $('select').select2();
        });
        function GetTargetValues(selectedValue) {
            var sourceId = $("#SourceId").val();
            if (sourceId == null || sourceId == "") {
                $('#TargetId option').remove();
                $('#TargetId').append($("<option selected>-- Select Parent --</option>"));
                return;
            }
            else {
                var targetTypeValue = $("#TargetType").val();
                var targetId = document.getElementById("hfTargetId").value;
                $.ajaxSettings.cache = false;
                $.getJSON("/Configuration/GetParentDetails",
                        { targetType: targetTypeValue },
                        function (data) {
                            if (data == "NoRecord" || data == "Error") {
                                GetErrorMessage(data);
                            }
                            else {
                                $('#TargetId option').remove();
                                $('#TargetId').append($("<option selected>-- Select Parent --</option>"));
                                for (var count = 0; count < data.length; count++) {
                                    var parentValue = data[count].Text.split(",");
                                    if (parentValue[0] == sourceId)
                                        continue;
                                    if (targetId != null && targetId != "" && parentValue[0] == targetId)
                                        $('#TargetId').append($("<option value=" + parentValue[0] + " selected> " + parentValue[1] + "</option>"));
                                    else
                                        $('#TargetId').append($("<option value=" + parentValue[0] + "> " + parentValue[1] + "</option>"));
                                }
                            }
                        });
            }
        }

        function GetErrorMessage(data) {
            if (data == "Error")
                alert('<%: this.GetLocalResourceObject("Error") %>');
            else
                alert('<%: this.GetLocalResourceObject("EmptyRecord") %>');
        }
    </script>
</asp:Content>
