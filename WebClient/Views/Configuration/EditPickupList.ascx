<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.Configuration.PickupList>" %>
<% 
    using (Html.BeginForm("EditPickupList", "Configuration", FormMethod.Post, new { id = "EditPickupListPage" }))
    {
%>
<%
        HtmlHelper.ClientValidationEnabled = false;
        HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel indigo">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("EditPickupList")%></h4>
    </header>
    <div class="panel-body">
        <%if (Html.ValidationSummary() != null)
          { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <%}%>
        <div class="form-container">
            <div class="form-group <%= Html.ValidationMessage("Name", "*") !=null ? "has-error" : ""%>">
                <label class="mandatory"><%: this.GetLocalResourceObject("Name") %></label>
                <%= Html.Hidden("ID", Model.Id)%>
                <%= Html.Hidden("TenantCode", Model.TenantCode)%>
                <%= Html.TextBox("Name", Model.Name, new { maxlength = 100 })%>
                <%= Html.CelloValidationMessage("Name", "*")%>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="#" onclick="$('form#EditPickupListPage').submit()" title="<%= this.GetGlobalResourceObject("General","Save") %>">
                    <i class="fa fa-save"></i>&nbsp;<%= this.GetGlobalResourceObject("General","Save") %></a>
                <a class="btn btn-default" href="#" title="Cancel" onclick="javascript:btnCancel()">
                    <%= this.GetGlobalResourceObject("General","Cancel") %></a>
            </div>
        </div>
        <script>
            $(function () {
                $('form#EditPickupListPage').submit(function () {
                    UpdatePickupListDetails();
                    return false;
                });
            });
        </script>
    </div>
</section>
<% } %>
