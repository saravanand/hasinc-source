﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<Dictionary<string, CelloSaaS.Model.Configuration.ModuleConfiguration>>" %>

<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% bool isProductAdmin = CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.IsProductAdminTenant; %>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title") %></h3>
        <% if (!isProductAdmin)
           { %>
        <div class="pull-right">
            <a class="btn btn-success" href="#" onclick="$('form').submit();" title="<%=this.GetGlobalResourceObject("General","Save")%>">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
        </div>
        <% } %>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-body">
                <% if (ViewData["Success"] != null)
                   { %>
                <div class="alert alert-success">
                    <%=ViewData["Success"]%>
                </div>
                <% }
                   else if (Html.ValidationSummary() != null)
                   { %>
                <div class="alert alert-danger">
                    <%=Html.ValidationSummary() %>
                </div>
                <% } %>
                <%using (Html.BeginForm())
                  { %>
                <%
                      var configSettingValue = ViewData["ModuleConfigSettingValue"] as Dictionary<string, string>;
                      if (configSettingValue == null)
                          configSettingValue = new Dictionary<string, string>();
                %>
                <% if (this.Model != null && this.Model.Count > 0)
                   { %>
                <% Html.Grid(this.Model.Values).Columns(col =>
                  {
                      col.For(x => x.Name).Named(this.GetLocalResourceObject("Name").ToString()).DoNotEncode();
                      col.For(x => x.Description).Named(this.GetLocalResourceObject("Description").ToString()).DoNotEncode();
                      col.For(x => Html.RadioButton(x.Id, ConfigurationSettingValueConstant.OWN_SETTING, isProductAdmin ? true : configSettingValue.ContainsKey(x.Id) ? configSettingValue[x.Id].Equals(ConfigurationSettingValueConstant.OWN_SETTING) : true))
                          .Named(this.GetLocalResourceObject("OwnSetting").ToString())
                          .Attributes(@class => "text-center")
                          .HeaderAttributes(@class => "text-center")
                          .DoNotEncode();
                      if (!isProductAdmin)
                      {
                          col.For(x => Html.RadioButton(x.Id, ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING, configSettingValue.ContainsKey(x.Id) && configSettingValue[x.Id].Equals(ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING)))
                              .Named(this.GetLocalResourceObject("ParentSetting").ToString())
                              .Attributes(@class => "text-center")
                              .HeaderAttributes(@class => "text-center")
                              .DoNotEncode();
                      }
                  }).Attributes(@class => "celloTable", style => "width:85%;margin:20px auto;").Render();
                %>
                <% }
                   else
                   { %>
                <div class="info">
                    <%: this.GetLocalResourceObject("EmptyRecord")%>
                </div>
                <% } %>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
