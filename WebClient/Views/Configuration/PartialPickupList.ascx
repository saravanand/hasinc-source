<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
    var filterTable = null;
    $(document).ready(function () {
        filterTable = $('#dataList').dataTable({
            "bDestroy": true,
            "iDisplayLength": 10,
            "bAutoWidth": true,
            "bFilter": true,
            "aaSorting": [[0, "asc"]],
            "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2, -3] }]
        });
        $(document).on('keypress', '#searchText', function (e) {
            if (e.keyCode == 13) {
                DoSearch();
                e.preventDefault();
            }
        });
    });

    function DoSearch() {
        var searchText = $('#searchText').val();
        if (searchText == 'Search') {
            searchText = '';
        }
        filterTable.fnFilter(searchText);
        $('div.alert').remove();
        return false;
    }
</script>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    var model = ViewData["PickupList"] as IEnumerable<CelloSaaS.Model.Configuration.PickupList>;
%>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <%if (model != null && model.Count() > 0)
              { %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                    </span>
                </div>
            </div>
            <% } %>
            <div class="col-sm-6 m-b-xs pull-right">
                <div class="pull-right">
                    <% if (UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.AddPickupList))
                       { %>
                    <a class="btn btn-success" href="#" title="<%=this.GetGlobalResourceObject("General","Add") %>" onclick="AddPickupList();">
                        <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Create") %></a>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body">
        <%if (TempData["Success"] != null)
          { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=TempData["Success"]%>
        </div>
        <% } %>
        <%if (TempData["Error"] != null)
          { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=TempData["Error"] %>
        </div>
        <% } %>
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Success")))
          { %>
        <div class="alert alert-success">
            <%=Html.CelloValidationMessage("Success")%>
        </div>
        <%} %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("Error")%>
        </div>
        <%} %>
        <%if (model != null && model.Count() > 0)
          { %>
        <% 
              Html.Grid(model).Columns(
            column =>
            {
                column.For(col => col.Name).Named(this.GetLocalResourceObject("Name").ToString());
                column.For(col => (string.IsNullOrEmpty(col.TenantCode) || !col.Status.Value) ? "-" : "<a id='Edit' href='#' title='" + string.Format(this.GetLocalResourceObject("Edit").ToString(), col.Name) + "' onclick =EditPickupList('" + col.Id + "')><i class='fa fa-edit'></i></a>")
                    .DoNotEncode().Named(this.GetGlobalResourceObject("General", "Edit").ToString()).Attributes(@class => "halign", style => "width:120px;").HeaderAttributes(@class => "halign");
                column.For(col => (string.IsNullOrEmpty(col.TenantCode)
                    ? "-"
                    : (col.Status.Value ?
                                "<a id='Delete' href='#' title='" + string.Format(this.GetLocalResourceObject("Deactivate").ToString(), col.Name) + "'  onclick=DeletePickupList('" + col.Id + "')><i class='fa fa-ban'></i></a>"
                                : "<a id='Activate' href='#' title='" + string.Format(this.GetLocalResourceObject("Activate").ToString(), col.Name) + "'  onclick=ActivatePickupList('" + col.Id + "')><i class='fa fa-plus-circle'></i></a>"))
                      ).DoNotEncode().Named(this.GetLocalResourceObject("Activate/Deactivate").ToString()).Attributes(@class => "halign", style => "width:130px;").HeaderAttributes(@class => "halign");
                column.For(col => !col.Status.Value ? "-" : Html.CelloActionLink("Manage", "PickUpListValues", new { pickUpListId = col.Id }).Replace("Manage", "<i class='fa fa-wrench'></i>"))
                    .Named(this.GetLocalResourceObject("ManageValues").ToString()).Attributes(@class => "halign", style => "width:150px;").HeaderAttributes(@class => "halign").DoNotEncode();
            }).Attributes(id => "dataList", @class => "celloTable").Render();
        %>
        <%}
          else
          {%>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("NoRecordFound") %>
        </div>
        <%}%>
    </div>
</div>
