<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<script type="text/javascript">
    var filterTable = null;
    $(document).ready(function () {
        filterTable = $('#pickupListValueTable').dataTable({
            "bDestroy": true,
            "iDisplayLength": 10,
            "bAutoWidth": true,
            "bFilter": true,
            "aaSorting": [[0, "asc"]],
            "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }]
        });
        $(document).on('keypress', '#searchText', function (e) {
            if (e.keyCode == 13) {
                DoSearch();
                e.preventDefault();
            }
        });
    });

    function DoSearch() {
        var searchText = $('#searchText').val();
        if (searchText == 'Search') {
            searchText = '';
        }
        filterTable.fnFilter(searchText);
        $('div.alert').remove();
        return false;
    }
</script>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    var model = ViewData["PickupListValue"] as IEnumerable<CelloSaaS.Model.Configuration.PickupListValue>;
%>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <%if (model != null && model.Count() > 0)
              { %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                    </span>
                </div>
            </div>
            <% } %>
            <div class="col-sm-6 m-b-xs pull-right">
                <div class="pull-right">
                    <% if (ViewData["PickupListForTenant"] != null && (bool)ViewData["PickupListForTenant"])
                       { %>
                    <a class="btn btn-warning" href="#" title="Copy" onclick="CopyPickupListValues('<%=ViewData["PickupListId"] %>')">
                        <%: this.GetLocalResourceObject("CopyFromGeneralPickUpList") %></a>
                    <%}%>
                    <% if (ViewData["SettingSource"].ToString() == ConfigurationSettingValueConstant.OWN_SETTING)
                       { %>
                    <a class="btn btn-success" href="#" onclick="AddPickupListValue('<%=ViewData["PickupListId"] %>')" title="<%=this.GetGlobalResourceObject("General", "Add")%>">
                        <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Add")%></a>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body">
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Success")))
          { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("Success")%>
        </div>
        <%} %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("Error")%>
        </div>
        <%} %>
        <%= Html.Hidden("PickupListId",ViewData["PickupListId"],new{id="PickupListId"}) %>
        <%if (model != null && model.Count() > 0)
          { %>
        <% 
              Html.Grid(model).Columns(
                  column =>
                  {
                      column.For(col => col.ItemId).Named(this.GetLocalResourceObject("Value").ToString());
                      column.For(col => col.ItemName).Named(this.GetLocalResourceObject("Name").ToString());
                      column.For(col => col.Description).Named(this.GetLocalResourceObject("Description").ToString());
                      if (ViewData["SettingSource"].ToString() == ConfigurationSettingValueConstant.OWN_SETTING)
                      {
                          column.For(col => !col.Status.Value ? "-" : "<a id='Edit' href='#' title='" + string.Format(this.GetLocalResourceObject("Edit").ToString(), col.ItemName) + "' onclick=EditPickupListValue('" + col.Id + "','" + col.PickupListId + "')><i class='fa fa-edit'></i></a>")
                              .DoNotEncode().Named("Edit").Attributes(@class => "halign", style => "width:120px;").HeaderAttributes(@class => "halign");
                          column.For(col => (col.Status.Value ?
                              "<a id='Delete' href='#' title='" + string.Format(this.GetLocalResourceObject("Deactivate").ToString(), col.ItemName) + "' onclick=DeletePickupListValue('" + col.Id + "','" + col.PickupListId + "','" + col.TenantCode + "')><i class='fa fa-ban'></i></a>"
                              : "<a id='Activate' href='#' title='" + string.Format(this.GetLocalResourceObject("Activate").ToString(), col.ItemName) + "' onclick=ActivatePickupListValue('" + col.Id + "','" + col.PickupListId + "','" + col.TenantCode + "')><i class='fa fa-plus-circle'></i></a>")
                              ).DoNotEncode().Named(this.GetLocalResourceObject("Activate/Deactivate").ToString()).Attributes(@class => "halign", style => "width:120px;").HeaderAttributes(@class => "halign");
                      }
                  }).Attributes(id => "pickupListValueTable", @class => "celloTable").Render();
        %>
        <%}
          else
          {%>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("NoValuesAvailable") %>
        </div>
        <%}%>
    </div>
</div>
<div class="divide-20"></div>
<div id="pickupListValueDetails">
</div>
