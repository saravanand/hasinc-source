<%@ Page Title="<%$ Resources:PickupList %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var pickupListActionUrl = '<%=Url.Action("PickupListDetails")%>';
        var addPickupListActionUrl = '<%=Url.Action("AddPickupList")%>';
        var editPickupListActionUrl = '<%=Url.Action("EditPickupList")%>';
        var deletePickupListActionUrl = '<%=Url.Action("DeletePickupList")%>';
        var activatePickupListActionUrl = '<%=Url.Action("ActivatePickupList")%>';

        function AddPickupListDetails() {
            $('div.alert').remove();
            formDetails = $('#AddPickupListPage');
            serializedForm = formDetails.serialize();
            $.post(addPickupListActionUrl, serializedForm, pickupListDetail_callBack);
        }

        function UpdatePickupListDetails() {
            $('div.alert').remove();
            formDetails = $('#EditPickupListPage');
            serializedForm = formDetails.serialize();
            $.post(editPickupListActionUrl, serializedForm, pickupListDetail_callBack);
        }

        function btnCancel() {
            loadPickupList();
            $('div.alert').remove();
        }

        function loadPickupList() {
            $.ajaxSettings.cache = false;
            $.get(pickupListActionUrl, pickupList_callBack);
        }

        function EditPickupList(pickupListId) {
            $('div.alert').remove();
            $.get(editPickupListActionUrl + "?pickupListId=" + pickupListId, loadPickupListDetails);
        }

        function DeletePickupList(pickupListId) {
            $('#statusMessage').fadeOut();
            if (confirm('<%: this.GetLocalResourceObject("DeactivateConfirmation") %>')) {
                $.get(deletePickupListActionUrl + "?pickupListId=" + pickupListId, pickupList_callBack);
            }
        }

        function ActivatePickupList(pickupListId) {
            $('#statusMessage').fadeOut();
            if (confirm('<%: this.GetLocalResourceObject("ActivateConfirmation") %>')) {
                $.get(activatePickupListActionUrl + "?pickupListId=" + pickupListId, pickupList_callBack);

            }
        }

        function AddPickupList() {
            $('div.alert').remove();
            $.get(addPickupListActionUrl, loadPickupListDetails);
        }


        function loadPickupListDetails(data) {
            $("#pickupListDetails").html(data);
        }

        //This method is used to render the data after added the role details
        function pickupListDetail_callBack(data) {
            if (data == "Success") {
                loadPickupList();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a><%: this.GetLocalResourceObject("SavedSuccessfully") %></div>').fadeIn();
            }
            else {
                // data = data + '<div id="pickupListDetails"></div>';
                $("#pickupListDetails").html(data);
            }

        }

        //This method is used to render the role details in the role List div
        function pickupList_callBack(data) {
            data = data + '<div id="pickupListDetails"></div>';
            $("#pickupListMain").html(data);

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("PickupList") %></h3>
        <div class="pull-right">
            <% if ((bool)ViewData["CanCopyFromParent"])
               { %>
            <a class="btn btn-warning" href="<%:Url.Action("CopyAllPickupListsFromParent","Configuration") %>" title="<%: this.GetLocalResourceObject("CopyAllPickupListFromParent")%>">
                <%: this.GetLocalResourceObject("CopyAllPickupListFromParent") %>
            </a>
            <% } %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <div id="statusMessage">
        </div>
        <div id="pickupListMain">
            <% Html.RenderPartial("PartialPickupList"); %>
        </div>
        <div class="divide-20"></div>
        <div id="pickupListDetails">
        </div>
    </div>
</asp:Content>
