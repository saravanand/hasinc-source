<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var pickUpListValueActionUrl = '<%=Url.Action("PickupListValuesList")%>';
        var addPickupListValueActionUrl = '<%=Url.Action("AddPickupListValue")%>';
        var editPickupListValueActionUrl = '<%=Url.Action("EditPickupListValue")%>';
        var deletePickupListValueActionUrl = '<%=Url.Action("DeletePickupListValue")%>';
        var activatePickupListValueActionUrl = '<%=Url.Action("ActivatePickupListValue")%>';
        var copyPickupListValuesActionUrl = '<%=Url.Action("CopyPickupListValues")%>';

        function CopyPickupListValues(pickupListId) {
            $.get(copyPickupListValuesActionUrl + "?pickupListId=" + pickupListId, pickupListValue_callBack);
        }

        function AddPickupListValueDetails() {
            formDetails = $('#AddPickupListValuePage');
            serializedForm = formDetails.serialize();
            $.post(addPickupListValueActionUrl, serializedForm, pickupListValueDetail_callBack);
        }

        function UpdatePickupListValueDetails() {
            formDetails = $('#EditPickupListValuePage');
            serializedForm = formDetails.serialize();
            $.post(editPickupListValueActionUrl, serializedForm, pickupListValueDetail_callBack);
        }

        function btnCancel() {
            loadPickupListValue();
        }

        function loadPickupListValue() {
            var pickupListId = document.getElementById('PickupListID').value;
            $.get(pickUpListValueActionUrl + "?pickupListId=" + pickupListId, pickupListValue_callBack);
        }

        function EditPickupListValue(pickupListValueId, pickupListId) {
            $.get(editPickupListValueActionUrl + "?pickupListValueId=" + pickupListValueId + "&pickupListId=" + pickupListId, loadPickupListValueDetails);
        }

        function DeletePickupListValue(pickupListValueId, pickupListId, tenantCode) {
            if (confirm('<%: this.GetLocalResourceObject("DeactivateConfirmation") %>')) {
                $.get(deletePickupListValueActionUrl + "?pickupListValueId=" + pickupListValueId + "&pickupListId=" + pickupListId + "&tenantCode=" + tenantCode, pickupListValue_callBack);
            }
        }

        function ActivatePickupListValue(pickupListValueId, pickupListId, tenantCode) {
            $('#statusMessage').fadeOut();
            if (confirm('<%: this.GetLocalResourceObject("ActivateConfirmation") %>')) {
                $.get(activatePickupListValueActionUrl + "?pickupListValueId=" + pickupListValueId + "&pickupListId=" + pickupListId + "&tenantCode=" + tenantCode, pickupListValue_callBack);
            }
        }

        function AddPickupListValue(pickupListId) {
            $.get(addPickupListValueActionUrl + "?pickupListId=" + pickupListId, loadPickupListValueDetails);
        }


        function loadPickupListValueDetails(data) {
            $('.info').fadeOut();
            $("#pickupListValueDetails").html(data);
        }

        function pickupListValueDetail_callBack(data) {
            if (data.indexOf('class="alert alert-success"') != -1) {
                $("#pickupListValueMain").html(data);
            } else {
                $("#pickupListValueDetails").html(data);
            }
        }

        function pickupListValue_callBack(data) {
            $("#pickupListValueMain").html(data);
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%:Url.Action("PickupList","Configuration") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <span class="semi-bold"><%:  ViewData["PickupistName"] %></span> - <%=this.GetLocalResourceObject("Title")%>
        </h3>
    </div>
    <% if (ViewData["PickupistName"] != null && !String.IsNullOrEmpty(ViewData["PickupistName"].ToString()))
       { %>
    <div class="row-fluid pd-25">
        <div id="pickupListValueMain">
            <% Html.RenderPartial("PartialPickupListValues"); %>
        </div>
    </div>
    <%}
       else
       { %>
    <div class="alert alert-info">
        <%: this.GetLocalResourceObject("NoValuesAvailable") %>
    </div>
    <%} %>
</asp:Content>
