<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;
        $(document).ready(function () {
            filterTable = $('#dataList').dataTable({
                "iDisplayLength": 10,
                "bAutoWidth": true,
                "bFilter": true,
                "aaSorting": [[0, "asc"]],
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1] }]
            });
            $('#searchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
         <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("RelationshipSuccessMessage")))
              { %>
            <div class="alert alert-success">
                <%=Html.CelloValidationMessage("RelationshipSuccessMessage")%>
            </div>
            <%} %>
            <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("RelationshipErrorMessage")))
               { %>
            <div class="alert alert-danger">
                <%=Html.CelloValidationMessage("RelationshipErrorMessage")%>
            </div>
            <%} %>   
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input id="searchText" name="searchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 m-b-xs pull-right">
                        <div class="btn-group btn-group-sm pull-right">
                            <%=Html.CelloCreateButton(Url.Action("AddRelationshipMetaData"), ("<i class=\"fa fa-plus\"></i> "+this.GetGlobalResourceObject("General","Add")), null, null, "AddRelationshipMetaData") %>
                        </div>
                    </div>
                </div>
            </div>      
                           
            <div class="grid-body">
                <%   if (ViewData["RelationshipMetaDataList"] != null && ((IEnumerable<CelloSaaS.Model.Configuration.RelationshipMetaData>)ViewData["RelationshipMetaDataList"]).Count() > 0)
                     {
                %>
                <%
                         string strDeactivateImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/remove_but.gif");
                         string strActivateImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/add_plan_icon.gif");

                         Html.Grid((IEnumerable<CelloSaaS.Model.Configuration.RelationshipMetaData>)ViewData["RelationshipMetaDataList"]).Columns(
                          column =>
                          {
                              column.For(col => col.SourceName).Named(this.GetLocalResourceObject("ChildName").ToString());
                              column.For(col => col.TargetName).Named(this.GetLocalResourceObject("ParentName").ToString());
                              column.For(col => string.IsNullOrEmpty(col.TargetKeyName) ? "  -  " : col.TargetKeyName).Named(this.GetLocalResourceObject("ParentKeyName").ToString());
                              column.For(col => string.IsNullOrEmpty(col.TargetDisplayName) ? "  -  " : col.TargetDisplayName).Named(this.GetLocalResourceObject("ParentDisplayName").ToString());
                              column.For(col => col.TargetType).Named(this.GetLocalResourceObject("ParentType").ToString());
                              //column.For(col => Html.CelloActionLink("Edit", "EditRelationshipMetaData", new { relationshipId = col.Id }, new { title = "Edit relationship details" }).Replace("Edit", "<img src='" + strEditImageUrl + "' alt='Edit Relationship details' />")).Named("Edit").DoNotEncode();
                              column.For(col => (col.Status.Equals(true)) ?
                                    (Html.CelloActionLink("~Deactivate~", "DeactivateRelationshipMetaData", new { relationshipId = col.Id, status = false }, new { onclick = "return confirm('" + this.GetLocalResourceObject("DeactivateConfirmation") + "')", title = this.GetLocalResourceObject("Deactivate") })).Replace("~Deactivate~", "<i class='fa fa-times-circle'></i>")
                                    : (Html.CelloActionLink("~Activate~", "DeactivateRelationshipMetaData", new { relationshipId = col.Id, status = true }, new { onclick = "return confirm('" + this.GetLocalResourceObject("ActivateConfirmation") + "')", title = this.GetLocalResourceObject("Activate") })).Replace("~Activate~", "<i class='fa fa-plus-circle'></i>")
                                    ).Named(this.GetLocalResourceObject("Activate/Deactivate").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                          }).Attributes(id => "dataList", @class => "celloTable", cellspacing => "1", cellpading => "0").Render();                                                        
                %>
                <%}
                     else
                     {%>
                <div class="alert alert-info">
                    <span>
                        <%: this.GetLocalResourceObject("NoRecord")%></span>
                </div>
                <%}%>
            </div>
        </div>
    </div>
</asp:Content>
