﻿<%@ Page Title="<%$Resources:h_AccountDashboard%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("h_AccountDashboard") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div id="dashboard-container">
            <div class="row-fluid">
                <% if (UserIdentity.HasPrivilege(PrivilegeConstants.ViewTenant))
                   { %>
                <% Html.RenderAction("MyAccountDetails"); %>
                <% } %>
            </div>
            <br />
            <div class="row-fluid">
                <div class="col-md-5">
                    <% if (UserIdentity.HasPrivilege(PrivilegeConstants.ViewUsageAudit))
                       { %>
                    <div class="row">
                        <section class="panel purple">
                            <header class="panel-heading">
                                <h4><%: this.GetLocalResourceObject("h_UsageDetails") %></h4>
                            </header>
                            <div class="panel-body">
                                <% Html.RenderAction("UsageAlert"); %>
                            </div>
                        </section>
                    </div>
                    <% } %>
                    <% if (UserIdentity.HasPrivilege(PrivilegeConstants.ViewInvoice))
                       { %>
                    <div class="row">
                        <section class="panel red">
                            <header class="panel-heading">
                                <h4><%: this.GetLocalResourceObject("h_Un-PaidBills") %></h4>
                            </header>
                            <div class="panel-body" id="divUnpaidBills">
                                <% Html.RenderAction("UnPaidBills"); %>
                            </div>
                        </section>
                    </div>
                    <% } %>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <% if (UserIdentity.HasPrivilege(PrivilegeConstants.ViewUser))
                           { %>
                        <div class="col-md-6">
                            <div class="grid simple horizontal green">
                                <div class="grid-title no-border">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4><%: this.GetLocalResourceObject("h_OnlineUsers") %></h4>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="actions">
                                                <a href="#" onclick="loadOnlineUsers();" title="<%: this.GetLocalResourceObject("t_RefereshOnlineUsers") %>"><i class="fa fa-refresh"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-body no-border" style="overflow-x: hidden; overflow-y: auto; height: 425px;padding:0px;">
                                    <div id="divOnlineUsers"></div>
                                </div>
                            </div>
                        </div>
                        <% } %>
                        <% if (UserIdentity.HasPrivilege(PrivilegeConstants.ViewEventAudit))
                           { %>
                        <div class="col-md-6 pd-0">
                            <div class="grid simple horizontal red">
                                <div class="grid-title no-border">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4><%: this.GetLocalResourceObject("h_ActivityStream") %></h4>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="actions">
                                                <a href="<%=Url.Action("SearchEventAudit","Events") %>" title="<%: this.GetLocalResourceObject("t_ClickToViewFull") %>"> <%: this.GetLocalResourceObject("lbl_more") %></a>
                                                <a href="#" onclick="loadActivityStream();" title="<%: this.GetLocalResourceObject("t_RefreshActivityStream") %>"><i class="fa fa-refresh"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-body no-border" style="overflow-x: hidden; overflow-y: auto; height: 425px;padding:0px;">
                                    <div id="divActivityStream"></div>
                                </div>
                            </div>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            if ($('#divOnlineUsers').length > 0)
                loadOnlineUsers();

            if ($('#divActivityStream').length > 0)
                loadActivityStream();
        });

        function loadOnlineUsers() {
            $('#divOnlineUsers').load('<%=Url.Action("OnlineUsers","Dashboard")%>');
            setTimeout(loadOnlineUsers, 1 * 60 * 1000); // every 1 minute
        }

        function loadActivityStream() {
            var url = '<%=Url.Action("SearchEventAudit","Events")%>';
            $.post(url, { paginate: false }, function (html) {
                $('#divActivityStream').html(html);
                setTimeout(loadActivityStream, 1 * 60 * 1000); // every 1 minute
            });
        }
    </script>
</asp:Content>
