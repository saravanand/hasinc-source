﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaSApplication.Models.TenantViewModel>" %>

<%@ Import Namespace="CelloSaaSApplication.Models" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<% Html.RenderPartial("StatusMessage"); %>
<% if (Model != null)
   { %>
<%
       var lstUsages = new List<Tuple<string, double, double, double, bool>>();
       var packageDetails = Model.PackageDetails;
       var usageAmounts = Model.UsageAmounts ?? new Dictionary<string, double>();
       var usageLimits = packageDetails != null && packageDetails.PackageUsageDetails != null ? packageDetails.PackageUsageDetails : new Dictionary<string, PackageUsage>();

       if (usageLimits.Count != usageAmounts.Count)
       {
           foreach (var u in usageLimits)
           {
               if (!usageAmounts.ContainsKey(u.Value.UsageCode))
               {
                   usageAmounts.Add(u.Value.UsageCode, 0.0);
               }
           }
       }

       foreach (var usage in usageLimits.OrderBy(x => x.Value.UsageName))
       {
           double maxLimit = usage.Value.MaximumCapacity ?? 0.0;
           double usageAmt = usageAmounts != null && usageAmounts.ContainsKey(usage.Value.UsageCode) ? usageAmounts[usage.Value.UsageCode] : 0.0;
           double rem = (maxLimit - usageAmt);
           bool crossedThreshold = false;

           if (maxLimit > 0.0 & rem <= (maxLimit * (usage.Value.UsageThreshold / 100.0)))
           {
               crossedThreshold = true;
           }

           lstUsages.Add(Tuple.Create(usage.Value.UsageName, rem, usageAmt, maxLimit, crossedThreshold));
       }

       string dtFormat = CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat();
%>
<div class="row" id="accountDashDiv">
    <div class="col-md-4">
        <div class="tiles green">
            <div class="tiles-body">
                <div class="heading">
                    <%:Model.TenantDetails.TenantName %>
                </div>
                <p class="l-item">
                    <%: this.GetLocalResourceObject("m_Membersince") %>  <span class="label label-danger" title="<%: this.GetLocalResourceObject("t_createdOn") %> <%:Model.TenantDetails.CreatedOn.ToUIDateTimeString() %>">
                        <%
       string mstr = string.Empty;
       var mdiff = (DateTime.Now - Model.TenantDetails.CreatedOn);
       int tdays = (int)mdiff.TotalDays;
       if (tdays == 0)
       {
           if (mdiff.TotalMinutes < 60)
           {
               mstr = (int)mdiff.TotalMinutes + this.GetLocalResourceObject("lbl_minutes").ToString();
           }
           else
           {
               mstr = (int)mdiff.TotalHours + this.GetLocalResourceObject("lbl_hours").ToString();
           }
       }
       else if (tdays < DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month))
       {
           mstr = tdays + this.GetLocalResourceObject("lbl_days").ToString();
       }
       else if (tdays < 365)
       {
           mstr = (int)(tdays / 30) + this.GetLocalResourceObject("lbl_months").ToString();
       }
       else
       {
           mstr = (int)(tdays / 365) + this.GetLocalResourceObject("lbl_years").ToString();
       }
                        %>
                        <%:mstr %>
                    </span>
                </p>
                <p>
                    <i class="fa fa-link" title="Tenant Login URL"></i>&nbsp;
                    <%:Model.TenantDetails.URL ?? this.GetLocalResourceObject("e_NotAvailable").ToString() %>
                </p>
                <p>
                    <i class="fa fa-globe" title="Tenant website"></i>&nbsp;
                    <%:string.IsNullOrEmpty(Model.TenantDetails.Website) ? "NA" : Model.TenantDetails.Website %>
                </p>
                <p>
                    <i class="fa fa-envelope" title="Contact email"></i>&nbsp;
                    <%:Model.TenantContactDetail != null && !string.IsNullOrEmpty(Model.TenantContactDetail.Email) ? Model.TenantContactDetail.Email : this.GetLocalResourceObject("e_NotAvailable").ToString()%>
                </p>
                <p>
                    <i class="fa fa-phone" title="Contact phone"></i>&nbsp;
                    <%:Model.TenantContactDetail != null && !string.IsNullOrEmpty(Model.TenantContactDetail.Phone) ? Model.TenantContactDetail.Phone : this.GetLocalResourceObject("e_NotAvailable").ToString()%>
                </p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="tiles blue">
            <div class="tiles-body">
                <div class="actions">
                    <% if (lstUsages.Count > 0)
                       { %>
                    <div style="text-align: right;">
                        <a href="#tenant<%:Model.TenantDetails.TenantCode%>" data-tenantid="<%:Model.TenantDetails.TenantCode%>"
                            id="btnViewUsages<%:Model.TenantDetails.TenantCode%>"
                            title="<%: this.GetLocalResourceObject("t_UsageValues") %> <div style='position: absolute;top: 9px;right: 10px;'><a data-target='#btnViewUsages<%:Model.TenantDetails.TenantCode%>' href='#tenant<%:Model.TenantDetails.TenantCode%>' class='btnPopoverClose'><i class='fa fa-times-circle'></i></a></div>">
                            <span><i class="fa fa-search"></i></span>
                        </a>
                        <% if (lstUsages.Any(x => x.Item5))
                           { %>
                        <div class="usage-alert-div" style="display: inline-block;">
                            <a class="text-danger important" id="btnUsageAlertInfo<%:Model.TenantDetails.TenantCode%>" href="#" title=" <%: this.GetLocalResourceObject("m_UsegeLimit") %>">
                                <i class="fa fa-info-circle"></i>
                            </a>
                        </div>
                        <% } %>
                        <div id="divUsageLimitInfo<%:Model.TenantDetails.TenantCode%>" style="display: none;">
                            <div style="width: 330px;">
                                <table style="width: 100%;">
                                    <tbody>
                                        <% foreach (var usage in lstUsages)
                                           {
                                               int maxPer = (int)(((usage.Item4 - usage.Item3) / usage.Item4) * 100);
                                               int usedPer = 100 - maxPer;
                                        %>
                                        <tr>
                                            <td style="vertical-align: top; width: 30%;">
                                                <span class="<%:usage.Item5 ? "red" : "" %>">
                                                    <%:usage.Item1 %></span>
                                            </td>
                                            <td class="text-right">
                                                <% if (usage.Item4 == 0.0)
                                                   { %>
                                                <span><%:usage.Item3 %></span>
                                                <% }
                                                   else
                                                   { %>
                                                <div class="pull-right progress progress-sm progress-striped active" title="<%: this.GetLocalResourceObject("m_Maxlimit") %> <%:usage.Item4 %> <%: this.GetLocalResourceObject("m_used") %> <%:usage.Item3 %>">
                                                    <div class="progress-bar progress-bar-danger" style="width: <%:usedPer %>%" title="Max limit: <%:usage.Item4 %>, Used: <%:usage.Item3 %>" data-toggle="tooltip"></div>
                                                </div>
                                                <% } %>
                                            </td>
                                        </tr>
                                        <% } %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
                <div class="tiles-title">
                    <%:Model.License.PackageName %> <%=Model.Plan != null ? " - " + Model.Plan.Name: string.Empty %>
                </div>
                <div class="heading" title="<%:Model.Plan!=null ? Model.Plan.BillFrequency.ToString() : "" %>">
                    <%:Model.Plan!=null 
                    ? (Model.Plan.Price %1 == 0) ? Model.Plan.Price.ToBillingCurrency("C0") : Model.Plan.Price.ToBillingCurrency("C2")
                        : (0.0).ToBillingCurrency("C0") %>
                </div>
                <div class="row">
                    <div class="col-md-6" title="<%: this.GetLocalResourceObject("t_ValidityStartDate") %> "><%:Model.License.ValidityStart.ToString(dtFormat) %></div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <% if (Model.License.TrialEndDate.HasValue && Model.License.TrialEndDate.Value >= DateTime.Now)
                               { %>
                            <span title="<%: this.GetLocalResourceObject("t_trialEndOn") %> <%:Model.License.TrialEndDate.Value.ToUIDateTimeString() %>"><%:Model.License.TrialEndDate.Value.ToString(dtFormat) %></span>
                            <% }
                               else if (Model.License.ValidityEnd.HasValue && Model.License.ValidityEnd.Value >= DateTime.Now)
                               { %>
                            <span title="<%: this.GetLocalResourceObject("t_SubscriptionEndOn") %> <%:Model.License.ValidityEnd.Value.ToUIDateTimeString() %>"><%:Model.License.ValidityEnd.Value.ToString(dtFormat) %></span>
                            <% }
                               else if (Model.License.ValidityEnd.HasValue)
                               { %>
                            <span title="<%: this.GetLocalResourceObject("t_SubscriptionEndedOn") %> <%:Model.License.ValidityEnd.Value.ToUIDateTimeString() %>"><%:Model.License.ValidityEnd.Value.ToString(dtFormat) %></span>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-6">
                <div class="tiles purple">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_TotalUsers") %>
                        </div>
                        <div class="heading">
                            <a href="<%=Url.Action("UserList", "User")%>" title="<%: this.GetLocalResourceObject("t_totalActiveUsers") %>">
                                <%:Model.TotalUsers %>
                            </a>/ 
                            <%:Model.OnlineUsers %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <% if (UserIdentity.HasPrivilege(PrivilegeConstants.AddTenant))
                   { %>
                <div class="tiles lt-blue">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_ChildTenants") %>
                        </div>
                        <div class="heading">
                            <a href="<%=Url.Action("Index", "Tenant")%>">
                                <%:Model.TotalChildTenants %>
                            </a>
                        </div>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
        <% if (Model.BillStatistics != null)
           { %>
        <br />
        <div class="row">
            <div class="col-md-6">
                <div class="tiles lt-green">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("t_BillAmount") %>
                        </div>
                        <div class="heading" title="<%:Model.BillStatistics.TotalAmount.ToBillingCurrency() %>">
                            <a href="<%=Url.Action("MyBills", "Billing")%>">
                                <%: (Model.BillStatistics.TotalAmount %1 ==0) ?Model.BillStatistics.TotalAmount.ToBillingCurrency("C0"):Model.BillStatistics.TotalAmount.ToBillingCurrency("C2")  %>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="tiles red">
                    <div class="tiles-body">
                        <div class="tiles-title captilize" title="<%: this.GetLocalResourceObject("t_TotalOverdue") %>">
                            <%: this.GetLocalResourceObject("lbl_DueAmount") %>
                        </div>
                        <div class="heading" title="<%:Model.OverallOverdue.ToBillingCurrency() %>">
                            <a href="<%=Url.Action("MyBills", "Billing")%>">
                                <%: (Model.OverallOverdue %1 ==0) ? Model.OverallOverdue.ToBillingCurrency("C0") : Model.OverallOverdue.ToBillingCurrency("C2") %>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('a[id^=btnViewUsages]').popover({
            placement: 'right',
            trigger: 'click',
            html: true,
            content: function () {
                return $('#divUsageLimitInfo' + $(this).data('tenantid')).html();
            }
        });

        $(document).on('click', 'a.btnPopoverClose', function () {
            $($(this).data('target')).popover('hide');
            return false;
        });
    });
</script>
<% } %>