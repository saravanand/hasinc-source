﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<% Html.RenderPartial("StatusMessage"); %>

<%
    var users = ViewData["OnlineUsers"] as Dictionary<string, UserDetails> ?? new Dictionary<string, UserDetails>();
%>
<% if (users.Count > 0)
   { %>
<ul class="list-group alt">
    <% foreach (var user in users)
       {
           string msg =this.GetLocalResourceObject("t_JustNow").ToString();
           string css = "text-success";
           string title =this.GetLocalResourceObject("t_Active").ToString();

           var diff = DateTime.Now - user.Value.MembershipDetails.LastActivityDate;

           if (diff.TotalMinutes > 1 && diff.TotalMinutes < 10)
           {
               msg = string.Format(this.GetLocalResourceObject("m_MinutesAgo").ToString(), (int)diff.TotalMinutes);
           }
           else if (diff.TotalMinutes >= 10)
           {
               title = this.GetLocalResourceObject("t_Inactive").ToString();
               css = "text-warning";
               msg = string.Format(this.GetLocalResourceObject("m_MinutesAgo").ToString(), (int)diff.TotalMinutes);
           }
           else if (diff.TotalMinutes >= 10)
           {
               title = this.GetLocalResourceObject("t_Away").ToString();
               css = "text-muted";
               msg = string.Format(this.GetLocalResourceObject("m_MinutesAgo").ToString(), (int)diff.TotalMinutes);
           }
    %>
    <li class="list-group-item">
        <div class="media">
            <span class="pull-left thumb-sm"></span>
            <div class="pull-right m-t-sm <%:css %>" title="<%:title %>">
                <i class="fa fa-circle"></i>
            </div>
            <div class="media-body">
                <div>
                    <%=user.Value.MembershipDetails.UserName %>
                </div>
                <small class="text-muted" title="<%:user.Value.MembershipDetails.LastActivityDate.ToUIDateTimeString() %>"><%:msg %></small>
            </div>
        </div>
    </li>
    <% } %>
</ul>
<% }
   else
   { %>
<div class="text-warning pd-10">
    <%: this.GetLocalResourceObject("e_NoUsers") %>
</div>
<% } %>
