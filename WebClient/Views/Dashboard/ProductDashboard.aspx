﻿<%@ Page Title="<%$ Resources:h_ProductDashboard %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("h_ProductDashboard") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row">
            <div class="col-md-8">
                <div id="tenantDashDiv">
                    <% Html.RenderAction("TenantDashboard", "Tenant"); %>
                </div>
                <div id="totalRevenuesDiv">
                    <% Html.RenderAction("TotalRevenues", "Billing", new { hideChart = "True" }); %>
                </div>
                <div id="topRevenuesTenantsDiv">
                    <% Html.RenderAction("TopRevenueTenants", "Billing", new { hideTable = "True" }); %>
                </div>
                <div id="product_usage_chart_div">
                    <% Html.RenderPartial("~/Views/Audit/ProductUsageChart.ascx"); %>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid simple horizontal red">
                    <div class="grid-title no-border">
                        <div class="row">
                            <div class="col-md-6">
                                <h4><%: this.GetLocalResourceObject("h_ActivityStream") %></h4>
                            </div>
                            <div class="col-md-6">
                                <div class="actions">
                                    <a href="<%=Url.Action("SearchEventAudit","Events") %>" title="<%: this.GetLocalResourceObject("t_ClickView") %>"><%: this.GetLocalResourceObject("lbl_more") %> </a>
                                    <a href="#" onclick="loadActivityStream();" title="<%: this.GetLocalResourceObject("t_RefereshActivity") %>"><i class="fa fa-refresh"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-body no-border" style="overflow-x: hidden; overflow-y: auto; height: 700px; padding: 0px;">
                        <div id="divActivityStream"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            if ($('#divActivityStream').length > 0)
                loadActivityStream();
        });

        function loadActivityStream() {
            var url = '<%=Url.Action("SearchEventAudit","Events")%>';
            $.post(url, { paginate: false, pageSize: 30 }, function (html) {
                $('#divActivityStream').html(html);
                setTimeout(loadActivityStream, 1 * 60 * 1000); // every 1 minute
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
</asp:Content>
