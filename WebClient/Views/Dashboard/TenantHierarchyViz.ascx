﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%
    var tenantInfo = CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantInfo(UserIdentity.TenantID);
    var data = ViewBag.hierarchyData as CelloSaaSApplication.Models.TenantHierarchyViewModel;
    string _data = Newtonsoft.Json.JsonConvert.SerializeObject(data);

    if (data.children == null || data.children.Count == 0)
    {
%>
<div class="alert alert-info"><%: this.GetLocalResourceObject("i_NoTenants") %></div>
<% }
    else
    { %>
<%=Html.Hidden("tenantHirVizData",_data) %>
<div>
    <% if (tenantInfo.Types != null)
       { %>
    <div class="ledgend" style="padding: 3px; margin-bottom: 5px;">
        <label><%: this.GetLocalResourceObject("lbl_TenantType") %></label>
        <% if (tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.ISV))
           { %>
        <div class="ledgend-box" style="background-color: lightsalmon"></div>
        <%: this.GetLocalResourceObject("lbl_Reseller") %> 
        <% } %>

        <% if (tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller))
           { %>
        <div class="ledgend-box" style="background-color: magenta"></div>
         <%: this.GetLocalResourceObject("lbl_Enterprise") %>
        <div class="ledgend-box" style="background-color: mediumpurple"></div>
         <%: this.GetLocalResourceObject("lbl_SMB") %>
        <% }
           else if (tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.Enterprise))
           { %>
        <div class="ledgend-box" style="background-color: magenta"></div>
       <%: this.GetLocalResourceObject("lbl_Enterprise") %>
        <% }
           else if (tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.SMB))
           { %>
        <div class="ledgend-box" style="background-color: mediumpurple"></div>
       <%: this.GetLocalResourceObject("lbl_SMB") %>
        <% } %>
        <div class="ledgend-box" style="background-color: #3182bd"></div>
        <%: this.GetLocalResourceObject("lbl_OrganizationalUnit") %>
        <div class="clearfix"></div>
    </div>
    <% } %>
    <svg></svg>
</div>
<script src="/Scripts/d3.v3.min.js"></script>
<script>
    var margin = { top: -5, right: -5, bottom: -5, left: -5 },
    w = 1100 - margin.left - margin.right,
    h = 800 - margin.top - margin.bottom;
    var r = 4.5;
    var diameter = 1150;

    var zoom = d3.behavior.zoom()
        .center([w/2, h/2])
        .scaleExtent([0.5, 5])
        .on("zoom", zoomed);

    var vis = d3.select("svg")
        .attr("width", diameter)
        .attr("height", diameter - 150)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.right + ")")
        .call(zoom);

    var rect = vis.append("rect")
                .attr("transform", "translate(" + margin.left + "," + margin.right + ")")
                .attr("width", w)
                .attr("height", h)
                .style("fill", "white")
                .style("pointer-events", "all");

    var svg = vis.append("g")
                 .attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

    var tree = d3.layout.tree()
                    .size([360, diameter / 2 - 120])
                    .separation(function (a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

    var diagonal = d3.svg.diagonal.radial()
                    .projection(function (d) { return [d.y, d.x / 180 * Math.PI]; });
    
    var json = JSON.parse($('#tenantHirVizData').val());

    var i = 0;
    var node, links, link, circle, text, path, marker;
    var root = json;
    root.fixed = false;

    updateForce();

    d3.select(self.frameElement).style("height", diameter - 150 + "px");

    /*force graph*/
    function updateForce() {
        var nodes = tree.nodes(root);
        links = tree.links(nodes);

        link = svg.selectAll(".link")
                .data(links)

        link.enter().append("path")
            .attr("class", "link")
            .attr("d", diagonal);

        link.exit().remove();

        // Update the nodes…
        node = svg.selectAll("g.node")
            .data(nodes, function (d) { return d.id || (d.id = ++i); });

        var nodeEnter = node.enter().append("svg:g")
            .attr("class", "node")
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .attr("transform", function (d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })
        
        // Enter any new nodes.
        circle = nodeEnter.append("svg:circle")
            .attr("r", function (d) { return r; })
            .attr("class", function (d) { return d.type + " " + (d.children ? "haschild" : ""); });

        text = nodeEnter.append("svg:text")
        	.attr("fill", "black")
  		    .style("pointer-events", "none")
            .attr("dy", ".31em")
            .attr("text-anchor", function (d) { return d.x < 180 ? "start" : "end"; })
            .attr("transform", function (d) { return d.x < 180 ? "translate(8)" : "rotate(180)translate(-8)"; })
            .text(function (d) { return d.name; });

        // Exit any old nodes.
        node.exit().remove();
    }

    function mouseover() {
        d3.select(this).select("circle").transition()
            .duration(750)
            .attr("r", function (d) { return (r * 2); });

        d3.select(this).select("text").transition()
            .duration(350)
            .style("font-size", "13px")
            .style("font-weight", "bold");
    }

    function mouseout() {
        d3.select(this).select("circle").transition()
            .duration(350)
            .attr("r", function (d) { return r; });

        d3.select(this).select("text").transition()
            .duration(350)
            .style("font-size", "10px")
            .style("font-weight", "normal");
    }

    function zoomed() {
        //svg.attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")scale(" + d3.event.scale + ")");
        svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }    
</script>
<style type="text/css">
    .link {
        stroke: #999;
        stroke-opacity: .6;
    }

    path.link {
        fill: none;
        stroke: #666;
        stroke-width: 1.5px;
    }

    marker#licensing {
        fill: red;
    }

    path.link.SMB {
        stroke: lightblue;
    }

    path.link.Enterprise {
        stroke: hotpink;
    }

    path.link.Reseller {
        stroke: blue;
    }

    path.link.OrgUnit {
        stroke: darksalmon;
    }

    path.link.ProductAdmin,
    path.link.ISV {
        stroke-dasharray: 0,2 1;
    }

    circle {
        stroke: #ccc;
        stroke-width: 1.0px;
        cursor: pointer;
    }

        circle.SMB {
            fill: mediumpurple;
        }

        circle.Enterprise {
            fill: magenta;
        }

        circle.OrgUnit {
            fill: #3182bd;
        }

        circle.Reseller {
            fill: lightsalmon;
        }

        circle.ProductAdmin, circle.ISV {
            fill: lightcoral;
        }

        circle.haschild {
            stroke: #3182bd;
            stroke-width: 1.5px;
        }

    text {
        font: 10px sans-serif;
        pointer-events: none;
        text-align: center;
    }

        text.shadow {
            stroke: #fff;
            stroke-width: 1px;
            stroke-opacity: .8;
        }

    .ledgend-box {
        width: 12px;
        height: 12px;
        border: 1px solid #eee;
        display: inline-block;
        margin-left: 20px;
    }
</style>
<% } %>