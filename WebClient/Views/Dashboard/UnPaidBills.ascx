﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    var invoiceResult = ViewData["UnPaidBills"] as InvoiceSearchResult ?? new InvoiceSearchResult();
    var invoiceList = invoiceResult.Items;
%>
<% 
    if (invoiceList != null && invoiceList.Count > 0)
    {
        Html.Grid<Invoice>(invoiceList.Values).Columns(column =>
            {
                column.For(x => string.Format("<a href='{1}' title='Click to download this invoice!'>{0}</a>", x.InvoiceNo, Url.Action("DownloadInvoice", "Billing", new { invoiceId = x.Id, tenantId = x.TenantId })))
                    .Named(this.GetLocalResourceObject("lbl_Invoice").ToString()).DoNotEncode();
                column.For(x => string.Format("{0} - {1}", x.StartDate.ToUIDateString(), x.EndDate.ToUIDateString()))
                    .Named(this.GetLocalResourceObject("lbl_BillPeriod").ToString());
                column.For(x => x.InvoiceDate.ToUIDateString()).Named(this.GetLocalResourceObject("lbl_InvoiceDate").ToString());
                column.For(x => x.Amount.ToBillingCurrency("C")).Named(this.GetLocalResourceObject("lbl_Amount").ToString())
                    .Attributes(@class => "tright").HeaderAttributes(@class => "tright");
            }).Attributes(@class => "table table-condensed", id => "tblUnpaidBills").Render();
%>
<script type="text/javascript">
    $(function () {
        $('#tblUnpaidBills').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": false,
            "bInfo": true
        });
    });
</script>
<%
    }
    else
    { %>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("e_UnPaidBills") %>
</div>
<% } %>