﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>
<% if (Html.ValidationMessage("Error") != null)
   { %>
<div class="alert alert-danger"><%:Html.ValidationMessage("Error") %></div>
<% }
   else
   { %>
<%
       bool showIcon = (ViewData["ShowAlertIcon"] != null && (bool)ViewData["ShowAlertIcon"]);
       var lstUsages = new List<Tuple<string, double, double, double, double, bool>>();
       var thresholdUsages = new List<Tuple<string, double, double, double>>();
       var packageDetails = ViewData["PackageDetails"] as PackageDetails;
       var usageAmounts = ViewData["UsageAmounts"] as Dictionary<string, double> ?? new Dictionary<string, double>();
       var childUsageAmounts = ViewData["ChildUsageAmounts"] as Dictionary<string, Dictionary<string, double>> ?? new Dictionary<string, Dictionary<string, double>>();
       var totalChildUsageAmounts = new Dictionary<string, double>();
       var usageLimits = packageDetails != null && packageDetails.PackageUsageDetails != null ? packageDetails.PackageUsageDetails : new Dictionary<string, PackageUsage>();

       if (usageLimits.Count != usageAmounts.Count)
       {
           foreach (var u in usageLimits)
           {
               if (!usageAmounts.ContainsKey(u.Value.UsageCode))
               {
                   usageAmounts.Add(u.Value.UsageCode, 0.0);
               }
           }
       }

       foreach (var child in childUsageAmounts)
       {
           foreach (var u in child.Value)
           {
               if (totalChildUsageAmounts.ContainsKey(u.Key))
               {
                   totalChildUsageAmounts[u.Key] += u.Value;
               }
               else
               {
                   totalChildUsageAmounts.Add(u.Key, u.Value);
               }
           }
       }

       foreach (var usage in usageLimits.OrderBy(x => x.Value.UsageName))
       {
           double maxLimit = usage.Value.MaximumCapacity ?? 0.0;
           double usageAmt = usageAmounts.ContainsKey(usage.Value.UsageCode) ? usageAmounts[usage.Value.UsageCode] : 0.0;
           double childAmt = totalChildUsageAmounts.ContainsKey(usage.Value.UsageCode) ? totalChildUsageAmounts[usage.Value.UsageCode] : 0.0;
           double tenantRem = (maxLimit - usageAmt);
           double rem = (maxLimit - (usageAmt + childAmt));
           bool crossedThreshold = false;

           if (maxLimit > 0.0 & rem <= (maxLimit * (usage.Value.UsageThreshold / 100.0)))
           {
               thresholdUsages.Add(Tuple.Create(usage.Value.UsageName, rem, (usageAmt + childAmt), maxLimit));
               crossedThreshold = true;
           }

           lstUsages.Add(Tuple.Create(usage.Value.UsageName, rem, usageAmt, maxLimit, childAmt, crossedThreshold));
       }
%>
<% if (!showIcon && lstUsages.Count > 0)
   {
%>
<div class="grid-part">
    <table class="table table-condensed" id="tblUsageAmounts">
        <thead>
            <tr>
                <th><%: this.GetLocalResourceObject("lbl_UsageName") %>
                </th>
                <th class="tright"><%: this.GetLocalResourceObject("lbl_MaxLimit") %>
                </th>
                <th class="tright"><%: this.GetLocalResourceObject("lbl_Used") %>
                </th>
                <% if (childUsageAmounts.Count > 0)
                   { %>
                <th class="tright"><%: this.GetLocalResourceObject("lbl_ChildUsage") %></th>
                <% } %>
                <th class="tright"><%: this.GetLocalResourceObject("lbl_Remaining") %>
                </th>
            </tr>
        </thead>
        <tbody>
            <%  foreach (var usage in lstUsages)
                {
            %>
            <tr class="<%: usage.Item6 ? "danger text-danger" : string.Empty%>" title="<%: usage.Item6 ? this.GetLocalResourceObject("lbl_Reached").ToString() : ""%>">
                <td>
                    <%:usage.Item1%>
                </td>
                <td class="tright">
                    <%: usage.Item4 == 0.0 ? this.GetLocalResourceObject("lbl_UNLIMITED").ToString() : usage.Item4.ToString()%>
                </td>
                <td class="tright">
                    <%:usage.Item3%>
                </td>
                <% if (childUsageAmounts.Count > 0)
                   { %>
                <td class="tright">
                    <%:usage.Item5%>
                </td>
                <% } %>
                <td class="tright">
                    <%: usage.Item4 == 0.0 ? this.GetLocalResourceObject("lbl_UNLIMITED").ToString() : usage.Item2.ToString()%>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
</div>
<% }
   else if (!showIcon)
   { %>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("e_UsageNotAvailable") %>
</div>
<% } %>
<% if (thresholdUsages.Count > 0 && showIcon)
   { %>
<a class="dropdown-toggle animated bounceIn" data-toggle="popover" data-html="true" data-placement="bottom" role="button" id="btnUsageAlertInfo" href="#divUsageLimitInfo" title="<%: this.GetLocalResourceObject("t_UsageLimit") %>">
    <i class="fa fa-exclamation-circle text-danger"></i>
</a>
<div id="divUsageLimitInfo" class="fade hide">
    <div class="row">
        <div class="col-md-12">
            <table style="width: 250px;">
                <tbody>
                    <% foreach (var usage in thresholdUsages)
                       {
                    %>
                    <tr>
                        <td style="vertical-align: top; width: 30%;"><%:usage.Item1 %></td>
                        <td class="text-right">
                            <% if (usage.Item4 == 0.0)
                               { %>
                            <span><%:usage.Item3 %></span>
                            <% }
                               else
                               {
                                   int maxPer = (int)((usage.Item2 / usage.Item4) * 100);
                                   int usedPer = 100 - maxPer;
                            %>
                            <div class="progress progress-sm progress-striped active">
                                <div class="progress-bar progress-bar-danger" style="width: <%:usedPer %>%" title="<%: this.GetLocalResourceObject("t_MaxLimit") %> <%:usage.Item4 %>, <%: this.GetLocalResourceObject("t_Used") %> <%:usage.Item3 %>" data-toggle="tooltip"></div>
                            </div>
                            <% } %>
                        </td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
            <% if (ViewData["ShowUpgradeButton"] != null && (bool)ViewData["ShowUpgradeButton"])
               { %>
            <div class="pull-right">
                <a class="btn btn-info" href="<%=Url.Action("ManageSettings","MySettings") %>" title="<%: this.GetLocalResourceObject("t_UpgradePlan") %>"><%: this.GetLocalResourceObject("lbl_Upgrade") %></a>
            </div>
            <% } %>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#btnUsageAlertInfo').popover({
            placement: 'bottom',
            trigger: 'click',
            html: true,
            content: function () {
                return $('#divUsageLimitInfo').html();
            }
        });
    });
</script>
<% } %>
<% } %>