﻿<%@ Page Title="<%$Resources:ManageEntity%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%: Url.Action("EntityList","Data")%>" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: this.GetLocalResourceObject("AddVirtualEntity")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           {   %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>
        <% using (Html.BeginForm("AddVirtualEntity", "Data", FormMethod.Post, new { @id = "AddVirtualEntityForm" }))
           {

               bool canAdd = false;
               IAccessControlService accessControlService = (IAccessControlService)ServiceLocator.GetServiceImplementation(typeof(IAccessControlService));
               if (accessControlService.CheckPrivilege(EntityPrivilegePrefix.AddPrefix + "VirtualEntity"))
               {
                   canAdd = true;
               }
        %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("AddVirtualEntity")%></h4>
            </header>
            <div class="panel-body">
                <div class="form-container">
                    <div class="form-group">
                        <label class="mandatory">
                            <%: this.GetLocalResourceObject("EntityName")%>
                        </label>
                        <%= Html.TextBox("EntityName", ViewData["EntityName"], new { maxlength = 50 })%>
                    </div>
                    <div class="form-group">
                        <label class="mandatory">
                            <%: this.GetLocalResourceObject("Feature")%>
                        </label>
                        <% if (ViewData["FeatureList"] != null)
                           { %>
                        <%= Html.DropDownList("Features", (List<SelectListItem>)ViewData["FeatureList"], new { style = "width:100%" })%>
                        <% } %>
                    </div>
                    <div class="form-group  pull-right">
                        <% if (canAdd)
                           { %>
                        <button type="submit" class="btn btn-success" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                            <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Save")%></button>
                        <% }%>
                        <a class="btn btn-default" href="<%: Url.Action("EntityList","Data")%>" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                            <%=this.GetGlobalResourceObject("General", "Cancel")%>
                        </a>
                    </div>
                </div>
            </div>
            <% } %>
        </section>
    </div>
    <script type="text/javascript">
        $(function () {
            $('select#Features').select2();
        });
    </script>
</asp:Content>
