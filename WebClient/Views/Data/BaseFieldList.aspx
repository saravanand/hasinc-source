<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;
        $(document).ready(function () {
            filterTable = $('#dataList').dataTable({
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "bFilter": true,
                "aaSorting": [[0, "asc"]],
                "bRetrieve": true,
                "bStateSave": true
            });
            $(document).on('keypress', '#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%:Url.Action("EntityList","Data") %>" title="<%:this.GetLocalResourceObject("BackTitle") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <span class="semi-bold"><%= ViewData["EntityName"] %></span> <%:this.GetLocalResourceObject("Title") %>
        </h3>
    </div>
    <% var baseFieldList = ViewData["DicEntityFieldMetaData"] as IEnumerable<EntityFieldMetaData>; %>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <% if (baseFieldList != null && baseFieldList.Count() > 0)
               {
            %>
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <%}%>
        </div>
        <div class="grid-body">
            <%  if (baseFieldList != null && baseFieldList.Count() > 0)
                {
            %>
            <% Html.Grid(baseFieldList).Columns(
                              column =>
                              {
                                  column.For(col => col.EntityFieldIdentifier).Named(this.GetLocalResourceObject("FieldIdentifier").ToString());
                                  column.For(col => col.Name).Named(this.GetLocalResourceObject("FieldName").ToString());
                              }).Attributes(id => "dataList", @class => "celloTable").Render(); 
            %>
            <%
                }
                else
                {%>
            <div class="alert alert-info">
                <%:this.GetLocalResourceObject("NoRecords") %>
            </div>
            <%}%>
        </div>
    </div>
</asp:Content>
