<%@ Page Title="<%$Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.DataManagement.EntityMetaData>" %>

<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(document).ready(function () {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }
            filterTable = $('#dataList').dataTable({
                "iDisplayLength": 10,
                "bFilter": true,
                "bAutoWidth": false,
                "aaSorting": [[0, "asc"]],
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
            $('#searchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("Title") %></h3>
        <div class="pull-right">
            <% 
                if (ViewData["Source"].ToString() == ConfigurationSettingValueConstant.OWN_SETTING
                   && !ViewData["TenantId"].ToString().Equals(CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (ViewData["HasExtendedFields"].ToString() == "false")
                    {
            %>
            <a class="btn btn-warning" href="<%=Url.Action("CopyAllParentExtendedFields") %>" title="<%:this.GetLocalResourceObject("CopyExtnFields") %>">
                <%:this.GetLocalResourceObject("CopyExtnFields")%></a>
            <% } %>
            <% } %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (TempData["Success"] != null)
           { %>
        <div class="alert alert-success">
            <%:TempData["Success"] %>
        </div>
        <% } %>
        <% if (TempData["Error"] != null)
           { %>
        <div class="alert alert-danger">
            <%:TempData["Error"] %>
        </div>
        <% } %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("StatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <% } %>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-6 m-b-xs pull-right">
                        <div class="pull-right">
                            <%  if (UserIdentity.HasPrivilege(PrivilegeConstants.AddVirtualEntity))
                                { %>
                            <a class="btn btn-success" id="AddVirtualEntityButton" href="<%= Url.Action("AddVirtualEntity","Data") %>" title="<%=this.GetGlobalResourceObject("General","Add")%>">
                                <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Create") %></a>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <div id="EntityDetails">
                    <% if (ViewData["EntityMetaData"] != null && ((IEnumerable<EntityMetaData>)ViewData["EntityMetaData"]).Count() > 0)
                       {
                    %>
                    <%     var EntityValidationRules = ViewBag.EntityValidationRules as Dictionary<string, bool>;
                           var EntityPreprocessRules = ViewBag.EntityPreprocessRules as Dictionary<string, bool>;

                           Html.Grid((List<EntityMetaData>)ViewData["EntityMetaData"]).Columns(
                              column =>
                              {
                                  column.For(col => col.EntityName).Named(this.GetLocalResourceObject("EntityName").ToString());
                                  column.For(col => Html.CelloActionLink("Manage", "FieldList", new { entityId = col.EntityIdentifier, name = col.EntityName }).Replace("Manage", "<i class='fa fa-wrench'></i>"))
                                      .Named(this.GetLocalResourceObject("ManageExtnFields").ToString())
                                      .HeaderAttributes(@class => "halign noSortCol")
                                      .Attributes(@class => "halign")
                                      .DoNotEncode();
                                  column.For(col => Html.CelloActionLink("View", "BaseFieldList", new { entityId = col.EntityIdentifier }).Replace("View", "<i class='fa fa-eye'></i>"))
                                      .Named(this.GetLocalResourceObject("ViewBaseFields").ToString())
                                      .HeaderAttributes(@class => "halign noSortCol")
                                      .Attributes(@class => "halign")
                                      .DoNotEncode();

                                  /*column.For(col => !(EntityValidationRules != null && EntityValidationRules.ContainsKey(col.EntityIdentifier) && EntityValidationRules[col.EntityIdentifier]) ?
                                                                "---" : Html.CelloActionLink("Edit", "ManageEntityRule", new { entityIdentifier = col.EntityIdentifier, ruleType = "Validation" }).Replace("Edit", "<i class='fa fa-pencil'></i>"))
                                                                .Named(this.GetLocalResourceObject("ValidationRule").ToString())
                                                                .HeaderAttributes(@class => "halign noSortCol")
                                                                .Attributes(@class => "halign")
                                                                .DoNotEncode();
                                  column.For(col => !(EntityPreprocessRules != null && EntityPreprocessRules.ContainsKey(col.EntityIdentifier) && EntityPreprocessRules[col.EntityIdentifier]) ?
                                                                "---" : Html.CelloActionLink("Edit", "ManageEntityRule", new { entityIdentifier = col.EntityIdentifier, ruleType = "Preprocessor" }).Replace("Edit", "<i class='fa fa-pencil'></i>"))
                                                                .Named(this.GetLocalResourceObject("PreProcessorRule").ToString())
                                                                .HeaderAttributes(@class => "halign noSortCol")
                                                                .Attributes(@class => "halign")
                                                                .DoNotEncode();
                                  */
                                  column.For(col => ((string.IsNullOrEmpty(col.TenantId) || !UserIdentity.HasPrivilege(PrivilegeConstants.DeleteVirtualEntity)) ? "-" : Html.CelloActionLink("DeleteIcon", "DeleteVirtualEntity", new { entityId = col.EntityIdentifier, entityName = col.EntityName }, new { onclick = "javascript:return confirm('" + string.Format(this.GetLocalResourceObject("i_Areyousureyouwanttodelete").ToString(), col.EntityName) + "')" }).Replace("DeleteIcon", "<i class='fa fa-trash-o'></i>")))
                                 .Named(this.GetLocalResourceObject("DeleteVirtualEntity").ToString())
                                 .HeaderAttributes(@class => "halign noSortCol")
                                 .Attributes(@class => "halign")
                                 .DoNotEncode();

                              })
                              .Attributes(id => "dataList", @class => "celloTable")
                              .Render(); 
                    %>
                    <%
                       }
                       else
                       {%>
                    <div class="alert alert-info">
                        <%:this.GetLocalResourceObject("NoRecords") %>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
        <div id="divVirtualEntityForm" style="display: none;">
        </div>
    </div>
</asp:Content>
