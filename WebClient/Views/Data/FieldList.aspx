<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;
        $(document).ready(function () {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }
            filterTable = $('table.celloTable').dataTable({
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "bFilter": true,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
            $(document).on('keypress', '#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }

        function AddDisplayColumnName() {
            var postdata = $('form#AddDisplayColumnNameForm').serialize();
            $.post('<%=Url.Action("AddDisplayColumnName") %>', postdata, function (data) {
                if (data.Success) {
                    alert(data.Success);
                }
                else if (data.Error) {
                    alert(data.Error);
                }
            }, 'json');
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%:Url.Action("EntityList","Data") %>" title="<%:this.GetLocalResourceObject("BackTitle") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <span class="semi-bold"><%= ViewData["EntityName"] %></span> - <%=this.GetLocalResourceObject("Title") %></h3>
        <div class="pull-right">
            <% if (ViewData["Source"] != null && ViewData["Source"].ToString() == ConfigurationSettingValueConstant.OWN_SETTING)
               {
                   if (ViewData["CheckExtendedFields"] != null && ViewData["CheckExtendedFields"].ToString() == "false")
                   {
            %>
            <a class="btn btn-warning" href="CopyParentExtendedFields?entityId=<%= Request.QueryString["entityId"] %>"
                title="<%:this.GetLocalResourceObject("CopyTitle") %>"><span>
                    <%:this.GetLocalResourceObject("Copy") %></span></a>
            <% }
               } %>
            <a class="btn btn-success" href="<%:Url.Action("ManageField", "Data", new { entityId=Request.QueryString["entityId"] })%>" title="<%:this.GetLocalResourceObject("AddTitle") %>">
                <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Add") %></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("SuccessMessage")))
          { %>
        <div class="alert alert-success">
            <%=Html.CelloValidationMessage("SuccessMessage")%>
        </div>
        <%} %>
        <% if (TempData["SuccessMessage"] != null)
           { %>
        <div class="alert alert-success">
            <%:TempData["SuccessMessage"]%>
        </div>
        <% } %>
        <% if (TempData["Error"] != null)
           { %>
        <div class="alert alert-danger">
            <%:TempData["Error"] %>
        </div>
        <% } %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("StatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <% } %>
        <% using (Html.BeginForm("AddDisplayColumnName", "Data", FormMethod.Post, new { @id = "AddDisplayColumnNameForm", @class = "form-inline" }))
           { %>
        <%=Html.Hidden("entityId",Request.QueryString["entityId"]) %>
        <%if (TempData["CanUseDisplayColumnName"] != null && (bool)TempData["CanUseDisplayColumnName"] == true)
          {%>
        <div class="content-box">
            <div class="form-group">
                <label>
                    <%: this.GetLocalResourceObject("DisplayColumnName")%>
                </label>
                <%= Html.DropDownList("DisplayColumnName", (List<SelectListItem>)ViewData["FieldMetaData"], new { style = "width:200px;" })%>
            </div>
            <div class="form-group">
                <a class="btn btn-info" href="#" onclick="AddDisplayColumnName();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                    <i class="fa fa-save"></i>
                    <%=this.GetGlobalResourceObject("General", "Save")%></a>
            </div>
        </div>
        <% } %>
        <% } %>
        <% var fieldList = ViewData["DicEntityFieldMetaData"] as List<EntityFieldMetaData>; %>
        <% if (ViewData["Source"].ToString() == ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING)
           { %>
        <div class="grid simple">
            <% if (fieldList != null && fieldList.Count() > 0)
               {
            %>
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <h4><%:this.GetLocalResourceObject("ParentExtn") %></h4>
                    </div>
                    <%--<div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                            </span>
                        </div>
                    </div>--%>
                </div>
            </div>
            <% } %>
            <div class="grid-body">
                <%if (fieldList != null && fieldList.Count() > 0)
                  {
                      Html.Grid(fieldList).Columns(
                        column =>
                        {
                            column.For(col => col.Name).Named(this.GetLocalResourceObject("FieldName").ToString());
                            column.For(col => col.Length).Named(this.GetLocalResourceObject("Length").ToString());
                            column.For(col => col.IsUnique).Named(this.GetLocalResourceObject("IsUnique").ToString());
                            column.For(col => HtmlExtensions.GetDataFieldTypeDisplayName(col.TypeID)).Named(this.GetLocalResourceObject("FieldType").ToString());
                        }).Attributes(@class => "celloTable").Render();
                  }
                  else
                  {%>
                <div class="alert alert-info">
                    <%:this.GetLocalResourceObject("NoRecords") %>
                </div>
                <% } %>
            </div>
        </div>
        <% } %>
        <div class="grid simple">
            <%  
                if (ViewData["Source"].ToString() == ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING)
                {
                    fieldList = ViewData["OwnEntityFieldMetaData"] != null ? ((List<EntityFieldMetaData>)ViewData["OwnEntityFieldMetaData"]) : null;
            %>
            <div class="grid-title">
                <h4>
                    <%:this.GetLocalResourceObject("OwnExtn") %></h4>
            </div>
            <% } %>
            <div class="grid-body">
                <% if (fieldList != null && fieldList.Count() > 0)
                   {
                       Html.Grid(fieldList).Columns(
                         column =>
                         {
                             column.For(col => col.Name).Named(this.GetLocalResourceObject("FieldName").ToString());
                             column.For(col => col.Length).Named(this.GetLocalResourceObject("Length").ToString());
                             column.For(col => col.IsUnique).Named(this.GetLocalResourceObject("IsUnique").ToString());
                             column.For(col => HtmlExtensions.GetDataFieldTypeDisplayName(col.TypeID)).Named(this.GetLocalResourceObject("FieldType").ToString());
                             column.For(col => Html.CelloActionLink("~Edit~", "ManageField", new { fieldId = col.EntityFieldIdentifier, entityId = Request.QueryString["entityId"], name = Request.QueryString["name"] }, null).Replace("~Edit~", "<i class='fa fa-pencil'></i>"))
                                 .Named(this.GetLocalResourceObject("EditField").ToString())
                                 .HeaderAttributes(@class => "halign noSortCol")
                                 .Attributes(@class => "halign")
                                 .DoNotEncode();
                             column.For(col => Html.CelloActionLink("DeleteField", "DeleteEntityField", new { fieldId = col.EntityFieldIdentifier, entityId = Request.QueryString["entityId"], name = Request.QueryString["name"] }).Replace("DeleteField", "<i class='fa fa-trash-o'></i>"))
                                 .Named(this.GetLocalResourceObject("DeleteField").ToString())
                                 .HeaderAttributes(@class => "halign noSortCol")
                                 .Attributes(@class => "halign")
                                 .DoNotEncode();
                         }).Attributes(@class => "celloTable").Render();
                   }
                   else
                   { %>
                <div class="alert alert-info">
                    <%:this.GetLocalResourceObject("NoRecords") %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
