<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.DataManagement.EntityFieldMetaData>" %>

<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.DataManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {

            $('form select').each(function () {
                $(this).attr('style', 'width:100%;');
            });
            $('form select').select2();

            toggle();
            checklength();

            $("select[name='TypeID']").change(function () {
                toggle();
            });

            $("#Length").blur(function () {
                checklength();
            });
        });

        function toggle() {
            var elementValue = document.getElementById("TypeID").value;

            var entityFieldId = document.getElementById("ReferenceEntityFieldId").value;
            var pickUpFieldId = document.getElementById("PickUpFieldId").value;
            var varcharFieldId = document.getElementById("VarcharFieldId").value;
            var intFieldId = document.getElementById("IntFieldId").value;
            var floatFieldId = document.getElementById("FloatFieldId").value;
            var booleanFieldId = document.getElementById("BooleanFieldId").value;
            var dateFieldId = document.getElementById("DateFieldId").value;

            var entity = document.getElementById("ReferenceEntityId");
            var pickup = document.getElementById("PickupListId");
            var lengthField = document.getElementById("Length");
            var isUniqueField = document.getElementById("IsUnique");
            var regularExpressionField = document.getElementById("ValidationRegEx");

            pickup.disabled = (elementValue != pickUpFieldId);
            entity.disabled = (elementValue != entityFieldId);
            isUniqueField.disabled = (elementValue == booleanFieldId || elementValue == pickUpFieldId || elementValue == entityFieldId);
            lengthField.disabled = (elementValue != varcharFieldId);
            regularExpressionField.disabled = (elementValue == pickUpFieldId || elementValue == entityFieldId || elementValue == booleanFieldId);
        }

        function checklength() {
            var elementValue = document.getElementById("Length").value;
            if (elementValue.replace(/\s/g, "") == "") {
                document.getElementById("Length").value = "0";
                return;
            }
            if (isNaN(elementValue)) {
                alert("<%:this.GetLocalResourceObject("i_ValidLengtgh") %>");
                document.getElementById("Length").select();
                return;
            }
            else {
                if (elementValue > 1000) {
                    alert("<%:this.GetLocalResourceObject("i_Gtlength") %>");
                    document.getElementById("Length").select();
                    return;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%:Url.Action("FieldList", "Data", new { entityId=ViewData["EntityId"], name=ViewData["EntityName"] })%>" title="<%:this.GetLocalResourceObject("t_Goback") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <span class="semi-bold"><%: ViewData["EntityName"] %></span> - 
                <%=this.GetLocalResourceObject("FieldDetails")%>
        </h3>
    </div>
    <% using (Html.BeginForm("ManageField", "Data", FormMethod.Post))
       { %>
    <%=Html.Hidden("EntityId",ViewData["EntityId"]) %>
    <%=Html.Hidden("FieldId", ViewData["fieldId"])%>
    <%=Html.Hidden("PickUpFieldId", CelloSaaS.Model.DataManagement.DataFieldType.PickUpField)%>
    <%=Html.Hidden("VarcharFieldId", CelloSaaS.Model.DataManagement.DataFieldType.Varchar)%>
    <%=Html.Hidden("IntFieldId", CelloSaaS.Model.DataManagement.DataFieldType.Int)%>
    <%=Html.Hidden("FloatFieldId", CelloSaaS.Model.DataManagement.DataFieldType.Float)%>
    <%=Html.Hidden("BooleanFieldId", CelloSaaS.Model.DataManagement.DataFieldType.Boolean)%>
    <%=Html.Hidden("DateFieldId", CelloSaaS.Model.DataManagement.DataFieldType.Date)%>
    <%=Html.Hidden("ReferenceEntityFieldId", CelloSaaS.Model.DataManagement.DataFieldType.Entity)%>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetLocalResourceObject("t_ManageFieldDetails") %></h4>
            </header>
            <div class="panel-body">
                <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
                   { %>
                <div class="alert alert-danger">
                    <%=Html.ValidationSummary()%>
                </div>
                <% }  %>
                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%= Html.ValidationMessage("Name") !=null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%=this.GetLocalResourceObject("Name")%>
                                </label>
                                <%= Html.TextBox("Name", Model.Name)%>
                                <%= Html.Hidden("EntityFieldIdentifier",Model.EntityFieldIdentifier)%>
                                <%= Html.ValidationMessage("Name","*") %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <%= Html.ValidationMessage("TypeID") !=null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%=this.GetLocalResourceObject("TypeID")%>
                                </label>
                                <%= Html.DropDownList("TypeID", null, new { style="width:100%;" })%>
                                <%= Html.CelloValidationMessage("TypeID", "*") %>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%= Html.ValidationMessage("PickupListId") !=null ? "has-error" : "" %>">
                                <label>
                                    <%=this.GetLocalResourceObject("PickUpField")%>
                                </label>
                                <%=Html.DropDownList("PickupListId", null, new { style="width:100%;" })%>
                                <%= Html.CelloValidationMessage("PickupListId", "*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <%= Html.ValidationMessage("ReferenceEntityId") !=null ? "has-error" : "" %>">
                                <label>
                                    <%=this.GetLocalResourceObject("EntityField")%>
                                </label>
                                <%=Html.DropDownList("ReferenceEntityId", null, new { style="width:100%;" })%>
                                <%= Html.CelloValidationMessage("ReferenceEntityId", "*")%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%= Html.ValidationMessage("Length") !=null ? "has-error" : "" %>">
                                <label>
                                    <%=this.GetLocalResourceObject("Length")%>
                                </label>
                                <%= Html.TextBox("Length") %>
                                <%= Html.CelloValidationMessage("Length", "*") %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%=this.GetLocalResourceObject("ValidationRegEx")%>
                                </label>
                                <%= Html.TextBox("ValidationRegEx", string.IsNullOrEmpty(Model.ValidationRegEx) ? "" : Model.ValidationRegEx, new { maxlength = 200 })%>
                                <%= Html.CelloValidationMessage("ValidationRegEx", "*") %>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%=this.GetLocalResourceObject("IsUnique")%>
                                </label>
                                <%= Html.CheckBox("IsUnique") %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <% if (string.IsNullOrEmpty(Html.CelloValidationMessage("AccessError")))
                                   { %>
                                <button type="submit" class="btn btn-success" href="#" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                                    <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></button>
                                <% } %>
                                <a class="btn btn-default" href="<%:Url.Action("FieldList","Data",new{entityId=ViewData["EntityId"]}) %>"
                                    title="<%:this.GetLocalResourceObject("BackTitle") %>">
                                    <%=this.GetGlobalResourceObject("General","Back") %></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <% } %>
</asp:Content>
