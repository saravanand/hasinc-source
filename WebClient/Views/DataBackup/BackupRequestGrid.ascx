﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.DataBackup.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% if (Html.ValidationMessage("Success") != null)
   { %>
<div class="alert alert-success">
    <%=Html.ValidationMessage("Success") %>
</div>
<% } %>
<% if (Html.ValidationMessage("Error") != null)
   { %>
<div class="alert alert-danger">
    <%=Html.ValidationMessage("Error")%>
</div>
<% } %>
<% if (ViewBag.DataBackupList != null && ViewBag.DataBackupList.Count > 0)
   {
       var cond = (ViewBag.Condition as BackupRequestSearchCondition) ?? new BackupRequestSearchCondition();
       var rv = new
       {
           PageNumber = ViewBag.PageNumber,
           PageSize = ViewBag.PageSize,
           SortString = cond.SortString,
           SortDirection = cond.SortDirection,
           BackupStatus = cond.BackupStatus,
           BackupMode = cond.BackupMode,
           BackupDestinationType = cond.BackupDestinationType,
           FromRequestDate = cond.FromRequestDate,
           ToRequestDate = cond.ToRequestDate,
           PrimaryTenantId = cond.PrimaryTenantId
       };

       var tenantNames = ViewBag.TenantNames as Dictionary<string, string>;
       Html.Grid<CelloSaaS.DataBackup.Model.BackupRequest>((ViewBag.DataBackupList as Dictionary<string, CelloSaaS.DataBackup.Model.BackupRequest>).Values)
           .Columns(column =>
           {
               column.For(x => tenantNames != null ? tenantNames[x.PrimaryTenantId] : string.Empty)
                   .Named(this.GetLocalResourceObject("PrimaryTenant").ToString()).DoNotEncode();

               column.For(x => new System.Data.SqlClient.SqlConnectionStringBuilder(x.Source).InitialCatalog).Named(this.GetLocalResourceObject("Source").ToString());

               /*column.For(x => x.BackupMode)
                   .Named(Ajax.ActionLink(this.GetLocalResourceObject("BackupMode").ToString(), "Index", "DataBackup", new
            {
                PageNo = ViewData["PageNumber"],
                PageSize = ViewData["PageSize"],
                SortString = "BackupMode",
                SortDirection = (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("BackupMode") && !string.IsNullOrEmpty(cond.SortDirection) && cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Decending)) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
                BackupStatus = cond.BackupStatus,
                BackupMode = cond.BackupMode,
                BackupDestinationType = cond.BackupDestinationType,
                FromRequestDate = cond.FromRequestDate,
                ToRequestDate = cond.ToRequestDate,
                PrimaryTenantId = cond.PrimaryTenantId
            },
            new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }).ToHtmlString())
            .HeaderAttributes(new
            Hash(@class => (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("BackupMode") ? ((string.IsNullOrEmpty(cond.SortDirection) || cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"), style => "min-width:90px"))
            .DoNotEncode(); */

               column.For(x => x.DestinationType)
                   .Named(Ajax.ActionLink(this.GetLocalResourceObject("DestinationType").ToString(), "Index", "DataBackup", new
            {
                PageNo = ViewData["PageNumber"],
                PageSize = ViewData["PageSize"],
                SortString = "DestinationType",
                SortDirection = (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("DestinationType") && !string.IsNullOrEmpty(cond.SortDirection) && cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Decending)) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
                BackupStatus = cond.BackupStatus,
                BackupMode = cond.BackupMode,
                BackupDestinationType = cond.BackupDestinationType,
                FromRequestDate = cond.FromRequestDate,
                ToRequestDate = cond.ToRequestDate,
                PrimaryTenantId = cond.PrimaryTenantId
            },
            new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }).ToHtmlString())
            .HeaderAttributes(new
            Hash(@class => (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("DestinationType") ? ((string.IsNullOrEmpty(cond.SortDirection) || cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"), style => "min-width:90px"))
            .DoNotEncode();

               //column.For(x => x.RequestBy).Named(this.GetLocalResourceObject("RequestBy").ToString());

               column.For(x => x.RequestDate.ToUIDateTimeString())
                   .Named(Ajax.ActionLink(this.GetLocalResourceObject("RequestDate").ToString(), "Index", "DataBackup", new
            {
                PageNo = ViewData["PageNumber"],
                PageSize = ViewData["PageSize"],
                SortString = "RequestDate",
                SortDirection = (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("RequestDate") && !string.IsNullOrEmpty(cond.SortDirection) && cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Decending)) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
                BackupStatus = cond.BackupStatus,
                BackupMode = cond.BackupMode,
                BackupDestinationType = cond.BackupDestinationType,
                FromRequestDate = cond.FromRequestDate,
                ToRequestDate = cond.ToRequestDate,
                PrimaryTenantId = cond.PrimaryTenantId
            },
            new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }).ToHtmlString())
            .HeaderAttributes(new
            Hash(@class => (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("RequestDate") ? ((string.IsNullOrEmpty(cond.SortDirection) || cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"), style => "min-width:90px"))
            .DoNotEncode();

               column.For(x => string.Format("<div class=\"{0}\">{0}</div>", x.BackupStatus))
                   .Attributes(@class => "halign")
                   .Named(Ajax.ActionLink(this.GetLocalResourceObject("BackupStatus").ToString(), "Index", "DataBackup", new
            {
                PageNo = ViewData["PageNumber"],
                PageSize = ViewData["PageSize"],
                SortString = "BackupStatus",
                SortDirection = (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("BackupStatus") && !string.IsNullOrEmpty(cond.SortDirection) && cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Decending)) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
                BackupStatus = cond.BackupStatus,
                BackupMode = cond.BackupMode,
                BackupDestinationType = cond.BackupDestinationType,
                FromRequestDate = cond.FromRequestDate,
                ToRequestDate = cond.ToRequestDate,
                PrimaryTenantId = cond.PrimaryTenantId
            },
            new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }).ToHtmlString())
            .HeaderAttributes(new
            Hash(@class => "halign " + (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("BackupStatus") ? ((string.IsNullOrEmpty(cond.SortDirection) || cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"), style => "min-width:90px"))
            .DoNotEncode();

               column.For(x => x.StartDate == DateTime.MinValue ? "--" : (x.StartDate.ToUIDateTimeString()))
                   .Named(Ajax.ActionLink(this.GetLocalResourceObject("StartDate").ToString(), "Index", "DataBackup", new
            {
                PageNo = ViewData["PageNumber"],
                PageSize = ViewData["PageSize"],
                SortString = "StartDate",
                SortDirection = (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("StartDate") && !string.IsNullOrEmpty(cond.SortDirection) && cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Decending)) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
                BackupStatus = cond.BackupStatus,
                BackupMode = cond.BackupMode,
                BackupDestinationType = cond.BackupDestinationType,
                FromRequestDate = cond.FromRequestDate,
                ToRequestDate = cond.ToRequestDate,
                PrimaryTenantId = cond.PrimaryTenantId
            },
            new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }).ToHtmlString())
            .HeaderAttributes(new
            Hash(@class => (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("StartDate") ? ((string.IsNullOrEmpty(cond.SortDirection) || cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"), style => "min-width:90px"))
            .DoNotEncode();

               column.For(x => x.CompletedDate == DateTime.MinValue ? "--" : (x.CompletedDate.ToUIDateTimeString()))
                   .Named(Ajax.ActionLink(this.GetLocalResourceObject("CompletedDate").ToString(), "Index", "DataBackup", new
            {
                PageNo = ViewData["PageNumber"],
                PageSize = ViewData["PageSize"],
                SortString = "CompletedDate",
                SortDirection = (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("CompletedDate") && !string.IsNullOrEmpty(cond.SortDirection) && cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Decending)) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
                BackupStatus = cond.BackupStatus,
                BackupMode = cond.BackupMode,
                BackupDestinationType = cond.BackupDestinationType,
                FromRequestDate = cond.FromRequestDate,
                ToRequestDate = cond.ToRequestDate,
                PrimaryTenantId = cond.PrimaryTenantId
            },
            new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }).ToHtmlString())
            .HeaderAttributes(new
            Hash(@class => (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("CompletedDate") ? ((string.IsNullOrEmpty(cond.SortDirection) || cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"), style => "min-width:90px"))
            .DoNotEncode();

               column.For(x => x.BackupStatus != BackupStatus.Completed ? "--" : x.BackupSize.ToString("F2"))
                   .Named(Ajax.ActionLink(this.GetLocalResourceObject("BackupSize").ToString(), "Index", "DataBackup", new
            {
                PageNo = ViewData["PageNumber"],
                PageSize = ViewData["PageSize"],
                SortString = "BackupSize",
                SortDirection = (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("BackupSize") && !string.IsNullOrEmpty(cond.SortDirection) && cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Decending)) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
                BackupStatus = cond.BackupStatus,
                BackupMode = cond.BackupMode,
                BackupDestinationType = cond.BackupDestinationType,
                FromRequestDate = cond.FromRequestDate,
                ToRequestDate = cond.ToRequestDate,
                PrimaryTenantId = cond.PrimaryTenantId
            },
            new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }).ToHtmlString())
            .HeaderAttributes(new
            Hash(@class => (!string.IsNullOrEmpty(cond.SortString) && cond.SortString.Equals("BackupSize") ? ((string.IsNullOrEmpty(cond.SortDirection) || cond.SortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"), style => "min-width:90px"))
            .DoNotEncode();

               column.For(x => "<a href='" + Url.Action("ViewRequestDetails", new
               {
                   requestId = x.RequestId,
                   PageNo = ViewData["PageNumber"],
                   PageSize = ViewData["PageSize"],
                   SortString = cond.SortString,
                   SortDirection = cond.SortDirection,
                   BackupStatus = cond.BackupStatus,
                   BackupMode = cond.BackupMode,
                   BackupDestinationType = cond.BackupDestinationType,
                   FromRequestDate = cond.FromRequestDate,
                   ToRequestDate = cond.ToRequestDate,
                   PrimaryTenantId = cond.PrimaryTenantId
               }) + "' title=' " + this.GetLocalResourceObject("t_Viewrequestdetails").ToString() + "'><i class='fa fa-search'></i></a>")
                   .Attributes(@class => "halign")
                   .HeaderAttributes(@class => "halign")
                   .Named(this.GetLocalResourceObject("View").ToString()).DoNotEncode();

               column.For(x => x.BackupStatus == BackupStatus.New ? "<a href='" + Url.Action("CancelRequest", new
               {
                   requestId = x.RequestId,
                   PageNo = ViewData["PageNumber"],
                   PageSize = ViewData["PageSize"],
                   SortString = cond.SortString,
                   SortDirection = cond.SortDirection,
                   BackupStatus = cond.BackupStatus,
                   BackupMode = cond.BackupMode,
                   BackupDestinationType = cond.BackupDestinationType,
                   FromRequestDate = cond.FromRequestDate,
                   ToRequestDate = cond.ToRequestDate,
                   PrimaryTenantId = cond.PrimaryTenantId
               }) + "'><i class='fa fa-trash-o'></i></a>" : "--")
                   .Attributes(@class => "halign")
                   .HeaderAttributes(@class => "halign")
                   .Named(this.GetLocalResourceObject("Cancel").ToString()).DoNotEncode();

           }).Attributes(id => "dataList", @class => "celloTable").Render();

       Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divResult", LoadingElementDuration = 500, LoadingElementId = "loading" }, rv)
            .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
            .Render();
   }
   else
   { %>
<div class="alert alert-info">
    <%:this.GetLocalResourceObject("NoRecords") %>
</div>
<% } %>
