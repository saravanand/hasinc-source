﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel indigo">
    <header class="panel-heading">
        <h4><%:this.GetLocalResourceObject("Title")%></h4>
    </header>
    <div class="panel-body">
        <div class="form-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group <%=Html.ValidationMessage("ServerName", "*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("ServerAddress")%>
                        </label>
                        <%=Html.TextBox("ServerName") %>
                        <%=Html.ValidationMessage("ServerName", "*")%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group <%=Html.ValidationMessage("DBName", "*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("BackupDatabaseName")%>
                        </label>
                        <%=Html.TextBox("DBName") %>
                        <%=Html.ValidationMessage("DBName", "*")%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group <%=Html.ValidationMessage("UserName", "*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("UserName")%>
                        </label>
                        <%=Html.TextBox("UserName", "", new { autocomplete="off" })%>
                        <%=Html.ValidationMessage("UserName", "*")%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group <%=Html.ValidationMessage("Password", "*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("Password")%>
                        </label>
                        <%=Html.Password("Password") %>
                        <%=Html.ValidationMessage("Password", "*")%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
