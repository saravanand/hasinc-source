﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.DataBackup.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel green">
    <header class="panel-heading">
        <h4><%:this.GetLocalResourceObject("Title")%></h4>
    </header>
    <div class="panel-body">
        <div class="form-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group <%=Html.ValidationMessage("FilePath", "*") != null ? "has-error" : "" %>">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("FilePath")%>
                        </label>
                        <%=Html.TextBox("FilePath") %>
                        <%=Html.ValidationMessage("FilePath", "*")%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("FileFormat")%></label>
                        <%=Html.DropDownList("DataFormat", typeof(ExportDataFormat).ToSelectList(), new { style="width:100%;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label><%:this.GetLocalResourceObject("ZipFiles")%></label>
                        <%=Html.CheckBox("ZipFiles",true)%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label><%:this.GetLocalResourceObject("UploadtoFTP")%></label>
                        <%=Html.CheckBox("UploadToFtp",false)%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="ftpDetails">
    <section class="panel orange">
        <header class="panel-heading">
            <h4><%:this.GetLocalResourceObject("FtpDetails")%></h4>
        </header>
        <div class="panel-body">
            <div class="form-container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group <%=Html.ValidationMessage("FtpDetails.ServerName", "*") != null ? "has-error" : "" %>">
                            <label><%:this.GetLocalResourceObject("ServerAddress")%></label>
                            <%=Html.TextBox("FtpDetails.ServerName")%>
                            <%=Html.ValidationMessage("FtpDetails.ServerName", "*")%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <%=Html.ValidationMessage("FtpDetails.Path", "*") != null ? "has-error" : "" %>">
                            <label><%:this.GetLocalResourceObject("FTPPath")%></label>
                            <%=Html.TextBox("FtpDetails.Path")%>
                            <%=Html.ValidationMessage("FtpDetails.Path", "*")%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <%=Html.ValidationMessage("FtpDetails.UserName", "*") != null ? "has-error" : "" %>">
                            <label><%:this.GetLocalResourceObject("UserName")%></label>
                            <%=Html.TextBox("FtpDetails.UserName", "", new { autocomplete = "off" })%>
                            <%=Html.ValidationMessage("FtpDetails.UserName", "*")%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <%=Html.ValidationMessage("FtpDetails.Password", "*") != null ? "has-error" : "" %>">
                            <label><%:this.GetLocalResourceObject("Password")%></label>
                            <%=Html.Password("FtpDetails.Password")%>
                            <%=Html.ValidationMessage("FtpDetails.Password", "*")%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <%=Html.ValidationMessage("FtpDetails.PortNo", "*") != null ? "has-error" : "" %>">
                            <label><%:this.GetLocalResourceObject("PortNo")%></label>
                            <%=Html.TextBox("FtpDetails.PortNo")%>
                            <%=Html.ValidationMessage("FtpDetails.PortNo", "*")%>
                            <br />
                            <span>(<%:this.GetLocalResourceObject("i_PortNo")%>)</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(function () {
        $('#UploadToFtp').change(function () {
            var checked = $(this).is(':checked');

            if (checked) {
                $('#ftpDetails').fadeIn();
            } else {
                //$('#ftpDetails input').val('');
                $('#ftpDetails').fadeOut();
            }
        });
        $('#UploadToFtp').trigger('change');
        $('#DataFormat').select2();
    });
</script>
