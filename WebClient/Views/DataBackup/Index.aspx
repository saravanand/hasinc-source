﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.DataBackup.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("Title") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetGlobalResourceObject("General", "Filter") %></h4>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="form-container">
                        <% using (Html.BeginForm("Index", "DataBackup", FormMethod.Post, new { id = "searchForm", role = "role" }))
                           {
                        %>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%:this.GetLocalResourceObject("BackupStatus")%>
                                <%= Html.DropDownList("BackupStatus", typeof(BackupStatus).ToSelectList(null, true), new { style="width:100%" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%:this.GetLocalResourceObject("DestinationType")%>
                                <%= Html.DropDownList("BackupDestinationType", typeof(BackupDestinationType).ToSelectList(null, true), new { style="width:100%" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%:this.GetLocalResourceObject("From")%>
                                <%= Html.TextBox("FromRequestDate", "", new { placeholder = this.GetLocalResourceObject("FromPlaceHolder"), title = this.GetLocalResourceObject("FromPlaceHolder"), @class = "datetime" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%:this.GetLocalResourceObject("To")%>
                                <%= Html.TextBox("ToRequestDate", "", new { placeholder = this.GetLocalResourceObject("ToPlaceHolder"), title = this.GetLocalResourceObject("ToPlaceHolder"), @class = "datetime" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <a class="btn btn-info" href="#" onclick="$('form').submit()" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                                    <%:this.GetGlobalResourceObject("General", "Search")%></a>
                                <a class="btn btn-default" href="#" onclick="javascript:formReset();" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                                    <%:this.GetGlobalResourceObject("General", "Reset")%></a>
                            </div>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </section>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <h4> <%:this.GetLocalResourceObject("h_Databackup") %></h4>
                    </div>
                    <div class="col-md-6">
                        <% if (ViewBag.CanCreate)
                           { %>
                        <div class="pull-right">
                            <a class="btn btn-success" href="<%=Url.Action("ManageBackupRequest") %>" title="<%:this.GetLocalResourceObject("CreateTitle") %>">
                                <i class="fa fa-plus"></i>&nbsp;<%:this.GetLocalResourceObject("Create") %></a>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <div id="divResult">
                    <% Html.RenderPartial("BackupRequestGrid"); %>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#BackupDestinationType,#BackupStatus').select2();

            $('#FromRequestDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>",
            });

            $('#ToRequestDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>",
            });

            $('#searchForm')[0].reset();

            $('#searchForm').submit(function () {
                var fromDate = new Date($('#FromRequestDate').val());
                var toDate = new Date($('#ToRequestDate').val());

                if (fromDate && toDate) {
                    if (fromDate > toDate) {
                        alert('<%:this.GetLocalResourceObject("e_LessData") %>');
                        return false;
                    }
                }

                var postdata = $(this).serialize();

                var pageSize = $('input[type=hidden][name=pager_pageSize]').val();
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                postdata += "&pageSize=" + pageSize;
                postdata += "&sortString=" + (sortString || '');
                postdata += "&sortDirection=" + (sortDirection || '');

                $.post($(this).attr('action'), postdata, function (data) {
                    $('#divResult').hide().html(data).slideDown();
                }).error(function () {
                    $('#divResult').hide().html('<div class="error"><%:this.GetLocalResourceObject("e_Search") %></div>').slideDown();
                });

                return false;
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                var queryString = '?pageSize=' + $(this).val();
                queryString += '&sortString=' + (sortString || '');
                queryString += '&sortDirection=' + (sortDirection || '');

                queryString += '&' + $('#searchForm').serialize();

                $.post('<%=Url.Action("Index")%>' + queryString, null, function (data) {
                    $('#divResult').fadeOut().html(data).slideDown();
                }).error(function () {
                    $('#divResult').html('<div class="error"><%:this.GetLocalResourceObject("e_fecthingBackup") %></div>').fadeIn();
                });

                $('div.alert').remove();
            });
        });

        function formReset() {
            $('#searchForm')[0].reset();
            $('#BackupDestinationType,#BackupStatus').trigger('change');
            $('#searchForm').trigger('submit');
            $("#FromRequestDate").datepicker("option", "maxDate", new Date());
            $("#ToRequestDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#FromRequestDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#ToRequestDate").datepicker("option", "maxDate", new Date());
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .Completed, .Errored, .New, .Started, .Canceled {
            border: 1px solid #999;
            text-align: center;
            color: #fff;
            margin: 0 10px;
        }

        .Errored {
            background: #a90329; /* Old browsers */ /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2E5MDMyOSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQ0JSIgc3RvcC1jb2xvcj0iIzhmMDIyMiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM2ZDAwMTkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, #a90329 0%, #8f0222 44%, #6d0019 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a90329), color-stop(44%,#8f0222), color-stop(100%,#6d0019)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #a90329 0%,#8f0222 44%,#6d0019 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #a90329 0%,#8f0222 44%,#6d0019 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #a90329 0%,#8f0222 44%,#6d0019 100%); /* IE10+ */
            background: linear-gradient(to bottom, #a90329 0%,#8f0222 44%,#6d0019 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a90329', endColorstr='#6d0019',GradientType=0 ); /* IE6-8 */
        }

        .Canceled {
            background: rgb(246,230,180); /* Old browsers */ /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2Y2ZTZiNCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNlZDkwMTciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, rgba(246,230,180,1) 0%, rgba(237,144,23,1) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(246,230,180,1)), color-stop(100%,rgba(237,144,23,1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(246,230,180,1) 0%,rgba(237,144,23,1) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(246,230,180,1) 0%,rgba(237,144,23,1) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(246,230,180,1) 0%,rgba(237,144,23,1) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(246,230,180,1) 0%,rgba(237,144,23,1) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f6e6b4', endColorstr='#ed9017',GradientType=0 ); /* IE6-8 */
        }

        .Started {
            background: rgb(144,191,84); /* Old browsers */ /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzkwYmY1NCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjM1JSIgc3RvcC1jb2xvcj0iIzkxYjc2YSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjcxJSIgc3RvcC1jb2xvcj0iIzc2YWI0ZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM2N2E0NDciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, rgba(144,191,84,1) 0%, rgba(145,183,106,1) 35%, rgba(118,171,79,1) 71%, rgba(103,164,71,1) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(144,191,84,1)), color-stop(35%,rgba(145,183,106,1)), color-stop(71%,rgba(118,171,79,1)), color-stop(100%,rgba(103,164,71,1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(144,191,84,1) 0%,rgba(145,183,106,1) 35%,rgba(118,171,79,1) 71%,rgba(103,164,71,1) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(144,191,84,1) 0%,rgba(145,183,106,1) 35%,rgba(118,171,79,1) 71%,rgba(103,164,71,1) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(144,191,84,1) 0%,rgba(145,183,106,1) 35%,rgba(118,171,79,1) 71%,rgba(103,164,71,1) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(144,191,84,1) 0%,rgba(145,183,106,1) 35%,rgba(118,171,79,1) 71%,rgba(103,164,71,1) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#90bf54', endColorstr='#67a447',GradientType=0 ); /* IE6-8 */
        }

        .New {
            background: #6db3f2; /* Old browsers */ /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzZkYjNmMiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzU0YTNlZSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzM2OTBmMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMxZTY5ZGUiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, #6db3f2 0%, #54a3ee 50%, #3690f0 51%, #1e69de 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#6db3f2), color-stop(50%,#54a3ee), color-stop(51%,#3690f0), color-stop(100%,#1e69de)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #6db3f2 0%,#54a3ee 50%,#3690f0 51%,#1e69de 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #6db3f2 0%,#54a3ee 50%,#3690f0 51%,#1e69de 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #6db3f2 0%,#54a3ee 50%,#3690f0 51%,#1e69de 100%); /* IE10+ */
            background: linear-gradient(to bottom, #6db3f2 0%,#54a3ee 50%,#3690f0 51%,#1e69de 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6db3f2', endColorstr='#1e69de',GradientType=0 ); /* IE6-8 */
        }

        .Completed {
            background: #627d4d; /* Old browsers */ /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzYyN2Q0ZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMxZjNiMDgiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, #627d4d 0%, #1f3b08 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#627d4d), color-stop(100%,#1f3b08)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #627d4d 0%,#1f3b08 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #627d4d 0%,#1f3b08 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #627d4d 0%,#1f3b08 100%); /* IE10+ */
            background: linear-gradient(to bottom, #627d4d 0%,#1f3b08 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#627d4d', endColorstr='#1f3b08',GradientType=0 ); /* IE6-8 */
        }
    </style>
</asp:Content>
