﻿<%@ Page Title="<%$Resources:NewDataBackupRequest%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.DataBackup.Model.BackupRequest>" %>

<%@ Import Namespace="CelloSaaS.DataBackup.Model" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="<%:this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%:this.GetLocalResourceObject("NewDataBackupRequest")%>
        </h3>
        <div class="pull-right">
            <a class="btn btn-default" href="<%=Url.Action("Index") %>" title="<%:this.GetGlobalResourceObject("General","Cancel") %>">
                <%:this.GetGlobalResourceObject("General","Cancel") %></a>
            <a class="btn btn-info" href="#" onclick="$('form').submit()" title="Submit this Backup Request!">
                <%:this.GetGlobalResourceObject("General","Submit") %></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary()%>
        </div>
        <% } %>
        <% using (Html.BeginForm())
           {
        %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetLocalResourceObject("h_Sourcedetails") %></h4>
            </header>
            <div class="panel-body">
                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("SourceDatabase")%></label>
                                <%=Html.DropDownList("Source", (IEnumerable<SelectListItem>)ViewBag.Sources, new { style = "width:100%;" })%>
                                <%=Html.ValidationMessage("Source", "*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("BackupCommonData")%></label>
                                <%=Html.DropDownList("BackupMode",typeof(BackupMode).ToSelectList(), new { style = "width:100%;" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <% if (ViewBag.TenantIds != null)
                                   { %>
                                <label class="mandatory">
                                    <%:this.GetLocalResourceObject("PrimaryTenant")%></label>
                                <%=Html.DropDownList("PrimaryTenantId", (IEnumerable<SelectListItem>)ViewBag.TenantIds, new { style = "width:100%;" })%>
                                <%=Html.ValidationMessage("PrimaryTenantId", "*")%>
                                <% }
                                   else
                                   { %>
                                <label>
                                    <%:this.GetLocalResourceObject("TenantName")%></label>
                                <br />
                                <%: CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantInfo(TenantContext.GetTenantId("_DataBackup")).TenantDetails.TenantName %>
                                <%=Html.Hidden("PrimaryTenantId", TenantContext.GetTenantId("_DataBackup"))%>
                                <% } %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> <%:this.GetLocalResourceObject("lbl_Scope")%></label>
                                <%=Html.DropDownList("TenantScopes",null, new { style = "width:100%;" })%>
                                <%=Html.Hidden("SelectedTenantIds") %>
                                <a href="#" id="btn-selectTenant" style="display: none;" title="<%:this.GetLocalResourceObject("t_ClickToSelectTenants")%>">
                                    <i class="fa fa-wrench"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("DestinationType")%></label>
                                <%=Html.DropDownList("DestinationType", typeof(BackupDestinationType).ToSelectList(), new { style = "width:100%;" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("Description")%></label>
                                <%=Html.TextArea("Description", new { maxlength=1000 })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="destinationDetails">
            <% Html.RenderPartial(this.Model.DestinationType.ToString() + "Form", this.Model.DestinationDetails); %>
        </div>
        <div class="pull-right">
            <a class="btn btn-default" href="<%=Url.Action("Index") %>" title="<%:this.GetGlobalResourceObject("General","Cancel") %>">
                <%:this.GetGlobalResourceObject("General","Cancel") %></a>
            <a class="btn btn-info" href="#" onclick="$('form').submit()" title="Submit this Backup Request!">
                <%:this.GetGlobalResourceObject("General","Submit") %></a>
        </div>
        <% } %>
    </div>
    <div class="modal fade" id="selectiveTenantModal" tabindex="-1" role="dialog" aria-labelledby="selectiveTenantModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 700px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title"><%:this.GetLocalResourceObject("h_SelectTenants")%></h4>
                </div>
                <div class="modal-body" style="max-height:300px;overflow-y:auto;">
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%:this.GetGlobalResourceObject("General","Close") %></button>
                        <button type="button" class="btn btn-info" id="btnSaveSelectiveTenant"><%:this.GetGlobalResourceObject("General","Submit") %></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('select').select2();
            
            $('form').submit(function () {
                var primaryTenantId = $('#PrimaryTenantId').val();
                var scopeId = $('select[name=TenantScopes]').val();

                if (!primaryTenantId) {
                    alert('<%:this.GetLocalResourceObject("e_primarytenant")%>');
                    return false;
                }

                if (scopeId == '<%=TenantScopeConstant.SELECTIVE_TENANT_SCOPE %>') {
                    if ($('input#SelectedTenantIds').val() == '') {
                        alert('<%:this.GetLocalResourceObject("e_SelectTenant")%>');
                        return false;
                    }
                }
                return true;
            });

            $('select[name=PrimaryTenantId]').change(function () {
                $('select[name=TenantScopes]').trigger('change');
            });

            $('select[name=TenantScopes]').change(function () {
                var id = $(this).val();
                var primaryTenantId = $('#PrimaryTenantId').val();

                if (id == '<%=TenantScopeConstant.SELECTIVE_TENANT_SCOPE %>') {
                    $('#btn-selectTenant').show();
                    $('input#SelectedTenantIds').val('');
                    $.post('<%=Url.Action("GetChildTenants") %>', { primaryTenantId: primaryTenantId }, function (data) {
                        if (data && data.length > 0) {
                            var html = '<div class="row">';
                            for (var i = 0; i < data.length; i++) {
                                html += '<div class="col-md-4">';
                                html += '<input type="checkbox" name="SelectedTenantIds" value="' + data[i].Id + '" /> ';
                                html += data[i].Name;
                                html += '</div>';
                            }
                            html += "</div>";
                            $("#selectiveTenantModal .modal-body").html(html);
                        } else {
                            $("#selectiveTenantModal .modal-body").html('<div class="alert alert-info"><%:this.GetLocalResourceObject("e_NoChildTenant")%><div>').fadeIn();
                        }
                        $('#btn-selectTenant').trigger('click');
                    }, 'json').error(function () {
                        $("#selectiveTenantModal .modal-body").html('<div class="alert alert-danger"><%:this.GetLocalResourceObject("e_Loading")%></div>').fadeIn();
                        $('#btn-selectTenant').trigger('click');
                    });
                } else {
                    $('#btn-selectTenant').hide();
                    $("#selectiveTenantModal .modal-body").empty();
                }
            });

            $('#btn-selectTenant').on('click', function () {
                if ($("#selectiveTenantModal .modal-body input[type=checkbox]").length == 0) {
                    alert('<%:this.GetLocalResourceObject("e_NoChildTenant")%>');
                } else {
                    $("#selectiveTenantModal").modal('show');
                }
                return false;
            });

            $("#selectiveTenantModal").modal({ show: false });

            $('#btnSaveSelectiveTenant').click(function () {
                if ($("#selectiveTenantModal input[type=checkbox]:checked").length == 0) {
                    alert('<%:this.GetLocalResourceObject("e_SelectTenant")%>');
                } else {
                    var tenantIds = $("#selectiveTenantModal input[type=checkbox]:checked").map(function () { return $(this).val(); }).get();
                    $('input#SelectedTenantIds').val(tenantIds);
                    $("#selectiveTenantModal").modal('hide');
                }
            });

            $('select[name=DestinationType]').change(function () {
                $.post('<%=Url.Action("GetDestinationDetailsForm") %>', { DestinationType: $(this).val() }, function (data) {
                    $('#destinationDetails').html(data);
                }).error(function () {
                    $('#destinationDetails').html('<div class="alert alert-danger"><%:this.GetLocalResourceObject("e_LoadingDataBackup")%></div>');
                });
            });

            if ($('select[name=PrimaryTenantId]').length > 0) {
                $('select[name=PrimaryTenantId]').trigger('change');
            } else {
                $('select[name=TenantScopes]').trigger('change');
            }
        });
    </script>
</asp:Content>
