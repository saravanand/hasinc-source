﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.DataBackup.Model.BackupRequest>" %>

<%@ Import Namespace="CelloSaaS.DataBackup.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% var cond = (ViewBag.Condition as BackupRequestSearchCondition) ?? new BackupRequestSearchCondition(); %>
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("Index",new{page= cond.PageNo,
               pageSize = cond.PageSize,
               BackupStatus = cond.BackupStatus,
               BackupMode = cond.BackupMode,
               BackupDestinationType = cond.BackupDestinationType,
               FromRequestDate = cond.FromRequestDate,
               ToRequestDate = cond.ToRequestDate,
               PrimaryTenantId = cond.PrimaryTenantId}) %>"
            title="<%:this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%:this.GetLocalResourceObject("Title") %>
        </h3>
    </div>
    <div class="row-fluid pd-25 form-container">
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary()%>
        </div>
        <% } %>
        <% if (this.Model != null)
           {
               FTPDetails ftpDetails = null;
        %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4>  <%:this.GetLocalResourceObject("h_BackupRequestDetails") %></h4>
            </header>
            <div class="panel-body">
                <table class="table no-border">
                    <tbody>
                        <tr>
                            <td style="width: 190px">
                                <label>
                                    <%:this.GetLocalResourceObject("SourceDatabaseName") %></label>
                            </td>
                            <td style="width: 350px">
                                <% var connInfo = new System.Data.SqlClient.SqlConnectionStringBuilder(this.Model.Source);%>
                                <%: connInfo.InitialCatalog%>
                            </td>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("BackupCommonData") %></label>
                            </td>
                            <td>
                                <%:this.Model.BackupMode == BackupMode.Full ? "True" : "False" %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("DestinationType") %></label>
                            </td>
                            <td>
                                <%:this.Model.DestinationType %>
                            </td>
                            <td>
                                <label><%:this.GetLocalResourceObject("Description")%></label>
                            </td>
                            <td>
                                <%:this.Model.Description %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("RequestedBy")%></label>
                            </td>
                            <td>
                                <% var user = CelloSaaS.ServiceProxies.UserManagement.UserDetailsProxy.GetUserDetailsByUserId(this.Model.RequestBy); %>
                                <%: user != null ? user.MembershipDetails.UserName : this.Model.RequestBy %>
                            </td>
                            <td>
                                <label><%:this.GetLocalResourceObject("RequestedDate")%></label>
                            </td>
                            <td>
                                <%:this.Model.RequestDate.ToUIDateTimeString()%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("StartedDate")%></label>
                            </td>
                            <td>
                                <% if (this.Model.StartDate != DateTime.MinValue)
                                   { %>
                                <%:this.Model.StartDate.ToUIDateTimeString()%>
                                <%}
                                   else
                                   {%>
                            --
                            <% } %>
                            </td>
                            <td>
                                <label><%:this.GetLocalResourceObject("CompletedDate")%></label>
                            </td>
                            <td>
                                <% if (this.Model.CompletedDate != DateTime.MinValue)
                                   { %>
                                <%:this.Model.CompletedDate.ToUIDateTimeString() %>
                                <% }
                                   else
                                   {%>
                            --
                            <% } %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("BackupSize")%></label>
                            </td>
                            <td>
                                <% if (this.Model.BackupStatus == BackupStatus.Completed)
                                   { %>
                                <%: this.Model.BackupSize.ToString("F2")%>
                                <%}
                                   else
                                   {%>
                            --
                            <% } %>
                            </td>
                            <td>
                                <label><%:this.GetLocalResourceObject("BackupStatus")%></label>
                            </td>
                            <td>
                                <%:this.Model.BackupStatus%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("PrimaryTenant")%></label>
                            </td>
                            <td>
                                <%=CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantInfo(this.Model.PrimaryTenantId).TenantDetails.TenantName%>
                            </td>
                            <td>
                                <label><%= (this.Model.TenantIds.Length > 1) ? this.GetLocalResourceObject("ChildTenants") : string.Empty %></label>
                            </td>
                            <td>
                                <% if (this.Model.TenantIds.Length > 1)
                                   {
                                       var childTenantIds = this.Model.TenantIds.ToList();
                                       childTenantIds.Remove(this.Model.PrimaryTenantId);
                                       var lstTenantDetails = CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantDetailsForShareUsers(childTenantIds.ToArray());
                                       var tenantNames = lstTenantDetails != null ? lstTenantDetails.OrderBy(x => x.Value.TenantName).Select(x => x.Value.TenantName) : null;
                                       Response.Write(string.Join(", ", tenantNames ?? this.Model.TenantIds));
                                   }
                                %>
                            </td>
                        </tr>
                        <% if (this.Model.BackupStatus == BackupStatus.Errored)
                           { %>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("ExceptionDetails")%></label>
                            </td>
                            <td colspan="3">
                                <%:this.Model.ExceptionDetails %>
                            </td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
        </section>
        <section class="panel green">
            <header class="panel-heading">
                <h4><%=this.Model.DestinationType %> <%:this.GetLocalResourceObject("Details")%></h4>
            </header>
            <div class="panel-body">
                <% if (this.Model.DestinationType == BackupDestinationType.Database)
                   {
                       var dest = this.Model.DestinationDetails as DBBackupDestination; %>
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("ServerAddress")%></label>
                            </td>
                            <td style="width: 350px">
                                <%:dest.ServerName%>
                            </td>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("DatabaseName")%></label>
                            </td>
                            <td>
                                <%:dest.DBName%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("Username")%></label>
                            </td>
                            <td style="width: 350px">
                                <%:dest.UserName%>
                            </td>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("Password")%></label>
                            </td>
                            <td>
                                <%:this.GetLocalResourceObject("PasswordSymbol")%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <% }
                   else if (this.Model.DestinationType == BackupDestinationType.File)
                   {
                       var dest = this.Model.DestinationDetails as FileBackupDestination;
                       ftpDetails = dest.UploadToFtp ? dest.FtpDetails : null;
                %>
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("FilePath")%></label>
                            </td>
                            <td style="width: 350px">
                                <%:dest.FilePath%>
                            </td>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("DataFormat")%></label>
                            </td>
                            <td>
                                <%:dest.DataFormat%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("Zipped")%></label>
                            </td>
                            <td style="width: 350px">
                                <%:dest.ZipFiles%>
                            </td>
                            <td style="width: 190px"></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <% } %>
            </div>
        </section>
        <% if (ftpDetails != null)
           { %>
        <section class="panel blue">
            <header class="panel-heading">
                <h4><%:this.GetLocalResourceObject("FTPDetails")%></h4>
            </header>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("FTPServerAddress")%></label>
                            </td>
                            <td style="width: 350px">
                                <%:ftpDetails.ServerName%>
                            </td>
                            <td style="width: 190px">
                                <label><%:this.GetLocalResourceObject("FTPPath")%></label>
                            </td>
                            <td>
                                <%:ftpDetails.Path%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("Username")%></label>
                            </td>
                            <td>
                                <%:ftpDetails.UserName%>
                            </td>
                            <td>
                                <label><%:this.GetLocalResourceObject("Password")%></label>
                            </td>
                            <td>
                                <%:this.GetLocalResourceObject("PasswordSymbol")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label><%:this.GetLocalResourceObject("FTPPortNumber")%></label>
                            </td>
                            <td>
                                <%:ftpDetails.PortNo%>
                            </td>
                            <td>
                                <%-- SSL--%>
                            </td>
                            <td>
                                <%--ftpDetails.EnableSSL--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <% } %>
        <% } %>
    </div>
</asp:Content>
