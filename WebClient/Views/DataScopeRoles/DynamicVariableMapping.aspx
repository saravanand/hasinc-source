﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<script type="text/javascript">

    $(document).ready(function () {
        document.getElementById("TB_ajaxContent").setAttribute("class", "dynamic-var");
        document.getElementById("TB_ajaxContent").removeAttribute("style");
        //document.getElementById("TB_ajaxContent").removeAttribute("width");
    });

    function SaveMultiSelectDatascopeValue() {
        var ids = document.getElementById("DynamicVariablesIds").value;
        var dynamicVaribleIds = new Array();
        dynamicVaribleIds = ids.split(',');
        $.each(dynamicVaribleIds, function (key, value) {
            var dynamicVariableId = value;
            var saveUrl = '<%:Url.Action("SaveDataScopeValue") %>';
            var datascopeValueId;
            var rolePrivilegeId;
            var value;
            if ($('input[type=hidden][name=datascopeValueId_' + dynamicVariableId + ']') != null) {
                datascopeValueId = $('input[type=hidden][name=datascopeValueId_' + dynamicVariableId + ']').val();
            }
            if ($('input[type=hidden][name=rolePrivilegeId]') != null) {
                rolePrivilegeId = $('input[type=hidden][name=rolePrivilegeId]').val();
            }
            var relationIds = new Array();

            // get selected privileges
            //        $('input[type=checkbox][name=chk_' + dynamicVariableId + ']', $('#relationList')).each(function () {
            $('input[type=checkbox][name=chk_' + dynamicVariableId + ']').each(function () {
                if ($(this).is(':checked')) {
                    relationIds.push($(this).val());
                }
            });
            if (relationIds.length > 0) {
                value = "'" + relationIds.join("','") + "'";
            } else { value = ""; }
            $.post(saveUrl, { dynamicDatascopeValueId: datascopeValueId, dynamicVariableId: dynamicVariableId, rolePrivilegeId: rolePrivilegeId, value: value }, function (data) {
                var obj = eval('(' + data + ')');
                if (obj.Error == undefined) {
                    ShowAjaxStatusMessage(obj.Success, true);
                } else {
                    ShowAjaxStatusMessage(obj.Error, false);
                }
            }, 'Json'
        );
        });
        tb_remove();
    }
    function ShowAjaxStatusMessage(message, success) {
        var div = $('#ajaxStatusMessage');
        div.hide();

        var html = '';
        if (success) {
            html += '<div class="success algn_dynamicVar_succ_msg">';
        } else {
            html += '<div class="error algn_dynamicVar_succ_msg">';
        }

        html += '<ul><li style="list-style: none outside none;">';
        html += message;
        html += '</li></ul>';
        html += '</div>';

        div.html(html);
        div.show();
    }
</script>
<% Html.RenderPartial("PartialDyanmicVariableMapping"); %>

  
