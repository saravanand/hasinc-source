<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function FieldRolePrivilegeDetails() {
            formDetails = $('#FieldPrivilegeFrom');
            serializedForm = formDetails.serialize();
            $.post("/DataScopeRoles/FieldPrivilegeDetails", serializedForm, detailsAdd_callBack);
        }

        //This method is used to render the data after added the role privilege details
        function detailsAdd_callBack(data) {
            data = data + '<div id="PrivilegeDetails"></div>';
            $('.error').remove();
            $('.success').remove();
            $("#master").html(data);
        }

        //Role ddl SelectedIndex change event
        function OnSelectedIndexChange() {
            var RoleId = document.getElementById('RoleName').value;
            var moduleId = document.getElementById('hfmoduleId').value;
            var entityid = document.getElementById('hfEntityid').value;
            $.get("../DataScopeRoles/FieldPrivilegeLoad?RoleId=" + RoleId + "&moduleId=" + moduleId + "&entityid=" + entityid, detailsAdd_callBack);
        }

        function OnChnage(cname, id, ddltype) {
            var control = ddltype + id;
            if (cname.checked == true) {
                if (cname.name != 'ViewCheckbox' + id) {
                    if (document.getElementById('ViewCheckbox' + id) != null) {
                        if (document.getElementById('ViewCheckbox' + id).checked == false) {
                            document.getElementById('ViewCheckbox' + id).checked = true;
                            document.getElementById('ViewField_' + id).disabled = false;
                        }
                    }
                }
                document.getElementById(control).disabled = false;
            }
            else {
                if (cname.name == 'ViewCheckbox' + id) {
                    if (document.getElementById('EditCheckbox' + id) != null) {
                        document.getElementById('EditCheckbox' + id).checked = false;
                        document.getElementById('EditField_' + id).disabled = true;
                    }
                }
                document.getElementById(control).disabled = true;
            }
        }

        function OnPrivilegeLoad() {
            var RoleId = document.getElementById('RoleName').value;
            var moduleId = document.getElementById('hfmoduleId').value;
            window.location = "../DataScopeRoles/ManagePrivilege?roleId=" + RoleId + "&moduleId=" + moduleId;
        }
        function ManageDynamicVariable(obj, rolePrivilegeId) {
            if (rolePrivilegeId != "") {
                var id = $(obj).val();
                var text1 = $(obj).text();

                if ($('#dynDSIds').val().length < 1)
                    return;

                var dynDSIds = $('#dynDSIds').val().split(',');
                var url = '<%=Url.Action("DynamicVariableMapping") %>';
                // remove any <a/> tag
                $('a', $(obj).parent('td')).remove();
                if (id != '' && $.inArray(id, dynDSIds) > -1) {
                    var params = '?rolePrivilegeId=' + rolePrivilegeId + '&datascopeId=' + id;
                    $(obj).parent('td').append('<a title="<%=this.GetLocalResourceObject("DynamicVariables")%>" class="thickbox" href="' + url + params + '">DynamicVar</a>');
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <%ViewData["strEntityid"] = Request.QueryString["entityId"];  %>
    <div class="page-title">
        <a href="#" onclick="OnPrivilegeLoad()" title="<%: this.GetLocalResourceObject("GoBacktoRoleList")%>">
            <i class="icon-custom-left"></i>
        </a>
        <h3><%= ViewData["strEntityid"] %>
            <%=this.GetLocalResourceObject("FieldList")%></h3>
        <div class="pull-right">
            <button class="btn btn-success" title="<%=this.GetLocalResourceObject("SaveRolePrivilege")%>" onclick="javascript:FieldRolePrivilegeDetails()">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %>
            </button>
            <button class="btn btn-default" title="<%=this.GetGlobalResourceObject("General", "Back")%>" onclick="OnPrivilegeLoad()">
                <%=this.GetGlobalResourceObject("General", "Back")%>
            </button>
        </div>
    </div>
    <div class="row-fluid pd-25" id="master">
        <% Html.RenderPartial("PartialFieldPrivilege"); %>
    </div>
</asp:Content>
