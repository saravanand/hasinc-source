<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //This method is used to add role privilege details
        function RolePrivilegeDetails() {
            formDetails = $('#RolePrivilegeForm');
            serializedForm = formDetails.serialize();
            $.post("ManagePrivilege", serializedForm, detailsAdd_callBack);
        }

        function OnSelectedIndexChange() {
            var roleId = document.getElementById('RoleName').value;
            var moduleId = document.getElementById('ModuleName').value;
            $.get("../DataScopeRoles/ManagePrivilegePartialView?roleId=" + roleId + "&moduleId=" + moduleId, detailsAdd_callBack);
        }

        function detailsAdd_callBack(data) {
            $("#master").html(data);
        }

        function GetModuleName() {
            var a = document.getElementById('ModuleName').Text;
            document.getElementById(strmoduleName).value = document.getElementById('ModuleName').Text;
        }
        function OnChnage(cname, id, ddltype) {
            var control = ddltype + id;
            if (cname.checked == true) {
                if (cname.name != 'ViewCheckbox' + id) {
                    if (document.getElementById('ViewCheckbox' + id) != null) {
                        if (document.getElementById('ViewCheckbox' + id).checked == false) {
                            document.getElementById('ViewCheckbox' + id).checked = true;
                            document.getElementById('View_' + id).disabled = false;
                        }
                    }
                }
                document.getElementById(control).disabled = false;
            }
            else {
                if (cname.name == 'ViewCheckbox' + id) {
                    if (document.getElementById('EditCheckbox' + id) != null) {
                        document.getElementById('EditCheckbox' + id).checked = false;
                        document.getElementById('Edit_' + id).disabled = true;
                    }
                    if (document.getElementById('AddCheckbox' + id) != null) {
                        document.getElementById('AddCheckbox' + id).checked = false;
                        document.getElementById('Add_' + id).disabled = true;
                    }
                    if (document.getElementById('DeleteCheckbox' + id) != null) {
                        document.getElementById('DeleteCheckbox' + id).checked = false;
                        document.getElementById('Delete_' + id).disabled = true;
                    }
                }
                document.getElementById(control).disabled = true;
            }
        }

        function OnOthrPrivlgChnage(cname, id, ddltype) {
            var control = ddltype + id;
            if (cname.checked == true)
                document.getElementById(control).disabled = false;
            else
                document.getElementById(control).disabled = true;
        }

        function OnManageField(entityid) {
            var RoleId = document.getElementById('RoleName').value;
            var moduleid = document.getElementById('ModuleName').value;
            window.location = "../DataScopeRoles/FieldPrivilege?roleId=" + RoleId + "&moduleId=" + moduleid + "&entityId=" + entityid;
        }

        function OnRoleLoad() {
            window.location = "../Roles/RoleDetailsList";
        }

        function ManageDynamicVariable(obj, rolePrivilegeId) {
            var id = $(obj).val();
            var text1 = $(obj).text();

            if ($('#dynDSIds').val().length < 1)
                return;

            var dynDSIds = $('#dynDSIds').val().split(',');
            var url = '<%=Url.Action("DynamicVariableMapping") %>';

            // remove any <a/> tag
            $('a', $(obj).parent('td')).remove();

            if (id != '' && $.inArray(id, dynDSIds) > -1) {
                // show the link
                var params = '?rolePrivilegeId=' + rolePrivilegeId + '&datascopeId=' + id;
                $(obj).parent('td').append('<a title="<%=this.GetLocalResourceObject("DynamicVariables") %>" class="thickbox" href="' + url + params + '">Dynamic Variable</a>');
                tb_init('a.thickbox');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("RoleDetailsList","Roles") %>" title="<%: this.GetLocalResourceObject("GoBacktoRoleList")%>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%=this.GetLocalResourceObject("Title") %></h3>
        <div class="pull-right">
            <button class="btn btn-success" title="<%=this.GetLocalResourceObject("SaveRolePrivilege")%>" onclick="javascript:RolePrivilegeDetails()">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %>
            </button>
            <a class="btn btn-default" href="<%=Url.Action("RoleDetailsList", "Roles") %>" title="<%=this.GetGlobalResourceObject("General", "Back")%>">
                <%=this.GetGlobalResourceObject("General", "Back")%>
            </a>
        </div>
    </div>
    <div id="master" class="row-fluid pd-25">
        <% Html.RenderPartial("PartialMangePrivilege"); %>
    </div>
</asp:Content>
