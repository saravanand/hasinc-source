﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%if (ViewData["RolePrivilegeId"] != null && !string.IsNullOrEmpty(ViewData["RolePrivilegeId"].ToString()))
  {  %>
<input type="hidden" value="<%:ViewData["RolePrivilegeId"].ToString() %>" name="rolePrivilegeId" />
<%} %>
<div id="ajaxStatusMessage">
</div>
<div class="algn_dynamicVar">
    <table class="celloTable">
        <tr>
            <th style="width: 100px;">
                <%= this.GetLocalResourceObject("VariableName")%>
            </th>
            <th>
                <%= this.GetLocalResourceObject("Values")%>
            </th>
        </tr>
        <%
            string dynamicVariablesIds = string.Empty;
        %>
        <%
            if (ViewData["DynamicVariableList"] != null
                && ((Dictionary<string, DynamicVariable>)ViewData["DynamicVariableList"]).Count > 0)
            {

                var dynamicVariables1 = (Dictionary<string, DynamicVariable>)ViewData["DynamicVariableList"];
                dynamicVariablesIds = string.Join(",", dynamicVariables1.Keys);
        %>
        <input type="hidden" value="<%=dynamicVariablesIds%>" name="DynamicVariablesIds"
            id="DynamicVariablesIds" />
        <%} %>
        <%
            string strSaveImageUrl = ResolveClientUrl("../../App_Themes/BlueTheme/save.png");
            if (ViewData["DynamicVariableList"] != null
                && ((Dictionary<string, DynamicVariable>)ViewData["DynamicVariableList"]).Count > 0)
            {
                var dynamicVariables = (Dictionary<string, DynamicVariable>)ViewData["DynamicVariableList"];
                var dynamicDataScopeValues = (Dictionary<string, DynamicDatascopeValue>)ViewData["DynamicDatascopeValues"];
                int count = 0;
                foreach (var item in dynamicVariables)
                { 
        %>
        <tr>
            <td style="padding-left: 8px;">
                <b>
                    <%:item.Value.Name%></b><br />
                <%--(<i><%:item.Value.VariableName%></i>)--%>
            </td>
            <td class="halign">
                <% if (item.Value.FieldTypeId == FieldType.MultiSelect)
                   {
                       List<string> selectedValues = null;
                       if (dynamicDataScopeValues != null && dynamicDataScopeValues.Count > 0 && dynamicDataScopeValues.ContainsKey(item.Key))
                       {
                           string dynamicDataScopeValue = dynamicDataScopeValues[item.Key].Value;
                           if (!string.IsNullOrEmpty(dynamicDataScopeValue))
                           {
                               dynamicDataScopeValue = dynamicDataScopeValue.Remove(0, 1);
                               dynamicDataScopeValue = dynamicDataScopeValue.Remove(dynamicDataScopeValue.Length - 1);
                               dynamicDataScopeValue = dynamicDataScopeValue.Replace("','", ",");
                               if (!string.IsNullOrEmpty(dynamicDataScopeValue))
                               {
                                   selectedValues = dynamicDataScopeValue.Split(',').ToList();
                               }
                           }
                %>
                <input type="hidden" value="<%:dynamicDataScopeValues[item.Key].DynamicDatascopeValueId %>"
                    name="datascopeValueId_<%:item.Value.DynamicVariableId %>" />
                <%
                       } %>
                <div id="relationList" class="dynamicvariable-relation">
                    <%
                       if (item.Value.DynamicValues != null && item.Value.DynamicValues.Count > 0)
                       {
                           foreach (var dynamicValue in item.Value.DynamicValues)
                           {
                    %>
                    <div class="dynamicvariable-relselected">
                        <%if (selectedValues != null && selectedValues.Count > 0 && selectedValues.Contains(dynamicValue.Key))
                          {
                        %>
                        <input type="checkbox" style="width: 15px;" checked="checked" name="chk_<%:item.Value.DynamicVariableId %>"
                            value="<%:dynamicValue.Key %>" />
                        <%
                          }
                          else
                          {
                        %>
                        <input type="checkbox" style="width: 15px;" name="chk_<%:item.Value.DynamicVariableId %>"
                            value="<%:dynamicValue.Key %>" />
                        <%
                          } %>
                        <b>
                            <%:dynamicValue.Value%></b></div>
                    <%
                           }
                       }
                    %>
                </div>
                <%
                   } %>
            </td>
        </tr>
        <%}
            } %>
        <tr>
            <td style="padding-left: 8px;">
            </td>
            <td class="halign">
                <div class="green_but">
                    <a href="#" title="<%=this.GetLocalResourceObject("SaveRolePrivilege")%>" onclick="javascript:SaveMultiSelectDatascopeValue();">
                        <span>
                            <%=this.GetGlobalResourceObject("General","Save") %></span> </a>
                </div>
                <div class="green_but">
                    <a href="#" onclick="tb_remove();" title="<%=this.GetGlobalResourceObject("General", "Back")%>"><span>
                        <%=this.GetGlobalResourceObject("General", "Back")%></span> </a>
                </div>
            </td>
        </tr>
    </table>
</div>
