﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<div id="FieldPrivilegeList">
    <% 
        HtmlHelper.ClientValidationEnabled = false;
        HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
        AjaxOptions ajaxOption = new AjaxOptions();
        ajaxOption.UpdateTargetId = "master";
        using (Ajax.BeginForm("FieldPrivilegeDetails", "DataScopeRoles", new { }, ajaxOption, new { id = "FieldPrivilegeFrom" }))
        {
    %>
    <%
           List<string> dynamicDatascope = new List<string>();// new List<string>() { "f48a468b-333e-e011-810a-001ec9dab123", "2964258B-0A30-E011-95D9-001EC9DAB123" };
           //dynamicDatascope = CelloSaaS.ServiceProxies.AccessControlManagement.DataAccessProxy.GetAllDataScope().Select(ds => ds.Id).ToList();

           List<DataScope> dynamicDatascopes = CelloSaaS.ServiceProxies.AccessControlManagement.DataAccessProxy.GetAllDataScope();
           foreach (var dynData in dynamicDatascopes)
           {
               if (dynData.IsDynamic == true)
               {
                   dynamicDatascope.Add(dynData.Id.ToString());
               }
           }
    %>
    <input type="hidden" value="<%= string.Join(",", dynamicDatascope)%>" name="dynDSIds"
        id="dynDSIds" />
    <%= Html.Hidden("hfmoduleId", ViewData["moduleId"])%>
    <%= Html.Hidden("hfEntityid", ViewData["strEntityid"])%>
    <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("success")))
      { %>
    <div class="alert alert-success">
        <%=Html.CelloValidationMessage("success")%>
    </div>
    <%} %>
    <% else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
      { %>
    <div class="alert alert-danger">
        <%= Html.CelloValidationMessage("Error")%>
    </div>
    <%} %>
    <div class="grid simple horizontal purple">
        <div class="grid-body form-container">
            <label class="filter-opt">Role</label>
            <% if (ViewData["Roles"] != null)
               { %>
            <%= Html.DropDownList("RoleName", new SelectList((IEnumerable)ViewData["Roles"], "RoleId", "RoleName", ViewData["SelectedRole"].ToString()), new { onChange = "OnSelectedIndexChange()" ,style="width:200px;" })%>
            <%= Html.ValidationMessage("valRoleName", "*")%>
            <%} %>
        </div>
    </div>
    <% if (ViewData["DicEntityFieldMetaData"] != null && ((IEnumerable<EntityFieldMetaData>)ViewData["DicEntityFieldMetaData"]).Count() > 0)
       {                           
    %>
    <div class="grid simple horizontal purple">
        <div class="grid-body form-container">
            <table class="celloTable">
                <thead>
                    <tr>
                        <th>
                            <%: this.GetLocalResourceObject("FieldName")%>
                        </th>
                        <th class="halign">
                            <%: this.GetLocalResourceObject("Visibility")%>
                        </th>
                        <th class="halign">
                            <%: this.GetLocalResourceObject("Editable")%>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% 
           Dictionary<string, RolePrivilege> dicRolePrvilege = (Dictionary<string, RolePrivilege>)ViewData["RolePrivilege"];
           Dictionary<string, RolePrivilege> privilegeDatascopeId = new Dictionary<string, RolePrivilege>();

           if (dicRolePrvilege != null && dicRolePrvilege.Keys.Count > 0)
           {
               foreach (KeyValuePair<string, RolePrivilege> rolePrivilege in dicRolePrvilege)
                   if (!privilegeDatascopeId.ContainsKey(rolePrivilege.Value.PrivilegeId))
                       privilegeDatascopeId.Add(rolePrivilege.Value.PrivilegeId, rolePrivilege.Value);
           }
           string selectedViewValue = string.Empty;
           string selectedEditValue = string.Empty;
           List<DataScope> lstDataScope = (List<DataScope>)ViewData["DataScope"];

           int count = 0;
           foreach (EntityFieldMetaData FieldMetadata in ((IEnumerable<EntityFieldMetaData>)ViewData["DicEntityFieldMetaData"]))
           {
               bool blViewPrivilege = false, blEditPrivilege = false;

               string viewPermission = "ViewField_" + FieldMetadata.EntityFieldIdentifier;
               string editPermission = "EditField_" + FieldMetadata.EntityFieldIdentifier;

               selectedViewValue = string.Empty;
               selectedEditValue = string.Empty;
               string strViewKey = string.Empty;
               string strEditKey = string.Empty;
               string name = FieldMetadata.Name;
               if (privilegeDatascopeId != null && privilegeDatascopeId.Keys.Count > 0)
               {
                   if (privilegeDatascopeId.Keys.Contains(viewPermission))
                   {
                       selectedViewValue = privilegeDatascopeId[viewPermission].DataScopeId;
                       if (string.IsNullOrEmpty(selectedViewValue))
                           selectedViewValue = "-1";
                       strViewKey = privilegeDatascopeId[viewPermission].Id;
                       blViewPrivilege = true;
                   }
                   if (privilegeDatascopeId.Keys.Contains(editPermission))
                   {
                       selectedEditValue = privilegeDatascopeId[editPermission].DataScopeId;
                       if (string.IsNullOrEmpty(selectedEditValue))
                           selectedEditValue = "-1";
                       strEditKey = privilegeDatascopeId[editPermission].Id;
                       blEditPrivilege = true;
                   }
               }
                    %>
                    <tr class="<%=(count%2==0) ? "even" : "odd" %>">
                        <td>
                            <%= FieldMetadata.Name%>
                        </td>
                        <td class="halign">
                            <%= Html.Hidden("hf" + viewPermission, strViewKey)%>
                            <%= Html.CheckBox("ViewCheckbox" + FieldMetadata.EntityFieldIdentifier, blViewPrivilege, new { onClick = "OnChnage(this,'" + FieldMetadata.EntityFieldIdentifier + "','ViewField_')" })%>
                            <%
               object objViewAttribute = new { style = "width: 200px", onchange = "ManageDynamicVariable(this,'" + strViewKey + "');" };
               if (!blViewPrivilege)
                   objViewAttribute = new { disabled = "true", style = "width: 200px", onchange = "ManageDynamicVariable(this,'" + strViewKey + "');" };
                            %>
                            <%= Html.DropDownList(viewPermission, new SelectList(lstDataScope, "ID", "Name", selectedViewValue.ToString()), objViewAttribute)%>
                            <%= Html.ValidationMessage("val" + viewPermission, "*")%>
                            <%if (dynamicDatascope != null && dynamicDatascope.Contains(selectedViewValue))
                              { %>
                            <%= Html.CelloActionLink("DynamicVar", "DynamicVariableMapping", new { rolePrivilegeId = strViewKey, datascopeId = selectedViewValue }, new { @class = "thickbox", title = "Dynamic Variables" })%>
                            <%
                              } %>
                        </td>
                        <td class="halign">
                            <%= Html.Hidden("hf" + editPermission, strEditKey)%>
                            <%= Html.CheckBox("EditCheckbox" + FieldMetadata.EntityFieldIdentifier, blEditPrivilege, new { onClick = "OnChnage(this,'" + FieldMetadata.EntityFieldIdentifier + "','EditField_')" })%>
                            <%
                              object objEditAttribute = new { style = "width: 200px", onchange = "ManageDynamicVariable(this,'" + strEditKey + "');" };
                              if (!blEditPrivilege)
                                  objEditAttribute = new { disabled = "true", style = "width: 200px", onchange = "ManageDynamicVariable(this,'" + strEditKey + "');" };
                            %>
                            <%= Html.DropDownList(editPermission, new SelectList(lstDataScope, "ID", "Name", selectedEditValue.ToString()), objEditAttribute)%>
                            <%= Html.ValidationMessage("val" + editPermission, "*")%>
                            <%if (dynamicDatascope != null && dynamicDatascope.Contains(selectedEditValue))
                              { %>
                            <%= Html.CelloActionLink("DynamicVar","DynamicVariableMapping", new { rolePrivilegeId = strEditKey, datascopeId = selectedEditValue }, new { @class = "thickbox", title =name+"- Dynamic Variables" })%>
                            <%
                              } %>
                        </td>
                    </tr>
                    <% count++;
           }%>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="pull-right">
    <button class="btn btn-success" title="<%=this.GetLocalResourceObject("SaveRolePrivilege")%>" onclick="javascript:FieldRolePrivilegeDetails()">
        <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %>
    </button>
    <button class="btn btn-default" title="<%=this.GetGlobalResourceObject("General", "Back")%>" onclick="OnPrivilegeLoad()">
        <%=this.GetGlobalResourceObject("General", "Back")%>
    </button>
</div>
<%
       }
       else
       {%>
<div class="alert alert-info">
    <%=this.GetGlobalResourceObject("General", "NoData")%>
</div>
<%}%>
<%}%>
<script>
    $(function () {
        $('#FieldPrivilegeList select').select2({dropdownAutoWidth: true});
    });
</script>
