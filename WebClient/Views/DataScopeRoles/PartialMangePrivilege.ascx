﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<div id="FieldPrivilegeList">
    <% 
        HtmlHelper.ClientValidationEnabled = false;
        HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
        AjaxOptions ajaxOption = new AjaxOptions { UpdateTargetId = "master" };
        using (Ajax.BeginForm("ManagePrivilege", "Role", new { }, ajaxOption, new { id = "RolePrivilegeForm" }))
        {
    %>
    <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("success")))
      { %>
    <div class="alert alert-success">
        <%=Html.CelloValidationMessage(this.GetLocalResourceObject("success").ToString())%>
    </div>
    <% }
      else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("error")))
      { %>
    <div class="alert alert-danger">
        <%=Html.CelloValidationSummary(this.GetLocalResourceObject("ValidationSummary").ToString())%>
    </div>
    <% } %>
    <div class="grid simple horizontal purple">
        <div class="grid-body form-container">
            <table style="width: 100%;">
                <colgroup>
                    <col style="width: 50%;" />
                    <col style="width: 50%;" />
                </colgroup>
                <tbody>
                    <tr>
                        <td>
                            <label for="RoleName">
                                <%=this.GetLocalResourceObject("Role")%></label>
                            <% if (ViewData["Roles"] != null)
                               { %>
                            <%= Html.DropDownList("RoleName", new SelectList((IEnumerable)ViewData["Roles"], "RoleId", "RoleName", ViewData["SelectedRole"].ToString()), new { onChange = "OnSelectedIndexChange()", style="width:100%" })%>
                            <%= Html.CelloValidationMessage("valRoleName", "*")%>
                            <%} %>
                        </td>
                        <td>
                            <label for="ModuleName">
                                <%=this.GetLocalResourceObject("Module")%></label>
                            <% if (ViewData["Modules"] != null)
                               { %>
                            <%= Html.DropDownList("ModuleName", new SelectList((IEnumerable)ViewData["Modules"], "ModuleCode", "ModuleName", ViewData["SelectedModule"].ToString()), new { onChange = "OnSelectedIndexChange()", style="width:100%" })%>
                            <%= Html.CelloValidationMessage("valModuleName", "*")%>
                            <%} %>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <% if (ViewData["DictEntityMetaData"] != null && ((IEnumerable<EntityMetaData>)ViewData["DictEntityMetaData"]).Count() > 0)
       {  
    %>
    <div class="grid simple horizontal blue">
        <div class="grid-body">
            <table class="celloTable">
                <thead>
                    <tr>
                        <th>
                            <%=this.GetLocalResourceObject("th_Entity")%>
                        </th>
                        <th class="halign" style="width: 100px">
                            <%=this.GetLocalResourceObject("th_View")%>
                        </th>
                        <th class="halign" style="width: 100px">
                            <%=this.GetLocalResourceObject("th_Add")%>
                        </th>
                        <th class="halign" style="width: 100px">
                            <%=this.GetLocalResourceObject("th_Edit")%>
                        </th>
                        <th class="halign" style="width: 100px">
                            <%=this.GetLocalResourceObject("th_Delete")%>
                        </th>
                        <th style="width: 160px;">
                            <%=this.GetLocalResourceObject("th_Others")%>
                        </th>
                        <th class="halign" style="width: 100px;">
                            <%=this.GetLocalResourceObject("th_ManageFields")%>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% 
           Dictionary<string, Dictionary<string, EntityPrivilege>> EntityPrivileges = new Dictionary<string, Dictionary<string, EntityPrivilege>>();
           if (ViewData["EntityPrivileges"] != null && ((Dictionary<string, Dictionary<string, EntityPrivilege>>)ViewData["EntityPrivileges"]).Count() > 0)
           {
               EntityPrivileges = (Dictionary<string, Dictionary<string, EntityPrivilege>>)ViewData["EntityPrivileges"];
           }
           Dictionary<string, RolePrivilege> dicRolePrvilege = (Dictionary<string, RolePrivilege>)ViewData["RolePrivilege"];
           Dictionary<string, RolePrivilege> privilegeDatascopeId = new Dictionary<string, RolePrivilege>();

           if (dicRolePrvilege != null && dicRolePrvilege.Keys.Count > 0)
           {
               foreach (KeyValuePair<string, RolePrivilege> rolePrivilege in dicRolePrvilege)
               {
                   if (!privilegeDatascopeId.ContainsKey(rolePrivilege.Value.PrivilegeId))
                       privilegeDatascopeId.Add(rolePrivilege.Value.PrivilegeId, rolePrivilege.Value);
               }
           }

           string selectedViewValue = string.Empty;
           string selectedAddValue = string.Empty;
           string selectedEditValue = string.Empty;
           string selectedDeleteValue = string.Empty;

           string selectedPrivilegeValue = string.Empty;

           //DataScope for each entities
           Dictionary<string, Dictionary<string, DataScope>> dictEntityDataScope = (Dictionary<string, Dictionary<string, DataScope>>)ViewData["EntityDataScope"];
           Dictionary<string, DataScope> dictDataScopes;
           List<string> dynamicDatascope = new List<string>();// new List<string>() { "f48a468b-333e-e011-810a-001ec9dab123", "2964258B-0A30-E011-95D9-001EC9DAB123" };
           //dynamicDatascope = CelloSaaS.ServiceProxies.AccessControlManagement.DataAccessProxy.GetAllDataScope().Select(ds => ds.Id).ToList();

           List<DataScope> dynamicDatascopes = CelloSaaS.ServiceProxies.AccessControlManagement.DataAccessProxy.GetAllDataScope();
           foreach (var dynData in dynamicDatascopes)
           {
               if (dynData.IsDynamic == true)
               {
                   dynamicDatascope.Add(dynData.Id.ToString());
               }
           }

           int count = 0;

           foreach (EntityMetaData EntityMetadata in ((IEnumerable<EntityMetaData>)ViewData["DictEntityMetaData"]))
           {
               string EntityId = EntityMetadata.EntityIdentifier;

               if (!EntityPrivileges.Keys.Contains(EntityId))
               {
                   continue;
               }

               //DataScope for the Entity
               dictDataScopes = new Dictionary<string, DataScope>();
               if (dictEntityDataScope.Keys.Contains(EntityId))
                   dictDataScopes = dictEntityDataScope[EntityId];
               List<DataScope> lstDataScope = new List<DataScope>();
               foreach (DataScope dataScopes in dictDataScopes.Values)
               {
                   lstDataScope.Add(dataScopes);
               }
               lstDataScope.Insert(0, new DataScope { Id = "-1", Name = "- All -" });

               bool blViewPrivilege = false, blEditPrivilege = false, blDeletePrivilege = false, blAddPrivilege = false;

               if (EntityPrivileges.Keys.Contains(EntityId))
               {
                   Dictionary<string, EntityPrivilege> dicEntityPrivilege = EntityPrivileges[EntityId];

                   foreach (string rolePrivilege in dicEntityPrivilege.Keys)
                   {
                       if (rolePrivilege == "View_" + EntityId)
                       {
                           blViewPrivilege = true;
                       }
                       if (rolePrivilege == "Edit_" + EntityId)
                       {
                           blEditPrivilege = true;
                       }
                       if (rolePrivilege == "Delete_" + EntityId)
                       {
                           blDeletePrivilege = true;
                       }
                       if (rolePrivilege == "Add_" + EntityId)
                       {
                           blAddPrivilege = true;
                       }
                   }
               }

               string viewPermission = "View_" + EntityMetadata.EntityIdentifier;
               string addPermission = "Add_" + EntityMetadata.EntityIdentifier;
               string editPermission = "Edit_" + EntityMetadata.EntityIdentifier;
               string deletePermission = "Delete_" + EntityMetadata.EntityIdentifier;

               selectedViewValue = string.Empty;
               selectedAddValue = string.Empty;
               selectedEditValue = string.Empty;
               selectedDeleteValue = string.Empty;

               string strViewKey = string.Empty;
               string strAddKey = string.Empty;
               string strEditKey = string.Empty;
               string strDeleteKey = string.Empty;
               string strPrivilegeKey = string.Empty;

               bool blView = false;
               bool blAdd = false;
               bool blDelete = false;
               bool blEdit = false;

               bool blPrivilege = false;

               if (privilegeDatascopeId != null && privilegeDatascopeId.Keys.Count > 0)
               {
                   if (privilegeDatascopeId.Keys.Contains(viewPermission))
                   {
                       selectedViewValue = privilegeDatascopeId[viewPermission].DataScopeId;
                       if (string.IsNullOrEmpty(selectedViewValue))
                           selectedViewValue = "-1";
                       strViewKey = privilegeDatascopeId[viewPermission].Id;
                       blView = true;
                   }

                   if (privilegeDatascopeId.Keys.Contains(addPermission))
                   {
                       selectedAddValue = privilegeDatascopeId[addPermission].DataScopeId;
                       if (string.IsNullOrEmpty(selectedAddValue))
                           selectedAddValue = "-1";
                       strAddKey = privilegeDatascopeId[addPermission].Id;
                       blAdd = true;
                   }

                   if (privilegeDatascopeId.Keys.Contains(editPermission))
                   {
                       selectedEditValue = privilegeDatascopeId[editPermission].DataScopeId;
                       if (string.IsNullOrEmpty(selectedEditValue))
                           selectedEditValue = "-1";
                       strEditKey = privilegeDatascopeId[editPermission].Id;
                       blEdit = true;
                   }

                   if (privilegeDatascopeId.Keys.Contains(deletePermission))
                   {
                       selectedDeleteValue = privilegeDatascopeId[deletePermission].DataScopeId;
                       if (string.IsNullOrEmpty(selectedDeleteValue))
                           selectedDeleteValue = "-1";
                       strDeleteKey = privilegeDatascopeId[deletePermission].Id;
                       blDelete = true;
                   }
               }        
                    %>
                    <tr class="<%=(count%2==0) ? "even" : "odd" %>">
                        <td>
                            <%= EntityMetadata.EntityIdentifier%>
                        </td>
                        <td class="halign">
                            <% if (blViewPrivilege)
                               { %>
                            <%= Html.Hidden("hf" + viewPermission, strViewKey)%>
                            <%= Html.CheckBox("ViewCheckbox" + EntityId, blView, new { onClick = "OnChnage(this,'" + EntityId + "','View_')" })%>
                            <%
                                   object objViewAttribute = new { style = "width: 100px", onchange = "ManageDynamicVariable(this,'" + strViewKey + "');" };
                                   if (!blView)
                                       objViewAttribute = new { disabled = "true", style = "width: 100px" };
                            %>
                            <%if (lstDataScope.Count > 1)
                              { %>
                            <%= Html.DropDownList(viewPermission, new SelectList(lstDataScope, "ID", "Name", selectedViewValue.ToString()), objViewAttribute)%>
                            <% }
                              else
                              { %>
                            <%=Html.Hidden(viewPermission, lstDataScope[0].Id)%>
                            <%} %>
                            <br />
                            <%if (dynamicDatascope != null && dynamicDatascope.Contains(selectedViewValue))
                              { %>
                            <%= Html.CelloActionLink("DynamicVar", "DynamicVariableMapping", new { rolePrivilegeId = strViewKey, datascopeId = selectedViewValue }, new { @class = "thickbox", title = "Dynamic Variables" })%>
                            <%
                              } %>
                            <% } %>
                        </td>
                        <td class="halign">
                            <% if (blAddPrivilege)
                               { %>
                            <%= Html.Hidden("hf" + addPermission, strAddKey)%>
                            <%= Html.CheckBox("AddCheckbox" + EntityId, blAdd, new { onClick = "OnChnage(this,'" + EntityId + "','Add_')" })%>
                            <%
                                   object objAddAttribute = new { style = "width: 100px", onchange = "ManageDynamicVariable(this,'" + strAddKey + "');" };
                                   if (!blAdd)
                                       objAddAttribute = new { disabled = "true", style = "width: 100px" };
                            %>
                            <%if (lstDataScope.Count > 1)
                              { %>
                            <%= Html.DropDownList(addPermission, new SelectList(lstDataScope, "ID", "Name", selectedAddValue.ToString()), objAddAttribute)%>
                            <%}
                              else
                              { %>
                            <%=Html.Hidden(addPermission, lstDataScope[0].Id)%>
                            <%} %>
                            <br />
                            <%if (dynamicDatascope != null && dynamicDatascope.Contains(selectedAddValue))
                              { %>
                            <%= Html.CelloActionLink("DynamicVar", "DynamicVariableMapping", new { rolePrivilegeId = strAddKey, datascopeId = selectedAddValue }, new { @class = "thickbox", title = "Dynamic Variables" })%>
                            <%
                              }%>
                            <% }%>
                        </td>
                        <td class="halign">
                            <% if (blEditPrivilege)
                               { %>
                            <%= Html.Hidden("hf" + editPermission, strEditKey)%>
                            <%= Html.CheckBox("EditCheckbox" + EntityId, blEdit, new { onClick = "OnChnage(this,'" + EntityId + "','Edit_')" })%>
                            <%

                                   object objEditAttribute = new { style = "width: 100px", onchange = "ManageDynamicVariable(this,'" + strEditKey + "');" };
                                   if (!blEdit)
                                       objEditAttribute = new { disabled = "true", style = "width: 100px" };
                            %>
                            <%if (lstDataScope.Count > 1)
                              { %>
                            <%= Html.DropDownList(editPermission, new SelectList(lstDataScope, "ID", "Name", selectedEditValue.ToString()), objEditAttribute)%>
                            <%}
                              else
                              { %>
                            <%=Html.Hidden(editPermission,lstDataScope[0].Id) %>
                            <%} %>
                            <br />
                            <%if (dynamicDatascope != null && dynamicDatascope.Contains(selectedEditValue))
                              { %>
                            <%= Html.CelloActionLink("DynamicVar", "DynamicVariableMapping", new { rolePrivilegeId = strEditKey, datascopeId = selectedEditValue }, new { @class = "thickbox", title = "Dynamic Variables" })%>
                            <%
                              }%>
                            <% }%>
                        </td>
                        <td class="halign">
                            <% if (blDeletePrivilege)
                               { %>
                            <%= Html.Hidden("hf" + deletePermission, strDeleteKey)%>
                            <%= Html.CheckBox("DeleteCheckbox" + EntityId, blDelete, new { onClick = "OnChnage(this,'" + EntityId + "','Delete_')" })%>
                            <%
                                   object objDeleteAttribute = new { style = "width: 100px", onchange = "ManageDynamicVariable(this,'" + strDeleteKey + "');" };
                                   if (!blDelete)
                                       objDeleteAttribute = new { disabled = "true", style = "width: 100px" };
                            %>
                            <%if (lstDataScope.Count > 1)
                              { %>
                            <%= Html.DropDownList(deletePermission, new SelectList(lstDataScope, "ID", "Name", selectedDeleteValue.ToString()), objDeleteAttribute)%>
                            <%}
                              else
                              { %>
                            <%=Html.Hidden(deletePermission, lstDataScope[0].Id)%>
                            <%} %>
                            <br />
                            <%if (dynamicDatascope != null && dynamicDatascope.Contains(selectedDeleteValue))
                              { %>
                            <%= Html.CelloActionLink("DynamicVar", "DynamicVariableMapping", new { rolePrivilegeId = strDeleteKey, datascopeId = selectedDeleteValue }, new { @class = "thickbox", title = "Dynamic Variables" })%>
                            <%
                              } %>
                            <% }%>
                        </td>
                        <% if (ViewData["OtherEntityPrivilege"] != null && ((Dictionary<string, Dictionary<string, EntityPrivilege>>)ViewData["OtherEntityPrivilege"]).Count() > 0)
                           {%>
                        <td>
                            <%
                               Dictionary<string, Dictionary<string, EntityPrivilege>> dictOtherEntityPrivileges = new Dictionary<string, Dictionary<string, EntityPrivilege>>();
                               dictOtherEntityPrivileges = (Dictionary<string, Dictionary<string, EntityPrivilege>>)ViewData["OtherEntityPrivilege"];
                               if (dictOtherEntityPrivileges.Keys.Contains(EntityMetadata.EntityIdentifier))
                               {
                                   Dictionary<string, EntityPrivilege> dictPrivilege = new Dictionary<string, EntityPrivilege>();
                                   dictPrivilege = dictOtherEntityPrivileges[EntityMetadata.EntityIdentifier];
                                   foreach (EntityPrivilege entityPrivilege in dictPrivilege.Values)
                                   {
                                       selectedPrivilegeValue = string.Empty;
                                       strPrivilegeKey = string.Empty;
                                       blPrivilege = false;

                                       if (privilegeDatascopeId != null && privilegeDatascopeId.Keys.Count > 0)
                                       {
                                           if (privilegeDatascopeId.Keys.Contains(entityPrivilege.PrivilegeId))
                                           {
                                               selectedPrivilegeValue = privilegeDatascopeId[entityPrivilege.PrivilegeId].DataScopeId;
                                               if (string.IsNullOrEmpty(selectedPrivilegeValue))
                                                   selectedPrivilegeValue = "-1";
                                               strPrivilegeKey = privilegeDatascopeId[entityPrivilege.PrivilegeId].Id;
                                               blPrivilege = true;
                                           }
                                       }

                            %>
                            <%= Html.Hidden("hf" + entityPrivilege.PrivilegeId, strPrivilegeKey)%>
                            <%
                                       object objPrivilegeAttribute = new { style = "width: 100px", onchange = "ManageDynamicVariable(this,'" + strPrivilegeKey + "');" };
                                       if (!blPrivilege)
                                       {
                                           objPrivilegeAttribute = new { disabled = "true", style = "width: 100px" };
                                       }
                            %>
                            <%= Html.CheckBox("PrivilegeCheckbox" + entityPrivilege.PrivilegeId, blPrivilege, new { onClick = "OnOthrPrivlgChnage(this,'" + EntityId + "','" + entityPrivilege.PrivilegeId + "')" })%>
                            <%=  entityPrivilege.PrivilegeId%>
                            <%if (lstDataScope.Count > 1)
                              { %>
                            <%= Html.DropDownList(entityPrivilege.PrivilegeId + EntityId, new SelectList(lstDataScope, "ID", "Name", selectedPrivilegeValue.ToString()), objPrivilegeAttribute)%>
                            <%}
                              else
                              { %>
                            <%=Html.Hidden(entityPrivilege.PrivilegeId + EntityId, lstDataScope[0].Id)%>
                            <%} %>
                            <br />
                            <%if (dynamicDatascope != null && dynamicDatascope.Contains(selectedPrivilegeValue))
                              { %>
                            <%= Html.CelloActionLink("DynamicVar", "DynamicVariableMapping", new { rolePrivilegeId = strPrivilegeKey, datascopeId = selectedPrivilegeValue }, new { @class = "thickbox", title = "Dynamic Variables" })%>
                            <% }%>
                            <% }
                               } %>
                        </td>
                        <% }
                           else
                           { %>
                        <td class="halign">
                            <span>
                                <%:this.GetLocalResourceObject("NoPrivilegesAvailable") %></span>
                        </td>
                        <% } %>
                        <td class="halign">
                            <a href="#" onclick="OnManageField('<%= EntityMetadata.EntityIdentifier %>')">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </td>
                    </tr>
                    <% 
                           count++;
           }
                    %>
                </tbody>
            </table>
            <input type="hidden" value="<%= string.Join(",", dynamicDatascope)%>" name="dynDSIds"
                id="dynDSIds" />
        </div>
    </div>
    <%
       }
       else
       {%>
    <div class="alert alert-info">
        <%=this.GetGlobalResourceObject("General", "m_NoData")%>
    </div>
    <%}%>
    <%}%>
</div>
<% if (ViewData["DictEntityMetaData"] != null && ((IEnumerable<EntityMetaData>)ViewData["DictEntityMetaData"]).Count() > 0)
   {  
%>
<div class="pull-right">
    <button class="btn btn-success" title="<%=this.GetLocalResourceObject("SaveRolePrivilege")%>" onclick="javascript:RolePrivilegeDetails()">
        <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %>
    </button>
    <a class="btn btn-default" href="<%=Url.Action("RoleDetailsList", "Roles") %>" title="<%=this.GetGlobalResourceObject("General", "Back")%>">
        <%=this.GetGlobalResourceObject("General", "Back")%>
    </a>
</div>
<% } %>
<script>
    $(function () {
        $('#FieldPrivilegeList select').select2({ dropdownAutoWidth: true });
    });
</script>
