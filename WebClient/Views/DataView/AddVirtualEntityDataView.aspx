﻿<%@ Page Title="<%$Resources:ManageDataView%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%: Url.Action("DataViewList","DataView")%>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%: this.GetLocalResourceObject("AddVirtualEntityDataView")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("h_DataViewDetails")%></h4>
            </header>
            <div class="panel-body">
                <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
                   { %>
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                    <%=Html.CelloValidationSummary()%>
                </div>
                <% } %>
                <% using (Html.BeginForm("AddVirtualEntityDataView", "DataView", FormMethod.Post, new { @id = "AddVirtualEntityDatViewForm" }))
                   {
                %>
                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.MainEntity","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("MainEntity")%>
                                </label>
                                <% if (ViewData["MainEntity"] != null)
                                   { %>
                                <%= Html.DropDownList("DataView.MainEntity", (List<SelectListItem>)ViewData["MainEntity"], new { style = "width:100%" })%>
                                <% } %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.DataViewID","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("DataViewId")%>
                                </label>
                                <%= Html.TextBox("DataView.DataViewID", ViewData["DataViewID"].ToString(), new { maxlength = 50, style = "width:100%;" })%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.Name","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("DataViewName")%>
                                </label>
                                <%= Html.TextBox("DataView.Name", ViewData["DataViewName"].ToString(), new { maxlength = 50, style = "width:100%;" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.Description","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("Description")%>
                                </label>
                                <%= Html.TextArea("DataView.Description", ViewData["DataViewDescription"] !=null?ViewData["DataViewDescription"].ToString() : string.Empty, new { style = "width:100%;" })%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                                    <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Save")%></button>
                                <a class="btn btn-default" href="<%: Url.Action("DataViewList","DataView")%>" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                                    <%=this.GetGlobalResourceObject("General", "Cancel")%></a>
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("select[name='DataView.MainEntity']").select2();
        });
    </script>
</asp:Content>
