<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.ViewManagement.DataViewFieldMetaData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%:Url.Action("DataViewFieldList","DataView",new{dataViewID=ViewData["DataViewID"],entityId=ViewData["EntityID"]}) %>" title="<%:this.GetLocalResourceObject("BackTitle") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <span class="semi-bold"><%:ViewData["DataViewName"] %></span> -
            <%:this.GetLocalResourceObject("Title") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("h_ManageFieldDetails")%></h4>
            </header>
            <div class="panel-body">
                <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
                   { %>
                <div class="alert alert-danger">
                    <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
                </div>
                <%} %>
                <% if (this.Model != null)
                   { %>
                <% using (Html.BeginForm())
                   { %>
                <%=Html.Hidden("GlobalFieldID", this.Model.GlobalFieldID)%>
                <%=Html.Hidden("ID", ViewData["ID"])%>
                <%=Html.Hidden("passedDataViewID", ViewData["DataViewID"])%>
                <%=Html.Hidden("updatedOrdinalList", ViewData["OrdinalList"])%>
                <%=Html.Hidden("passedEntityID", ViewData["EntityID"])%>
                <%=Html.Hidden("passedPickupListID", this.Model.PickupListId)%>
                <%=Html.Hidden("passedOrdinal", this.Model.Ordinal)%>
                <%=Html.Hidden("IsExtendedField", this.Model.IsExtendedField)%>
                <%=Html.Hidden("ViewDataName", this.Model.ViewDataName)%>
                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DisplayName","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%:this.GetLocalResourceObject("DisplayName") %>
                                </label>
                                <% if (this.Model.GlobalFieldID > 0)
                                   {%>
                                <strong>
                                    <%:this.Model.DisplayName%></strong>
                                <% }
                                   else
                                   { %>
                                <%= Html.TextBox("DisplayName", this.Model.DisplayName)%>
                                <%= Html.ValidationMessage("DisplayName", "*")%>
                                <% } %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("IsExtendedField") %>
                                </label>
                                <br />
                                <span>
                                    <%=this.Model.IsExtendedField%></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <%if (this.Model.IsExtendedField)
                                  { %>
                                <label>
                                    <%:this.GetLocalResourceObject("MaxLength")%>
                                </label>
                                <% 
                                      if (this.Model.GlobalFieldID > 0)
                                      {%>
                                <span>
                                    <%=this.Model.MaxLength%></span>
                                <%}
                                      else
                                      {
                                          if ((ViewData["lengthField"] != null && (bool)ViewData["lengthField"]))
                                          {%>
                                <%= Html.TextBox("MaxLength", this.Model.MaxLength, new { onkeypress = "javascript:return onlyNumbers();", maxlength = "5", disabled = "disabled"})%>
                                <%}
                                          else
                                          {%>
                                <%= Html.TextBox("MaxLength", this.Model.MaxLength, new { onkeypress = "javascript:return onlyNumbers();", maxlength = "5"})%>
                                <%}
                                      }
                                %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("IsEditable")%>
                                </label>
                                <br />
                                <% 
                                      if (this.Model.GlobalFieldID > 0)
                                      {%>
                                <%= Html.CheckBox("IsEditable", this.Model.IsEditable, new { disabled = "disabled" })%>
                                <%}
                                      else
                                      { %>
                                <%= Html.CheckBox("IsEditable", this.Model.IsEditable)%>
                                <%}
                                %>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("RegularExpression")%>
                                </label>
                                <% 
                                      if (this.Model.GlobalFieldID > 0)
                                      {%>
                                <span>
                                    <%:this.Model.RegularExpression%></span>
                                <%}
                                      else
                                      {
                                          if ((ViewData["regularExpressionField"] != null && (bool)ViewData["regularExpressionField"]))
                                          {%>
                                <%= Html.TextBox("RegularExpression", this.Model.RegularExpression, new { disabled ="disabled" })%>
                                <%}
                                          else
                                          {%>
                                <%= Html.TextBox("RegularExpression", this.Model.RegularExpression)%>
                                <%}
                                      }
                                %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("IsMandatory")%>
                                </label>
                                <br />
                                <%
                                      if (this.Model.GlobalFieldID > 0 || this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.CheckBox || this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.RadioButton)
                                      {%>
                                <span>
                                    <%=this.Model.IsMandatory%></span>
                                <%}
                                      else
                                      { %>
                                <%= Html.CheckBox("IsMandatory", this.Model.IsMandatory)%>
                                <%}
                                %>
                                <% }
                                %>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Default value
                                </label>
                                <%= Html.TextBox("DefaultValue", this.Model.DefaultValue)%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    Is Hidden
                                </label>
                                <br />
                                <%= Html.CheckBox("IsHidden", this.Model.IsHidden)%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("FieldType")%>
                                </label>
                                <%if ((this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.DropdownList || this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.CascadeDropDownList) && ViewData["PickupListType"] != null)
                                  { %>
                                <%= Html.DropDownList("PickupListType", null, new { style="width:100%;" }) %>
                                <%}
                                  else
                                  { %>
                                <br />
                                <span>
                                    <%:this.Model.FieldType%>
                                </span>
                                <%} %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("IsVisible")%>
                                </label>
                                <br />
                                <% if (this.Model.GlobalFieldID > 0 && !this.Model.IsFixed)
                                   {%>
                                <span>
                                    <%=this.Model.IsVisible%></span>
                                <%}
                                   else
                                   { %>
                                <%= Html.CheckBox("IsVisible", this.Model.IsVisible)%>
                                <%}
                                %>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <% if (this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.DropdownList || this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.CascadeDropDownList)
                           {%>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("ParentField")%>
                                </label>
                                <%= Html.DropDownList("ParentField", null, new { style="width:100%;" })%>
                                <%= Html.CelloValidationMessage("valParentField", "*")%>
                                <%if (ViewData["ControlFieldID"] != null)
                                  { %>
                                <%= Html.Hidden("ControlFieldID",ViewData["ControlFieldID"]) %>
                                <%} %>
                                <%= Html.Hidden("CascadeDropDownListType",(int)CelloSaaS.Model.ViewManagement.FieldType.CascadeDropDownList) %>
                                <%= Html.Hidden("DropDownListType",(int)CelloSaaS.Model.ViewManagement.FieldType.DropdownList) %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("PickupList")%>
                                </label>
                                <br />
                                <span>
                                    <%=ViewData["PickupListName"] ?? "--" %>
                                </span>
                            </div>
                        </div>
                        <%}%>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("Description")%>
                                </label>
                                <% if (this.Model.GlobalFieldID > 0)
                                   {%>
                                <span>
                                    <%:this.Model.Description%></span>
                                <%}
                                   else
                                   { %>
                                <%= Html.TextArea("Description", this.Model.Description, 5, 30, null)%>
                                <%} %>
                            </div>
                        </div>
                        <%if (this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.DropdownList || this.Model.FieldType == CelloSaaS.Model.ViewManagement.FieldType.CascadeDropDownList)
                          { %>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("IsMultiSelect")%>
                                </label>
                                <br />
                                <%= Html.CheckBox("IsMultiLine",(bool)ViewData["IsMultiLine"]) %>
                            </div>
                        </div>
                        <%} %>
                    </div>
                </div>
                <% } %>
                <br />
                <div class="pull-right">
                    <a class="btn btn-default" href="<%:Url.Action("DataViewFieldList","DataView",new{dataViewID=ViewData["DataViewID"],entityId=ViewData["EntityID"]}) %>"
                        title="<%:this.GetLocalResourceObject("BackTitle")%>">
                        <%=this.GetGlobalResourceObject("General","Cancel") %></a>
                    <a class="btn btn-info" href="#" title="<%=this.GetGlobalResourceObject("General","Update") %>" onclick="$('form').submit();">
                        <%=this.GetGlobalResourceObject("General","Update") %></a>
                </div>
                <% } %>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;

        }
        $(document).ready(function () {
            SetParentPickupListVisible();
            $("#PickupListType").change(function () {
                SetParentPickupListVisible();
            });
        });
        function SetParentPickupListVisible() {
            $.ajaxSettings.cache = false;
            //Get Pickup list field type
            var fieldType = document.getElementById("PickupListType");
            if (fieldType != null && fieldType != "") {
                var cascadeValue = document.getElementById("CascadeDropDownListType").value;
                var parentField = document.getElementById("ParentField");
                var typeValue = document.getElementById("PickupListType").value;
                if (typeValue != "" && typeValue == cascadeValue) {
                    parentField.disabled = false;

                    //Get data view field meta data pickup list id
                    var pickupListId = document.getElementById("passedPickupListID").value;
                    //Get control field id control
                    var controlField = document.getElementById("ControlFieldID");
                    var entityId = document.getElementById("passedEntityID").value;
                    var controlFieldId = "";
                    //Get Control field id value
                    if (controlField != null && controlField != "")
                        controlFieldId = controlField.value;
                    if (pickupListId != "") {
                        $.getJSON("/DataView/GetParentFieldDetails",
                        { entityId: entityId, fieldType: typeValue, childPickupListId: pickupListId, controlFieldId: controlFieldId },
                        function (data) {
                            if (data == "NoRelation" || data == "Error") {
                                ShowErrorMessage(data);
                            }
                            else {
                                $('#ParentField option').remove();
                                for (var count = 0; count < data.length; count++) {
                                    var parentValue = data[count].Text.split(",");
                                    if (data[count].Selected == true)
                                        $('#ParentField').append($("<option value=" + parentValue[0] + " selected> " + parentValue[1] + "</option>"));
                                    else
                                        $('#ParentField').append($("<option value=" + parentValue[0] + "> " + parentValue[1] + "</option>"));
                                }
                            }
                        });
                    }
                    else
                        ShowErrorMessage("");
                }
                else
                    parentField.disabled = true;
            }
        }

        function ShowErrorMessage(data) {
            var fieldType = document.getElementById("PickupListType");
            for (var count = 0; count < fieldType.length; count++) {
                if (fieldType[count].value == document.getElementById("DropDownListType").value) {
                    var parentField = document.getElementById("ParentField");
                    fieldType[count].selected = true;
                    parentField.disabled = true;
                    if (data == "Error")
                        alert('<%:this.GetLocalResourceObject("e_parentfielddetails")%>');
                    else
                        alert('<%:this.GetLocalResourceObject("e_nopickuplist")%>');
                    break;
                }
            }

        }
    </script>
</asp:Content>
