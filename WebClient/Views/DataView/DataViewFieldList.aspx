<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="MvcContrib.UI.Grid.ActionSyntax" %>
<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<asp:Content ID="headcnt" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;
        $(document).ready(function () {
            filterTable = $('#dataList').dataTable({
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "bFilter": true,
                "aaSorting": [[0, "asc"]],
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-2] }]
            });

            $(document).on('keypress', '#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });

            $('#reorderFieldListModal').modal({ show: false });

            $('#btnReorder').click(function () {
                $('#reorderFieldListModal .modal-body').load($(this).attr('href'), function (data) {
                    $('#reorderFieldListModal').modal('show');
                });
                return false;
            });

            $('#btnReorderFields').click(function () {
                $('form#frmReorderDataViewFields').submit();
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <a href="<%:Url.Action("DataViewList","DataView") %>" title="<%:this.GetLocalResourceObject("BackTitle") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <span class="semi-bold"><%:ViewData["DataViewName"]%></span> - Fields
        </h3>
        <div class="pull-right">
            <% if (ViewData["ExtnSettingValue"].ToString() == ConfigurationSettingValueConstant.OWN_SETTING && !string.IsNullOrWhiteSpace(ViewData["OrdinalList"] as string))
               { %>
            <a id="btnReorder" class="btn btn-warning" title="<%:this.GetLocalResourceObject("ReorderTitle") %>" href="DataViewFieldListReorder?weight=200&height=300&dataViewId=<%=ViewData["DataViewID"]%>&entityId=<%=ViewData["EntityID"]%>">
                <%=this.GetGlobalResourceObject("General", "ReOrder")%></a>
            <% } %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("SuccessMessage")))
                  { %>
                <div class="alert alert-success">
                    <%=Html.CelloValidationMessage("SuccessMessage")%>
                </div>
                <%} %>
                <div class="grid-part">
                    <% Html.RenderPartial("DataViewFieldListDetails"); %>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="reorderFieldListModal" tabindex="-1" role="dialog" aria-labelledby="reorderFieldListModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 700px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title"><%: this.GetLocalResourceObject("h_ReorderFields")%></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close") %></button>
                        <button type="button" id="btnReorderFields" class="btn btn-primary">
                            <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
