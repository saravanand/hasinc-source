<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<% if (ViewData["IsDeleted"] != null && Convert.ToBoolean(ViewData["IsDeleted"]))
   {
%>
<div class="alert alert-success">
    <%:this.GetLocalResourceObject("ExcludeMessage") %>
</div>
<% } %>
<% if (ViewData["FieldList"] != null && ((List<CelloSaaS.Model.ViewManagement.DataViewFieldMetaData>)ViewData["FieldList"]).Count > 0)
   {
       Html.Grid((List<CelloSaaS.Model.ViewManagement.DataViewFieldMetaData>)ViewData["FieldList"]).Columns(
          column =>
          {
              //column.For(col => col.FieldID).Named(this.GetLocalResourceObject("FieldID").ToString()); 
              column.For(col => col.DisplayName).Named(this.GetLocalResourceObject("DisplayName").ToString());
              column.For(col => col.FieldType).Named(this.GetLocalResourceObject("FieldType").ToString());
              column.For(col => col.Ordinal)
                  .Named(this.GetLocalResourceObject("Ordinal").ToString())
                  .HeaderAttributes(@class => "halign").Attributes(@class => "halign");
              column.For(col => col.IsVisible).Named(this.GetLocalResourceObject("IsVisible").ToString());
              column.For(col => col.IsExtendedField).Named(this.GetLocalResourceObject("IsExtendedField").ToString());
              column.For(col => (col.IsExtendedField) ? col.IsEditable.ToString() : "-").Named(this.GetLocalResourceObject("IsEditable").ToString());
              if (ViewData["ExtnSettingValue"].ToString() == ConfigurationSettingValueConstant.OWN_SETTING)
              {
                  column.For(col => (col.GlobalFieldID == 0) ? Html.CelloActionLink((col.ID > 0) ? "EditDataViewFieldDetail" : this.GetLocalResourceObject("Include").ToString(), "DataViewFieldDetails", new { modal = "true", fieldID = col.FieldID, ordinalList = ViewData["OrdinalList"], dataViewID = ViewData["DataViewID"], exId = col.ID, fieldType = col.FieldType, entityID = ViewData["EntityID"], globalFieldID = col.GlobalFieldID, newField = col.TenantID }, new { @class = "" }).Replace("EditDataViewFieldDetail", "<i class='fa fa-pencil'></i>") : "-")
                      .Named(this.GetLocalResourceObject("EditField").ToString())
                      .HeaderAttributes(@class => "halign")
                      .Attributes(@class => "halign")
                      .DoNotEncode();
                  column.For(col => (col.IsExtendedField && col.ID > 0) ? Html.CelloActionLink(this.GetLocalResourceObject("Exclude").ToString(), "DeleteExtendedField", new { deleteFieldID = col.FieldID, id = col.ID, passedDataViewID = ViewData["DataViewID"], passedEntityID = ViewData["EntityID"] }, new { onclick = "javascript:return confirm('" + this.GetLocalResourceObject("DeleteMsg").ToString() + "')" }) : "-")
                      .Named(this.GetLocalResourceObject("Exclude").ToString())
                      .HeaderAttributes(@class => "halign")
                      .Attributes(@class => "halign")
                      .DoNotEncode();
              }
          }).Attributes(id => "dataList", @class => "celloTable").Render();                       
%>
<%}
   else
   { %>
<div class="alert alert-info">
    <%:this.GetLocalResourceObject("NoRecords") %>
</div>
<%} %>
