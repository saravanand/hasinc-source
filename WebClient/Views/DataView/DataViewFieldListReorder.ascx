<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="divReorderDataViewFields">
    <script type="text/javascript">
        function MoveUp(currId) {
            var IdArray = document.getElementById('IDs').value.split(',');
            var currIndex = $.inArray(currId, IdArray);
            if (currIndex > 0)
                SwapItems(currIndex, currIndex - 1, IdArray);
            return false;
        }

        function MoveDown(currId) {
            var IdArray = document.getElementById('IDs').value.split(',');
            var currIndex = $.inArray(currId, IdArray);
            var arrayLength = IdArray.length;
            if (currIndex + 1 < arrayLength)
                SwapItems(currIndex, currIndex + 1, IdArray);
            return false;
        }

        function SwapItems(FirstIndex, SecondIndex, IdArray) {
            var arrayLength = IdArray.length;
            var currId = IdArray[FirstIndex];
            var prevId = IdArray[SecondIndex];

            // Get the html content based on ID
            var currValue = $("#" + currId).html();
            var prevValue = $("#" + prevId).html();
            // Swap the div content
            $("#" + prevId).html(currValue);
            $("#" + currId).html(prevValue);
            // Swap the div ID
            $("#" + prevId).attr("id", "temp");
            $("#" + currId).attr("id", prevId);
            $("#temp").attr("id", currId);
            // swap the items in array
            var temp = IdArray[FirstIndex];
            IdArray[FirstIndex] = IdArray[SecondIndex];
            IdArray[SecondIndex] = temp;
            // Save back the item in the hidden field
            document.getElementById('IDs').value = IdArray.toString();
        }
    </script>
    <% using (Ajax.BeginForm("DataViewFieldListReorder", "DataView", new AjaxOptions { LoadingElementId = "loading", UpdateTargetId = "divReorderDataViewFields" }, new { id = "frmReorderDataViewFields" }))
       { %>
    <%=Html.Hidden("passedDataViewId", ViewData["DataViewID"])%>
    <%=Html.Hidden("passedEntityId", ViewData["EntityID"])%>
    <% if (Html.CelloValidationMessage("ExceptionMessage") == null || string.IsNullOrEmpty(Html.CelloValidationMessage("ExceptionMessage")))
       {
           if (Model != null)
           { %>
    <%=Html.Hidden("IDs", ViewData["FieldIds"])%>
    <%
               var fields = (IEnumerable<CelloSaaS.Model.ViewManagement.DataViewFieldMetaData>)Model;
               int currIndex = 0;
               foreach (CelloSaaS.Model.ViewManagement.DataViewFieldMetaData field in fields)
               {
                   if (field.ID > 0)
                   {
    %>
    <div class="row" id="<%=field.ID%>" style="border-bottom: 1px solid #eee;margin:0px;">
        <div class="col-md-2 text-center">
            <a href="#" onclick="MoveUp('<%=field.ID%>')">
                <i class="fa fa-2x fa-caret-up"></i>
            </a>
        </div>
        <div class="col-md-8 text-center">
            <%=field.FieldID%>
        </div>
        <div class="col-md-2 text-center">
            <a href="#" onclick="MoveDown('<%=field.ID%>')">
                <i class="fa fa-2x fa-caret-down"></i>
            </a>
        </div>
    </div>
    <% 
                       currIndex++;
                   }
               }
    %>
    <%} %>
    <%}
       else
       {
           Html.CelloValidationMessage("ExceptionMessage");
       }
       } %>
</div>
