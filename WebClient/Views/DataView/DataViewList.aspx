<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.DataManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("Title") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-6 m-b-xs pull-right">
                        <div class="pull-right">
                            <%  if (UserIdentity.HasPrivilege(PrivilegeConstants.AddVirtualEntity))
                                { %>
                            <a class="btn btn-success" id="AddVirtualEntityDataViewButton" href="<%= Url.Action("AddVirtualEntityDataView","DataView") %>" title="<%=this.GetGlobalResourceObject("General","Add")%>">
                              <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Create") %></a>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <% Html.RenderPartial("StatusMessage"); %>
                <% if (Html.ValidationSummary() != null)
                   { %>
                <div class="alert alert-danger">
                    <%= Html.ValidationSummary()%>
                </div>
                <% } %>
                <%  if (this.Model != null)
                    {
                        var allEntityMetaDatalist = ViewData["AllEntityMetadata"] != null ? ViewData["AllEntityMetadata"] as Dictionary<string, EntityMetaData> : new Dictionary<string, EntityMetaData>();

                        Html.Grid((IEnumerable<CelloSaaS.Model.ViewManagement.DataView>)Model).Columns(
                             column =>
                             {
                                 column.For(col => col.Name).Named(this.GetLocalResourceObject("Name").ToString());
                                 column.For(col => col.Description).Named(this.GetLocalResourceObject("Description").ToString());

                                 column.For(col => allEntityMetaDatalist.ContainsKey(col.MainEntity) ? allEntityMetaDatalist[col.MainEntity].EntityName : col.MainEntity).Named(this.GetLocalResourceObject("MainEntity").ToString());

                                 //column.For(col => col.MainEntity).Named(this.GetLocalResourceObject("MainEntity").ToString());
                                 column.For(col => Html.CelloActionLink("Show Fields", "DataViewFieldList", new { dataViewID = col.DataViewID, string.Empty}).Replace("Show Fields", "<i class='fa fa-wrench'></i>"))
                                     .Named(this.GetLocalResourceObject("ManageField").ToString())
                                     .Attributes(@class => "halign")
                                     .HeaderAttributes(@class => "halign").Attributes(@class => "halign")
                                     .DoNotEncode();

                                 column.For(col => (!string.IsNullOrEmpty(col.TenantId) && UserIdentity.HasPrivilege(PrivilegeConstants.AddVirtualEntity))
                                     ? Html.CelloActionLink("EditIcon", "EditVirtualEntityDataView", new { dataViewId = col.DataViewID, mainEntity = col.MainEntity, tenantId = col.TenantId, name = col.Name, description = col.Description })
                                     .Replace("EditIcon", "<i class='fa fa-pencil'></i>")
                                     : "-")
                                .Named(this.GetLocalResourceObject("EditVirtualEntityDataView").ToString())
                                .HeaderAttributes(@class => "halign noSortCol")
                                .Attributes(@class => "halign")
                                .DoNotEncode();

                                 column.For(col => (!string.IsNullOrEmpty(col.TenantId) && UserIdentity.HasPrivilege(PrivilegeConstants.DeleteVirtualEntity))
                                     ? Html.CelloActionLink("DeleteIcon", "DeleteVirtualEntityDataView", new { dataViewId = col.DataViewID, mainEntity = col.MainEntity, tenantId = col.TenantId, dataViewName = col.Name }, new { onclick = "javascript:return confirm('" + string.Format(this.GetLocalResourceObject("m_confirmMessage").ToString(), col.Name) + "')" })
                                     .Replace("DeleteIcon", "<i class='fa fa-trash-o'></i>")
                                     : "-")
                                .Named(this.GetLocalResourceObject("DeleteVirtualEntityDataView").ToString())
                                .HeaderAttributes(@class => "halign noSortCol")
                                .Attributes(@class => "halign")
                                .DoNotEncode();

                             }).Attributes(id => "dataList", @class => "celloTable").Render();
                    }
                    else
                    {%>
                <div class="alert alert-info">
                    <%:this.GetLocalResourceObject("NoRecords") %>
                </div>
                <%} %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;
        $(document).ready(function () {
            filterTable = $('#dataList').dataTable({
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "bFilter": true,
                "aaSorting": [[0, "asc"]],
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1] }]
            });
            $(document).on('keypress', '#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }
    </script>
</asp:Content>
