﻿<%@ Page Title="<%$Resources:ManageDataView%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.ViewManagement.DataViewMetaData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%: Url.Action("DataViewList","DataView")%>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%: this.GetLocalResourceObject("EditVirtualEntityDataView")%></h3>
    </div>
    <% using (Html.BeginForm("EditVirtualEntityDataView", "DataView", FormMethod.Post, new { @id = "EditVirtualEntityDatViewForm" }))
       {
    %>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("h_EditDataView")%></h4>
            </header>
            <div class="panel-body">
                <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("VirtualEntityStatusMessage")))
                   { %>
                <div class="alert alert-danger">
                    <%=Html.CelloValidationMessage("VirtualEntityStatusMessage")%>
                </div>
                <%} %>
                <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
                   { %>
                <div class="alert alert-danger">
                    <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
                </div>
                <%} %>
                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.MainEntity","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("MainEntity")%>
                                </label>
                                <br />
                                <%:Model.DataView.MainEntity %>
                                <%= Html.Hidden("DataView.MainEntity")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.DataViewID","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("DataViewId")%>
                                </label>
                                <br />
                                <%:Model.DataView.DataViewID %>
                                <%= Html.Hidden("DataView.DataViewID")%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.Name","*") != null ? "has-error" : "" %>">
                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("DataViewName")%>
                                </label>
                                <%= Html.TextBox("DataView.Name", Model.DataView.Name, new { maxlength = 50 })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("DataView.Name","*") != null ? "has-error" : "" %>">

                                <label class="mandatory">
                                    <%: this.GetLocalResourceObject("Description")%>
                                </label>
                                <%= Html.TextBox("DataView.Description", Model.DataView.Description, new { maxlength = 100, style = "width:100%;" })%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Save")%></button>
                            <a class="btn btn-default" href="<%: Url.Action("DataViewList","DataView")%>" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                                <%=this.GetGlobalResourceObject("General", "Cancel")%></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <% } %>
</asp:Content>
