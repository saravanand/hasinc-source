﻿<%@ Page Title="<%$ Resources:DatabaseAnalyticsResource,t_DatabaseAnalytics %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.DbAnalytics.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        HtmlHelper.ClientValidationEnabled = false;
        HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
    %>
    <div id="loading"></div>

    <div class="page-title">
        <h3><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","t_DatabaseAnalytics") %></h3>
    </div>
    <%
        var tenantList = ViewData["TenantList"] as Dictionary<string, TenantDetails>;
    %>

    <div class="row-fluid pd-25">

        <% if (Html.ValidationMessage("Error") != null)
           { %>
        <div class="alert alert-danger danger">
            <%=Html.ValidationMessage("Error")%>
        </div>
        <% }
           if (tenantList == null || tenantList.Count <= 0)
           { %>
        <div class="alert alert-info"><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","e_Tenant") %></div>
        <%  }
           else
           {
               var tenantSelectList = tenantList.Values.OrderBy(x => x.TenantName).Select(x => new SelectListItem { Text = x.TenantName, Value = x.TenantCode }).ToList();
        %>
        <div class="content-box row">
            <div class="col-md-6">
                <label style="float: left; line-height: 30px; margin-right: 10px;">
                    <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_Tenant") %></label>

                <%=Html.DropDownList("viewTenantId", tenantSelectList, new { style="width:70%;" })%>
            </div>
            <%=Html.Hidden("filterId",DbAnalyticsFilter.Weeks) %>
           
             <div class="col-md-6">
                <div class="pull-right">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-sm btn-success active " id="btnWeek">
                            <input type="radio" name="Filters" checked="checked"><%:DbAnalyticsFilter.Weeks %>
                        </label>
                        <label class="btn btn-sm btn-success" id="btnDays">
                            <input type="radio" name="Filters"><%:DbAnalyticsFilter.Days %>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div id="dataBaseAnalyticsDiv">
            <%Html.RenderPartial("PartialDatabaseAnalytics"); %>
        </div>
        <% } 
        %>
        <script type="text/javascript">
            var canTrigger = true;
        </script>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        $(function () {
            $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

            $(document).on('click', '#btnWeek', function () {
                document.getElementById('filterId').value = '<%:DbAnalyticsFilter.Weeks%>';
                SearchDatabaseAnalytics();
                return true;
            });

            $(document).on('click', '#btnDays', function () {
                document.getElementById('filterId').value = '<%:DbAnalyticsFilter.Days%>';
                SearchDatabaseAnalytics();
                return true;
            });

            $('select[name=viewTenantId]').change(function (e) {
                SearchDatabaseAnalytics();
            });

            $(document).on('click', '#btnDbAnalyticsSearch', function (w) {

                var serverNames = $('select[name=ServerNames]').val();
                var databaseName = $('select[name=DbNames]').val();

                if (!serverNames) {
                    $('#statusMessage').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>Please select server name.</div>').fadeIn();
                }
                else if (!databaseName) {
                    $('#statusMessage').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>Please select server name.</div>').fadeIn();
                }
                else {
                    var viewTenantId = $('select[name=viewTenantId]').val();
                    var filters = $('#filterId').val();
                    var postdata = $('#frmSearch').serialize();
                    postdata += '&tenantId=' + viewTenantId;
                    postdata += '&filter=' + filters;
                    $.post('<%=Url.Action("SearchDatabaseAnalytics","DatabaseAnalytics") %>', postdata, function (data) {
                        $('#PreviewDatabaseAnalyticsDiv').html(data);
                    });
                    $('#statusMessage').empty();
                    $('#statusMessage').fadeOut();
                }
                return false;
            });

            $('select[name=viewTenantId]').select2();

            if (canTrigger)
                $('select[name=viewTenantId]').trigger('change');

        });

    function SearchDatabaseAnalytics() {
        var viewTenantId = $('select[name=viewTenantId]').val();
        var filter = $('#filterId').val();
        $.post('/DatabaseAnalytics/Index?tenantId=' + viewTenantId + '&serverName=All&databaseName=All&filter=' + filter, function (data) {
            $('#dataBaseAnalyticsDiv').html(data);
        });
    }

    </script>
    <link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.common.min.css" rel="stylesheet" />
    <link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.default.min.css" rel="stylesheet" />
    <link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.dataviz.min.css" rel="stylesheet" />
    <link href="http://cdn.kendostatic.com/2014.2.716/styles/kendo.dataviz.default.min.css" rel="stylesheet" />
    <script src="http://cdn.kendostatic.com/2014.2.716/js/kendo.all.min.js"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopMenu" runat="server">
</asp:Content>



