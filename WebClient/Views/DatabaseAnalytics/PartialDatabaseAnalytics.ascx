﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaS.DbAnalytics.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>

<% if (Html.ValidationMessage("Error") != null)
   { %>
<div class="alert alert-danger danger">
    <%=Html.ValidationMessage("Error")%>
</div>
<% }
   else
   {
       var serverNameListItem = ViewBag.ServerListItem as List<SelectListItem>;

       if (serverNameListItem != null)
       {
           double startTime = ViewBag.analyticsStartDate != null ? ViewBag.analyticsStartDate : 0;
           double endTime = ViewBag.analyticsEndDate != null ? ViewBag.analyticsEndDate : 0;
           string startDate = string.Format("{0}d", startTime);
           string endDate = string.Format("{0}d", endTime);
           DbAnalyticsFilter filter = DbAnalyticsFilter.Weeks;
           
           if (ViewBag.Filter != null && ViewBag.Filter is DbAnalyticsFilter)
           {
               filter = ViewBag.Filter;
           }
%>

<div class="row pd-25">
    <section class="panel purple">

        <header class="panel-heading">
            <h4><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_Filter") %></h4>
        </header>

        <div class="panel-body">
            <div id="statusMessage"></div>
            <form id="frmSearch" name="frmSearch" method="post">

                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_ServerName") %></label>

                                <%=Html.DropDownList("ServerNames", serverNameListItem, new { style="width:100%;" ,@multiple="multiple"})%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_DatabaseName") %></label>
                                <%
           var databaseItem = ViewBag.databaseItem as IEnumerable<SelectListItem> ?? new List<SelectListItem>() { new SelectListItem { Text = "ALL", Value = "All", Selected = true } };
                                %>
                                <%: Html.DropDownList("DbNames", databaseItem, new { style="width:100%;",@multiple="multiple" })%>
                            </div>
                        </div>
                    </div>

                    <%if (filter.Equals(DbAnalyticsFilter.Days) || filter.Equals(DbAnalyticsFilter.Days))
                      {%>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>From</label>
                                <%=Html.TextBox("StartDate",DateTime.Now.Date.AddDays(startTime).ToShortDateString(), new { @class = "datetime form-control", @style="width:100%", @readonly="readonly" })%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>To</label>
                                <%=Html.TextBox("EndDate", DateTime.Now.Date.AddDays(endTime).ToShortDateString(), new { @class = "datetime form-control",@style="width:100%" , @readonly="readonly"})%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-4 pd-0">
                                    <%= Html.DropDownList("Metrics", (IEnumerable<SelectListItem>)ViewData["DbAnalyticsMetrics"], new { style = "width: 100%;" })%>
                                </div>
                                <div class="col-md-4" style="padding-right: 0px;">
                                    <%= Html.DropDownList("SearchOperand", (IEnumerable<SelectListItem>)ViewData["DbAnalyticsOperands"], new { style = "width: 100%;" })%>
                                </div>
                                <div class="col-md-4" style="padding-right: 0px;">
                                    <%=Html.TextBox("SearchValue", "",new { placeHolder="In millSecond", style="height: 34px;",onkeypress="return isNumberKey(event)" } ) %>
                                </div>

                            </div>
                        </div>
                    </div>
                    <%}%>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <button class="btn btn-info" id="btnDbAnalyticsSearch"><i class="fa fa-search"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Search") %></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
<div class="row pd-25">
    <div id="PreviewDatabaseAnalyticsDiv">
        <%Html.RenderPartial("PreviewDatabaseAnalytics"); %>
    </div>
</div>

<script>

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $(function () {

       

        $('#ServerNames').select2();
        $("#DbNames").select2();
        $('#Metrics').select2();
        $("#SearchOperand").select2();

        $("select[name=Metrics]").change(function () {
            var metrics = $(this).val();

            switch (metrics) {
                case '<%=DbAnalyticsMetrics.CpuTime%>':
                    $("#SearchValue").attr("placeholder", "In millSecond").blur();
                    break;
                case '<%=DbAnalyticsMetrics.MemoryUsage%>':
                    $("#SearchValue").attr("placeholder", "In Kb").blur();
                    break;
                default:
                    $("#SearchValue").attr("placeholder", "In Count").blur();
                    break;
            }
        });

        $("#StartDate").datepicker({
            startDate: '<%=startDate%>',
            endDate: '<%=endDate%>',
            autoClose: true,
            format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
        });


        $("#EndDate").datepicker({
            startDate: '<%=startDate%>',
            endDate: '<%=endDate%>',
            autoClose: true,
            format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>",
            autoclose: true,
        });


        $("select[name=DbNames]").change(function () {
            var dbName = $(this).val();

            if (dbName && dbName.length > 1) {
                if ($.inArray("All", dbName) >= 0) {
                    var new_data = $.grep($('select[name=DbNames]').select2('data'), function (value) {
                        return value['id'] != "All";
                    });
                    $('select[name=DbNames]').select2('data', new_data);
                }
                $('#statusMessage').empty();
                $('#statusMessage').fadeOut();
            }
        });

        //get database name by server.
        $("select[name=ServerNames]").change(function () {

            var serverName = $(this).val();


            if (serverName && serverName.length > 1) {
                if ($.inArray("All", serverName) >= 0) {
                    serverName.splice(serverName.indexOf('All'), 1)

                    var new_data = $.grep($('select[name=ServerNames]').select2('data'), function (value) {
                        return value['id'] != "All";
                    });
                    $('select[name=ServerNames]').select2('data', new_data);
                }
            }

            var url = '/DataBaseAnalytics/GetDatabaseName';

            $("select[name='DbNames']").empty();
            $("select[name='DbNames']").trigger('change');

            var details = { "serverName": serverName, "tenantId": $('select[name=viewTenantId]').val(), "filter": $('#filterId').val() }

            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: url,
                data: JSON.stringify(details),
                success: function (data) {
                    var selectListHtml = '';

                    if (data && !data.Error) {
                        for (i = 0; i < data.length; i++) {
                            selectListHtml += data[i].Selected == true ? '<option value="' + data[i].Value + '" selected=true' : '<option value="' + data[i].Value + '"';
                            selectListHtml += '>' + data[i].Text + '</option>';
                        }
                        $("select[name='DbNames']").html(selectListHtml);
                    }
                    $("select[name='DbNames']").trigger('change');
                },
                failure: function (response) {
                    $("select[name='DbNames']").html(response);
                    $("select[name='DbNames']").trigger('change');
                }
            });
            $('#statusMessage').empty();
            $('#statusMessage').fadeOut();

        });

    });
</script>
<% }
       else
       {%>
<div class="alert alert-info"><%:this.GetGlobalResourceObject("General","m_NoData") %></div>
<% }%>
<%} %>


