﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%@ Import Namespace="CelloSaaS.DbAnalytics.Model" %>
<% if (Html.ValidationMessage("Error") != null)
   { %>
<div class="alert alert-danger danger">
    <%=Html.ValidationMessage("Error")%>
</div>
<% }
   else
   {%>

<%var model = ViewBag.AnalyticsDetails != null ? ViewBag.AnalyticsDetails as Dictionary<DateTime, CelloSaaS.DbAnalytics.Model.AnalyticsMetrics> : null;%>

<%if (model == null || model.Count < 1)
  {%>
<div class="alert alert-info"><%:this.GetGlobalResourceObject("General","m_NoData") %></div>
<%}
  else
  { %>

<%model = model.OrderByDescending(c => c.Key).ToDictionary(c => c.Key, c => c.Value);
  DbAnalyticsFilter filter = DbAnalyticsFilter.Weeks;
  if (ViewBag.Filter != null && ViewBag.Filter is DbAnalyticsFilter)
  {
      filter = ViewBag.Filter;
  }
  int minusDays = 6;
  double sweepCount = model.Count;
  string formatString = "MM/dd/yyyy";
  if (DbAnalyticsFilter.Days.Equals(filter))
  {
      minusDays = 1;
  }
  else if (DbAnalyticsFilter.Hours.Equals(filter))
  {
      sweepCount = CelloSaaS.Library.Helpers.ConfigHelper.SweepCount;
      formatString = "HH:mm";
  }
  TimeSpan times = new TimeSpan(23, 59, 59); 
%>
<section class="panel green">
    <header class="panel-heading">
        <h4><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","h_AnalyticsDashboard") %></h4>
    </header>

    <div class="panel-body" style="position: relative;">
        <% 
  var totalLogicalReads = (model.Values.Sum(c => c.LogicalReads) / sweepCount);
  var totalWrites = (model.Values.Sum(c => c.Writes) / sweepCount);
  var totalReads = (model.Values.Sum(c => c.Reads) / sweepCount);
  var totalConnection = (model.Values.Sum(c => c.TotalConnection) / sweepCount);
  var newConnection = (model.Values.Sum(c => c.NewConnection) / sweepCount);
  var memoryUsage =(model.Values.Sum(c => c.MemoryUsage) / sweepCount);
  var cpuTime = (model.Values.Sum(c => c.CpuTime) / sweepCount);
        %>

        <div class="row">
            <div class="col-md-3">
                <div class="tiles purple">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_CpuTime") %>
                        </div>
                        <div class="heading">
                            <%:Math.Round(cpuTime,3)%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles red">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_MemoryUsage") %>
                        </div>
                        <div class="heading">
                            <%:Math.Round(memoryUsage,3) %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles green">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_TotalConnection") %>
                        </div>
                        <div class="heading">
                            <%:Math.Round(totalConnection,3) %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles orange">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_NewConnection") %>
                        </div>
                        <div class="heading">
                            <%:Math.Round(newConnection,3) %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3">
                <div class="tiles blue">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_Writes") %>
                        </div>
                        <div class="heading">
                            <%:Math.Round(totalWrites,3) %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles indigo">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_Read") %>
                        </div>
                        <div class="heading">
                            <%:Math.Round(totalReads,3) %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles lt-green">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_LogicalReads") %>
                        </div>
                        <div class="heading">
                            <%:Math.Round(totalLogicalReads,3) %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="panel green">
    <header class="panel-heading">
        <h4><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","h_AnalyticsDetails") %>
        </h4>
        <div class="pull-right">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-sm btn-default active btnChart">
                    <input type="radio" name="options" checked="checked"><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_Chart") %>
                </label>
                <label class="btn btn-sm btn-default btnTable">
                    <input type="radio" name="options"><%:this.GetGlobalResourceObject("DatabaseAnalyticsResource","lbl_Table") %>
                </label>
            </div>
        </div>
    </header>
    <div class="panel-body" style="position: relative;">
        <div class="row">
            <div class="col-md-12 analytics_Table">
                <% Html.Grid(model.Values).Columns(
                column =>
                {
                    if (DbAnalyticsFilter.Weeks.Equals(filter))
                    {
                        column.For(c => c.AnalyticsDate.AddDays(-minusDays)).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_StartDate").ToString());
                        column.For(c => c.AnalyticsDate.Add(times)).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_EndDate").ToString());
                    }
                    else
                    {
                        column.For(c => c.AnalyticsDate).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_AnalyticsDate").ToString());
                    }
                    column.For(c => c.CpuTime).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_CpuTime").ToString());
                    column.For(c => c.MemoryUsage).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_MemoryUsage").ToString());
                    column.For(c => c.TotalConnection).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_TotalConnection").ToString());
                    column.For(c => c.NewConnection).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_NewConnection").ToString());
                    column.For(c => c.Reads).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_Read").ToString());
                    column.For(c => c.Writes).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_Writes").ToString());
                    column.For(c => c.LogicalReads).Named(this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_LogicalReads").ToString());

                }).Attributes(id => "dataList", @class => "celloTable").Render();
                %>
            </div>

            <div class="col-md-12 analytics_Map">
                <%  var temp = new List<dynamic>();
                    foreach (var x in model.Values)
                    {
                        temp.Add(new
                        {
                            CpuTime = x.CpuTime,
                            MemoryUsage = x.MemoryUsage,
                            NewConnection = x.NewConnection,
                            TotalConnection = x.TotalConnection,
                            Reads = x.Reads,
                            Writes = x.Writes,
                            LogicalReads = x.LogicalReads,
                            EndDate = x.AnalyticsDate.ToString(formatString),
                            startDate = x.AnalyticsDate.AddDays(minusDays).ToString(formatString),
                        });

                    }
                    string chartData = Newtonsoft.Json.JsonConvert.SerializeObject(temp, Newtonsoft.Json.Formatting.None);
                %>

                <%:Html.Hidden("Analytics_WeekData", chartData) %>

                <div id="analyticsWeekChart">
                    <div class="demo-section k-content">
                        <div id="weekChart"></div>
                    </div>
                </div>

                <script>
                    $(function () {
                        $('.analytics_Table').hide();
                        $('.btnChart').click(function () {
                            $('.analytics_Map').show();
                            $('.analytics_Table').hide();
                        });

                        $('.btnTable').click(function () {
                            $('.analytics_Map').hide();
                            $('.analytics_Table').show();
                        });
                    });
                    if ($('#Analytics_WeekData').length > 0) {
                        var Tenant_dist_data = JSON.parse($('#Analytics_WeekData').val());
                        var stocksDataSource = new kendo.data.DataSource({
                            data: Tenant_dist_data,

                            sort: {
                                field: "EndDate",
                                dir: "asc"
                            },

                            schema: {
                                model: {
                                    fields: {
                                        date: {
                                            type: "date"
                                        }
                                    }
                                }
                            }
                        });

                        function createChart() {
                            $("#weekChart").kendoChart({
                                title: { text: "Database Analytics" },
                                dataSource: stocksDataSource,
                                series: [{
                                    type: "line",
                                    field: "CpuTime",
                                    name: '<%:this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_CpuTime")%>',
                                    color: "#736086"
                                },
                                {
                                    type: "line",
                                    field: "MemoryUsage",
                                    name: '<%:this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_MemoryUsage")%>',
                                    color: "Red"
                                },
                                {
                                    type: "line",
                                    field: "TotalConnection",
                                    name: '<%:this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_TotalConnection")%>',
                                    color: "#0AA699"
                                },
                                {
                                    type: "line",
                                    field: "NewConnection",
                                    name: '<%:this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_NewConnection")%>',
                                    color: "#FCC44D"
                                },
                                {
                                    type: "line",
                                    field: "Writes",
                                    name: '<%: this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_Writes").ToString()%>',
                                    color: "#0090D9"
                                },
                                {
                                    type: "line",
                                    field: "Reads",
                                    name: '<%:this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_Read").ToString()%>',
                                    color: "#695EA5"
                                }, {
                                    type: "line",
                                    field: "LogicalReads",
                                    name: '<%:this.GetGlobalResourceObject("DatabaseAnalyticsResource", "lbl_LogicalReads")%>',
                                    color: "#A6D87A"
                                }],
                                legend: {
                                    position: "bottom"
                                },
                                valueAxis: {
                                    labels: {
                                        format: "{0}"
                                    }
                                },
                                tooltip: {
                                    visible: true,
                                    shared: true,
                                },
                                categoryAxis: {
                                    field: "EndDate",
                                    baseUnit: "weeks"

                                }
                            });
                        }

                        $(document).ready(createChart);
                        $(document).bind("kendo:skinChange", createChart);

                    }
                </script>
            </div>

        </div>
    </div>
</section>
<%} %>

<%} %>
