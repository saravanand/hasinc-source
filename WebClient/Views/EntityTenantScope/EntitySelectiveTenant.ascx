﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<script type="text/javascript">
    function EntitySelectiveTenant() {
        formDetails = $('#EntitySelectiveTenantPage');
        serializedForm = formDetails.serialize();
        $.post("UpdateEntitySelectiveTenant", serializedForm, detailsAdd_callBack);
    }
    function detailsAdd_callBack(data) {
        $("#EntitySelectiveTenetPageId").html(data);
    }   
</script>
<div id="EntitySelectiveTenetPageId">
    <% AjaxOptions ajaxOption = new AjaxOptions { UpdateTargetId = "EntitySelectiveTenantPage" };
       using (Ajax.BeginForm("EntitySelectiveTenant", "EntityTenantScope", new { }, ajaxOption, new { id = "EntitySelectiveTenantPage" }))
       {
          Dictionary<string,TenantDetails> tenantDetails=(Dictionary<string,TenantDetails>)ViewData["TenantDetailsDic"];
          List<string> tenantid=(List<string>)ViewData["SelectedTenantIdList"];
    %>
    <%=Html.Hidden("EntityId") %>
    <%=Html.Hidden("TenantId") %>
    <%=Html.Hidden("EntitySelectiveTenant") %>
    <table cellspacing="0" cellpadding="0" width="100%" class="container-admin">
        <tr>
            <td>
                <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("success")))
                  { %>
                <div class="notification-bg">
                    <div id="msg-success">
                        <div class="header">
                            <img src="../../App_Themes/CelloSkin/scs-top1.gif" alt="" class="left" />
                            <img src="../../App_Themes/CelloSkin/scs-top2.gif" alt="" class="right" />
                        </div>
                        <div class="content">
                            <span class="success1">
                                <%=Html.CelloValidationMessage("success")%></span>
                        </div>
                        <div class="footer">
                            <img src="../../App_Themes/CelloSkin/scs-btm1.gif" alt="" class="left" />
                            <img src="../../App_Themes/CelloSkin/scs-btm2.gif" alt="" class="right" />
                        </div>
                    </div>
                </div>
                <%} %>
                <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("error")))
                   { %>
                <div class="notification-bg">
                    <div id="msg-failure">
                        <div class="header">
                            <img src="../../App_Themes/CelloSkin/fail-top1.gif" alt="" class="left" />
                            <img src="../../App_Themes/CelloSkin/fail-top2.gif" alt="" class="right" />
                        </div>
                        <div class="content">
                            <span class="error">
                                <%=Html.CelloValidationSummary(this.GetLocalResourceObject("summary").ToString(), new { @class = "exception" })%>
                        </div>
                        <div class="footer">
                            <img src="../../App_Themes/CelloSkin/fail-btm1.gif" alt="" class="left" />
                            <img src="../../App_Themes/CelloSkin/fail-btm2.gif" alt="" class="right" />
                        </div>
                    </div>
                </div>
                <%} %>
            </td>
        </tr>
        <% int i=0;
            if(tenantDetails!=null  && tenantDetails.Count>0){
             foreach (var tenant in tenantDetails)
               {           
        %>
        <%if(i==0 || i==3){ %>
        <tr>
        <%} %>
            <td class="grid-row-alt-left">
                <%= Html.CheckBox("Chk_" + tenant.Value.Identifier,tenantid.Contains(tenant.Value.Identifier)?true:false,new {id="Chk_"+tenant.Value.Identifier})%> 
                <%= tenant.Value.TenantName%>
            </td>  
          <%if(i==0 || i==3){ 
                i=0;%>            
        </tr>
        <%} %>
        <% i++;}
           %>
        <tr>
        <td align="right">
                <div class="imageAlign">
                    <img src="<%= this.ResolveClientUrl("../../App_Themes/CelloSkin/btnsave.png")%>"
                        alt="Save" onclick="javascript:EntitySelectiveTenant()" />                    
                </div>
            </td>
        </tr>
        <%}
         else{%>
        <tr>
            <td colspan="3">
                <table class="emptyGrid">
                    <tr>
                        <td>
                            <%: this.GetLocalResourceObject("nodata") %>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%} %>
    </table>
</div>
