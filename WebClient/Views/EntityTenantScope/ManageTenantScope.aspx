﻿<%@ Page Title="<%$Resources:ManageEntityTenantScope%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('select').select2();

            LoadPopUp();
            OnSelectedIndexChange();
        });

        function LoadPopUp() {
            $("#EntitySelectiveTenantPageId").dialog({
                modal: true,
                height: 400,
                width: 500,
                autoOpen: false
            });
        }

        function EntityTenantScopeDetails() {
            formDetails = $('#TenantScopePage');
            serializedForm = formDetails.serialize();
            $.post("UpdateTenantScope", serializedForm, detailsAdd_callBack);
        }

        function detailsAdd_callBack(data) {
            data = data + '<div id="EntityTenantScopeDetails"></div>';
            $("#master").html(data);
        }
        function OnSelectedIndexChange() {
            var tenantId = $('#TenantName').val();
            //            var moduleId = $('#ModuleName').val();
            var moduleId = '-1';

            if (tenantId && moduleId)
                $.get("../EntityTenantScope/PartialManageTenantScope?tenantId=" + tenantId + "&moduleId=" + moduleId, detailsAdd_callBack);
        }
        function OnSelected(id) {
            // debugger;
            var a = $('#' + id).val();
            var tenantId = $('#TenantName').val();
            var selectedTenants = $('#hdn' + id.substring(3)).val();
            if (a == "98654139-dc8f-e111-95b3-000c29c8e241") {
                $.ajax({
                    type: "GET",
                    url: "../EntityTenantScope/PopulateTenant",
                    data:
                          "&tenantId=" + tenantId.toString() +
                          "&selectedTenetIds=" + selectedTenants.toString() +
                          "&entityId=" + id.toString(),
                    success: function (msg) {
                        if (msg != null && msg != "") {
                            $("#EntitySelectiveTenantPageId").html(msg);
                            $("#EntitySelectiveTenantPageId").dialog("open");
                        }
                        else {

                        }
                    },
                    error: function (msg) { }
                });

                $('div', $('#' + id).parent()).show();
            }
            else {
                $('div', $('#' + id).parent()).hide();
                $('input[type=hidden]', $('#' + id).parent()).val('');
            }
        }
        function EntitySelectiveTenant() {
            var checkedTenantIds = [];
            $('#EntitySelectiveTenantPageId input[type=checkbox]:checked').each(function () {
                checkedTenantIds.push($(this).val());
            });
            if (checkedTenantIds == null || checkedTenantIds.length == 0) {
                alert("Please select atleast one tenant");
                var hiddenId = $('#EntitySelectiveTenantPageId #Selected').val();
                $('#' + hiddenId).val('');
            }
            else {
                var hiddenId = $('#EntitySelectiveTenantPageId #Selected').val();
                $('#' + hiddenId).val("'" + checkedTenantIds.join("','") + "'");
                $("#EntitySelectiveTenantPageId").dialog("close");
            }
        }         
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="center_container">
        <div class="heading_container">
            <h1>
                <%: this.GetLocalResourceObject("ManageEntityTenantScope")%></h1>
            <div class="inner_hold">
                <% if (ViewData["TenantList"] != null && ViewData["Modules"] != null)
                   { %>
                <div class="green_but">
                    <a href="#" title="<%: this.GetLocalResourceObject("SavePackageDetails")%>" onclick="javascript:EntityTenantScopeDetails();">
                        <span>
                            <%=this.GetGlobalResourceObject("General","Save") %></span></a>
                </div>
                <%}%>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="grid-part">
            <% AjaxOptions ajaxOption = new AjaxOptions { UpdateTargetId = "partialPageId" };
               using (Ajax.BeginForm("UpdateTenantScope", "EntityTenantScope", new { }, ajaxOption, new { id = "TenantScopePage" }))
               {
            %>
            <% if (ViewData["TenantList"] != null && ViewData["Modules"] != null)
               { %>
            <div class="box1" style="padding: 5px; width: 98.8%;">
                <table class="details_container_table">
                    <tbody>
                        <tr>
                            <td>
                                <span class="filter-opt"><%: this.GetLocalResourceObject("Tenant")%></span>
                                <%= Html.DropDownList("TenantName", new SelectList((IEnumerable)ViewData["TenantList"], "TenantCode", "TenantName", ViewData["SelectedTenantId"].ToString()), new { onChange = "OnSelectedIndexChange()" })%>
                            </td>
                            <%--<td>
                                <span class="filter-opt">Module</span>
                                <%= Html.DropDownList("ModuleName", new SelectList((IEnumerable)ViewData["Modules"], "ModuleCode", "ModuleName", ViewData["SelectedModuleId"].ToString()), new { onChange = "OnSelectedIndexChange()" })%>
                                <%= Html.CelloValidationMessage("valModuleName", "*")%>
                            </td>--%>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clear">
            </div>
            <% }
               else
               { %>
            <div class="info">
                <%: this.GetLocalResourceObject("Nochildtenant")%></div>
            <%} %>
            <div id="master">
                <div id="EntityTenantScopeDetails">
                </div>
            </div>
            <%}%>
        </div>
    </div>
    <div id="EntitySelectiveTenantPageId" title="Available Tenants" class="PopUp" style="display: none">
    </div>
</asp:Content>
