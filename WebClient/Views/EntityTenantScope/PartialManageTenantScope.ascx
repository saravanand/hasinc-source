﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<div id="partialPageId">
    <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("success")))
      { %>
    <div class="success">
        <%=Html.CelloValidationMessage("success")%>
    </div>
    <%} %>
    <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("error")))
       { %>
    <div class="error">
        <%=Html.CelloValidationSummary(this.GetLocalResourceObject("summary").ToString(), new { @class = "exception" })%>
    </div>
    <%} %>
    <% if (ViewData["DictEntityMetaData"] != null && ((IEnumerable<EntityMetaData>)ViewData["DictEntityMetaData"]).Count() > 0)
       {
           Dictionary<string, EntityTenantScope> entityTenantScopeList = new Dictionary<string, EntityTenantScope>();
           if (ViewData["EntityTenantScopeList"] != null && ((Dictionary<string, EntityTenantScope>)ViewData["EntityTenantScopeList"]).Count > 0)
           {
               entityTenantScopeList = (Dictionary<string, EntityTenantScope>)ViewData["EntityTenantScopeList"];
           }
    %>
    <table class="celloTable details_container_table" cellspacing="1" cellpadding="3">
        <thead>
            <tr>
                <th style="width: 260px;">
                    <%: this.GetLocalResourceObject("Entity")%>
                </th>
                <th style="width: 260px;">
                    <%: this.GetLocalResourceObject("ViewTenantScope")%>
                </th>
                <th style="width: 260px;">
                    <%: this.GetLocalResourceObject("EditTenantScope")%>
                </th>
                <th style="width: 260px;">
                    <%: this.GetLocalResourceObject("DeleteTenantScope")%>
                </th>
            </tr>
        </thead>
        <tbody>
            <% 
           foreach (EntityMetaData EntityMetadata in ((IEnumerable<EntityMetaData>)ViewData["DictEntityMetaData"]))
           {           
            %>
            <tr>
                <td>
                    <%= EntityMetadata.EntityIdentifier%>
                </td>
                <td>
                    <% if (entityTenantScopeList.ContainsKey(EntityMetadata.EntityIdentifier))
                       { %>
                    <%= Html.DropDownList("ddl_view_" + EntityMetadata.EntityIdentifier, new SelectList((IEnumerable)ViewData["TenantScopeList"], "Id", "Name", entityTenantScopeList[EntityMetadata.EntityIdentifier].ViewTenantScopeId), new { id = "ddl_view_" + EntityMetadata.EntityIdentifier, onChange = "OnSelected(id);" })%>
                    <%= Html.Hidden("hdn_view_" + EntityMetadata.EntityIdentifier,entityTenantScopeList[EntityMetadata.EntityIdentifier].ViewSelectiveTenant)%>
                    <%}
                       else
                       { %>
                    <%= Html.DropDownList("ddl_view_" + EntityMetadata.EntityIdentifier, new SelectList((IEnumerable)ViewData["TenantScopeList"], "Id", "Name"), new { id = "ddl_view_" + EntityMetadata.EntityIdentifier, onChange = "OnSelected(id);" })%>
                    <%= Html.Hidden("hdn_view_" + EntityMetadata.EntityIdentifier)%>
                    <%} %>
                    <% string displayValue = "none";
                       if (entityTenantScopeList.ContainsKey(EntityMetadata.EntityIdentifier)
                           && (entityTenantScopeList[EntityMetadata.EntityIdentifier].ViewTenantScopeId) == "98654139-dc8f-e111-95b3-000c29c8e241")
                       {
                           displayValue = "block";
                       } %>
                    <div style="float: right;display: <%=displayValue%>">
                        <img src="../../App_Themes/CelloSkin/manageaccess.gif"
                            alt="<%: this.GetLocalResourceObject("Configure")%>" onclick="OnSelected('ddl_view_<%= EntityMetadata.EntityIdentifier %>');" />
                    </div>
                </td>
                <td>
                    <% if (entityTenantScopeList.ContainsKey(EntityMetadata.EntityIdentifier))
                       { %>
                    <%= Html.DropDownList("ddl_edit_" + EntityMetadata.EntityIdentifier, new SelectList((IEnumerable)ViewData["TenantScopeList"], "Id", "Name", entityTenantScopeList[EntityMetadata.EntityIdentifier].EditTenantScopeId), new { id = "ddl_edit_" + EntityMetadata.EntityIdentifier, onChange = "OnSelected(id);" })%>
                    <%= Html.Hidden("hdn_edit_" + EntityMetadata.EntityIdentifier, entityTenantScopeList[EntityMetadata.EntityIdentifier].EditSelectiveTenant)%>
                    <%}
                       else
                       { %>
                    <%= Html.DropDownList("ddl_edit_" + EntityMetadata.EntityIdentifier, new SelectList((IEnumerable)ViewData["TenantScopeList"], "Id", "Name"), new { id = "ddl_edit_" + EntityMetadata.EntityIdentifier, onChange = "OnSelected(id);" })%>
                    <%= Html.Hidden("hdn_edit_" + EntityMetadata.EntityIdentifier)%>
                    <%} %>
                    <% displayValue = "none";
                       if (entityTenantScopeList.ContainsKey(EntityMetadata.EntityIdentifier)
                           && (entityTenantScopeList[EntityMetadata.EntityIdentifier].EditTenantScopeId) == "98654139-dc8f-e111-95b3-000c29c8e241")
                       {
                           displayValue = "block";
                       } %><div style="float: right;display: <%=displayValue%>">
                           <img src="../../App_Themes/CelloSkin/manageaccess.gif" 
                               alt="<%: this.GetLocalResourceObject("Configure")%>" onclick="OnSelected('ddl_edit_<%= EntityMetadata.EntityIdentifier %>');" />
                       </div>
                </td>
                <td>
                    <% if (entityTenantScopeList.ContainsKey(EntityMetadata.EntityIdentifier))
                       { %>
                    <%= Html.DropDownList("ddl_delete_" + EntityMetadata.EntityIdentifier, new SelectList((IEnumerable)ViewData["TenantScopeList"], "Id", "Name", entityTenantScopeList[EntityMetadata.EntityIdentifier].DeleteTenantScopeId), new { id = "ddl_delete_" + EntityMetadata.EntityIdentifier, onChange = "OnSelected(id);" })%>
                    <%= Html.Hidden("hdn_delete_" + EntityMetadata.EntityIdentifier, entityTenantScopeList[EntityMetadata.EntityIdentifier].DeleteSelectiveTenant)%>
                    <%}
                       else
                       { %>
                    <%= Html.DropDownList("ddl_delete_" + EntityMetadata.EntityIdentifier, new SelectList((IEnumerable)ViewData["TenantScopeList"], "Id", "Name"), new { id = "ddl_delete_" + EntityMetadata.EntityIdentifier, onChange = "OnSelected(id);" })%>
                    <%= Html.Hidden("hdn_delete_" + EntityMetadata.EntityIdentifier)%>
                    <%} %>
                    <% displayValue = "none";
                       if (entityTenantScopeList.ContainsKey(EntityMetadata.EntityIdentifier)
                           && (entityTenantScopeList[EntityMetadata.EntityIdentifier].DeleteTenantScopeId) == "98654139-dc8f-e111-95b3-000c29c8e241")
                       {
                           displayValue = "block";
                       } %>
                    <div style="float: right;display: <%=displayValue%>">
                        <img src="../../App_Themes/CelloSkin/manageaccess.gif" 
                            alt="<%: this.GetLocalResourceObject("Configure")%>" onclick="OnSelected('ddl_delete_<%= EntityMetadata.EntityIdentifier %>');" />
                    </div>
                </td>
            </tr>
            <% }%>
        </tbody>
    </table>
    <% }
       else
       {%>
    <div class="info">
        <%: this.GetLocalResourceObject("nodata")%>
    </div>
    <%}%>
</div>
