﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.AccessControlManagement.EntityTenantScope>" %>
<% int i = 3;
   string entityId = ViewData["EntityId"].ToString();
   string selectedEntityId = ViewData["EntityId"].ToString().Substring(3);
   string[] entityArr = entityId.Split('_');
   selectedEntityId = "hdn" + selectedEntityId;
   var tenantDetails = ViewData["TenantDetails"] as Dictionary<string, CelloSaaS.Model.TenantManagement.TenantDetails>;
   var entityTenantScope = this.Model;
   string entityDataScope = string.Empty;
   List<string> entityDataScopeTenantList = new List<string>();
   string selectedTenantIds=ViewData["SelectedTenantDetails"].ToString();
   entityDataScopeTenantList = selectedTenantIds.Split(',').ToList();    
%>
<% if(tenantDetails != null && tenantDetails.Count > 0){
       %>
       <table cellspacing="0" cellpadding="0" class="details_container_table">
    <% foreach (var tenant in tenantDetails)
       {
           if (i == 3)
           {%>
    <tr>
        <%}
           i--;
        %>
        <td>
            <% if (entityDataScopeTenantList.Contains("'" + tenant.Value.Identifier + "'"))
               {%>
            <input type="checkbox" name="Chk_<%=tenant.Value.Identifier%>" checked="checked"
                style="margin-right: 5px;" value="<%=tenant.Value.Identifier%>" />
            <%= tenant.Value.TenantName %>
            <%}
               else
               {%>
            <input type='checkbox' name="Chk_<%= tenant.Value.Identifier%>" value="<%=tenant.Value.Identifier%>"
                style="margin-right: 5px;" />
            <%= tenant.Value.TenantName %>
            <% } %>
        </td>
        <%if (i == 0)
          { %>
    </tr>
    <%
              i = 3;
          }
       }
    %>
</table>
<div class="inner_hold">
    <input type='hidden' name="Selected" id="Selected" value="<%=selectedEntityId %>" />
    <div class="green_but">
        <a href="#" onclick="EntitySelectiveTenant()"><span><%: this.GetLocalResourceObject("Update") %></span></a>
    </div>
</div>
       <%}
   else
   { %>
<div class="info">
    <%: this.GetLocalResourceObject("nodata") %></div>
<% } %>

