﻿<%@ Page Title="Access Denied" Language="C#" MasterPageFile="~/Views/Shared/ErrorMaster.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="error-container">
        <div class="error-main">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 scrollable">
                    <div class="span4 error-container error-description">
                       <i class="fa fa-lock"></i>&nbsp;Access is denied
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
