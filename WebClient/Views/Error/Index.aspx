﻿<%@ Page Title="Error" Language="C#" MasterPageFile="~/Views/Shared/ErrorMaster.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="error-container">
        <div class="error-main">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 scrollable">
                    <% Html.RenderPartial("~/Views/Error/PartialIndex.ascx"); %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
