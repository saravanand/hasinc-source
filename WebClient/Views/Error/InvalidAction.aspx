<%@ Page Title="Invalid Action Name" Language="C#" Inherits="System.Web.Mvc.ViewPage"
    MasterPageFile="~/Views/Shared/ErrorMaster.Master" %>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="content-admin">
        <div style="margin-top: 20px; padding: 10px;">
            <div class="error">
                <% if (ViewData["ErrorMessage"] != null)
                   {
                %>
                <%: ViewData["ErrorMessage"].ToString()%>
                <%
                   }
                %>
            </div>
        </div>
    </div>
</asp:Content>
