<%@ Page Title="Page Not Found" Language="C#" Inherits="System.Web.Mvc.ViewPage"
    MasterPageFile="~/Views/Shared/ErrorMaster.Master" %>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="error-container " style="height: 612px; left: 50%;">
        <div class="error-main">
            <div class="error-number">404 </div>
            <div class="span4 error-container error-description">We seem to have lost you in the clouds. </div>
            <div class="error-description-mini">The page your looking for is not here </div>
            <br>
            <div class="row-fluid">
                <%--<div class="input-with-icon span12">
                        <i class="fa fa-search"></i>
                        <input id="form1Name" class="span12" type="text" name="form1Name">
                    </div>--%>
                <a class="btn btn-primary btn-cons" href="/">Return to Home!</a>
            </div>
            <%--<br>
                <button class="btn btn-primary btn-cons" type="button">Search</button>--%>
        </div>
    </div>
</asp:Content>
