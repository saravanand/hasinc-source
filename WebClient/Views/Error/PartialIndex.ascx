﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Web.Mvc.HandleErrorInfo>" %>
<div class="span4 error-container error-description">
    Error has occured while processing your request.
</div>
<div class="divide-20"></div>
<div class="error-description-mini">
    <%
        if (Model != null && HttpContext.Current.IsDebuggingEnabled)
        {
    %>
    <div class="text-left">
        <p><b>Exception:</b></p>
        <p><%:Model.Exception.Message%></p>

        <% if (!string.IsNullOrWhiteSpace(Model.ControllerName))
           { %>
        <p><b>Controller:</b></p>
        <p><%:Model.ControllerName%></p>
        <% } %>

        <% if (!string.IsNullOrWhiteSpace(Model.ActionName))
           { %>
        <p><b>Action:</b></p>
        <p><%:Model.ActionName%></p>
        <% } %>

        <p><b>StackTrace:</b></p>
        <div style="overflow: auto; text-align: left; max-height: 450px;">
            <pre>
<%:Model.Exception.StackTrace %>
            </pre>
        </div>
    </div>
    <%
        }
    %>
</div>
<div class="row-fluid">
    <a class="btn btn-primary btn-cons" href="/">Return to Home!</a>
</div>
