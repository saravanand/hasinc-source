﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Event>>" %>
<%@ Import Namespace="CelloSaaS.EventScheduler.EventPublishingEngine" %>
<%@ Import Namespace="MvcContrib.Pagination" %>
<%@ Import Namespace="CelloSaaS.View" %>
<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.EventScheduler.Model" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% Html.RenderPartial("StatusMessage"); %>
<%  if (Model != null && Model.Count() > 0)
    {
        bool paginate = Convert.ToBoolean(this.Request["paginate"] ?? "True");
        EventAuditSearchCondition condition = new EventAuditSearchCondition();
        if (ViewData["eventAuditSearchCondition"] != null)
        {
            condition = (EventAuditSearchCondition)ViewData["eventAuditSearchCondition"];
        }
        string sortDirection = (string)ViewData["SortDirection"];
        string sortString = (string)ViewData["SortString"];
        int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
        int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
        int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;

        var routeValues = new
            {
                EventIds = condition.EventIds,
                ActivityLogId = condition.ActivityLogId,
                LogTime = condition.LogTime,
                ToLogTime = condition.ToLogTime,
                FromLogTime = condition.FromLogTime,
                EventId = condition.EventId,
                ActivityEventLogId = condition.ActivityEventLogId,
                EventStatus = condition.EventStatus,
                SubjectId = condition.SubjectId,
                SubjectType = condition.SubjectType,
                TargetType = condition.TargetType,
                TargetId = condition.TargetId,
                ContextId = condition.ContextId,
                ContextType = condition.ContextType,
                LocaleId = condition.LocaleId,
                Condition = condition.Condition
            };

        var userList = ViewBag.UserList1 as Dictionary<string, string> ?? new Dictionary<string, string>();
%>
<ul class="list-group alt">
    <%
        foreach (var item in Model)
        {
            string icon = "fa fa-bolt", event_css = "";
            string eventName = item.EventName;

            if (eventName.Contains("Add") || eventName.Contains("Activate"))
            {
                icon = "fa fa-plus";
                event_css = "text-success";
            }
            else if (eventName.Contains("Delete") || eventName.Contains("Deactivate"))
            {
                icon = "fa fa-times-circle";
                event_css = "text-danger";
            }
            else if (eventName.Contains("Change"))
            {
                icon = "fa fa-random";
                event_css = "text-warning";
            }
            else if (eventName.Contains("Success"))
            {
                icon = "fa fa-check-circle";
                event_css = "text-success";
            }
            else if (eventName.Contains("Ended") || eventName.Contains("Reject") || eventName.Contains("Failure"))
            {
                icon = "fa fa-warning";
                event_css = "text-danger";
            }
            else if (eventName.Contains("Request"))
            {
                icon = "fa fa-bell";
                event_css = "text-info";
            }
            else if (eventName.Contains("Login"))
            {
                icon = "fa fa-sign-in";
                event_css = "text-info";
            }
            else if (eventName.Contains("Logout"))
            {
                icon = "fa fa-sign-out";
                event_css = "text-warning";
            }
            string content = item.TransformedDescription;
            var username = userList.ContainsKey(item.UserId) ? userList[item.UserId] : "Uknown";
            var span = (DateTime.Now - item.CreatedOn);
            string dstring = (int)span.TotalMinutes + this.GetLocalResourceObject("lbl_MinutesAgo").ToString();

            if ((int)span.TotalMinutes == 0)
                dstring = this.GetLocalResourceObject("lbl_JustNow").ToString();

            if (Math.Abs(span.TotalMinutes) > 60)
            {
                dstring = (int)span.TotalHours +this.GetLocalResourceObject("lbl_HoursAgo").ToString();

                if (Math.Abs(span.TotalHours) > 24)
                {
                    int days = (int)span.TotalDays;
                    dstring = days + this.GetLocalResourceObject("lbl_DaysAgo").ToString();

                    if (days == 1)
                    {
                        dstring =this.GetLocalResourceObject("lbl_Yesterday").ToString();
                    }

                    if (Math.Abs(span.TotalDays) > 30)
                    {
                        dstring = ((int)span.TotalDays / 30) + this.GetLocalResourceObject("lbl_MonthsAgo").ToString();

                        if (Math.Abs(span.TotalDays) > 30 * 365)
                        {
                            dstring = ((int)span.TotalDays / 365) +this.GetLocalResourceObject("lbl_YearsAgo").ToString();
                        }
                    }
                }
            }
            
    %>
    <li class="list-group-item">
        <div class="media">
            <span class="pull-left thumb-sx <%:event_css %>" title="<%:eventName %>">
                <i class="icon <%:icon %>"></i>
            </span>
            <div class="pull-right m-t-sm">
            </div>
            <div class="media-body">
                <div><%=content %></div>
                <small class="text-muted">
                    <%:username %> <span title="<%:item.CreatedOn.ToUIDateTimeString() %>"><%:dstring %></span>
                </small>
            </div>
        </div>
    </li>
    <% } %>
    <% if (paginate)
       { %>
    <% Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementId = "loading", LoadingElementDuration = 500 }, routeValues).SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString()).Render(); %>
    <% } %>
</ul>
<% }
    else
    {
%>
<div class="alert alert-info">
    <%=this.GetGlobalResourceObject("Eventsresource", "NoDataInfo")%>
</div>
<% } %>