﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Event>>" %>
<%@ Import Namespace="CelloSaaS.EventScheduler.EventPublishingEngine" %>
<%@ Import Namespace="MvcContrib.Pagination" %>
<%@ Import Namespace="CelloSaaS.View" %>
<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.EventScheduler.Model" %>
<% Html.RenderPartial("ActivityStreamView", Model); %>