﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.EventScheduler.Model.EventMetadata>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="form-contianer">
    <div id="eventFrmDiv">
        <form id="editeventform" name="editeventform" action="<%=Url.Action("ManageEvent") %>">
            <% Html.RenderPartial("ManageEvent"); %>
        </form>
    </div>
    <% if (UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.TenantAdmin))
       { %>
    <a class="btn btn-info" href="#" id="btnSave" title="<%:this.GetGlobalResourceObject("General","Save") %>">
        <i class="fa fa-save"></i>&nbsp;<%:this.GetGlobalResourceObject("General","Save") %></a>
    <% } %>
</div>
<script type="text/javascript">
    $(function () {
        $('#btnSave').click(function () {
            var eventName = $('form#editeventform input[name=EventName]').val();

            if (!eventName || eventName.length < 1) {
                alert(' <%:this.GetGlobalResourceObject("EventsResource","GetEvent_Alert")%>');
                return;
            }

            var postdata = $('form#editeventform').serialize();

            $.post($('form#editeventform').attr('action'), postdata, function (data) {
                if (data && data.Success) {
                    $("#eventListPanel").jstree('rename_node', null, eventName);
                    ShowSuccess(data.Success);
                } else {
                    $('form#editeventform').html(data);
                }
            });

            return false;
        });
    });
</script>
