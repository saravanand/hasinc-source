﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% Html.RenderPartial("StatusMessage"); %>
<% if (ViewBag.EventJobs == null || ViewBag.EventJobs.Count == 0)
   {   %>
<div class="form-container">
    <form id="addeventjobform" name="addeventjobform" action="<%=Url.Action("AddEventJob") %>">
        <div class="form-group">
            <label class="mandatory">
                <%:this.GetLocalResourceObject("lbl_WorkflowName")%>
            </label>
            <div class="row">
                <div class="col-md-11">
                    <%--<%:Html.TextBox("WFName")%>--%>
                    <%:Html.DropDownList("WFName", null, new { style="width:750px;" })%>
                    <%:Html.Hidden("jobId", "ab860f9a-74b4-e111-98c8-000000000000")%>
                    <%:Html.Hidden("eventId") %>
                </div>
                <div class="col-md-1 pd-0">
                    <a href="#" onclick="ManageWorkflow()" title="<%:this.GetLocalResourceObject("t_ManageWorkflow")%>">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_ServiceEndpoint")%>
            </label>
            <div class="row">
                <div class="col-md-11">
                    <%:Html.DropDownList("ServiceEndpointId", null, new { style="width:750px;" })%>
                </div>
                <div class="col-md-1 pd-0">
                    <a href="#" onclick="ManageServiceEndpoint()" title="<%:this.GetLocalResourceObject("t_ManageServiceEndpoint")%>">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_ServiceInputTransformXSL")%>
            </label>
            <%:Html.TextArea("XSLString", new { rows="10", style="width:100%;" })%>
        </div>
        <div class="form-group">
            <label class="mandatory">
                <%:this.GetLocalResourceObject("lbl_MapIdXPath")%>
            </label>
            <%:Html.TextBox("MapIdXPath", ViewData["MapIdXPath"])%>
        </div>
        <div class="form-actions">
            <a class="btn btn-info" id="btn-addwfjob" href="#" title="<%:this.GetLocalResourceObject("t_Addnewworkflowjobhandler")%>"><span>
                <%:this.GetLocalResourceObject("AddWFSubscriber")%></span></a>
        </div>
    </form>
</div>
<% } %>
<div id="jobList">
    <% if (ViewBag.EventJobs != null && ViewBag.EventJobs.Count > 0)
       {
           foreach (var item in (ViewBag.EventJobs as Dictionary<string, CelloSaaS.EventScheduler.Model.Job>).Values)
           { 
    %>
    <div class="form-list">
        <table class="table" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="width: 165px;">
                        <label>
                            <%:this.GetLocalResourceObject("lbl_JobName")%>
                        </label>
                    </td>
                    <td>
                        <%:item.Name %>
                    </td>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("lbl_JobType")%>
                            </label>
                        </td>
                        <td>
                            <%:item.Description%>
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    <br />
    <div class="heading_container">
        <h4>
            <%:this.GetLocalResourceObject("lbl_JobParameters")%></h4>
    </div>
    <% Html.RenderAction("GetJobParameter", new { eventId = ViewData["eventId"], jobId = item.JobId }); %>
    <%
           }
       }
       else
       { %>
    <!--<div class="info">
        No jobs available!
    </div>-->
    <% } %>
</div>
<script type="text/javascript">
    function ManageServiceEndpoint() {
        var url = '/ServiceEndpoint/Index';
        var ServiceEndpointId = $('#ServiceEndpointId').val();
        if (ServiceEndpointId && ServiceEndpointId.length > 0) {
            url = "/ServiceEndpoint/Manage?endpointId=" + ServiceEndpointId;
        }

        window.open(url);
    }

    function ManageWorkflow() {
        var url = '/Workflow/Index';
        var wfname = $('#WFName').val();
        if (wfname && wfname.length > 0) {
            url = '/Workflow/Index?workflowName=' + wfname;
        }
        window.open(url);
    }

    $(function () {
        window.xsleditor = CodeMirror.fromTextArea(document.getElementById("XSLString"), {
            mode: { name: "xml", alignCDATA: true },
            lineNumbers: true,
            lineWrapping: true,
            matchBrackets: true,
            autoCloseTags: true
        });

        window.xsleditor.on("cursorActivity", function () {
            window.xsleditor.matchHighlight("CodeMirror-matchhighlight");
        });

        $('select#WFName').select2();
        $('select#ServiceEndpointId').select2();

        var eventId = '<%:ViewData["eventId"] %>';

        $('#jobList a.del').click(function (e) {
            e.preventDefault();

            if (!confirm('<%:this.GetLocalResourceObject("q_DeleteJobMapping")%>')) {
                return false;
            }

            $.post($(this).attr('href'), null, function (data) {
                if (data && data.Success) {
                    ShowSuccess(data.Success);
                    LoadEventJobs(eventId);
                } else {
                    ShowError(data.Error || '<%:this.GetLocalResourceObject("e_DeleteEventJob")%>');
                }
            });

            return false;
        });

        $("#btn-addwfjob").click(function () {
            var wfName = $('form#addeventjobform select[name=WFName]').val();
            if (!wfName || wfName.length < 1) {
                alert('<%:this.GetLocalResourceObject("e_WorkflowName")%>');
                return false;
            }

            window.xsleditor.save();

            var mapId = $('form#addeventjobform input[name=MapIdXPath]').val();

            if (!mapId || mapId.length < 1) {
                alert('<%:this.GetLocalResourceObject("e_MapIdXpath")%>');
                return;
            }

            var postdata = $('form#addeventjobform').serialize();

            $.post($('form#addeventjobform').attr('action'), postdata, function (data) {
                if (data && data.Success) {
                    ShowSuccess(data.Success);
                    LoadEventJobs(eventId);
                    $('form#addeventjobform')[0].reset();
                    $('#AddEventJob').dialog("close");
                } else {
                    alert(data.Error);
                }
            });
            return false;
        });

        $('#WFName').change(function () {            
            if (document.getElementById('WFName').value == -1) {                
                ManageWorkflow();
            }
        });
    });
</script>
