﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.EventScheduler.Model.WFJobParameter>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% Html.RenderPartial("StatusMessage"); %>
<% if (this.Model != null)
   {
%>
<div class="form-container">
    <form id="updatejobparam" name="updatejobparam" action="<%=Url.Action("UpdateJobParameter") %>">
        <div class="form-group">
            <label class="mandatory">
                <%:this.GetLocalResourceObject("lbl_WorkflowName")%>
            </label>
            <div class="row">
                <div class="col-md-11">
                    <%--<%:Html.TextBox("WFName")%>--%>
                    <%:Html.DropDownList("WFName", null, new { style="width:750px;" })%>
                    <%:Html.Hidden("id")%>
                    <%:Html.Hidden("jobId")%>
                    <%:Html.Hidden("eventId") %>
                </div>
                <div class="col-md-1 pd-0">
                    <a href="#" onclick="ManageWorkflow()" title="<%:this.GetLocalResourceObject("t_ManageWorkflow")%>">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_ServiceEndpoint")%>
            </label>
            <div class="row">
                <div class="col-md-11">
                    <%:Html.DropDownList("ServiceEndpointId", null, new { style="width:750px;" })%>
                </div>
                <div class="col-md-1 pd-0">
                    <a href="#" onclick="ManageServiceEndpoint()" title="<%:this.GetLocalResourceObject("t_ManageServiceEndpoint")%>">
                        <i class="fa fa-wrench"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_ServiceInputTransformXSL")%>
            </label>
            <%:Html.TextArea("XSLString", new { rows="20", style="width:100%;" })%>
        </div>
        <div class="form-group">
            <label class="mandatory">
                <%:this.GetLocalResourceObject("lbl_MapIdXPath")%>
            </label>
            <%:Html.TextBox("MapIdXPath", Model.MapIdXPath)%>
        </div>
        <div class="form-actions">
            <a class="btn btn-info" id="btn-savewfjob" href="#" title="<%:this.GetLocalResourceObject("t_Savejobparameterdetails")%>"><span>
                <%:this.GetGlobalResourceObject("General", "Save")%></span></a>
            <a class="btn btn-info del" href="<%=Url.Action("DeleteEventJob", new { eventId = ViewData["eventId"], jobId = ViewData["jobId"] }) %>"
                title="<%:this.GetLocalResourceObject("t_Deletethisjob")%>"><span>
                    <%:this.GetGlobalResourceObject("General", "Delete")%></span></a>
        </div>
    </form>
</div>
<% }
   else
   { %>
<div class="info">
    <%:this.GetLocalResourceObject("e_JobParameter")%>
</div>
<% } %>
<script type="text/javascript">
    $(function () {
        var eventId = '<%:ViewData["eventId"] %>';

        $('#jobList a.del').click(function (e) {
            e.preventDefault();

            if (!confirm('<%:this.GetLocalResourceObject("q_DeleteJobMapping")%>')) {
                return false;
            }

            $.post($(this).attr('href'), null, function (data) {
                if (data && data.Success) {
                    ShowSuccess(data.Success);
                    LoadEventJobs(eventId);
                } else {
                    ShowError(data.Error || '<%:this.GetLocalResourceObject("e_DeleteEventJob")%>');
                }
            });

            return false;
        });

        $("#btn-savewfjob").click(function () {
            var wfName = $('form#updatejobparam select[name=WFName]').val();

            if (!wfName || wfName.length < 1) {
                alert('<%:this.GetLocalResourceObject("e_WorkflowName")%>');
                return false;
            }

            window.xsleditor.save();

            var mapId = $('form#updatejobparam input[name=MapIdXPath]').val();

            if (!mapId || mapId.length < 1) {
                alert('<%:this.GetLocalResourceObject("e_MapIdXpath")%>');
                return;
            }

            var postdata = $('form#updatejobparam').serialize();

            $.post($('form#updatejobparam').attr('action'), postdata, function (data) {
                if (data && data.Success) {
                    ShowSuccess(data.Success);
                    LoadEventJobs(eventId);
                    $('form#updatejobparam')[0].reset();
                } else {
                    ShowError(data.Error);
                }
            });
            return false;
        });

        window.xsleditor = CodeMirror.fromTextArea(document.getElementById("XSLString"), {
            mode: { name: "xml", alignCDATA: true },
            lineNumbers: true,
            lineWrapping: true,
            autoCloseTags: true
        });

        window.xsleditor.on("cursorActivity", function () {
            window.xsleditor.matchHighlight("CodeMirror-matchhighlight");
        });
    });
</script>
