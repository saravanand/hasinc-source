﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("manageEvents")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row">
            <div class="col-md-3">
                <div id="eventsActionPanel" class="grid simple">
                    <div class="grid-title">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="eventTreeSearchbox" name="eventTreeSearchbox" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                            </span>
                        </div>
                    </div>
                    <div class="grid-body" style="padding: 10px 0; overflow-x: auto;">
                        <div id="eventListPanel">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 pd-0">
                <div id="statusMessage">
                </div>
                <section id="rightPanel" class="panel">
                    <header class="panel-heading bg-info">
                        <ul class="nav nav-tabs nav-justified captilize">
                            <li class="active"><a data-toggle="tab" href="#eventConfigurePanel" title="<%:this.GetLocalResourceObject("m_EventDetails")%>">
                                <%:this.GetLocalResourceObject("m_EventDetails")%></a></li>
                            <li><a data-toggle="tab" href="#eventJobsPanel" title="<%:this.GetLocalResourceObject("m_EventProcessTitle")%>"><%:this.GetLocalResourceObject("m_EventProcess")%></a></li>
                        </ul>
                    </header>
                    <div class="panel-body pd-0">
                        <div class="tab-content" style="overflow: visible;">
                            <div id="eventConfigurePanel" class="tab-pane fade in active">
                                <div class="alert alert-info">
                                    <%:this.GetLocalResourceObject("m_SelectEvent")%>
                                </div>
                            </div>
                            <div id="eventJobsPanel" class="tab-pane fade">
                                <div class="alert alert-info">
                                    <%:this.GetLocalResourceObject("EventsInfo")%>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="modal fade" id="AddEvent" tabindex="-1" role="dialog" aria-labelledby="AddEvent" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 700px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title"><%:this.GetLocalResourceObject("t_AddNewBusinessEvent")%></h4>
                </div>
                <div class="modal-body">
                    <form id="addeventform" name="addeventform" action="<%=Url.Action("ManageEvent") %>">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close") %></button>
                    <button type="button" id="btnSaveEvent" class="btn btn-primary"><%=this.GetGlobalResourceObject("General","Save") %></button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        li.addevent ins, li.deleteevent ins {
            background-position: center bottom !important;
        }

        label {
            white-space: nowrap;
        }
    </style>
    <script type="text/javascript" src="/Scripts/jquery.jstree.js"></script>
    <script type="text/javascript">
        function DoSearch() {
            $("#eventListPanel").jstree("deselect_all");
            var val = $('#eventTreeSearchbox').val();

            if (val && val != '') {
                $("#eventListPanel").jstree('search', val);
            } else {
                $("#eventListPanel").jstree('clear_search');
            }

            return false;
        }

        function LoadEventJobs(eventId) {
            $('#eventJobsPanel').load('<%:Url.Action("GetEventJobDetails") %>' + '?eventId=' + eventId);
        }

        $(function () {
            var canAddEvent = 'True';// '<%: UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin) %>';

            $('#eventTreeSearchbox').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });

            var selectNodes = [];
            var selectNodeId = '<%= (ViewData["SelectEventId"] == null ? "" : ViewData["SelectEventId"] )%>';

            if (selectNodeId && selectNodeId != '') {
                selectNodes.push(selectNodeId);
            }

            LoadEventsTreeView();

            function LoadEventsTreeView() {
                $("#eventListPanel")
                .bind("loaded.jstree", function (event, data) {
                    $("#eventListPanel").jstree("open_all", -1, true);
                    if (selectNodeId && selectNodeId != '' && $('#' + selectNodeId).length > 0) {
                        setTimeout(function () {
                            $("#eventListPanel").jstree("deselect_all");
                            $("#eventListPanel").jstree("select_node", $('#' + selectNodeId), true);
                        }, 500);
                    }
                })
                .bind("select_node.jstree", function (event, data) {
                    if (!data) return;
                    $('div.alert-error,div.alert-success').remove();
                    var el = data.rslt.obj;
                    if (el.hasClass('category') || el.hasClass('root')) {
                        // don't do anything
                    } else {
                        //load the event config page
                        var categoryName = data.inst._get_parent(el).attr('id');
                        var eventId = el.attr('id');

                        if (!eventId)
                            return;

                        $.get('<%=Url.Action("GetEventDetails") %>', { categoryName: categoryName, eventId: eventId }, function (html) {
                            $("#eventConfigurePanel").html(html);
                        });

                        LoadEventJobs(eventId);
                    }
                })
	            .jstree({
	                core: {
	                    /* core options go here */
	                    "animation": 100,
	                    "initially_select": selectNodes
	                },
	                plugins: ["themes", "json_data", "search", "ui", "crrm", "contextmenu", "types", "cookies"],
	                "search": {
	                    "show_only_matches": true
	                },
	                "ui": {
	                    "select_limit": 1
	                },
	                "json_data": {
	                    "ajax": {
	                        "url": '<%=Url.Action("GetAllEventDetails") %>',
	                        "data": function (n) {
	                            return {
	                                "id": n.attr ? n.attr("id").replace("node_", "") : 1
	                            }
	                        }
	                    }
	                },
	                "contextmenu": {
	                    "select_node": false,
	                    "items": getContextMenu
	                },
	                "types": {
	                    "valid_children": ["root"],
	                    "type_attr": "rel",
	                    "types": {
	                        "root": {
	                            "valid_children": ["category"],
	                            "max_depth": -1,
	                            "hover_node": false,
	                            "select_node": function () { return false; }
	                        },
	                        "category": {
	                            "valid_children": ["default"],
	                            "max_depth": -1,
	                            "hover_node": false,
	                            "select_node": function () { return false; }
	                        },
	                        "default": {
	                            "valid_children": ["default"]
	                        }
	                    }
	                }
	            });
            }

            function CreateNewEvent(inst, el) {
                $('#AddEvent form').load('<%:Url.Action("ManageEvent") %>', function () {
                    var categoryName = el.attr('id');
                    $('input[name=Category]').val(el.hasClass('root') ? '' : categoryName);
                    $("#AddEvent").modal('show');
                });
            }

            function getContextMenu(el) {
                if (el.hasClass('category') || el.hasClass('root')) {
                    return canAddEvent != 'True' ? {} : {
                        "addevent": {
                            "label": "Add Event",
                            "action": function (obj) { CreateNewEvent(this, obj); },
                            "_disabled": false,
                            "_class": "addevent",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": "/Content/images/plus-ico.png"
                        }
                    };
                } else {
                    return canAddEvent != 'True' ? {} : {
                        "deleteevent": {
                            "label": "Delete",
                            "action": function (obj) {
                                if (!confirm('<%: this.GetLocalResourceObject("q_DeleteEvent")%>')) {
                                    return;
                                }

                                var eventId = obj.attr('id');

                                if (!eventId)
                                    return;

                                var inst = $.jstree._reference('#eventListPanel');

                                // delete this ruleset
                                $.post('<%=Url.Action("DeleteEvent") %>', { eventId: eventId }, function (data) {
                                    if (data && !data.Error) {
                                        alert('<%: this.GetLocalResourceObject("s_DeleteEvent")%>');

                                        var parent = inst._get_parent(obj); // get category node
                                        inst.delete_node(obj);

                                        if (parent) {
                                            var childs = inst._get_children(parent); // get its child nodes

                                            // if there is no child node then delete it
                                            if (childs == false || (childs && childs.length == 0)) {
                                                inst.delete_node(parent);   // remove the category node
                                            }
                                        }
                                    } else {
                                        ShowError(data.Error || '<%: this.GetLocalResourceObject("e_DeleteEvent")%>');
                                    }
                                });
                            },
                            "_disabled": false,
                            "_class": "deleteevent",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": "/Content/images/del-ico.png"
                        }
                    };
                }
            }

            $("#AddEvent").modal({ show: false });

            $('#btnSaveEvent').click(function () {
                var eventName = $('form#addeventform input[name=EventName]').val();

                if (!eventName || eventName.length < 1) {
                    alert('<%: this.GetLocalResourceObject("e_ValidEventName")%>');
                    return;
                }

                var postdata = $('form#addeventform').serialize();

                $.post($('form#addeventform').attr('action'), postdata, function (data) {
                    if (data && data.Success) {
                        ShowSuccess('<%: this.GetLocalResourceObject("s_CreateEvent")%>');
                        $("#eventListPanel").jstree('destroy');
                        LoadEventsTreeView();
                        $("#AddEvent").modal('hide');
                    } else {
                        $('#AddEvent form').html(data);
                    }
                });
            });
        });

        function ManageTemplate() {
            var templateId = $('#TemplateList').val();
            var templateName = $('#TemplateList option:selected').text();
            var url = '<%= Url.Action("LocaleTemplateDetails", "Template") %>';
            var TemplateUrl = '<%= Url.Action("TemplateDetails", "Template") %>';
            if (templateId.length == 0) {
                window.open(TemplateUrl + "?category=EventTemplate");
            }
            else {
                window.open(url + "?templateId=" + templateId + "&templateName=" + templateName + "&category=EventTemplate");
            }
        }
    </script>
    <script src="<%: Url.Content("~/bundles/codemirrorscripts") %>" type="text/javascript"></script>
    <link rel="Stylesheet" href="/Content/codemirror.css" />
</asp:Content>
