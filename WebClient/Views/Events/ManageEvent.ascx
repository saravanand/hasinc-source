﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.EventScheduler.Model.EventMetadata>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% Html.RenderPartial("StatusMessage"); %>
<%
    var isGlobal = (!string.IsNullOrEmpty(this.Model.EventId) && string.IsNullOrEmpty(this.Model.TenantId));
%>
<div class="form-container">
    <div class="form-group">
        <label class="mandatory">
            <%:this.GetLocalResourceObject("lbl_EventName") %></label>
        <% if (!isGlobal)
           { %>
        <%:Html.TextBox("EventName") %>
        <% }
           else
           { %>
        <br />
        <%:this.Model.EventName %>
        <%:Html.Hidden("EventName") %>
        <% } %>
        <%:Html.ValidationMessage("EventName","*") %>
        <%:Html.Hidden("EventId") %>
    </div>
    <div class="form-group">
        <label>
            <%:this.GetLocalResourceObject("lbl_Category")%>
        </label>
        <% if (string.IsNullOrEmpty(this.Model.EventId))
           { %>
        <%:Html.TextBox("Category")%>
        <%}
           else
           { %>
        <br />
        <%:this.Model.Category ?? "Default" %>
        <%:Html.Hidden("Category")%>
        <% } %>
    </div>
    <div class="form-group">
        <label>
            <%:this.GetLocalResourceObject("lbl_Description")%>
        </label>
        <% if (!isGlobal)
           { %>
        <%:Html.TextArea("Description") %>
        <% }
           else
           { %>
        <br />
        <%:this.Model.Description%>
        <%:Html.Hidden("Description")%>
        <% } %>
    </div>
    <div class="form-group">
        <label>
            <%:this.GetLocalResourceObject("lbl_Template")%>
        </label>
        <% if (UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.TenantAdmin))
           { %>
        <div class="row">
            <div class="col-md-11">
                <%=Html.DropDownList("TemplateList", null, "--Select Template--", new { style="width:100%;" })%>
            </div>
            <div class="col-md-1 pd-0">
                <a href="#" onclick="javascript:ManageTemplate();" title="<%=this.GetLocalResourceObject("t_ManageTemplate") %>">
                    <i class="fa fa-wrench"></i>
                </a>
            </div>
        </div>
        <% }
           else
           { %>
        <br />
        <%:ViewData["EventTemplateName"]%>
        <% } %>
    </div>
    <% if (UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin)
               && string.IsNullOrEmpty(this.Model.EventId))
       { %>

    <div class="form-group">
        <label for="IsGlobal">
            <%:this.GetLocalResourceObject("lbl_IsGlobal")%></label>
        <input type="checkbox" value="true" name="IsGlobal" id="IsGlobal" />
    </div>
    <% } %>
</div>
<script type="text/javascript">
    $(function () {
        $('select#TemplateList').select2();
    });
</script>