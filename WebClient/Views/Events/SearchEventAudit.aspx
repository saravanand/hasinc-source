﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%@ Import Namespace="CelloSaaS.EventScheduler.EventPublishingEngine" %>
<%@ Import Namespace="MvcContrib.Pagination" %>
<%@ Import Namespace="CelloSaaS.View" %>
<%@ Import Namespace="CelloSaaS.Library" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select[name=EventId],select[name=UserId],select[name=EventStatus]').select2();

            $(document).on('change', 'select[name=pageSize]', function () {
                var postdata = $('form#SearchEventAudit').serialize();                
                var queryString = '?pageSize=' + $(this).val();

                $.post('<%=Url.Action("SearchEventAudit")%>' + queryString, postdata, function (data) {
                    $('#divGrid').html(data);
                });

                $('div.alert').remove();
            });
        });

        function ResetEvents() {
            $('form#SearchEventAudit')[0].reset();            
            $('form#SearchEventAudit select').trigger('change');
            SearchEvents();
            return false;
        }

        function SearchEvents() {
            var postdata = $('form#SearchEventAudit').serialize();
            $.post('<%=Url.Action("SearchEventAudit")%>', postdata, function (data) {
                $('#divGrid').html(data);
            });
            $('div.alert').remove();;
        }
    </script>
    <div id="loading">
    </div>
    <% using (Html.BeginForm("SearchEventAudit", "Events", new { }, FormMethod.Post, new { id = "SearchEventAudit" }))
       {%>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("lbl_EventAudits")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("Error")%>
        </div>
        <%} %>
        <%
           var userList = ViewBag.UserList1 as Dictionary<string, string> ?? new Dictionary<string, string>();
           var lstUsers = userList.OrderBy(x => x.Value).Select(x => new SelectListItem
           {
               Text = x.Value,
               Value = x.Key
           }).ToList();
           lstUsers.Insert(0, new SelectListItem { Text = "All", Value = "" });
        %>
        <div class="row">
            <div id="searchFormDiv" class="col-md-3  pd-0">
                <section class="panel purple">
                    <header class="panel-heading">
                        <h4><%:this.GetLocalResourceObject("FilterHeading")%></h4>
                    </header>
                    <div class="panel-body">
                        <div class="form-container">
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_EventId")%></label>
                                <%= Html.DropDownList("EventId", null, new { style="width:100%;" })%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_UserId")%></label>
                                <%= Html.DropDownList("UserId", lstUsers, new { style="width:100%;" })%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_SubjectId")%></label>
                                <%= Html.TextBox("SubjectId")%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_SubjectType")%></label>
                                <%= Html.TextBox("SubjectType")%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_TargetId")%></label>
                                <%= Html.TextBox("TargetId")%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_TargetType")%></label>
                                <%= Html.TextBox("TargetType")%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_ContextId")%></label>
                                <%= Html.TextBox("ContextId")%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_ContextType")%></label>
                                <%= Html.TextBox("ContextType")%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_FromLogTime")%></label>
                                <%= Html.CelloCalendar("FromLogTime",null, null)%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_ToLogTime")%></label>
                                <%= Html.CelloCalendar("ToLogTime",null,null)%>
                            </div>
                            <div class="form-group">
                                <label><%:this.GetLocalResourceObject("lbl_EventStatus")%></label>
                                <%= Html.DropDownList("EventStatus", typeof(EventStatus).ToSelectList(), new { style="width:100%;" })%>
                            </div>
                            <div class="form-actions pull-right">
                                <a class="btn btn-info" href="#" title="<%=this.GetGlobalResourceObject("General", "Search")%>" onclick="SearchEvents()">
                                    <i class="fa fa-search">&nbsp;<%=this.GetGlobalResourceObject("General", "Search")%></i></a>
                                <a class="btn btn-default" href="#" title="<%=this.GetGlobalResourceObject("General", "Reset")%>" onclick="ResetEvents()">
                                    <%=this.GetGlobalResourceObject("General", "Reset")%></a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-9">
                <div class="grid simple horizontal green">
                    <div class="grid-title no-border">
                        <h4><%:this.GetLocalResourceObject("lbl_EventAudits")%></h4>
                    </div>
                    <div class="grid-body no-border" style="padding: 0px;">
                        <div id="divGrid">
                            <% Html.RenderPartial("EventAuditGrid", ViewData["EventAuditDetails"], ViewData);  %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <% } %>
</asp:Content>
