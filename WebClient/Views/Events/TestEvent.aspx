﻿<%@ Page Title="<%$Resources:h_TestEvent%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="heading_container">
        <h1>
            <%:this.GetLocalResourceObject("h_TestEvent")%> </h1>
        <div class="inner_hold">
            <div class="green_but">
                <a href="#" onclick="$('form').submit();" title="<%:this.GetLocalResourceObject("t_Raise")%>"><%:this.GetLocalResourceObject("b_Raise")%></a>
            </div>
        </div>
    </div>
    <div class="clear">
        <% Html.RenderPartial("StatusMessage"); %>
    </div>
    <div class="grid-part">
        <%using (Html.BeginForm())
          { %>
        <div style="vertical-align: top">
            <label>
                <%:this.GetLocalResourceObject("lbl_EventName")%> </label>
        </div>
        <div style="vertical-align: top">
            <%:Html.TextBox("eventName") %>
        </div>
        <br /><br />
        <div style="vertical-align: top">
            <label>
                <%:this.GetLocalResourceObject("lbl_XmlInput")%> </label>
        </div>
        <div style="vertical-align: top">
            <%:Html.TextArea("subjectXmlValue", new { cols="30",rows="20", style="width:80%;" })%>
        </div>
        <div class="inner_hold">
            <div class="green_but">
                <a href="#" onclick="$('form').submit();" title=" <%:this.GetLocalResourceObject("t_Raise")%>"> <%:this.GetLocalResourceObject("b_Raise")%></a>
            </div>
        </div>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/Scripts/codemirror.js"></script>
    <script type="text/javascript" src="/Scripts/xml.js"></script>
    <script type="text/javascript" src="/Scripts/util/formatting.js"></script>
    <script type="text/javascript" src="/Scripts/util/xml-hint.js"></script>
    <script type="text/javascript" src="/Scripts/util/closetag.js"></script>
    <script type="text/javascript" src="/Scripts/util/searchcursor.js"></script>
    <script type="text/javascript" src="/Scripts/util/matchBrackets.js"></script>
    <script type="text/javascript" src="/Scripts/util/match-highlighter.js"></script>
    <link href="/Content/codemirror.css" rel="Stylesheet" />
    <script type="text/javascript">
        $(function () {
            var editor = CodeMirror.fromTextArea(document.getElementById("subjectXmlValue"), {
                mode: { name: "xml", alignCDATA: true },
                lineNumbers: true,
                lineWrapping: true,
                matchBrackets:true,
                autoCloseTags: true
            });

            editor.on("cursorActivity", function () {
                editor.matchHighlight("CodeMirror-matchhighlight");
            });
        });
    </script>
</asp:Content>
