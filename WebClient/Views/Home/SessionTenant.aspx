﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/SessionTenantMaster.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaSApplication.Models" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="divide-20"></div>
    <%
        var tenantList = ViewData["TenantList"] as List<TenantViewModel>;
    %>
    <% Html.RenderPartial("StatusMessage"); %>
    <%
        if (tenantList != null && tenantList.Count > 0 && string.IsNullOrEmpty(UserIdentity.SessionTenantID))
        {
    %>
    <div class="page-title">
        <%
            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AllowCommonLogin"] ?? "false") != true)
            {
        %>
        <a href="<%:Url.Action("Index","Home") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <% } %>
        <h3>
            <%: this.GetLocalResourceObject("lbl_header") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <%=Html.TextBox("txtTenantSearch",string.Empty, new { @class="input-sm form-control search", placeholder=this.GetLocalResourceObject("p_SearchText")})%>
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" id="btnTenantSearch" type="button"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <% using (Html.BeginForm("SetSessionTenant", "Home", FormMethod.Post, new { @id = "sessionTenantForm" }))
                   { %>
                <%=Html.Hidden("sessionTenantId") %>
                <% } %>
                <div class="row list-view-container" id="tl-container">
                    <% foreach (var item in tenantList)
                       { %>
                    <div class="col-md-6 l-item">
                        <div class="row">
                            <div class="col-md-4">
                                <div style="margin-top: 15%;">
                                    <a href="#" class="btnSelectTenant" data-tenantid="<%:item.TenantDetails.TenantCode %>" title="<%: this.GetLocalResourceObject("t_Clicktologinasthistenant") %>">
                                        <%= Html.Logo(item.TenantDetails.TenantCode) %></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3>
                                    <a href="#" class="btnSelectTenant" data-tenantid="<%:item.TenantDetails.TenantCode %>" title="<%: this.GetLocalResourceObject("t_Clicktologinasthistenant") %>" style="color: #427FED">
                                        <%:item.TenantDetails.TenantName %>
                                        <small>
                                            <%=item.TenantDetails.IsSelfRegistered ? "<small><i title='"+this.GetLocalResourceObject("t_Selfregisteredtenant")+"' class='fa fa-cloud'></i></small>" : "" %>
                                            <%=item.TenantDetails.EnableAutoDebit ? "<small><i title='"+this.GetLocalResourceObject("t_Autobilldebitenabled")+"' class='fa fa-credit-card'></i></small>" : "" %>
                                        </small>
                                    </a>
                                </h3>
                                <p>
                                    <i class="fa fa-link" title="<%: this.GetLocalResourceObject("t_TenantLoginURL") %>"></i>&nbsp;<%:!string.IsNullOrEmpty(item.TenantDetails.URL) ? item.TenantDetails.URL : "NA" %>
                                </p>
                            </div>
                            <div class="col-md-2 pull-right">
                                <div class=" pull-right" style="margin-top: 25%;">
                                    <a class="btn btn-info btnSelectTenant" href="#" data-tenantid="<%:item.TenantDetails.TenantCode %>" title="<%: this.GetLocalResourceObject("t_Clicktologinasthistenant") %>"><%=this.GetGlobalResourceObject("General","Select") %></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <% 
                       } 
                    %>
                </div>
            </div>
        </div>
    </div>
    <% 
        }
        else if (!string.IsNullOrEmpty(UserIdentity.SessionTenantID))
        { %>
    <div class="page-title">
        <%
            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AllowCommonLogin"] ?? "false") != true)
            {
        %>
        <a href="<%:Url.Action("Index","Home") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <%
            }
        %>
        <h3>
            <%: this.GetLocalResourceObject("lbl_currenttenantcontext") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("lbl_currenttenantheader") %>
                </h4>
            </header>
            <div class="panel-body">
                <% var currentTenant = CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantInfo(UserIdentity.TenantID); %>
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <td style="width: 250px">
                                <label><%: this.GetLocalResourceObject("lbl_TenantName") %></label>
                            </td>
                            <td class="row5">
                                <b>
                                    <%: currentTenant.TenantDetails.TenantName %>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 250px">
                                <label><%=this.GetGlobalResourceObject("General","Description") %></label>
                            </td>
                            <td class="row5">
                                <%: currentTenant.TenantDetails.Description ?? "NA"%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 250px">
                                <label><%=this.GetGlobalResourceObject("General","URL") %></label>
                            </td>
                            <td class="row5">
                                <%: currentTenant.TenantDetails.URL ?? "NA"%>
                            </td>
                        </tr>
                    </tbody>
                    <tr>
                        <td></td>
                        <td>
                            <a class="btn btn-info" href="<%: Url.Action("ResetSessionTenant","Home") %>" title="<%: this.GetLocalResourceObject("lbl_reset") %>">
                                <%: this.GetLocalResourceObject("lbl_reset") %></a>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <script type="text/javascript">
            $.cookie("TenantContextCookie", null, { expires: 1, path: '/', raw: true });
        </script>
    </div>
    <% } %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btnSelectTenant').click(function () {
                var tenantId = $(this).data('tenantid');
                $('#sessionTenantId').val(tenantId);
                $('form#sessionTenantForm').submit();
            });

            $('#txtTenantSearch').keypress(function (e) {
                if (e.keyCode == 13) {
                    $('#btnTenantSearch').trigger('click');
                }
            });

            $('#btnTenantSearch').click(function () {
                $('#tl-container #emptyMsg').remove();
                var query = $('#txtTenantSearch').val();

                if (!query || query == '' || query.trim() == '') {
                    $('#tl-container .l-item').show();
                    return false;
                }

                $('#tl-container .l-item').each(function () {
                    var el = $(this);
                    console.log(el);
                    if (!el.html().match(new RegExp('.*?' + query + '.*?', 'i'))) {
                        el.hide();
                    } else {
                        el.show();
                    }
                });

                if ($('#tl-container .l-item:visible').length == 0) {
                    $('#tl-container').append('<div id="emptyMsg" class="alert alert-info"> <%: this.GetLocalResourceObject("e_NoTenantFound") %><i>"' + query + '"</i></div>');
                }

                return false;
            });
        });
    </script>
</asp:Content>
