<%@ Page Title="<%$Resources:Title%>" Language="C#" Inherits="System.Web.Mvc.ViewPage<CelloSaaSApplication.Controllers.LicenseViewModel>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <title><%:this.GetLocalResourceObject("Title") %></title>
    <%=this.Html.Theme() %>
    <style type="text/css">
        html, body {
            height: 100%;
            overflow: auto;
        }

        #logo {
            text-align: center;
        }
    </style>
</head>
<body>
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div id="logo">
                            <a href="/" title="<%: this.GetLocalResourceObject("t_GoToLogin") %>">
                                <h1>Cello<span class="semi-bold">SaaS</span></h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </section>
    <section id="login" class="visible animated fadeInDown">
        <div class="container">
            <section class="panel purple">
                <header class="panel-heading">
                    <h4><%:this.GetLocalResourceObject("Title") %></h4>
                </header>
                <div class="panel-body pd-0">
                    <% if (Model.HasError && !string.IsNullOrEmpty(Model.ErrorMessage))
                       { %>
                    <div class="alert alert-warning">
                        <%: Model.ErrorMessage%>
                    </div>
                    <% }
                       if (ViewData["Status"] != null)
                       { %>
                    <div class="alert alert-info">
                        <%: ViewData["Status"]%>
                    </div>
                    <% } %>
                    <% var activationDetail = ViewData["ActivationDetail"] == null ? null : ViewData["ActivationDetail"] as CelloSaaS.License.ActivationDetailsService.ActivationDetail;
                       using (Html.BeginForm("Index", "License", FormMethod.Post, new { name = "frm", id = "frm" }))
                       {  %>
                    <table class="celloTable">
                        <colgroup>
                            <col style="width: 120px;" />
                            <col style="width: auto;" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("LicenseType")%>
                                </td>
                                <td style="width: 80%;">
                                    <%: Model.LicenseName%>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("IsValid")%>
                                </td>
                                <td>
                                    <%: Model.IsValid%>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("lbl_Validity") %>
                                </td>
                                <td>
                                    <% if (Model.IsExpired)
                                       { %>
                                    <%:this.GetLocalResourceObject("ZeroDaysLeft")%>
                                    <% }
                                       else if ((Model.LicenseName.Contains("Developer") || Model.LicenseName.Contains("Production") || Model.LicenseName.Contains("Staging")) && Model.IsValid)
                                       { %>
                                Infinite
                                    <% 
                                       }
                                       else
                                       { %>
                                    <%: Model.DaysLeft.Days%>
                                    <%:this.GetLocalResourceObject("Days")%>,
                                <%: Model.DaysLeft.Hours%>
                                    <%:this.GetLocalResourceObject("Hours")%>,
                                <%: Model.DaysLeft.Minutes%>
                                    <%:this.GetLocalResourceObject("MinutesLeft")%>.
                                <% } %>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("lbl_OperatingSystem") %>
                                </td>
                                <td>
                                    <%= Model.OperatingSystem%>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("SystemInfo")%>
                                </td>
                                <td>
                                    <%= Model.SystemInfo%>
                                </td>
                            </tr>
                            <% if (!string.IsNullOrEmpty(Model.ActivationKey))
                               { %>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("ActivationKey")%>
                                </td>
                                <td>
                                    <div style="width: 615px; overflow-x: auto;">
                                        <%= Model.ActivationKey%>
                                    </div>
                                </td>
                            </tr>
                            <% } %>
                            <% if (!string.IsNullOrEmpty(Model.CompanyName))
                               { %>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("CompanyName")%>
                                </td>
                                <td>
                                    <span style="position: relative; left: 0px; overflow: auto; display: block; width: 550px;">
                                        <%= Model.CompanyName%></span>
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("CustomerName")%>
                                </td>
                                <td>
                                    <span style="position: relative; left: 0px; overflow: auto; display: block; width: 550px;">
                                        <%= Model.CustomerName%></span>
                                </td>
                            </tr>
                            <% if (!string.IsNullOrEmpty(Model.EmailId))
                               { %>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("CustomerEmail")%>
                                </td>
                                <td>
                                    <span style="position: relative; left: 0px; overflow: auto; display: block; width: 550px;">
                                        <%= Model.EmailId%></span>
                                </td>
                            </tr>
                            <% } %>
                            <% if (!string.IsNullOrEmpty(Model.PhoneNumber))
                               { %>
                            <tr>
                                <td class="text-right">
                                    <%:this.GetLocalResourceObject("PhoneNumber")%>
                                </td>
                                <td>
                                    <span style="position: relative; left: 0px; overflow: auto; display: block; width: 550px;">
                                        <%= Model.PhoneNumber%></span>
                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                    <div class="pull-right" style="margin: 10px;">
                        <a class="btn btn-default" href="<%=Url.Action("Index","Home") %>" title="<%:this.GetLocalResourceObject("t_GotoHome") %>">
                            <%:this.GetLocalResourceObject("lbl_GotoHome") %>
                        </a>
                        <% if (!Model.IsTrial)
                           { %>
                        <% if (!Model.IsActivated)
                           { %>
                        <a class="btn btn-success" href="<%=Url.Action("Index","License",new{ wa = "Activate" }) %>" title="<%:this.GetLocalResourceObject("ActivateTitle") %>">
                            <%:this.GetLocalResourceObject("Activate")%>
                        </a>
                        <% }
                           else
                           { %>
                        <a class="btn btn-danger" href="<%=Url.Action("Index","License",new{ wa = "Deactivate" }) %>" title="<%:this.GetLocalResourceObject("DeActivateTitle")%>">
                            <%:this.GetLocalResourceObject("DeActivate")%>
                        </a>
                        <% } %>
                        <% } %>
                    </div>
                    <% } %>
                </div>
            </section>
        </div>
    </section>
</body>
</html>
