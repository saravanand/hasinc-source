﻿<%@ Page Title="<%$ Resources:t_Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%:this.GetLocalResourceObject("t_Title") %></h3>
        <div class="pull-right">
            <a href="<%=Url.Action("Subscriptions") %>" title="Click here to subscribe to exceptions!" class="btn btn-success">Subscribe</a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetGlobalResourceObject("General","Filter") %></h4>
            </header>
            <div class="panel-body">
                <form id="frmSearch" name="frmSearch" method="post" action="<%=Url.Action("Log") %>">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("lbl_Tenant") %>
                                    <%= Html.DropDownList("TenantId", (IEnumerable<SelectListItem>)ViewData["TenantList"], new { style = "width:100%;" })%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("lbl_Severity") %>
                                    <%= Html.DropDownList("Severage", (IEnumerable<SelectListItem>)ViewData["lstTraceEventType"], new { style = "width:100%;" })%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("lbl_FromDate") %>
                                    <%=Html.TextBox("FromDate", "", new { @class = "datetime" })%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%:this.GetLocalResourceObject("lbl_ToDate") %>
                                    <%=Html.TextBox("ToDate","", new { @class = "datetime" })%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-4 pd-0">
                                        <%= Html.DropDownList("SearchType", (IEnumerable<SelectListItem>)ViewData["AdditionalSearchType"], new { style = "width: 100%;" })%>
                                    </div>
                                    <div class="col-md-8" style="padding-right: 0px;">
                                        <%=Html.TextBox("SearchValue", "",new { placeHolder="Search value...", style="height: 34px;"}) %>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <a class="btn btn-info" href="#" id="searchButton" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                                        <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%></a>
                                    <a class="btn btn-default" href="#" id="btnReset" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                                        <%:this.GetGlobalResourceObject("General", "Reset")%></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <div id="divGrid">
            <% Html.RenderPartial("PartialLog", this.Model);  %>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="previewExceptionMessage" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><%:this.GetLocalResourceObject("h_PreviewExceptionMessage") %></h4>
                </div>
                <div class="modal-body" style="white-space: pre-wrap; overflow: auto;">
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCancel" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close") %></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#FromDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#ToDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#frmSearch select').select2();

            $('#btnReset').click(function () {
                $('#frmSearch')[0].reset();
                $('#frmSearch select').trigger('change');
                $('#searchButton').trigger('click');
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var searchField = $('#searchField option:selected').val();
                var searchValue = $('#searchValue').val();
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                sortString = sortString == undefined ? '' : sortString;
                sortDirection = sortDirection == undefined ? '' : sortDirection;

                var postdata = "?" + $('#frmSearch').serialize();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                postdata += '&searchField=' + searchField;
                postdata += '&searchValue=' + searchValue;
                postdata += '&sortString=' + sortString;
                postdata += '&sortDirection=' + sortDirection;

                $.post('<%=Url.Action("Index","Logger") %>' + postdata, null, function (data) {
                    $('#divGrid').html(data);
                });
            });

            $('#searchButton').click(function () {
                var searchField = $('#searchField option:selected').val();
                var searchValue = $('#searchValue').val();
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();

                sortString = sortString == undefined ? '' : sortString;
                sortDirection = sortDirection == undefined ? '' : sortDirection;

                var postdata = "?" + $('#frmSearch').serialize();
                postdata += '&page=' + $('input[type=hidden][name=pager_pageNumber]').val();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                postdata += '&searchField=' + searchField;
                postdata += '&searchValue=' + searchValue;
                postdata += '&sortString=' + sortString;
                postdata += '&sortDirection=' + sortDirection;

                $.post('<%=Url.Action("Index","Logger") %>' + postdata, null, function (data) {
                    $('#divGrid').html(data);
                });

                return false;
            });

            $(document).on('click', '#divGrid a.view-exception', function () {
                var Id = $(this).attr('divId');
                var ex = $('#div' + Id).text();
                $('#previewExceptionMessage .modal-body').html(ex);
                $('#previewExceptionMessage').modal('show');
            });

            $('#btnCancel').click(function () { $('#previewExceptionMessage').modal('hide'); return false; });
        });
    </script>
</asp:Content>
