﻿<%@ Page Title="Manage Exception Email Subscriptions" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.Logger.ExceptionSubscription>" %>

<%@ Import Namespace="CelloSaaS.Model.Logger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <% using (Html.BeginForm())
       { %>
    <div class="page-title">
        <a href="<%=Url.Action("Subscriptions") %>" title="Go Back!">
            <i class="icon-custom-left"></i>
        </a>
        <h3>Manage Exception Email Subscription</h3>
        <div class="pull-right">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></button>
            <%=Html.CelloButton(Url.Action("Subscriptions"), this.GetGlobalResourceObject("General","Cancel")) %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4>Enter Subscription Details</h4>
            </header>
            <div class="panel-body form-container">
                <%=Html.HiddenFor(x=>x.Id) %>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <%=Html.LabelFor(x => x.Name, new { @class="mandatory" })%>
                            <%=Html.TextBoxFor(x=>x.Name) %>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <%=Html.LabelFor(x=>x.Description) %>
                            <%=Html.TextAreaFor(x=>x.Description) %>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <%=Html.LabelFor(x=>x.To, new { @class="mandatory" }) %>
                            <span class="text-info"><i class="fa fa-info-circle"></i>&nbsp;Separate multiple email address by ; (semi-colon)</span>
                            <%=Html.TextBoxFor(x=>x.To) %>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <%=Html.LabelFor(x=>x.Cc) %>
                            <span class="text-info"><i class="fa fa-info-circle"></i>&nbsp;Separate multiple email address by ; (semi-colon)</span>
                            <%=Html.TextBoxFor(x=>x.Cc) %>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <%=Html.Label("Notification", new { @class="mandatory" }) %>
                            <%=Html.DropDownList("NotificationId", (IEnumerable<SelectListItem>)ViewBag.NotificationNames) %>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <%=Html.Label("Enabled", new { @for = "Status" })%><br />
                            <%=Html.CheckBoxFor(x=>x.Status) %>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="panel blue">
            <header class="panel-heading">
                <h4>Select Categories</h4>
                <div class="actions pull-right">
                    <input id="chkSelectAll" name="chkSelectAll" type="checkbox">
                    <label for="chkSelectAll">Select All</label>
                </div>
            </header>
            <div class="panel-body form-container">
                <%if (ViewBag.Categories as List<SelectListItem> != null)
                  {
                      foreach (SelectListItem item in ViewBag.Categories as List<SelectListItem>)
                      { 
                %>
                <div class="col-sm-2">
                    <input type="checkbox" name="Categories" id="<%:item.Value %>" value="<%:item.Value %>" <%:item.Selected ? "checked='checked'" : string.Empty %> />&nbsp;
                    <label for="<%:item.Value %>"><%:item.Value %></label>
                </div>
                <%
                      }
                  }
                  else
                  { %>
                <div class="alert alert-info">
                    No categories available to subscribe!
                </div>
                <% } %>
            </div>
        </section>
        <div class="pull-right">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></button>
            <%=Html.CelloButton(Url.Action("Subscriptions"), this.GetGlobalResourceObject("General","Cancel")) %>
        </div>
    </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $('select#NotificationId').attr('style', 'width:100%');
            $('select#NotificationId').select2();

            $('#chkSelectAll').click(function () {
                val = $(this).is(':checked');
                $('input[name="Categories"]').each(function () {
                    $(this).prop('checked', val ? val : null);
                });
            });
        });
    </script>
</asp:Content>
