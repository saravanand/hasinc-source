﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.Model.Logger.CelloLogEntry>>" %>
<%@ Import Namespace="CelloSaaS.Model.Logger" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    string sortDirection = (string)ViewData["SortDirection"];
    string sortString = (string)ViewData["SortString"];
    var search = ViewData["search"] != null ? ViewData["search"] as CelloLogEntrySearchCondition : new CelloLogEntrySearchCondition();
    var totals = ViewData["TotalCount"] != null ? ViewData["TotalCount"] as Dictionary<string, int> : new Dictionary<string, int>();

    int pageNumber = 0, totalCount = 0, pageSize = 0;
    if (ViewData.ContainsKey("PageNumber") && ViewData["PageNumber"] != null)
    {
        pageNumber = (int)ViewData["PageNumber"];
    }

    if (totals.ContainsKey("TotalCount"))
    {
        totalCount = totals["TotalCount"];
    }

    if (ViewData.ContainsKey("PageSize") && ViewData["PageSize"] != null)
    {
        pageSize = (int)ViewData["PageSize"];
    }    
%>
<% Html.RenderPartial("StatusMessage"); %>
<% if (this.Model != null && this.Model.Count() > 0)
   {
%>
<div class="row">
    <div class="col-md-2">
        <div class="tiles orange">
            <div class="tiles-body">
                <div class="tiles-title"><%:this.GetLocalResourceObject("lbl_TotalError") %></div>
                <div class="heading">
                    <span class="animate-number" data-animation-duration="1200" data-value="<%= totals["TotalError"].ToString("0.##") %>">
                        <%= totals["TotalError"].ToString("0.##") %></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="tiles red">
            <div class="tiles-body">
                <div class="tiles-title"><%:this.GetLocalResourceObject("lbl_TotalCritical") %></div>
                <div class="heading">
                    <span class="animate-number" data-animation-duration="1200" data-value="<%= totals["TotalCritical"].ToString("0.##") %>">
                        <%= totals["TotalCritical"].ToString("0.##") %></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="tiles purple">
            <div class="tiles-body">
                <div class="tiles-title"><%:this.GetLocalResourceObject("lbl_TotalWarning") %></div>
                <div class="heading">
                    <span class="animate-number" data-animation-duration="1200" data-value="<%= totals["TotalWarning"].ToString("0.##") %>">
                        <%= totals["TotalWarning"].ToString("0.##") %></span>
                </div>
            </div>
        </div>
    </div>
</div>
<br />
<div class="grid simple">
    <div class="grid-body no-border pdm-0">
        <div style="overflow: auto;">
            <%
       Html.Grid(this.Model).Columns(
        column =>
        {
            column.For(col => col.TenantName).Named(this.GetLocalResourceObject("lbl_TenantName").ToString()).Visible(search.TenantId == "-1" ? true : false);
            column.For(col => col.UserName).Named(this.GetLocalResourceObject("lbl_UserName").ToString());
            column.For(col => col.CategoryName).Named(this.GetLocalResourceObject("lbl_Catagory").ToString());
            column.For(col => col.Timestamp.ToUIDateTimeString()).Named(this.GetLocalResourceObject("lbl_TimeStamp").ToString());
            column.For(col => col.Severity).Named(this.GetLocalResourceObject("lbl_severity").ToString());
            column.For(col => col.MachineName).Named(this.GetLocalResourceObject("lbl_MachineName").ToString());
            //column.For(col => col.ProcessID).Named(this.GetLocalResourceObject("lbl_ProcessID").ToString());
            //column.For(col => col.Title).Named(this.GetLocalResourceObject("lbl_title").ToString());
            column.For(col => string.Format("<span>{0}</span><div id='div{2}' style='display:none'>{1}</div>", ((col.Message.Length <= 30) ? HttpUtility.HtmlEncode(col.Message) : HttpUtility.HtmlEncode(col.Message.Remove(30)) + string.Format("<a href='#' class='view-exception red' divId='{0}' title='" + this.GetLocalResourceObject("t_more") + "'> '" + this.GetLocalResourceObject("lbl_more") + "' </a>", col.LogId)), HttpUtility.HtmlEncode(col.Message), col.LogId))
                .Named(this.GetLocalResourceObject("lbl_Message").ToString()).DoNotEncode();
        }).Attributes(id => "dataList", @class => "celloTable").Render();
        
       var rval = new { TenantId = search.TenantId, FromDate = search.FromDate, ToDate = search.ToDate, Severage = search.Severage, SearchType = search.SearchType, SearchValue = search.SearchValue };
       Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementId = "loading", LoadingElementDuration = 500 }, rval)
           .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
           .SetPageNumber(pageNumber).SetPageSize(pageSize).SetTotalCount(totalCount)
           .Render();
            %>
        </div>
    </div>
</div>
<% }
   else
   { %>
<div class="alert alert-info">
    <%:this.GetGlobalResourceObject("General","m_NoData") %>
</div>
<% } %>