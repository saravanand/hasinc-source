﻿<%@ Page Title="Exception Email Subscriptions" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<List<CelloSaaS.Model.Logger.ExceptionSubscription>>" %>

<%@ Import Namespace="CelloSaaS.Model.Logger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="Go Back!">
            <i class="icon-custom-left"></i>
        </a>
        <h3>Exception Email Subscriptions</h3>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <div class="grid simple">
            <header class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <form name="frmFilter" id="frmFilter">
                            <div id="sub_filter" class="dataTables_filter">
                                <div class="input-group">
                                    <input id="searchText" name="searchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-white" type="submit"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-5 pull-right">
                        <div class="pull-right">
                            <a class="btn btn-success" href="<%=Url.Action("ManageSubscription") %>" title="Create new subscription">
                                <i class="fa fa-plus"></i>&nbsp;Create
                            </a>
                        </div>
                    </div>
                </div>
            </header>
            <div class="grid-body">
                <% if (this.Model != null && this.Model.Count() > 0)
                   {
                %>
                <%
                       Html.Grid(this.Model).Columns(
                        column =>
                        {
                            column.For(col => col.Name).Named("Name");
                            column.For(col => col.Description.Truncate()).Named("Description");
                            column.For(col => col.To).Named("To");
                            column.For(col => col.Categories.Truncate()).Named("Catagories");
                            column.For(col => col.Status ? "<i class='fa fa-check'></i>" : "<i class='fa fa-times'></i>")
                                .Named("Enabled").HeaderAttributes(@class => "text-center").Attributes(@class => "text-center").DoNotEncode();
                            column.For(col => string.Format("<a href='{0}' title='Click to edit!'><i class='fa fa-edit'></i></a>", Url.Action("ManageSubscription", new { id = col.Id })))
                                .Named("Edit").DoNotEncode().HeaderAttributes(@class => "text-center").Attributes(@class => "text-center");
                            column.For(col => string.Format("<a href='{0}' title='Click to delete!'><i class='fa fa-trash-o'></i></a>", Url.Action("DeleteSubscription", new { id = col.Id })))
                                .Named("Delete").DoNotEncode().HeaderAttributes(@class => "text-center").Attributes(@class => "text-center");
                        }).Attributes(id => "dataList", @class => "celloTable").Render();
                %>
                <% }
                   else
                   { %>
                <div class="alert alert-info">
                    <%:this.GetGlobalResourceObject("General","m_NoData") %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        var filterTable = null;
        $(function () {
            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }]
            });

            $('#frmFilter').submit(function () {
                var searchText = $('#searchText').val();
                if (searchText == 'Search') {
                    searchText = '';
                }
                filterTable.fnFilter(searchText);
                $("div.alert").remove();
                return false;
            });
        });
    </script>
</asp:Content>
