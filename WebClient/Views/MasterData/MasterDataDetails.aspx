<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Model.MasterDataManagement" %>
<%@ Import Namespace="CelloSaaS.Library.Configuration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(function () {
            $('form select').each(function () {
                $(this).attr('style', 'width:100%;');
            });
            $('form select').select2();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% 
        using (Html.BeginForm("MasterDataDetails", "MasterData"))
        {
    %>
    <div class="page-title">
        <a href="../MasterData/MasterDataList/<%=ViewData["MasterDataName"]%>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%:this.GetLocalResourceObject("Manage")%>&nbsp;-
            <span class="semi-bold"><% =Html.Encode(ViewData["MasterDataName"]) %></span>
        </h3>
        <div class="pull-right">
            <a class="btn btn-success" href="#" onclick='javascript:document.forms[0].submit();'><i class="fa fa-save"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Save")%> </a>
            <a class="btn btn-default" href="../MasterData/MasterDataList/<%=ViewData["MasterDataName"]%>">
                <%:this.GetLocalResourceObject("Back")%></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("StatusMessageSuccess")))
          { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("StatusMessageSuccess")%>
        </div>
        <%}
          else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("StatusMessage")))
          { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("StatusMessage")%>
        </div>
        <%} %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4>
                    <%if (string.IsNullOrEmpty(Convert.ToString(ViewData["MasterDataIdentity"])))
                      { %><%:this.GetLocalResourceObject("Add")%>
                    <% =Html.Encode(ViewData["MasterDataName"]) %>
                    <%}
                      else
                      { %><%:this.GetLocalResourceObject("Update")%>
                    <% =Html.Encode(ViewData["MasterDataName"]) %>
                    <%} %></h4>
            </header>
            <div class="panel-body">
                <%                                            
                      string _masterDataIdentifier = string.Empty;
                      CurrentFormMode currentMode = new CurrentFormMode();
                      string _masterDataName = string.Empty;
                      string _modelPrefix = "MasterDataEntity";

                      if (ViewData["FormMode"] != null)
                          currentMode = (CurrentFormMode)ViewData["FormMode"];

                      if (ViewData["MasterDataName"] != null)
                          _masterDataName = ViewData["MasterDataName"].ToString();

                      if (ViewData["MasterDataIdentity"] != null)
                          _masterDataIdentifier = ViewData["MasterDataIdentity"].ToString();

                      GenericMasterData masterData = new GenericMasterData(_masterDataName);

                      if (ViewData["MasterDataEntity"] != null)
                          masterData = (GenericMasterData)ViewData["MasterDataEntity"];

                      Dictionary<int, ColumnProperties> columnAttributes = new Dictionary<int, ColumnProperties>();

                      ColumnProperties columnAttribute = new ColumnProperties
                          {
                              LabelContentProperties =
                                  new Dictionary<string, string>() { { "class", "row4" } },
                              FieldContentProperties =
                                  new Dictionary<string, string>() { { "class", "row5" } }
                          };

                      columnAttributes.Add(1, columnAttribute);
                      columnAttributes.Add(2, columnAttribute);

                      AlternateRowStyle alternateRowStyle = new AlternateRowStyle();
                      Dictionary<string, string> formAttributes = new Dictionary<string, string>();
                %>
                <%=Html.Hidden("MasterDataName", _masterDataName)%>
                <%=Html.Hidden("ModelPrefix", _modelPrefix)%>
                <%=Html.Hidden("MasterDataIdentity", _masterDataIdentifier)%>
                <%
            try
            {
                MasterDataConfiguration masterDataConfiguration = (MasterDataConfiguration)System.Configuration.ConfigurationManager.GetSection(MasterDataConfiguration.SectionName);
                MasterDataElement masterDataElement = masterDataConfiguration.MasterData.Get(_masterDataName);
                var frmTable = new DataViewTable("", currentMode, formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, masterData, _modelPrefix);
                MasterDataWebAdapter.AddMasterDataDetailsColumns(this.Html, _masterDataName, frmTable, _modelPrefix, masterData);
                this.Html.ViewContext.Writer.Write(frmTable.Render());
            }
            catch (Exception ex)
            {
                this.Html.ViewContext.Writer.Write(ex.ToString());
            }   
                %>
            </div>
        </section>
        <div class="pull-right">
            <a class="btn btn-success" href="#" onclick='javascript:document.forms[0].submit();'><i class="fa fa-save"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Save")%> </a>
            <a class="btn btn-default" href="../MasterData/MasterDataList/<%=ViewData["MasterDataName"]%>">
                <%:this.GetLocalResourceObject("Back")%></a>
        </div>
        <% } %>
    </div>
    <script type="text/javascript">
        $(document).ready(function () { $("select").select2({ dropdownAutoWidth: true, width: "100%" }); });
    </script>
</asp:Content>
