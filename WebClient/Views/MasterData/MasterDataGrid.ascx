﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Data.DataTable>" %>
<%@ Import Namespace="System.Data" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="row-fluid pd-25">
    <%
        if (ViewData["Success"] != null && !string.IsNullOrEmpty(ViewData["Success"].ToString()))
        {
    %>
    <div class="alert alert-success">
        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
        <%: ViewData["Success"].ToString() %>
    </div>
    <%
        }
    %>
    <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("StatusMessageSuccess")))
      { %>
    <div class="alert alert-success">
        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
        <%=Html.CelloValidationMessage("StatusMessageSuccess")%>
    </div>
    <%}
      else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("StatusMessage")))
      { %>
    <div class="alert alert-danger">
        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
        <%=Html.CelloValidationMessage("StatusMessage")%>
    </div>
    <%} %>
</div>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="masterTableSearchText" name="masterTableSearchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                    </span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <a class="btn btn-success" href="/MasterData/MasterDataDetails?masterDataName=<%: ViewData["MasterDataName"] %>"
                        title="<%=this.GetGlobalResourceObject("General","Add") %>">
                        <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Add")%>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body">
        <% if (Model != null && ((DataTable)Model).AsEnumerable().Count() > 0)
           {
               DataTable masterData = new DataTable();
               string masterDataName = this.ViewData["MasterDataName"].ToString();
               if (Model != null)
               {
                   masterData = (DataTable)Model;
               }
               string masterDataIdentifierName = "MasterDataIdentifierName";

               Html.Grid(masterData.AsEnumerable()).Columns(
                           column =>
                           {
                               foreach (DataColumn dataColumn in masterData.Columns)
                               {
                                   DataColumn col = dataColumn;
                                   if (!col.ColumnName.Equals(masterDataIdentifierName))
                                       column.For(x => x[col].ToString().Length > 50 ? x[col].ToString().Substring(0, 50) + "..." : x[col].ToString()).Named(col.ColumnName);
                               }

                               column.For(col => Html.CelloActionLink("Manage", "MasterDataDetails", new { masterDataName = masterDataName, referenceId = col[masterDataIdentifierName].ToString() }).Replace("Manage", "<i class='fa fa-pencil'></i>")).Named(this.GetGlobalResourceObject("General", "Edit").ToString()).Attributes(@class => "halign", style => "width:100px;").HeaderAttributes(@class => "halign").DoNotEncode();
                               column.For(col => "<a href='javascript:void();' onclick='DeactivateMasterData(this)' data-name='" + masterDataName + "' data-id='" + col[masterDataIdentifierName].ToString() + "' title='DeActivate'><i class='fa fa-trash-o'></i></a>").Named(this.GetGlobalResourceObject("General", "Delete").ToString()).Attributes(@class => "halign", style => "width:100px;").HeaderAttributes(@class => "halign").DoNotEncode();
                           }).Attributes(id => "dataList", @class => "celloTable", cellspacing => "0", cellpading => "0").Render();
           }
           else
           {%>
        <div class="alert alert-info">
            <%: string.Format(this.GetGlobalResourceObject("MasterDataResource","e_NoRecordsFound").ToString(),this.ViewData["MasterDataName"].ToString()) %>
        </div>
        <%}%>
    </div>
</div>
