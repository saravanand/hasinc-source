<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();
            BindSearchControl();
        });

        function BindSearchControl() {
            $('#masterTableSearchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        }

        function DeactivateMasterData(element) {

            var name = $(element).data("name");
            var id = $(element).data("id");

            var targetUri = "/MasterData/DeleteMasterDataDetails?masterDataName=" + name + "&referenceId=" + id;

            $.ajax({
                url: targetUri,
                type: "post",
                success: function (result) {
                    $(".masterdatagrid").html(result);
                }, error: function () {
                    alert("Exception occured while deleting " + name);
                },
                complete: function () {
                    jQDataTable();
                    BindSearchControl();
                }
            });
        }

        function jQDataTable() {
            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }]
            });
        }

        function DoSearch() {
            var searchText = $('#masterTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("manage")%>
            - <span class="semi-bold"><%= Html.Encode(ViewData["MasterDataName"]) %>
            </span>
        </h3>
    </div>
    <div class="masterdatagrid">
        <% Html.RenderPartial("MasterDataGrid", ViewData["MasterDataList"] as DataTable); %>
    </div>
</asp:Content>
