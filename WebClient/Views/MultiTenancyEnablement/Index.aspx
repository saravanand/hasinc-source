﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<dynamic>>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading" style="display: none; visibility: hidden;"></div>

    <div class="page-title">
        <a href="/Tenant" title="Back To Tenant List">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
        <%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","header") %>
                
        </h3>

    </div>
    <div id="mainDiv" class="row-fluid pd-25">

        <section id="tab-container" class="panel">
            <header class="panel-heading bg-success">
                <ul class="nav nav-tabs nav-justified captilize">
                    <li id="step1"><a id="astep1" data-toggle="tab" href="#connectToServerDiv" title="View Connect to Server "><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","tabHeaderStep1") %></a></li>
                    <li id="step2" class="disabled"><a id="astep2" href="#tablesToBeModifyDiv" title="View Tables to Be Modify"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","tabHeaderStep2") %></a></li>
                    <li id="step3" class="disabled"><a id="astep3" href="#summaryDiv" title="View Summary"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","tabHeaderStep3") %></a></li>
                    <li id="step4" class="disabled"><a id="astep4" href="#generatedScriptDiv" title="View Generated Script"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","tabHeaderStep4") %></a></li>

                </ul>
            </header>
            <div class="panel-body pd-0">
                <div class="tab-content">
                    <div class="tab-pane fade" id="connectToServerDiv" style="padding: 0px;">
                        <form id="dbMigrationFormStep1" action="Index" enctype="multipart/form-data" role="form" method="post">

                            <div class="row-fluid pd-0">

                                <div id="Div1" class="form_list">
                                    <section class="panel purple">
                                        <header class="panel-heading">
                                            <h4><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","tabHeaderStep1") %></h4>
                                        </header>
                                        <div class="panel-body">
                                            <div class="form-container">
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group  ">
                                                                <label id="Label_TenantDBMigration_ServerName" class="mandatory"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","serverName") %></label><input id="ServerName" required="required" maxlength="50" name="ServerName" type="text" value=""></div>
                                                        </div>
                                                        <div class="col-md-6">  <%if (Html.ValidationSummary() != null)
                                                              {  %>
                                                            <div class="alert alert-danger"><%= Html.ValidationSummary(true)%></div>
                                                            <% } %></div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group  ">
                                                                <label id="Label_TenantDBMigration_DBName" class="mandatory"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","dataBaseName") %></label><input id="InstanceName" maxlength="50" name="InstanceName" required="required" type="text" value=""></div>
                                                        </div>
                                                        <div class="col-md-6"></div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group  ">
                                                                <label id="Label_TenantDBMigration_LoginName" class="mandatory"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","loginId") %></label><input id="LoginID" maxlength="50" name="LoginID" type="text" required="required" value=""></div>
                                                        </div>
                                                        <div class="col-md-6"></div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group  ">
                                                                <label id="Label_TenantDBMigration_Password" class="mandatory"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","password") %></label><input id="Password" maxlength="50" name="Password" type="password" required="required" value=""></div>
                                                        </div>
                                                        <div class="col-md-6">

                                                          
                                                        </div>

                                                    </div>
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-success"><i class="fa fa-arrow-right"></i>&nbsp;<%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","next") %></button>
                                                        <a href="/Tenant" class="btn btn-default"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","cancel") %></a>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="tablesToBeModifyDiv">
                        <form id="dbMigrationFormStep2" action="Summary" method="post">
                            <div class="row">
                                <div class="col-md-12 alert alert-info" style="padding: 5px;"><i class="fa fa-info-circle fa-2x" style="padding-left: 30px; padding-top: 5px"></i>
                                    <label style="font-size: medium; padding: 5px"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","step2Info") %></label><div class="pull-right">
                                        <input type="checkbox" id="selectAll" checked="checked" /><span><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","checkAll") %></span></div>
                                </div>
                            </div>
                            <% if (ViewBag.step.ToString() == "step2")
                               {
                                   string[] sectionColors = new string[] { "green", "blue", "purple", "red", "orange" };
                                   int columnCount = 1; int schemaCount = 0;
                                   var schemaList = Model.Select(x => x.Schema).Distinct().ToList();
                                   foreach (string schema in schemaList)
                                   {
                                       var sectionId=schema+"Section";
                                   
                            %>
                            <section id="<%=schema %>Section" class="panel <%=sectionColors[schemaCount] %>">
                                <header class="panel-heading">
                                    <h4><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","schemaName") %> : <%=schema %></h4>
                                    <div class="pull-right">
                                        <input style="margin: 5px;" type="checkbox" id="<%=schema %>Id" onclick="checkCheckboxes(this.id,'<%=sectionId%>');" checked="checked" />
                              
                                        <span style="color: white"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","checkAll") %></span></div>
                                </header>
                                <%schemaCount++;
                                  if (schemaCount >= 5) { schemaCount = 0; }
                                  int tableCount = 0;
                                  int foreachCount = Model.Where(m => m.Schema == schema).Count();
                                  var foreachModel = Model.Where(m => m.Schema == schema);
                                  foreach (var tableMetadata in foreachModel)
                                  {
                                      tableCount++;
                                      var alertcss = "";
                                      if (tableMetadata.HasTenantIdColumn && tableMetadata.HasTenantKeyValid)
                                      { alertcss = "fa fa-thumbs-o-up alert alert-success"; }
                                      else if (tableMetadata.HasTenantIdColumn && !tableMetadata.HasTenantKeyValid) { alertcss = "fa fa-info-circle alert alert-info"; }
                                      else { alertcss = "fa fa-exclamation-triangle alert alert-warning"; }
                                      if (columnCount == 1)
                                      { %>

                                <div class="row" style="margin-left: inherit; margin-right: inherit;">
                                    <div class="col-md-4 alert" style="margin-bottom: 0px; padding-bottom: 0px;">
                                        <i class="<%=alertcss %>" style="padding: 5px"></i>


                                        <% if (!tableMetadata.HasTenantIdColumn)
                                           {%>
                                        <input type="checkbox" checked="checked" name="<%=tableMetadata.Name%>" id="<%=tableMetadata.Name%>" />
                                        <%}     %><%=tableMetadata.Schema%>.<%=tableMetadata.Name %>

                                        <%  }
                                   else
                                   { %><div class="col-md-4 alert" style="margin-bottom: 0px; padding-bottom: 0px;">
                              <i class="<%=alertcss %>" style="padding: 5px"></i>

                              <% if (!tableMetadata.HasTenantIdColumn)
                                 {%>
                              <input type="checkbox" checked="checked" name="<%=tableMetadata.Name%>" id="<%=tableMetadata.Name%>" />
                              <%}     %><%=tableMetadata.Schema%>.<%=tableMetadata.Name %>
                              <%}


                                   if (columnCount == 3 || foreachCount == tableCount)
                                   {
                                       columnCount = 1;
                              %>
                          </div>
                                    </div>
                                    <%}
                                   else
                                   {%>
                                </div>
                                <%
                                         columnCount++;

                                     }
                               }
                                %>
                            </section>
                            <%
                               }

                           } %>

                            <div class="pull-right">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-arrow-right"></i>&nbsp;<%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","next") %></button>
                                    <a href="/Tenant" class="btn btn-default"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","cancel") %></a>
                                </div>
                                <div class="form-group  ">
                                    <label id="Label_TenantDBMigration_TablePrefix" class="optional"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","tablePrefix") %></label><input id="Tenant_DBMigration_TablePrefix" maxlength="50" name="Tenant.DBMigration.TablePrefix" type="text" value=""></div>
                            </div>
                            <div class="pull-left">
                                <i class="fa fa-thumbs-o-up alert alert-success" style="padding: 5px"><span><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","successmsg") %></span></i><br />
                                <i class="fa fa-info-circle alert alert-info" style="padding: 5px"><span><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","warningmsg") %></span></i><br />
                                <i class="fa fa-exclamation-triangle alert alert-warning" style="padding: 5px"><span><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","dangermsg") %></span></i>
                            </div>
                            <br />
                        </form>
                    </div>
                    <div class="tab-pane fade" id="summaryDiv">
                        <form id="dbMigrationFormStep3" action="GenerateScript" method="post">
                            <%if (ViewBag.step.ToString() == "step3")
                              {%>
                            <input type="hidden" name="Tenant.DBMigration.TablePrefix" value="<%=ViewBag.TablePrefix %>" />
                            <table class="celloTable">
                                <thead>
                                    <tr>
                                        <th><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","th1") %></th>
                                        <th><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","th2") %></th>
                                        <th><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","th3") %></th>
                                        <th><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","th4") %></th>
                                    </tr>
                                </thead>

                                <%  int Sno = 1; foreach (var tableMetadata in Model)
                                    {
                                %>
                                <input type="hidden" value="<%=tableMetadata.Name%>" name="<%=tableMetadata.Name%>" id="<%=tableMetadata.Name%>" />
                                <tr>
                                    <td><%=Sno++ %></td>
                                    <td>
                                        <%=tableMetadata.Schema%>.<%=tableMetadata.Name %>
                                    </td>
                                    <td>
                                        <%=tableMetadata.HasTenantIdColumn %>
                                    </td>
                                    <td>
                                        <%=tableMetadata.HasTenantKeyValid %>
                                    </td>
                                </tr>
                                <% }%>
                            </table>

                            <% }%>
                            <br />
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","generateScript") %></button>
                                <a href="/Tenant" class="btn btn-default"><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","cancel") %></a>
                            </div>

                            <br />
                        </form>
                    </div>
                    <div class="tab-pane fade" id="generatedScriptDiv">
                        <form method="post" action="DownloadFile"> 
                        <section class="panel purple">
                            <header class="panel-heading">
                                <h4><%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","tabHeaderStep4") %></h4>
                                <div class="pull-right">
                                <button id="Btn_Download_Script" type="submit"  class="btn btn-danger"> <i class="fa fa-download"></i> <%:this.GetGlobalResourceObject("MultiTenancyEnablementResource","download") %></button>
                         
                            </div>
                            </header>
                            <%if (ViewBag.step.ToString() == "step4")
                              {
                                  string fullQuery="";
                                  %>
                            <div id="Generated_Script" style="padding: 20px; padding-left: 50px">
                                <%foreach (string query in Model)
                                  {
                                      fullQuery += query + "~$"; %>
                                <%=query %><br />
                                <%} Html.Hidden("fullQuery", fullQuery); %>
                                <input type="hidden" id="full_Query" name="full_Quert" value="<%=fullQuery %>" />
                            </div>
                            <%} %>
                        </section>
                            </form>
                    </div>
                </div>
            </div>
        </section>
    </div>



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function () {

            var step = '<%=ViewBag.step%>';
           switch (step) {
               case "step1":
                   $("#connectToServerDiv").addClass("active in");
                   $("#step1").addClass("active");
                   $("#astep2").attr("data-toggle", "");
                   $("#step2").addClass("disabled");
                   $("#astep3").attr("data-toggle", "");
                   $("#step3").addClass("disabled");
                   $("#astep4").attr("data-toggle", "");
                   $("#step4").addClass("disabled");

                   break;
               case "step2":
                   $("#tablesToBeModifyDiv").addClass("active in");
                   $("#step2").addClass("active");
                   $("#astep2").attr("data-toggle", "tab");
                   $("#step2").removeClass("disabled");
                   $("#astep3").attr("data-toggle", "");
                   $("#astep4").attr("data-toggle", "");
                   $("#step4").addClass("disabled");
                   break;
               case "step3":
                   $("#summaryDiv").addClass("active in");
                   $("#step3").addClass("active");
                   $("#astep3").attr("data-toggle", "tab");
                   $("#step3").removeClass("disabled");
                   $("#astep2").attr("data-toggle", "");
                   $("#astep4").attr("data-toggle", "");
                   $("#step2").addClass("disabled");
                   $("#step4").addClass("disabled");
                 
                   break;
               case "step4":
                   $("#generatedScriptDiv").addClass("active in");
                   $("#step4").addClass("active");
                   $("#astep4").attr("data-toggle", "tab");
                   $("#step4").removeClass("disabled");
                   $("#astep2").attr("data-toggle", "");
                   $("#astep3").attr("data-toggle", "");
                   $("#step2").addClass("disabled");
                   $("#step3").addClass("disabled");

                   break;
           }
        
       });
       function checkCheckboxes( id, pID ){

           $('#'+pID).find(':checkbox').each(function(){
              
               jQuery(this).prop('checked', $('#'+id).is(":checked"));
           });     

       }
       $(document).ready(function() {
           $('#selectAll').click(function(event) {  //on click 
               // check select status
               $('#mainDiv').find(':checkbox').each(function(){
                   jQuery(this).prop('checked', $('#selectAll').is(":checked"));       
               });
           });
         
       });


    </script>
    <style>
        .pull-right {
            margin: 10px;
        }
    </style>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopMenu" runat="server">
</asp:Content>
