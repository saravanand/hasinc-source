﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<script src="../../Scripts/knockout-3.1.0.js"></script>
<script type="text/javascript">
    $(function () {
        var settings = {};
            <%
    if (ViewBag.Settings == null)
    {
            %>
        settings.SettingsModel = null;
            <%
    }
    else
    {
       %>
        settings.SettingsModel = JSON.parse('<%=ViewBag.Settings%>');
            <%
    }
    if (ViewBag.LDAPAuthInfo == null)
    {
            %>
        settings.LDAPAuthInfo = null;
            <%
    }
    else
    {
       %>
        settings.LDAPAuthInfo = JSON.parse('<%=ViewBag.LDAPAuthInfo%>');
            <%
    }
    if (ViewBag.LDAPConfig == null)
    {
            %>
        settings.LDAPConfig = null;
            <%
    }
    else
    {
       %>
        settings.LDAPConfig = JSON.parse('<%=ViewBag.LDAPConfig%>');
            <%
    }
    if (ViewBag.AuthSettings == null)
    {
            %>
        settings.AuthSettings = null;
            <%
    }
    else
    {
       %>
        settings.AuthSettings = JSON.parse('<%=ViewBag.AuthSettings%>');
            <%
    }
        %>
        ko.applyBindings(settings);

        var chkRegister = $("#AllowUserRegistration");
        var hdChkRegister = $("#chkRegStatus");

        if ($(chkRegister).is(":checked")) {
            $(hdChkRegister).val(true);
        }
        else {
            $(hdChkRegister).val(false);
        }

        $("#AllowUserRegistration").click(function () {
            if ($(this).is(":checked")) {
                $(hdChkRegister).val(true);
            }
            else {
                $(hdChkRegister).val(false);
            }
        });
    });
</script>
<div class="settings">

    <div class="authinfo" data-bind="with: AuthSettings">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="ProviderName">Authorization Provider Name</label>
                    <input type="hidden" name="ProviderId" data-bind="attr: { value: ProviderId }" />
                    <input type="text" class="form-control" placeholder="Authorization Provider Name"  name="ProviderName" data-bind="attr: { value: ProviderName }" readonly="readonly" />
                </div>
                <div class="form-group">
                    <label for="TypeName">Authorization Type Name</label>
                    <input type="hidden" name="" data-bind="attr: { value: TypeId }" />
                    <input type="text" class="form-control" placeholder="Authorization Type Name"  name="TypeName" data-bind="attr: { value: TypeName }" readonly="readonly" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="DefaultScopes">Default Auth Scopes</label>
                    <input type="text" class="form-control" placeholder="Default Scopes"  name="DefaultScopes" data-bind="attr: { value: DefaultScopes }" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="providers">
    <div data-bind="foreach: SettingsModel">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="hidden" name="AuthTypeId" data-bind="attr: { value: AuthTypeId, name: '[' + $index() + '].AuthTypeId' }" />
                    <input type="hidden" name="AuthTypeName" data-bind="attr: { value: AuthTypeName, name: '[' + $index() + '].AuthTypeName' }" />
                    <label for="" data-bind="text: AuthTypeName"></label>
                    <input type="text" class="form-control" placeholder="Claim Map"  name="ClaimMap" data-bind="attr: { value: ClaimMap, name: '[' + $index() + '].ClaimMap' }" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="AllowUserRegistration">Allow User Self Registration</label>

                <%
                    bool allowRegistration = false;
                    if (ViewBag.AllowUserRegistration != null)
                    {
                        allowRegistration = Convert.ToBoolean(ViewBag.AllowUserRegistration);
                    }

                    if (allowRegistration)
                    {
                %>
                <input type="hidden" name="AllowUserRegistration" id="chkRegStatus" value="true" />
                <input type="checkbox"  id="AllowUserRegistration" checked="checked" />

                <%}
                    else
                    {%>
                <input type="hidden" name="AllowUserRegistration" id="chkRegStatus" value="false" />
                <input type="checkbox"  id="AllowUserRegistration" />
                <% 
                    }
                %>
                <p class="text-info">
                    <i class="fa fa-info-circle"></i>&nbsp;This may affect the billing if user as a usage is defined
                </p>

            </div>
        </div>
    </div>
</div>

<div class="ldapAuthInfo" data-bind="with: LDAPAuthInfo">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="LdapUserName">Ldap User Name</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    <input type="text" class="form-control" placeholder="LDAP User Name"  name="LdapUserName" data-bind=" value: LdapUserName" required="required" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="LdapUserPassword">LDAP Admin User Password</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input type="password" class="form-control" placeholder="LDAP Admin User Password"  name="LdapUserPassword" data-bind="attr: { value: LdapUserPassword }" required="required" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="LdapConnectionString">LDAP Connection String</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input type="password" class="form-control" placeholder="LDAP Connection String"  name="LdapConnectionString" data-bind="attr: { value: LdapConnectionString }" required="required" />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ldapConfig" data-bind="with: LDAPConfig">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="AuthorizationEndPointUri">Authorization EndPoint Uri</label>
                <input type="text" class="form-control" placeholder="Authorization EndPoint Uri"  name="AuthorizationEndPointUri" data-bind="attr: { value: AuthorizationEndPointUri }" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="ProfileEndPointUri">Token EndPoint Uri</label>
                <input type="text" class="form-control" placeholder="Token EndPoint Uri"  name="TokenEndpointUri" data-bind="attr: { value: TokenEndpointUri }" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="ProfileEndPointUri">Profile EndPoint Uri</label>
                <input type="text" class="form-control" placeholder="Profile EndPoint Uri"  name="ProfileEndPointUri" data-bind="attr: { value: ProfileEndPointUri }" />
            </div>
        </div>
    </div>
</div>
