﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Payment.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.TenantManagement.Tenant>" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("h_Pageheading") %></h3>
    </div>
    <div class="sign-up row-fluid pd-25">
        <%
            string billFrequency = BillFrequency.Monthly.ToString("F");
            var userDetails = ViewData["UserDetails"] as UserDetails;
            var tenantDetails = ViewBag.TenantDetails as Tenant;
            var currentLicense = ViewBag.CurrentLicense as TenantLicense;
            var availablePackages = ViewBag.AvailablePackages as List<LicensePackage>;
            var licenseHistory = ViewBag.LicenseHistory as List<TenantLicense> ?? new List<TenantLicense>();
            var pricePlans = ViewBag.PricePlans as Dictionary<Guid, PricePlan>;
            var packagePricePlans = ViewBag.PackagePricePlans as Dictionary<string, List<PricePlan>> ?? new Dictionary<string, List<PricePlan>>();
            var defaultPlans = CelloSaaS.ServiceProxies.Configuration.PickupListProxy.GetAllPickupListValues(CelloSaaSApplication.Models.Constants.DefaultPricePlanPickupListId, UserIdentity.TenantID) ?? new List<CelloSaaS.Model.Configuration.PickupListValue>();

            bool canUpgrade = UserIdentity.HasPrivilege(PrivilegeConstants.SelfUpgradePackage) && ViewData["Error"] == null;
            string membershipId = null, discountCode = null, cssStyle = string.Empty;

            if (ViewBag.DiscountCode != null && !string.IsNullOrEmpty((string)ViewBag.DiscountCode))
            {
                discountCode = (string)ViewBag.DiscountCode;
            }

            if (ViewBag.MembershipId != null && !string.IsNullOrEmpty((string)ViewBag.MembershipId))
            {
                membershipId = (string)ViewBag.MembershipId;
            }

            if (string.IsNullOrEmpty(discountCode))
            {
                cssStyle = "display:none";
            }
        %>
        <% if (ViewData["Success"] != null)
           { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=ViewData["Success"] %>
        </div>
        <% } %>
        <% if (Html.ValidationSummary() != null && Html.ValidationMessage("Success") == null)
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%:Html.ValidationSummary() %>
        </div>
        <% } %>
        <% if (Model != null)
           { %>
        <form id="myDetailsFrom" action="<%=Url.Action("ManageSettings") %>" enctype="multipart/form-data" method="post">
            <% if (canUpgrade)
               { %>
            <%
                   bool showMsg = false, isEvaluation = true;
                   int expireDays = 0;
                   int trialDays = 30;
                   PackageDetails packageDetails = null;
                   if (currentLicense != null)
                   {
                       if (currentLicense.TrialEndDate.HasValue)
                       {
                           expireDays = (int)(currentLicense.TrialEndDate.Value - DateTime.Now).TotalDays;
                           trialDays = (int)(currentLicense.ValidityStart - currentLicense.TrialEndDate.Value).TotalDays;
                       }

                       packageDetails = LicenseProxy.GetPackageDetailsByPackageId(currentLicense.PackageId);
                       isEvaluation = packageDetails.IsEvaluation;
                       showMsg = expireDays < 0 && (isEvaluation || string.IsNullOrEmpty(currentLicense.PricePlanId));
                   }
            %>
            <% if (showMsg)
               { %>
            <div class="alert alert-info">
                <p><%:string.Format(this.GetLocalResourceObject("m_trialinfo").ToString(), trialDays, (isEvaluation ? this.GetLocalResourceObject("m_evaluationPeriod") : this.GetLocalResourceObject("m_trial")))%> </p>
            </div>
            <% } %>
            <section class="panel purple">
                <header class="panel-heading">
                    <h4><%=this.GetLocalResourceObject("h_Subscriptionplan") %></h4>
                    <% if (licenseHistory != null && licenseHistory.Count > 0)
                       {
                    %>
                    <div class="actions pull-right">
                        <a class="btn btn-sm btn-info" href="#licenseHistoryDiv" onclick="showLicenseHistory();" title="<%=this.GetLocalResourceObject("t_ViewDetails") %>"><i class="fa fa-search"></i>&nbsp; <%=this.GetLocalResourceObject("lbl_ViewDetails") %> </a>
                    </div>
                    <% } %>
                </header>
                <div id="packageListingDiv" class="panel-body">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.Label(this.GetLocalResourceObject("lbl_BillingCycle").ToString(), new { @class ="mandatory" })%>
                                    <select id="billingFrequency" onchange="reloadPlans()" style="width: 100%;">
                                        <%
                       string temp = this.Request["frequency"] ?? (ViewBag.PricePlanFrequency != null ? (string)ViewBag.PricePlanFrequency : BillFrequency.Monthly.ToString());
                       var currentFreq = (BillFrequency)Enum.Parse(typeof(BillFrequency), temp);
                       billFrequency = currentFreq.ToString();
                                        %>
                                        <option value="<%: BillFrequency.Monthly%>" <%=currentFreq == BillFrequency.Monthly ? "selected=\"selected\"" : "" %>><%: BillFrequency.Monthly %></option>
                                        <option value="<%: BillFrequency.Quarterly %>" <%=currentFreq == BillFrequency.Quarterly ? "selected=\"selected\"" : "" %>><%: BillFrequency.Quarterly %></option>
                                        <option value="<%: BillFrequency.Semiannual %>" <%=currentFreq == BillFrequency.Semiannual ? "selected=\"selected\"" : "" %>><%: BillFrequency.Semiannual %></option>
                                        <option value="<%: BillFrequency.Annual %>" <%=currentFreq == BillFrequency.Annual ? "selected=\"selected\"" : "" %>><%: BillFrequency.Annual %></option>
                                    </select>
                                    <%: Html.Hidden("BillFrequency",billFrequency) %>
                                </div>
                            </div>
                            <%--<div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.Label(this.GetLocalResourceObject("lbl_DiscountCode").ToString())%>
                                    <%: Html.TextBox("DiscountCode", discountCode,new { onblur = "showmembership()" })%>
                                </div>
                            </div>--%>
                        </div>
                        <div class="row">
                            <%=Html.Hidden("PackageId", currentLicense != null ? currentLicense.PackageId : string.Empty) %>
                            <%=Html.Hidden("PricePlanId", currentLicense != null ? currentLicense.PricePlanId : string.Empty) %>
                            <% if (availablePackages != null && availablePackages.Count > 0 && packagePricePlans.SelectMany(x => x.Value).Count() > 0)
                               {
                                   var colors = new string[] { "blue", "indigo", "purple", "green", "lt-green", "lt-blue" };
                                   bool isDiscounted = false;
                                   int cnt = 0;
                                   string selectedPackageId = currentLicense.PackageId;
                                   foreach (var item in availablePackages.OrderBy(x => x.Name))
                                   {
                                       PricePlan plan = null;
                                       string color = "orange selected";
                                       List<PricePlan> plans = packagePricePlans.ContainsKey(item.LicensepackageId) ? packagePricePlans[item.LicensepackageId] : null;

                                       if (plans != null && plans.Count > 0)
                                       {
                                           if (!string.IsNullOrEmpty(billFrequency))
                                           {
                                               plans = plans.Where(x => x.BillFrequency.ToString().Equals(billFrequency)).ToList();
                                           }

                                           var matchedPlans = string.IsNullOrEmpty(currentLicense.PricePlanId) ? null : plans.Where(p => p.Id == Guid.Parse(currentLicense.PricePlanId));

                                           if (matchedPlans != null && matchedPlans.Count() > 0)
                                           {
                                               plan = matchedPlans.Single();
                                               string discountedPlanId = ViewBag.DiscountedPricePlanId != null && !string.IsNullOrEmpty(ViewBag.DiscountedPricePlanId as string)
                                                                            ? (string)ViewBag.DiscountedPricePlanId
                                                                            : null;

                                               if (!string.IsNullOrEmpty(discountedPlanId) && plan.Id.ToString().Equals(discountedPlanId, StringComparison.OrdinalIgnoreCase))
                                               {
                                                   isDiscounted = true;
                                               }
                                           }
                                           else
                                           {
                                               if (defaultPlans.Count > 0)
                                               {
                                                   // if default plans available then check if this plan is available
                                                   var dpid = defaultPlans.Where(d => d.ItemId == plan.Id.ToString()).FirstOrDefault();

                                                   if (dpid == null) continue;

                                                   plan = plans.Where(x => x.Id.ToString() == dpid.ItemId).FirstOrDefault();
                                               }
                                               else
                                               {
                                                   plan = plans.OrderBy(x => x.CreatedOn).FirstOrDefault();
                                               }
                                           }
                                       }

                                       if (plan == null)
                                       {
                                           continue;
                                       }

                                       string cssClass = string.IsNullOrEmpty(selectedPackageId)
                                           || !selectedPackageId.Equals(item.LicensepackageId, StringComparison.OrdinalIgnoreCase)
                                            ? "" : "selected";

                                       if (string.IsNullOrEmpty(selectedPackageId) || !selectedPackageId.Equals(item.LicensepackageId, StringComparison.OrdinalIgnoreCase))
                                       {
                                           color = colors[cnt];
                                       }
                                       cnt++;
                                       if (cnt >= colors.Length) cnt = 0;
                            %>
                            <div class="col-md-3">
                                <div class="plan tiles <%: color%> <%:plan.BillFrequency.ToString() %>" id="<%: item.LicensepackageId %>" onclick="showAsSelected('<%: item.LicensepackageId %>','<%=plan.Id %>')">
                                    <div class="tiles-body">
                                        <div class="tiles-title">
                                            <%= item.Name %> - <%:plan.BillFrequency.ToString() %>
                                        </div>
                                        <div class="heading planPrice" data-id="<%=plan.Id %>">
                                            <% if (plan.Price > 0)
                                               { %>
                                            <span class="animate-number" data-animation-duration="1200" data-value="<%:plan.Price %>">
                                                <%: (plan.Price %1 ==0) ? plan.Price.ToBillingCurrency("C0") : plan.Price.ToBillingCurrency("C2")%>
                                            </span>
                                            <% } %>
                                        </div>
                                        <div class="description">
                                            <% if (isDiscounted)
                                               {
                                            %>
                                            <i class="fa fa-info-circle text-info" title="<%: this.GetLocalResourceObject("t_DiscountedPrice")%>"></i>&nbsp;
                                            <%  isDiscounted = false;
                                               } 
                                            %>
                                            <%= HttpUtility.HtmlDecode(string.IsNullOrWhiteSpace(plan.Description) ? item.Description : plan.Description) %>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <% if (cnt == 0 || cnt == 3)
                               {%>
                        </div>
                        <br />
                        <div class="row">
                            <%}

                                   }
                               }
                               else
                               {
                            %>
                            <% if (currentLicense != null)
                               {
                                   if (packageDetails == null)
                                       packageDetails = LicenseProxy.GetPackageDetailsByPackageId(currentLicense.PackageId);
                                   string color = "orange selected";
                                   var plan = !string.IsNullOrEmpty(currentLicense.PricePlanId) && pricePlans.ContainsKey(Guid.Parse(currentLicense.PricePlanId)) ? pricePlans[Guid.Parse(currentLicense.PricePlanId)] : null;
                                   var item = packageDetails;
                            %>
                            <div class="col-md-3">
                                <div class="plan tiles <%: color%>" id="<%: item.PackageId %>" onclick="showAsSelected('<%: item.PackageId %>','<%=plan != null ? plan.Id.ToString() : "" %>')">
                                    <div class="tiles-body">
                                        <div class="tiles-title">
                                            <%:item.PackageName %><%:plan != null ? (" - " + plan.BillFrequency.ToString()) : "" %>
                                        </div>
                                        <div class="heading planPrice" data-id="<%=plan != null ? plan.Id.ToString() : "" %>">
                                            <% if (plan != null && plan.Price > 0)
                                               { %>
                                            <span class="animate-number" data-animation-duration="1200" data-value="<%:plan.Price %>">
                                                <%: (plan.Price %1 ==0) ? plan.Price.ToBillingCurrency("C0") : plan.Price.ToBillingCurrency("C2")%>
                                            </span>
                                            <% } %>
                                        </div>
                                        <div class="description">
                                            <%= HttpUtility.HtmlDecode((plan != null && string.IsNullOrEmpty(plan.Description)) ? plan.Description : item.PackageDescription) %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <% }
                               else
                               { %>
                            <div class="alert alert-info">
                                <%:string.Format(this.GetLocalResourceObject("e_NoPlans").ToString(),billFrequency)%>
                            </div>
                            <% } %>
                            <% } %>
                        </div>
                    </div>
                </div>
            </section>
            <% }
               else
               { %>
            <%=Html.Hidden("PackageId", currentLicense != null ? currentLicense.PackageId : string.Empty) %>
            <%=Html.Hidden("PricePlanId", currentLicense != null ? currentLicense.PricePlanId : string.Empty) %>
            <%=Html.Hidden("BillFrequency", billFrequency) %>
            <% } %>
            <section class="panel indigo">
                <header class="panel-heading">
                    <h4>
                        <%=this.GetLocalResourceObject("h_CompanyDetails") %>
                    </h4>
                </header>
                <div class="panel-body" id="companydetails">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.HiddenFor(model=> model.Identifier) %>
                                    <%: Html.HiddenFor(model=> model.TenantDetails.Identifier) %>
                                    <%: Html.HiddenFor(model=> model.ContactDetail.Identifier) %>
                                    <%: Html.HiddenFor(model=> model.TenantDetails.TenantCodeString) %>
                                    <%: Html.HiddenFor(model => model.Address.AddressId)%>
                                    <%: Html.LabelFor(model => model.TenantDetails.TenantName,this.GetLocalResourceObject("lbl_TenantName").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.TenantDetails.TenantName) %>
                                    <%= Html.CelloValidationMessage("TenantDetails.TenantName","*")%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.TenantDetails.Description,  this.GetLocalResourceObject("lbl_Description").ToString())%>
                                    <%: Html.TextAreaFor(model=> model.TenantDetails.Description) %>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.TenantDetails.Website, this.GetLocalResourceObject("lbl_Website").ToString())%>
                                    <%: Html.TextBoxFor(model=> model.TenantDetails.Website) %>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.TenantDetails.CompanySize,  this.GetLocalResourceObject("lbl_CompanySize").ToString(), new { })%>
                                    <%: Html.DropDownListFor(model=> model.TenantDetails.CompanySize,(IEnumerable<SelectListItem>)ViewData["CompanySize"], new { style="width:100%;" }) %>
                                    <%= Html.CelloValidationMessage("TenantDetails.CompanySize","*")%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.ContactDetail.FirstName,this.GetLocalResourceObject("lbl_FirstName").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model => model.ContactDetail.FirstName, new { required = "required" })%>
                                    <%= Html.CelloValidationMessage("ContactDetail.FirstName","*")%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.ContactDetail.LastName,  this.GetLocalResourceObject("lbl_LastName").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.ContactDetail.LastName, new { required = "required" }) %>
                                    <%= Html.CelloValidationMessage("ContactDetail.LastName","*")%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.ContactDetail.Email, this.GetLocalResourceObject("lbl_EmailId").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.ContactDetail.Email, new { required = "required",tooltip = Resources.TenantResource.m_sampleEmail, placeholder = Resources.TenantResource.m_sampleEmail }) %>
                                    <%= Html.CelloValidationMessage("ContactDetail.Email","*")%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.ContactDetail.Phone, this.GetLocalResourceObject("lbl_PhoneNumber").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.ContactDetail.Phone, new { required = "required",tooltip = Resources.TenantResource.m_samplePhone, placeholder = Resources.TenantResource.m_samplePhone}) %>
                                    <%= Html.CelloValidationMessage("ContactDetail.Phone","*")%>
                                    <div>e.g.:  +1-234-567-8901 or +1 (234) 56 89 901</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="memberdiscounts">
                                    <%: Html.Label("Membership Id")%>
                                    <%: Html.TextBox("MembershipId", membershipId)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel blue">
                <header class="panel-heading">
                    <h4><%=this.GetLocalResourceObject("h_BillingAddress") %></h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <% if (Model.Address == null) this.Model.Address = new Address(); %>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.Address.Address1,this.GetLocalResourceObject("lbl_Address1").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.Address.Address1, new { required = "required" }) %>
                                    <%= Html.CelloValidationMessage("Address.Address1","*")%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.Address.Address2,this.GetLocalResourceObject("lbl_Address2").ToString())%>
                                    <%: Html.TextBoxFor(model=> model.Address.Address2) %>
                                    <%= Html.CelloValidationMessage("Address.Address2","*")%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.Address.City,this.GetLocalResourceObject("lbl_City").ToString() ,new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.Address.City, new { required = "required" }) %>
                                    <%= Html.CelloValidationMessage("Address.City","*")%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.Address.State, this.GetLocalResourceObject("lbl_State").ToString(), new { @class = "mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.Address.State, new { required = "required" }) %>
                                    <%= Html.CelloValidationMessage("Address.State","*")%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.Address.CountryId, this.GetLocalResourceObject("lbl_Country").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.DropDownList("Address.CountryId", null, new { style="width:100%;" })%>
                                    <%= Html.CelloValidationMessage("Address.CountryId", "*")%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%: Html.LabelFor(model => model.Address.PostalCode, this.GetLocalResourceObject("lbl_ZipCode").ToString(), new { @class ="mandatory" })%>
                                    <%: Html.TextBoxFor(model=> model.Address.PostalCode, new { required = "required" }) %>
                                    <%= Html.CelloValidationMessage("Address.PostalCode","*")%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel green">
                <header class="panel-heading">
                    <h4><%=this.GetLocalResourceObject("h_Settings") %></h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><%=this.GetLocalResourceObject("lbl_Theme") %></label>
                                    <%=Html.DropDownList("Theme", (IEnumerable<SelectListItem>)ViewData["Themes"], new { style="width:100%" })%>
                                </div>
                                <div class="form-group">
                                    <label><%=this.GetLocalResourceObject("lbl_Logo") %></label>
                                    <input id="logoPath" name="logoPath" value="" type="file" contenteditable="false" />
                                    <%= Html.Hidden("SavedImagePath", ViewData["ImageURL"]) %>
                                    <p class="text-info">
                                        <i class="fa fa-info-circle"></i>&nbsp;<%=this.GetLocalResourceObject("info_Logo") %>&nbsp;
                                         <%: this.GetLocalResourceObject("SupportedFormats")%>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label><%=this.GetLocalResourceObject("lbl_LogoPreview") %></label><br />
                                <% if (ViewData["ImageURL"] != null)
                                   { %>
                                <img src="<%=Url.Content("~/" + ViewData["ImageURL"].ToString())%>" alt="<%=this.GetLocalResourceObject("lbl_Logo") %>" />
                                <%} %>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <% if (CelloSaaS.Library.Helpers.ConfigHelper.EnableExternalAuthentication)
               { %>
            <section class="panel red">
                <header class="panel-heading">
                    <h4>Identity & Authorization Settings</h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <% Html.RenderPartial("AuthSettings"); %>
                    </div>
                </div>
            </section>
            <% } %>
            <div class="pull-right">
                <button type="submit" class="btn btn-info" onclick="ValidateAndSave()"><i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></button>
            </div>
        </form>
        <% Html.RenderPartial("PartialLicenseHistory", licenseHistory); %>
        <% } %>
    </div>
    <script type="text/javascript">
        function showAsSelected(selectedItemId, planId) {
            $('.plan.selected').removeClass('selected');
            $('.plan').attr("title", '<%=this.GetLocalResourceObject("t_PlanTitle")%>');
            $("#PackageId").val(selectedItemId);
            $('#PricePlanId').val(planId);
            var el = $('.planPrice[data-id="' + planId + '"]').closest('.plan');
            el.addClass("selected");
            el.attr("title", "<%=this.GetLocalResourceObject("t_PlanTitle1")%>");
        }

        function showLicenseHistory() {
            $('#tenantLicenseHistory').modal('show');
        }

        function ValidateAndSave() {
            if ($("#AllowUserRegistration").is(":checked")) {
                if (confirm('CAUTION: Allowing User Self-Signup option may affect the billing if approved')) {
                    return true;
                }
                return false;
            }
            return true;
        }

        $(function () {
            $('select').select2();
            $('#tenantLicenseHistory').modal({ show: false });
            $('.plan').attr("title", "<%=this.GetLocalResourceObject("t_PlanTitle2")%>");

            var id = $("#PackageId").val();
            var planId = $('#PricePlanId').val();
            if (id && planId) {
                $('.plan.selected').removeClass('selected');
                var el = $('.planPrice[data-id="' + planId + '"]').closest('.plan');
                el.addClass("selected");
                el.attr("title", "<%=this.GetLocalResourceObject("t_PlanTitle1")%>");
            }
            showmembership();
        });


        function reloadPlans() {
            var baseUrl = '<%=Url.Action("ManageSettings")%>';
            var selectedOption = $("#billingFrequency").val();
            $("#BillFrequency").val(selectedOption);
            window.location = baseUrl + "?frequency=" + selectedOption;
        }

        function showmembership() {
            if ($("#DiscountCode").length > 0 && $("#DiscountCode").val().length > 0) {
                $("#memberdiscounts").show();
            }
            else {
                $("#memberdiscounts").hide();
                $("#MembershipId").val('');
            }
        }
    </script>
    <style>
        .selected {
            border: 4px solid #ff0056;
        }

            .selected .tiles-body {
                padding: 15px 15px 11px 20px;
            }
    </style>
</asp:Content>

