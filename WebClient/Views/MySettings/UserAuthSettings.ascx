﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="UserSettings">
    <div class="userAuthinfo">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="ProviderName">Authorization Provider Name</label>
                    <input type="text" class="form-control" placeholder="Authorization Provider Name" name="" value="<%: ViewBag.IdentityProvider as string %>" readonly="readonly" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="TypeName">Authorization Type Name</label>
                    <input type="text" class="form-control" placeholder="Authorization Type Name" name="" value="<%: ViewBag.AuthenticationType as string %>" readonly="readonly" />
                </div>
            </div>
        </div>
    </div>
</div>
