﻿<%@ Page Title="<%$ Resources:h_MyDetails %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.UserManagement.UserDetails>" %>

<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row-fluid pd-25">
        <div class="divide-20"></div>
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (Html.ValidationSummary() != null && Html.ValidationMessage("Success") == null)
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%:Html.ValidationSummary() %>
        </div>
        <% } %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%=this.GetLocalResourceObject("h_MyDetails") %></h4>
            </header>
            <div class="panel-body">
                <% if (this.Model != null)
                   { %>
                <% var userDetails = this.Model; %>
                <% using (Html.BeginForm())
                   { %>
                <div class="form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label( this.GetLocalResourceObject("lbl_FirstName").ToString(), new { @class ="mandatory" }) %>
                                <%: Html.TextBox("UserDetails.User.FirstName", userDetails.User.FirstName)%>
                                <%=Html.CelloValidationMessage("UserDetails.User.FirstName","*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label(this.GetLocalResourceObject("lbl_LastName").ToString(), new { @class ="mandatory" }) %>
                                <%: Html.TextBox("UserDetails.User.LastName", userDetails.User.LastName)%>
                                <%=Html.CelloValidationMessage("UserDetails.User.LastName","*")%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label( "User Name") %>
                                <%: Html.TextBox("userDetails.MembershipDetails.UserName", userDetails.MembershipDetails.UserName, new { @readonly = "readonly" })%>
                                <%=Html.CelloValidationMessage("UserDetails.MembershipDetails.UserName","*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label("Email") %>
                                <%: Html.TextBox("userDetails.MembershipDetails.EmailId", userDetails.MembershipDetails.EmailId, new { @readonly = "readonly" })%>
                                <%=Html.CelloValidationMessage("userDetails.MembershipDetails.EmailId","*")%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label(this.GetLocalResourceObject("lbl_Address").ToString())%>
                                <%: Html.TextBox("UserDetails.Address.Address1", userDetails.Address !=null ? userDetails.Address.Address1:string.Empty)%>
                                <%=Html.CelloValidationMessage("UserDetails.Address.Address1", "*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label(this.GetLocalResourceObject("lbl_City").ToString())%>
                                <%: Html.TextBox("UserDetails.Address.City", userDetails.Address !=null ?userDetails.Address.City:string.Empty)%>
                                <%=Html.CelloValidationMessage("UserDetails.Address.City", "*")%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label(this.GetLocalResourceObject("lbl_State").ToString())%>
                                <%: Html.TextBox("UserDetails.Address.State", userDetails.Address !=null ?userDetails.Address.State:string.Empty)%>
                                <%=Html.CelloValidationMessage("UserDetails.Address.State", "*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label(this.GetLocalResourceObject("lbl_Country").ToString())%>
                                <%: Html.DropDownList("UserDetails.Address.CountryId", null, new { style="width:100%;" })%>
                                <%= Html.CelloValidationMessage("UserDetails.Address.CountryId", "*")%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <%: Html.Label(this.GetLocalResourceObject("lbl_PostalCode").ToString())%>
                                <%: Html.TextBox("UserDetails.Address.PostalCode",userDetails.Address !=null ?userDetails.Address.PostalCode:string.Empty) %>
                                <%= Html.CelloValidationMessage("UserDetails.Address.PostalCode", "*")%>
                            </div>
                        </div>
                    </div>
                    <%if (CelloSaaS.Library.Helpers.ConfigHelper.EnableExternalAuthentication)
                      { %>
                    <% Html.RenderPartial("UserAuthSettings"); %>
                    <%} %>
                    <% if (string.IsNullOrEmpty(UserIdentity.SessionTenantID))
                       { %>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></button>
                    </div>
                    <% }
                   } %>
                </div>
                <% } %>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select').select2();
        });
    </script>
</asp:Content>
