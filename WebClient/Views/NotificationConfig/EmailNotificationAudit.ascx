﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    CelloSaaS.Notification.Model.NotificationAuditSearchCondition condition = (ViewBag.Condition as CelloSaaS.Notification.Model.NotificationAuditSearchCondition) ?? new CelloSaaS.Notification.Model.NotificationAuditSearchCondition();

    string sortExpression = (string)ViewData["SortExpression"];
    string sortString = (string)ViewData["SortString"];
    
    var routeValues = new
    {
        NotificationName = condition.NotificationName,
        NotificationId = condition.NotificationId,
        DestinationType = condition.DestinationType,
        TenantId = condition.TenantId,
        StartDate = condition.StartDate,
        EndDate = condition.EndDate
    };

    var audits = ViewData["NotificationAudit"] as IEnumerable<CelloSaaS.Notification.Model.NotificationAudit>;

    if (audits != null && audits.Count() > 0)
    { %>
<div style="max-height: 500px; overflow: auto; overflow-y: auto;">
    <% foreach (var item in audits)
       {
           var dest = ((CelloSaaS.Notification.Model.Dispatch.EmailDestination)item.NotificationDestination);
           var content = ((CelloSaaS.Notification.Model.Content.TextMessageContent)item.NotificationContent);
    %>
    <div class="email-item" data-id="<%:item.Id %>">
        <div class="email-name">
            <%:item.NotificationDetails.NotificationName %>
            <small>
                <% if (item.NotificationStatus == CelloSaaS.Notification.Model.NotificationStatus.Success)
                   { %>
                <i class="fa fa-check-circle text-success" title="<%:this.GetLocalResourceObject("t_Successfullysent")%>"></i>
                <% }
                   else if (item.NotificationStatus == CelloSaaS.Notification.Model.NotificationStatus.Failure)
                   { %>
                <i class="fa fa-exclamation-circle text-danger" title="<%:this.GetLocalResourceObject("t_Failureinsendingemail")%>"></i>
                <% } %>
            </small>
            <div class="email-sent-date"><%:item.CreatedOn.ToUIDateTimeString()%></div>
        </div>
        <div class="email-to-address"><i class="fa fa-envelope"></i>&nbsp;<%:dest.RecipientAddress %></div>
        <div class="email-subject"><%:content.Subject%></div>
        <div class="email-body" id="email-body-<%:item.Id %>">
            <%=content.ProcessedContent %>
        </div>
    </div>
    <% } %>
</div>
<% Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "notificationAuditDetails", LoadingElementDuration = 500, LoadingElementId = "loading" }, routeValues)
                   .SetSortValues(sortString, sortExpression)
                   .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
                   .Render(); %>
<% }
    else
    { %>
<div class="alert alert-info">
    <%:this.GetLocalResourceObject("e_DetailsNotAvailable")%>
</div>
<% }%>
