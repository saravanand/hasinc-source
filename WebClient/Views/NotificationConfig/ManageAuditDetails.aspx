﻿<%@ Page Title="<%$ Resources:Titile %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#ManageNotificationAuditPage select').select2();

            ShowAuditDetails();

            $('#StartDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#EndDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();
                var queryString = 'pageSize=' + $(this).val();
                queryString += '&sortString=' + (sortString || '');
                queryString += '&sortDirection=' + (sortDirection || '');
                queryString += '&' + $('#ManageNotificationAuditPage').serialize();

                $.ajax({
                    type: "POST",
                    data: queryString,
                    success: function (data) {
                        $('#show_email_details').html('<div class="alert alert-info">Select an email from the left pane!</div>');
                        $('#notificationAuditDetails').fadeOut().html(data).slideDown();
                    },
                    error: function () {
                        $('#notificationAuditDetails').html('<div class="alert alert-danger"><%: this.GetLocalResourceObject("Error")%></div>').fadeIn();
                    }
                });

                $('div.alert').remove();
            });

            $(document).on('click', '.email-item', function () {
                $('#notificationAuditDetails .email-item.active').removeClass('active');
                $(this).addClass('active');
                var id = $(this).data('id');
                var body = $('#email-body-' + id).html();
                $('#show_email_details').html(body).show();
            });
        });

        function addPopup() {
            $("#PopUpContainer").dialog({
                modal: true,
                height: 300,
                width: 800
            });
        }

        var notificationAuditUrl = "MyNotificationDetails";

        function ShowAuditDetails() {
            notificationType = $('#DestinationType').val();
            if (notificationType == -1) {
                $('#scriptValidationError').html('<div><%: this.GetLocalResourceObject("e_SelectType")%></div>').fadeIn();
            } else {
                serializedForm = $('#ManageNotificationAuditPage').serialize();
                $.post(notificationAuditUrl, serializedForm, function (data) {
                    $('#scriptValidationError').fadeOut();
                    $('#notificationAuditDetails').html(data).show();
                    $('#show_email_details').html('<div class="alert alert-info"><%: this.GetLocalResourceObject("m_SelectEmail")%></div>');
                });
            }
        }

        function ShowContentDetails(NotificationId) {
            $.get("ShowContentDestinationDetails" + "?NotificationId=" + NotificationId, function (data) {
                addPopup();
                $('#scriptValidationError').fadeOut();
                $('#PopUpContainer').html(data);
                $('#PopUpContainer').dialogdialog("open");
            });
        }

        function TypeChange() {
            var notificationType = $('#DestinationType').val();
            if (notificationType == 'SystemNotification' || notificationType == 'BatchSystemNotification') {
                $("#MapId").val("");
                $('.systemNotificationPart').fadeIn();
            }
            else {
                $('.systemNotificationPart').fadeOut();
            }
        }

        function formReset() {
            $('#ManageNotificationAuditPage')[0].reset();
            $('#ManageNotificationAuditPage select').trigger('change');
            $("#StartDate").datepicker("option", "maxDate", new Date());
            $("#EndDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#StartDate").datepicker("option", "minDate", new Date(2012, 01, 01));
            $("#EndDate").datepicker("option", "maxDate", new Date());
            ShowAuditDetails();
        }
    </script>
    <style type="text/css">
        /* css for timepicker */
        .ui-timepicker-div .ui-widget-header {
            margin-bottom: 8px;
        }

        .ui-timepicker-div dl {
            text-align: left;
        }

            .ui-timepicker-div dl dt {
                height: 25px;
                margin-bottom: -25px;
            }

            .ui-timepicker-div dl dd {
                margin: 0 10px 10px 65px;
            }

        .ui-timepicker-div td {
            font-size: 90%;
        }

        .ui-tpicker-grid-label {
            background: none;
            border: none;
            margin: 0;
            padding: 0;
        }

        .email-name {
            position: relative;
        }

        .email-sent-date {
            font-size: 10px;
            font-style: italic;
            position: absolute;
            right: 3px;
            top: 0;
        }

        .email-to-address {
            color: #333333;
        }

        .email-subject {
            color: #999999;
            text-indent: 18px;
        }

        .email-body {
            display: none;
        }

        .email-item {
            padding-bottom: 3px;
            padding-top: 5px;
            border-bottom: 1px solid #cccccc;
            cursor: pointer;
        }

            .email-item.active {
                background-color: lightyellow;
            }

        form.form-inline label {
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="center_container">
        <div class="page-title">
            <% if (ViewData["MyDetails"] == null)
               { %>
            <h3>
                <%:this.GetLocalResourceObject("NotificationAuditDetails")%>
            </h3>
            <% }
               else
               { %>
            <h3>
                <%:this.GetLocalResourceObject("MyNotificationDetails")%></h3>
            <% } %>
        </div>
        <div class="row-fluid pd-25">
            <div id="ManageAuditDetails" class="row">
                <% Html.RenderPartial("ManageNotificationAudit"); %>
            </div>
            <div class="row">
                <div id="scriptValidationError" class="error" style="display: none">
                </div>
                <div class="grid simple horizontal green">
                    <div class="grid-title no-border-side">
                        <h4><%: this.GetLocalResourceObject("SearchResult")%></h4>
                    </div>
                    <div class="grid-body no-border">
                        <div class="row">
                            <div id="notificationAuditDetails" class="col-md-5 pd-0">
                                <div class="text-info"><%: this.GetLocalResourceObject("LoadingInfo")%></div>
                            </div>
                            <div id="show_email_details" class="col-md-7" style="max-height: 550px; overflow: auto; overflow-y: auto;">
                                <div class="alert alert-info"><%: this.GetLocalResourceObject("m_SelectEmail")%></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="PopUpContainer" style="display: none;">
            </div>
        </div>
    </div>
</asp:Content>
