﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Notification.Model.Content" %>
<%@ Import Namespace="CelloSaaS.Notification.Model.Dispatch" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    TextMessageContent content = (TextMessageContent)ViewData["EmailContent"];
    EmailDestination destination = (EmailDestination)ViewData["EmailDestination"];
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "ManageEmailDetails";
   using (Ajax.BeginForm("ManageEmailDetails", "NotificationConfig", new { }, ajaxOption, new { id = "ManageEmailDetailsPage" }))
   {
%>
<%if (!Convert.ToBoolean(ViewData["IsGlobal"]))
  { %>
<section class="panel green">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("ManageEmailDetails")%>
            <span class="semi-bold">&nbsp;:&nbsp;<%=Html.Display("NotificationName", ViewData["NotificationName"])%></span>
        </h4>
        <div class="actions pull-right">
            <%if (destination != null && !string.IsNullOrEmpty(destination.NotificationDestinationId))
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:UpdateEmailDetails();" title="<%=this.GetGlobalResourceObject("General","Update") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Update") %></span></a>
            <%}
              else
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:ManageEmailDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Save") %></span></a>
            <%} %>
            <a class="btn btn-sm btn-default" href="#" onclick="javascript:EmailDispatchCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Cancel") %></span></a>
            <a class="btn btn-sm btn-warning" href="#" onclick="javascript:ClearEmailDetails();" title="<%=this.GetGlobalResourceObject("General","Clear") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General", "Clear")%></span></a>
        </div>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <%} %>
        <div class="form-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("NotificationType")%>
                        </label>
                        <%= Html.Hidden("notificationId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("NotificationMasterId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("NotificationDestinationId", destination.NotificationDestinationId)%>
                        <%= Html.Hidden("NotificationContentId", content.NotificationContentId)%>
                        <%= Html.Hidden("NotificationType", ViewData["NotificationType"])%>
                        <%= Html.Hidden("SSL", destination.EnableSSL)%>
                        <%= Html.Hidden("IsGlobalConfig", ViewData["GlobalConfig"])%>
                        <br />
                        <%= Html.Display("NotificationType", ViewData["NotificationType"])%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("SenderAddress")%>
                        </label>
                        <%= Html.TextBox("SenderAddress", destination.SenderAddress, new { maxlength = 255, style = "width:100%;" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("SmtpAddress")%>
                        </label>
                        <%= Html.TextBox("SmtpAddress", destination.SmtpAddress, new { maxlength = 255, style = "width:100%;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("SmtpUserName")%>
                        </label>
                        <%= Html.TextBox("SmtpUserName", destination.SmtpUserName, new { maxlength = 255, style = "width:100%;" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("SmtpPassword")%>
                        </label>
                        <%= Html.Password("SmtpPassword", destination.SmtpPassword, new { maxlength = 255, style = "width:100%;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("PortNumber")%>
                        </label>
                        <%= Html.TextBox("PortNumber", destination.PortNumber, new { maxlength = 255, style = "width:100%;" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("EnableSSL")%>
                        </label>
                        <%=Html.CheckBox("EnableSSL", destination.EnableSSL)%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4><b><%:this.GetLocalResourceObject("AddEmailContent")%></b></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("Subject")%>
                        </label>
                        <%= Html.TextBox("Subject", content.Subject, new { maxlength = 255, style = "width:100%;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("AttachmentFolder")%>
                        </label>
                        <%= Html.TextBox("AttachmentFileFolderPath", content.AttachmentFileFolderPath, new { maxlength = 255, style = "width:100%;" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("HasTemplate")%>
                        </label>
                        <%=Html.CheckBox("HasTemplate",Convert.ToBoolean(ViewData["HasTemplate"]))%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("AttachmentFiles")%>
                        </label>
                        <%= Html.TextArea("AttachmentFiles", content.AttachmentFiles != null ? string.Join(",", content.AttachmentFiles) : null, new { style = "width:100%;", onKeyDown = "limitText(this,1000)", onKeyUp = "limitText(this,1000)" })%>
                    </div>
                </div>
            </div>
            <div class="row" id="ContentPart" style="<%=(content.Template == null) ? "": "display:none;" %>">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("Content")%>
                        </label>
                        <%= Html.TextArea("TextContent", ViewData["TextContent"] != null ? ViewData["TextContent"].ToString() : null, new { @class = "RichTextEditor", rows="10" })%>
                    </div>
                </div>
            </div>
            <div class="row" id="TemplatePart" style="<%=(!Convert.ToBoolean(ViewData["HasTemplate"]) && ((content.Content != null && content.Template == null) || string.IsNullOrEmpty(content.NotificationContentId))) ? "display:none;": "" %>">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("RuleSetCode")%>
                        </label>
                        <div class="col-md-11 pd-0">
                            <%= Html.TextBox("RuleSetCode", content.RuleSetCode, new { style="width:100%" })%>
                        </div>
                        <div class="col-md-1 pd-0">
                            <a href="#" onclick="javascript:ManageRules();" title="<%=this.GetLocalResourceObject("ManageRule") %>">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("DefaultTemplate")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                            <%= Html.Hidden("NotificationTemplateId", ViewData["NotificationTemplateId"])%>
                        </label>
                        <div class="col-md-11 pd-0">
                            <%= Html.DropDownList("Templates", null, "---Select---", new { style="width:100%" })%>
                        </div>
                        <div class="col-md-1 pd-0">
                            <a href="#" onclick="javascript:ManageTemplate();" title="<%=this.GetLocalResourceObject("ManageTemplate") %>">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <%if (destination != null && !string.IsNullOrEmpty(destination.NotificationDestinationId))
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:UpdateEmailDetails();" title="<%=this.GetGlobalResourceObject("General","Update") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Update") %></span></a>
            <%}
              else
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:ManageEmailDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Save") %></span></a>
            <%} %>
            <a class="btn btn-default" href="#" onclick="javascript:EmailDispatchCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
            <a class="btn btn-warning" href="#" onclick="javascript:ClearEmailDetails();" title="<%=this.GetGlobalResourceObject("General","Clear") %>">
                <%=this.GetGlobalResourceObject("General", "Clear")%></a>
        </div>
    </div>
</section>
<%}
  else
  { %>
<section class="panel indigo">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("ManageEmailDetails")%>
            <span class="semi-bold">&nbsp;:&nbsp;<%=Html.Display("NotificationName", ViewData["NotificationName"])%></span>
        </h4>
        <div class="actions pull-right">
            <a class="btn btn-sm btn-warning" href="#" onclick="javascript:OverrideDetails('#ManageEmailDetailsPage');" title="<%=this.GetGlobalResourceObject("General","OverrideDetails") %>">
                <%=this.GetGlobalResourceObject("General","OverrideDetails") %></a>
            <a class="btn btn-sm btn-default" href="#" onclick="javascript:EmailDispatchCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <%} %>
        <div class="form-container">
            <table style="width: 100%;">
                <colgroup>
                    <col style="width: 200px;" />
                    <col style="width: auto;" />
                    <col style="width: 200px;" />
                    <col style="width: auto;" />
                </colgroup>
                <tbody>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("NotificationType")%>
                            </label>
                            <%= Html.Hidden("notificationId", ViewData["NotificationMasterId"])%>
                            <%= Html.Hidden("NotificationContentDetailsId", ViewData["NotificationContentDetailsId"])%>
                            <%= Html.Hidden("NotificationType", ViewData["NotificationType"])%>
                            <%= Html.Hidden("NotificationName", ViewData["NotificationName"])%>
                            <%= Html.Hidden("SenderAddress", destination.SenderAddress)%>
                            <%= Html.Hidden("SmtpAddress", destination.SmtpAddress)%>
                            <%= Html.Hidden("SmtpUserName", destination.SmtpUserName)%>
                            <%= Html.Hidden("SmtpPassword", destination.SmtpPassword)%>
                            <%= Html.Hidden("PortNumber", destination.PortNumber)%>
                            <%= Html.Hidden("EnableSSL", destination.EnableSSL)%>
                            <%= Html.Hidden("Subject", content.Subject)%>
                            <%= Html.Hidden("RuleSetCode", content.RuleSetCode)%>
                            <%= Html.Hidden("AttachmentFileFolderPath", content.AttachmentFileFolderPath)%>
                            <%= Html.Hidden("AttachmentFiles", content.AttachmentFiles!=null?string.Join(",",content.AttachmentFiles):null)%>
                        </td>
                        <td colspan="3">
                            <strong>
                                <%= ViewData["NotificationType"]%>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("SenderAddress")%>
                                <%-- <%= Html.Hidden("NotificationMasterId", ViewData["NotificationMasterId"])%>--%>
                            </label>
                        </td>
                        <td>
                            <%= destination.SenderAddress%>
                        </td>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("SmtpAddress")%>
                            </label>
                        </td>
                        <td>
                            <%: destination.SmtpAddress%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("SmtpUserName")%>
                            </label>
                        </td>
                        <td>
                            <%= destination.SmtpUserName%>
                        </td>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("SmtpPassword")%>
                            </label>
                        </td>
                        <td>
                            <%= Html.Label("*****")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("PortNumber")%>
                            </label>
                        </td>
                        <td>
                            <%= destination.PortNumber%>
                        </td>
                        <td style="width: 130px;">
                            <label>
                                <%:this.GetLocalResourceObject("EnableSSL")%>
                            </label>
                        </td>
                        <td>
                            <%= destination.EnableSSL.ToString()%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <h4><strong><%:this.GetLocalResourceObject("AddEmailContent")%></strong></h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("Subject")%>
                            </label>
                        </td>
                        <td style="padding: 5px; width: 350px;">
                            <%= content.Subject%>
                        </td>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("AttachmentFolder")%>
                            </label>
                        </td>
                        <td>
                            <%= content.AttachmentFileFolderPath%>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <label>
                                <%:this.GetLocalResourceObject("AttachmentFiles")%>
                            </label>
                        </td>
                        <td>
                            <%= content.AttachmentFiles != null ? string.Join(",", content.AttachmentFiles) : null%>
                        </td>
                        <%if (ViewData["NotificationTemplateId"] == null)
                          { %>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                //$(".RichTextEditor").jqte()[0].disable(true);
                                $(".RichTextEditor").attr('disabled', 'disabled');
                            });
                        </script>
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <label>
                                <%:this.GetLocalResourceObject("Content")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                            </label>
                        </td>
                        <td colspan="3" style="padding: 5px;">
                            <%= Html.TextArea("Content", ViewData["TextContent"] != null ? ViewData["TextContent"].ToString() : null, new { @class = "RichTextEditor", @readonly = "true" })%>
                            <%= Html.Hidden("TextContent", ViewData["TextContent"])%>
                        </td>
                    </tr>
                    <% 
                          }
                          else
                          {
                    %>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("RuleSetCode")%>
                            </label>
                        </td>
                        <td>
                            <%= content.RuleSetCode%>
                        </td>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("TemplateName")%>
                                <%= Html.Hidden("NotificationTemplateId", ViewData["NotificationTemplateId"])%>
                            </label>
                        </td>
                        <td>
                            <%= content.Template.TemplateName%>
                        </td>
                    </tr>
                    <%} %>
                </tbody>
            </table>
        </div>
    </div>
</section>
<% } %>
<% } %>
<script type="text/javascript">
    $(function () {
        $('select#Templates').select2();
    });
</script>
