﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    CelloSaaS.Notification.Model.Content.FileContent content = (CelloSaaS.Notification.Model.Content.FileContent)ViewData["FileContent"];
    CelloSaaS.Notification.Model.Dispatch.FtpDestination destination = (CelloSaaS.Notification.Model.Dispatch.FtpDestination)ViewData["FtpDestination"];
    AjaxOptions ajaxOption = new AjaxOptions();
    ajaxOption.UpdateTargetId = "ManageFtpDetails";
    using (Ajax.BeginForm("ManageFtpDetails", "NotificationConfig", new { }, ajaxOption, new { id = "ManageFtpDetailsPage" }))
    {
%>
<%if (!Convert.ToBoolean(ViewData["IsGlobal"]))
  { %>
<section class="panel green">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("ManageFtpDetails")%>
            <span class="semi-bold">&nbsp;:&nbsp;<%=Html.Display("NotificationName", ViewData["NotificationName"])%></span>
        </h4>
        <div class="actions pull-right">
            <%if (destination != null && !string.IsNullOrEmpty(destination.NotificationDestinationId))
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:UpdateFtpDetails();" title="<%=this.GetGlobalResourceObject("General","Update") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Update") %></span></a>
            <%}
              else
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:ManageFtpDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Save") %></span></a>
            <%} %>
            <a class="btn btn-sm btn-default" href="#" onclick="javascript:MasterConfigCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Cancel") %></span></a>
            <a class="btn btn-sm btn-warning" href="#" onclick="javascript:ClearFormDetails();" title="<%=this.GetGlobalResourceObject("General","Clear") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General", "Clear")%></span></a>
        </div>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <%} %>
        <div class="form-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("FtpType")%>
                        </label>
                        <%= Html.Hidden("NotificationMasterId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("NotificationContentId", content.NotificationContentId)%>
                        <%= Html.Hidden("NotificationDestinationId", destination.NotificationDestinationId)%>
                        <%= Html.Hidden("IsSecured", destination.IsSecured)%>
                        <%= Html.Hidden("NotificationType", ViewData["NotificationType"])%>
                        <%= Html.Hidden("notificationId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("IsGlobalConfig", ViewData["GlobalConfig"])%>
                        <br />
                        <strong>
                            <%= Html.Display("NotificationType", ViewData["NotificationType"])%></strong>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("FtpAddress")%>
                        </label>
                        <%= Html.TextBox("FtpAddress", destination.FtpAddress, new { maxlength = 50, style = "width:100%;" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("FtpUserName")%>
                        </label>
                        <%= Html.TextBox("UserName", destination.UserName, new { maxlength = 50, style = "width:100%;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("FtpPassword")%>
                        </label>
                        <%= Html.Password("Password", destination.Password, new { maxlength = 50, style = "width:100%;" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4><b><%:this.GetLocalResourceObject("AddFtpContent")%></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("FilePath")%>
                        </label>
                        <%= Html.TextBox("FilePath", content.FilePath, new { maxlength = 50, style = "width:100%;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%:this.GetLocalResourceObject("FileName")%>
                        </label>
                        <%= Html.TextBox("FileName", content.FileName, new { maxlength = 50, style = "width:100%;" })%>
                    </div>
                </div>
            </div>
        </div>

        <br />
        <div class="pull-right">
            <%if (destination != null && !string.IsNullOrEmpty(destination.NotificationDestinationId))
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:UpdateFtpDetails();" title="<%=this.GetGlobalResourceObject("General","Update") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Update") %></span></a>
            <%}
              else
              {%>
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:ManageFtpDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Save") %></span></a>
            <%} %>
            <a class="btn btn-default" href="#" onclick="javascript:MasterConfigCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
            <a class="btn btn-warning" href="#" onclick="javascript:ClearFormDetails();" title="<%=this.GetGlobalResourceObject("General","Clear") %>">
                <%=this.GetGlobalResourceObject("General", "Clear")%></a>
        </div>
    </div>
</section>
<%}
  else
  {
%>
<section class="panel indigo">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("ManageFtpDetails")%>
            <span class="semi-bold">&nbsp;:&nbsp;<%=Html.Display("NotificationName", ViewData["NotificationName"])%></span>
        </h4>
        <div class="actions pull-right">
            <a class="btn btn-sm btn-warning" href="#" onclick="javascript:OverrideDetails('#ManageFtpDetailsPage');" title="<%=this.GetGlobalResourceObject("General","OverrideDetails") %>">
                <%=this.GetGlobalResourceObject("General","OverrideDetails") %></a>
            <a class="btn btn-sm btn-default" href="#" onclick="javascript:EmailDispatchCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <%} %>
        <div class="form-container">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("FtpType")%>
                            </label>
                        </td>
                        <td>
                            <strong>
                                <%= Html.Display("NotificationType", ViewData["NotificationType"])%></strong>
                        </td>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("FtpAddress")%>
                                <%= Html.Hidden("NotificationMasterId", ViewData["NotificationMasterId"])%>
                                <%--<%= Html.Hidden("NotificationContentId", content.NotificationContentId)%>
                        <%= Html.Hidden("NotificationDestinationId", destination.NotificationDestinationId)%>--%>
                                <%= Html.Hidden("IsSecured", destination.IsSecured)%>
                                <%= Html.Hidden("notificationId", ViewData["NotificationMasterId"])%>
                                <%= Html.Hidden("NotificationType", ViewData["NotificationType"])%>
                                <%= Html.Hidden("NotificationName", ViewData["NotificationName"])%>
                                <%= Html.Hidden("FtpAddress", destination.FtpAddress)%>
                                <%= Html.Hidden("UserName", destination.UserName)%>
                                <%= Html.Hidden("Password", destination.Password)%>
                                <%= Html.Hidden("FilePath", content.FilePath)%>
                                <%= Html.Hidden("FileName", content.FileName)%>
                            </label>
                        </td>
                        <td>
                            <%= destination.FtpAddress%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("FtpUserName")%>
                            </label>
                        </td>
                        <td>
                            <%= destination.UserName%>
                        </td>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("FtpPassword")%>
                            </label>
                        </td>
                        <td colspan="3" style="padding: 5px;">
                            <%= Html.Label("****")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <h4>
                                <b>
                                    <%:this.GetLocalResourceObject("AddFtpContent")%>
                                </b>
                            </h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("FilePath")%>
                            </label>
                        </td>
                        <td>
                            <%= content.FilePath%>
                        </td>
                        <td>
                            <label>
                                <%:this.GetLocalResourceObject("FileName")%>
                            </label>
                        </td>
                        <td>
                            <%= content.FileName%>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<%} %>
<% } %>
