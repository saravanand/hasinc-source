﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Notification.Model.NotificationDetails>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "AddEmailDispatch";
   using (Ajax.BeginForm("AddEmailDispatch", "NotificationConfig", new { }, ajaxOption, new { id = "ManageMasterDetailsPage" }))
   {
%>
<section class="panel indigo">
    <header class="panel-heading">
        <h4>
            <%=Model.NotificationName %></h4>
    </header>
    <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
       { %>
    <div class="alert alert-danger">
        <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
    </div>
    <%} %>
    <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
       { %>
    <div class="alert alert-danger">
        <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
    </div>
    <%} %>
    <div class="panel-body">
        <table class="details_container_table">
            <tbody>
                <tr>
                    <td style="width: 130px;">
                        <label class="mandatory">
                            <%:this.GetLocalResourceObject("NotificationType")%>
                        </label>
                    </td>
                    <td style="padding: 2px;">
                        <%= Html.DropDownList("NotificationType", (IEnumerable<SelectListItem>)ViewData["NotificationTypes"], new { style = "width:202px;", onchange = "TypeChange();" })%>
                    </td>
                    <%= Html.HiddenFor(model=>model.NotificationId)%>
                    <%= Html.Hidden("GlobalConfig", true)%>
                    <%= Html.HiddenFor(model=>model.TenantId)%>
                    <%= Html.Hidden("IsNew", true)%>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript">$(function () { $('select[name=NotificationType]').trigger('change'); });</script>
    </div>
</section>
<% } %>
