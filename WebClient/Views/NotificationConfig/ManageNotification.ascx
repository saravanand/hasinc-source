﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Notification.Model.NotificationDetails>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "ManageNotification";
   using (Ajax.BeginForm("ManageNotification", "NotificationConfig", new { }, ajaxOption, new { id = "ManageNotificationPage" }))
   {
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("AddMasterConfiguration")%></h4>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-success">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <% }
           else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <% } %>
        <div class="form-container">
            <div class="form-group <%=  Html.ValidationMessage("NotificationName","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("NotificationName")%>
                </label>
                <%=Html.HiddenFor(model=>model.NotificationId)%>
                <%=Html.TextBox("NotificationName", Model.NotificationName, new { maxlength = 100, style = "width:100%;" })%>
                <%=Html.ValidationMessage("NotificationName","*") %>
            </div>
            <div class="form-group">
                <label>
                    <%:this.GetLocalResourceObject("Description")%>
                </label>
                <%= Html.TextArea("Description", Model.Description, new { style="width:100%;", onKeyDown = "limitText(this,1000)", onKeyUp = "limitText(this,1000)" })%>
            </div>
            <div class="form-group">
                <label>
                    <%:this.GetLocalResourceObject("Category")%>
                </label>
                <%= Html.TextBox("Category", Model.Category, new { maxlength=100, style = "width:100%;" })%>
            </div>
            <div class="form-group">
                <%if ((bool)ViewData["IsProductAdmin"] && string.IsNullOrEmpty(Model.NotificationId))
                  { %>
                <label>
                    <%:this.GetLocalResourceObject("IsPublic")%>
                </label>
                <%= Html.CheckBox("IsPublic",true)%>
                <%}
                  else
                  { %>
                <div style="display: none;">
                    <label>
                        <%:this.GetLocalResourceObject("IsPublic")%>
                    </label>
                    <%= Html.CheckBox("IsPublic")%>
                </div>
                <%} %>
            </div>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="#" onclick="javascript:manageNotificationDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>"><span>
               <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></span></a>
            <a class="btn btn-default" href="#" onclick="javascript:MasterConfigCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>"><span>
                <%=this.GetGlobalResourceObject("General","Cancel") %></span></a>
        </div>
    </div>
</section>
<% } %>