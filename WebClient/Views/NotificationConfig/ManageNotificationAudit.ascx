﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% using (Html.BeginForm("ManageNotificationAudit", "NotificationConfig", FormMethod.Post, new { id = "ManageNotificationAuditPage",  @class="form-inline" }))
   {
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4><%:this.GetGlobalResourceObject("General", "Filter")%></h4>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("AuditStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("AuditStatusMessage")%>
        </div>
        <% }
           else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <% }
           else
           { %>
        <div class="form-container">
            <%if (ViewData["MyDetails"] == null)
              {%>
            <div class="form-group">
                <label>
                    <%:this.GetLocalResourceObject("lbl_NotificationType")%>
                </label>
                <%= Html.DropDownList("DestinationType", (IEnumerable<SelectListItem>)ViewData["NotificationTypes"], new { style = "width:160px;", onchange = "TypeChange();" })%>
            </div>
            <%}
              else
              {%>
            <%= Html.Hidden("DestinationType", "Email")%>
            <%= Html.Hidden("DestinationTypes", "Email")%>
            <%= Html.Hidden("DestinationTypes", "BatchEmail")%>
            <%= Html.Hidden("TrackId", UserIdentity.UserId)%>
            <%= Html.Hidden("BatchNotificationType","BatchSystemNotification")%>
            <%} %>
            <div class="form-group">
                <label><%:this.GetLocalResourceObject("lbl_NotificationName")%></label>
                <%= Html.DropDownList("NotificationId", (IEnumerable<SelectListItem>)ViewData["NotificationNames"], new { style = "width:190px;" })%>
            </div>
            <div class="form-group">
                <label><%:this.GetLocalResourceObject("lbl_Status")%></label>
                <%= Html.DropDownList("NotificationStatus", (IEnumerable<SelectListItem>)ViewData["NotificationStatus"], new { style = "width:100px;" })%>
            </div>
            <div class="form-group">
                <label><%:this.GetLocalResourceObject("lbl_startTime")%></label>
                <%= Html.TextBox("StartDate", "", new { @class = "datetime"})%>
            </div>
            <div class="form-group">
                <label><%:this.GetLocalResourceObject("lbl_EndTime")%></label>
                <%= Html.TextBox("EndDate", "", new { @class = "datetime" })%>
            </div>
            <%if (ViewData["MyDetails"] == null)
              {%>
            <div class="form-group">
                <label><%:this.GetLocalResourceObject("lbl_MapId")%></label>
                <%= Html.TextBox("TrackId", "")%>
            </div>
            <%} %>

            <div class="form-actions pull-right">
                <br />
                <a class="btn btn-info" href="#" title="<%=this.GetGlobalResourceObject("General", "Search")%>" onclick="ShowAuditDetails();">
                    <i class="fa fa-search"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Search")%></a>
                <a class="btn btn-default" href="#" onclick="javascript:formReset();" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                    <%:this.GetGlobalResourceObject("General", "Reset")%></a>
            </div>
        </div>
        <% } %>
    </div>
</section>
<% } %>
