﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Notification.Model.Content" %>
<%@ Import Namespace="CelloSaaS.Notification.Model.Dispatch" %>
<% 
    SystemNotificationContent content = (SystemNotificationContent)ViewData["SystemNotificationContent"];
    SystemNotificationDestination destination = (SystemNotificationDestination)ViewData["SystemNotificationDestination"];
    TextMessageNotificationTemplate template = (TextMessageNotificationTemplate)ViewData["NotificationTemplate"];
    AjaxOptions ajaxOption = new AjaxOptions();
    ajaxOption.UpdateTargetId = "ManageSystemNotificationDetails";
    using (Ajax.BeginForm("ManageSystemNotificationDetails", "NotificationConfig", new { }, ajaxOption, new { id = "ManageSystemNotificationDetailsPage" }))
    {
%>
<%if (!Convert.ToBoolean(ViewData["IsGlobal"]))
  { %>
<section class="panel green">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("ManageSystemNotificationDetails")%>
            <%if (ViewData["NotificationName"] != null && !string.IsNullOrEmpty(ViewData["NotificationName"].ToString()))
              { %>
            <span>&nbsp; : &nbsp;</span><%=Html.Display("NotificationName", ViewData["NotificationName"])%>
            <%} %>
        </h4>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <%} %>
        <table class="details_container_table">
            <tbody>
                <tr>
                    <td>
                        <label>
                            <%:this.GetLocalResourceObject("NotificationType")%>
                        </label>
                        <%= Html.Hidden("NotificationMasterId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("NotificationId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("Type", ViewData["Type"])%>
                        <%= Html.Hidden("NotificationType", ViewData["NotificationType"])%>
                        <%= Html.Hidden("IsGlobalConfig", ViewData["GlobalConfig"])%>
                        <%= Html.Hidden("TenantId", UserIdentity.TenantID)%>
                        <%if (destination != null && !string.IsNullOrEmpty(destination.NotificationDestinationId))
                          {%>
                        <%= Html.Hidden("NotificationDestinationId", destination.NotificationDestinationId)%>
                        <% }%>
                        <%if (content != null && !string.IsNullOrEmpty(content.NotificationContentId))
                          {%>
                        <%= Html.Hidden("NotificationContentId", content.NotificationContentId)%>
                        <% }%>
                        <%if (template != null && !string.IsNullOrEmpty(template.TemplateId))
                          {%>
                        <%= Html.Hidden("TemplateId", template.TemplateId)%>
                        <% }%>
                    </td>
                    <td colspan="3" style="padding: 5px;">
                        <h3>
                            <%= Html.Display("NotificationType", ViewData["NotificationType"])%>
                        </h3>
                    </td>
                </tr>
                <%if (ViewData["IsBatch"] != null && Convert.ToBoolean(ViewData["IsBatch"]))
                  { %>
                <tr style="display: none;">
                    <%}
                  else
                  {%>
                <tr>
                    <%} %>
                    <td>
                        <label>
                            <%:this.GetLocalResourceObject("MapId")%>
                        </label>
                    </td>
                    <td style="padding: 5px;">
                        <%= Html.TextBox("MapId", destination.MapId, new { maxlength = 50, style = "width:200px;" })%>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <h2>
                            <b>
                                <%:this.GetLocalResourceObject("AddSystemNotificationContent")%></b></h2>
                    </td>
                </tr>
                <%--<%if (string.IsNullOrEmpty(content.NotificationContentId))
                  { %>--%>
                <tr>
                    <td style="width: 130px; vertical-align: top">
                        <label>
                            <%:this.GetLocalResourceObject("HasTemplate")%>
                        </label>
                    </td>
                    <td style="vertical-align: top">
                        <%if (ViewData["HasTemplate"] == null || ViewData["HasTemplate"].ToString().Equals("False", StringComparison.OrdinalIgnoreCase))
                          {%>
                        <input type="checkbox" name="HasTemplate" id="HasTemplate" />
                        <%}

                          else
                          { %>
                        <input type="checkbox" name="HasTemplate" checked="checked" id="HasTemplate" />
                        <%} %>
                    </td>
                </tr>
                <%-- <%} %>--%>
            </tbody>
            <%if (template == null || string.IsNullOrEmpty(template.TemplateId))
              { %>
            <tbody id="ContentPart">
                <%}
              else
              { %>
            <tbody id="ContentPart" style="display: none">
                <% }%>
                <tr>
                    <td style="vertical-align: top">
                        <label>
                            <%:this.GetLocalResourceObject("Content")%>
                            <span style="color: Red">
                                <%:this.GetLocalResourceObject("Mandatory")%>
                            </span>
                        </label>
                    </td>
                    <td colspan="3" style="padding: 5px;">
                        <%= Html.TextArea("Content", content.Content != null ? content.Content.ToString() : null, new { @class = "RichTextEditor" })%>
                    </td>
                </tr>
            </tbody>

            <%if (template == null || string.IsNullOrEmpty(destination.NotificationDestinationId))
              { %>
            <tbody id="TemplatePart" style="display: none">
                <%}
              else
              {%>
            <tbody id="TemplatePart">
                <%} %>
                <tr>
                    <td colspan="4">
                        <h4>
                            <%:this.GetLocalResourceObject("AddSystemNotificationTemplate")%></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <% string strManageImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/manage.gif"); %>
                        <label>
                            <%:this.GetLocalResourceObject("RuleSetCode")%>
                        </label>
                    </td>
                    <td style="padding: 5px;">
                        <%= Html.TextBox("RuleSetCode", content.RuleSetCode, new { maxlength = 50, style = "width:200px;" })%>
                        <a href="#" onclick="javascript:ManageRules();" title="<%=this.GetGlobalResourceObject("General","ManageRule") %>">
                            <img src='<%: strManageImageUrl%>' alt='' />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <%:this.GetLocalResourceObject("TemplateName")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                            <% if (template != null && !string.IsNullOrEmpty(template.TemplateId))
                               { %>
                            <%= Html.Hidden("NotificationTemplateId", ViewData["NotificationTemplateId"])%>
                            <%} %>
                        </label>
                    </td>
                    <td style="padding: 5px; vertical-align: top">
                        <%= Html.DropDownList("Templates", "---Select---")%>
                        <a href="#" onclick="javascript:ManageTemplate();" title="<%=this.GetGlobalResourceObject("General","ManageTemplate") %>">
                            <img src='<%: strManageImageUrl%>' alt='' />
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <div class="pull-right">
            <%if (destination != null && !string.IsNullOrEmpty(destination.NotificationDestinationId))
              {%>
            <div class="green_but">
                <a href="#" onclick="javascript:UpdateSystemNotificationDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                    <span>
                        <%=this.GetGlobalResourceObject("General", "Save")%></span></a>
            </div>
            <%}
              else
              {%>
            <div class="green_but">
                <a href="#" onclick="javascript:AddSystemNotificationDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                    <span>
                        <%=this.GetGlobalResourceObject("General", "Save")%></span></a>
            </div>
            <%} %>
            <div class="green_but">
                <a href="#" onclick="javascript:EmailDispatchCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                    <span>
                        <%=this.GetGlobalResourceObject("General","Cancel") %></span></a>
            </div>
            <div class="green_but">
                <a href="#" onclick="javascript:ClearEmailDetails();" title="<%=this.GetGlobalResourceObject("General","Clear") %>">
                    <span>
                        <%=this.GetGlobalResourceObject("General", "Clear")%></span></a>
            </div>
        </div>
    </div>
</section>
<%}
  else
  { %>
<section class="panel green">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("ManageSystemNotificationDetails")%>
            <%if (ViewData["NotificationName"] != null && !string.IsNullOrEmpty(ViewData["NotificationName"].ToString()))
              { %>
            <span>&nbsp; : &nbsp;</span><%=Html.Display("NotificationName", ViewData["NotificationName"])%>
            <%} %>
        </h4>
        <div class="actions pull-right">
            <div class="green_but">
                <a href="#" onclick="javascript:OverrideDetails('#ManageSystemNotificationDetailsPage');"
                    title="<%=this.GetGlobalResourceObject("General","OverrideDetails") %>"><span>
                        <%=this.GetGlobalResourceObject("General","OverrideDetails") %></span></a>
            </div>
            <div class="green_but">
                <a href="#" onclick="javascript:EmailDispatchCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                    <span>
                        <%=this.GetGlobalResourceObject("General","Cancel") %></span></a>
            </div>
        </div>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <%} %>
        <table class="details_container_table">
            <tbody>
                <tr>
                    <td>
                        <label>
                            <%:this.GetLocalResourceObject("NotificationType")%>
                        </label>
                        <%= Html.Hidden("NotificationMasterId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("Type", ViewData["Type"])%>
                        <%= Html.Hidden("notificationId", ViewData["NotificationMasterId"])%>
                        <%= Html.Hidden("NotificationType", ViewData["NotificationType"])%>
                        <%= Html.Hidden("NotificationName", ViewData["NotificationName"])%>
                        <%= Html.Hidden("TextContent", content.Content)%>
                        <%= Html.Hidden("NotificationTemplateId", ViewData["NotificationTemplateId"])%>
                        <%if (destination != null && !string.IsNullOrEmpty(destination.NotificationDestinationId))
                          {%>
                        <%--<%= Html.Hidden("NotificationDestinationId", destination.NotificationDestinationId)%>--%>
                        <% }%>
                        <%if (content != null && !string.IsNullOrEmpty(content.NotificationContentId))
                          {%>
                        <%-- <%= Html.Hidden("NotificationContentId", content.NotificationContentId)%>--%>
                        <% }%>
                        <%if (template != null && !string.IsNullOrEmpty(template.TemplateId))
                          {%>
                        <%= Html.Hidden("TemplateId", template.TemplateId)%>
                        <%-- <%= Html.Hidden("NotificationTemplateId", template.TemplateId)%>--%>
                        <% }%>
                        <%= Html.Hidden("NotificationType", ViewData["NotificationType"])%>
                    </td>
                    <td colspan="3" style="padding: 5px;">
                        <h3>
                            <%= Html.Display("NotificationType", ViewData["NotificationType"])%>
                        </h3>
                    </td>
                </tr>
                <%if (ViewData["IsBatch"] != null)
                  { %>
                <tr style="display: none;">
                    <%}
                  else
                  {%>
                <tr>
                    <%} %>
                    <td>
                        <label>
                            <%:this.GetLocalResourceObject("MapId")%>
                        </label>
                    </td>
                    <td style="padding: 5px;">
                        <span>
                            <%:destination.MapId%></span>
                    </td>
                </tr>
                <%if (template == null || string.IsNullOrEmpty(template.TemplateId))
                  { %>
                <tr>
                    <td colspan="4">
                        <h2>
                            <b>
                                <%:this.GetLocalResourceObject("AddSystemNotificationContent")%></b></h2>
                    </td>
                </tr>
                <%if (string.IsNullOrEmpty(content.NotificationContentId))
                  { %>
                <tr>
                    <td style="width: 130px; vertical-align: top">
                        <label>
                            <%:this.GetLocalResourceObject("HasTemplate")%>
                        </label>
                    </td>
                    <td style="vertical-align: top">
                        <input type="checkbox" name="HasTemplate" id="HasTemplate" />
                    </td>
                </tr>
                <%} %>
            </tbody>
            <tbody id="Tbody1">
                <tr>
                    <td style="vertical-align: top">
                        <label>
                            <%:this.GetLocalResourceObject("Content")%>
                            <span style="color: Red">
                                <%:this.GetLocalResourceObject("Mandatory")%>
                            </span>
                        </label>
                    </td>
                    <td colspan="3" style="padding: 5px;">
                        <script type="text/javascript">
                            $(document).ready(function () {
                                //$(".RichTextEditor").jqte()[0].disable(true);
                                $(".RichTextEditor").attr('disabled', 'disabled');
                            });
                        </script>
                        <%= Html.TextArea("Content", content.Content != null ? content.Content.ToString() : null, new { @class = "RichTextEditor", @readonly = "readonly" })%>
                    </td>
                </tr>
            </tbody>
            <% }
                  if (template != null || string.IsNullOrEmpty(destination.NotificationDestinationId))
                  {
                      if (template == null || string.IsNullOrEmpty(destination.NotificationDestinationId))
                      { %>
            <tbody id="Tbody2" style="display: none">
                <%}
                  else
                  {%>
            <tbody id="Tbody3">
                <%} %>
                <tr>
                    <td colspan="4">
                        <h4>
                            <%:this.GetLocalResourceObject("AddSystemNotificationTemplate")%></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <%:this.GetLocalResourceObject("RuleSetCode")%>
                        </label>
                    </td>
                    <td style="padding: 5px;">
                        <%= content.RuleSetCode%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <%:this.GetLocalResourceObject("TemplateName")%>
                            <% if (template != null && !string.IsNullOrEmpty(template.TemplateId))
                               { %>
                            <%--<%= Html.Hidden("NotificationTemplateId", ViewData["NotificationTemplateId"])%>--%>
                            <%} %>
                        </label>
                    </td>
                    <td style="padding: 5px;">
                        <%= content.Template.TemplateName%>
                    </td>
                </tr>
            </tbody>
            <%} %>
        </table>
    </div>
</section>
<%} %>
<% } %>
