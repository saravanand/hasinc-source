﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Notification.Model.NotificationDetails>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
    { %>
<div class="alert alert-danger">
    <%=Html.CelloValidationSummary()%>
</div>
<%
  }
%>
<%
    if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Success")))
    { %>
<div class="alert alert-success">
    <%:this.GetLocalResourceObject("s_DeleteNotification")%>
</div>
<%
  }
%>
<% if (ViewData["NotificationDetails"] == null || ((IEnumerable<CelloSaaS.Notification.Model.NotificationDetails>)ViewData["NotificationDetails"]).Count() <= 0)
   { %>
<div class="alert alert-info">
    <%:this.GetLocalResourceObject("e_NotificationMasterDetails")%>
</div>
<% }
   else
   { %>
<% Html.Grid((IEnumerable<CelloSaaS.Notification.Model.NotificationDetails>)ViewData["NotificationDetails"]).Columns(
   column =>
   {
       column.For(col => col.NotificationName).Named(this.GetLocalResourceObject("notificationName").ToString());
       column.For(col => string.Format("<span title=\"{0}\">{1}</span>", HttpUtility.HtmlEncode(col.Description), col.Description.Truncate(40)))
           .Named(this.GetLocalResourceObject("Description").ToString())
           .Attributes(style => "width:28%;white-space: -moz-pre-wrap !important; ").DoNotEncode();
       column.For(col => col.Category ?? "-").Named(this.GetLocalResourceObject("Category").ToString());
       column.For(col => "<a id='Manage' href='#' title='" + string.Format(this.GetLocalResourceObject("manageDetails").ToString(), col.NotificationName) + "' onclick =ManageMasterDetails('" + col.NotificationId.Trim() + "','False')><i class='fa fa-edit'></i></a>").Named(this.GetLocalResourceObject("manageDispatch").ToString()).Attributes(@class => "halign", style => "width:auto;").HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
       if (CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId.Equals(UserIdentity.TenantID, StringComparison.OrdinalIgnoreCase))
       {
           column.For(col => string.IsNullOrEmpty(col.TenantId) ? "<a id='Manage' href='#' title='" + string.Format(this.GetLocalResourceObject("EditGlobalDispatch").ToString(), col.NotificationName) + "' onclick =ManageMasterDetails('" + col.NotificationId.Trim() + "','True')><i class='fa fa-wrench'></i></a>" : "-").Named(this.GetLocalResourceObject("EditGlobalDispatch").ToString()).Attributes(@class => "halign", style => "width:auto;").HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
       }
       if (UserIdentity.HasPrivilege(CelloSaaS.Notification.Model.NotificationPrivilegeConstants.EditNotification))
       {
           column.For(col => CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId.Equals(UserIdentity.TenantID, StringComparison.OrdinalIgnoreCase) || col.TenantId == UserIdentity.TenantID ? "<a id='Edit' href='#' title='" + string.Format(this.GetLocalResourceObject("editDetails").ToString(), col.NotificationName) + "' onclick =ManageNotification('" + col.NotificationId.Trim() + "')><i class='fa fa-pencil'></i></a>" : "-").Named(this.GetLocalResourceObject("edit").ToString()).Attributes(@class => "halign", style => "width:auto;").HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
       }
       if (UserIdentity.HasPrivilege(CelloSaaS.Notification.Model.NotificationPrivilegeConstants.DeleteNotification))
       {
           column.For(col => CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId.Equals(UserIdentity.TenantID, StringComparison.OrdinalIgnoreCase) || col.TenantId == UserIdentity.TenantID ? "<a id='Delete' href='#' title='" + string.Format(this.GetLocalResourceObject("deleteDetails").ToString(), col.NotificationName) + "' onclick =DeleteDetails('" + col.NotificationId.Trim() + "')><i class='fa fa-trash-o'></i></a>" : "-").Named(this.GetLocalResourceObject("delete").ToString()).Attributes(@class => "halign", style => "width:auto;").HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
       }
   }).Attributes(id => "dataList", @class => "celloTable").Render();
%>
<% } %>
