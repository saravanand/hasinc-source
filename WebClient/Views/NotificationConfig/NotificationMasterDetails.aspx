﻿<%@ Page Title="<%$Resources:manageNotification%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Scripts/jquery-te-1.4.0.min.js" type="text/javascript"></script>
    <link href="/Content/jquery-te-1.4.0.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();

            $('#masterTableSearchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {

            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }
            filterTable = $('table#dataList').dataTable({
                "iDisplayLength": 25,
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        }

        function DoSearch() {
            var searchText = $('#masterTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
    <script type="text/javascript">
        var masterListActionUrl = "ShowNotificationDetails";
        var manageNotification = "ManageNotification";
        var manageMasterDetails = '<%:Url.Action("ManageMasterDetails") %>';
        var manageTemplateDetails = '<%:Url.Action("ShowLocaleTemplateDetails") %>';

        function manageNotificationDetails() {
            $('#statusMessage').fadeOut();
            $('div.success').fadeOut();
            formDetails = $('#ManageNotificationPage');
            serializedForm = formDetails.serialize();
            $.post(manageNotification, serializedForm, addMaster_callBack);
        }

        function ManageMasterDetails(notificationMasterId, isGlobalEdit) {
            $('#statusMessage').fadeOut();
            $('div.success').fadeOut();
            $.get(manageMasterDetails + "?notificationMasterId=" + notificationMasterId + "&isGlobalEdit=" + isGlobalEdit, loadAddMasterConfig);
        }

        function ManageTemplateDetails(notificationMasterId) {
            $('#statusMessage').fadeOut();
            $('div.success').fadeOut();
            $.get(manageTemplateDetails + "?notificationMasterId=" + notificationMasterId, loadAddLocaleTemplate);
        }

        function TypeChange() {
            var type = $('#NotificationType option:selected').val();
            if (type == -1) {
                $("#AddEmailDispatch").fadeOut();
                return false;
            }
            else {
                $('#statusMessage').fadeOut();
                $('div .success').fadeOut();
                formDetails = $('#ManageMasterDetailsPage');
                serializedForm = formDetails.serialize();
                $.post("ManageMasterDetails", serializedForm, loadAddEmailConfig);
                return false;
            }
        }

        function DeleteDetails(notificationMasterId) {
            if (confirm('<%: this.GetLocalResourceObject("m_confirmmess")%>')) {
                $("#AddMasterConfig").fadeOut();
                $('#statusMessage').fadeOut();
                $('div .success').fadeOut();
                $.get("RemoveMasterDetails?notificationMasterId=" + notificationMasterId, masterList_callBack);
            }
        }

        function MasterConfigCancel() {
            $("#AddMasterConfig").fadeOut();
            $("#AddEmailDispatch").fadeOut();
        }

        function EmailDispatchCancel() {
            $("#AddMasterConfig").fadeOut();
            $("#AddEmailDispatch").fadeOut();
        }

        function loadMasterList() {
            $.ajaxSettings.cache = false;
            $.get(masterListActionUrl, masterList_callBack);
            $("#AddMasterConfig").fadeOut();
        }

        function ManageNotification(notificationMasterId) {
            $('#statusMessage').fadeOut();
            $.get(manageNotification + "?notificationMasterId=" + notificationMasterId, loadAddMasterConfig);
            return true;
        }

        function loadAddEmailConfig(data) {
            $("#AddEmailDispatch").html(data);
            loadRichTextEditor();
            $("#AddEmailDispatch").fadeIn();
            checkEvent();
        }
        function loadAddMasterConfig(data) {
            if (data == "NoDispatch") {
                loadMasterList();
                $('#statusMessage').html('<div class="alert alert-danger"><%: this.GetLocalResourceObject("e_DispatchDetailsNotAvailable")%></div>').fadeIn();
            }
            else {
                $("#AddMasterConfig").html(data);
                loadRichTextEditor();
                $("#AddMasterConfig").fadeIn();
                $("#AddEmailDispatch").fadeOut();
                checkEvent();
                $("#AddMasterConfig").focus();

                if ($("#SenderAddress").length) {
                    $("#SenderAddress").focus();
                }
                if ($("#FtpAddress").length) {
                    $("#FtpAddress").focus();
                }

            }
        }

        function ManageEmailDetails() {
            var url = '<%:Url.Action("ManageEmailDetails","NotificationConfig") %>';
            formDetails = $('#ManageEmailDetailsPage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, manageEmail_callBack);
        }

        function UpdateEmailDetails() {
            var url = '<%:Url.Action("ManageEmailDetails","NotificationConfig") %>';
            formDetails = $('#ManageEmailDetailsPage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, updateEmail_callBack);
        }

        function OverrideEmailDetails() {
            $('#statusMessage').fadeOut();
            $('div.success').fadeOut();
            formDetails = $('#ManageEmailDetailsPage');
            serializedForm = formDetails.serialize();
            $.post("ManageMasterDetails", serializedForm, loadOverrideEmailDetails);
            return false;
        }

        function loadOverrideEmailDetails(data) {
            $("#AddEmailDispatch").html(data);
            loadRichTextEditor();
            $("#AddMasterConfig").html('');
            $("#AddMasterConfig").fadeOut();
            $("#AddEmailDispatch").fadeIn();
            if ($('#HasTemplate').attr("checked")) {
                $('#ContentPart').hide();
                $('#TemplatePart').show();
            }
            else {
                $('#TemplatePart').hide();
                $('#ContentPart').show();
            }
            $("#AddEmailDispatch").css('margin-top', '30px');
            checkEvent();
            return false;
        }

        function ManageFtpDetails() {
            var url = '<%:Url.Action("ManageFtpDetails","NotificationConfig") %>';
            formDetails = $('#ManageFtpDetailsPage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, manageFtp_callBack);
        }

        function UpdateFtpDetails() {
            var url = '<%:Url.Action("ManageFtpDetails","NotificationConfig") %>';
            formDetails = $('#ManageFtpDetailsPage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, updateFtp_callBack);
        }

        function OverrideDetails(formId) {
            $('#statusMessage').fadeOut();
            $('div.success').fadeOut();
            formDetails = $(formId);
            serializedForm = formDetails.serialize();
            $.post("ManageMasterDetails", serializedForm, loadOverrideEmailDetails);
            return false;
        }

        function AddSystemNotificationDetails() {
            var url = '<%:Url.Action("ManageSystemNotificationDetails","NotificationConfig") %>';
            formDetails = $('#ManageSystemNotificationDetailsPage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, addSystemNotification_callBack);
        }

        function UpdateSystemNotificationDetails() {
            var url = '<%:Url.Action("ManageSystemNotificationDetails","NotificationConfig") %>';
            formDetails = $('#ManageSystemNotificationDetailsPage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, updateSystemNotification_callBack);
        }

        function updateEmail_callBack(data) {
            if (data == "Success") {
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").fadeOut();
                $('#statusMessage').html('<div class="alert alert-success"><%: this.GetLocalResourceObject("s_SaveEmailDetails")%></div>').fadeIn();
                // loadEmailDispatchList();
            }
            else {
                $("#AddEmailDispatch").html(data);
                loadRichTextEditor();
                $("#AddMasterConfig").html('');
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").fadeIn();
                $("#SenderAddress").focus();
                checkEvent();
            }
        }

        function manageEmail_callBack(data) {
            if (data == "Success") {
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").fadeOut();
                $('#statusMessage').html('<div class="alert alert-success"><%: this.GetLocalResourceObject("s_SaveEmailDetails")%></div>').fadeIn();
                // loadEmailDispatchList();
            }
            else {
                $("#AddEmailDispatch").html(data);
                loadRichTextEditor();
                $("#AddEmailDispatch").fadeIn();
                $("#SenderAddress").focus();
                checkEvent();
            }
        }

        function addSystemNotification_callBack(data) {
            if (data == "Success") {
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").fadeOut();
                $('#statusMessage').html('<div class="alert alert-success"><%: this.GetLocalResourceObject("s_SaveSystem")%></div>').fadeIn();
                // loadEmailDispatchList();
            }
            else {
                $("#AddEmailDispatch").html(data);
                //$("#AddMasterConfig").html('');
                //$("#AddMasterConfig").fadeOut();
                loadRichTextEditor();
                $("#AddEmailDispatch").fadeIn();
                if ($('#HasTemplate').attr("checked")) {
                    $('#ContentPart').hide();
                    $('#TemplatePart').show();
                }
                else {
                    $('#TemplatePart').hide();
                    $('#ContentPart').show();
                }
                checkEvent();
            }
        }

        function updateSystemNotification_callBack(data) {
            if (data == "Success") {
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").fadeOut();
                $('#statusMessage').html('<div class="alert alert-success"><%: this.GetLocalResourceObject("s_SaveSystem") %></div>').fadeIn();
                // loadEmailDispatchList();
            }
            else {
                $("#AddEmailDispatch").html(data);
                //$("#AddMasterConfig").html('');
                //$("#AddMasterConfig").fadeOut();
                loadRichTextEditor();
                $("#AddEmailDispatch").fadeIn();
                if ($('#HasTemplate').attr("checked")) {
                    $('#ContentPart').hide();
                    $('#TemplatePart').show();
                }
                else {
                    $('#TemplatePart').hide();
                    $('#ContentPart').show();
                }
                checkEvent();
            }
        }

        function manageFtp_callBack(data) {
            if (data == "Success") {
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").fadeOut();
                $('#statusMessage').html('<div class="alert alert-success"><%: this.GetLocalResourceObject("s_SaveFtp")%></div>').fadeIn();
            }
            else {
                $("#AddEmailDispatch").html(data);
                $("#AddEmailDispatch").fadeIn();
            }
        }

        function updateFtp_callBack(data) {
            if (data == "Success") {
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").fadeOut();
                $('#statusMessage').html('<div class="alert alert-success"><%: this.GetLocalResourceObject("s_SaveFtp")%></div>').fadeIn();
            }
            else {
                $("#AddMasterConfig").html('');
                $("#AddMasterConfig").fadeOut();
                $("#AddEmailDispatch").html(data);
                $("#AddEmailDispatch").fadeIn();
            }
        }


        //This method is used to render the data after added the role details
        function addMaster_callBack(data) {
            if (data == "Success") {
                loadMasterList();

                $('#statusMessage').html('<div class="alert alert-success"> <%: this.GetLocalResourceObject("s_SaveMaster") %></div>').fadeIn();
            }
            else {
                $('#AddMasterConfig').html(data);
                $('#AddMasterConfig').fadeIn();
            }
        }

        //This method is used to render the master details in the role List div
        function masterList_callBack(data) {
            $("#notificationMasterDetails").html(data);
            jQDataTable();
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        function checkEvent() {
            $(document).on('click', 'input[name=HasTemplate]', function () {
                if ($(this).is(":checked")) {
                    $('#ContentPart').hide();
                    $('#TemplatePart').show();
                }
                else {
                    $('#TemplatePart').hide();
                    $('#ContentPart').show();
                }
            });
        }

        function ManageTemplate() {
            var templateId = $('#Templates').val();
            var templateName = $('#Templates option:selected').text();
            var url = '<%= Url.Action("LocaleTemplateDetails", "Template") %>';
            var TemplateUrl = '<%= Url.Action("TemplateDetails", "Template") %>';
            if (templateId.length == 0) {
                window.open(TemplateUrl + "?category=NotificationTemplate");
            }
            else {
                window.open(url + "?templateId=" + templateId + "&templateName=" + templateName + "&category=NotificationTemplate");
            }
        }

        function ManageRules() {
            var ruleSetCode = $('#RuleSetCode').val();
            var url = '<%= Url.Action("Index", "Rules/Index") %>';
            if (ruleSetCode && ruleSetCode.length > 0) {
                url += '?ruleSetCode=' + ruleSetCode;
            }
            window.open(url);
        }

        function ClearEmailDetails() {
            ClearFormDetails();
            $(".RichTextEditor").jqteVal('');
            return false;
        }
        function ClearFormDetails() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox').val('').removeAttr('selected');
            return false;
        }

        function ClearSystemNotificationDetails() {
            $('#MapId').val("");
            $('#Content').val("");
            $('#RuleSetCode').val("");
            return false;
        }

        function loadRichTextEditor() {
            $(document).ready(function () {
                $(".RichTextEditor").jqte();
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("manageNotification")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <div id="statusMessage">
        </div>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input id="masterTableSearchText" name="masterTableSearchText" class="input-sm form-control" type="text" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" name="roleTableSearchText">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" onclick="DoSearch();" type="button"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 m-b-xs pull-right">
                        <div class="pull-right">
                            <%if (UserIdentity.HasPrivilege(CelloSaaS.Notification.Model.NotificationPrivilegeConstants.AddNotification))
                              { %>
                            <a class="btn btn-success" href="#AddMasterConfig" title="<%=this.GetGlobalResourceObject("General", "Add")%>" onclick="ManageNotification('');">
                                <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Add")%></a>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <div id="notificationMasterDetails">
                    <% Html.RenderPartial("NotificationMaster"); %>
                </div>
            </div>
        </div>
        <div id="AddMasterConfig" tabindex='1' style="margin-top: 30px; display: none;">
        </div>
        <div id="AddEmailDispatch" tabindex='2' style="margin-top: 3px; display: none;">
        </div>
    </div>
</asp:Content>
