﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<% 
    CelloSaaS.Notification.Model.NotificationAuditSearchCondition condition = (ViewBag.Condition as CelloSaaS.Notification.Model.NotificationAuditSearchCondition) ?? new CelloSaaS.Notification.Model.NotificationAuditSearchCondition();

    string sortExpression = (string)ViewData["SortExpression"];
    string sortString = (string)ViewData["SortString"];
    int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
    int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
    int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;

    var routeValues = new
    {
        NotificationName = condition.NotificationName,
        DestinationType = condition.DestinationType,
        TenantId = condition.TenantId,
        StartDate = condition.StartDate,
        EndDate = condition.EndDate,
        TrackId = condition.TrackId,
        DestinationTypes = condition.DestinationTypes != null ? condition.DestinationType : null
    };

    if (ViewData["NotificationAudit"] != null && ((IEnumerable<CelloSaaS.Notification.Model.NotificationAudit>)ViewData["NotificationAudit"]).Count() > 0)
    {
        string strManageImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/manage.gif");
        string strEditImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/edit_icon.gif");
        string strDeleteImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/remove_but.gif");

        Html.Grid((IEnumerable<CelloSaaS.Notification.Model.NotificationAudit>)ViewData["NotificationAudit"]).Columns(
    column =>
    {
        column.For(col => col.NotificationDetails.NotificationName).Named(this.GetLocalResourceObject("NotificationName").ToString());
        column.For(col => col.CreatedOn.ToUIDateTimeString()).Named(Ajax.ActionLink(this.GetLocalResourceObject("DispatchedTime").ToString(), "ManageAuditDetails", "NotificationConfig", new
        {
            TenantId = condition.TenantId,
            sortString = "NotificationAudit_CreatedOn",
            page = pageNumber,
            pageSize = pageSize,
            sortExpression = sortString.Equals("NotificationAudit_CreatedOn") && !string.IsNullOrEmpty(sortExpression) && sortExpression.Equals(CelloSaaS.Model.SortExpressionConstants.Decending) ? CelloSaaS.Model.SortExpressionConstants.Ascending : string.Empty,
            NotificationName = condition.NotificationName,
            DestinationType = condition.DestinationType,
            StartDate = condition.StartDate,
            EndDate = condition.EndDate
        },
                    new AjaxOptions { UpdateTargetId = "notificationAuditDetails" }).ToHtmlString()).HeaderAttributes(new
                    Hash(@class => (sortString.Equals("NotificationAudit_CreatedOn") ? ((string.IsNullOrEmpty(sortExpression) || sortExpression.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting")));
        column.For(col => ((CelloSaaS.Notification.Model.Content.SystemNotificationContent)col.NotificationContent).ProcessedContent).Named(this.GetLocalResourceObject("Content").ToString()).DoNotEncode();
        if ((ViewData["MyDetails"] == null))
        {
            column.For(col => ((CelloSaaS.Notification.Model.Dispatch.SystemNotificationDestination)col.NotificationDestination).MapId).Named(this.GetLocalResourceObject("MapId").ToString()).DoNotEncode();
        }
        column.For(col => col.NotificationStatus).Named(this.GetLocalResourceObject("NotificationStatus").ToString()).Attributes(style => "width:10%;white-space: -moz-pre-wrap ");
    }).Attributes(id => "dataList", @class => "celloTable").Render();

        Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "notificationAuditDetails", LoadingElementDuration = 500, LoadingElementId = "loading" }, routeValues)
                   .SetPagerValues(pageNumber, pageSize, totalCount)
                   .SetSortValues(sortString, sortExpression)
                   .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
                   .Render();
    }
    else
    { %>
<div class="clear">
</div>
<div class="info">
    <%:this.GetLocalResourceObject("e_DetailsNotAvailable")%>
</div>
<% }%>
