<%@ Page Title="<%$ Resources:AddPackageDetails%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.LicenseManagement.PackageDetails>" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("AddPackage", "Package"))
       { %>
    <div class="page-title">
        <a href="<%:Url.Action("PackageList","Package") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%: this.GetLocalResourceObject("AddPackageDetails") %>
        </h3>
        <div class="pull-right">
            <a class="btn btn-success" href="#" title="<%=this.GetGlobalResourceObject("General","Save") %>" onclick="$('form').submit();">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="<%:Url.Action("PackageList","Package") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <% } %>
        <%
           Dictionary<string, PackageFeature> availableFeatures = null;
           if (ViewData["AvailableFeatureDetails"] != null)
           {
               availableFeatures = (Dictionary<string, PackageFeature>)ViewData["AvailableFeatureDetails"];
           }
           else
           {
               availableFeatures = new Dictionary<string, PackageFeature>();
           }

           Dictionary<string, PackageUsage> availableUsages = null;
           if (ViewData["AvailableUsageDetails"] != null)
           {
               availableUsages = (Dictionary<string, PackageUsage>)ViewData["AvailableUsageDetails"];
           }
           else
           {
               availableUsages = new Dictionary<string, PackageUsage>();
           }
        %>
        <% if (Model != null)
           {
               var tenantInfo = CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantInfo(UserIdentity.TenantID);
               bool isIsvOrReseller = tenantInfo.Types != null && tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);
        %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("PackageDetails") %>
                    <% if (!string.IsNullOrEmpty(Model.PackageId))
                       { %>
                    <span class="semi-bold">(<%: this.GetLocalResourceObject("lbl_ReferenceId") %>:<%:Model.PackageId%>)</span>
                    <% } %>
                </h4>
            </header>
            <div class="panel-body">
                <div class="form-container">
                    <div class="row">
                        <% var tabIndx = 1; %>
                        <div class="col-md-6">
                            <div class="form-group <%= Html.ValidationMessage("PackageName", "*") != null ? "has-error" : ""%>">
                                <label class="mandatory"><%: this.GetLocalResourceObject("PackageName") %></label>
                                <%= Html.Hidden("PackageId") %>
                                <%= Html.TextBox("PackageName", Model.PackageName, new {@tabindex=tabIndx++})%>
                                <%= Html.CelloValidationMessage("PackageName", "*")%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <%= Html.ValidationMessage("PackageDescription", "*") != null ? "has-error" : ""%>">
                                <label class="mandatory"><%: this.GetLocalResourceObject("PackageDescription") %></label>
                                <%= Html.TextArea("PackageDescription", Model.PackageDescription, new {@tabindex=tabIndx++})%>
                                <%= Html.CelloValidationMessage("PackageDescription", "*")%>
                            </div>
                        </div>
                    </div>
                    <% if (isIsvOrReseller)
                       { %>
                    <div class="row">
                        <%--<div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_IsEvaluation") %>
                                </label>
                                <br />
                                <%=Html.CheckBox("IsEvaluation")%>
                            </div>
                        </div>--%>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("lbl_TrialPeriod") %>
                                </label>
                                <div class="input-group">
                                    <%=Html.TextBox("ExpiryDays", Model.ExpiryDays, new { @tabindex = tabIndx++ })%>
                                    <span class="input-group-addon"><%: this.GetLocalResourceObject("lbl_days") %></span>
                                    <%=Html.ValidationMessage("ExpiryDays","*") %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </section>
        <%if (ViewData["ServiceDetails"] != null && ((IEnumerable<PackageService>)ViewData["ServiceDetails"]).Count() > 0)
          {%>
        <section class="panel">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("ServiceDetails") %>
                </h4>
            </header>
            <div class="panel-body">
                <table class="details_container_table">
                    <tr>
                        <td>
                            <%if (ViewData["ServiceDetails"] != null && ((IEnumerable<PackageService>)ViewData["ServiceDetails"]).Count() > 0)
                              {

                                  foreach (var service in ((IEnumerable<PackageService>)ViewData["ServiceDetails"]))
                                  {
                                      if (ViewData["SelectedAssignedServices"] != null && ((IEnumerable<string>)ViewData["SelectedAssignedServices"]).Contains(service.ServiceCode))
                                      {
                            %>
                            <input type="checkbox" name="service" checked="checked" value="<%= service.ServiceCode %>"
                                onchange="GetModuleList('<%= service.ServiceCode %>')" onload="GetModuleList('<%= service.ServiceCode %>')"
                                tabindex="<%: tabIndx %>" />
                            <%}
                                      else
                                      {%>
                            <input type="checkbox" name="service" value="<%= service.ServiceCode %>" onchange="GetModuleList('<%= service.ServiceCode %>')"
                                tabindex="<%: tabIndx %>" />
                            <%}%>
                            <%= service.ServiceName%><br />
                            <%
                                      tabIndx++;
                                  }
                              }

                              else
                              {%>
                            <div class="alert alert-info">
                                <%: this.GetLocalResourceObject("ServiceNotAvailable") %>
                            </div>
                            <%}%>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
        <%}%>
        <section class="panel horizontal red">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("ModuleDetails") %>
                </h4>
            </header>
            <div class="panel-body">
                <div class="text-info">
                    <i class="fa fa-exclamation-circle"></i>&nbsp;'AccessControl', 'Configuration', 'Setting', 'User', 'Package', 'Tenant' <%: this.GetLocalResourceObject("mandatoryInfo")%>
                </div>
                <div class="col-md-6">
                    <h4>
                        <%: this.GetLocalResourceObject("lbl_AssignedModules")%>
                    </h4>
                    <div class="checkAllDiv">
                        <input type="checkbox" name="assignedModulesSelectAll" />
                        <span><%: this.GetLocalResourceObject("lbl_CheckAll")%> </span>
                    </div>
                    <%if (ViewData["ModuleDetailsByService"] != null && ViewData["ServiceDetails"] != null && ((IEnumerable<PackageService>)ViewData["ServiceDetails"]).Count() > 0)
                      {
                          Dictionary<string, PackageModule> availableModule = null;
                          if (ViewData["ModuleDetails"] != null)
                          {
                              availableModule = (Dictionary<string, PackageModule>)ViewData["ModuleDetails"];
                          }
                          else
                          {
                              availableModule = new Dictionary<string, PackageModule>();
                          }
                          foreach (var service in ((IEnumerable<PackageService>)ViewData["ServiceDetails"]))
                          {
                              Dictionary<string, Dictionary<string, Module>> moduleService = (Dictionary<string, Dictionary<string, Module>>)ViewData["ModuleDetailsByService"];
                              if (moduleService.ContainsKey(service.ServiceCode) && moduleService[service.ServiceCode] != null)
                              {
                    %>
                    <div id="<%=service.ServiceCode %>" style="display: none">
                        <fieldset>
                            <legend>
                                <%=service.ServiceName%>
                            </legend>
                            <%
                                  foreach (var module in moduleService[service.ServiceCode].Values)
                                  {
                                      if (!availableModule.ContainsKey(module.ModuleCode))
                                      {
                                          continue;
                                      }
                                      if (ViewData["SelectedAssignedModules"] != null && ((IEnumerable<string>)ViewData["SelectedAssignedModules"]).Contains(module.ModuleCode))
                                      {
                            %>
                            <input type="checkbox" name="assigned_module_<%= module.ServiceCode %>" checked="checked"
                                value="<%= module.ModuleCode %>" onchange="GetFeatureList('<%= module.ModuleCode%>','<%= module.ServiceCode %>','assigned')"
                                onload="GetFeatureList('<%= module.ModuleCode%>','<%= module.ServiceCode %>')"
                                tabindex="<%:tabIndx %>" />
                            <%}
                                      else
                                      {%>
                            <input type="checkbox" name="module_<%= module.ServiceCode %>" value="<%= module.ModuleCode %>"
                                onchange="GetFeatureList('<%= module.ModuleCode%>','<%= module.ServiceCode %>','assigned')"
                                tabindex="<%:tabIndx %>" />
                            <%}%>
                            <%= module.ModuleName%><br />
                            <%
                                      tabIndx++;
                                  }
                            %>
                        </fieldset>
                    </div>
                    <%                                                            
                              }
                          }
                      } %>
                    <div id="otherModule">
                        <fieldset>
                            <legend>
                                <%: this.GetLocalResourceObject("lbl_OtherModules") %>
                            </legend>
                            <%
                      var packageModule = ViewData["ModuleDetails"] as Dictionary<string, PackageModule>;
                      var selectedAssignedModules = ViewData["SelectedAssignedModules"] as IEnumerable<string>;
                      if (packageModule != null && packageModule.Count > 0)
                      {
                          foreach (var module in packageModule.Values)
                          {
                              if (!string.IsNullOrEmpty(module.ServiceId))
                                  continue;
                              if (selectedAssignedModules != null && selectedAssignedModules.Contains(module.ModuleCode))
                              {
                            %>
                            <input type="checkbox" name="assigned_module_other" checked="checked" value="<%= module.ModuleCode %>"
                                onchange="GetFeatureList('<%= module.ModuleCode%>','other','assigned')" onload="GetFeatureList('<%= module.ModuleCode %>','other','assigned')"
                                tabindex="<%: tabIndx %>" />
                            <%}
                              else
                              {%>
                            <input type="checkbox" name="assigned_module_other" value="<%= module.ModuleCode %>"
                                onchange="GetFeatureList('<%= module.ModuleCode%>','other','assigned')" tabindex="<%: tabIndx %>" />
                            <%}%>
                            <%= module.ModuleName%><br />
                            <%
                              tabIndx++;
                          }
                      }
                      else
                      { %>
                            <%: this.GetLocalResourceObject("ModulesNotAvailable") %>
                            <% } %>
                        </fieldset>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>
                        <%: this.GetLocalResourceObject("lbl_AssignableModules") %>
                    </h4>
                    <div class="checkAllDiv">
                        <input type="checkbox" name="assignableModulesSelectAll" />
                        <span><%: this.GetLocalResourceObject("lbl_CheckAll")%> </span>
                    </div>
                    <%if (ViewData["ModuleDetailsByService"] != null && ViewData["ServiceDetails"] != null && ((IEnumerable<PackageService>)ViewData["ServiceDetails"]).Count() > 0)
                      {
                          Dictionary<string, PackageModule> availableModule = null;
                          if (ViewData["ModuleDetails"] != null)
                          {
                              availableModule = (Dictionary<string, PackageModule>)ViewData["ModuleDetails"];
                          }
                          else
                          {
                              availableModule = new Dictionary<string, PackageModule>();
                          }

                          foreach (var service in ((IEnumerable<PackageService>)ViewData["ServiceDetails"]))
                          {
                              Dictionary<string, Dictionary<string, Module>> moduleService = (Dictionary<string, Dictionary<string, Module>>)ViewData["ModuleDetailsByService"];
                              if (moduleService.ContainsKey(service.ServiceCode) && moduleService[service.ServiceCode] != null)
                              {
                    %>
                    <div id="<%=service.ServiceCode %>" style="display: none">
                        <fieldset>
                            <legend class="service">
                                <%=service.ServiceName%>
                            </legend>
                            <p>
                                <%
                                  foreach (var module in moduleService[service.ServiceCode].Values)
                                  {
                                      if (!availableModule.ContainsKey(module.ModuleCode))
                                      {
                                          continue;
                                      }
                                      if (ViewData["SelectedAssignableModules"] != null && ((IEnumerable<string>)ViewData["SelectedAssignableModules"]).Contains(module.ModuleCode))
                                      {
                                %>
                                <input type="checkbox" name="assignable_module_<%= module.ServiceCode %>" checked="checked"
                                    value="<%= module.ModuleCode %>" onchange="GetFeatureList('<%= module.ModuleCode%>','<%= module.ServiceCode %>','assignable')"
                                    onload="GetFeatureList('<%= module.ModuleCode%>','<%= module.ServiceCode %>','assignable')"
                                    tabindex="<%:tabIndx  %>" />
                                <%}
                                      else
                                      {%>
                                <input type="checkbox" name="assignable_module_<%= module.ServiceCode %>" value="<%= module.ModuleCode %>"
                                    onchange="GetFeatureList('<%= module.ModuleCode%>','<%= module.ServiceCode %>','assignable')"
                                    tabindex="<%:tabIndx  %>" />
                                <%}%>
                                <%= module.ModuleName%><br />
                                <%
                                      tabIndx++;
                                  }
                                %>
                            </p>
                        </fieldset>
                    </div>
                    <%                                                            
                              }
                          }
                      } %>
                    <div id="assignable_otherModule">
                        <fieldset>
                            <legend>
                                <%: this.GetLocalResourceObject("lbl_OtherModules") %>
                            </legend>
                            <%
                      var selectedAssignableModules = ViewData["SelectedAssignableModules"] as IEnumerable<string>;
                      if (packageModule != null && packageModule.Count > 0)
                      {
                          foreach (var module in packageModule.Values)
                          {
                              if (!string.IsNullOrEmpty(module.ServiceId))
                                  continue;
                              if (selectedAssignableModules != null && selectedAssignableModules.Contains(module.ModuleCode))
                              {
                            %>
                            <input type="checkbox" name="assignable_module_other" checked="checked" value="<%= module.ModuleCode %>"
                                onchange="GetFeatureList('<%= module.ModuleCode%>','other','assignable')" onload="GetFeatureList('<%= module.ModuleCode %>','other','assignable')"
                                tabindex="<%: tabIndx %>" />
                            <%}
                              else
                              {%>
                            <input type="checkbox" name="assignable_module_other" value="<%= module.ModuleCode %>"
                                onchange="GetFeatureList('<%= module.ModuleCode%>','other','assignable')" tabindex="<%: tabIndx %>" />
                            <%}%>
                            <%= module.ModuleName%><br />
                            <%
                              tabIndx++;
                          }
                      }
                      else
                      {%>
                            <%: this.GetLocalResourceObject("ModulesNotAvailable") %>
                            <%}%>
                        </fieldset>
                    </div>
                </div>
            </div>
        </section>
        <section class="panel horizontal green">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("FeatureDetails") %>
                </h4>
            </header>
            <div class="panel-body">
                <div class="col-md-6">
                    <div style="position: relative;">
                        <h4>
                            <%: this.GetLocalResourceObject("lbl_AssignedFeatures") %>
                        </h4>
                        <div class="checkAllDiv">
                            <input type="checkbox" name="assignedFeaturesSelectAll" />
                            <span><%: this.GetLocalResourceObject("lbl_CheckAll")%> </span>
                        </div>
                        <%
                      Dictionary<string, Dictionary<string, Feature>> moduleFeatures = ViewData["FeatureDetails"] as Dictionary<string, Dictionary<string, Feature>>;
                      if (packageModule != null && moduleFeatures != null && moduleFeatures.Count > 0)
                      {
                          foreach (string moduleCode in moduleFeatures.OrderBy(x => x.Key).Select(x => x.Key))
                          {
                              if (packageModule.ContainsKey(moduleCode))
                              {
                                  var module = packageModule[moduleCode];
                                  string serviceCode = string.Empty;
                                  Dictionary<string, Feature> featureDetails = moduleFeatures[moduleCode];
                                  if (featureDetails != null && featureDetails.Count > 0)
                                  {
                                      if (string.IsNullOrEmpty(module.ServiceId))
                                      {
                                          serviceCode = "other";
                                      }
                                      else
                                      {
                                          serviceCode = module.ServiceId;
                                      }
                        %>
                        <div id="assigned_featureDiv_<%= module.ModuleCode + "_" + serviceCode  %>" style="display: none">
                            <fieldset>
                                <legend class="module">
                                    <%= moduleCode%>
                                </legend>
                                <% 
                                      var selectedAssignedFeatures = ViewData["SelectedAssignedFeatures"] as IEnumerable<string>;
                                      foreach (Feature feature in featureDetails.Values)
                                      {
                                          if (!availableFeatures.ContainsKey(feature.FeatureCode))
                                          {
                                              continue;
                                          }
                                %>
                                <div class="col-md-6">
                                    <%
                                          if (selectedAssignedFeatures != null && selectedAssignedFeatures.Contains(feature.FeatureCode))
                                          {
                                    %>
                                    <input type="checkbox" checked="checked" name="assigned_feature_<%= moduleCode %>"
                                        value="<%= feature.FeatureCode %>" tabindex="<%:tabIndx %>" />
                                    <% 
                                          }
                                          else
                                          {
                                    %>
                                    <input type="checkbox" name="assigned_feature_<%= moduleCode %>" value="<%= feature.FeatureCode %>"
                                        tabindex="<%:tabIndx %>" />
                                    <%
                                          }
                                    %>
                                    <%=feature.FeatureName%>
                                </div>
                                <%
                                          tabIndx++;
                                      }
                                              
                                %>
                            </fieldset>
                        </div>
                        <%               
                                  }
                                  else
                                  {
                        %>
                        <div class="alert alert-info">
                            <%: this.GetLocalResourceObject("FeatureNotAvailable") %>
                        </div>
                        <%
                                  }
                              }
                          }
                      }%>
                        <div class="alert alert-info assigned_ErrorMessage" style="display: block">
                            <%: this.GetLocalResourceObject("NoFeaturesAvailable") %>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div style="position: relative;">
                        <h4>
                            <%: this.GetLocalResourceObject("lbl_AssignableFeatures") %>
                        </h4>
                        <div class="checkAllDiv">
                            <input type="checkbox" name="assignableFeaturesSelectAll" />
                            <span><%: this.GetLocalResourceObject("lbl_CheckAll")%></span>
                        </div>
                        <%
               if (packageModule != null && moduleFeatures != null && moduleFeatures.Count > 0)
               {
                   foreach (string moduleCode in moduleFeatures.Keys.OrderBy(x => x).ToList())
                   {
                       if (packageModule.ContainsKey(moduleCode))
                       {
                           var module = packageModule[moduleCode];
                           string serviceCode = string.Empty;
                           Dictionary<string, Feature> featureDetails = moduleFeatures[moduleCode];
                           if (featureDetails != null && featureDetails.Count > 0)
                           {
                               if (string.IsNullOrEmpty(module.ServiceId))
                               {
                                   serviceCode = "other";
                               }
                               else
                               {
                                   serviceCode = module.ServiceId;
                               }
                        %>
                        <div id="assignable_featureDiv_<%= module.ModuleCode + "_" + serviceCode  %>" style="display: none">
                            <fieldset>
                                <legend>
                                    <%= moduleCode%>
                                </legend>
                                <%  var selectedAssignableFeatures = ViewData["SelectedAssignableFeatures"] as IEnumerable<string>;
                                    foreach (Feature feature in featureDetails.Values.ToList())
                                    {
                                        if (!availableFeatures.ContainsKey(feature.FeatureCode))
                                        {
                                            continue;
                                        }
                                %>
                                <div class="col-md-6">
                                    <% 
                                        if (selectedAssignableFeatures != null && selectedAssignableFeatures.Contains(feature.FeatureCode))
                                        {
                                    %>
                                    <input type="checkbox" checked="checked" name="assignable_feature_<%= moduleCode %>"
                                        value="<%= feature.FeatureCode %>" tabindex="<%:tabIndx %>" />
                                    <%
                                        }
                                        else
                                        {
                                    %>
                                    <input type="checkbox" name="assignable_feature_<%= moduleCode %>" value="<%= feature.FeatureCode %>"
                                        tabindex="<%:tabIndx %>" />
                                    <%
                                        }
                                    %>
                                    <%=feature.FeatureName%>
                                </div>
                                <% tabIndx++;
                                    }
                                %>
                            </fieldset>
                        </div>
                        <%               
                           }
                           else
                           {
                        %>
                        <div class="alert alert-info">
                            <%: this.GetLocalResourceObject("FeatureNotAvailable") %>
                        </div>
                        <%
                           }
                       }
                   }
               }%>
                        <div class="alert alert-info assignable_ErrorMessage" style="display: block">
                            <%: this.GetLocalResourceObject("NoFeaturesAvailable") %>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <% if (isIsvOrReseller)
           { %>
        <section class="panel horizontal purple">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("lbl_SetMaxUsageCapacity")%>
                </h4>
            </header>
            <div class="panel-body form-container">
                <div class="text-info">
                    <i class="fa fa-exclamation-circle"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_UsageInfo")%>
                </div>
                <div class="col-md-6">
                    <h4>
                        <%: this.GetLocalResourceObject("lbl_AssignedUsage")%>
                    </h4>
                    <%
               Dictionary<string, Dictionary<string, Usage>> moduleUsageDetails = ViewData["UsageDetails"] as Dictionary<string, Dictionary<string, Usage>>;
               if (packageModule != null && moduleUsageDetails != null && moduleUsageDetails.Count > 0)
               {
                   foreach (string moduleCode in moduleUsageDetails.Keys.OrderBy(x => x))
                   {
                       if (packageModule.ContainsKey(moduleCode))
                       {
                           var module = packageModule[moduleCode];
                           string serviceCode = string.Empty;
                           Dictionary<string, Usage> usageDetails = moduleUsageDetails[moduleCode];
                           if (usageDetails != null && usageDetails.Count > 0)
                           {
                               if (string.IsNullOrEmpty(module.ServiceId))
                               {
                                   serviceCode = "other";
                               }
                               else
                               {
                                   serviceCode = module.ServiceId;
                               }
                    %>
                    <div id="assigned_usageDiv_<%= module.ModuleCode + "_" + serviceCode  %>" style="display: none">
                        <fieldset>
                            <legend class="module">
                                <%= moduleCode%>
                            </legend>
                            <div class="moduleUsage">
                                <% 
                               Dictionary<string, double> selectedAssignedUsages = ViewData["SelectedAssignedUsages"] != null ? (Dictionary<string, double>)ViewData["SelectedAssignedUsages"] : null;
                               foreach (Usage usage in usageDetails.Values.OrderBy(x => x.UsageName))
                               {
                                   if (!availableUsages.ContainsKey(usage.UsageCode))
                                   {
                                       continue;
                                   }

                                   double usageAmount = usage.MaximumCapacityUsage.HasValue ? usage.MaximumCapacityUsage.Value : 0; %>
                                <div class="form-group">
                                    <%if (selectedAssignedUsages != null && selectedAssignedUsages.ContainsKey(usage.UsageCode))
                                      {
                                          usageAmount = selectedAssignedUsages[usage.UsageCode];

                                      } %>
                                    <input type="hidden" name="assigned_usage_<%= moduleCode %>" value="<%= usage.UsageCode %>" />
                                    <label>
                                        <%=usage.UsageName%>
                                            (<%: this.GetLocalResourceObject("lbl_Maximum")%>: <%: !availableUsages[usage.UsageCode].MaximumCapacity.HasValue ? this.GetLocalResourceObject("lbl_Unlimited").ToString() : availableUsages[usage.UsageCode].MaximumCapacity.ToString()%>)
                                    </label>
                                    <%: Html.TextBox("assigned_usage_" + moduleCode + "_" + usage.UsageCode, usageAmount)%>
                                </div>
                                <% tabIndx++;
                               } %>
                            </div>
                        </fieldset>
                    </div>
                    <%               
                           }
                       }
                   }
               }
                    %>
                    <div class="alert alert-info assigned_UsageErrorMessage" style="display: block">
                        <%: this.GetLocalResourceObject("e_NoUsageAvailable")%>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>
                        <%: this.GetLocalResourceObject("lbl_AssignableUsage")%>
                    </h4>
                    <%
               if (packageModule != null && moduleUsageDetails != null && moduleUsageDetails.Count > 0)
               {
                   foreach (string moduleCode in moduleUsageDetails.Keys.OrderBy(x => x))
                   {
                       if (packageModule.ContainsKey(moduleCode))
                       {
                           var module = packageModule[moduleCode];
                           string serviceCode = string.Empty;
                           Dictionary<string, Usage> usageDetails = moduleUsageDetails[moduleCode];
                           if (usageDetails != null && usageDetails.Count > 0)
                           {
                               if (string.IsNullOrEmpty(module.ServiceId))
                               {
                                   serviceCode = "other";
                               }
                               else
                               {
                                   serviceCode = module.ServiceId;
                               }
                    %>
                    <div id="assignable_usageDiv_<%= module.ModuleCode + "_" + serviceCode  %>" style="display: none">
                        <fieldset>
                            <legend>
                                <%= moduleCode%>
                            </legend>
                            <div class="moduleUsage">
                                <% 
                               Dictionary<string, double> selectedAssignableUsages = ViewData["SelectedAssignableUsages"] != null ? (Dictionary<string, double>)ViewData["SelectedAssignableUsages"] : null;
                               foreach (Usage usage in usageDetails.Values)
                               {
                                   if (!availableUsages.ContainsKey(usage.UsageCode))
                                   {
                                       continue;
                                   }

                                   double usageAmount = usage.MaximumCapacityUsage != null ? (double)usage.MaximumCapacityUsage : 0; %>
                                <div class="form-group">
                                    <% if (selectedAssignableUsages != null && selectedAssignableUsages.ContainsKey(usage.UsageCode))
                                       {
                                           usageAmount = selectedAssignableUsages[usage.UsageCode];
                                       }%>
                                    <input type="hidden" name="assignable_usage_<%= moduleCode %>" value="<%= usage.UsageCode %>" />
                                    <label><%=usage.UsageName%></label>
                                    <%: Html.TextBox("assignable_usage_" + moduleCode + "_" + usage.UsageCode, usageAmount)%>
                                </div>
                                <%
                                       tabIndx++;

                               }
                                %>
                            </div>
                        </fieldset>
                    </div>
                    <%               
                           }
                       }
                   }
               }
                    %>
                    <div class="alert alert-info assignable_UsageErrorMessage" style="display: block">
                        <%: this.GetLocalResourceObject("e_NoUsageAvailable")%>
                    </div>
                </div>
            </div>
        </section>
        <%} %>
        <div class="pull-right">
            <a class="btn btn-success" href="#" title="<%=this.GetGlobalResourceObject("General","Save") %>" onclick="$('form').submit();">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="<%:Url.Action("PackageList","Package") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
        <% } %>
    </div>
    <% } %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            //To set Package Name maxlength 50
            $("#PackageName").attr('maxlength', '50');

            init('assigned');
            init('assignable');

            $(".module").click(function () {
                var thisModuleFeatures = $(this).parent().find(".moduleFeature input[type=checkbox]");
            });

            $('input[type=checkbox][name=assignedModulesSelectAll]').click(function () {
                if ($(this).is(':checked')) {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_UnCheckAll")%>');
                    $('input[type=checkbox][name^=assigned_module_]').each(function () {
                        if (!$(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                } else {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_CheckAll")%>');
                    $('input[type=checkbox][name^=assigned_module_]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                }
            });
            $('input[type=checkbox][name=assignableModulesSelectAll]').click(function () {
                if ($(this).is(':checked')) {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_UnCheckAll")%>');
                    $('input[type=checkbox][name^=assignable_module_]').each(function () {
                        if (!$(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                } else {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_CheckAll")%>');
                    $('input[type=checkbox][name^=assignable_module_]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                }
            });
            $('input[type=checkbox][name=assignedFeaturesSelectAll]').click(function () {
                if ($(this).is(':checked')) {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_UnCheckAll")%>');
                    $('input[type=checkbox][name^=assigned_feature_]').each(function () {
                        if (!$(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                } else {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_CheckAll")%>');
                    $('input[type=checkbox][name^=assigned_feature_]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                }
            });
            $('input[type=checkbox][name=assignableFeaturesSelectAll]').click(function () {
                if ($(this).is(':checked')) {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_UnCheckAll")%>');
                    $('input[type=checkbox][name^=assignable_feature_]').each(function () {
                        if (!$(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                } else {
                    $(this).next().text('<%: this.GetLocalResourceObject("lbl_CheckAll")%>');
                    $('input[type=checkbox][name^=assignable_feature_]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).trigger('click');
                        }
                    });
                }
            });
        });

        function init(typeName) {
            $('input[type=checkbox][name=service]').each(function () {
                if ($(this).is(':checked') == true) {
                    var serviceId = $(this).attr('value');
                    GetModuleList(serviceId);

                    $('input[type=checkbox][name=' + typeName + '_module_' + serviceId + ']').each(function () {
                        if ($(this).is(':checked') == true) {
                            var moduleId = $(this).attr('value');
                            GetFeatureList(moduleId, serviceId, typeName);
                            GetUsageList(moduleId, serviceId, typeName);
                        }
                    });
                }
            });
            $('input[type=checkbox][name=' + typeName + '_module_other]').each(function () {
                if ($(this).is(':checked') == true) {
                    var moduleId = $(this).attr('value');
                    GetFeatureList(moduleId, 'other', typeName);
                    GetUsageList(moduleId, 'other', typeName);
                }
            });
        }

        function GetFeatureList(moduleid, serviceId, typename) {

            GetUsageList(moduleid, serviceId, typename);

            var moduleFeatureList = $('#' + typename + '_featureDiv_' + moduleid + '_' + serviceId)[0];

            if (moduleFeatureList == null) {
                return;
            }
            var moduleDiv = $('input[type=checkbox][name=' + typename + '_module_' + serviceId + '][value=' + moduleid + ']');
            if (moduleDiv.is(':checked') == true) {
                //moduleFeatureList.style.display = 'block';
                $("#" + typename + '_featureDiv_' + moduleid + '_' + serviceId).show();
            }
            else {
                moduleFeatureList.style.display = 'none';
                ClearFeatureForModule(moduleid);
                ClearUsageForModule(moduleid);
                //ClearFeatureForModule(moduleid, typeName);              
            }
            //  DisplayFeatureErrorMessage();
            DisplayFeatureErrorMessageByTypeName(typename)

        }

        function GetUsageList(moduleid, serviceId, typename) {
            var moduleUsageList = document.getElementById(typename + '_usageDiv_' + moduleid + '_' + serviceId);
            if (moduleUsageList == null) {
                $('div.' + typename + '_UsageErrorMessage').css('display', 'block');
                DisplayUsageErrorMessage(typename);
                return;
            }
            var moduleDiv = $('input[type=checkbox][name=' + typename + '_module_' + serviceId + '][value=' + moduleid + ']');
            if (moduleDiv.is(':checked') == true) {
                //moduleFeatureList.style.display = 'block';
                $('div.' + typename + '_UsageErrorMessage').css('display', 'none');
                $("#" + typename + '_usageDiv_' + moduleid + '_' + serviceId).show();
            }
            else {
                moduleUsageList.style.display = 'none';
                $('div.' + typename + '_UsageErrorMessage').css('display', 'block');
                ClearFeatureForModule(moduleid);
                //ClearFeatureForModule(moduleid, typeName);              
            }
            //  DisplayFeatureErrorMessage();            
            DisplayFeatureErrorMessageByTypeName(typename);
            DisplayUsageErrorMessage(typename);
        }

        function GetModuleList(serviceId) {
            var serviceModuleList = document.getElementById(serviceId);
            var serviceDiv = $('input[type=checkbox][name=service][value=' + serviceId + ']');
            if (serviceDiv.is(':checked') == true) {
                //serviceModuleList.style.display = 'block';
                $("#" + serviceId).show();
            }
            else {
                HideFeatureForService(serviceId);
                serviceModuleList.style.display = 'none';
                ClearModuleForService(serviceId);
            }
            DisplayFeatureErrorMessage();
        }

        function ClearModuleForService(serviceId, typeName) {
            $('input[type=checkbox][name=' + typeName + '_module_' + serviceId + ']').each(function () {
                $(this).attr('checked', null);
            });
        }

        function ClearFeatureForModule(moduleId, typeName) {
            $('input[type=checkbox][name=' + typeName + '_feature_' + moduleId + ']').each(function () {
                $(this).attr('checked', null);
            });
        }

        function ClearUsageForModule(moduleId, typeName) {
            $('input[type=checkbox][name=' + typeName + '_usage_' + moduleId + ']').each(function () {
                $(this).attr('checked', null);
            });
        }

        function HideFeatureForService(serviceId, typeName) {
            $('input[type=checkbox][name=' + typeName + '_module_' + serviceId + ']').each(function () {
                if ($(this).is(':checked') == true) {
                    var moduleFeatureList = document.getElementById(typeName + '_featureDiv_' + $(this).attr('value') + '_' + serviceId);
                    moduleFeatureList.style.display = 'none';
                    ClearFeatureForModule($(this).attr('value'));
                }
            });
        }
        function DisplayFeatureErrorMessageByTypeName(typename) {
            if (typename == "assigned") {
                if ($('#otherModule input[type=checkbox]:checked').length > 0) {
                    $('div.' + typename + '_ErrorMessage').css('display', 'none');
                }
                else {
                    $('div.' + typename + '_ErrorMessage').css('display', 'block');
                }
            }
            else {
                if ($('#assignable_otherModule input[type=checkbox]:checked').length > 0) {
                    $('div.' + typename + '_ErrorMessage').css('display', 'none');
                }
                else {
                    $('div.' + typename + '_ErrorMessage').css('display', 'block');
                }
            }
        }

        function DisplayUsageErrorMessage(typename) {
            if (typename == "assigned") {
                var count = 0;
                $('div[id*=assigned_usageDiv_]').each(function () {
                    if ($(this).css('display') == 'block') {
                        count++
                    }
                });
                if (count > 0) {
                    $('div.' + typename + '_UsageErrorMessage').css('display', 'none');
                }
                else {
                    $('div.' + typename + '_UsageErrorMessage').css('display', 'block');
                }
            }
            else {
                var assignableCount = 0;
                $('div[id*=assignable_usageDiv_]').each(function () {
                    if ($(this).css('display') == 'block') {
                        assignableCount++
                    }
                });
                if (assignableCount > 0) {
                    $('div.' + typename + '_UsageErrorMessage').css('display', 'none');
                }
                else {
                    $('div.' + typename + '_UsageErrorMessage').css('display', 'block');
                }
            }
        }

        function DisplayFeatureErrorMessage() {
            var count = 0
            $('div[id*=featureDiv_]').each(function () {
                if ($(this).css('display') == 'block') {
                    count++
                }
            });
            if (count > 0) {
                $('div.ErrorMessage').css('display', 'none');
            }
            else {
                $('div.ErrorMessage').css('display', 'block');
            }
        }
    </script>
    <style type="text/css">
        fieldset
        {
            margin-top: 15px !important;
        }

            fieldset legend
            {
                font-size: 14px;
                font-weight: 600;
            }

        input[type=checkbox]
        {
            margin-bottom: 8px !important;
        }
    </style>
</asp:Content>
