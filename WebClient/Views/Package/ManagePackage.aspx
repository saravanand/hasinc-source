﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.LicenseManagement.PackageDetails>" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var tenantInfo = CelloSaaS.ServiceProxies.TenantManagement.TenantProxy.GetTenantInfo(UserIdentity.TenantID);
        bool isIsvOrReseller = tenantInfo.Types != null && tenantInfo.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);
    %>
    <div class="heading_container">
        <h1><%: this.GetLocalResourceObject("AddPackageDetails") %></h1>
        <div class="actions">
            <div class="green_but">
                <a href="<%:Url.Action("PackageList","Package") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
                    <span>
                        <%=this.GetGlobalResourceObject("General","Cancel") %></span></a>
            </div>
            <div class="green_but">
                <a href="#" title="<%=this.GetGlobalResourceObject("General","Save") %>" onclick="$('form').submit();">
                    <span>
                        <%=this.GetGlobalResourceObject("General","Save") %></span></a>
            </div>
        </div>
    </div>
    <div class="clear clearfix">
    </div>
    <% if (UserIdentity.IsInRole(RoleConstants.ProductAdmin))
       {
    %>
    <div class="box">
        <%: this.GetLocalResourceObject("TenantName") %>: <b>
            <%
           if (!string.IsNullOrEmpty(TenantContext.GetTenantName(new PackageDetails().EntityIdentifier)))
           {
            %>
            <%:TenantContext.GetTenantName(new PackageDetails().EntityIdentifier)  %>
            <%
           }
           else
           {
            %>
            <%: tenantInfo.TenantDetails.TenantName %>
            <%
           }
            %></b>
    </div>
    <% } %>
    <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
       { %>
    <div class="error">
        <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
    </div>
    <div class="clear clearfix">
    </div>
    <% } %>
    <% using (Html.BeginForm())
       { %>
    <div class="w30 left form-container">
        <div class="heading">
            Package Details
        </div>
        <table class="details_container_table">
            <tbody>
                <tr>
                    <td style="width: 92px;">
                        <label class="mandatory">
                            <%: this.GetLocalResourceObject("PackageName") %>
                        </label>
                    </td>
                    <td>
                        <%= Html.Hidden("PackageId") %>
                        <%= Html.TextBox("PackageName", Model.PackageName, new { required = "required" })%>
                        <%= Html.CelloValidationMessage("PackageName", "*")%>
                    </td>
                </tr>
                <tr>
                    <td class="valignTop">
                        <label class="mandatory">
                            <%: this.GetLocalResourceObject("PackageDescription") %>
                        </label>
                    </td>
                    <td class="valignTop">
                        <%= Html.TextArea("PackageDescription", Model.PackageDescription, new { required = "required" })%>
                        <%= Html.CelloValidationMessage("PackageDescription", "*")%>
                    </td>
                </tr>
                <% if (!string.IsNullOrEmpty(Model.PackageId))
                   { %>
                <tr>
                    <td>
                        <label>
                            <%: this.GetLocalResourceObject("ReferenceId") %>
                        </label>
                    </td>
                    <td>
                        <%:Model.PackageId%>
                    </td>
                </tr>
                <% } %>
                <% if (isIsvOrReseller)
                   { %>
                <tr>
                    <td>
                        <label>
                            <%: this.GetLocalResourceObject("IsEvaluation") %>
                        </label>
                    </td>
                    <td>
                        <%=Html.CheckBox("IsEvaluation")%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <%: this.GetLocalResourceObject("TrialPreiod") %>
                        </label>
                    </td>
                    <td>
                        <%=Html.TextBox("ExpiryDays", Model.ExpiryDays)%>
                        <%=Html.ValidationMessage("ExpiryDays","*") %>
                        days
                    </td>
                </tr>
                <% } %>
            </tbody>
        </table>
    </div>
    <div class="right form-container" style="width: 69%;">
        <div class="heading">Selected Modules</div>
        <div id="selected-module-div" style="width: 30%; float: left; height: 250px; overflow-y: auto;">
            <ul class="module-ul">
                <% foreach (var module in Model.PacakageModuleDetails.Values)
                   { %>
                <li data-code="<%:module.ModuleCode %>">
                    <%:module.ModuleName %>
                    <button type="button" title="Click to remove this module!" class="close">&times;</button>
                    <div class="mod-feature hide">
                        <% if (module.FeatureDetails != null)
                           { %>
                        <ul class="feature-ul">
                            <% foreach (var feature in module.FeatureDetails.Values)
                               { %>
                            <li data-code="<%:feature.FeatureId %>">
                                <input type="checkbox" name="chk_assigned_<%:feature.FeatureId %>" checked="checked" />
                                <%:feature.FeatureName%>
                            </li>
                            <% } %>
                        </ul>
                        <% }
                           else
                           { %>
                        <div class="info">Features not available!</div>
                        <% } %>
                    </div>
                </li>
                <% } %>
            </ul>
        </div>
        <div style="width: 68%; float: left;">
            <div class="heading_container">
                <h1>Features</h1>
            </div>
            <div class="clear clearfix">
            </div>
            <div id="selected-feature-div">
                <div class="info">Please select a module to view its features!</div>
            </div>
        </div>
        <div class="clear clearfix">
        </div>
        <div id="available-module-div">
            <div class="blueHeading">
                <h3>Available Modules</h3>
            </div>
            <div style="width: 30%; float: left; height: 250px; overflow-y: auto;">
                <ul class="module-ul">
                    <%
                   var packageModule = ViewData["ModuleDetails"] as Dictionary<string, PackageModule> ?? new Dictionary<string, PackageModule>();
                   var remModules = Model.PacakageModuleDetails.Values.Where(x => !packageModule.ContainsKey(x.ModuleCode));
                    %>
                    <% foreach (var module in remModules)
                       { %>
                    <li data-code="<%:module.ModuleCode %>">
                        <%:module.ModuleName %>
                        <span title="Click to Add this module!" class="add close" style="font-size:14px;cursor:pointer;"><i class="fa fa-plus"></i></span>
                        <div class="mod-feature hide">
                            <% if (module.FeatureDetails != null)
                               { %>
                            <ul class="feature-ul">
                                <% foreach (var feature in module.FeatureDetails.Values)
                                   { %>
                                <li data-code="<%:feature.FeatureId %>">
                                    <%:feature.FeatureName%>
                                </li>
                                <% } %>
                            </ul>
                            <% }
                               else
                               { %>
                            <div class="info">Features not available!</div>
                            <% } %>
                        </div>
                    </li>
                    <% } %>
                </ul>
            </div>
            <div style="width: 68%; float: left;">
                <div class="heading_container">
                    <h1>Features</h1>
                </div>
                <div class="clear clearfix">
                </div>
                <div id="remaining-feature-div">
                    <div class="info">Please select a module to view its features!</div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear clearfix">
    </div>
    <div class="actions">
        <div class="green_but">
            <a href="<%:Url.Action("PackageList","Package") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Cancel") %></span></a>
        </div>
        <div class="green_but">
            <a href="#" title="<%=this.GetGlobalResourceObject("General","Save") %>" onclick="$('form').submit();">
                <span>
                    <%=this.GetGlobalResourceObject("General","Save") %></span></a>
        </div>
    </div>
    <div class="clear clearfix">
    </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#selected-module-div ul.module-ul li').click(function () {
                $('#selected-module-div ul.module-ul li.selected').removeClass('selected');
                $(this).addClass('selected');
                var html = $('div.mod-feature', $(this)).html();
                $('#selected-feature-div').hide().html(html).fadeIn();
            });

            $('#selected-module-div ul.module-ul li button.close').click(function () {
                var li = $(this).closest('li');
                li.remove();
            });

            $('#available-module-div ul.module-ul li').click(function () {
                $('#available-module-div ul.module-ul li.selected').removeClass('selected');
                $(this).addClass('selected');
                var html = $('div.mod-feature', $(this)).html();
                $('#remaining-feature-div').hide().html(html).fadeIn();
            });

            $('#available-module-div ul.module-ul li span.add').click(function () {
                var li = $(this).closest('li');
                var li = li.detach();
                li.removeClass('selected');
                $('#selected-module-div ul.module-ul').append(li);
            });
        });
    </script>
    <style type="text/css">
        ul.module-ul li {
            padding: 8px 5px;
            display: block;
            border-bottom: 1px solid #eee;
            cursor: pointer;
            position: relative;
        }

            ul.module-ul li:hover,
            ul.module-ul li.selected {
                background: #0094ff;
                color: #fff;
            }

            ul.module-ul li button,
            ul.module-ul li span.add {
                position: absolute;
                right: 5px;
                top: 5px;
            }

        ul.feature-ul li {
            padding: 5px;
            display: inline-block;
            margin-right: 5px;
            width: 42%;
            overflow: hidden;
        }
    </style>
</asp:Content>
