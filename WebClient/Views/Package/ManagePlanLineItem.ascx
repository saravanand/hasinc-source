﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Billing.Model.PricePlanLineItem>" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<% 
    var lineItem = Model ?? new PricePlanLineItem();
    if (lineItem.PriceTable == null)
        lineItem.PriceTable = new PriceTable();

    var lst = new Dictionary<string, string>();
    lst.Add(BillingConstants.CalculationTypeConstants.ThresholdPricing, "Threshold Pricing");
    lst.Add(BillingConstants.CalculationTypeConstants.StepPricing, "Step Pricing");
    var lstCalculationTypes = new SelectList(lst, "Key", "Value", lineItem.PriceTable.CalculationType);

    var packageDetails = ViewData["packageDetails"] as PackageDetails;

    var slabUsageSelectItems = new List<SelectListItem>();
    var actedonUsageSelectItems = new List<SelectListItem>();
    slabUsageSelectItems.Add(new SelectListItem { Text = "--Select--", Value = "" });
    actedonUsageSelectItems.Add(new SelectListItem { Text = "--Select--", Value = "" });

    if (packageDetails != null && packageDetails.PackageUsageDetails != null)
    {
        slabUsageSelectItems.AddRange(packageDetails.PackageUsageDetails.Select(x =>
            new SelectListItem
            {
                Text = x.Value.UsageName,
                Value = x.Value.UsageCode,
                Selected = (x.Value.UsageCode == lineItem.PriceTable.SlabMeterVariable)
            }
        ));

        actedonUsageSelectItems.AddRange(packageDetails.PackageUsageDetails.Select(x =>
            new SelectListItem
            {
                Text = x.Value.UsageName,
                Value = x.Value.UsageCode,
                Selected = (x.Value.UsageCode == lineItem.PriceTable.ActedOnMeterVariable)
            }
        ));
    }
%>
<% Html.RenderPartial("StatusMessage"); %>
<form name="frmPricePlanLineItem" id="frmPricePlanLineItem" action="<%=Url.Action("ManagePlanLineItem") %>" method="post">
    <div class="grid simple">
        <div class="grid-body no-border" style="padding: 0px;">
            <div class="row form-container">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required">
                            <%=this.GetGlobalResourceObject("General","Name") %></label>
                        <%=Html.TextBox("Name", lineItem.Name) %>
                        <%=Html.Hidden("Id", lineItem.Id)%>
                        <%=Html.Hidden("PricePlanId", lineItem.PricePlanId)%>
                        <%=Html.Hidden("PriceTableId", lineItem.PriceTableId)%>
                        <%=Html.Hidden("PriceTable.Id", lineItem.PriceTable.Id)%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%=this.GetGlobalResourceObject("General","Description") %>
                        </label>
                        <%=Html.TextArea("Description", lineItem.Description)%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required">
                            <%: this.GetLocalResourceObject("lbl_FactorType") %>
                        </label>
                        <%=Html.DropDownList("PriceTable.FactorType", typeof(FactorType).ToSelectList(), new { style="width:340px;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required">
                            <%: this.GetLocalResourceObject("lbl_CalculationType") %>
                        </label>
                        <%=Html.DropDownList("PriceTable.CalculationType", lstCalculationTypes, new { style="width:340px;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="required">
                            <%: this.GetLocalResourceObject("lbl_SlabUsageVariable") %>
                        </label>
                        <%=Html.DropDownList("PriceTable.SlabMeterVariable", slabUsageSelectItems, new { style="width:340px;" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <%: this.GetLocalResourceObject("lbl_ActedOnUsageVariable") %></label>
                        <%=Html.DropDownList("PriceTable.ActedOnMeterVariable", actedonUsageSelectItems, new { style="width:340px;" })%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="panel green">
        <header class="panel-heading">
            <h4><%: this.GetLocalResourceObject("t_SlabRates") %></h4>
        </header>
        <div class="panel-body">
            <div class="form-container">
                <div class="slabeRatesDiv">
                    <table class="celloTable" id="priceSlabsTable">
                        <thead>
                            <tr>
                                <th><%: this.GetLocalResourceObject("lbl_StartValue") %>
                                </th>
                                <th><%: this.GetLocalResourceObject("lbl_EndValue") %>
                                </th>
                                <th><%: this.GetLocalResourceObject("lbl_CostFactor") %> (<%=Html.GetCurrencySymbol() %>)
                                </th>
                                <th style="width: 30px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% if (lineItem.PriceTable.PriceSlabs != null && lineItem.PriceTable.PriceSlabs.Count > 0)
                               {
                                   foreach (var slab in lineItem.PriceTable.PriceSlabs)
                                   { 
                            %>
                            <tr>
                                <td>
                                    <input type="hidden" name="PriceTable.PriceSlabs[]Id" value="<%=slab.Id %>" />
                                    <input type="hidden" name="PriceTable.PriceSlabs[]PriceTableId" value="<%=slab.PriceTableId %>" />
                                    <input type="text" name="PriceTable.PriceSlabs[]StartValue" value="<%=slab.StartValue %>" />
                                </td>
                                <td>
                                    <input type="text" name="PriceTable.PriceSlabs[]EndValue" value="<%=slab.EndValue %>" />
                                </td>
                                <td>
                                    <input type="text" name="PriceTable.PriceSlabs[]Factor" value="<%=slab.Factor %>" />
                                </td>
                                <td class="halign">
                                    <a href="#" class="delPriceSlabs"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <%
                                   }
                               } 
                            %>
                        </tbody>
                    </table>
                    <div class="clearfix">
                    </div>
                    <br />
                    <div class="actions">
                        <a class="btn btn-info btnAddPriceSlab" id="btnAddPriceSlab" href="#" title="<%: this.GetLocalResourceObject("t_AddNewSlab") %>"><i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Add") %></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<script id="priceSlabTemplate" type="text/template">
    <tr>
        <td>
            <input type="hidden" name="PriceTable.PriceSlabs[]Id" value="<%=Guid.Empty %>" />
            <input type="hidden" name="PriceTable.PriceSlabs[]PriceTableId" value="<%=lineItem.PriceTableId%>" />
            <input type="text" name="PriceTable.PriceSlabs[]StartValue" value="" />
        </td>
        <td>
            <input type="text" name="PriceTable.PriceSlabs[]EndValue" value="" />
        </td>
        <td>
            <input type="text" name="PriceTable.PriceSlabs[]Factor" value="" />
        </td>
        <td class="halign">
            <a href="#" class="delPriceSlabs"><i class="fa fa-trash-o"></i></a>
        </td>
    </tr>
</script>
<script type="text/javascript">
    $(function () {
        $('#frmPricePlanLineItem select').select2();
    });
</script>
<style type="text/css">
    .required:after {
        content: " *";
        color: red;
    }
</style>
