﻿<%@ Page Title="<%$ Resources:t_ManagePricePlan%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Billing.Model.PricePlan>" %>

<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var packageDetails = ViewData["packageDetails"] as PackageDetails;
        string packageId = packageDetails != null ? packageDetails.PackageId : string.Empty;
    %>
    <div class="page-title">
        <a href="<%=Url.Action("PricePlanList",new{ packageId = packageId}) %>" title="<%: this.GetLocalResourceObject("M_goback") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%: this.GetLocalResourceObject("ManagePricePlans") %>
            <%=packageDetails!=null ? "- <span class=\"semi-bold\">" + packageDetails.PackageName + "</span>" : string.Empty%>
        </h3>
        <div class="pull-right">
            <a class="btn btn-success" href="#btnSave" id="btnSave" title="<%: this.GetLocalResourceObject("M_SavePlan") %>">
                <i class="fa fa-save"></i>&nbsp; <%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="<%=Url.Action("PricePlanList",new{ packageId = packageId}) %>" title="<%: this.GetLocalResourceObject("M_goback") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <% }
           else
           { %>
        <% Html.RenderPartial("StatusMessage"); %>
        <% } %>
        <div id="statusMessage" class="hide">
        </div>
        <% if (Model == null)
           { %>
        <div class="alert alert-danger">
            <%: this.GetLocalResourceObject("e_PricePlan") %>
        </div>
        <% }
           else
           { %>
        <div class="form-container">
            <% using (Html.BeginForm("ManagePricePlan", "Package", FormMethod.Post, new { id = "frmMangePricePlan" }))
               { %>
            <section class="panel purple">
                <header class="panel-heading">
                    <h4> <%: this.GetLocalResourceObject("t_PlanDetails") %>
                        <% if (Model.Id != Guid.Empty)
                           { %>
                        <span class="semi-bold">(<%=this.GetLocalResourceObject("ReferenceId") %>:<%:Model.Id %>)</span>
                        <% } %>
                    </h4>
                </header>
                <div class="panel-body form-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <%=Html.ValidationMessage("PricePlan.Name", "*") != null ? "has-error" : ""%>">
                                <label for="PricePlan_Name" class="mandatory">
                                    <%: this.GetLocalResourceObject("lbl_PlanName") %></label>
                                <%=Html.Hidden("packageId", packageId)%>
                                <%=Html.TextBox("PricePlan.Name", Model.Name)%>
                                <%=Html.ValidationMessage("PricePlan.Name", "*")%>
                                <%=Html.Hidden("PricePlan.Id", Model.Id)%>
                                <%=Html.Hidden("PricePlan.PackageId", packageId)%>
                                <%=Html.Hidden("PricePlan.CreatedBy", Model.CreatedBy)%>
                                <%=Html.Hidden("PricePlan.UpdatedBy", Model.UpdatedBy)%>
                                <%=Html.Hidden("PricePlan.Status", Model.Status)%>
                                <%=Html.Hidden("PricePlan.SkipCalculation", false)%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="PricePlan_Description">
                                    <%: this.GetLocalResourceObject("lbl_PlanDescription") %></label>
                                <%=Html.TextArea("PricePlan.Description", Model.Description)%>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="PricePlan_Price">
                                    <%: this.GetLocalResourceObject("lbl_PlanPrice") %></label>
                                <div class="input-group">
                                    <%=Html.TextBox("PricePlan.Price", Model.Price)%>
                                    <span class="input-group-addon">
                                        <%=Html.GetCurrencySymbol() %>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="mandatory" for="PricePlan_BillFrequency">
                                    <%: this.GetLocalResourceObject("lbl_BillFrequency") %></label>
                                <br />
                                <%=Html.DropDownList("PricePlan.BillFrequency", typeof(BillFrequency).ToSelectList(Model.BillFrequency), new { style="width:100%;" })%>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <% } %>
            <div id="lineitemscontainer">
                <section class="panel red">
                    <header class="panel-heading">
                        <h4><%: this.GetLocalResourceObject("LineItems") %></h4>
                        <% if (Model.Id != Guid.Empty)
                           { %>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-info" href="#btnAddNewLineItem" id="btnAddNewLineItem" title="<%: this.GetLocalResourceObject("M_AddNew") %>"><span>
                                <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Add") %></span></a>
                        </div>
                        <% } %>
                    </header>
                    <div class="panel-body">
                        <% if (Model.Id == Guid.Empty)
                           { %>
                        <div class="alert alert-info">
                            <%: this.GetLocalResourceObject("e_SavePricePlan") %>
                        </div>
                        <% }
                           else
                           { %>
                        <div id="pricePlanLineItemsDiv">
                            <% Html.RenderPartial("PlanLineItemsGrid", Model.LineItems ?? new List<PricePlanLineItem>()); %>
                        </div>
                        <% } %>
                    </div>
                </section>
            </div>
            <div class="modal fade" id="pricePlanLineItemModal" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
                <div class="modal-dialog" style="left: -7%;">
                    <div class="modal-content" style="width: 800px;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="model-title"><%: this.GetLocalResourceObject("ManageLineItem") %></h4>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <div class="actions">
                                <button type="button" id="btnLineItemSave" title="<%=this.GetGlobalResourceObject("General","m_SavelineItem") %>" class="btn btn-primary">
                                    <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %>
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close") %></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#pricePlanLineItemModal').modal({ show: false });

            $('#btnSave').click(function (e) {
                $('form#frmMangePricePlan').submit();
                return false;
            });

            $('input[type=checkbox][name="PricePlan.SkipCalculation"]').change(function () {
                var checked = $(this).is(':checked');

                if (checked) {
                    $('select[name="PricePlan.BillFrequency"]').attr('disabled', 'disabled');
                    $('input[name="PricePlan.Price"]').attr('disabled', 'disabled');
                    $('input[name="PricePlan.Price"]').val('0');
                    $('#lineitemscontainer').fadeOut();
                } else {
                    $('select[name="PricePlan.BillFrequency"]').attr('disabled', null);
                    $('input[name="PricePlan.Price"]').attr('disabled', null);
                    $('#lineitemscontainer').fadeIn();
                }
            });

            $('input[type=checkbox][name="PricePlan.SkipCalculation"]').trigger('change');

            $('#btnAddNewLineItem').click(function (e) {
                var pricePlanId = $('input#PricePlan_Id').val();
                $.get('/Package/ManagePlanLineItem', { pricePlanId: pricePlanId }, function (data) {
                    $('#pricePlanLineItemModal .modal-body').html(data);
                    $('#pricePlanLineItemModal').modal('show');
                });
                return false;
            });

            $('#pricePlanLineItemModal').on('click', 'a.btnAddPriceSlab', function (e) {
                e.preventDefault();
                var tmp = $('#priceSlabTemplate').html();
                var $template = $(tmp);
                //$template.hide();
                $('table#priceSlabsTable tbody').append($template);
                //$template.fadeIn(200);
                return false;
            });

            $('#pricePlanLineItemModal').on('click', 'a.delPriceSlabs', function (e) {
                e.preventDefault();
                //$(this).closest('tr').remove();
                var el = $(this).closest('tr');
                el.animate({ backgroundColor: 'lightcyan' }, 100).fadeOut(200, function () {
                    el.remove();
                });
                return false;
            });

            $(document).on('click', '#btnLineItemCancel', function (e) {
                $('#pricePlanLineItemModal').modal('hide');
                $('#pricePlanLineItemModal .modal-body').empty();
                return false;
            });

            $(document).on('click', '#btnLineItemSave', function (e) {
                var postdata = $('form#frmPricePlanLineItem').serializeJSON();
                var pricePlanId = $('input#PricePlan_Id').val();
                var packageId = $('input#PricePlan_PackageId').val();

                if (postdata['PriceTable.PriceSlabs']) {
                    var tmp = postdata['PriceTable.PriceSlabs'];
                    var slabs = [];

                    if ($.isArray(postdata['PriceTable.PriceSlabs']['StartValue'])) {
                        for (i = 0; i < postdata['PriceTable.PriceSlabs']['StartValue'].length; i++) {
                            slabs.push({
                                'Id': tmp.Id[i],
                                'PriceTableId': tmp.PriceTableId[i],
                                'StartValue': tmp.StartValue[i],
                                'EndValue': tmp.EndValue[i],
                                'Factor': tmp.Factor[i]
                            });
                        }
                    } else {
                        slabs.push({
                            'Id': tmp.Id,
                            'PriceTableId': tmp.PriceTableId,
                            'StartValue': tmp.StartValue,
                            'EndValue': tmp.EndValue,
                            'Factor': tmp.Factor
                        });
                    }

                    postdata['PriceTable.PriceSlabs'] = slabs;
                }

                //console.log(postdata);

                $.ajax({
                    url: '/Package/ManagePlanLineItem',
                    dataType: 'json',
                    contentType: 'application/json',
                    type: 'POST',
                    data: JSON.stringify({ lineItem: postdata }),
                    success: function (data) {
                        if (data.Success) {
                            //alert(data.Success);
                            ShowSuccessMsg(data.Success);

                            $('#pricePlanLineItemsDiv').load('/Package/ManagePricePlan?packageId=' + packageId + '&pricePlanId=' + pricePlanId);
                            $('#pricePlanLineItemModal').modal('hide');
                        } else {
                            var errHtml ='<%: this.GetLocalResourceObject("e_updateLineItem") %>';
                            if (data.Error) {
                                var errs = data.Error.split(';');
                                errHtml = '<%: this.GetLocalResourceObject("e_fixError") %>';
                                for (i = 0; i < errs.length; ++i) {
                                    errHtml += errs[i] + '\n';
                                }
                            }
                            alert(errHtml);
                        }
                    }
                });
                return false;
            });

            $('#pricePlanLineItemsDiv').on('click', 'a.editlineitem', function () {
                var pricePlanId = $(this).data('priceplanid');
                var lineItemId = $(this).data('lineitemid');
                $.get('/Package/ManagePlanLineItem', { pricePlanId: pricePlanId, lineItemId: lineItemId }, function (data) {
                    $('#pricePlanLineItemModal .modal-body').html(data);
                    $('#pricePlanLineItemModal').modal('show');
                });
                return false;
            });

            $('#pricePlanLineItemsDiv').on('click', 'a.deletelineitem', function () {
                if (!confirm('Are you sure to delete this line item?')) {
                    return false;
                }
                var pricePlanId = $(this).data('priceplanid');
                var lineItemId = $(this).data('lineitemid');
                var packageId = $(this).data('packageid');
                $.post('/Package/DeletePlanLineItem', { pricePlanId: pricePlanId, lineItemId: lineItemId }, function (data) {
                    if (data.Error) {
                        //alert(data.Error);
                        ShowErrorMsg(data.Error);
                    } else if (data.Success) {
                        //alert(data.Success);
                        ShowSuccessMsg(data.Success);
                        $('#pricePlanLineItemsDiv').load('/Package/ManagePricePlan?packageId=' + packageId + '&pricePlanId=' + pricePlanId);
                    }
                });
                return false;
            });

            $('#pricePlanLineItemsDiv').on('click', 'a.viewpriceslabs', function () {
                var priceSlabs = $(this).data('priceslabs');
                //console.log(priceSlabs);
                var $tr = $(this).closest('tr');

                if ($tr.hasClass('highlight')) {
                    $tr.removeClass('highlight');
                    $tr.next().fadeOut('normal', function () { $tr.next().remove(); });
                } else {
                    $tr.addClass('highlight');

                    if (priceSlabs && priceSlabs.length > 0) {
                        var html = '<tr class="priceSlabs" style="display: table-row;">';
                        html += '<td colspan="9">';
                        html += '<table class="celloTable table-bordered" style="width:75%;margin:0 auto;">';
                        html += '<thead>';
                        html += '<tr>';
                        html += '<th>Start Value</th>';
                        html += '<th>End Value</th>';
                        html += '<th>Cost Factor (<%=Html.GetCurrencySymbol() %>)</th>';
                        html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                        for (i = 0; i < priceSlabs.length; i++) {
                            html += '<tr>';
                            html += '<td>' + priceSlabs[i].StartValue + '</td>';
                            html += '<td>' + (priceSlabs[i].EndValue || '&infin;') + '</td>';
                            html += '<td>' + priceSlabs[i].Factor + '</td>';
                            html += '</tr>';
                        }
                        html += '</tbody>';
                        html += '</table>';
                        html += '</td>'
                        html += '</tr>';
                        //console.log($(html));
                        var el = $(html);
                        $(this).closest('tr').after(el);
                        el.fadeIn();
                    } else {
                        var html = '<tr class="priceSlabs">';
                        html += '<td colspan="9"><div class="alert alert-info"><%: this.GetLocalResourceObject("e_NoPriceSlabs") %></div></td>'
                        html += '</tr>';
                        //console.log(html);
                        var el = $(html);
                        el.hide();
                        $(this).closest('tr').after(el);
                        el.hide().fadeIn();
                    }
                }

                return false;
            });
        });
    </script>
</asp:Content>
