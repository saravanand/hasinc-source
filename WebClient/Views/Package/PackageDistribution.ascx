﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaSApplication.Models" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%    
    var lstTenants = ViewData["TenantDetails"] as IEnumerable<TenantViewModel> ?? new List<TenantViewModel>();
    var totalTenants = (double)lstTenants.Count();

    var grp = lstTenants.Select(x => new { x.License.PackageName, x.TenantDetails.TenantName })
                        .GroupBy(x => x.PackageName);
    var tmp = new List<dynamic>();

    foreach (var x in grp)
    {
        tmp.Add(new
        {
            category = x.Key,
            value = Math.Round((1.0 - (totalTenants - (double)x.Count()) / totalTenants) * 100.0, 2)
        });
    }

    string chartData = Newtonsoft.Json.JsonConvert.SerializeObject(tmp, Newtonsoft.Json.Formatting.None); 
%>
<% if (totalTenants > 0)
   { %>
<div id="packageDistributionChartDiv">
    <%:Html.Hidden("package_chartData", chartData) %>
    <div class="clearfix"></div>
    <div id="package_chart_area"></div>
</div>
<% }
   else
   { %>
<div class="alert alert-info mg-5"><%: this.GetLocalResourceObject("e_NoTenants")%></div>
<% } %>
<script type="text/javascript">
    $(function () {
        if ($('#package_chartData').length > 0) {
            var package_dist_data = JSON.parse($('#package_chartData').val());

            function createPackageDashChart() {
                var options = {
                    title: {
                        visible: true
                    },
                    dataSource: { data: package_dist_data },
                    legend: {
                        visible: true,
                        position: 'bottom'
                    },
                    seriesDefaults: {
                        type: 'pie',
                        stack: false,
                        labels: {
                            template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                            position: "outsideEnd",
                            visible: true,
                            background: "transparent",
                            align: "column"
                        },
                        tooltip: {
                            visible: true
                        }
                    },
                    series: [{
                        type: 'pie',
                        data: package_dist_data
                    }],
                    tooltip: {
                        visible: true,
                        template: "#= category # - #= kendo.format('{0:P}', percentage) #"
                    }
                };

                $('#package_chart_area').celloChart(options);
            }

            setTimeout(function () {
                try {
                    createPackageDashChart();
                } catch (e) { console.log(e); }
            }, 100);
        }
    });
</script>
