<%@ Page Title="<%$ Resources:PackageDetailsList %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("PackageDetailsList") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("PackageSuccessMessage")))
          { %>
        <div class="alert alert-success">
            <%=Html.CelloValidationMessage("PackageSuccessMessage")%>
        </div>
        <%} %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("PackageErrorMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <%} %>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input id="searchText" name="searchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 m-b-xs pull-right">
                        <div class="pull-right">
                            <%=Html.CelloCreateButton(Url.Action("AddPackage"), "<i class=\"fa fa-plus\"></i> "+this.GetGlobalResourceObject("General","Create"),null,PrivilegeConstants.AddPackage) %>
                        </div>
                    </div>
                </div>
            </div>
            <div id="packageGridDiv" class="grid-body">
                <%  var packageList = ViewData["PackageDetails"] as IEnumerable<LicensePackage>;
                    if (packageList != null && packageList.Count() > 0)
                    {
                        bool viewPricePlan = UserIdentity.HasPrivilege(PrivilegeConstants.ViewPricePlan);

                        Html.Grid(packageList).Columns(
                          column =>
                          {
                              column.For(col => string.Format("<span title=\"{1}\">{0}</span>", col.Name, col.Description))
                                  .Named(this.GetGlobalResourceObject("General", "Name").ToString())
                                  .Attributes(style => "min-width:200px;")
                                  .DoNotEncode();

                              if (viewPricePlan)
                              {
                                  column.For(col => col.ExpiryDays)
                                            .Named(this.GetLocalResourceObject("TrialPeriod").ToString())
                                            .Attributes(style => "text-align:right;")
                                            .HeaderAttributes(@class => "halign", style => "min-width:70px;")
                                            .DoNotEncode();

                                  column.For(col => "<a title='Manage price plans' href='" + Url.Action("PricePlanList", new { packageId = col.LicensepackageId }) + "'><i class='fa fa-wrench'></i></a>").Named(this.GetLocalResourceObject("ManagePricePlans").ToString())
                                      .Attributes(@class => "halign").HeaderAttributes(@class => "halign").DoNotEncode();
                              }
                              else
                              {
                                  column.For(col => col.Description)
                                               .Named(this.GetLocalResourceObject("Description").ToString());
                              }

                              column.For(col => Html.CelloActionLink("Edit Package", "EditPackage", new { packageId = col.LicensepackageId }, new { title = this.GetGlobalResourceObject("General", "Edit") + "\"" + col.Name + "\" details" }).Replace("Edit Package", "<i class='fa fa-edit'></i>")).Named(this.GetGlobalResourceObject("General", "Edit").ToString())
                                  .Attributes(@class => "halign")
                                  .HeaderAttributes(@class => "halign", style => "min-width:50px;")
                                  .DoNotEncode().Visible(UserIdentity.HasPrivilege(PrivilegeConstants.UpdatePackage));
                          }).Attributes(id => "dataList", @class => "celloTable").Render();
                    }
                    else
                    {
                %>
                <div class="alert alert-info">
                    <%: this.GetLocalResourceObject("PackageDetailsnotavailable.")%>
                </div>
                <% } %>
            </div>
        </div>
        <% if (packageList != null && packageList.Count() > 0)
           { %>
        <section class="panel red">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("PackageDistribution")%></h4>
            </header>
            <div class="panel-body">
                <div id="packageDisDiv">
                </div>
            </div>
        </section>
        <% } %>
    </div>

    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>
    <script type="text/javascript">
        var filterTable = null;
        $(document).ready(function () {
            filterTable = $('#dataList').dataTable({
                "iDisplayLength": 10,
                "bAutoWidth": true,
                "bFilter": true,
                "aaSorting": [[0, "asc"]],
                "bRetrieve": true,
                "bStateSave": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }]
            });
            $(document).on('keypress', '#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });

            $('#packageDisDiv').load('<%= Url.Action("PackageDistribution", "Package", new { area = "" })%>');
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }
    </script>
</asp:Content>
