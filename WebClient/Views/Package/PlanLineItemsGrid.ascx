﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ICollection<CelloSaaS.Billing.Model.PricePlanLineItem>>" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%
    var packageDetails = ViewData["packageDetails"] as PackageDetails;
    string packageId = packageDetails != null ? packageDetails.PackageId : string.Empty;
    var calcTypes = new Dictionary<string, string>();
    calcTypes.Add(BillingConstants.CalculationTypeConstants.ThresholdPricing, "Threshold Pricing");
    calcTypes.Add(BillingConstants.CalculationTypeConstants.StepPricing, "Step Pricing");
%>
<% if (Model != null && Model.Count > 0)
   {
       Html.Grid(Model).Columns(column =>
       {
           column.For(x => x.Name).Named(this.GetGlobalResourceObject("General", "Name").ToString());
           column.For(x => x.Description ?? "--").Named(this.GetGlobalResourceObject("General", "Description").ToString());
           column.For(x => x.PriceTable.SlabMeterVariable).Named(this.GetLocalResourceObject("lbl_SlabUsageVariable").ToString());
           column.For(x => x.PriceTable.ActedOnMeterVariable ?? "--").Named(this.GetLocalResourceObject("lbl_ActedOnUsageVariable").ToString());
           column.For(x => calcTypes.ContainsKey(x.PriceTable.CalculationType)
               ? calcTypes[x.PriceTable.CalculationType]
               : x.PriceTable.CalculationType).Named(this.GetLocalResourceObject("lbl_CalculationType").ToString());
           column.For(x => string.Format("<a href='#' class='viewpriceslabs' data-priceplanid='{0}' data-lineitemid='{1}' data-packageid='{2}' data-priceslabs='{3}'><i class='fa fa-search'></i></a>", x.PricePlanId, x.Id, packageId, new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(x.PriceTable.PriceSlabs)))
               .Named(this.GetLocalResourceObject("ViewSlabs").ToString())
               .Attributes(@class => "halign").HeaderAttributes(@class => "halign").DoNotEncode();
           column.For(x => string.Format("<a href='#' class='editlineitem' data-priceplanid='{0}' data-lineitemid='{1}' data-packageid='{2}'><i class='fa fa-pencil'></i></a>", x.PricePlanId, x.Id, packageId))
               .Named(this.GetGlobalResourceObject("General", "Edit").ToString()).Attributes(@class => "halign").HeaderAttributes(@class => "halign").DoNotEncode();
           column.For(x => string.Format("<a href='#' class='deletelineitem' data-priceplanid='{0}' data-lineitemid='{1}' data-packageid='{2}'><i class='fa fa-trash-o'></i></a>", x.PricePlanId, x.Id, packageId))
               .Named(this.GetGlobalResourceObject("General", "Delete").ToString()).Attributes(@class => "halign").HeaderAttributes(@class => "halign").DoNotEncode();
       }).Attributes(id => "pricePlanLineItemTable", @class => "celloTable").Render();
   }
   else
   {
%>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("e_LineItem") %>   
</div>
<%
   }
%>