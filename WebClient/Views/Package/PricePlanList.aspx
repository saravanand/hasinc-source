﻿<%@ Page Title="<%$ Resources:t_PricePlanListing %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<Dictionary<Guid, CelloSaaS.Billing.Model.PricePlan>>" %>

<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var packageDetails = ViewData["packageDetails"] as PackageDetails;
        string packageId = packageDetails != null ? packageDetails.PackageId : string.Empty;
        string packageName = packageDetails != null ? packageDetails.PackageName : string.Empty;
    %>
    <div class="page-title">
        <a href="<%=Url.Action("PackageList") %>" title="<%: this.GetLocalResourceObject("t_GoBack")%>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: this.GetLocalResourceObject("PricePlansfor")%> - <span class="semi-bold">
            <%:packageName %></span></h3>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input id="searchText" name="searchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 m-b-xs pull-right">
                        <div class="pull-right">
                            <%=Html.CelloCreateButton(Url.Action("ManagePricePlan",new { packageId=packageId }), "<i class=\"fa fa-plus\"></i> "+this.GetGlobalResourceObject("General","Create"),null,PrivilegeConstants.AddPricePlan) %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <% if (Model != null && Model.Count > 0)
                   { %>
                <%
                       bool editPricePlan = UserIdentity.HasPrivilege(PrivilegeConstants.EditPricePlan);
                       bool deletePricePlan = UserIdentity.HasPrivilege(PrivilegeConstants.DeletePricePlan);
                       Html.Grid(Model.Values).Columns(col =>
                       {
                           col.For(x => x.Name).Named(this.GetLocalResourceObject("lbl_Planname").ToString());
                           col.For(x => x.Description).Named(this.GetLocalResourceObject("lbl_Plandescription").ToString());
                           col.For(x => x.Price.ToBillingCurrency("C")).Named(this.GetLocalResourceObject("lbl_Price").ToString())
                               .HeaderAttributes(style => "text-align:right;").Attributes(style => "text-align:right;");
                           col.For(x => x.BillFrequency.ToString()).Named(this.GetLocalResourceObject("lbl_Billfrequency").ToString());
                           /*col.For(x => x.SkipCalculation ? "<i class='fa fa-check'></i>" : "<i class='fa fa-times'></i>")
                               .Named(this.GetLocalResourceObject("lbl_Skipcalculation").ToString())
                               .HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();*/
                           if (editPricePlan)
                           {
                               col.For(x => "<a href='" + Url.Action("ManagePricePlan", new { pricePlanId = x.Id, packageId = x.PackageId }) + "'><i class='fa fa-pencil'></i></a>")
                                   .Named(this.GetGlobalResourceObject("General", "Edit").ToString())
                                   .HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                           }
                           if (deletePricePlan)
                           {
                               col.For(x => "<a href='" + Url.Action("DeletePricePlan", new { pricePlanId = x.Id, packageId = x.PackageId }) + "'><i class='fa fa-trash-o'></i></a>")
                                   .Named(this.GetGlobalResourceObject("General", "Delete").ToString())
                                   .HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                           }
                       }).Attributes(@class => "celloTable", id => "pricePlanTable").Render();
                %>
                <div class="alert alert-info" style="margin-top: 20px;">
                    <%:this.GetLocalResourceObject("info_priceplan") %>
                </div>
                <% }
                   else
                   { %>
                <div class="alert alert-info">
                    <%: this.GetLocalResourceObject("e_ Priceplans")%>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //JQuery table extension plugin for currency sort
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "currency-pre": function (a) {
                a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
                return parseFloat(a);
            },

            "currency-asc": function (a, b) {
                return a - b;
            },

            "currency-desc": function (a, b) {
                return b - a;
            }
        });
        var filterTable = null;
        $(document).ready(function () {
            filterTable = $('#pricePlanTable').dataTable({
                "iDisplayLength": 10,
                "bAutoWidth": true,
                "bFilter": true,
                "aaSorting": [[0, "asc"]],
                "bRetrieve": true,
                "bStateSave": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }, { "sType": "currency", "aTargets": [2] }],
          
            });

            $(document).on('keypress', '#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });
      
        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }
    </script>
</asp:Content>
