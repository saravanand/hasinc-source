﻿<%@ Page Title="<%$ Resources:AddDataModuleTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.PartitionManagement.Model.DataModule>" %>

<%@ Import Namespace="CelloSaaS.PartitionManagement.ServiceProxies" %>
<%@ Import Namespace="CelloSaaS.PartitionManagement.Model" %>
<%@ Import Namespace="CelloSaaS.PartitionManagement.ServiceContracts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var idStatus = Model != null && Model.Id != Guid.Empty ? true : false;
        var title = idStatus ? this.GetLocalResourceObject("EditDataModuleTitle") : this.GetLocalResourceObject("AddDataModuleTitle");
        var mode = idStatus ? this.GetGlobalResourceObject("General", "Update") : this.GetGlobalResourceObject("General", "Save");
        var modeTitle = idStatus ? this.GetLocalResourceObject("UpdateDataModuleTitle") : this.GetLocalResourceObject("AddDataModuleTitle");
    %>
    <div class="page-title">
        <a href="ManageDataModule" title='<%: this.GetLocalResourceObject("BackDataModuleTitle")%>'>
            <i class="icon-custom-left"></i>
        </a>
        <h3><%:title%></h3>
        <div class="pull-right">
            <a class="btn btn-default" href="ManageDataModule" title='<%: this.GetLocalResourceObject("BackDataModuleTitle")%>'>
                <%: this.GetGlobalResourceObject("General", "Back")%></a>
            <a class="btn btn-info" href="#" onclick="$('#AddDataModuleFormID').submit();" title="<%:modeTitle%>">
                <i class="fa fa-save"></i>&nbsp;<%:mode %></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <div id="AddDataModuleForm">
            <%using (Html.BeginForm("AddDataModuleDetails", "PartitionManagement", FormMethod.Post, new { @id = "AddDataModuleFormID" }))
              { %>
            <section class="panel purple">
                <header class="panel-heading">
                    <h4>Manage Data Module Details</h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataModuleWarning")))
                           { %>
                        <div class="alert alert-danger">
                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                            <%=Html.CelloValidationSummary()%>
                        </div>
                        <%} %>
                        <% else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataModuleError")))
                           { %>
                        <div class="alert alert-danger">
                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                            <%=Html.CelloValidationMessage("DataModuleError")%>
                        </div>
                        <%} %>
                        <%if (this.Model != null)
                          { %>
                        <%:Html.HiddenFor(c=>c.Id)%>
                        <div class="form-group <%=Html.ValidationMessage("DataModuleName","*") != null ? "has-error" : "" %>">
                            <label class="mandatory">
                                <%: this.GetLocalResourceObject("DataModuleName")%>
                            </label>
                            <%= Html.TextBoxFor(c=>c.DataModuleName, new { maxlength = 100 })%>
                        </div>
                        <div class="form-group <%=Html.ValidationMessage("DataModuleCode","*") != null ? "has-error" : "" %>">
                            <label class="mandatory">
                                <%: this.GetLocalResourceObject("DataModuleCode")%>
                            </label>
                            <%= Html.TextBoxFor(c=>c.DataModuleCode, new { maxlength = 200 })%>
                        </div>
                        <div class="form-group <%=Html.ValidationMessage("Description","*") != null ? "has-error" : "" %>">
                            <label class="mandatory">
                                <%: this.GetLocalResourceObject("Description")%>
                            </label>
                            <%= Html.TextAreaFor(c=>c.Description)%>
                        </div>
                        <div class="form-group <%=Html.ValidationMessage("CreateScript","*") != null ? "has-error" : "" %>">
                            <label>
                                <%: this.GetLocalResourceObject("CreateScript")%>
                            </label>
                            <%= Html.TextAreaFor(c=>c.CreateScript, new {style = "width:100%;height:300px" })%>
                        </div>
                        <div class="form-group <%=Html.ValidationMessage("UpdateScript","*") != null ? "has-error" : "" %>">
                            <label>
                                <%: this.GetLocalResourceObject("UpdateScript")%>
                            </label>
                            <%= Html.TextAreaFor(c=>c.UpdateScript, new {style = "width:100%;height:300px" })%>
                        </div>
                        <div class="form-group pull-right">
                            <a class="btn btn-default" href="ManageDataModule" title='<%: this.GetLocalResourceObject("BackDataModuleTitle")%>'>
                                <%: this.GetGlobalResourceObject("General", "Back")%></a>
                            <a class="btn btn-info" href="#" onclick="$('#AddDataModuleFormID').submit();" title="<%:modeTitle%>">
                                <i class="fa fa-save"></i>&nbsp;<%:mode %></a>
                        </div>
                        <%} %>
                    </div>
                </div>
            </section>
            <%} %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Save() {
            $('#AddDataModuleFormID').submit();
        }
    </script>
</asp:Content>

