﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataPartition>" %>

<%var idStatus = Model != null && Model.Id != Guid.Empty ? true : false;
  var title = idStatus ? this.GetLocalResourceObject("EditDataPartitionTitle") : this.GetLocalResourceObject("AddDataPartitionTitle");
  var mode = idStatus ? this.GetGlobalResourceObject("General", "Update") : this.GetGlobalResourceObject("General", "Save");
  var modeTitle = idStatus ? this.GetLocalResourceObject("UpdateDataPartitionTitle") : this.GetLocalResourceObject("AddDataPartitionTitle");%>

<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4><%:title%></h4>
    </header>
    <div class="panel-body">
        <div class="form-container">
            <%using (Html.BeginForm("", "", FormMethod.Post, new { @id = "DataPartitionFormID" }))
              { %>
            <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionWarning")))
               { %>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                <%=Html.CelloValidationSummary()%>
            </div>
            <%} %>
            <%else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionError")))
               { %>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                <%=Html.CelloValidationMessage("DataPartitionError")%>
            </div>
            <%} %>

            <%if (this.Model != null)
              { %>
            <%:Html.HiddenFor(c=>c.Id)%>
            <div class="form-group <%=Html.ValidationMessage("DataPartitionName","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("DataPartitionName")%>
                </label>
                <%= Html.TextBoxFor(c => c.DataPartitionName, new { maxlength = 100 })%>
            </div>
            <div class="form-group <%=Html.ValidationMessage("Description","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("Description")%>
                </label>
                <%= Html.TextAreaFor(c=>c.Description)%>
            </div>
            <div class="form-group pull-right">
                <a class="btn btn-default" href="#" onclick="javascript:AddDataPartitions();" title="<%:this.GetLocalResourceObject("ResetDataPartitionTitle")%>"><%: this.GetGlobalResourceObject("General", "Reset")%></a>
                <a class="btn btn-info" href="#" onclick="javascript:AddDataPartitionDetailsForm();" title="<%:modeTitle %>"><%:mode%></a>
            </div>
            <%} %>
            <%} %>
        </div>

    </div>
</section>
