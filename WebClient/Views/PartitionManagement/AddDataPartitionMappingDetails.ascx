﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataPartitionMapping>" %>

<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% var idStatus = Model != null && Model.Id != Guid.Empty ? true : false;
   var title = idStatus ? this.GetLocalResourceObject("EditDataPartitionMappingTitle") : this.GetLocalResourceObject("AddDataPartitionMappingTitle");
   var mode = idStatus ? this.GetGlobalResourceObject("General", "Update") : this.GetGlobalResourceObject("General", "Save");
   var modeTitle = idStatus ? this.GetLocalResourceObject("UpdateDataPartitionMappingTitle") : this.GetLocalResourceObject("EditDataPartitionMappingTitle");
   var emptyList=new List<SelectListItem>(){new SelectListItem(){Text="No Data",Value=Guid.Empty.ToString()}};
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4><%=title%></h4>
    </header>
    <div class="panel-body">
        <div class="form-container">
            <% using (Html.BeginForm("", "", FormMethod.Post, new { @id = "AddDataPartitionMappingFormID" }))
               { %>
            <%if (ViewData["DataPartitionID"] == null || ViewData["DataPartitionID"] as Guid? == Guid.Empty)
              { %>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                No DataPartitionMapping Found
            </div>
            <%}
              else
              { %>
            <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionMappingWarning")))
               { %>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                <%=Html.CelloValidationSummary()%>
            </div>
            <%} %>

            <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionMappingError")))
               { %>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                <%=Html.CelloValidationMessage("DataPartitionMappingError")%>
            </div>
            <% } %>
            <%if (this.Model != null)
              { %>
            <%:Html.Hidden("DataPartitionID", ViewData["DataPartitionID"])%>
            <%:Html.HiddenFor(c=>c.Id) %>
            <div class="form-group <%=Html.ValidationMessage("DataModuleId","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("DataModuleName")%>
                </label>
                <%:Html.DropDownListFor(c => c.DataModuleId,ViewData["DataModuleList"]!=null? (List<SelectListItem>)ViewData["DataModuleList"]:emptyList, new { style="width:100%;" })%>
            </div>
            <div class="form-group <%=Html.ValidationMessage("DataServerId","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("DataServerName")%>
                </label>
                <%:Html.DropDownListFor(c => c.DataServerId, ViewData["DataServerList"]!=null?(List<SelectListItem>)ViewData["DataServerList"]:emptyList, new { style="width:100%;" })%>
            </div>
            <div class="form-group pull-right">
                <a class="btn btn-default" href="#" onclick="javascript:AddDataPartitionMappings();" title="<%:this.GetLocalResourceObject("ResetDatapartitionMappingTitle")%>"><%: this.GetGlobalResourceObject("General", "Reset")%></a>
                <a class="btn btn-info" href="#" onclick="javascript:AddDataPartitionMappingDetailsForm();" title="<%:modeTitle %>"><%:mode%></a>
            </div>
            <%} %>
            <% } %>
            <% } %>
        </div>

    </div>
</section>
<script type="text/javascript">
    $(function () {
        $("select[name='DataModuleId'],select[name='DataServerId']").select2();
    });
</script>
