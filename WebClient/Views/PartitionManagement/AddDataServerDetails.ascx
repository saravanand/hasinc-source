﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataServer>" %>

<% var idStatus = Model != null && Model.Id != Guid.Empty ? true : false;
   var title = idStatus ? this.GetLocalResourceObject("EditDataServer") : this.GetLocalResourceObject("AddDataServerTitle");
   var mode = idStatus ? this.GetGlobalResourceObject("General", "Update") : this.GetGlobalResourceObject("General", "Save");
   var modeTitle = idStatus ? this.GetLocalResourceObject("UpdateDataServerTitle") : this.GetLocalResourceObject("AddDataServerTitle");%>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4><%:title%></h4>
    </header>
    <div class="panel-body">
        <%using (Html.BeginForm("", "", FormMethod.Post, new { @id = "DataServerFormID" }))
          { %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataServerWarning")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>

        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataServerError")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataServerError")%>
        </div>
        <%} %>
        <%:Html.HiddenFor(c=>c.Id)%>
        <div class="form-container">
            <div class="form-group <%=Html.ValidationMessage("Name","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%: this.GetLocalResourceObject("DataServerName")%>
                </label>
                <%= Html.TextBoxFor(c=>c.Name, new { maxlength = 100})%>
            </div>
            <div class="form-group <%=Html.ValidationMessage("ServerAddress","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%: this.GetLocalResourceObject("ServerAddress")%>
                </label>
                <%= Html.TextBoxFor(c=>c.ServerAddress, new { maxlength = 200 })%>
            </div>
            <div class="form-group <%=Html.ValidationMessage("DataBaseName","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%: this.GetLocalResourceObject("DataBaseName")%>
                </label>
                <%= Html.TextBoxFor(c=>c.DataBaseName, new { maxlength = 200 })%>
            </div>
            <div class="form-group <%=Html.ValidationMessage("ConnectionString","*") != null ? "has-error" : "" %>">
                <label class="mandatory">
                    <%: this.GetLocalResourceObject("ConnectionString")%>
                </label>
                <%= Html.TextAreaFor(c=>c.ConnectionString)%>
            </div>
            <div class="pull-right">
                <a class="btn btn-default" href="#" onclick="javascript:AddDataServers();" title="<%: this.GetLocalResourceObject("ResetTitle")%>"><%: this.GetGlobalResourceObject("General", "Reset")%></a>
                <a class="btn btn-info" href="#" onclick="javascript:AddDataServerDetailsForm();" title="<%:modeTitle%>"><%:mode%></a>
            </div>
        </div>
        <%} %>
    </div>
</section>
