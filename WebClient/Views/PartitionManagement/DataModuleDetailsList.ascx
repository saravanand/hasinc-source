﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataModule>" %>

<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%var dataModuleList = ViewData["DataModuleList"] != null ? ViewData["DataModuleList"] as Dictionary<Guid, CelloSaaS.PartitionManagement.Model.DataModule> : null;%>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <% if (dataModuleList != null && dataModuleList.Count > 0)
               { %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="DataModuleTableSearchText" name="DataModuleTableSearchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                    </span>
                </div>
            </div>
            <% } %>
            <div class="col-md-6 pull-right">
                <div class="pull-right">
                    <a class="btn btn-success" href="AddDataModuleDetails" title="<%: this.GetLocalResourceObject("AddDataModuleDetails")%>">
                        <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Add")%>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataModuleWarning")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>

        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataModuleError")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataModuleError")%>
        </div>
        <%} %>
        <% if (TempData["DataModuleSuccess"] != null)
           { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=TempData["DataModuleSuccess"].ToString()%>
        </div>
        <%} %>

        <%if (dataModuleList != null && dataModuleList.Count > 0 && dataModuleList.Values != null && dataModuleList.Values.Count > 0)
          {
              Html.Grid(dataModuleList.Values).Columns(
            column =>
            {
                column.For(c => c.DataModuleName).Named(this.GetLocalResourceObject("DataModuleName").ToString()).Attributes(style => "width:120px").DoNotEncode();
                column.For(c => c.DataModuleCode).Named(this.GetLocalResourceObject("DataModuleCode").ToString()).Attributes(style => "width:220px").DoNotEncode();
                column.For(c => c.Description).Named(this.GetLocalResourceObject("Description").ToString())
                        .HeaderAttributes(@class => "noSortCol").DoNotEncode();
                column.For(c => "<a href=EditDataModuleDetails?dataModuleId=" + c.Id + " alt=Delete title='" + this.GetLocalResourceObject("EditDataModuleTitle") + "' ><i class='fa fa-edit'></i></a>").Named(this.GetLocalResourceObject("EditDataModule").ToString()).Attributes(@class => "halign", style => "width:60px;")
                        .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
                column.For(c => "<a href=# onclick='DeleteDataModuleDeatilsForm(\"" + c.Id + "\",\"" + c.DataModuleName.ToString().Trim() + "\")' alt='Delete' title='" + this.GetLocalResourceObject("DeleteDataModuleTitle") + "'><i class='fa fa-trash-o'></i></a>").Named(this.GetLocalResourceObject("DeleteDataModule").ToString()).Attributes(@class => "halign", style => "width:60px;")
                        .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
            }).Attributes(id => "dataList", @class => "celloTable").Render();
          } %>
        <%else
          { %>
        <div class="alert alert-info">
            <%:this.GetGlobalResourceObject("General", "NoData").ToString()%>
        </div>
        <%} %>
    </div>
</div>
