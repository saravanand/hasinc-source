﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataPartition>" %>

<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%var dataPartitionList = ViewData["DataPartitionList"] != null ? ViewData["DataPartitionList"] as Dictionary<Guid, CelloSaaS.PartitionManagement.Model.DataPartition> : null;%>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <% if (dataPartitionList != null && dataPartitionList.Count > 0)
               { %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="DataPartitionTableSearchText" name="DataPartitionTableSearchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                    </span>
                </div>
            </div>
            <% } %>
        </div>
    </div>
    <div class="grid-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionWarning")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>

        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionError")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataPartitionError")%>
        </div>
        <%} %>

        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionSuccess")))
           { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataPartitionSuccess")%>
        </div>
        <%} %>

        <%if (dataPartitionList != null && dataPartitionList.Count > 0 && dataPartitionList.Values != null && dataPartitionList.Values.Count() > 0)
          {
              Html.Grid(dataPartitionList.Values).Columns(
            column =>
            {
                column.For(c => c.DataPartitionName).Named(this.GetLocalResourceObject("DataPartitionName").ToString()).Attributes(style => "width:120px").DoNotEncode();
                column.For(c => c.Description).Named(this.GetLocalResourceObject("Description").ToString())
                        .HeaderAttributes(@class => "noSortCol").DoNotEncode();
                column.For(c => c.IsDefault == true ? Html.CheckBox("IsDefault", c.IsDefault, new { id = c.Id + "IsDefault", disabled = "true" }) : Html.CheckBox("IsDefault", c.IsDefault, new { id = c.Id + "IsDefault", onchange = "MakeDefalutForm('" + c.Id + "')" })).Named(this.GetLocalResourceObject("IsDefaultDataPartition").ToString()).Attributes(@class => "halign")
                       .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
                column.For(c => "<a href=# onclick=EditDataPartitionDetailsForm('" + c.Id + "') alt=Edit title='" + this.GetLocalResourceObject("EditDataPartitionTitle") + "'><i class='fa fa-edit'></i></a>").Named(this.GetLocalResourceObject("EditDataPartition").ToString()).Attributes(@class => "halign", style => "width:60px;")
                        .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
                column.For(c => "<a href=ManageDataPartitionMapping?dataPartitionId=" + c.Id + " title='" + this.GetLocalResourceObject("MapDataPartitionTitle") + "'><i class='fa fa-wrench'> </i> </a>").Named(this.GetLocalResourceObject("MapDataPartition").ToString()).Attributes(@class => "halign", style => "width:60px;")
                        .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
            }).Attributes(id => "dataList", @class => "celloTable").Render();
          } %>
        <%else
          { %>
        <div class="alert alert-info">
            <%:this.GetGlobalResourceObject("General", "NoData").ToString()%>
        </div>
        <%} %>
    </div>
</div>
