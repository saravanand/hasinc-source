﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataPartitionMapping>" %>

<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%var dataPartitionMappingList = ViewData["DataPartitionMappingList"] != null ? ViewData["DataPartitionMappingList"] as IEnumerable<CelloSaaS.PartitionManagement.Model.DataPartitionMapping> : null;%>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <% if (dataPartitionMappingList != null && dataPartitionMappingList.Count() > 0)
               { %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="DataPartitionMappingTableSearchText" name="DataPartitionMappingTableSearchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                    </span>
                </div>
            </div>
            <% } %>
        </div>
    </div>
    <div class="grid-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionMappingWarning")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>
        <%else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionMappingError")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataPartitionMappingError")%>
        </div>
        <%} %>
        <%else  if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataPartitionMappingSuccess")))
           { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataPartitionMappingSuccess")%>
        </div>
        <%} %>

        <%if (dataPartitionMappingList != null && dataPartitionMappingList.Count() > 0)
          {

              Html.Grid(dataPartitionMappingList).Columns(
             column =>
             {
                 column.For(c => c.DataModule.DataModuleName).Named(this.GetLocalResourceObject("DataModuleName").ToString()).Attributes(style => "width:120px").DoNotEncode();
                 column.For(c => c.DataServer.Name).Named(this.GetLocalResourceObject("DataServerName").ToString()).Attributes(style => "width:120px").DoNotEncode();
                 column.For(c => "<a href=# onclick=EditDataPartitionMappingDetailsForm('" + c.Id + "') alt=Edit title='" + this.GetLocalResourceObject("EditDataPartitionMappingTitle") + "'> <i class='fa fa-edit' ></i></a>").Named(this.GetLocalResourceObject("EditDataPartitionMapping").ToString()).Attributes(@class => "halign", style => "width:60px;")
                         .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
                 column.For(c => "<a href=# onclick=DeleteDataPartitionMappingDetailsForm('" + c.Id + "','" + c.DataPartitionId + "') alt=Delete title='" + this.GetLocalResourceObject("DeleteDataPartitionMappingTitle") + "'><i class='fa fa-trash-o'></i></a>").Named(this.GetLocalResourceObject("DeleteDataPartitionMapping").ToString()).Attributes(@class => "halign", style => "width:60px;")
                         .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
             }).Attributes(id => "dataList", @class => "celloTable").Render();
          } %>
        <%else
          { %>
        <div class="alert alert-info">
            <%:this.GetGlobalResourceObject("General", "NoData").ToString()%>
        </div>
        <%} %>
    </div>
</div>
