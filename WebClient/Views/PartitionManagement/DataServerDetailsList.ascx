﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataServer>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%var dataServerList = ViewData["DataServerList"] != null ? ViewData["DataServerList"] as Dictionary<Guid, CelloSaaS.PartitionManagement.Model.DataServer> : null;%>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <% if (dataServerList != null && dataServerList.Count > 0)
               { %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="DataServerTableSearchText" name="DataServerTableSearchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                    </span>
                </div>
            </div>
            <% } %>
        </div>
    </div>
    <div class="grid-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataServerWarning")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>

        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataServerError")))
           { %>
        <div class="alert alert-danger">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataServerError")%>
        </div>
        <%} %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DataServerSuccess")))
           { %>
        <div class="alert alert-success">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <%=Html.CelloValidationMessage("DataServerSuccess")%>
        </div>
        <%} %>

        <%if (dataServerList != null && dataServerList.Count > 0 && dataServerList.Values != null && (dataServerList.Values).Count() > 0)
          {
              Html.Grid(dataServerList.Values).Columns(
            column =>
            {
                column.For(c => c.Name).Named(this.GetLocalResourceObject("DataServerName").ToString()).Attributes(style => "width:120px").DoNotEncode();
                column.For(c => c.ServerAddress).Named(this.GetLocalResourceObject("ServerAddress").ToString()).Attributes(style => "width:220px").DoNotEncode();
                column.For(c => c.DataBaseName).Named(this.GetLocalResourceObject("DataBaseName").ToString()).Attributes(style => "width:220px").DoNotEncode();
                column.For(c => "<a href=# onclick=EditDataServerDetailsForm('" + c.Id + "') alt=Edit title='" + this.GetLocalResourceObject("EditDataServerTitle") + "'><i class='fa fa-edit'></i></a>").Named(this.GetLocalResourceObject("EditDataServer").ToString()).Attributes(@class => "halign", style => "width:60px;")
                        .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
                column.For(c => "<a href=# onclick='DeleteDataServerDetailsForm(\"" + HttpUtility.HtmlEncode(c.Name) + "\",\"" + c.Id + "\")' alt=Delete title='" + this.GetLocalResourceObject("DeleteDataServerTitle") + "'><i class='fa fa-trash-o'></i></a>").Named(this.GetLocalResourceObject("DeleteDataServer").ToString()).Attributes(@class => "halign", style => "width:60px;")
                        .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
            }).Attributes(id => "dataList", @class => "celloTable").Render();

          } %>
        <%else
          { %>
        <div class="alert alert-info">
            <%:this.GetGlobalResourceObject("General", "NoData").ToString()%>
        </div>
        <%} %>
    </div>
</div>
