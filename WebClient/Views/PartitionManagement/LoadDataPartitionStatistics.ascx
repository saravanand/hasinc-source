﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.PartitionManagement.Model.DataPartition>" %>
<%@ Import Namespace="CelloSaaS.PartitionManagement.ServiceProxies" %>

<section class="panel indigo">
    <header class="panel-heading">
        <h4><%:this.GetLocalResourceObject("DataPartitionStatsHeading")%></h4>
    </header>
    <div class="panel-body">
        <% 
            var datapartitionList = new Dictionary<Guid, CelloSaaS.PartitionManagement.Model.DataPartition>();
            datapartitionList = ViewData["DataPartitionList"] != null ? ViewData["DataPartitionList"] as Dictionary<Guid, CelloSaaS.PartitionManagement.Model.DataPartition> : null;

            if (datapartitionList == null || datapartitionList.Count < 1)
            {
                datapartitionList = DataPartitionProxy.GetAllDataPartition();
            }
            var DataPartitionStatsList = ViewData["DataPartitionStats"] as Dictionary<Guid, List<Guid>>;

            var temp = new List<dynamic>();
            var totalDataPartition = DataPartitionStatsList != null && DataPartitionStatsList.Count > 0 ? DataPartitionStatsList.Values.Count : 0;

            if (datapartitionList != null && datapartitionList.Count > 0 && totalDataPartition > 0)
            {

                var dataPartitionDic = datapartitionList.ToDictionary(c => c.Key, c => c.Value.DataPartitionName);

                foreach (var x in DataPartitionStatsList)
                {
                    temp.Add(new
                    {
                        category = dataPartitionDic.ContainsKey(x.Key) ? dataPartitionDic[x.Key] : "-",
                        value = Math.Round((double)x.Value.Count / 100, 2)
                    });
                }
            }
            string chartData = Newtonsoft.Json.JsonConvert.SerializeObject(temp, Newtonsoft.Json.Formatting.None); 
        %>
        <% if (totalDataPartition > 0)
           { %>
        <div id="DataPartitionStatisticsChartDiv">
            <%:Html.Hidden("DataPartitionStatistics_chartData", chartData) %>
            <div class="clearfix"></div>
            <div id="DataPartition_chart_area"></div>
        </div>
        <% }
           else
           { %>
        <div class="alert alert-info"><%:this.GetLocalResourceObject("DataPartitionStatsInfo")%></div>
        <% } %>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        if ($('#DataPartitionStatistics_chartData').length > 0) {
            var Tenant_dist_data = JSON.parse($('#DataPartitionStatistics_chartData').val());

            function createDataPartitionStatisticsChart() {
                var options = {
                    title: {
                        text: '<%:this.GetLocalResourceObject("DataPartitionStatsMessage")%>',
                        visible: true
                    },
                    dataSource: { data: Tenant_dist_data },
                    legend: {
                        visible: true,
                        position: 'bottom'
                    },
                    seriesDefaults: {
                        type: 'pie',
                        stack: false,
                        labels: {
                            template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                            position: "outsideEnd",
                            visible: true,
                            background: "transparent",
                            align: "column"
                        },
                        tooltip: {
                            visible: true
                        }
                    },
                    series: [{
                        type: 'pie',
                        data: Tenant_dist_data
                    }],
                    tooltip: {
                        visible: true,
                        template: "#= category # - #= kendo.format('{0:P}', percentage) #"
                    }
                };

                $('#DataPartition_chart_area').celloChart(options);
            }

            setTimeout(function () {
                try {
                    createDataPartitionStatisticsChart();
                } catch (e) { console.log(e); }
            }, 100);
        }
    });
</script>
