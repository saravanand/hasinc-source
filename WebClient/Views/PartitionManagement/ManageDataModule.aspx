﻿<%@ Page Title="<%$ Resources:DataModuleTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>


<%@ Import Namespace="CelloSaaS.PartitionManagement.Model" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%: this.GetLocalResourceObject("DataModuleHeading")%></h3>        
    </div>
    <div class="row-fluid pd-25">
        <div id="statusMessage">
        </div>
        <div id="DataModuleList" class="grid-part">
            <% Html.RenderPartial("DataModuleDetailsList"); %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();

            $(document).on('keypress', '#DataModuleTableSearchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        }

        function DoSearch() {
            var searchText = $('#DataModuleTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error,div.alert").remove();
            return false;
        }



    </script>

    <script type="text/javascript">

        function LoadDataModuleDetailsList() {
            $.get("DataModuleDetailsList", '', function (resp) { $("#DataModuleList").html(resp); jQDataTable(); });
        }

        function DeleteDataModuleDeatilsForm(ID, name) {
            var confirma = confirm("<%=this.GetGlobalResourceObject("PartitionManagementResource","Confirm")%>");

            if (confirma) {
                $.get('DeleteDataModuleDetails?dataModuleId=' + ID + '&dataModuleName=' + name, '', DeleteDataModule_CalBack);
            }
            else {
                return false;
            }
        }

        function DeleteDataModule_CalBack(data) {
            $('#DataModuleList').html(data).show();
            jQDataTable();
        }
    </script>
</asp:Content>
