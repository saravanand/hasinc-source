﻿<%@ Page Title="<%$ Resources:DataPartitionTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.PartitionManagement.Model" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%:this.GetLocalResourceObject("DataPartitionHeading")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row">
            <div class="col-md-8">
                <div id="statusMessage">
                </div>
                <div id="DataPartitionsList" class="grid-part">
                    <% Html.RenderPartial("DataPartitionDetailsList"); %>
                </div>
            </div>
            <div class="col-md-4 pd-0">
                <div id="AddDataPartition">
                    <% Html.RenderPartial("AddDataPartitionDetails"); %>
                </div>
            </div>
        </div>
        <div id="DataPartitionStatsDiv">
            <% Html.RenderPartial("LoadDataPartitionStatistics"); %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/Content/Kendo/kendo.common.min.css" />
    <link rel="stylesheet" href="/Content/Kendo/kendo.default.min.css" />
    <link rel="stylesheet" href="/Content/Kendo/kendo.black.min.css" />
    <link rel="stylesheet" href="/Content/Kendo/kendo.dataviz.min.css" />
    <link rel="stylesheet" href="/Content/Kendo/kendo.dataviz.black.min.css" />
    <link rel="stylesheet" href="/Content/Kendo/kendo.dataviz.blueopal.min.css" />
    <link rel="stylesheet" href="/Content/Kendo/kendo.dataviz.metro.min.css" />
    <link rel="stylesheet" href="/Content/Kendo/kendo.dataviz.metroblack.min.css" />
    <script type="text/javascript" src="/Scripts/kendo.dataviz.min.js"></script>
    <script type="text/javascript" src="/Scripts/celloChart.js"></script>
    <script type="text/javascript" src="/Scripts/rgbcolor.js"></script>
    <script type="text/javascript" src="/Scripts/canvg.js"></script>
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();
            $(document).on('keypress', '#DataPartitionTableSearchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        }

        function DoSearch() {
            var searchText = $('#DataPartitionTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error,div.alert").remove();
            return false;
        }

    </script>


    <script type="text/javascript">

        function AddDataPartitionDetailsForm() {
            var dataList = $("#DataPartitionFormID").serialize();
            $.post('AddDataPartitionDetails', dataList, Add_PartitionSet_Callback);
        }

        function LoadDataPartitionList() {
            $.get("DataPartitionDetailsList", '', function (resp) { $("#DataPartitionsList").html(resp); jQDataTable(); });
        }

        function EditDataPartitionDetailsForm(ID) {
            $.get('EditDataPartitionDetails?dataPartitionId=' + ID, '', function (resp) {
                $("#AddDataPartition").html(resp);
                $('#statusMessage').fadeOut();
                $('.alert-success').remove();
            });
        }

        function LoadDataPartitionStatisticsDetails() {
            $.get("LoadDataPartitionStatistics", '', function (resp) { $("#DataPartitionStatsDiv").html(resp) });
        }
        
        function AddDataPartitions() {
            $.get('AddDataPartitionDetails', '', function (data) {
                $("#AddDataPartition").html(data).show();
            });
        }

        function MakeDefalutForm(id) {

            var confirma = confirm("<%: this.GetLocalResourceObject("DefaultConfirm")%>");

            if (confirma) {
                if ($("#" + id + "IsDefault").is(":checked")) {
                    $.get('SetDefalutDataPartition?dataPartitionId=' + id, '', function (resp) {
                        $("#DataPartitionsList").html(resp);
                        jQDataTable();
                    });
                }
            }
            else {
                return false;
            }
            $('#statusMessage').fadeOut();
            $('.alert-success').remove();
        }

        function Add_PartitionSet_Callback(data) {
            var name = $("#DataPartitionName").val();
            if (data == "AddSuccess") {
                AddDataPartitions();
                LoadDataPartitionList();
                LoadDataPartitionStatisticsDetails();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' + name + '<%=this.GetGlobalResourceObject("PartitionManagementResource","AddSuccessMessage") %> </div>').fadeIn();
            }
            else if (data == "UpdateSuccess") {
                AddDataPartitions();
                LoadDataPartitionList();
                LoadDataPartitionStatisticsDetails();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' + name + ' <%:this.GetGlobalResourceObject("PartitionManagementResource","UpdateSuccessMessage")%> </div>').fadeIn();
            }
            else {
                $("#AddDataPartition").html(data).fadeIn();
                $('#statusMessage').fadeOut();
                $('.alert-success').remove();
            }
    }
    </script>

</asp:Content>

