﻿<%@ Page Title="<%$ Resources:MappingTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>


<%@ Import Namespace="CelloSaaS.PartitionManagement.Model" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="ManageDataPartition" title='<%= this.GetLocalResourceObject("BackDataPartitionTitle")%>'>
            <i class="icon-custom-left"></i>
        </a>
        <h3><%= this.GetLocalResourceObject("MappingHeading")%>
            <span class="semi-bold"><%:ViewData["DataPartitionName"] %></span>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row">
            <div class="col-md-8">
                <div id="statusMessage">
                </div>
                <div id="DataPartitionMappingList" class="grid-part">
                    <% Html.RenderPartial("DataPartitionMappingDetailsList"); %>
                </div>
            </div>
            <div class="col-md-4 pd-0">
                <div id="AddDataPartitionMapping">
                    <% Html.RenderPartial("AddDataPartitionMappingDetails"); %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();
            $(document).on('keypress', '#DataPartitionMappingTableSearchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        }

        function DoSearch() {
            var searchText = $('#DataPartitionMappingTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error,div.alert").remove();
            return false;
        }

    </script>

    <script type="text/javascript">
        function AddDataPartitionMappingDetailsForm() {
            var valueofelement = $("#DataPartitionID").val();
            if (valueofelement == null || valueofelement == '00000000-0000-0000-0000-000000000000') {
                window.location.assign("/PartitionManagement/ManageDataPartition");
            }
            else {
                var dataList = $("#AddDataPartitionMappingFormID").serialize();
                $.post('AddDataPartitionMappingDetails', dataList, Add_DataPartitionMapping_Callback);
            }
        }

        function LoadDataPartitionMappingList() {
            var valueofelement = $("#DataPartitionID").val();
            if (valueofelement == null || valueofelement == '00000000-0000-0000-0000-000000000000') {
                window.location.assign("/PartitionManagement/ManageDataPartition");
            }
            else {
                $.get("DataPartitionMappingDetailsList?dataPartitionId=" + valueofelement, '', function (resp) { $("#DataPartitionMappingList").html(resp); jQDataTable(); });
            }
        }

        function EditDataPartitionMappingDetailsForm(ID) {
            var valueofelement = $("#DataPartitionID").val();
            if (valueofelement == null || valueofelement == '00000000-0000-0000-0000-000000000000') {
                window.location.assign("/PartitionManagement/ManageDataPartition");
            }
            else {
                $.get('EditDataPartitionMappingDetails?dataPartitionMappingId=' + ID, '', function (resp) {
                    $("#AddDataPartitionMapping").html(resp);
                    $('#statusMessage').fadeOut();
                    $('.alert-success').remove();
                });
            }
        }

        function DeleteDataPartitionMappingDetailsForm(ID, ID1) {

            if (ID1 == null || ID1 == '00000000-0000-0000-0000-000000000000') {
                window.location.assign("/PartitionManagement/ManageDataPartition");
            }
            else {

                var confirma = confirm("<%=this.GetGlobalResourceObject("PartitionManagementResource","Confirm")%>");
                if (confirma) {
                    $.get('DeleteDataPartitionMappingDetails?dataPartitionMappingId=' + ID + '&dataPartitionId=' + ID1, '', function (data) {
                        $("#DataPartitionMappingList").html(data).show();
                        jQDataTable();
                        AddDataPartitionMappings();
                    });
                }
                else {
                    return false;
                }
                $('#statusMessage').fadeOut();
            }
        }

        function Add_DataPartitionMapping_Callback(data) {
            if (data == "AddSuccess") {
                AddDataPartitionMappings();
                LoadDataPartitionMappingList();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a><%: this.GetLocalResourceObject("MappingAddSuccess") %></div>').fadeIn();
            }
            else if (data == "UpdateSuccess") {
                AddDataPartitionMappings();
                LoadDataPartitionMappingList();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a><%: this.GetLocalResourceObject("MappingUpdateSuccess") %> </div>').fadeIn();
            }
            else {
                $("#AddDataPartitionMapping").html(data).fadeIn();
                LoadDataPartitionMappingList();
            }
    }

    function AddDataPartitionMappings() {
        var valueofelement = $("#DataPartitionID").val();
        if (valueofelement == null || valueofelement == '00000000-0000-0000-0000-000000000000') {
            window.location.assign("/PartitionManagement/ManageDataPartition");
        }
        else {
            $.get('AddDataPartitionMappingDetails?dataPartitionId=' + valueofelement, '', function (data) {
                $("#AddDataPartitionMapping").html(data).show();
            });
        }
    }
    </script>
</asp:Content>

