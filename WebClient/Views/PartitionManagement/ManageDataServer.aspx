﻿<%@ Page Title="<%$ Resources:DataServerTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>


<%@ Import Namespace="CelloSaaS.PartitionManagement.Model" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3><%:this.GetLocalResourceObject("DataServerHeading") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row">
            <div class="col-md-8">
                <div id="statusMessage">
                </div>
                <div id="DataServerList" class="grid-part">
                    <% Html.RenderPartial("DataServerDetailsList"); %>
                </div>
            </div>
            <div class="col-md-4 pd-0">
                <div id="AddDataServer">
                    <% Html.RenderPartial("AddDataServerDetails"); %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();
            $(document).on('keypress', '#DataServerTableSearchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        }

        function DoSearch() {
            var searchText = $('#DataServerTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error,div.alert").remove();
            return false;
        }

    </script>

    <script type="text/javascript">

        function AddDataServerDetailsForm() {
            var dataList = $("#DataServerFormID").serialize();
            $.post('AddDataServerDetails', dataList, Add_DataServer_CallBack);
        }

        function LoadDataServerDetailsList() {
            $.get("DataServerDetailsList", '', function (resp) { $("#DataServerList").html(resp); jQDataTable(); });
        }

        function EditDataServerDetailsForm(ID) {
            $.get('EditDataServerDetails?dataServerId=' + ID, '', function (resp) {
                $("#AddDataServer").html(resp);
                $('#statusMessage').fadeOut();
                $('.success').remove();
            });
        }

        function DeleteDataServerDetailsForm(name, ID) {
            var confirma = confirm("<%=this.GetGlobalResourceObject("PartitionManagementResource","Confirm")%>");

            if (confirma) {
                $.get('DeleteDataServerDetails?dataServerId=' + ID + '&dataServerName=' + name, '', DeleteDataServer_CalBack);
            }
            else {
                return false;
            }
            $('#statusMessage').fadeOut();


        }

        function Add_DataServer_CallBack(data) {
            var name = $("#Name").val();
            if (data == "AddSuccess") {
                AddDataServers();
                LoadDataServerDetailsList();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' + name + ' <%=this.GetGlobalResourceObject("PartitionManagementResource","AddSuccessMessage") %> </div>').fadeIn();
            }
            else if (data == "UpdateSuccess") {
                AddDataServers();
                LoadDataServerDetailsList();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' + name + ' <%:this.GetGlobalResourceObject("PartitionManagementResource","UpdateSuccessMessage")%> </div>').fadeIn();
            }

            else {
                $("#AddDataServer").html(data).fadeIn();
                $('#statusMessage').fadeOut();
                $('.alert-success').remove();
            }
    }

    function AddDataServers() {
        $.get('AddDataServerDetails', '', function (data) {
            $("#AddDataServer").html(data).show();

        });
    }

    function DeleteDataServer_CalBack(data) {
        $('#DataServerList').html(data).show();
        jQDataTable();
        AddDataServers();
    }

    </script>
</asp:Content>


