﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<%
    var dynVars = ViewData["DynamicVariables"] as Dictionary<string, string>;
%>
<% if (dynVars == null || dynVars.Count == 0)
   { %>
<div class="info">
    No field mappings available!
</div>
<% }
   else
   {
       QueryRelation queryRelationDetails = (ViewData["QueryRelationDetails"] as QueryRelation) ?? new QueryRelation();
       if (queryRelationDetails.VariableMappings == null)
       {
           queryRelationDetails.VariableMappings = new Dictionary<string, string>();
       }

       QueryDetails queryDetails = ViewData["ParentQueryDetails"] as QueryDetails;
       Dictionary<string, string> fieldList = new Dictionary<string, string>();

       if (queryDetails != null && queryDetails.QueryInfo != null && queryDetails.QueryInfo.SelectClauseFields != null)
       {
           var fields = queryDetails.QueryInfo.SelectClauseFields;
           fieldList = fields.ToDictionary(x => x.FieldInfo.EntityName + "." + x.FieldInfo.MappedFieldName, y => y.FieldInfo.DisplayName);
       }
%>
<h3>
    Variable Mappings</h3>
<%
       foreach (var dvar in dynVars)
       { 
%>
<div class="chkFieldDiv">
    <label>
        <%=dvar.Key%></label>
    <%
           List<SelectListItem> selectList = new List<SelectListItem>();
           selectList.Add(new SelectListItem { Text = "--Select--", Value = "" });
           selectList.AddRange(fieldList.Select(x => new SelectListItem { Text = x.Value, Value = x.Key, Selected = (queryRelationDetails.VariableMappings.ContainsKey(dvar.Key) && queryRelationDetails.VariableMappings[dvar.Key] == x.Key) }));

    %>
    <%=Html.DropDownList("QueryRelation.VariableMappings[" + dvar.Key + "]", selectList)%>
</div>
<%      
       }
   }
%>