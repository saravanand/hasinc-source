﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Collections.Generic.List<CelloSaaS.QueryBuilderLibrary.Model.QueryDetails>>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="grid simple horizontal red">
    <div class="grid-title">
        <div class="row">
            <div class="col-md-6">
                <h4>Global Report Queries</h4>
            </div>
            <%  
                if (this.Model != null && this.Model.Count > 0)
                {
            %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="globsearchText" name="globsearchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoGlobalQuerySearch();">Go!</button>
                    </span>
                </div>
            </div>
            <% } %>
        </div>
    </div>
    <div class="grid-body">
        <%  
            if (this.Model == null || this.Model.Count < 1)
            {
        %>
        <div class="alert alert-info">
            No Global Queries Found
        </div>
        <%
            }
            else
            {
        %>
        <%
                bool canEditQuery = false, canDeleteQuery = false, canViewQuery = false;

                if (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin) ||
                    CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.QueryBuilderLibrary.ServiceContracts.QueryBuilderPrivilegeConstants.ViewQuery))
                {
                    canViewQuery = true;
                }

                if (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
                {
                    canDeleteQuery = canEditQuery = true;
                }

                if (CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.QueryBuilderLibrary.ServiceContracts.QueryBuilderPrivilegeConstants.ExecuteQuery))
                {
                    canViewQuery = true;
                }

                Html.Grid(this.Model).Columns(column =>
                {
                    column.For(c => c.Name).Named("Query Name");
                    column.For(c => c.Description).Named("Query Description");
                    if (!CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
                    {
                        column.For(c => "<a href='" + Url.Action("CopyGlobalQuery", "QueryBuilder") + "?queryId=" + c.Identifier + "'><i class='fa fa-copy'></i></a>").Named("Copy").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                    }
                    if (canEditQuery)
                    {
                        column.For(c => "<a href='" + Url.Action("ManageQuery", "QueryBuilder") + "?queryId=" + c.Identifier + "&QueryType=" + c.QueryType.ToString("F") + "'><i class='fa fa-pencil'></i></a>").Named("Edit").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                    }
                    if (canViewQuery)
                    {
                        column.For(c => "<a title='Execute and preview this query output!' href='" + Url.Action("PreviewQueryResult", "QueryBuilder", new { queryId = c.Identifier, renderMode = c.RenderMode }) + "' \"><i class='fa fa-eye'></i></a>").Named("Preview").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign");
                    }
                    if (canDeleteQuery)
                    {
                        column.For(c => "<a href='" + Url.Action("DeleteQuery", "QueryBuilder") + "?queryId=" + c.Identifier + "'><i class='fa fa-trash-o'></i></a>").Named("Delete").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                    }

                }).Attributes(id => "globalQueryList", @class => "celloTable").Render();
        %>
        <% } %>
    </div>
</div>
