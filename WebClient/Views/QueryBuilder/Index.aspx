﻿<%@ Page Title="Query Builder" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div id="divGlobalQueriesList">
        <div class="page-title">
            <h3>Query Builder</h3>
            <div class="pull-right">
                <%
                    if (!CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin)
                        && CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.QueryBuilderLibrary.QueryBuilderPrivilegeConstants.AddQuery))
                    {
                %>
                <a class="btn btn-default" href="<%=Url.Action("CopyAllQueriesFromParent") %>" title="Copy Parent Queries">Copy Parent Queries</a>
                <%
                    }
                    /* normal query */
                    if (CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.QueryBuilderLibrary.QueryBuilderPrivilegeConstants.AddQuery))
                    {
                %>
                <a class="btn btn-success" id="addQuery" href="<%=Url.Action("ManageQuery","QueryBuilder", new { QueryType = CelloSaaS.QueryBuilderLibrary.Model.QueryTypes.ReportQuery }) %>"
                    title="Add New Query"><i class="fa fa-plus"></i>&nbsp;New Query</a>
                <% }
                    /* datascope query */
                    if (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
                    {
                %>
                <!--<div class="green_but">
                    <a id="a1" href="<%=Url.Action("ManageQuery","QueryBuilder", new { QueryType = CelloSaaS.QueryBuilderLibrary.Model.QueryTypes.DatascopeQuery }) %>"
                        title="Add New Query"><span><i class="fa fa-plus"></i>&nbsp;New Datascope</span></a>
                </div>-->
                <%
                    }
                %>
            </div>
        </div>
        <div class="row-fluid pd-25">
            <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Success")))
               { %>
            <div class="alert alert-success">
                <%=Html.CelloValidationMessage("Success")%>
            </div>
            <% }
               else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
               { %>
            <div class="alert alert-danger">
                <%=Html.CelloValidationSummary("Please correct the following errors and try again")%>
            </div>
            <% } %>
            <% Html.RenderAction("GlobalQueries", "QueryBuilder");%>
            <div id="divTenantCreatedQueries">
                <%
                    var queryList = (List<CelloSaaS.QueryBuilderLibrary.Model.QueryDetails>)ViewData["QueryList"];
                %>
                <% Html.RenderPartial("QueriesList", queryList); %>
            </div>
        </div>
    </div>
    <div class="dynamicVariables" style="display: none">
        <% ViewData["BuiltInHandler"] = true;%>
        <% Html.RenderPartial("ManageDynamicVariables"); %>
    </div>
    <div id="queryCopyDialog" style="display: none;" title="Save Query As">
        <div class="form-container">
            <div class="form-group">
                <label>
                    Name:</label>
                <input type="hidden" name="oldQueryId" />
                <input type="text" name="newQueryName" />
            </div>
            <div class="form-group">
                <label>
                    Description:</label>
                <textarea rows="3" cols="10" name="newQueryDescription"></textarea>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" href="/Content/QueryBuilderStyles.css" />
    <!-- SCRIPT Sections -->
    <script type="text/javascript">
        // Constants
        var queryCopyURL = '<%: Url.Action("CopyAllQueriesFromParent","QueryBuilder") %>';
        var busyURL = '<%=Url.Content("~/App_Themes/CelloSkin/loading.gif") %>';
        var manageQueryURL = '<%=Url.Action("ManageQuery") %>';
        var deleteQueryURL = '<%=Url.Action("DeleteQuery") %>';
        var dynamicVariablesURL = '<%: Url.Action("GetDynamicVariables") %>';
        var savedQueryExecutionURL = '<%=Url.Action("ExecuteQueryById") %>';
    </script>
    <script type="text/javascript" src="/Scripts/querybuilder.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#localQueryList a.queryCopy').on('click', null, function () {
                var queryId = $(this).data('queryid');
                $("#queryCopyDialog input[name='oldQueryId']").val(queryId);
                $('#queryCopyDialog').dialog('open');
                $("#queryCopyDialog input[name='newQueryName']").focus();
            });

            $('#queryCopyDialog').dialog({
                width: 400,
                height: 320,
                autoOpen: false,
                modal: true,
                show: {
                    effect: "slide",
                    direction: "up",
                    duration: 300
                },
                hide: {
                    effect: "slide",
                    direction: "down",
                    duration: 200
                },
                buttons: {
                    "Submit": function () {
                        var queryId = $("#queryCopyDialog input[name='oldQueryId']").val();
                        var queryName = $("#queryCopyDialog input[name='newQueryName']").val();
                        var queryDescription = $("#queryCopyDialog textarea[name='newQueryDescription']").val();

                        if (queryName == '' || queryName.trim() == '') {
                            alert('Enter a valid query name!');
                            $("#queryCopyDialog input[name='newQueryName']").val('');
                            $("#queryCopyDialog input[name='newQueryName']").focus();
                            return false;
                        }

                        var postdata = JSON.stringify({ queryId: queryId, queryName: queryName, queryDescription: queryDescription });

                        $.ajax
                        ({
                            type: "POST",
                            url: '/QueryBuilder/CopyQuery',
                            contentType: 'application/json; charset=utf-8',
                            data: postdata,
                            success: function (data) {
                                $('#queryCopyDialog').dialog("close");
                                $("#queryCopyDialog input,#queryCopyDialog textarea").val('');

                                if (data) {
                                    alert(data.Success || data.Error);
                                    window.location.reload();
                                } else {
                                    alert('Error while copying query!');
                                }
                            }
                        });
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        });
    </script>
</asp:Content>
