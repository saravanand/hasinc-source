﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div>
    <table class="celloTable details_container_table">
        <thead>
            <tr>
                <th>
                    Dynamic Variable
                </th>
                <th>
                    Value
                </th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="pull-right" style="padding-top: 10px;">
            <div class="btn btn-default">
                <%
                    if (ViewData["BuiltInHandler"] != null && bool.Parse(ViewData["BuiltInHandler"].ToString()))
                    {
                %>
                <a class="updateDynamicVariableValues" title="Update Dynamic Variable Values" onclick="UpdateDynamicVariableValues()">
                    <span>Apply</span></a>
                <%
                }
                else
                {
                %>
                <a class="updateDynamicVariableValues" title="Update Dynamic Variable Values"><span>
                    Apply</span></a>
                <%
                }
                %>
            </div>
        </div>
</div>
