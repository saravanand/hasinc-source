﻿<%@ Page Title="Manage Query" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" ValidateRequest="false" %>

<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <% 
        CurrentFormMode currentMode = ViewData["FormMode"] != null ? (CurrentFormMode)ViewData["FormMode"] : CurrentFormMode.Insert;
        QueryTypes queryType = ViewData["QueryType"] != null ? (QueryTypes)ViewData["QueryType"] : QueryTypes.ReportQuery;

        using (Html.BeginForm("ManageQuery", "QueryBuilder", FormMethod.Post, new { id = "saveForm" }))
        {
    %>
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="Back to Query List">
            <i class="icon-custom-left"></i>
        </a>
        <%if (currentMode == CurrentFormMode.Insert)
          {
        %>
        <h3>New
            <%: queryType == QueryTypes.ReportQuery ? "Query" : "Datascope Query" %>
        </h3>
        <%
          }
          else if (currentMode == CurrentFormMode.Edit)
          {
        %>
        <h3>Update
            <%: ViewData["queryName"] %></h3>
        <% 
          }
        %>
        <div class="pull-right">
            <%if (UserIdentity.HasPrivilege(QueryBuilderPrivilegeConstants.AddQuery)
              || UserIdentity.HasPrivilege(QueryBuilderPrivilegeConstants.EditQuery))
              { %>
            <a class="btn btn-success" onclick="javascript:$('#saveForm').submit();" title="Save Query"><span>Save</span>
            </a>
            <% } %>
            <%if (UserIdentity.HasPrivilege(QueryBuilderPrivilegeConstants.ExecuteQuery))
              { %>
            <a class="btn btn-info" id="btnExecuteQuery1" title="Execute Query & Show Results!"><span>Execute</span>
            </a>
            <%} %>
        </div>
    </div>
    <div class="row pd-25">
        <div id="reportContainer">
            <section class="panel">
                <header class="panel-heading bg-info">
                    <ul class="nav nav-tabs nav-justified captilize" id="qbTabs">
                        <li class="active"><a id="designTabLink" data-toggle="tab" href="#queryGeneratorDiv">Design</a></li>
                        <li><a data-toggle="tab" id="previewTabLink" href="#executeQueryDiv">Preview</a></li>
                    </ul>
                </header>
                <div class="panel-body pd-0">
                    <div class="tab-content">
                        <div id="queryGeneratorDiv" class="tab-pane fade in active">
                            <div class="row">
                                <%Html.RenderPartial("QueryGenerator"); %>
                                <div class="pull-right">
                                    <%if (UserIdentity.HasPrivilege(QueryBuilderPrivilegeConstants.AddQuery) || UserIdentity.HasPrivilege(QueryBuilderPrivilegeConstants.EditQuery))
                                      { %>
                                    <a class="btn btn-success" onclick="javascript:$('#saveForm').submit();" title="Save Query">Save
                                    </a>
                                    <% } %>
                                    <%if (UserIdentity.HasPrivilege(QueryBuilderPrivilegeConstants.ExecuteQuery))
                                      { %>
                                    <a class="btn btn-info" id="btnExecuteQuery" title="Execute Query & Show Results!">Execute
                                    </a>
                                    <%} %>
                                </div>
                            </div>
                        </div>
                        <div id="executeQueryDiv" class="tab-pane fade">
                            <div class="alert alert-info">
                                Please Click "Execute" button to view the query result here!
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <% } %>
    <form action="<%=Url.Action("Export") %>" id="exportForm" method="post">
        <input type="hidden" id="JsonData" name="JsonData" value="" />
    </form>

    <!-- Modal -->
    <div class="modal fade" id="previewExpressionDetails" tabindex="-1" role="dialog" aria-labelledby="previewExpressionDetails" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Expression Builder</h4>
                </div>
                <div class="modal-body" style="overflow: auto;">
                    <div class="exprcolumns">
                        <div style="padding-top: 15px">
                            <%
                                if (CelloSaaS.Library.UserIdentity.Roles.Contains(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.TenantAdmin))
                                {
                            %>
                            <div style="clear: both; padding: 5px">
                                <input type="radio" name="expressionOption" value="build" checked="checked" onclick="disableFreeFlowExpression()" />Build
                Expression
                <input type="radio" name="expressionOption" value="freeflow" onclick="enableFreeFlowExpression()" />FreeFlow
                Expression
                            </div>
                            <%
                                }
                            %>
                        </div>
                        <table class="celloTable details_container_table">
                            <thead>
                                <tr>
                                    <th colspan="2">Manage Expression Columns
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <label for="expressionName">
                                            Expression Name <span style="color: Red">*</span> [Max 120 Chars]
                                        </label>
                                        <input id="expressionName" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Function
                                    </td>
                                    <td>
                                        <select name='exprSandbox' class="fns" id="baseFunctions" onchange="onFunctionChanged(this)">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="exprSandbox" id="exprSandbox">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding: 10px">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="ManageExprCols btn btn-success" onclick="SendData()" title="Add New Expression Column"><span>
                        <i class="fa fa-plus"></i>&nbsp;Add</span></a>
                    <button type="button" id="ExpressionDetailsCancel" class="btn btn-default" data-dismiss="modal"><%:HttpContext.GetGlobalResourceObject("General", "Close")%></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="previewDynamicvarDetails" tabindex="-1" role="dialog" aria-labelledby="previewDynamicvarDetails" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel1">Manage Dynamic Variables"</h4>
                </div>
                <div class="modal-body" style="overflow: auto;">
                    <div class="dynamicVariables">
                        <% Html.RenderPartial("ManageDynamicVariables"); %>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="DynamicvarDetailsCancel" class="btn btn-default" data-dismiss="modal"><%:HttpContext.GetGlobalResourceObject("General", "Close")%></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

     <%
        var queryId = ViewData["queryId"] as string;
        if (!string.IsNullOrEmpty(queryId))
        {
    %>
    <div class="modal fade" id="saveAsChartModal-<%=queryId %>" title="Save as Chart" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">Save as Chart</h4>
                </div>
                <div class="modal-body">
                    <div class="statusMessage">
                    </div>
                    <div class="form-container">
                        <div class="form-group">
                            <label class="mandatory">Chart Name</label>
                            <input type="text" name="chartName" value="" />
                            <input type="hidden" name="queryId" value="<%=queryId %>" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button id="btnSaveChart-<%=queryId %>" type="button" class="btn btn-success"><%: this.GetGlobalResourceObject("General","Save") %> </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Close") %> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('#saveAsChartModal-<%=queryId %>').modal({ show: false });

            $(document).on('click', '#btnSave-<%=queryId %>', function () {
                $('#saveAsChartModal-<%=queryId %>').modal("show");
            });
        });
    </script>
    <% } %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/Content/QueryBuilderStyles.css" />
    <link rel="stylesheet" href="/Content/jquery.autocomplete.css" />

    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>

    <script type="text/javascript" src="/Scripts/jquery.treeview.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.tablednd.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.autocomplete.js"></script>
    <script type="text/javascript" src="/Scripts/ExpressionModels.js"></script>
    <script type="text/javascript" src="/Scripts/expressionbuilder.js"></script>
    <script type="text/javascript" src="/Scripts/managequery.js"></script>
    <script type="text/javascript">
        var qb = null;
        $(function () {

            $('#loading').hide();

            $('#loading').ajaxStart(function () {
                $(this).show();
            }).ajaxStop(function () {
                $(this).hide();
            });

            $('#moduleDDL').select2();
            $('#entityDDL').select2();
            $('#renderMode').select2();
            $('#conditionField').select2();
            $('#conditionOperator').select2();
            $('#AggregateFunction').select2();

            $("#selectedFieldsTable tbody").on('mouseenter', 'tr', function () {
                $(this.cells[0]).append('<i class="fa fa-sort"></i>');
                $(this.cells[0]).addClass('showDragHandle');
            });

            $("#selectedFieldsTable tbody").on('mouseleave', 'tr', function () {
                $(this.cells[0]).empty();
                $(this.cells[0]).removeClass('showDragHandle');
            });

            qb = celloQueryBuilder({
                formMode: '<%: ViewData["FormMode"] %>',
                entityLoadURL: '<%=Url.Action("GetEntitiesForModuleId") %>',
                fieldsLoadURL: '<%=Url.Action("GetFieldInfoForEntityId") %>',
                relatedEntitiesLoadURL: '<%=Url.Action("GetRelatedEntitiesForEntityId") %>',
                executeQueryURL: '<%=Url.Action("ExecuteQuery") %>',
                fetchQueryDetailsURL: '<%=Url.Action("GetQueryInfo") %>',
                saveQueryURL: '<%=Url.Action("SaveQuery") %>'
            });

            qb.init();
        });
    </script>
    <script type="text/javascript">
        $(function () {
            function _export(type, queryId) {
                if (!type || !queryId) {
                    alert("Please save the query before download!");
                    return;
                }

                if (window['qb'] != undefined) {
                    qb._export(type, queryId);
                } else {
                    var jsonData = { queryList: queryId, exportType: type, dynamicVariables: window.currentDynamicVariables };

                    $('#exportForm #JsonData').val(JSON.stringify(jsonData));
                    $('#exportForm').submit();
                }
            }

            $(document).on('click', 'a.btnExportExcel', null, function (e) {
                e.preventDefault();
                var queryId = $(this).data('queryid');
                _export('Excel', queryId);
            });

            $(document).on('click', 'a.btnExportPDF', null, function (e) {
                e.preventDefault();
                var queryId = $(this).data('queryid');
                _export('PDF', queryId);
            });
        });
    </script>
</asp:Content>
