﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.QueryBuilderLibrary.Model.QueryRelation>" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4>Manage Query Relation</h4>
    </header>
    <div class="panel-body">
        <%
            var lstQueries = ViewData["RelatedQueries"] != null ? (List<QueryDetails>)ViewData["RelatedQueries"] : new List<QueryDetails>();
            var selectListQueries = lstQueries.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Identifier,
                Selected = Model.RelatedQueryId == x.Identifier
            }).ToList();
            selectListQueries.Insert(0, new SelectListItem { Text = "--Select a query--", Value = "" });
        %>
        <% if (Html.ValidationSummary() != null)
           {%>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary("Please correct the following errors and submit again:") %>
        </div>
        <% } %>
        <div class="form-container">
            <form name="frmQueryRelation" id="frmQueryRelation" action="<%=Url.Action("ManageQueryRelation") %>">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mandatory" for="QueryRelation.Name">
                                Name</label>
                            <%=Html.Hidden("QueryRelation.Id", Model.Id) %>
                            <%=Html.TextBox("QueryRelation.Name", Model.Name)%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="QueryRelation.RelatedQueryId">
                                Query *</label>
                            <%=Html.DropDownList("QueryRelation.RelatedQueryId", selectListQueries, new { style="width:100%" })%>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="QueryRelation.Description">
                                Description</label>
                            <%=Html.TextArea("QueryRelation.Description", Model.Description)%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                Relation Type</label>
                            <% if ((bool)ViewData["CanAddChildQuery"])
                               { %>
                            <%=Html.DropDownList("QueryRelation.RelationType", typeof(QueryRelationType).ToSelectList(Model.RelationType), new { style="width:100%" })%>
                            <% }
                               else
                               { %>
                            <%=Html.Hidden("QueryRelation.RelationType", QueryRelationType.Related.ToString())%>
                            <%=QueryRelationType.Related.ToString()%>
                            <% } %>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                Render Mode</label>
                            <%=Html.DropDownList("QueryRelation.RenderMode", typeof(QueryRenderMode).ToSelectList(Model.RenderMode), new { style="width:100%" })%>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                Render Area</label>
                            <%=Html.DropDownList("QueryRelation.RenderArea", typeof(QueryRenderArea).ToSelectList(Model.RenderArea), new { style="width:100%" })%>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div id="variableMappingsDiv">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="pull-right">
                                <a class="btn btn-default" href="#" id="btnQueryRelationCancel" title="Cancel">Cancel</a>
                                <a class="btn btn-info" href="#" id="btnQueryRelationSave" title="Save">Save</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('#frmQueryRelation select').select2();
    });
</script>