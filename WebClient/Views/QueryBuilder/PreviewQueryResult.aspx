﻿<%@ Page Title="Query Result" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<System.Data.DataTable>" %>

<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <%
        QueryDetails queryDetails = ViewData["queryDetails"] == null ? new QueryDetails() : ViewData["queryDetails"] as QueryDetails;
    %>
    <%=Html.Hidden("queryId", queryDetails.Identifier) %>
    <%=Html.Hidden("queryName", queryDetails.Name) %>
    <%=Html.Hidden("queryDescription", queryDetails.Description) %>
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="Back to Query List">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%= queryDetails.Name%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("QueryResult", this.Model); %>
    </div>
    <form action="<%=Url.Action("ExportSavedQuery") %>" id="exportForm" method="post">
        <input type="hidden" id="JsonData" name="JsonData" value="" />
    </form>
    <%
        var queryId = queryDetails.Identifier;
        if (!string.IsNullOrEmpty(queryId))
        {
    %>
    <div class="modal fade" id="saveAsChartModal-<%=queryId %>" title="Save as Chart" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">Save as Chart</h4>
                </div>
                <div class="modal-body">
                    <div class="statusMessage">
                    </div>
                    <div class="form-container">
                        <div class="form-group">
                            <label class="mandatory">Chart Name</label>
                            <input type="text" name="chartName" value="" />
                            <input type="hidden" name="queryId" value="<%=queryId %>" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button id="btnSaveChart-<%=queryId %>" type="button" class="btn btn-success"><%: this.GetGlobalResourceObject("General","Save") %> </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Close") %> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('#saveAsChartModal-<%=queryId %>').modal({ show: false });

            $(document).on('click', '#btnSave-<%=queryId %>', function () {
                $('#saveAsChartModal-<%=queryId %>').modal("show");
            });
        });
    </script>
    <% } %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" href="/Content/QueryBuilderStyles.css" />

    <link href="<%: Url.Content("~/bundles/kendostyles") %>" type="text/css" rel="stylesheet" />
    <script src="<%: Url.Content("~/bundles/kendoscripts") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/bundles/viz") %>" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            function _export(type, queryId) {
                if (!type || !queryId) {
                    return;
                }

                if (window['qb'] != undefined) {
                    qb._export(type, queryId);
                } else {
                    var jsonData = { queryList: queryId, exportType: type, dynamicVariables: window.currentDynamicVariables };

                    $('#exportForm #JsonData').val(JSON.stringify(jsonData));
                    $('#exportForm').submit();
                }
            }

            $(document).on('click', 'a.btnExportExcel', null, function (e) {
                e.preventDefault();
                var queryId = $(this).data('queryid');
                _export('Excel', queryId);
            });

            $(document).on('click', 'a.btnExportPDF', null, function (e) {
                e.preventDefault();
                var queryId = $(this).data('queryid');
                _export('PDF', queryId);
            });
        });
    </script>
</asp:Content>
