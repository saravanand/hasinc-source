﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div class="chart-wrapper">
    <div id="chart-<%:ViewData["queryId"] %>">
    </div>
</div>
<script type="text/javascript">
    $(function () {
        function createChart(queryId) {
            var data = JSON.parse($('input#chartData').val());
            if (data) {
                var options = getOptions(queryId, data);
                $('#chart-' + queryId).celloChart(options);
            }
        }

        setTimeout(function () {
            createChart('<%:ViewData["queryId"] %>');
        }, 100);
    });
</script>
