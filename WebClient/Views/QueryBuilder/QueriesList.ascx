﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Collections.Generic.List<CelloSaaS.QueryBuilderLibrary.Model.QueryDetails>>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="grid simple horizontal purple">                
    <div class="grid-title">
        <div class="row">
            <div class="col-md-6">
                <h4>Report Queries</h4>
            </div>
            <%  
                if (this.Model != null && this.Model.Count > 0)
                {
            %>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="locsearchText" name="locsearchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoLocalQuerySearch();">Go!</button>
                    </span>
                </div>
            </div>
            <% } %>
        </div>
    </div>
    <div class="grid-body">
        <%
            if (this.Model == null || this.Model.Count < 1)
            {
        %>
        <div class="alert alert-info">
            No Report Queries Found
        </div>
        <%
            }
            else
            {
                bool canEditQuery = false, canDeleteQuery = false, canViewQuery = false;

                if (CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.QueryBuilderLibrary.ServiceContracts.QueryBuilderPrivilegeConstants.EditQuery))
                {
                    canEditQuery = true;
                }

                if (CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.QueryBuilderLibrary.ServiceContracts.QueryBuilderPrivilegeConstants.DeleteQuery))
                {
                    canDeleteQuery = true;
                }

                if (CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.QueryBuilderLibrary.ServiceContracts.QueryBuilderPrivilegeConstants.ExecuteQuery))
                {
                    canViewQuery = true;
                }
                                
                Html.Grid(this.Model).Columns(column =>
                {
                    column.For(c => c.Name).Named("Name");
                    column.For(c => c.Description).Named("Description");
                    if (canEditQuery)
                    {
                        column.For(c => "<a title='Edit this query!' href='" + Url.Action("ManageQuery", "QueryBuilder") + "?queryId=" + c.Identifier + "&QueryType=" + c.QueryType.ToString("F") + "'><i class='fa fa-edit'></i></a>").Named("Edit").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign");
                        column.For(c => "<a title='Manage query relationships!' href='" + Url.Action("QueryRelations", "QueryBuilder") + "?parentQueryId=" + c.Identifier + "&QueryType=" + c.QueryType.ToString("F") + "'><i class='fa fa-wrench'></i></a>").Named("Manage Relations").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign");
                    }
                    if (canViewQuery)
                    {
                        column.For(c => "<a title='Execute and preview this query output!' href='" + Url.Action("PreviewQueryResult", "QueryBuilder", new { queryId = c.Identifier, renderMode = c.RenderMode }) + "' \"><i class='fa fa-eye'></i></a>").Named("Preview").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign");
                    }
                    if (canEditQuery)
                    {
                        column.For(c => "<a title='Copy this query!' href='#' class='queryCopy' data-queryid='" + c.Identifier + "'><i class='fa fa-copy'></i></a>").Named("Copy").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign");
                    }
                    if (canDeleteQuery)
                    {
                        column.For(c => "<a title='Delete this query!' href='" + Url.Action("DeleteQuery", "QueryBuilder") + "?queryId=" + c.Identifier + "'><i class='fa fa-trash-o'></i></a>").Named("Delete").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "noSortCol halign");
                    }
                }).Attributes(id => "localQueryList", @class => "celloTable").Render();
            } 
        %>
    </div>
</div>
