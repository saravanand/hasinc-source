﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Success")))
   { %>
<div class="alert alert-success">
    <%=Html.ValidationMessage("Success")%>
</div>
<% }
   else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
   { %>
<div class="alert alert-danger">
    <%=Html.CelloValidationSummary("Please correct the following errors and submit the form again:")%>
</div>
<% } %>
<%: Html.Hidden("IsProductAdmin",CelloSaaS.Library.UserIdentity.Roles.Contains(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.TenantAdmin)) %>
<%: Html.Hidden("QueryType", ViewData["QueryType"] != null ? ViewData["QueryType"].ToString() : "")%>
<%
    var queryType = ViewData["QueryType"] != null ? (QueryTypes)Enum.Parse(typeof(QueryTypes), ViewData["QueryType"].ToString()) : QueryTypes.ReportQuery;
%>
<div class="col-md-3 pd-0">
    <section class="panel purple">
        <header class="panel-heading">
            <h4>Query Details</h4>
        </header>
        <div class="panel-body">
            <div class="form-container">
                <div class="form-group">
                    <label class="mandatory" for="queryName">
                        Name</label>
                    <input type="hidden" name="queryId" id="queryId" value="<%:ViewData["queryId"] %>" />
                    <input type="hidden" name="primaryEntityId" id="primaryEntityId" value="<%:ViewData["primaryEntityId"] %>" />
                    <input type="text" name="queryName" id="queryName" value="<%:ViewData["queryName"] %>" />
                </div>
                <div class="form-group">
                    <label for="queryDescription">
                        Description</label>
                    <%=Html.TextArea("queryDescription",
                                            ViewData["queryDescription"] == null ? string.Empty : ViewData["queryDescription"].ToString(), 
                                            2, 25, new { style = "" })%>
                </div>
                <div class="form-group">
                    <label for="renderMode">
                        Render Mode</label>
                    <%=Html.DropDownList("renderMode", typeof(QueryRenderMode).ToSelectList(ViewData["renderMode"]), new { style = "width:100%;" }) %>
                </div>
                <div class="form-group">
                    <label class="mandatory" for="moduleDDL">
                        Modules
                    </label>
                    <%if (ViewData["ModuleList"] != null)
                      { %>
                    <%= Html.DropDownList("moduleDDL", (IEnumerable<SelectListItem>)ViewData["ModuleList"], new { id = "moduleDDL", style = "width:100%;" })%>
                    <%} %>
                </div>
                <div class="form-group">
                    <label class="mandatory" for="entityDDL">
                        Primary Entity
                    </label>
                    <select name="entityDDL" id="entityDDL" style="width: 100%;">
                        <option value="-1">--Select a entity--</option>
                    </select>
                </div>
                <%
                    if (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
                    {
                %>
                <div class="form-group">
                    <%
                        // for datascope queries
                        if (queryType == QueryTypes.DatascopeQuery)
                        {
                    %>
                    <%: Html.Label("Is Global")%>
                    <%:Html.Label("No") %>
                    <input type="hidden" class="globalQuery" value="false" />
                    <%
                        }
                        else
                        {                            
                    %>
                    <%: Html.Label("Is Global")%>&nbsp;&nbsp;
                        <%
                            if (ViewData["queryId"] != null && !string.IsNullOrEmpty(ViewData["queryId"].ToString()))
                            {
                                if (ViewData["IsGlobalQuery"] != null && bool.Parse(ViewData["IsGlobalQuery"].ToString()))
                                {
                        %>
                    <input type="checkbox" class="globalQuery" disabled="disabled" checked="checked" style="vertical-align: middle;" />
                    <input type="hidden" class="globalQuery" value="true" />
                    <%
                                }
                                else
                                {
                    %>
                    <input type="checkbox" class="globalQuery" disabled="disabled" style="vertical-align: middle;" />
                    <input type="hidden" class="globalQuery" value="false" />
                    <%
                                }
                            }
                            else
                            {
                    %>
                    <input type="checkbox" class="globalQuery" style="vertical-align: middle;" />
                    <%
                            }
                    %>
                    <% } %>
                </div>
                <div class="form-group">
                    <label class="mandatory" for="primaryEntityAlias">
                        Primary Entity Alias
                    </label>
                    <input type="text" name="primaryEntityAlias" class="primaryEntityAlias" id="primaryEntityAlias" <%= (queryType == QueryTypes.ReportQuery) ? "readonly=\"readonly\"" : "" %> />
                </div>
                <%
                    }
                %>
            </div>
        </div>
    </section>
    <section class="panel green">
        <header class="panel-heading">
            <h4>Related Entities</h4>
        </header>
        <div class="panel-body pd-0">
            <div class="form-container" style="min-height: 390px;">
                <div id="relatedEntitiesDiv">
                    <div id="ParentEntitiesTable">
                    </div>
                    <div id="ChildEntitiesTable">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="col-md-9 pd-0">
    <section class="panel indigo">
        <header class="panel-heading">
            <h4>Select Clause</h4>
            <div class="actions pull-right">
                <a href="#" onclick="ShowExpressionColumn('insert')" class="btn btn-sm btn-default AddExprCol"
                    title="Add Expression Column"><i class="fa fa-plus"></i>&nbsp;Add
                </a>
            </div>
        </header>
        <div class="panel-body">
            <div id="selectClauseDiv">
                <table class="table" id="selectClauseTable">
                    <tbody>
                        <tr>
                            <td>
                                <select name="selectClauseFields" size="10" multiple="multiple" id="selectClauseFields"
                                    style="width: 350px; padding-left: 10px; height: 150px;">
                                </select>
                            </td>
                            <td style="vertical-align: middle;">
                                <select name="AggregateFunction" id="AggregateFunction" style="width: 100%;">
                                    <option value="-1">--Select a Field First--</option>
                                </select>
                            </td>
                            <td style="vertical-align: middle;">
                                <a class="btn btn-info" href="#" id="btnRightSelectClause"><i class="fa fa-plus"></i>&nbsp;Add</a>
                            </td>
                            <td>
                                <select name="selectedselectClauseFields" size="4" multiple="multiple" id="selectedselectClauseFields"
                                    style="height: 100px; width: 230px; padding-left: 10px; display: none;">
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="grid-part" style="padding: 10px; min-height: 160px;">
                    <table class="celloTable" id="selectedFieldsTable">
                        <thead>
                            <tr class="nodrop nodrag">
                                <th class="halign" style="width: 20px;">&nbsp;
                                </th>
                                <th>Field Id
                                </th>
                                <th>Display Name
                                </th>
                                <th class="halign">Sort
                                </th>
                                <th class="halign">Visibility
                                </th>
                                <th class="halign">Remove
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="expressionColList" style="display: block;">
                <div id="exprColsList">
                </div>
            </div>
        </div>
    </section>
    <section class="panel blue">
        <header class="panel-heading">
            <h4>Group Clause</h4>
        </header>
        <div class="panel-body">
            <div id="groupClause" style="overflow: auto; max-height: 250px;">
                <table class="celloTable" id="groupClauseTable">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <section style="min-height: 400px;" class="panel green">
        <header class="panel-heading">
            <h4>Filter Clause</h4>
        </header>
        <div class="panel-body">
            <div id="filterClause">
                <%
                    if (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin)
                        && ViewData["QueryType"] != null && ((QueryTypes)Enum.Parse(typeof(QueryTypes), ViewData["QueryType"].ToString())).Equals(QueryTypes.DatascopeQuery) && false)
                    {
                %>
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td>
                                <%: Html.TextArea("DataScopeFilter", new { @rows = "5", @cols="25", @style="width:95%" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <%
                    }
                %>
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td>Condition Field
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <select name="conditionField" id="conditionField" style="width:100%">
                                    <option value="-1">--Select a field--</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br/>
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td>Operator
                            </td>
                            <td>Condition Value
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <select name="conditionOperator" id="conditionOperator" style="width:50%">
                                    <option value="-1">--Select an operator--</option>
                                </select>
                            </td>
                            <td style="vertical-align: top;" rowspan="2">
                                <div id="conditionValueDiv">
                                    <input type="text" name="conditionValue" id="conditionValue" style="width: 160px;" />
                                </div>
                                <div style="float: left; margin: 5px;">
                                    <div class="autofillcontrols" style="display: none">
                                        <input type="checkbox" name="AutoPopulateFields" id="AutoPopulateFields" title="Fetch this field value from database!"
                                            style="position: relative; top: 2px;" />
                                        Auto Complete
                                    </div>
                                    <div class="gvControls" style="display: none">
                                        <input type="checkbox" name="GlobalVariable" id="GlobalVariable" title="Set this field value as Global Variable"
                                            style="position: relative; top: 2px;" />
                                        Global Variable
                                    </div>
                                    <div class="dynVariableControls" style="display: none">
                                        <input type="checkbox" name="DynamicVariable" id="DynamicVariable" title="Set this field value as Dynamic Variable"
                                            style="position: relative; top: 2px;" />
                                        Dynamic Variable
                                    </div>
                                    <div class="columnValues" style="display: none">
                                        <input type="checkbox" name="ColumnVariable" id="ColumnVariable" title="Set this field value as a Column"
                                            style="position: relative; top: 2px;" />
                                        Column
                                    </div>
                                </div>
                            </td>
                            <td style="vertical-align: top;">
                                <a class="btn btn-info" href="#" id="btnAddFilterCondidtion"><i class="fa fa-plus"></i>&nbsp;Add</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input id="conjunctionand" class="btn" type="button" value="And" />
                                <input id="conjunctionor" class="btn" type="button" value="Or" />
                            </td>
                            <td>
                                <a id="openparenthesis" name="parenthesis">
                                    <img src="/App_Themes/CelloSkin/openbracket.png" alt="(" />
                                </a><a id="closeparenthesis" name="parenthesis">
                                    <img src="/App_Themes/CelloSkin/closebracket.png" alt="(" />
                                </a>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div id="filterClauseDiv" style="padding: 10px; float: left; width: 99%;">
                                </div>
                                <input id="filterstring" name="filterstring" type="hidden" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
