﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    ViewData["RelatedQueries"] = new List<SelectListItem>();
    IEnumerable<QueryRelation> queryRelationList = ViewData["QueryRelationList"] != null ? (IEnumerable<QueryRelation>)ViewData["QueryRelationList"] : new List<QueryRelation>();
    Dictionary<string, QueryDetails> relatedQueries = ViewData["RelatedQueriesList"] != null ? ((Dictionary<string, QueryDetails>)ViewData["RelatedQueriesList"]) : new Dictionary<string, QueryDetails>();
%>
<div class="grid simple">
    <div class="grid-title">
        <h4>Query Relations</h4>
        <div class="pull-right">
            <a class="btn btn-success" href="#" id="btnQueryRelationAdd" title="Add Relation"><i class="fa fa-plus"></i>&nbsp;Add Relation</a>
        </div>
    </div>
    <div class="grid-body">
        <% if (queryRelationList.Count() == 0)
           { %>
        <div class="alert alert-info">
            No Relation defined yet.
        </div>
        <% }
           else
           { %>
        <%
               Html.Grid<QueryRelation>(queryRelationList).Columns(col =>
               {
                   col.For(x => x.Name).Named("Relation Name");
                   col.For(x => x.Description).Named("Relation Description");
                   col.For(x => !relatedQueries.ContainsKey(x.RelatedQueryId) ? x.RelatedQueryId : relatedQueries[x.RelatedQueryId].Name).Named("Related Query Name");
                   col.For(x => "<a href='#' class='editrelation' data-parentid='" + x.ParentQueryId + "' data-relationid='" + x.Id + "'><i class='fa fa-pencil'></i></a>").Named("Edit").HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                   col.For(x => "<a href='#' class='deleterelation' data-id='" + x.Id + "'><i class='fa fa-trash-o'></i></a>").Named("Delete").HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
               }).Attributes(@class => "celloTable").Render();
        %>
        <% } %>
    </div>
</div>
