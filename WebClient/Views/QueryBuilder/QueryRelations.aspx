﻿<%@ Page Title="Manage Query Relations" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        QueryDetails queryDetails = ViewData["QueryDetails"] as QueryDetails;
    %>
    <%=Html.Hidden("parentQueryId", queryDetails == null ? "" : queryDetails.Identifier) %>
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="Back to Query List">
            <i class="icon-custom-left"></i>
        </a>
        <h3>Manage Query Relations - <span class="semi-bold">
            <%= queryDetails == null ? string.Empty : queryDetails.Name%></span>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid-part" id="queryRelationTableDiv">
            <% Html.RenderPartial("QueryRelationGrid"); %>
        </div>
        <br />
        <div id="queryRelationsFormDiv">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/Content/QueryBuilderStyles.css" />
    <script type="text/javascript">
        $(function () {
            $('#queryRelationTableDiv').on('click', 'a.editrelation', function () {
                var queryRelationId = $(this).data('relationid');
                var parentQueryId = $(this).data('parentid');

                $('#queryRelationsFormDiv').fadeIn();
                $('#queryRelationsFormDiv').load('/QueryBuilder/ManageQueryRelation?queryRelationId=' + queryRelationId + '&parentQueryId=' + parentQueryId, function () {
                    $('#QueryRelation_RelatedQueryId').trigger('change');
                });
            });

            $('#queryRelationTableDiv').on('click', 'a.deleterelation', function () {             
                var queryRelationId = $(this).data('id');

                $.post('/QueryBuilder/DeleteQueryRelation?queryRelationId=' + queryRelationId, null, function (data) {
                    if (data.Error) {
                        alert(data.Error);
                    } else if (data.Success) {
                        alert(data.Success);
                    } else {
                        alert('Error while deleting relation details!');
                    }
                    $('#queryRelationsFormDiv').empty();
                    $('#queryRelationTableDiv').load('/QueryBuilder/QueryRelations?parentQueryId=' + $('#parentQueryId').val());
                });
            });

            $(document).on('click', '#btnQueryRelationAdd', function () {           
                $('#queryRelationsFormDiv').load('/QueryBuilder/ManageQueryRelation?parentQueryId=' + $('#parentQueryId').val());
                return false;
            });

            $('#queryRelationsFormDiv').on('click', '#btnQueryRelationCancel', function () {
                $('#queryRelationsFormDiv').empty();
                return false;
            });

            $('#queryRelationsFormDiv').on('change', '#QueryRelation_RelatedQueryId', function () {
                var queryId = $(this).val();
                var parentQueryId = $('#parentQueryId').val();
                var queryRelationId = $('#QueryRelation_Id').val();

                if (queryId == '') {
                    $('#variableMappingsDiv').empty();
                } else {
                    $('#variableMappingsDiv').load('/QueryBuilder/GetVariableMappings?queryId=' + queryId + '&parentQueryId=' + parentQueryId + '&queryRelationId=' + queryRelationId);
                }
            });

            $('#queryRelationsFormDiv').on('click', '#btnQueryRelationSave', function () {
                var postdata = $('form#frmQueryRelation').serializeJSON();
                var parentQueryId = $('#parentQueryId').val();

                postdata.parentQueryId = parentQueryId;

                $.ajax({
                    url: '/QueryBuilder/ManageQueryRelation',
                    data: JSON.stringify(postdata),
                    dataType: 'json',
                    contentType: 'application/json',
                    type: 'POST',
                    complete: function (data) {
                    },
                    success: function (data) {
                        if (data.Success) {
                            alert('Saved Successfully!');
                            $('#queryRelationsFormDiv').empty();
                            $('#queryRelationTableDiv').load('/QueryBuilder/QueryRelations?parentQueryId=' + $('#parentQueryId').val());
                        } else {
                            $('#queryRelationsFormDiv').html(data);
                        }
                    },
                    error: function (xhr) {
                        $('#queryRelationsFormDiv').html(xhr.responseText);
                    }
                });

                return false;
            });
        });
    </script>
</asp:Content>
