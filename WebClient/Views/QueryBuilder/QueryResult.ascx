﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Data.DataTable>" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    QueryDetails queryDetails = ViewData["queryDetails"] as QueryDetails;
    QueryRenderMode renderMode = ViewData["renderMode"] != null ? (QueryRenderMode)ViewData["renderMode"] : queryDetails.RenderMode;
%>
<div id="queryResult-<%=queryDetails.Identifier %>">
    <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
       { %>
    <div class="alert alert-danger">
        <%=Html.CelloValidationSummary("Error occured while executing the query!")%>
    </div>
    <% } %>
    <%
        var dynamicVars = ViewData["dynamicVariables"] as Dictionary<string, string>;
        var showDynamicVars = ViewData["hideSettings"] != null ? !(bool)ViewData["hideSettings"] : true;
    %>
    <% if (showDynamicVars && dynamicVars != null && dynamicVars.Count > 0)
       { %>
    <div id="dynamicVarsDiv-<%=queryDetails.Identifier %>" style="display: none;" title="Update dynamic values!">
        <form id="frmDynamicVars-<%=queryDetails.Identifier %>" action="">
            <table class="celloTable">
                <thead>
                    <tr>
                        <th>Dynamic Variable
                        </th>
                        <th>Value
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <%=Html.Hidden("renderMode", renderMode)%>
                    <% foreach (var dvar in dynamicVars)
                       { %>
                    <tr>
                        <td>
                            <label>
                                <%:dvar.Key %></label>
                        </td>
                        <td>
                            <% 
                           object value = string.Empty;
                           string classString = string.Empty;
                           if (dvar.Value == "Int")
                           {
                               value = 0;
                               classString = "intField";
                           }
                           else if (dvar.Value == "Float")
                           {
                               value = 0.0;
                               classString = "floatField";
                           }
                           else if (dvar.Value == "Date")
                           {
                               value = DateTime.Now.ToShortDateString();
                               classString = "dateField";
                           }
                            %>
                            <%=Html.TextBox(dvar.Key, value, new { @class=classString })%>
                        </td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </form>
        <script type="text/javascript">
            $(function () {
                $('#frmDynamicVars input.dateField').datepicker({
                    format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
                });
            });
        </script>
    </div>

    <div class="pull-right" style="margin-bottom:15px">
        <a class="btn btn-warning" href="#" onclick="javascript:UpdateDynamicValues(this,'<%=queryDetails.Identifier %>');"
            title="Update dynamic values!"><i class="fa fa-repeat"></i>&nbsp;Update
                    Dynamic Values</a>
    </div>
    <% } %>
    <div style="clear: both">
        <% if (renderMode == QueryRenderMode.Both || renderMode == QueryRenderMode.Chart)
           { %>
        <div class="chartPanel">
            <% Html.RenderPartial("RenderChart", Model); %>
        </div>
        <% } %>
        <% if (renderMode == QueryRenderMode.Both || renderMode == QueryRenderMode.Table)
           { %>
        <div class="tablePanel" style="margin-top: 20px;">
            <% Html.RenderPartial("RenderTable", Model); %>
        </div>
        <% } %>
    </div>
    <div class="modal fade" id="relatedQueryModalDiv-<%=queryDetails.Identifier %>"
        tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
        <div class="modal-dialog" style="left: -7%;">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">Related Query output</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function UpdateDynamicValues(el, queryId) {
        $('#dynamicVarsDiv-' + queryId).dialog('open');
    }

    function previewRelatedQuery(el) {
        var href = $(el).attr('href');
        var renderarea = $(el).data('renderarea');
        var rendermode = $(el).data('rendermode');
        var queryId = $(el).data('queryid');
        var relatedqueryId = $(el).data('relatedqueryid');

        if (renderarea == 'Window') {
            window.open(href);
            return false;
        } else {
            //window.open(href, 'Related Query Window', 'width=700,height=500,toolbar=0,menubar=0,location=0,status=0,scrollbars=1,resizable=1,left=0,top=0');
            //return false;
        }

        var params = href.split('?');

        if (params.length != 2) {
            return false;
        }

        params = params[1].split('&');

        var dvars = [];

        for (i = 0; i < params.length; ++i) {
            var p = params[i].split('=');
            if (p[0].indexOf('[') != -1) {
                var name = decodeURIComponent(p[0].substring(p[0].indexOf('[') + 1, p[0].length - 1));
                dvars.push({
                    VariableId: name,
                    Name: name,
                    Value: decodeURIComponent(p[1])
                });
            }
        }
        var postdata = JSON.stringify({ queryId: relatedqueryId, rendermode: rendermode, dynamicVariables: dvars, hideSettings: true });

        $.ajax
        ({
            type: "POST",
            url: '/QueryBuilder/PreviewQueryResult',
            contentType: 'application/json; charset=utf-8',
            async: true,
            data: postdata,
            success: function (htmldata) {
                $('#relatedQueryModalDiv-' + queryId + ' .modal-body').html(htmldata);
                $('#relatedQueryModalDiv-' + queryId).modal('show');
            }
        });
    }

    $(function () {
        $('#relatedQueryModalDiv-<%=queryDetails.Identifier %>').modal({ show: false });

        function initDynamicVarDialog(queryId) {
            $('#dynamicVarsDiv-' + queryId).dialog({
                width: "450px",
                autoOpen: false,
                modal: true,
                show: {
                    effect: "slide",
                    direction: "up",
                    duration: 300
                },
                hide: {
                    effect: "slide",
                    direction: "down",
                    duration: 300
                },
                open: function (event, ui) {
                    if (!$(this).html()) {
                        $(this).append($('#dynamicVarsDiv-<%=queryDetails.Identifier %>'));
                        $('#dynamicVarsDiv-<%=queryDetails.Identifier %>').show();
                    }
                },
                buttons: {
                    Cancel: function () {
                        $(this).dialog("close");
                    },
                    "Update": function () {
                        var queryId = $('input#queryId').val();

                        var $frm = $('form#frmDynamicVars-<%=queryDetails.Identifier %>');
                        var items = $frm.serializeArray();
                        var dvars = [];

                        for (i = 0; i < items.length; i++) {
                            dvars.push(
                                {
                                    VariableId: items[i].name,
                                    Name: items[i].name,
                                    Value: items[i].value
                                });
                        }

                        window.currentDynamicVariables = dvars;
                        var rendermode = $('#renderMode').val();
                        var postdata = JSON.stringify({ queryId: queryId, dynamicVariables: dvars, renderMode: rendermode });

                        var $div = $(this);

                        $.ajax
                            ({
                                type: "POST",
                                url: '/QueryBuilder/PreviewQueryResult',
                                contentType: 'application/json; charset=utf-8',
                                async: true,
                                data: postdata,
                                success: function (htmldata) {
                                    $div.dialog("close");
                                    $('#queryResult-<%=queryDetails.Identifier %>').html(htmldata);
                                    initDynamicVarDialog();
                                }
                            });
                    }
                }
            });
            }

        initDynamicVarDialog('<%=queryDetails.Identifier %>');

        if ($('#dynamicVarsDiv-<%=queryDetails.Identifier %>').length > 0
            && $('#dataList-<%=queryDetails.Identifier %>').length == 0) {
            UpdateDynamicValues('<%=queryDetails.Identifier %>');
        }
    });
</script>
