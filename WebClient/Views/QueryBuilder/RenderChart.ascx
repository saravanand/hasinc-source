﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Data.DataTable>" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    QueryDetails queryDetails = ViewData["queryDetails"] as QueryDetails;
    ViewData["queryId"] = queryDetails.Identifier;
%>
<%
    string jsonData = string.Empty;
    List<SelectListItem> linkColumnList = new List<SelectListItem>();
    List<SelectListItem> columnList = new List<SelectListItem>();

    if (Model != null && Model.Rows.Count > 0 && Model.Rows.Count < 50 && Model.Columns.Count >= 2 && Model.Columns.Count < 15)
    {
        var data = new List<Dictionary<string, object>>(Model.Columns.Count);

        for (int i = 0; i < Model.Columns.Count; ++i)
        {
            if (Model.Columns[i].ExtendedProperties.ContainsKey("IsLinkColumn"))
            {
                linkColumnList.Add(new SelectListItem { Text = Model.Columns[i].ColumnName, Value = i.ToString() });
            }
            else
            {
                columnList.Add(new SelectListItem { Text = Model.Columns[i].ColumnName, Value = i.ToString() });
            }
        }

        ViewData["title"] = Model.TableName;
        ViewData["xaxis.label"] = columnList[0].Text;
        ViewData["yaxis.label"] = columnList[1].Text;


        foreach (System.Data.DataRow item in Model.Rows)
        {
            var colVals = new Dictionary<string, object>();

            for (int col = 0; col < item.ItemArray.Length; ++col)
            {
                colVals.Add("field" + col.ToString(), item[col]);
            }

            data.Add(colVals);
        }

        jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.None);
    }

    List<SelectListItem> legendLocations = new List<SelectListItem>();
    legendLocations.Add(new SelectListItem { Text = "Top", Value = "top" });
    legendLocations.Add(new SelectListItem { Text = "Bottom", Value = "bottom" });
    legendLocations.Add(new SelectListItem { Text = "Left", Value = "left" });
    legendLocations.Add(new SelectListItem { Text = "Right", Value = "right" });

    ViewData["stack.enable"] = false;

    ViewData["legend.position"] = "bottom";
    ViewData["legend.visible"] = true;

    ViewData["tooltip.visible"] = true;
    ViewData["tooltip.format"] = "{0}";
    ViewData["tooltip.template"] = "#= category#: #= value#";

    ViewData["xaxis.data"] = 0;
    ViewData["xaxis.angle"] = 0;
    ViewData["xaxis.label.visible"] = true;
    ViewData["xaxis.format"] = "{0}";
    ViewData["xaxis.template"] = "#= value#";

    ViewData["yaxis.data"] = 1;
    ViewData["yaxis.angle"] = 0;
    ViewData["yaxis.label.visible"] = true;
    ViewData["yaxis.format"] = "{0}";
    ViewData["yaxis.template"] = "#= value#";

    ViewData["grid.visible"] = true;
    ViewData["grid.borderWidth"] = 1;
    ViewData["grid.lineColor"] = "";
    ViewData["grid.backgroundColor"] = "";
    ViewData["grid.borderColor"] = "";
    ViewData["yaxis.axis.visible"]=true;
%>
<% if (string.IsNullOrEmpty(jsonData))
   { %>
<div class="alert alert-info">
    No data available to render chart!
</div>
<% }
   else
   {
       var chartThemes = new List<SelectListItem>();
       chartThemes.Add(new SelectListItem { Value = "default", Text = "Default" });
       chartThemes.Add(new SelectListItem { Value = "black", Text = "Black" });
       chartThemes.Add(new SelectListItem { Value = "blueopal", Text = "Blue" });
       chartThemes.Add(new SelectListItem { Value = "metro", Text = "Metro", Selected = true });
       chartThemes.Add(new SelectListItem { Value = "metroblack", Text = "Metro Black" });
%>
<%=Html.Hidden("chartData", jsonData) %>
<section class="panel red">
    <header class="panel-heading">
        <h4>Chart</h4>
        <div class="actions pull-right">
            <% if (queryDetails != null && !string.IsNullOrEmpty(queryDetails.Identifier))
               { %>
            <a href="#" class="btn btn-sm btn-info" id="btnSave-<%=queryDetails.Identifier %>" data-queryid="<%=queryDetails.Identifier %>"
                title="Save this chart settings as new!"><span><i class="fa fa-save"></i>&nbsp;Save</span></a>
            <% } %>
            <a href="#" class="btn btn-sm btn-default" data-queryid="<%=queryDetails.Identifier %>" onclick="javascript:doRefreshChart(this);"
                title="Redraw the chart!"><span><i class="fa fa-refresh"></i>&nbsp;Refresh</span></a>
        </div>
    </header>
    <div id="chartWrap" class="panel-body">
        <form id="frmChartConfiguration-<%=queryDetails.Identifier %>" action="">
            <div id="common-chart-settings-<%=queryDetails.Identifier %>" class="chart-configuration form-container"
                data-queryid="<%=queryDetails.Identifier %>" style="background: none; width: auto; border: none;">
                <section class="panel blue">
                    <header class="panel-heading bg-info">
                        <ul class="nav nav-tabs nav-justified captilize" id="chartConfigTabs">
                            <li class="active"><a data-toggle="tab" href="#settings-common-<%=queryDetails.Identifier %>"><i class="fa fa-asterisk"></i>&nbsp;Common</a></li>
                            <li><a data-toggle="tab" href="#settings-category-axis-<%=queryDetails.Identifier %>"><i class="fa fa-arrows-h"></i>&nbsp;Category Axis</a></li>
                            <li><a data-toggle="tab" href="#settings-value-axis-<%=queryDetails.Identifier %>"><i class="fa fa-arrows-v"></i>&nbsp;Value Axis</a></li>
                        </ul>
                    </header>
                    <div class="panel-body pd-0">
                        <div class="tab-content">
                            <div id="settings-common-<%=queryDetails.Identifier %>" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input id="title" name="title" type="text" value="<%:ViewData["title"] %>" autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            <label for="chart_type">Chart Type</label>
                                            <br />
                                            <select name="chart.type" id="chart_type" style="width: 100%; margin-bottom: 10px;">
                                                <option value="line">Line</option>
                                                <option value="bar">Bar</option>
                                                <option value="column">Column</option>
                                                <option value="pie">Pie</option>
                                                <option value="donut">Donut</option>
                                                <option value="area">Area</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="stack_enable">Stack Series</label>
                                            <br />
                                             <input type="checkbox" style="margin-bottom: 15px;" name="stack.enable" id="stack_enable" <%:(bool)ViewData["stack.enable"] ? "checked" : "" %> />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="grid_visible">Enable Grid</label>
                                            <br />
                                            <input type="checkbox" style="margin-bottom: 15px;" name="grid.visible" id="grid_visible" <%:(bool)ViewData["grid.visible"] ? "checked" : "" %> />
                                        </div>
                                        <div class="form-group">
                                            <label for="legend_visible">Enable Legend</label>
                                            <br />
                                            <input type="checkbox" style="margin-bottom: 15px;" name="legend.visible" id="legend_visible" <%:(bool)ViewData["legend.visible"] ? "checked" : "" %> />
                                        </div>
                                        <div class="form-group">
                                            <label for="legend_position">Legend Position</label>
                                            <br />
                                            <%=Html.DropDownList("legend.position", legendLocations, new {  style = "width:100%;"})%>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <% if (linkColumnList.Count > 0)
                                           { %>
                                        <div class="form-group">
                                            <label for="chart_clickquerydatafield" style="width: 100%;">Click Query</label>
                                            <br />
                                            <%=Html.DropDownList("chart.clickquerydatafield", linkColumnList, new { style = "width:100%;", title = "Choose a related query. Which will be opened on clicking the data point!" })%>
                                        </div>
                                        <% } %>
                                        <div class="form-group">
                                            <label for="tooltip_visible" style="width: 100%;">Tooltip</label>
                                            <br />
                                            <input type="checkbox" style="margin-bottom: 15px;" name="tooltip.visible" id="tooltip_visible" <%:(bool)ViewData["tooltip.visible"] ? "checked" : "" %> />
                                        </div>
                                        <div class="form-group">
                                            <label for="tooltip_format" style="width: 100%;">Tooltip Format</label>
                                            <%=Html.TextBox("tooltip.format") %>
                                        </div>
                                        <div class="form-group">
                                            <label for="tooltip_template" style="width: 100%;">Tooltip Template</label>
                                            <%=Html.TextBox("tooltip.template") %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="settings-category-axis-<%=queryDetails.Identifier %>" class="tab-pane fade in">                             
                                <div class="row c-container">
                                    <div class=" col-md-4 c-series c-static" data-seriesid="1" style="padding-bottom: 10px;">
                                        <div class="tiles green">
                                            <div class="tiles-body">
                                                <div class="tiles-title c-seriesname" style="font-size: 12px;">Category</div>
                                                <div class="row" style="margin-top: 10px;" data-series="0">
                                                    <div class="form-group">
                                                        <label for="xaxis_data">Data</label>
                                                        <br />
                                                        <%=Html.DropDownList("xaxis.data", columnList, new Dictionary<string,object> { {"data-target","xaxis_label"}, {"style","width:100%;"} })%>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="checkbox" name="xaxis.label.visible" id="xaxis_label_visible" <%:(bool)ViewData["xaxis.label.visible"] ? "checked" : "" %> />
                                                        <label for="xaxis_label">
                                                            Label</label>
                                                        <%=Html.TextBox("xaxis.label") %>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="xaxis_format">
                                                            Format</label>
                                                        <%=Html.TextBox("xaxis.format") %>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="xaxis_template">
                                                            Template</label>
                                                        <%=Html.TextBox("xaxis.template") %>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="settings-value-axis-<%=queryDetails.Identifier %>" class="tab-pane fade in">
                                <div class="actions">
                                    <a class="btn btn-xs btn-info" href="#" onclick="javascript:return AddValueAxis('settings-value-axis-<%=queryDetails.Identifier %>','<%=queryDetails.Identifier %>');"><i class="fa fa-plus"></i>&nbsp;Add</a>
                                </div>
                                <br />
                                <div class="row c-container">
                                    <div class="col-md-4 c-series c-static" data-seriesid="1" style="padding-bottom: 10px;">
                                        <div class="tiles purple">
                                            <div class="tiles-body">
                                                <div class="actions">
                                                    <a href="#" onclick="javascript: return RemoveAxes(this,'settings-value-axis-<%=queryDetails.Identifier %>','<%=queryDetails.Identifier %>');"
                                                        title="Remove!"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                                <div class="tiles-title c-seriesname" style="font-size: 12px;">Series 1</div>
                                                <div class="row" style="margin-top: 10px;" data-series="0">
                                                    <div class="form-group">
                                                        <label for="yaxis_data">Data</label>
                                                        <%=Html.DropDownList("yaxis.data", columnList, new Dictionary<string, object> { { "data-target", "yaxis_label" }, {"style","width:100%;"} })%>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="checkbox" name="yaxis.label.checkbox" data-target="yaxis_label_visible" id="yaxis.label.checkbox" <%:(bool)ViewData["yaxis.label.visible"] ? "checked" : "" %> />
                                                        <input type="hidden" id="yaxis_label_visible" name="yaxis.label.visible" value=<%:(bool)ViewData["yaxis.label.visible"] ? "on" : "off" %> /> 
                                                        <label for="yaxis_label">Label</label>
                                                        <%=Html.TextBox("yaxis.label") %>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="yaxis_format">Format</label>
                                                        <%=Html.TextBox("yaxis.format") %>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="yaxis_template">Template</label>
                                                        <%=Html.TextBox("yaxis.template") %>
                                                    </div>
                                                     <div class="form-group">
                                                         <input type="checkbox" name="yaxis.axis.checkbox" data-target="yaxis_axis_visible" id="yaxis.axis.checkbox" <%:(bool)ViewData["yaxis.axis.visible"] ? "checked" : "" %> />
                                                        <input type="hidden" id="yaxis_axis_visible" name="yaxis.axis.visible" value=<%:(bool)ViewData["yaxis.axis.visible"] ? "on" : "off" %> /> 
                                                        <label for="yaxis_axis">Is Axis</label>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </section>
            </div>
        </form>
        <div class="content-box">
            Theme:
            <%=Html.DropDownList("chart.theme", chartThemes, new { id="chart_theme-"+ViewData["queryId"], style="width:120px;vertical-align:middle;", onchange = "javascript:changeChartTheme(this,'"+ViewData["queryId"] +"');" })%>
            <span style="padding-left: 15px;">Download: <a download="preview.png" href="#" class="c-button" data-queryid="<%:ViewData["queryId"] %>"
                onclick="javascript:downloadChart(this);" title="Download as image!"><span><i class="fa fa-cloud-download"></i>&nbsp;Png</span></a>
            </span>
        </div>
        <div id="chartPreviewDiv-<%=queryDetails.Identifier %>" style="min-height: 450px;">
            <% Html.RenderPartial("PreviewRenderChart"); %>
        </div>
    </div>
</section>
<script type="text/javascript">
    function doRefreshChart(el) {
        var queryId = $(el).data('queryid');
        var chart = $("#chart-" + queryId).data('celloChart');
        if (chart) {
            chart.update();
        }
    }

    function changeChartTheme(el, queryId) {
        var theme = $(el).val();
        var chart = $("#chart-" + queryId).data('celloChart');

        if (chart) {
            //chart.changeTheme(theme);
            chart.destroy();
            var type = chart.getOptions().seriesDefaults.type;
            $("#chartPreviewDiv-" + queryId).fadeTo('medium', 0.1, function () {
                $.get('/QueryBuilder/PreviewRenderChart?queryId=' + queryId + '&type=' + type + '&theme=' + theme, null, function (hdata) {
                    $("#chartPreviewDiv-" + queryId).html(hdata).fadeTo('medium', 1.0);
                });
            });
        }
    }

    $(document).on('click', '#btnSaveChart-<%=queryDetails.Identifier %>', function () {
        var queryId = $('#saveAsChartModal-<%=queryDetails.Identifier %> input[name=queryId]').val();
        var chartName = $('#saveAsChartModal-<%=queryDetails.Identifier %> input[name=chartName]').val();

        if (!chartName || chartName.length < 0) {
            alert('Enter a valid chart name!');
            return;
        }

        var queryId = '<%=queryDetails.Identifier %>';
        var chart = $('#chart-' + queryId).data('celloChart');
        var options = getOptions(queryId);

        options.data = null;

        if (options.dataSource) {
            options.dataSource.data = null;
        }

        if (options.series) {
            for (i = 0; i < options.series.length; i++) {
                options.series[i].data = null;
            }
        }

        if (options.categoryAxis && options.categoryAxis.categories) {
            options.categoryAxis.dataItems = null;
            options.categoryAxis.categories = null;
        }

        var chartOptions = JSON.stringify(options);

        var psdata = {
            queryId: queryId,
            chartName: chartName,
            chartOptions: chartOptions
        };

        $.post('/QueryBuilder/SaveAsChart', psdata, function (data) {
            if (data && data.Success) {
                alert('Saved successfully!');
                $('#saveAsChartModal-<%=queryDetails.Identifier %>').modal("hide");
            } else {
                alert((data && data.Error) ? data.Error : 'Error while saving chart details!');
            }
        });
    });
</script>
<% } %>