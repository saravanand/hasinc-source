﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.Data.DataTable>" %>
<%@ Import Namespace="CelloSaaS.QueryBuilderLibrary.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    int totalRecords = Model == null ? 0 : Model.Rows.Count;
    if (ViewData["totalRecords"] != null && !string.IsNullOrEmpty(ViewData["totalRecords"].ToString()))
    {
        totalRecords = int.Parse(ViewData["totalRecords"].ToString());
    }

    if (totalRecords > 0)
    {
        var queryDetails = ViewData["queryDetails"] as QueryDetails;
        //var queryRelations = ViewData["QueryRelationsList"] as Dictionary<string, QueryRelation>;
%>
<div class="grid simple">
    <div class="grid-body no-border">
        <br />
        <div class="content-box">
            <span><strong>Total Records:
        <%:totalRecords%></strong></span> <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            Download: <a href="#" title="Export to Excel" class="c-button btnExportExcel" data-queryid="<%=queryDetails.Identifier %>"
                rel="nofollow"><i class="fa fa-cloud-download"></i>&nbsp;Excel</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="#" title="Export to PDF" class="c-button btnExportPDF" data-queryid="<%=queryDetails.Identifier %>" rel="nofollow">
        <i class="fa fa-file"></i>&nbsp;PDF</a>
        </div>
        <table class="celloTable" id="dataList-<%=queryDetails.Identifier %>">
            <thead>
                <tr>
                    <% foreach (System.Data.DataColumn col in Model.Columns)
                       { %>
                    <th <%=col.ExtendedProperties.ContainsKey("IsLinkColumn") ? "class=\"halign\"" : ""%>>
                        <%:col.ColumnName%>
                    </th>
                    <% } %>
                </tr>
            </thead>
            <tbody>
                <%  
        foreach (System.Data.DataRow row in Model.Rows)
        {
                %>
                <tr>
                    <% for (int i = 0; i < row.ItemArray.Length; ++i)
                       {
                           string content = row.ItemArray[i].ToString();
                    %>
                    <% if (Model.Columns[i].ExtendedProperties.ContainsKey("IsLinkColumn"))
                       { %>
                    <td class="halign">
                        <!-- do not encode the value -->
                        <%= content %>
                    </td>
                    <% }
                       else
                       { %>
                    <td>
                        <%= content.StartsWith("http") ? HttpUtility.UrlDecode(content) : HttpUtility.HtmlEncode(content)%>
                    </td>
                    <% } %>
                    <% } %>
                </tr>
                <% } %>
            </tbody>
        </table>
        <script type="text/javascript">
            $(function () {
                $('#dataList-<%=queryDetails.Identifier %>').dataTable({
                    "bFilter": true,
                    "bSort": false,
                    "bAutoWidth": true,
                    "iDisplayLength": 10
                });

                $('#dataList-<%=queryDetails.Identifier %>').on('click', 'a.relatedquery', function (e) {
                    e.preventDefault();
                    previewRelatedQuery($(this)[0]);
                    return false;
                });
            });
        </script>
        <% 
    }
    else
    {
        %>
        <div class="alert alert-info">
            No Data Available!
        </div>
        <% } %>
    </div>
</div>
