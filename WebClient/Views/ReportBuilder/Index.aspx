﻿<%@ Page Title="<%$Resources:PageTitle%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.ReportBuilder.ServiceContracts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="manageReport">
        <div class="center_container">
            <div class="heading_container">
                <h1>
                    <%:this.GetLocalResourceObject("Title") %>
                </h1>
                <%
                    if (UserIdentity.HasPrivilege(ReportPrivileges.AddReport))
                    {	
                %>
                <div class="inner_hold">
                    <div class="green_but" id="addReportBtn">
                        <a title="Add New Report" onclick="AddReport()"><span>
                            <%: this.GetGlobalResourceObject("General","Add") %></span> </a>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
            <%
                if (Html.ValidationMessage("Error") != null && !string.IsNullOrEmpty(Html.ValidationMessage("Error").ToHtmlString()))
                {
            %>
            <div class="error">
                <%: Html.ValidationMessage("Error") %>
            </div>
            <%
                }
                if (Html.ValidationMessage("Success") != null && !string.IsNullOrEmpty(Html.ValidationMessage("Success").ToHtmlString()))
                {
            %>
            <div class="success">
                <%: Html.ValidationMessage("Success") %>
            </div>
            <%
                }
            %>
            <div class="form_list" style="clear: both">
                <%
                    IEnumerable<CelloSaaS.ReportBuilder.Model.Report> reports = (IEnumerable<CelloSaaS.ReportBuilder.Model.Report>)ViewData["ReportList"];

                    if (reports != null && reports.Count() > 0)
                    {
                %>
                <div class="secondary_search_container">
                    <div class="secondary_search_holder">
                        <div class="search_holder">
                            <input type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>"
                                id="searchText" name="searchText" />
                        </div>
                        <div class="go_but_grey">
                            <a title="GO" onclick="DoSearch();">
                                <%=this.GetGlobalResourceObject("General","GO") %></a></div>
                    </div>
                </div>
                <div class="clear">
                <%
                        string editUrl = this.ResolveClientUrl("../../App_Themes/CelloSkin/edit_icon.gif");
                        string deleteUrl = this.ResolveClientUrl("../../App_Themes/CelloSkin/remove_but.gif");
                        string viewUrl = this.ResolveClientUrl("../../App_Themes/CelloSkin/view_icon.gif");
                        string previewUrl = this.ResolveClientUrl("../../Content/preview.png");

                        Html.Grid(reports).Columns(column =>
                        {
                            column.For(c => c.Name).Named("Name");
                            column.For(c => c.Description).Named("Description");
                            column.For(c => c.CreatedOn.ToShortDateString()).Named("Creation Date");
                            column.For(c => "<a href='/ReportBuilder/ReportDesigner?reportId=" + c.ReportId + "'><img src='" + editUrl + "' /></a>").Named("Edit").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                            column.For(c => "<a href='/ReportBuilder/ReportViewer?reportId=" + c.ReportId + "'><img src='" + viewUrl + "' /></a>").Named("View").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                            column.For(c => "<a href='/ReportBuilder/PrintView?reportId=" + c.ReportId + "'><img src='" + previewUrl + "' /></a>").Named("Print Preview").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                            column.For(c => "<a href='/ReportBuilder/DeleteReport?reportId=" + c.ReportId + "'><img src='" + deleteUrl + "' /></a>").Named("Delete").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                        }).Attributes(id => "reportsList", @class => "celloTable").Render();
                        %>
                        </div>
                        <%
                    }
                    else
                    { %>
                <div class="info">
                    <%: this.GetLocalResourceObject("NoReportsFound")%>
                </div>
                <%
                    }                    
                %>
            </div>
            <div class="addReportContainer">
                <% Html.RenderPartial("ReportDetailPartialView"); %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%: Url.Content("../../Content/ReportBuilder.css")%>" rel="stylesheet"
        type="text/css" />
    <script src="<%: Url.Content("../../Scripts/json2.js")%>" type="text/javascript"></script>
    <script src="<%: Url.Content("../../Scripts/managereports.js")%>" type="text/javascript"></script>
    <script src="<%: Url.Content("../../Scripts/reporting_common.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        var thisDataTable = null;

        $(document).ready(function () {
            thisDataTable = $('#reportsList').dataTable({
                "bFilter": true,
                "bInfo": true,
                "sPaginationType": "full_numbers",
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray}]
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            thisDataTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
</asp:Content>
