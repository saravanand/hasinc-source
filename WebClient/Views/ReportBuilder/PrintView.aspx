﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaSApplication.Models.ReportViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="heading_container" id="reportHeader">
        <h1>
            <%:this.GetLocalResourceObject("Title") %>
        </h1>
        <div class="inner_hold">
            <div class="green_but">
                <a href="<%: Url.Action("Index","ReportBuilder") %>" title="Back to Report List">Back</a>
            </div>
        </div>
    </div>
    <%
        if (this.Model != null)
        {
    %>
    <%: Html.Hidden("reportId", this.Model.ReportId) %>
    <%: Html.Hidden("reportName", this.Model.Name) %>
    <%
        }
        else if (ViewData["reportId"] != null)
        {
    %>
    <%: Html.Hidden("reportId",ViewData["reportId"].ToString()) %>
    <%
        }
    %>
    <div class="fullReportView">
        <div class="report_execution_warnings" style="display: none">
            <div class="error">
            </div>
        </div>
        <div class="reportContent">
        </div>
    </div>
    <div class="reportParameters" style="display: none">
        <div>
            <div class="inner_hold">
                <div class="green_but">
                    <a class="updateDynamicVariableValues" onclick="updateDynamicVariableValues()" title="Update Dynamic Variable Values">
                        <span>
                            <%:this.GetLocalResourceObject("Apply")%></span></a>
                </div>
            </div>
        </div>
        <div class="dynamicVariablesForm">
        </div>
        <div>
            <div class="inner_hold">
                <div class="green_but">
                    <a class="updateDynamicVariableValues" onclick="updateDynamicVariableValues()" title="Update Dynamic Variable Values">
                        <span>
                            <%:this.GetLocalResourceObject("Apply")%></span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="topmenu" runat="server">
    <style type="text/css">
        .fullReportView
        {
            margin-top: 5px;
        }
        
        .executedContent table
        {
            font-family: "Lucida Sans Unicode" , "Lucida Grande" , Sans-Serif;
            font-size: 12px;
            width: 98%;
            text-align: left;
            border-collapse: collapse;
            border: 1px solid #B9C9FE;
            margin: 20px;
            overflow-x: scroll;
        }
        
        .executedContent .tableSource
        {
            overflow-x: scroll;
            margin-right: 5px;
        }
        
        .executedContent table th
        {
            font-size: 13px;
            font-weight: bold;
            background: #E8EDFF;
            border-top: 4px solid #AABCFE;
            border-bottom: 1px solid #E8EDFF;
            color: #039;
            padding: 8px;
            border: 1px solid #669;
        }
        
        .executedContent table td
        {
            background: #ffffff;
            border-bottom: 1px solid #E8EDFF;
            color: #669;
            border-top: 1px solid #E8EDFF;
            padding: 8px;
            border: 1px solid #669;
        }
        
        #chart
        {
            overflow: auto !important;
        }
    </style>
    <script type="text/javascript">
        /* Global Variables */
        var report = {};
        var _reportObjects = {};
        var reportIdentifier = '<%: ViewData["reportId"].ToString() %>';
        var tableSource = "A0DFECED-33F3-4DE1-91C4-3203E5EB2194";
        var textSource = "AE02E1E0-F37A-41B9-8098-64C9E71360C1";
        var imageSource = "5A3B3969-F960-4B1F-B9DD-9DE2A776AEA5";
        var chartSource = "35F17E82-DC46-491F-A6E2-A874258DC135";
        var msg_InvalidSource = '<%:this.GetLocalResourceObject("NoValidSourceFound")%>';
        var msg_exception = '<%:this.GetLocalResourceObject("ExceptionOnReportObject")%>';
        var reportingHomeUrl = '<%: Url.Action("Index","ReportBuilder") %>';
        var reportExecutionUrl = '<%: Url.Action("ExecuteReportObject","ReportBuilder") %>';
        var chartPreviewUrl = '<%: Url.Action("PreviewChart","ChartDetails")%>';
    </script>
    <script src='<%: Url.Content("../../Scripts/preexecutereport.js") %>' type="text/javascript"></script>
    <script type="text/javascript" src='<%: Url.Content("../../Scripts/reportprintview.js")%>'></script>
    <style type="text/css">
        .ui-dialog-titlebar-close
        {
            display: none;
        }
        .header
        {
            font-weight: bold;
            color: #5F92A7;
            text-decoration: underline;
        }
    </style>
</asp:Content>
