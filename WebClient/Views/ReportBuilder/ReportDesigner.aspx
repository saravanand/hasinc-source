﻿<%@ Page Title="<%$ Resources:PageTitle%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.ReportBuilder.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="reportManager">
        <div class="heading_container">
            <h1>
                <%:this.GetLocalResourceObject("Title") %>
            </h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a href="<%:Url.Action("Index","ReportBuilder") %>" title="Back"><span>Back</span></a>
                </div>
                <div class="green_but">
                    <a title="Back" onclick="SaveAllReportObjects()"><span>Save</span> </a>
                </div>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <%
            string reportId = string.Empty;

            if (ViewData["reportId"] != null)
            {
                reportId = ViewData["reportId"].ToString();
            }           
        %>
        <%: Html.Hidden("Report_Id",reportId) %>
        <div class="clear success" id="reportMessages" style="display: none">
        </div>
        <div class="manageReportObjects" style="display: block">
            <div class="report-object">
                <div class="report-object-header">
                    <h2>
                        <%: this.GetLocalResourceObject("ReportObject")%></h2>
                </div>
                <div class="report-object-list">
                    <div class="draggable" id="tableobj">
                        <img src="<%: this.ResolveClientUrl("../../Content/table-icon.png") %>" alt="Table Report Object" />
                    </div>
                    <div class="draggable" id="chartobj">
                        <img src="<%: this.ResolveClientUrl("../../Content/chart-icon.png") %>" alt="Table Report Object" />
                    </div>
                </div>
            </div>
            <div class="report-content">
                <div class="dropboxheader">
                    <h2>
                        <%: this.GetLocalResourceObject("ReportContainer")%>
                    </h2>
                    <%
                        if (ViewData["report"] == null)
                        {
                            ViewData["report"] = new Report();
                        }

                        Report currentReport = (Report)ViewData["report"];

                        if (!string.IsNullOrEmpty(currentReport.Identifier) && !string.IsNullOrEmpty(currentReport.ReportId))
                        {
                    %>
                    <div id="reportWarnings" style="display: none; clear: both">
                        <div class="content">
                        </div>
                    </div>
                    <div id="successMessages" style="display: none; clear: both">
                        <div class="notification-bg" id="msg-success">
                            <div class="content">
                            </div>
                        </div>
                    </div>
                    <table class="celloTable details_container_table" id="reportDetailsform">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <%: this.GetLocalResourceObject("ReportDetails")%>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="row4" style="width: 100px;">
                                    <%: this.GetLocalResourceObject("Name")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                                </td>
                                <td class="row5">
                                    <%:Html.Hidden("Identifier", currentReport.Identifier) %>
                                    <%:Html.TextBox("Name", currentReport.Name, new { @style = "width:95%" })%>
                                </td>
                            </tr>
                            <tr>
                                <td class="row4">
                                    <%: this.GetLocalResourceObject("Description")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                                </td>
                                <td class="row5">
                                    <%:Html.TextArea("Description", currentReport.Description, new { @style = "width:95%" })%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%
                        }
                    %>
                </div>
                <div class="loadingReportObjects" style="display: none;">
                    <p style="text-align: center">
                        <img src="<%: this.ResolveClientUrl("../../Content/ajax-loader.gif")%>" alt="loading" />
                    </p>
                </div>
                <div class="column" id="column1" style="border: thin solid #B5CBD3; float: right">
                </div>
            </div>
            <div class="tableSrcColumn" id="tableSrcColumn" style="display: none">
                <%--<div style="float: right">
                <span style="float: right"><a class="tablecolumncontent_close" title="close"></a>
                </span>
            </div>--%>
                <div class="columnContent" style="clear: both">
                </div>
                <div class="green_but">
                    <span style="float: right"><a onclick="SaveTableResultColumns()" title="close">
                        <%--<img src="../../App_Themes/CelloSkin/action-save.gif" alt="save" title="save" />--%>
                        <span>
                            <%: this.GetGlobalResourceObject("General", "Save")%></span> </a></span>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="<%: Url.Content("../../Scripts/managedatasource.js")%>" type="text/javascript"></script>
    <link href="<%: Url.Content("../../Content/ReportBuilder.css")%>" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="<%: Url.Content("../../Scripts/managereports.js")%>"></script>
    <style type="text/css">
        .content_area
        {
            min-height: 80% !important;
        }
    </style>
</asp:Content>
