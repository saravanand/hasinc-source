﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div style="clear: both; display: none" class="addReport">
    <div class="error" style="color: Red; display: none; margin-left: 25px">
    </div>
    <%
        if (ViewData["reportId"] == null)
        {                
    %>
    <table cellpadding="0" cellspacing="1" class="celloTable details_container_table"
        id="reportDetails" style="width: 100%">
        <%
                    }
        %>
        <thead>
            <tr>
                <th colspan="2">
                    <%: this.GetLocalResourceObject("Title") %>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="row4">
                    <%: Html.Label(this.GetLocalResourceObject("Name").ToString()) %><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                </td>
                <td class="row5">
                    <%
                        string rName = null, rDesc = null;
                        if (ViewData.ContainsKey("reportName"))
                        {
                            rName = ViewData["reportName"].ToString();
                        }

                        if (ViewData.ContainsKey("reportDesc"))
                        {
                            rDesc = ViewData["reportDesc"].ToString();
                        }
                    %>
                    <%:Html.TextBox("Name",rName) %>
                </td>
            </tr>
            <tr>
                <td class="row4">
                    <%: Html.Label(this.GetLocalResourceObject("Description").ToString())%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                </td>
                <td class="row5">
                    <%:Html.TextArea("Description",rDesc) %>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <div class="inner_hold">
                        <div class="green_but">
                            <div class="saveReport">
                                <a style="font-style: normal" title="Save Report" onclick="SaveReport()"><span><%: this.GetGlobalResourceObject("General", "Save")%></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
