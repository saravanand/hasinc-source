﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CelloSaaS.ReportBuilder.Model" %>
<%@ Import Namespace="CelloSaaS.ReportBuilder.ServiceContracts" %>
<%  
    if (ViewData["viewObjectId"] == null)
    {
        ViewData["viewObjectId"] = Guid.NewGuid().ToString();
    }

    string viewObjectId = ViewData["viewObjectId"].ToString();

    // for every new request, js will give the reportobject's view id
    string reportObjectId = string.Empty;

    if (ViewData["reportObjectId"] != null)
    {
        reportObjectId = ViewData["reportObjectId"].ToString();
    }

    if (ViewData["ordinal"] == null)
    {
        ViewData["ordinal"] = 0;
    }

    int ordinalValue = int.Parse(ViewData["ordinal"].ToString());

    if (ViewData["reportheader"] == null)
    {
        ViewData["reportheader"] = string.Empty;
    }
    string reportheader = ViewData["reportheader"].ToString();

    if (ViewData["reportObjectTypeId"] == null)
    {
        throw new ArgumentException(this.GetLocalResourceObject("e_InvalidReportObjectTypeIdentifier").ToString());
    }

    string reportObjectTypeId = ViewData["reportObjectTypeId"].ToString();

    if (ViewData["reportObjectTypeMapping"] == null)
    {
        throw new ArgumentException(this.GetLocalResourceObject("e_ReportObjectTypeMappingNotFound").ToString());
    }
    Dictionary<string, string> ReportObjectTypeMapping = (Dictionary<string, string>)ViewData["reportObjectTypeMapping"];

    if (ReportObjectTypeMapping == null || ReportObjectTypeMapping.Count < 1)
    {
        throw new ArgumentException(this.GetLocalResourceObject("e_ReportObjectTypeMappingNotFound").ToString());
    }

    string reportObjectTypeName = string.Empty;

    if (ViewData["reportObjectTypeName"] != null)
    {
        reportObjectTypeName = ViewData["reportObjectTypeName"].ToString();
    }

    if (!string.IsNullOrEmpty(reportObjectTypeName))
    {
        reportObjectTypeName = ReportObjectTypeMapping[reportObjectTypeId];
    }

    ReportObject reportObject = new ReportObject();

    string reportObjSourceId = string.Empty;

    if (ViewData["reportObjectSourceId"] != null)
    {
        reportObjSourceId = ViewData["reportObjectSourceId"].ToString();
    }

    if (!string.IsNullOrEmpty(reportObject.SourceId))
    {
        reportObjSourceId = reportObject.SourceId;
    }

    if (ViewData["reportObject"] != null)
    {
        reportObject = (ReportObject)ViewData["reportObject"];
        reportObjectId = reportObject.ReportObjectId;
    }
    else
    {
        reportObjectId = viewObjectId;
    }

    if (!string.IsNullOrEmpty(reportObject.TypeId) && ReportObjectTypeMapping.ContainsKey(reportObject.TypeId.ToUpperInvariant()))
    {
        reportheader = ReportObjectTypeMapping[reportObject.TypeId];
    }

    if (reportObject.SourceObject != null)
    {
        reportheader += reportObject.SourceObject.Name;
    }
    else
    {
        reportheader += "";
    }

    if (!string.IsNullOrEmpty(reportObject.TypeId))
    {
        reportObjectTypeId = reportObject.TypeId;
        reportObjectTypeName = ReportObjectTypeMapping[reportObjectTypeId];
    }

    var rowsPerPage = new Dictionary<string, string>
        {
            {"10","10"},
            {"25","25"},
            {"50","50"},
            {"100","100"},
            {"All","All"}
        };

    var selectedRowsPerPage = "All";

    if (reportObject != null && !string.IsNullOrEmpty(Convert.ToString(reportObject.RowsPerPage)))
    {
        selectedRowsPerPage = Convert.ToString(reportObject.RowsPerPage);
    }

    ViewData["ReportObject_RowsPerPage"] = new SelectList(rowsPerPage, "Key", "Value", selectedRowsPerPage);
%>
<div class="ReportObject dragbox" id="<%: reportObjectId %>" style="display: block;"
    title="<%:reportObjectId%>">
    <div id="<%:reportObjectId%>">
        <input type="hidden" value="<%:reportObjectId %>" class="viewObjectId" />
        <h2>
            [<span class="ordinal">
                <%:ordinalValue %>
            </span>] <span class="reportheader">
                <%: reportheader%>
            </span><span class="configure" onclick="removereportobject('<%:reportObjectId %>');">
                <a title="Delete">
                    <img src="<%: this.ResolveClientUrl("../../App_Themes/CelloSkin/remove_but.gif") %>"
                        title="Delete" alt="Delete" />
                </a></span><span class="configure" onclick="savereportobject('<%:reportObjectId
        %>',true,true);"><a title="Save">
            <img src="../../App_Themes/CelloSkin/save.jpg" alt="Save" title="Save" />
        </a></span>
            <% if (reportObjectTypeId.Equals(ReportObjectTypeConstants.TableObjectType, StringComparison.InvariantCultureIgnoreCase))
               {%>
            <span class="configure"><a onclick="onFetchColumnsClick('<%:reportObjectId%>')" class="fetchtablecolumns"
                title="Column configure">
                <img src="../../App_Themes/CelloSkin/manage.gif" alt="Manage Source Fields" title="Manage Source Fields" />
            </a></span><span class="configure" style="float: right; margin-top: 0px;">
                <span>
                    <%: this.GetLocalResourceObject("RowsPerPage") %>
                </span>
                <%: Html.DropDownList("ReportObject_RowsPerPage", null, new { @class = "ReportObject_RowsPerPage" })%>
            </span>
            <% } %>
            <span class="configure" style="float: right; margin-top: 0px;"><span>
                <%: this.GetLocalResourceObject("Name").ToString()%></span> <span>
                    <%: Html.TextBox("Name",null,new{@class="Name"}) %>
                </span>
                </span>
        </h2>
        <div class="dragbox-content">
            <%
                if (ViewData["statusMessage"] != null)
                {
            %>
            <%: ViewData["statusMessage"].ToString() %>
            <%
                }
            %>
            <%
                switch (reportObjectTypeId.ToUpperInvariant())
                {
                    case ReportObjectTypeConstants.TableObjectType:
            %>
            <% Html.RenderAction("GetTableSources", "TableSources", new { viewObjectId = reportObjectId, tableSourceId = reportObject.SourceId }); %>
            <%
               break;
                    case ReportObjectTypeConstants.ChartObjectType:
            %>
            <table style="width: 100%">
                <tr>
                    <td>
                        <%: Html.Label(this.GetLocalResourceObject("AvailableChartDetails").ToString())%>
                    </td>
                    <td>
                        <%: Html.DropDownList("ChartDataSources", null, new { @class = "ChartDataSources", @onchange = "OnChartObjectSourceChange('" + reportObjectId + "')" })%>
                    </td>
                </tr>
            </table>
            <%
break;
                    case ReportObjectTypeConstants.TextObjectType:
            %>
            <% Html.RenderAction("Index", "TextSource", new { viewObjectId = reportObjectId, textSourceId = reportObject.SourceId }); %>
            <%
               break;
                    default:
               break;
                }                
            %>
        </div>
    </div>
    <%
        if (reportObject != null && !string.IsNullOrEmpty(reportObject.ReportObjectId) || ViewData["reportObjectId"] != null)
        {
    %>
    <input type="hidden" class="Report_Object_Id" name="Report_Object_Id" value="<%:reportObject.ReportObjectId%>" />
    <%
        }
        else
        {
    %>
    <input type="hidden" class="Report_Object_Id" name="Report_Object_Id" />
    <%
        }
    %>
    <%
        if (reportObject != null && !string.IsNullOrEmpty(reportObject.SourceId))
        {
    %>
    <input type="hidden" class="R_SourceId" name="R_SourceId" value="<%:reportObject.SourceId %>" />
    <%
        }
        else
        {
    %>
    <input type="hidden" class="R_SourceId" name="R_SourceId" />
    <% } %>
    <input type="hidden" name="R_SourceObj_Name" class="R_SourceObj_Name" />
    <input type="hidden" name="R_SourceObj_Desc" class="R_SourceObj_Desc" />
    <input type="hidden" name="reportobjecttype" class="reportobjecttype" value="<%: reportObjectTypeId %>"
        name="<%:reportObjectTypeName %>" />
    <input type="hidden" name="reportObjectDataSrcId" class="reportObjectDataSrcId" />
</div>
<style type="text/css">
    .ReportObject_RowsPerPage
    {
        width: 50px;
    }
</style>
