﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="heading_container">
    <h1>
        <%: this.GetLocalResourceObject("Title") %>
    </h1>
    <div class="inner_hold">
        <div class="green_but">
            <a href="<%: Url.Action("Index","ReportBuilder") %>" title="Back"><span><%: this.GetGlobalResourceObject("General","Back") %></span>
            </a>
        </div>
        <%
            using (Html.BeginForm("ExportReportAsPdf", "ReportBuilder", FormMethod.Post, new { @id = "exportForm", @style = "float: left" }))
            {
        %>
        <div class="exportFormElements">
        </div>
        <div class="green_but">
            <a title="Back" onclick="ExportReport()"><span><%: this.GetLocalResourceObject("ExportAsPdf")%></span></a>
        </div>
        <%
            }
        %>
    </div>
</div>
<div>
    <%
        string reportId = string.Empty;

        if (ViewData["reportId"] != null)
        {
            reportId = ViewData["reportId"].ToString();
        }        
    %>
    <%: Html.Hidden("reportId",reportId) %>
</div>
<div class="reportData" style="clear: both">
    <table class="celloTable details_container_table">
        <thead>
            <tr>
                <th>
                    <span class="reportName"></span>
                </th>
            </tr>
        </thead>
    </table>
</div>
<div class="report_execution_warnings" style="display: none">
    <div class="error">
    </div>
</div>
<div class="reportContent celloTable" style="clear: both; overflow: scroll">
</div>
