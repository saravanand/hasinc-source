﻿<%@ Page Title="<%$Resources:PageTitle%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="clear: both">
    </div>
    <% Html.RenderPartial("ReportView"); %>
    <div class="reportParameters" style="display: none">
        <div>
            <div class="inner_hold">
                <div class="green_but">
                    <a class="updateDynamicVariableValues" onclick="updateDynamicVariableValues()" title="<%: this.GetLocalResourceObject("UpdateDynamicVarVal")%>">
                        <span>
                            <%: this.GetLocalResourceObject("Apply")%></span></a>
                </div>
            </div>
        </div>
        <div class="dynamicVariablesForm">
        </div>
        <div>
            <div class="inner_hold">
                <div class="green_but">
                    <a class="updateDynamicVariableValues" onclick="updateDynamicVariableValues()" title="<%: this.GetLocalResourceObject("UpdateDynamicVarVal")%>">
                        <span>
                            <%: this.GetLocalResourceObject("Apply")%></span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%:Url.Content("../../Content/ReportBuilder.css")%>" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript">
        /* Global Variables */
        var report = {};
        var _reportObjects = {};
        var reportIdentifier;
        var tableSource = "A0DFECED-33F3-4DE1-91C4-3203E5EB2194";
        var textSource = "AE02E1E0-F37A-41B9-8098-64C9E71360C1";
        var imageSource = "5A3B3969-F960-4B1F-B9DD-9DE2A776AEA5";
        var chartSource = "35F17E82-DC46-491F-A6E2-A874258DC135";

        $(function () {
            reportIdentifier = $("#reportId").val();

            preExecuteReport();
        });
    </script>
    <style type="text/css">
        .ui-dialog-titlebar-close
        {
            display: none;
        }
        .header
        {
            font-weight: bold;
            color: #5F92A7;
            text-decoration:underline;
        }
    </style>
    <script src="<%:Url.Content("../../Scripts/preexecutereport.js") %>" type="text/javascript"></script>
    <script src="<%:Url.Content("../../Scripts/reportviewer.js")%>" type="text/javascript"></script>
</asp:Content>
