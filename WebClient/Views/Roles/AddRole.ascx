<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.AccessControlManagement.Role>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "AddRole";
   using (Ajax.BeginForm("AddRoleDetails", "Roles", new { }, ajaxOption, new { id = "AddRolePage", role = "form" }))
   {
%>
<div class="grid simple horizontal red">
    <div class="grid-title">
        <h4>
            <%: this.GetLocalResourceObject("AddRole")%></h4>
    </div>
    <div class="grid-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("RoleStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("RoleStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary()%>
        </div>
        <% } %>
        <div class="row form-container">
            <div class="col-md-12">
                <div class="form-group">
                    <label>
                        <%: this.GetLocalResourceObject("RoleName")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                    </label>
                    <%= Html.TextBox("RoleName", Model.RoleName, new { maxlength = 50 })%>
                </div>
                <div class="form-group">
                    <label>
                        <%: this.GetLocalResourceObject("Description")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                    </label>
                    <%= Html.TextArea("Description", Model.Description, new { onKeyDown = "limitText(this,1000)", onKeyUp = "limitText(this,1000)" })%>
                </div>
                <%
       if ((CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin)
           || CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ServiceAdmin)))
       {
                %>
                <div class="form-group">
                    <label>
                        <%: this.GetLocalResourceObject("GlobalRole")%>
                    </label>
                    <input type="checkbox" name="IsGlobalRole" style="margin: 0px; padding: 0px; width: 25px;"
                        checked="checked" onclick="javascript: ShowServiceList(this);" />
                </div>
                <%
       }
                %>
                <% if (ViewData["ServiceList"] != null
       && ((List<CelloSaaS.Model.LicenseManagement.Service>)ViewData["ServiceList"]).Count > 0)
                   {
                %>
                <div class="form-group">
                    <label>
                        <%: this.GetLocalResourceObject("Services")%></label>
                    <%Html.RenderPartial("AddServiceMapping"); %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <div class="grid-footer">
        <div class="row">
            <div class="col-md-3 col-md-offset-9">
                <div class="pull-right">
                    <%=Html.CelloButton("#", "<i class='fa fa-save'></i>&nbsp;" + this.GetGlobalResourceObject("General","Save").ToString(), new Dictionary<string,object>() { {"class","btn btn-success"} },null,null,"AddRoleDetails()") %>
                    <%=Html.CelloButton("#", this.GetGlobalResourceObject("General","Cancel"), null,null,null,"RoleCancel()") %>
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>
