<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script language="javascript">
        var masterListActionUrl = "RolePrivilegeList";
        var updateRolePrivilegeActionUrl = "UpdateRolePrivilege";
        var deleteRolePrivilegeActionUrl = "DeleteRolePrivilege";


        //This method is used get the role privilege details
        function loadMasterList(roleId, roleName) {
            $.ajaxSettings.cache = false;
            roleId = escape(roleId);
            roleName = escape(roleName);
            $.get(masterListActionUrl + "?strRoleId=" + roleId + "&roleName=" + roleName, masterLoad_callBack);
        }

        //This method is used to render the role privilege details in the privilege details div
        function masterLoad_callBack(data) {
            data = data + '<div id="PrivilegeDetails"></div>';
            $("#master").html(data);
            setDataScopeSelectedValue();
        }

        //This method is used to add role privilege details
        function AddRolePrivilegeDetails() {
            formDetails = $('#AddRolePrivilegePage');
            serializedForm = formDetails.serialize();
            $.post("AddRolePrivilege", serializedForm, detailsAdd_callBack);
        }

        //This method is used to render the data after added the role privilege details
        function detailsAdd_callBack(data) {
            if (data == "Success") {
                var roleId = document.getElementById('hfRoleId').value;
                var roleName = document.getElementById('hfRoleName').value;
                loadMasterList(roleId, roleName);
            }
            else {
                $("#PrivilegeDetails").html(data);
            }
        }

        //This method is used to update role privilege details
        function UpdateDetails(rolePrivilegeId, roleId, dataScopeId) {
            var ddl = $get('dataScope');
            var selectedOption = ddl.options[ddl.selectedIndex];
            var selectedValue = selectedOption.value;
            var roleName = document.getElementById('hfRoleName').value;
            $.get(updateRolePrivilegeActionUrl + "?roleId=" + roleId + "&mode=edit&rolePrivilegeId=" + rolePrivilegeId + "&dataScopeId=" + selectedValue + "&roleName=" + roleName, masterLoad_callBack);
        }

        //This method is used to edit role privilege details
        function EditDetails(rolePrivilegeId, roleId, dataScopeId) {
            //var roleId=document.getElementById('hfRoleId').value; 
            var roleName = document.getElementById('hfRoleName').value;
            $.get(masterListActionUrl + "?strRoleId=" + roleId + "&mode=edit&rolePrivilegeId=" + rolePrivilegeId + "&dataScopeId=" + dataScopeId + "&roleName=" + roleName, masterLoad_callBack);
        }

        //This method is used to delete role privilege details
        function DeleteDetails(rolePrivilegeId, roleId) {
            if (confirm('Are you sure you want to delete this role privilege?')) {
                //var roleId=document.getElementById('hfRoleId').value; 
                var roleName = document.getElementById('hfRoleName').value;
                $.get(deleteRolePrivilegeActionUrl + "?roleId=" + roleId + "&rolePrivilegeId=" + rolePrivilegeId + "&roleName=" + roleName, masterLoad_callBack);
            }
            else {
                return;
            }
        }

        function CancelUpdateDetails(roleId) {
            var roleName = document.getElementById('hfRoleName').value;
            loadMasterList(roleId, roleName);
        }

        //This method is used to set data scope value in the Drop down list
        function setDataScopeSelectedValue() {
            var ddl = $get('dataScope');
            if (ddl != null) {
                var dataScopeSelectedValue = document.getElementById('DataScopeSelectedValue').value;
                for (var i = 0; i < ddl.options.length; i++) {
                    if (ddl.options[i].value.trim() == dataScopeSelectedValue.trim()) {
                        ddl.options[i].selected = true;
                        break;
                    }
                }
            }
            document.getElementById('SearchString').focus();
        }
    
    </script>
    <style>
        #example
        {
            width: 100%;
        }
    </style>
    <div id="master">
        <% Html.RenderPartial("RolePrivilegeList"); %>
        <div id="PrivilegeDetails">
        </div>
    </div>
</asp:Content>
