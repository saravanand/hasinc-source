<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.AccessControlManagement.Role>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "AddRole";
   using (Ajax.BeginForm("RoleUpdate", "Roles", new { }, ajaxOption, new { id = "EditRolePage" }))
   {
%>
<div class="grid simple horizontal green">
    <div class="grid-title">
        <h4>
            <%: this.GetLocalResourceObject("EditRole")%></h4>
    </div>
    <div class="grid-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("RoleStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("RoleStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary()%>
        </div>
        <% } %>
        <div class="row form-container">
            <div class="col-md-12">
                <div class="form-group">
                    <label>
                        <%: this.GetLocalResourceObject("RoleName")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                    </label>
                    <%= Html.TextBox("RoleName", Model.RoleName, new { maxlength = 50 })%>
                    <%= Html.Hidden("RoleId",Model.RoleId)%>
                    <%= Html.Hidden("IsGlobal",Model.IsGlobal)%>
                    <%= Html.Hidden("Status",Model.Status)%>
                </div>
                <div class="form-group">
                    <label>
                        <%: this.GetLocalResourceObject("Description")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                    </label>
                    <%= Html.TextArea("Description", Model.Description, new { onKeyDown = "limitText(this,1000)", onKeyUp = "limitText(this,1000)" })%>
                </div>
                <% if (Model.IsGlobal && ViewData["ServiceList"] != null && ((List<CelloSaaS.Model.LicenseManagement.Service>)ViewData["ServiceList"]).Count > 0)
                   { %>
                <div class="form-group">
                    <label>
                        <%: this.GetLocalResourceObject("Services")%></label>
                    <% Html.RenderPartial("EditServiceMapping", "display: block;"); %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <div class="grid-footer">
        <div class="row">
            <div class="col-md-3 col-md-offset-9">
                <div class="pull-right">
                    <%=Html.CelloButton("#", "<i class='fa fa-save'></i>&nbsp;" + this.GetGlobalResourceObject("General","Save").ToString(), new Dictionary<string,object>() { {"class","btn btn-success"} },null,null,"UpdateRoleDetails()") %>
                    <%=Html.CelloButton("#", this.GetGlobalResourceObject("General","Cancel"), null,null,null,"RoleCancel()") %>
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>
