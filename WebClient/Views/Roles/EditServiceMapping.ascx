﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div style="<%: (string)Model %>;" id="serviceListTable">
    <% 
        string disableCheckbox = (ViewData["DisableCheckbox"] == null || !((string)ViewData["DisableCheckbox"]).Equals("true", StringComparison.InvariantCultureIgnoreCase)) ? "disabled=\"disabled\"" : string.Empty;
    %>
    <% foreach (var item in (List<CelloSaaS.Model.LicenseManagement.Service>)ViewData["ServiceList"])
       { %>
    <div style="float: left; width: 100px;">
        <% if (ViewData["RoleServices"] != null & ((string[])ViewData["RoleServices"]).Contains(item.ServiceCode))
           { %>
        <input type="checkbox" name="roleService" value="<%=item.ServiceCode %>" style="margin: 0px;
            padding: 0px; width: 25px;" checked="checked" <%=disableCheckbox %> />
        <%=item.ServiceName%>
        <% }
           else
           { %>
        <input type="checkbox" name="roleService" value="<%=item.ServiceCode %>" style="margin: 0px;
            padding: 0px; width: 25px;" <%=disableCheckbox %> />
        <%=item.ServiceName%>
        <% } %>
    </div>
    <% } %>
</div>
