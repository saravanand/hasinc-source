<%@ Page Title="<%$ Resources:ManagePrivilege %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('select').select2();

            chkPrivilegeChecked();

            $('select[name=AvailableModuleName]').change(function () {
                $('#AddRolePrivilegeCheckAll').removeAttr('checked');
                $('#RemoveRolePrivilegeCheckAll').removeAttr('checked');
                LoadFeatures();
                $('input[type=text][name=searchAvailablePrivileges]').val('');
            });

            $('select[name=AvailableFeatureName]').change(function () {
                $('#AddRolePrivilegeCheckAll').removeAttr('checked');
                $('#RemoveRolePrivilegeCheckAll').removeAttr('checked');
                LoadAvailablePrivileges();
                LoadAssignedPrivileges();
                $('input[type=text][name=searchAvailablePrivileges]').val('');
            });

            function LoadFeatures() {
                var selectList = $('select[name=AvailableFeatureName]');
                var url = '<%:Url.Action("GetFeaturesByModule","Roles") %>';
                var moduleId = $('select[name=AvailableModuleName]').val();
                var roleId = $('input[type=hidden][name=roleId]').val();

                $.post(url, { roleId: roleId, moduleId: moduleId }, function (data) {
                    var html = '<option value="-1">-All-</option>';
                    if (data.Error == undefined) {
                        for (var i = 0; i < data.length; ++i) {
                            html += '<option value="' + data[i].Value + '">' + data[i].Text + '</option>';
                        }
                    }
                    selectList.html(html);
                    selectList.trigger('change');//("liszt:updated");
                    LoadAvailablePrivileges();
                    LoadAssignedPrivileges();
                }, 'json');

            }

            $('select[name=AvailableModuleName]').trigger('change');

            $(document).on('keyup', 'input[type=text][name=searchAvailablePrivileges]', function () {
                var privilegeName = $(this).val().trim().toLowerCase();
                $('#AddRolePrivilegeCheckAll').removeAttr('checked');
                $('#RemoveRolePrivilegeCheckAll').removeAttr('checked');
                SearchPrivileges($('#availablePrivilegesList'), privilegeName);
                SearchPrivileges($('#assignedPrivilegesList'), privilegeName);
            });

            function SearchPrivileges(div, name) {
                $('#availablePrivilegesList div.alert,#assignedPrivilegesList div.alert,#availablePrivilegesList p,#assignedPrivilegesList p').remove();

                $('div.privilegeDiv b', div).each(function () {
                    var text = $(this).text().trim().toLowerCase();

                    if (name == '' || text.indexOf(name) != -1) {
                        $(this).parent().show();
                    } else {                        
                        $(this).parent().hide();
                    }
                });

                if (!$('#availablePrivilegesList div.privilegeDiv:visible').length) {
                    $('#availablePrivilegesList').append('<div class="alert alert-info"><%: this.GetLocalResourceObject("Nomatchfound") %></div>');
                }

                if (!$('#assignedPrivilegesList div.privilegeDiv:visible').length) {
                    $('#assignedPrivilegesList').append('<div class="alert alert-info"><%: this.GetLocalResourceObject("Nomatchfound") %></div>');
                }
            }
        });

        function ShowAjaxStatusMessage(message, success) {
            var div = $('#ajaxStatusMessage');
            div.hide();

            var html = '';
            if (success) {
                html += '<div class="alert alert-success">';
            } else {
                html += '<div class="alert alert-danger">';
            }

            html += '<ul><li>';
            html += message;
            html += '</li></ul>';
            html += '</div>';

            div.html(html);
            div.show();
        }

        function LoadAvailablePrivileges() {
            var url = '<%: Url.Action("GetAssignablePrivilegesForModule") %>';

            // get all available privileges for the module
            var moduleId = $('select[name=AvailableModuleName]').val();
            var featureId = $('select[name=AvailableFeatureName]').val();
            var roleId = $('input[type=hidden][name=roleId]').val();

            $.post(url, { roleId: roleId, moduleId: moduleId, featureId: featureId }, function (data) {
                $('#availablePrivilegesList').html(data);
            });
        }

        function LoadAssignedPrivileges() {
            var url = '<%: Url.Action("GetAssignedPrivilegesForModule") %>';

            // get assigned privileges for the module
            var moduleId = $('select[name=AvailableModuleName]').val();
            var featureId = $('select[name=AvailableFeatureName]').val();
            var roleId = $('input[type=hidden][name=roleId]').val();

            $.post(url, { roleId: roleId, moduleId: moduleId, featureId: featureId }, function (data) {
                $('#assignedPrivilegesList').html(data);
            });
        }

        function AddRolePrivilege() {
            var addUrl = '<%:Url.Action("AddPrivilegesToRole") %>';
            var roleId = $('input[type=hidden][name=roleId]').val();
            var privilegeIds = new Array();

            $('#AddRolePrivilegeCheckAll').removeAttr('checked');

            $('#RemoveRolePrivilegeCheckAll').attr('checked', false);
            // get selected privileges
            $('input[type=checkbox][name=chkPrivilege]', $('#availablePrivilegesList')).each(function () {
                if ($(this).is(':checked')) {
                    privilegeIds.push($(this).val());
                    $(this).parent('div').remove();
                }
            });

            var availablePrivs = $('#availablePrivilegesList div.privilegeDiv:visible').length;

            if (privilegeIds.length == 0) {
                alert(availablePrivs > 0 ? '<%: this.GetLocalResourceObject("selectaprivilege") %>' : '<%: this.GetLocalResourceObject("Noprivileges") %>');
                return;
            }

            // add them
            $.ajax({
                url: addUrl,
                data: JSON.stringify({ roleId: roleId, privilegeIds: privilegeIds }),
                dataType: 'json',
                contentType: 'application/json',
                type: 'POST',
                success: function (data) {
                    if (data) {
                        //var obj = eval('(' + data + ')');
                        var obj = data;
                        if (obj.Error == undefined) {
                            ShowAjaxStatusMessage(obj.Success, true);
                            LoadAssignedPrivileges();
                        } else {
                            ShowAjaxStatusMessage(obj.Error, false);
                        }
                    } else {
                        ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("roleprivilege") %>', false);
            }
                },
                error: function (xhr) {
                    ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("roleprivilege") %>', false);
                }
            });
                return false;
            }

            function chkPrivilegeChecked() {

                if ($('#availablePrivilegesList>div').size() > 0) {
                    if (($('#availablePrivilegesList input:checkbox').size()) === ($('#availablePrivilegesList input:checkbox:checked').size()) && ($('#availablePrivilegesList input:checkbox:checked').size() > 0)) {
                        $('#AddRolePrivilegeCheckAll').attr('checked', true);
                    }
                    else {
                        $('#AddRolePrivilegeCheckAll').attr('checked', false);
                    }
                }
                else {
                    $('#AddRolePrivilegeCheckAll').attr('checked', false);
                }


                if ($('#assignedPrivilegesList>div').size() > 0) {

                    if (($('#assignedPrivilegesList input:checkbox').size()) === ($('#assignedPrivilegesList input:checkbox:checked').size()) && ($('#assignedPrivilegesList input:checkbox:checked').size() > 0)) {
                        $('#RemoveRolePrivilegeCheckAll').attr('checked', true);
                    }
                    else {
                        $('#RemoveRolePrivilegeCheckAll').attr('checked', false);
                    }
                }
                else {
                    $('#RemoveRolePrivilegeCheckAll').attr('checked', false);
                }

            }

            function RolePrivilegeChecked(CheckBoxId, DivId) {
                $("#" + DivId + " :checkbox:visible").prop('checked', $('#' + CheckBoxId).is(':checked'));
            }

            function RemoveRolePrivilege() {
                var removeUrl = '<%:Url.Action("DeletePrivilegesFromRole") %>';
                var roleId = $('input[type=hidden][name=roleId]').val();
                var privilegeIds = new Array();

                $('#RemoveRolePrivilegeCheckAll').removeAttr('checked');
                $('#AddRolePrivilegeCheckAll').attr('checked', false);

                // get selected privileges
                $('input[type=checkbox][name=chkPrivilege]', $('#assignedPrivilegesList')).each(function () {
                    if ($(this).is(':checked')) {
                        privilegeIds.push($(this).val());
                        $(this).parent('div').remove();
                    }
                });

                var assignedPrivs = $('#assignedPrivilegesList div.privilegeDiv:visible').length;

                if (privilegeIds.length == 0) {
                    alert(assignedPrivs > 0 ? '<%: this.GetLocalResourceObject("selectaprivilege") %>' : '<%: this.GetLocalResourceObject("Noprivilegestoremove") %>');
                    return;
                }

                // remove them
                $.ajax({
                    url: removeUrl,
                    data: JSON.stringify({ roleId: roleId, privilegeIds: privilegeIds }),
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data) {
                            //var obj = eval('(' + data + ')');
                            var obj = data;
                            if (obj.Error == undefined) {
                                ShowAjaxStatusMessage(obj.Success, true);
                                LoadAvailablePrivileges();
                            } else {
                                ShowAjaxStatusMessage(obj.Error, false);
                            }
                        }
                    },
                    error: function (xhr) {
                        ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("removingtheroleprivilegedetails") %>', false);
                    }
                });
                    return false;
                }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="<%=Url.Action("RoleDetailsList") %>" title="<%: this.GetLocalResourceObject("GoBacktoRoleList")%>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: this.GetLocalResourceObject("ManagePrivileges")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <div id="master">
            <% Html.RenderPartial("PartialMangePrivilege"); %>
        </div>
    </div>
</asp:Content>
