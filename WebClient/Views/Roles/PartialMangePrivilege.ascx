﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div id="FieldPrivilegeList">
    <% AjaxOptions ajaxOption = new AjaxOptions();
       ajaxOption.UpdateTargetId = "PrivilegeDetails";
       using (Ajax.BeginForm("ManagePrivilege", "Role", new { }, ajaxOption, new { id = "PrivilegePage" }))
       {
    %>
    <div id="ajaxStatusMessage" class="successMessage">
    </div>
    <div class="row-fluid">
        <div class="form-container">
            <div class="grid simple">
                <div class="grid-title">
                    <h4><%: this.GetLocalResourceObject("RoleName") %> - <span class="semi-bold"><%: ViewData["RoleName"]%></span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <input type="hidden" name="roleId" value="<%:ViewData["RoleId"] %>" />
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("Module")%>
                                </label>
                                <% if (ViewData["Modules"] != null)
                                   { %>
                                <%= Html.DropDownList("AvailableModuleName", (IEnumerable<SelectListItem>)ViewData["Modules"], new { style="width:100%" })%>
                                <% } %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("Features")%>
                                </label>
                                <select name="AvailableFeatureName" style="width: 100%;">
                                    <option value="-1">-All-</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <%: this.GetLocalResourceObject("SearchPrivileges")%>
                                </label>
                                <input type="text" name="searchAvailablePrivileges" value="" placeholder="Enter keyword to search..." />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="col-md-6 pd-0">
            <div class="grid simple vertical red">
                <div class="grid-title">
                    <div class="row">
                        <div class="col-md-4" style="white-space: nowrap;">
                            <h4>
                                <%: this.GetLocalResourceObject("AvailablePrivileges")%></h4>
                        </div>
                        <div class="col-md-5">
                            <div class="pull-right" style="padding: 8px 0px;">
                                <input type="checkbox" id="AddRolePrivilegeCheckAll" onclick="RolePrivilegeChecked(this.id, 'availablePrivilegesList');" />
                                <%: this.GetLocalResourceObject("SelectAll")%>
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="pull-right">
                                <%=Html.CelloButton("#", "<i class='fa fa-plus'></i> " + this.GetGlobalResourceObject("General", "Add"), new Dictionary<string, object> { { "class", "btn btn-success" } }, PrivilegeConstants.AddRolePrivilege, null, "AddRolePrivilege();")%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-body" style="padding: 5px;">
                    <div id="availablePrivilegesList" style="height: 500px;" class="scrollable">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 pd-0">
            <div class="grid simple vertical green">
                <div class="grid-title">
                    <div class="row">
                        <div class="col-md-4" style="white-space: nowrap;">
                            <h4>
                                <%: this.GetLocalResourceObject("AssignedPrivileges")%></h4>
                        </div>
                        <div class="col-md-5">
                            <div class="pull-right" style="padding: 8px 0px;">
                                <input type="checkbox" id="RemoveRolePrivilegeCheckAll" onclick="RolePrivilegeChecked(this.id, 'assignedPrivilegesList');" />
                                <%: this.GetLocalResourceObject("SelectAll")%>
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="pull-right">
                                <%=Html.CelloButton("#","<i class='fa fa-minus'></i> " + this.GetGlobalResourceObject("General","Remove"),new Dictionary<string, object> { { "class", "btn btn-warning" } },PrivilegeConstants.DeleteRolePrivilege,null,"RemoveRolePrivilege();") %>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-body" style="padding: 5px;">
                    <div id="assignedPrivilegesList" style="height: 500px;" class="scrollable">
                    </div>
                </div>
            </div>
        </div>
        <%
       }
        %>
    </div>
</div>
