<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div id="RolePrivilegeList">
    <table  class="details">
        <thead>
            <tr>
                <th>
                    <%: this.GetLocalResourceObject("PrivilegeDetails")%>
                </th>
            </tr>
        </thead>
        <tr>
            <td>
                <% AjaxOptions ajaxOption = new AjaxOptions { UpdateTargetId = "PrivilegeDetails" };
                   using (Ajax.BeginForm("AddRolePrivilege", "Roles", new { }, ajaxOption, new { id = "AddRolePrivilegePage" }))
                   {
                %>
                <% if (ViewData["NewPrivileges"] != null && ((IEnumerable<CelloSaaS.Model.AccessControlManagement.Privilege>)ViewData["NewPrivileges"]).Count() > 0)
                   {%>
                <fieldset>
                    <legend>
                        <%: this.GetLocalResourceObject("AddPrivilege")%></legend>
                    <table>
                        <tr>
                            <td colspan="3">
                                <%= Html.CelloValidationMessage("AddRolePrivilegeStatusMessage")%>
                            </td>
                            <td>
                                <% if (ViewData["RoleName"] != null && ViewData["RoleId"] != null)
                                   { %>
                                <%= Html.Hidden("RoleName", ViewData["RoleName"])%>
                                <%= Html.Hidden("RoleId", ViewData["RoleId"])%>
                                <%= Html.Hidden("SearchString", ViewData["SearchString"])%>
                                <%} %>
                            </td>
                        </tr>
                        <% foreach (CelloSaaS.Model.AccessControlManagement.Privilege privilege in ((IEnumerable<CelloSaaS.Model.AccessControlManagement.Privilege>)ViewData["NewPrivileges"]))
                           { %>
                        <tr>
                            <td width="40%">
                                <%= privilege.Name%>
                                :
                                <br />
                                <%= privilege.Description%>
                            </td>
                            <td>
                                <%= Html.DropDownList("DataScope_" + privilege.Id, (SelectList)ViewData["DataScope_" + privilege.Id], "- Select Data Scope -")%>
                                <%= Html.CelloValidationMessage("valDataScope_" + privilege.Id, "*")%>
                            </td>
                            <td>
                                <%if (ViewData["ListPrivilege"] != null && ((List<string>)ViewData["ListPrivilege"]).Count > 0 && ((List<string>)ViewData["ListPrivilege"]).Contains(privilege.Id))
                                  {
                                %>
                                <input type="checkbox" name="privilege" checked="checked" value="<%= privilege.Id %>" />
                                <%}
                                  else
                                  {%>
                                <input type="checkbox" name="privilege" value="<%= privilege.Id %>" />
                                <%} %>
                            </td>
                        </tr>
                        <%}%>
                        <tr>
                            <td colspan="3" align="center">
                                <input type="button" value="<%=this.GetGlobalResourceObject("General","Add") %>" id="Add" onclick="javascript:AddRolePrivilegeDetails()" />
                                <%-- <input type="submit" name="btnNavigate" value="Cancel" />--%>
                                <input type="button" value="<%=this.GetGlobalResourceObject("General","Cancel") %>" id="Cancel" onclick="javascript:loadMasterList('<%=ViewData["RoleId"]%>','<%=ViewData["RoleName"] %>')" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <%}
                   else
                   {%>
                <fieldset>
                    <legend>
                        <asp:Literal ID="litAddPrivileges" runat="server" Text="<%$ Resources:AddPrivilege%>" /></legend>
                    <table>
                        <tr>
                            <td>
                                <asp:Literal ID="litNoPrivilegesAvailable" runat="server" Text="<%$ Resources:NoPrivilegeAvailable%>" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <%} %>
                <%} %>
            </td>
        </tr>
    </table>
</div>
