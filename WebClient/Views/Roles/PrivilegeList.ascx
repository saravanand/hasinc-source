﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="row">
    <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
       { %>
    <div class="alert alert-danger mg-25">
        <%= Html.CelloValidationMessage("Error")%>
    </div>
    <%}
       else
       { %>
    <%
       if (ViewData["PrivilegeList"] != null
           && ((Dictionary<string, Privilege>)ViewData["PrivilegeList"]).Count > 0)
       {
           var privileges = (Dictionary<string, Privilege>)ViewData["PrivilegeList"];
           foreach (var item in privileges.OrderBy(x => x.Key))
           { 
    %>
    <div class="col-md-5 mg-5 privilegeDiv">
        <input onclick="chkPrivilegeChecked()" class="chkPrivilegeClass" type="checkbox"
            style="width: 15px;" name="chkPrivilege" value="<%:item.Key %>" />
        <b>
            <%:item.Value.Name %></b><br />
        <%: this.GetLocalResourceObject("Description") %>: <i>
            <%:item.Value.Description%></i>
    </div>
    <% } %>
    <% }
       else
       { %>
    <div class="alert alert-info mg-25">
        <%: this.GetLocalResourceObject("NotAvailable") %>
    </div>
    <% } %>
    <% } %>
</div>
