<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();
            $('#roleTableSearchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        }

        function DoSearch() {
            var searchText = $('#roleTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }

        function ShowServiceList(obj) {
            if ($(obj).is(':checked')) {
                $('#serviceListTable').fadeIn();
            } else {
                $('#serviceListTable').fadeOut();
            }
        }

    </script>
    <script type="text/javascript">
        var roleListActionUrl = "RoleDetails";
        var addRoleDetails = "AddRoleDetails";
        var editRoleDetails = "RoleUpdate";

        function AddRoleDetails() {
            formDetails = $('#AddRolePage');
            serializedForm = formDetails.serialize();
            $.post(addRoleDetails, serializedForm, addRole_callBack);
            return false;
        }

        function UpdateRoleDetails() {
            formDetails = $('#EditRolePage');
            serializedForm = formDetails.serialize();
            $.post(editRoleDetails, serializedForm, addRole_callBack);
            return false;
        }

        function EditRoleDetails(roleId) {
            $.get(editRoleDetails + "?roleId=" + roleId, loadAddRole);
            $('#statusMessage').fadeOut();
            $('.alert').remove();
            return false;
        }

        function DeleteRoleDetails(roleId) {
            if (confirm('<%: this.GetLocalResourceObject("deactivate") %>')) {
                $('#EditRolePage').hide();
                $.get("DeactivateRole?roleId=" + roleId, roleList_callBack);
                $('#statusMessage').fadeOut();
                $('.alert').remove();
            }
            return false;
        }

        function ActivateRoleDetails(roleId) {
            if (confirm('<%: this.GetLocalResourceObject("activate") %>')) {
                $('#EditRolePage').hide();
                $.get("ActivateRole?roleId=" + roleId, roleList_callBack);
                $('#statusMessage').fadeOut();
                $('.alert').remove();
            }
            return false;
        }

        function RoleCancel() {
            $("#AddRole").fadeOut();
            return false;
        }

        function loadRoleList() {
            $.ajaxSettings.cache = false;
            $.get(roleListActionUrl, roleList_callBack);
        }

        function AddRole() {
            $.get(addRoleDetails, loadAddRole);
            $('#statusMessage').fadeOut();
            $('.alert').remove();
            return false;
        }

        function loadAddRole(data) {
            $("#AddRole").html(data).fadeIn();
        }

        //This method is used to render the data after added the role details
        function addRole_callBack(data) {
            if (data == "Success") {
                loadRoleList();
                $('#statusMessage').html('<div class="alert alert-success"><%: this.GetLocalResourceObject("success") %></div>').fadeIn();
                $("#AddRole").fadeOut();
            }
            else {
                $("#AddRole").html(data);
                $("#AddRole").fadeIn();
            }
        }

        //This method is used to render the role details in the role List div
        function roleList_callBack(data) {
            $("#roleDetails").html(data);
            jQDataTable();
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% 
        bool canAddRole = UserIdentity.HasPrivilege(PrivilegeConstants.AddRole);
        bool canEditRole = UserIdentity.HasPrivilege(PrivilegeConstants.UpdateRole); 
    %>
    <div id="loading"></div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("RoleManagement")%></h3>
    </div>
    <div class="row-fluid pd-25">
        <div id="statusMessage">
        </div>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input id="roleTableSearchText" name="roleTableSearchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 m-b-xs pull-right">
                        <% if (canAddRole)
                           { %>
                        <%=Html.CelloButton(Url.Action("CopyAllRolesFromParent","Roles"), this.GetLocalResourceObject("CopyAllRoles"), null, null, "CanCopyFromParent") %>
                        <div class="pull-right">
                            <%=Html.CelloButton("#", "<i class=\"fa fa-plus\"></i> "+this.GetGlobalResourceObject("General","Create").ToString(), new Dictionary<string,object>() { {"class","btn btn-success"} },null,null,"AddRole();") %>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
            <div id="roleDetails" class="grid-body">
                <% Html.RenderPartial("RoleList"); %>
            </div>
        </div>
        <% if (canAddRole || canEditRole)
           { %>
        <div id="AddRole" class="row-fluid" style="display: none;">
            <% if (canAddRole)
               { %>
            <% Html.RenderAction("AddRoleDetails"); %>
            <% }
               else if (canEditRole)
               { %>
            <div class="alert alert-info"><%:this.GetLocalResourceObject("i_Clickonaroletoedit") %></div>
            <% } %>
        </div>
        <% } %>
    </div>
</asp:Content>
