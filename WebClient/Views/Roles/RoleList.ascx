<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.AccessControlManagement.Role>" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    var editAccess = true || TenantAccessProxy.CheckTenantAccess(new Role
    ().EntityIdentifier, TenantContext.GetTenantId(new Role().EntityIdentifier), CelloSaaS.Model.FetchType.Edit, TenantContext.GetTenantId(new Role().EntityIdentifier));
    var deleteAccess = true || TenantAccessProxy.CheckTenantAccess(new Role().EntityIdentifier, TenantContext.GetTenantId(new Role().EntityIdentifier), CelloSaaS.Model.FetchType.Delete, TenantContext.GetTenantId(new Role().EntityIdentifier));
%>
<%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("RoleListMessage")))
  { %>
<div class="alert alert-success">
    <%=Html.CelloValidationMessage("RoleListMessage")%>
</div>
<%} %>
<% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("RoleStatusMessage")))
   { %>
<div class="alert alert-danger">
    <%=Html.CelloValidationMessage("RoleStatusMessage")%>
</div>
<%} %>
<% if (ViewData["RoleDetails"] == null)
   { %>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("RolesNotAvailable")%>
</div>
<% }
   else
   { %>
<%
       if (ViewData["RoleDetails"] as IEnumerable<Role> != null && (ViewData["RoleDetails"] as IEnumerable<Role>).Count() > 0)
       {
           Html.Grid(ViewData["RoleDetails"] as IEnumerable<Role>).Columns(
         column =>
         {
             column.For(col => string.Format("<span title=\"{0}\">{1}</span>", col.Description, col.RoleName))
                 .Named(this.GetLocalResourceObject("Roles").ToString())
                .Attributes(style => "").DoNotEncode();

             /*column.For(col => string.Format("<span title=\"{0}\">{1}</span>", col.Description, col.Description.Length > 50 ? col.Description.Substring(0, 50) + "..." : col.Description))
                 .Named(this.GetLocalResourceObject("Description").ToString())
                 .Attributes(style => "min-width:200px;white-space: -moz-pre-wrap !important; ")
                 .DoNotEncode();
             */

             if (CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.UpdateRole) && editAccess)
             {
                 column.For(col => (col.RoleId.StartsWith("GR$") && !CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.IsProductAdminTenant) || col.Status.Equals(false) ? "--" : "<a id='Edit' href='#' title='" + string.Format(this.GetLocalResourceObject("Edit").ToString(), col.RoleName) + "' onclick =EditRoleDetails('" + col.RoleId.Trim() + "')><i class='fa fa-edit'></i></a>")
                     .Named(this.GetLocalResourceObject("Edit1").ToString())
                     .Attributes(@class => "halign", style => "width:80px;")
                     .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
             }

             if (CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.DeleteRole) && deleteAccess)
             {
                 column.For(col => col.RoleId.StartsWith("GR$") ? "---" : col.Status.Equals(true) ? "<a id='Deactivate' href='#' title='" + string.Format(this.GetLocalResourceObject("Deactivate").ToString(), col.RoleName) + "' onclick =DeleteRoleDetails('" + col.RoleId.Trim() + "')><i class='fa fa-times-circle'></i></a>"
                     : "<a id='Activate' href='#' title='" + string.Format(this.GetLocalResourceObject("Activate").ToString(), col.RoleName) + "' onclick = ActivateRoleDetails('" + col.RoleId.Trim() + "')><i class='fa fa-plus-circle'></i></a>")
                     .Named(this.GetLocalResourceObject("ActivateorDeactivate").ToString())
                     .Attributes(@class => "halign", style => "width:140px;")
                     .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
             }

             if ((CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.AddRolePrivilege)
                 || CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.UpdateRolePrivilege)))
             {
                 column.For(col => col.Status.Equals(false) ? "---" : Html.CelloActionLink("Manage Privilege", "ManagePrivilege", new { roleId = col.RoleId, moduleId = string.Empty }).Replace("Manage Privilege", "<i class='fa fa-pencil'></i>"))
                     .Named(this.GetLocalResourceObject("ManagePrivilige").ToString())
                     .Attributes(@class => "halign", style => "width:150px;")
                     .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
             }

             if ((CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.AddRolePrivilege)
                 || CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.UpdateRolePrivilege)))
             {
                 column.For(col => col.Status.Equals(false) ? "---" :
                     Html.CelloActionLink("DatascopePrivilege", "ManagePrivilege", "DataScopeRoles", null, null, null, new { roleId = col.RoleId, moduleId = string.Empty }, null)
                     .Replace("DatascopePrivilege", "<i class='fa fa-pencil'></i>"))
                     .Named(this.GetLocalResourceObject("DataScopePrivilege").ToString())
                     .Attributes(@class => "halign", style => "width: 175px;")
                     .HeaderAttributes(@class => "halign noSortCol").DoNotEncode();
             }

         }).Attributes(id => "dataList", @class => "celloTable").Render();
       }
       else
       {
%>
<div class="alert alert-info">
    <%=this.GetGlobalResourceObject("General", "m_NoData")%>
</div>
<% } %>
<% } %>
