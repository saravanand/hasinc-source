﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Rules.Core" %>
<div class="clear">
</div>
<input type="hidden" name="RulesetCode" value="<%:ViewData["rulesetCode"] %>" />
<input type="hidden" name="ActivityType" value="<%:ViewData["activityType"] %>" />
<% 
    ActivityType activityType = (ActivityType)ViewData["activityType"];
    if (activityType == ActivityType.EntityRuleActivity)
    { %>
<h4>
    <%:this.GetLocalResourceObject("lbl_EntityRuleActivity")%></h4>
<br />
<div class="form-group">
    <label class="mandatory">
        <%:this.GetLocalResourceObject("lbl_EntityAssemblyTypeName")%>
    </label>
    <input type="text" name="AssemblyType" />
    <br />
    <span class="text-info">
        <i class="fa fa-info-circle"></i>&nbsp;<%:this.GetLocalResourceObject("lbl_eg")%>
    </span>
</div>
<% }
    else if (activityType == ActivityType.ServiceCallRuleActivity)
    { %>
<h4>
    <%:this.GetLocalResourceObject("lbl_WebServiceCallRuleActivity")%> </h4>
<br />
<div class="form-group">
    <label class="mandatory">
        <%:this.GetLocalResourceObject("lbl_ServiceEndPoint")%>
    </label>
    <%=Html.DropDownList("ServiceEndpointId")%>
</div>
<% }
    else if (activityType == ActivityType.XMLRuleActivity)
    { %>
<h4>
    <%:this.GetLocalResourceObject("lbl_XMLRuleActivity")%> </h4>
<br />
<div class="form-group">
    <label class="mandatory">
        <%:this.GetLocalResourceObject("lbl_XmlStructure")%></label>
    <textarea id="ServiceOutputXML" name="ServiceOutputXML" cols="50" rows="15"></textarea>
</div>
<script type="text/javascript">
    $(function () {
        window.editor = CodeMirror.fromTextArea(document.getElementById("ServiceOutputXML"), {
            mode: { name: "xml", alignCDATA: true },
            lineNumbers: true,
            lineWrapping: true,
            matchBrackets: true,
            autoCloseTags: true
        });

        window.editor.on("cursorActivity", function () {
            window.editor.matchHighlight("CodeMirror-matchhighlight");
        });
    });
</script>
<% }
    else if (activityType == ActivityType.OpenRuleActivity)
    { %>
<h4>
    <%:this.GetLocalResourceObject("lbl_OpenRuleActivity")%> </h4>
<br />
<div class="form-group">
    <label class="mandatory">
        <%:this.GetLocalResourceObject("lbl_RuleActivityAssemblyTypeName")%></label>
    <input type="text" name="AssemblyType" />
    <br />
    <span class="text-info">
        <i class="fa fa-info-circle"></i>&nbsp;<%:this.GetLocalResourceObject("lbl_egCrm")%>
    </span>
</div>
<% } %>
