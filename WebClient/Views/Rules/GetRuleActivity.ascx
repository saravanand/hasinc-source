﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Integration" %>
<%@ Import Namespace="CelloSaaS.Rules.Core" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%  var ruleMetadata = ViewData["RuleMetadata"] as RuleSetMetadata; %>
<% //bool canAddActivity = !ruleMetadata.IsFixed && UserIdentity.HasPrivilege(PrivilegeConstants.SaveRule);
    bool canAddActivity = UserIdentity.HasPrivilege(PrivilegeConstants.SaveRule);
    if (string.IsNullOrEmpty(ruleMetadata.TenantId))
    {
        //canAddActivity = canAddActivity && UserIdentity.IsInRole(RoleConstants.ProductAdmin);
    }
%>
<% if (canAddActivity)
   { %>
<div class="row">
    <div class="col-md-6 pull-right">
        <div class="pull-right">
            <button class="btn btn-default" type="button" onclick="return AddRuleActivity('<%:ViewData["rulesetCode"] %>','EntityRuleActivity');" title="Add Entity Rule Activity">
                <%:this.GetLocalResourceObject("lbl_EntityActivity")%>
            </button>
            <%-- <button class="btn btn-default" type="button" onclick="return AddRuleActivity('<%:ViewData["rulesetCode"] %>','ServiceCallRuleActivity');"
            title="Add Service Call Activity">Service Call Activity</button> --%>
            <button class="btn btn-default" type="button" onclick="return AddRuleActivity('<%:ViewData["rulesetCode"] %>','OpenRuleActivity');" title="Add Open Rule Activity">
                <%:this.GetLocalResourceObject("lbl_OpenRuleActivity")%>
            </button>
            <button class="btn btn-default" type="button" onclick="return AddRuleActivity('<%:ViewData["rulesetCode"] %>','XMLRuleActivity');" title="Add XML Rule Activity">
                <%:this.GetLocalResourceObject("lbl_XMLRuleActivity")%>
            </button>
        </div>
    </div>
</div>
<% } %>
<div class="row form-container">
    <%if (ViewData["RuleActivityDefs"] != null && (ViewData["RuleActivityDefs"] as Dictionary<string, RuleActivityDefinition>).Count > 0)
      {
          var activities = ViewData["RuleActivityDefs"] as Dictionary<string, RuleActivityDefinition>;
          var endpoints = (ViewData["ServiceEndpoints"] as Dictionary<string, CelloServiceEndpoint>) ?? new Dictionary<string, CelloServiceEndpoint>();
          foreach (var x in activities.Values)
          {
    %>
    <div class="col-md-12">
        <h4><%:x.ActivityType.ToString() %>
            <% if (!x.IsFixed && canAddActivity)
               { %>
            <small>
                <a class="del" href="<%=Url.Action("DeleteRuleActivity",new { rulesetCode = x.RulesetCode, activityId = x.Id }) %>" title='Remove this activity!'><i class='fa fa-trash-o'></i></a>
            </small>
            <% } %>
        </h4>
        <% if (!string.IsNullOrEmpty(x.AssemblyType) && x.AssemblyType.Split(',').Length == 2)
           { %>
        <label>
            <%:this.GetLocalResourceObject("lbl_TypeName")%></label>
        <p>
            <%:x.AssemblyType.Split(',')[0]%>
        </p>
        <label>
            <%:this.GetLocalResourceObject("lbl_AssemblyName")%>
        </label>
        <p>
            <%:x.AssemblyType.Split(',')[1]%>
        </p>
        <% }
           else if (!string.IsNullOrEmpty(x.ServiceEndpointId) && endpoints.ContainsKey(x.ServiceEndpointId))
           { %>
        <label>
            <%:this.GetLocalResourceObject("lbl_ServiceEndpointName")%></label>
        <p>
            <%:endpoints[x.ServiceEndpointId].Name%>
        </p>
        <% }
           else if (!string.IsNullOrEmpty(x.ServiceOutputXML))
           { %>
        <label>
            <%:this.GetLocalResourceObject("lbl_XMLStructure")%>
        </label>
        <textarea rows="1" cols="1" class="bxml" style="display: none;"><%=(System.Xml.Linq.XElement.Parse(x.ServiceOutputXML).ToString()) %></textarea>
        <pre class="xmlOutput cm-s-default">
        </pre>
        <% } %>
        <hr />
    </div>
    <%
          }
      }
      else
      { %>
    <div class="alert alert-info">
        <%:this.GetGlobalResourceObject("General","m_NoData") %>
    </div>
    <% } %>
</div>
<script type="text/javascript">
    $(function () {
        $('.bxml').each(function () {
            var editor = CodeMirror.runMode($(this).val(), "application/xml", $(this).next()[0]);
        });

        $('a.del').click(function () {
            if (!confirm('<%:this.GetLocalResourceObject("q_RemoveRule")%>')) {
                return false;
            }

            $.post($(this).attr('href'), null, function (data) {
                if (data && !data.Error) {
                    ShowSuccess('<%:this.GetLocalResourceObject("s_RemoveRuleActivity")%>');
                    LoadRuleActivity($('#rulesetCode').val());
                } else {
                    ShowError(data.Error || '<%:this.GetLocalResourceObject("e_RemoveRuleActivity")%>');
                }
            });

            return false;
        });
    });
</script>
