﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Rules.Core" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%  var ruleMetadata = ViewData["RuleMetadata"] as RuleSetMetadata; %>
<% Html.RenderPartial("StatusMessage"); %>
<% if (ruleMetadata != null)
   {
       bool canSaveRule = UserIdentity.HasPrivilege(PrivilegeConstants.SaveRule);
       bool disabled = ruleMetadata.IsFixed || (string.IsNullOrEmpty(ruleMetadata.TenantId) && !UserIdentity.IsInRole(RoleConstants.ProductAdmin));
%>
<div class="form-container">
    <form name="rulemetadatafrom" id="rulemetadatafrom" method="post" action="<%=Url.Action("UpdateRuleset") %>">
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_RuleSetCode")%>
            </label>
            <br />
            <%:ruleMetadata.Code %>
            <input type="hidden" name="Id" id="pkid" value="<%:ruleMetadata.Id%>" />
            <input type="hidden" name="Code" id="rulesetCode" value="<%:ruleMetadata.Code%>" />
            <input type="hidden" name="TenantId" id="rule_tenantId" value="<%:ruleMetadata.TenantId%>" />
            <input type="hidden" name="Status" value="<%:ruleMetadata.Status%>" />
            <input type="hidden" name="ScreenType" value="<%:ruleMetadata.ScreenType.ToString()%>" />
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_RuleSetName")%></label>
            <input type="text" name="Name" value="<%:ruleMetadata.Name%>" <%: disabled ? "disabled=disabled" : "" %> />
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_Description")%></label>
            <textarea rows="3" cols="10" name="Description" <%: disabled ? "disabled=disabled" : "" %>><%:ruleMetadata.Description%></textarea>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_Category")%></label>
            <br />
            <%:ruleMetadata.Category ?? "Default"%>
            <input type="hidden" name="Category" value="<%:ruleMetadata.Category ?? "Default"%>" />
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_ScreenType")%>
            </label>
            <br />
            <%:ruleMetadata.ScreenType.ToString()%>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("lbl_IsGlobal")%>
            </label>
            <br />
            <%: string.IsNullOrEmpty(ruleMetadata.TenantId).ToString()%>
        </div>
    </form>
    <div class="actions">
        <% if (canSaveRule)
           { %>
        <% if (!disabled)
           { %>
        <a class="btn btn-success" href="#" id="btnSaveRulesetMetadata" title="<%:this.GetGlobalResourceObject("General","Save") %>">
            <span>
                <%:this.GetGlobalResourceObject("General", "Save")%></span></a>
        <% } %>
        <a class="btn btn-info" href="<%=Url.Action("Manage","Rules", new { ruleSetCode = ruleMetadata.Code, screenType = ruleMetadata.ScreenType.ToString() })%>">
            <span><%:this.GetLocalResourceObject("lbl_Configure")%> </span></a>
        <a class="btn btn-primary" href="<%=Url.Action("TestRule","Rules", new { ruleSetCode = ruleMetadata.Code, screenType = ruleMetadata.ScreenType.ToString() })%>">
            <span><%:this.GetLocalResourceObject("lbl_Test")%></span></a>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#btnSaveRulesetMetadata').click(function () {
            var ruleSetName = $('form#rulemetadatafrom input[name=Name]').val();

            if (!ruleSetName || ruleSetName.length < 1) {
                alert('<%:this.GetLocalResourceObject("e_RuleSetName")%>');
                return;
            }

            var postdata = $('form#rulemetadatafrom').serialize();

            $.post($('form#rulemetadatafrom').attr('action'), postdata, function (data) {
                if (data && !data.Error) {
                    $("#ruleListPanel").jstree('rename_node', null, ruleSetName);
                    ShowSuccess('<%:this.GetLocalResourceObject("s_UpdateRule")%>');
                    } else {
                        ShowError(data.Error || '<%:this.GetLocalResourceObject("e_UpdateRule")%>');
                    }
                });

            return false;
        });
    });
</script>
<% } %>
    