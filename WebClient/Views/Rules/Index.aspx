﻿<%@ Page Title="Manage Business Rules" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("h_ManageBussinessRules")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row">
            <div class="col-md-3">
                <div id="ruleActionPanel" class="grid simple">
                    <div class="grid-title">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="ruleTreeSearchbox" name="ruleTreeSearchbox" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                            </span>
                        </div>
                    </div>
                    <div class="grid-body" style="padding: 10px 0; overflow-x: auto; min-height: 300px;">
                        <div id="ruleListPanel">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 pd-0">
                <div id="statusMessage">
                </div>
                <section id="rightPanel" class="panel">
                    <header class="panel-heading bg-info">
                        <ul class="nav nav-tabs nav-justified captilize">
                            <li class="active"><a data-toggle="tab" href="#ruleConfigurePanel" title="<%:this.GetLocalResourceObject("lbl_RuleDetails")%>">
                                <%:this.GetLocalResourceObject("lbl_RuleDetails")%></a></li>
                            <li><a data-toggle="tab" href="#ruleActivityPanel" title="<%:this.GetLocalResourceObject("lbl_RuleActivities")%>">
                                <%:this.GetLocalResourceObject("lbl_RuleActivities")%></a></li>
                        </ul>
                    </header>
                    <div class="panel-body pd-0">
                        <div class="tab-content" style="overflow: visible;">
                            <div id="ruleConfigurePanel" class="tab-pane fade in active">
                                <div class="alert alert-info">
                                    <%:this.GetLocalResourceObject("i_SelectRule")%>
                                </div>
                            </div>
                            <div id="ruleActivityPanel" class="tab-pane fade">
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="modal fade" id="AddRuleset" tabindex="-1" role="dialog" aria-labelledby="AddRuleset" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">Create New Ruleset</h4>
                </div>
                <div class="modal-body">
                    <form id="addruleform" name="addruleform" action="<%=Url.Action("AddRuleset") %>">
                        <div class="form-container">
                            <div class="form-group">
                                <label class="mandatory">
                                    <%:this.GetLocalResourceObject("lbl_RuleSetCode")%>
                                </label>
                                <input type="text" name="Code" autocomplete="off" />
                            </div>
                            <div class="form-group">
                                <label class="mandatory">
                                    <%:this.GetLocalResourceObject("lbl_RuleSetName")%>
                                </label>
                                <input type="text" name="Name" autocomplete="off" />
                            </div>
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("lbl_Category")%>
                                </label>
                                <input type="text" name="Category" autocomplete="off" />
                            </div>
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("lbl_Description")%>
                                </label>
                                <textarea rows="5" cols="10" name="Description"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="mandatory">
                                    <%:this.GetLocalResourceObject("lbl_Screen Type")%></label>
                                <br />
                                <%:Html.DropDownList("ScreenType", typeof(CelloSaaS.Rules.Core.ScreenType).ToSelectList() ) %>
                            </div>
                            <%  
                                var tenantId = TenantContext.GetTenantId("_BusinessRule");
                                if (CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId.Equals(tenantId, StringComparison.InvariantCultureIgnoreCase))
                                { %>
                            <div class="form-group">
                                <label>
                                    <%:this.GetLocalResourceObject("lbl_IsGlobal")%>
                                </label>
                                <br />
                                <input type="checkbox" name="isGlobal" id="isGlobal" />
                            </div>
                            <% } %>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button type="button" class="btn btn-primary" id="btnAddRuleset"><%: this.GetGlobalResourceObject("General","Add") %> </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Close") %> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="AddRuleActivityDialog" tabindex="-1" role="dialog" aria-labelledby="AddRuleActivityDialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">Add Rule Activity</h4>
                </div>
                <div class="modal-body form-container">
                    <form action="<%=Url.Action("AddRuleActivity") %>" method="post" name="addruleactivityform" id="addruleactivityform">
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button type="button" class="btn btn-primary" id="btnAddRuleActivity"><%: this.GetGlobalResourceObject("General","Add") %> </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Close") %> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        li.addrule ins, li.deleterule ins {
            background-position: center bottom !important;
        }

        .stackbox {
            border: 1px solid #CCCCCC;
            margin: 10px auto;
            padding: 3px 5px;
            position: relative;
            width: 100%;
            display: inline-block;
        }

            .stackbox h4 {
                color: #5F92A7;
                margin-bottom: 0.5em;
                text-decoration: underline;
            }

            .stackbox label {
                clear: left;
                float: left;
                margin-right: 10px;
            }

            .stackbox > span {
                float: left;
            }

            .stackbox .actionlinks {
                position: absolute;
                right: 5px;
                top: 3px;
                width: auto;
            }
    </style>
    <script type="text/javascript" src="/Scripts/jquery.jstree.js"></script>
    <script type="text/javascript">
        function reload() {
            window.location.reload(true);
        }

        function DoSearch() {
            $("#ruleListPanel").jstree("deselect_all");
            var val = $('#ruleTreeSearchbox').val();

            if (val && val != '') {
                $("#ruleListPanel").jstree('search', val);
            } else {
                $("#ruleListPanel").jstree('clear_search');
            }

            return false;
        }

        function AddRuleActivity(rulesetCode, activityType) {
            $.get('<%=Url.Action("AddRuleActivity") %>', { rulesetCode: rulesetCode, activityType: activityType }, function (html) {
                if (html) {
                    $('#addruleactivityform').html(html);
                    $("#AddRuleActivityDialog").modal('show');
                }
            });
        }

        function LoadRuleActivity(rulesetCode) {
            if (rulesetCode) {
                $.get('<%=Url.Action("GetRuleActivity") %>', { rulesetCode: rulesetCode }, function (html) {
                    if (html) {
                        $("#ruleActivityPanel").html(html);
                    }
                });
            }
        }

        $(function () {
            $('#ruleTreeSearchbox').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });

            var selectNodes = [];
            var selectNodeId = '<%= (ViewData["SelectRuleSetCode"] == null ? "" : ViewData["SelectRuleSetCode"] )%>';

            if (selectNodeId && selectNodeId != '') {
                selectNodes.push(selectNodeId);
            }

            LoadRulesTreeView();

            function LoadRulesTreeView() {
                $("#ruleListPanel")
                .bind("loaded.jstree", function (event, data) {
                    $("#ruleListPanel").jstree("open_all", -1, true);
                    if (selectNodeId && selectNodeId != '' && $('#' + selectNodeId).length > 0) {
                        setTimeout(function () {
                            $("#ruleListPanel").jstree("deselect_all");
                            $("#ruleListPanel").jstree("select_node", $('#' + selectNodeId), true);
                        }, 500);
                    }
                })
                .bind("select_node.jstree", function (event, data) {
                    if (!data) return;
                    var el = data.rslt.obj;
                    if (el.hasClass('category') || el.hasClass('root')) {
                        // don't do anything
                    } else {
                        //load the rule config page
                        var categoryName = data.inst._get_parent(el).attr('id');
                        var rulesetCode = el.attr('id');

                        if (!rulesetCode)
                            return;

                        $.get('<%=Url.Action("GetRuleDefinition") %>', { categoryName: categoryName, rulesetCode: rulesetCode }, function (html) {
                            $("#ruleConfigurePanel").html(html);
                        });

                        LoadRuleActivity(rulesetCode);
                    }
                    $('#statusMessage').empty();
                })
	            .jstree({
	                core: {
	                    /* core options go here */
	                    "animation": 100,
	                    "initially_select": selectNodes
	                },
	                plugins: ["themes", "json_data", "search", "ui", "crrm", "contextmenu", "types", "cookies"],
	                "search": {
	                    "show_only_matches": true
	                },
	                "ui": {
	                    "select_limit": 1
	                },
	                "json_data": {
	                    "ajax": {
	                        "url": '<%=Url.Action("GetAllRuleMetadata") %>',
	                        "data": function (n) {
	                            return {
	                                "id": n.attr ? n.attr("id").replace("node_", "") : 1
	                            }
	                        }
	                    }
	                },
	                "contextmenu": {
	                    "select_node": false,
	                    "items": getContextMenu
	                },
	                "types": {
	                    "valid_children": ["root"],
	                    "type_attr": "rel",
	                    "types": {
	                        "root": {
	                            "valid_children": ["category"],
	                            "max_depth": -1,
	                            "hover_node": false,
	                            "select_node": function () { return false; }
	                        },
	                        "category": {
	                            "valid_children": ["default"],
	                            "max_depth": -1,
	                            "hover_node": false,
	                            "select_node": function () { return false; }
	                        },
	                        "default": {
	                            "valid_children": ["default"]
	                        }
	                    }
	                }
	            });
                //.one("reselect.jstree", function (e, data) { e.stopImmediatePropagation(); return false; });
            }

            function CreateNewRuleset(inst, el) {
                $('form#addruleform')[0].reset();

                var categoryName = el.attr('id');
                $('input[name=Category]').val(el.hasClass('root') ? '' : categoryName);

                $("#AddRuleset").modal('show');
            }

            function getContextMenu(el) {
                if (el.hasClass('category') || el.hasClass('root')) {
                    return {
                        "addrule": {
                            "label": "Add Ruleset",
                            "action": function (obj) { CreateNewRuleset(this, obj); },
                            "_disabled": false,
                            "_class": "addrule",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": "/Content/images/plus-ico.png"
                        }
                    };
                } else if (!el.hasClass('fixed')) {
                    return {
                        "none": {
                            "label": "Delete",
                            "action": function (obj) {
                                if (!confirm('<%:this.GetLocalResourceObject("q_DeleteRuleSet")%>')) {
                                    return;
                                }

                                var rulesetCode = obj.attr('id');

                                if (!rulesetCode)
                                    return;

                                var inst = $.jstree._reference('#ruleListPanel');

                                // delete this ruleset
                                $.post('<%=Url.Action("DeleteRuleset") %>', { rulesetCode: rulesetCode }, function (data) {
                                    if (data && !data.Error) {
                                        alert('<%:this.GetLocalResourceObject("s_DeleteRuleSet")%>');
                                        var parent = inst._get_parent(obj); // get category node
                                        inst.delete_node(obj);

                                        if (parent) {
                                            var childs = inst._get_children(parent); // get its child nodes

                                            // if there is no child node then delete it
                                            if (childs == false || (childs && childs.length == 0)) {
                                                inst.delete_node(parent);   // remove the category node
                                            }
                                        }
                                    } else {
                                        ShowError(data.Error || '<%:this.GetLocalResourceObject("e_DeleteRuleSet")%>');
                                    }
                                });
                            },
                            "_disabled": false,
                            "_class": "deleterule",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": "/Content/images/del-ico.png"
                        }
                    };
                }
        }

            $("#AddRuleset").modal({ show: false });
            $('#btnAddRuleset').click(function () {
                var ruleSetName = $('form#addruleform input[name=Name]').val();
                var ruleSetCode = $('form#addruleform input[name=Code]').val();

                if (!ruleSetName || ruleSetName.length < 1) {
                    alert('<%:this.GetLocalResourceObject("e_RuleSetName")%>');
                    return;
                }

                if (!ruleSetCode || ruleSetCode.length < 1) {
                    alert('<%:this.GetLocalResourceObject("e_RuleSetCode")%>');
                    return;
                }

                var postdata = $('form#addruleform').serialize();

                $.post($('form#addruleform').attr('action'), postdata, function (data) {
                    if (data && !data.Error) {
                        ShowSuccess('<%:this.GetLocalResourceObject("s_CreateRuleSet")%>');
                        $("#ruleListPanel").jstree('destroy');
                        LoadRulesTreeView();
                    } else {
                        ShowError(data.Error || '<%:this.GetLocalResourceObject("e_CreteRuleSet")%>');
                    }
                });

                $("#AddRuleset").modal('hide');
            });

            $("#AddRuleActivityDialog").modal({ show: false });
            $("#btnAddRuleActivity").click(function () {
                if (window.editor) {
                    window.editor.save();
                }

                var postdata = $('form#addruleactivityform').serialize();

                $.post($('form#addruleactivityform').attr('action'), postdata, function (data) {
                    if (data && !data.Error) {
                        ShowSuccess('<%:this.GetLocalResourceObject("s_AddRuleActivity")%>');
                        LoadRuleActivity($('#rulesetCode').val());   // reload rule activity div
                    } else {
                        ShowError(data.Error || '<%:this.GetLocalResourceObject("e_AddRuleActivity")%> ');
                    }
                });

                $("#AddRuleActivityDialog").modal('hide');
            });
        });
    </script>
    <script type="text/javascript" src="/bundles/codemirrorscripts"></script>
    <link rel="stylesheet" href="/Content/codemirror.css" type="text/css" />
</asp:Content>
