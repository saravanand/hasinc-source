﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        bool canCopy = ViewData["CanCopyFromParent"] != null ? (bool)ViewData["CanCopyFromParent"] : false;
    %>
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%: this.GetLocalResourceObject("Title") %></h3>
        <div class="pull-right">
            <a class="btn btn-default" href="<%=Url.Action("Index") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
                <span>
                    <%=this.GetGlobalResourceObject("General","Back") %></span></a>
            <% if (ViewData["RuleMetadata"] != null)
               { %>
            <a class="btn btn-danger" href="<%=Url.Action("Reset","Rules",new{ruleSetCode=ViewData["RuleName"],screenType=ViewData["ScreenType"]}) %>"
                title="<%=this.GetLocalResourceObject("ResetRuleset") %>"><span>
                    <%=this.GetLocalResourceObject("Reset") %></span></a>
            <a class="btn btn-primary" href="<%=Url.Action("TestRule","Rules", new { ruleSetCode = ViewData["RuleName"], screenType = ViewData["ScreenType"] })%>">
                <span>Test</span></a>
            <% if (canCopy)
               { %>
            <a class="btn btn-warning" href="<%:Url.Action("CopyRulesetFromParent","Rules", new{ruleSetCode=ViewData["RuleName"],screenType=ViewData["ScreenType"]}) %>"
                title="<%: this.GetLocalResourceObject("CopyFromParent")%>"><span>
                    <%: this.GetLocalResourceObject("CopyFromParent")%></span></a>
            <% } %>
            <% } %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <% if (ViewData["RuleMetadata"] != null)
           { %>
        <% Html.RenderPartial("BRuleControl"); %>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function reload() {
            window.location.reload(true);
        }

        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }
    </script>
</asp:Content>
