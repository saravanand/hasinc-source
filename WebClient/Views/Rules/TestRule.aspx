﻿<%@ Page Title="Test Business Rules" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="Go back to rule listing">
            <i class="icon-custom-left"></i>
        </a>
        <h3>Test Business Rule</h3>
        <div class="pull-right">
            <a class="btn btn-default" href="<%=Url.Action("Index") %>" title="Go back to rule listing"><span>
                <%=this.GetGlobalResourceObject("General","Back") %></span> </a>
            <a class="btn btn-success" href="#" onclick="$('form').submit()" title="Execute rule"><span>Execute</span>
            </a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <% using (Html.BeginForm())
           { %>
        <%=Html.Hidden("ruleSetCode", ViewData["ruleSetCode"])%>
        <div class="row">
            <div class="col-md-4">
                <section class="panel purple">
                    <header class="panel-heading">
                        <h4>Variable XML</h4>
                    </header>
                    <div class="panel-body" style="min-height:600px;">
                        <% if (ViewData["ruleVariableXml"] != null && !string.IsNullOrEmpty(ViewData["ruleVariableXml"].ToString()))
                           { %>
                        <textarea rows="1" cols="1" class="bxml" style="display: none;"><%=(System.Xml.Linq.XElement.Parse(ViewData["ruleVariableXml"].ToString()).ToString())%></textarea>
                        <pre class="outputxml cm-s-default" style="overflow: scroll; height: 100%;"></pre>
                        <% } %>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel indigo">
                    <header class="panel-heading">
                        <h4>Input XML</h4>
                    </header>
                    <div class="panel-body" style="min-height:600px;">
                        <%=Html.TextArea("inputXml", new { rows = "20", cols = "20", style = "width:100%;" })%>
                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="panel green">
                    <header class="panel-heading">
                        <h4>Output XML</h4>
                    </header>
                    <div class="panel-body" style="min-height:600px;">
                        <% if (ViewData["outputXml"] != null && !string.IsNullOrEmpty(ViewData["outputXml"].ToString()))
                           { %>
                        <div class="content-box" style="width: 90%; overflow: scroll; height: 100%;">
                            <textarea rows="1" cols="1" class="bxml" style="display: none;"><%=(System.Xml.Linq.XElement.Parse(ViewData["outputXml"].ToString()).ToString())%></textarea>
                            <pre class="outputxml cm-s-default"></pre>
                        </div>
                        <% } %>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="pull-right">
                <a class="btn btn-default" href="<%=Url.Action("Index") %>" title="Go back to rule listing"><span>
                    <%=this.GetGlobalResourceObject("General","Back") %></span> </a>
                <a class="btn btn-success" href="#" onclick="$('form').submit()" title="Execute rule"><span>Execute</span>
                </a>
            </div>
        </div>
        <% } %>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.bxml').each(function () {
                var editor = CodeMirror.runMode($(this).val(), "application/xml", $(this).next()[0]);
            });

            var editor = CodeMirror.fromTextArea(document.getElementById("inputXml"), {
                mode: { name: "xml", alignCDATA: true },
                lineNumbers: true,
                lineWrapping: true,
                matchBrackets: true,
                autoCloseTags: true
            });

            editor.on("cursorActivity", function () {
                editor.matchHighlight("CodeMirror-matchhighlight");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/Scripts/codemirror.js"></script>
    <script type="text/javascript" src="/Scripts/util/runmode.js"></script>
    <script type="text/javascript" src="/Scripts/xml.js"></script>
    <script type="text/javascript" src="/Scripts/util/formatting.js"></script>
    <script type="text/javascript" src="/Scripts/util/xml-hint.js"></script>
    <script type="text/javascript" src="/Scripts/util/closetag.js"></script>
    <script type="text/javascript" src="/Scripts/util/searchcursor.js"></script>
    <script type="text/javascript" src="/Scripts/util/matchBrackets.js"></script>
    <script type="text/javascript" src="/Scripts/util/match-highlighter.js"></script>
    <link rel="Stylesheet" href="/Content/codemirror.css" />
</asp:Content>
