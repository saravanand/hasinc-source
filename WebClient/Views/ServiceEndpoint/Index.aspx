﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<Dictionary<string,CelloSaaS.Integration.CelloServiceEndpoint>>" %>

<%@ Import Namespace="CelloSaaS.WcfLibrary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("Title") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>

        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <%if (this.Model != null && this.Model.Count > 0)
                      { %>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                            </span>
                        </div>
                    </div>
                    <% } %>
                    <% if (ViewBag.CanAdd)
                       { %>
                    <div class="col-md-6 pull-right">
                        <div class="pull-right">
                            <a class="btn btn-success" href="<%=Url.Action("Manage") %>" title="Add new service endpoint">
                                <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Add")%></a>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
            <div class="grid-body">
                <%if (this.Model != null && this.Model.Count > 0)
                  {
                      Html.Grid(this.Model.Values).Columns(column =>
                      {
                          column.For(col => col.Name).Named("Name");
                          column.For(col => col.Description).Named("Description");
                          column.For(col => col.EndpointAddress).Named("Endpoint Address");
                          column.For(col => col.SoapMetadata == null ? "REST" : "SOAP").Named("Type");

                          column.For(col => "<a href='" + Url.Action("TestService", new { endpointId = col.Id }) + "' title='Test this endpoint!'><i class='fa fa-wrench'></i></a>")
                               .Attributes(@class => "halign")
                               .HeaderAttributes(@class => "halign")
                               .Named("Test").DoNotEncode();

                          if (ViewBag.CanEdit)
                          {
                              column.For(col => "<a href='" + Url.Action("Manage", new { endpointId = col.Id }) + "' title='Edit endpoint details!'><i class='fa fa-edit'></i></a>")
                                   .Attributes(@class => "halign")
                                   .HeaderAttributes(@class => "halign")
                                   .Named("Edit").DoNotEncode();
                          }

                          if (ViewBag.CanDelete)
                          {
                              column.For(col => "<a href='" + Url.Action("Delete", new { endpointId = col.Id }) + "' onclick=\"return confirm('Are you sure to delete this endpoint?');\" title='Delete this endpoint!'><i class='fa fa-trash-o'></i></a>")
                                  .Attributes(@class => "text-center")
                                  .HeaderAttributes(@class => "text-center")
                                  .Named("Delete").DoNotEncode();
                          }

                      }).Attributes(@class => "celloTable", id => "serviceEPTable").Render();
                  }
                  else
                  { %>
                <div class="alert alert-info">
                    <%=this.GetGlobalResourceObject("General","m_NoData") %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable;
        $(function () {
            filterTable = $('#serviceEPTable').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2, -3] }]
            });

            $(document).on('keypress', '#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function DoSearch() {
            var searchText = $('#searchText').val();
            filterTable.fnFilter(searchText);
            $(".alert-success,.alert-danger").remove();
            return false;
        }
    </script>
</asp:Content>
