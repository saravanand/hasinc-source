﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    ValidateRequest="false" Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Integration.CelloServiceEndpoint>" %>

<%@ Import Namespace="CelloSaaS.Integration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="Go back to service endpoint listing">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: string.IsNullOrEmpty(this.Model.Id) ? "Add New" : "Edit" %>
            <%: this.GetLocalResourceObject("ServiceEndpoint")%></h3>
        <div class="pull-right">
            <a class="btn btn-info" href="#" onclick="$('form').submit()" title="Save Service endpoint">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="<%=Url.Action("Index") %>" title="Go back to service endpoint listing">
                <%=this.GetGlobalResourceObject("General","Back") %></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary("Please correct the errors and submit again:") %>
        </div>
        <% } %>
        <div class="form-list">
            <% using (Html.BeginForm())
               { %>
            <section class="panel purple">
                <header class="panel-heading">
                    <h4>Endpoint Details</h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <%:Html.ValidationMessage("Name", "*") != null ? "has-error" : ""%>">
                                    <label class="mandatory">
                                        <%: this.GetLocalResourceObject("Name")%></label>
                                    <%=Html.TextBox("Name") %>
                                    <%:Html.ValidationMessage("Name", "*")%>
                                    <%=Html.Hidden("Id") %>
                                    <%=Html.Hidden("Status") %>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>
                                        Description
                                <%: this.GetLocalResourceObject("Description")%>
                                    </label>
                                    <%=Html.TextArea("Description", new { rows = "5" })%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <%:Html.ValidationMessage("EndpointAddress", "*") != null ? "has-error" : ""%>">
                                    <label class="mandatory">
                                        <%: this.GetLocalResourceObject("EndpointURL")%>
                                    </label>
                                    <%=Html.TextBox("EndpointAddress", this.Model.EndpointAddress, new { maxlength=255 })%>
                                    <%:Html.ValidationMessage("EndpointAddress", "*")%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="content-box">
                <label style="display: inline-block;">
                    Web Service Type:
                </label>
                <% if (!string.IsNullOrEmpty(Model.Id))
                   { %>
                <%:Model.SoapMetadata == null ? "REST" : "SOAP" %>
                <%:Html.Hidden("webServiceType") %>
                <% }
                   else
                   { %>
                <select name="webServiceType" id="webServiceType">
                    <option <%:ViewData["webServiceType"]=="SOAP"?"selected":"" %>>SOAP</option>
                    <option <%:ViewData["webServiceType"]=="REST"?"selected":"" %>>REST</option>
                </select>
                <% } %>
            </div>
            <section class="panel indigo">
                <header class="panel-heading">
                    <h4 class="serviceTypeTitle">SOAP Details</h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <% if (Model.SoapMetadata != null)
                           { %>
                        <div id="SoapMetadataTable">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group <%:Html.ValidationMessage("SoapMetadata.SoapAction", "*") != null ? "has-error" : ""%>">
                                        <label class="mandatory">
                                            SOAP Action
                                        </label>
                                        <%=Html.TextBox("SoapMetadata.SoapAction")%>
                                        <%:Html.ValidationMessage("SoapMetadata.SoapAction", "*")%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Message Modifier Address
                                        </label>
                                        <%=Html.TextBox("SoapMetadata.MessageModifierAddress")%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Client Credential Type
                                        </label>
                                        <%= Html.DropDownList("SoapMetadata.CredentialType", typeof(CredentialType).ToSelectList(this.Model.SoapMetadata.CredentialType), new { style="width:100%;" })%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Username
                                            (Overrides Logged in user credentials)
                                        </label>
                                        <%=Html.TextBox("SoapMetadata.UserName")%>
                                        <%:Html.ValidationMessage("SoapMetadata.UserName", "*")%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Password
                                        </label>
                                        <%=Html.Password("SoapMetadata.Password", Model.SoapMetadata.Password)%>
                                        <%:Html.ValidationMessage("SoapMetadata.Password", "*")%>
                                    </div>
                                </div>
                            </div>
                            <div class="row encryptionCertificateCell">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Encryption Certificate (Base64 Encoded Key)
                                        </label>
                                        <%=Html.TextArea("SoapMetadata.ServiceCertificate", new { rows = "8", style = "width:300px;" })%>
                                        <%:Html.ValidationMessage("SoapMetadata.ServiceCertificate", "*")%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            EnableFederation
                                        </label>
                                        <input type="checkbox" name="EnableFederation" id="EnableFederation" value="True"
                                            <%=ViewData["EnableFederation"]!=null && Convert.ToBoolean(ViewData["EnableFederation"]) ? "checked=\"checked\"" : ""%> />
                                    </div>
                                </div>
                            </div>
                            <div class="row stsDetailsRow">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            STS Endpoint or Federation Endpoint URL
                                        </label>
                                        <%=Html.TextBox("SoapMetadata.StsEndpointAddress")%>
                                        <%:Html.ValidationMessage("SoapMetadata.StsEndpointAddress", "*")%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Federation Realm Endpoint URL
                                        </label>
                                        <%=Html.TextBox("SoapMetadata.RealmAddress")%>
                                    </div>
                                </div>
                            </div>
                            <div class="row stsDetailsRow">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Security Key Type
                                        </label>
                                        <%= Html.DropDownList("SoapMetadata.SecurityKeyType", typeof(System.IdentityModel.Tokens.SecurityKeyType).ToSelectList(this.Model.SoapMetadata.SecurityKeyType), new { style="width:100%;" })%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Establish Security Context
                                        </label>
                                        <%=Html.CheckBox("SoapMetadata.EstablishSecurityContext")%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Negotiate Service Credential
                                        </label>
                                        <%=Html.CheckBox("SoapMetadata.NegotiateServiceCredential")%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% } %>
                        <% if (Model.RestMetadata != null)
                           { %>
                        <div id="RestMetadataTable">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Request Method</label>
                                        <%=Html.DropDownList("RestMetadata.RequestMethod", typeof(HttpRequestMethod).ToSelectList(this.Model.RestMetadata.RequestMethod), new { style="width:100%;" })%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Request Timeout</label>
                                        <%=Html.TextBox("RestMetadata.RequestTimeout")%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Response Content Type</label>
                                        <%=Html.TextBox("RestMetadata.ContentType")%>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Encoding</label>
                                        <%=Html.TextBox("RestMetadata.Encoding")%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Request Parameters</label>
                                        <table id="requestparamtable" data-count="<%:Model.RestMetadata.RequestParameters.Count %>">
                                            <tbody>
                                                <%  int count = 0;
                                                    foreach (var kv in Model.RestMetadata.RequestParameters)
                                                    { %>
                                                <tr>
                                                    <td>
                                                        <input type="text" style="width: 75px;" name="RestMetadata.RequestParameters[<%:count %>].Key"
                                                            value="<%:kv.Key %>" />
                                                    </td>
                                                    <td>
                                                        <input type="text" style="width: 150px;" name="RestMetadata.RequestParameters[<%:count %>].Value"
                                                            value="<%:kv.Value %>" />
                                                    </td>
                                                    <td>
                                                        <div class="green_but" style="position: relative; top: 7px;">
                                                            <a href="#" class="btnRemoveRequestParam"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <% count++;
                                                    } %>
                                            </tbody>
                                        </table>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-sm btn-info" id="btnRequestparam">+ Add</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Header Parameters</label>
                                        <table id="headerparamtable" data-count="<%:Model.RestMetadata.HeaderParameters.Count %>">
                                            <tbody>
                                                <%     count = 0;
                                                       foreach (var kv in Model.RestMetadata.HeaderParameters)
                                                       { %>
                                                <tr>
                                                    <td>
                                                        <input type="text" style="width: 75px;" name="RestMetadata.HeaderParameters[<%:count %>].Key"
                                                            value="<%:kv.Key %>" />
                                                    </td>
                                                    <td>
                                                        <input type="text" style="width: 150px;" name="RestMetadata.HeaderParameters[<%:count %>].Value"
                                                            value="<%:kv.Value %>" />
                                                    </td>
                                                    <td>
                                                        <div class="green_but" style="position: relative; top: 7px;">
                                                            <a href="#" class="btnRemoveHeaderParam"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <% count++;
                                                       } %>
                                            </tbody>
                                        </table>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-sm btn-info" id="btnHeaderParam">+ Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% } %>
                        <!--<table class="details_container_table">
            <tbody>
                <tr>
                    <td style="vertical-align: top; width: 110px;">
                        <label>
                            Input Xml Schema</label>
                    </td>
                    <td style="vertical-align: top;">
                        <%=Html.TextArea("InputXML", new { rows = "10", style = "width:300px;" })%>
                    </td>
                    <td style="vertical-align: top;">
                        <label>
                            Output Xml Schema</label>
                    </td>
                    <td style="vertical-align: top;">
                        <%=Html.TextArea("OutputXML", new { rows = "10", style = "width:300px;" })%>
                    </td>
                </tr>
            </tbody>
        </table>-->
                        <% } %>
                    </div>
                </div>
            </section>
            <div class="pull-right">
                <a class="btn btn-info" href="#" onclick="$('form').submit()" title="Save Service endpoint">
                    <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
                <a class="btn btn-default" href="<%=Url.Action("Index") %>" title="Go back to service endpoint listing">
                    <%=this.GetGlobalResourceObject("General","Back") %></a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {

            $('#btnRequestparam').click(function (e) {
                var count = $('table#requestparamtable').data('count');
                var tr = '<tr><td><input type="text" style="width:75px;" name="RestMetadata.RequestParameters[' + count + '].Key"/></td>';
                tr += '<td><input type="text" style="width:150px;" name="RestMetadata.RequestParameters[' + count + '].Value"/></td>';
                tr += '<td><div><a href="#" class="btnRemoveRequestParam"><i class="fa fa-trash-o"></i></a></div></td></tr>';
                $('table#requestparamtable tbody').append(tr);
                count++;
                $('table#requestparamtable').data('count', count);
                return false;
            });

            $('#btnHeaderParam').click(function (e) {
                var count = $('table#headerparamtable').data('count');
                var tr = '<tr><td><input type="text" style="width:75px;" name="RestMetadata.HeaderParameters[' + count + '].Key"/></td>';
                tr += '<td><input type="text" style="width:150px;" name="RestMetadata.HeaderParameters[' + count + '].Value"/></td>';
                tr += '<td><div><a href="#" class="btnRemoveHeaderParam"><i class="fa fa-trash-o"></i></a></div></td></tr>';
                $('table#headerparamtable tbody').append(tr);
                count++;
                $('table#headerparamtable').data('count', count);
                return false;
            });

            $('table#requestparamtable').on('click', '.btnRemoveRequestParam', function () {
                var count = $('table#requestparamtable').data('count');
                count = count - 1;
                $(this).closest('tr').remove();
                $('table#requestparamtable').data('count', count);
            });

            $('table#headerparamtable').on('click', '.btnRemoveHeaderParam', function () {
                var count = $('table#headerparamtable').data('count');
                count = count - 1;
                $(this).closest('tr').remove();
                $('table#headerparamtable').data('count', count);
            });

            $("select[name='webServiceType']").change(function () {
                var val = $(this).val();

                if (val == 'REST') {
                    $('.serviceTypeTitle').text('REST Details');
                    $('#RestMetadataTable').show();
                    $('#SoapMetadataTable').hide();
                } else {
                    $('.serviceTypeTitle').text('SOAP Details');
                    $('#SoapMetadataTable').show();
                    $('#RestMetadataTable').hide();
                }
            });

            $("select[name='SoapMetadata.CredentialType']").change(function () {
                var val = $(this).val();

                if (val == 'None') {
                    $('.encryptionCertificateCell').hide();
                    $('.stsDetailsRow').hide();
                    $("input[type='checkbox'][name='EnableFederation']").attr('checked', null);
                } else {
                    $('.encryptionCertificateCell').show();
                }

                if (val == 'None' || val == 'CelloSharedKey') {
                    $('.credentialRow').hide();
                } else {
                    $('.credentialRow').show();
                }
            });

            $("input[type='checkbox'][name='EnableFederation']").click(function () {
                var checked = $(this).is(':checked');

                if (checked) {
                    $('.stsDetailsRow').show();
                } else {
                    $('.stsDetailsRow').hide();
                }
            });

            $("select[name='SoapMetadata.CredentialType']").trigger('change');
            $("select[name='webServiceType']").trigger('change');

            if ($("input[type='checkbox'][name='EnableFederation']").is(':checked')) {
                $('.stsDetailsRow').show();
            } else {
                $('.stsDetailsRow').hide();
            }
        });
    </script>
</asp:Content>
