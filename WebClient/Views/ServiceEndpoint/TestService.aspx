﻿<%@ Page Title="Test Service Endpoint" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%=Url.Action("Index") %>" title="Go back to service endpoint listing">
            <i class="icon-custom-left"></i>
        </a>
        <h3>Test Service</h3>
        <div class="pull-right">
            <a class="btn btn-info" href="#" onclick="$('form').submit()" title="Invoke Service">Invoke
            </a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary() %>
        </div>
        <% } %>
        <% using (Html.BeginForm())
           { %>
        <%=Html.Hidden("endpointId",ViewData["endpointId"]) %>
        <% if (ViewData["entity"] != null)
           {
               var entity = ViewData["entity"] as CelloSaaS.Integration.CelloServiceEndpoint;
        %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4>Endpoint Details</h4>
            </header>
            <div class="panel-body">
                <div class="form-container">
                    <div class="form-group">
                        <label>
                            Name</label>
                        <div><%:entity.Name %></div>
                    </div>
                    <div class="form-group">
                        <label>
                            Description</label>
                        <div><%:entity.Description %></div>
                    </div>
                    <div class="form-group">
                        <label>
                            EndpointAddress</label>
                        <div><%:entity.EndpointAddress %></div>
                    </div>
                    <div class="form-group">
                        <label>
                            Type</label>
                        <div><%:entity.SoapMetadata == null ? "REST" : "SOAP"%></div>
                    </div>
                    <div class="form-group">
                        <label>
                            Input Xml</label>
                        <%=Html.TextArea("inputXml", new { rows = "19", cols = "20", style = "width:350px;" })%>
                    </div>
                    <% if (ViewData["OutputXml"] != null && !string.IsNullOrEmpty(ViewData["OutputXml"].ToString()))
                       { %>
                    <div class="form-group">
                        <label>
                            Output Xml</label>
                        <textarea rows="1" cols="1" class="bxml" style="display: none;"><%=(System.Xml.Linq.XElement.Parse(ViewData["OutputXml"].ToString()).ToString()) %></textarea>
                        <pre class="xmlOutput cm-s-default"></pre>
                    </div>
                    <% } %>
                </div>
            </div>
        </section>
        <% }
           else
           { %>
        <div class="alert alert-warning">
            Invalid data!
        </div>
        <% } %>
        <div class="pull-right">
            <a class="btn btn-info" href="#" onclick="$('form').submit()" title="Invoke Service">Invoke
            </a>
        </div>
        <% } %>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.bxml').each(function () {
                var editor = CodeMirror.runMode($(this).val(), "application/xml", $(this).next()[0]);
            });

            window.xsleditor = CodeMirror.fromTextArea(document.getElementById("inputXml"), {
                mode: { name: "xml", alignCDATA: true },
                lineNumbers: true,
                lineWrapping: true,
                autoCloseTags: true
            });

            window.xsleditor.on("cursorActivity", function () {
                window.xsleditor.matchHighlight("CodeMirror-matchhighlight");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/Scripts/codemirror.js"></script>
    <script type="text/javascript" src="/Scripts/util/runmode.js"></script>
    <script type="text/javascript" src="/Scripts/xml.js"></script>
    <script type="text/javascript" src="/Scripts/util/formatting.js"></script>
    <script type="text/javascript" src="/Scripts/util/xml-hint.js"></script>
    <script type="text/javascript" src="/Scripts/util/closetag.js"></script>
    <script type="text/javascript" src="/Scripts/util/searchcursor.js"></script>
    <script type="text/javascript" src="/Scripts/util/matchBrackets.js"></script>
    <script type="text/javascript" src="/Scripts/util/match-highlighter.js"></script>
    <link rel="Stylesheet" href="/Content/codemirror.css" />
</asp:Content>
