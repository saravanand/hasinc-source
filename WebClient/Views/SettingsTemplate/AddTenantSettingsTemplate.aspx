﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.SettingsManagement.SettingsTemplate>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $().ready(function () {
            $('input[type="checkbox"]').bind('click', function () {
                if ($(this).is(':checked')) {
                    $(this).val(true);
                } else {
                    $(this).val(false);
                }
            });
            $('#addSubmit').click(function () {
                if ($("#settingsTemplateId option:selected").val() == "") {
                    alert('<%: this.GetLocalResourceObject("ChooseTemplate") %>');
                    return false;
                }
            });

            $("#settingsTemplateId").change(function () {
                if ($("#settingsTemplateId option:selected").val() == "") {
                    $("#templateDetails").empty();
                    alert('<%: this.GetLocalResourceObject("ChooseTemplate") %>');
                    return false;
                }

                $("#TenantSettingTemplateForm").submit();
            });

            if ($("#selectedTemplateId") !== undefined && $("#selectedTemplateId").val() !== null && $("#selectedTemplateId").val().length > 0) {
                var templateId = $("#selectedTemplateId").val();
                $("#settingsTemplateId option[value='" + templateId + "']").prop("selected", true);
                $("#settingsTemplateId").trigger("change");
            }
        });

        function onTemplateSelected(showdialog) {
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%: Url.Action("TenantSettingsTemplate","SettingsTemplate") %>" title="Back to Tenant SettingsTemplate!">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: this.GetLocalResourceObject("Title") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <%
            if (Html.ValidationSummary() != null)
            {
        %>
        <div id="errorMsg" class="alert alert-danger">
            <%: Html.ValidationSummary() %>
        </div>
        <%
            }
        %>
        <%   
            string templateId = null;
            if (ViewData["settingTemplateId"] != null && !string.IsNullOrEmpty(ViewData["settingTemplateId"].ToString()))
            {
                templateId = ViewData["settingTemplateId"].ToString();
            }                    
        %>
        <%: Html.Hidden("selectedTemplateId",templateId) %>
        <% Html.RenderAction("TenantTemplatesList", "SettingsTemplate"); %>
        <div id="loadingImage" style="display: none">
            <i class="fa-4x fa-spinner"></i>
        </div>
        <div id="templateDetails">
            <% if (this.Model != null)
               {
            %>
            <% Html.RenderAction("TenantSettingsTemplateDetails", "SettingsTemplate", new { @settingsTemplateId = this.Model.Id.ToString(), @forTenantProvisioning = false });%>
            <%} %>
        </div>
    </div>
</asp:Content>
