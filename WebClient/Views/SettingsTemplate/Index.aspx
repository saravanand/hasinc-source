﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.Model.SettingsManagement.SettingsTemplate>>" %>

<%@ Import Namespace="CelloSaaS.Model.SettingsManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            $('table#settingsTemplateList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        });

        function viewTemplate(templateId, tenantId) {
            $.ajax({
                url: '<%: Url.Action("ViewSettingsTemplate","SettingsTemplate") %>',
                type: "POST",
                data: { "settingTemplateId": templateId, 'tenantId': tenantId },
                beforeSend: function (xhr) {
                    $('#loading').show();
                },
                success: function (data) {
                    if (data !== undefined && data.length > 0) {
                        $(".viewTemplateDetail").html(data);
                    }
                },
                complete: function (data) {
                    $('#loading').hide();
                }
            });
        }

    </script>
    <style type="text/css">
        .viewlink, .link {
            cursor: pointer;
        }

        .gridContainer {
            position: static;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title")%>
        </h3>
        <div class="pull-right">
            <%
                if (string.IsNullOrEmpty(UserIdentity.SessionTenantID))
                {
            %>
            <div class="tenantContextblock" style="float: left">
                <% ViewData["DemandedPrivilege"] = SettingsTemplatePrivilegeConstants.ViewSettingsTemplate; %>
                <%Html.RenderPartial("TenantContextControl", new SettingsTemplate().EntityIdentifier);%>
            </div>
            <% } %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6 pull-right">
                        <div class="pull-right">
                            <%  
                                var editAccess = CelloSaaS.ServiceProxies.AccessControlManagement.TenantAccessProxy.CheckTenantAccess(new SettingsTemplate().EntityIdentifier, TenantContext.GetTenantId(new SettingsTemplate().EntityIdentifier), CelloSaaS.Model.FetchType.Edit, TenantContext.GetTenantId(new SettingsTemplate().EntityIdentifier)) || true;
                                if (editAccess && CelloSaaS.Library.UserIdentity.HasPrivilege(SettingsTemplatePrivilegeConstants.AddSettingsTemplate))
                                { %>
                            <a class="btn btn-success" href="<%: Url.Action("ManageSettingsTemplates","SettingsTemplate") %>" title="Add New SettingsTemplate!">
                                <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Add")%>
                            </a>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <div class="statusMessage">
                    <%            
                        if (Html.ValidationMessage("SettingsTemplateSuccess") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateSuccess").ToString()))
                        {	
                    %>
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                        <%: Html.ValidationMessage("SettingsTemplateSuccess")%>
                    </div>
                    <%
                        }
                        if (Html.ValidationMessage("SettingsTemplateError") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateError").ToString()))
                        {	
                    %>
                    <div class="alert alert-danger">
                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                        <%: Html.ValidationMessage("SettingsTemplateError")%>
                    </div>
                    <%
                        }
                    %>
                </div>
                <%  
                    if (this.Model != null && this.Model.Count() > 0)
                    {
                        var deleteAccess = CelloSaaS.ServiceProxies.AccessControlManagement.TenantAccessProxy.CheckTenantAccess(new SettingsTemplate().EntityIdentifier, TenantContext.GetTenantId(new SettingsTemplate().EntityIdentifier), CelloSaaS.Model.FetchType.Delete, TenantContext.GetTenantId(new SettingsTemplate().EntityIdentifier)) || true;

                        string tenantId = TenantContext.GetTenantId(new SettingsTemplate().EntityIdentifier);
                        Html.Grid(this.Model).Columns(column =>
                            {
                                column.For(c => c.Name).Named(this.GetLocalResourceObject("TemplateName").ToString());
                                column.For(c => c.TemplateType).Named(this.GetLocalResourceObject("TemplateType").ToString());
                                column.For(c => c.IsGlobal ? "Yes" : "No").Named(this.GetLocalResourceObject("Global").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign");
                                column.For(c => "<a class='link' onclick='viewTemplate(\"" + c.Id + "\")'><i class='fa fa-eye'></i></a>")
                                    .Named(this.GetLocalResourceObject("View").ToString())
                                    .HeaderAttributes(@class => "noSortCol halign").Attributes(@class => "halign")
                                    .DoNotEncode();
                                if (editAccess && CelloSaaS.Library.UserIdentity.HasPrivilege(SettingsTemplatePrivilegeConstants.EditSettingsTemplate))
                                {
                                    column.For(c => string.IsNullOrEmpty(c.TenantId) && !CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin)
                                ? "-" : Html.CelloActionLink("Edit", "ManageSettingsTemplates", new
                                {
                                    settingTemplateId
                                        = c.Id
                                }).Replace("Edit", "<i class='fa fa-pencil'></i>"))
                                .Named(this.GetLocalResourceObject("EditTemplate").ToString())
                                .HeaderAttributes(@class => "noSortCol halign").Attributes(@class => "halign")
                                .DoNotEncode();
                                }
                            }).Attributes(id => "settingsTemplateList", @class => "celloTable").Render();
                    }
                    else
                    {%>
                <div class="alert alert-info">
                    <%: this.GetGlobalResourceObject("General","NoData") %>
                </div>
                <%} %>
            </div>
            <br />
            <div class="viewTemplateDetail">
            </div>
        </div>
    </div>
</asp:Content>
