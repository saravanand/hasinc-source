﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.SettingsManagement.SettingsTemplate>" %>

<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="CelloSaaS.Model.SettingsManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.SettingsManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select').select2();

            $('input[type="checkbox"]').bind('click', function () {
                if ($(this).is(':checked')) {
                    $(this).val(true);
                } else {
                    $(this).val(false);
                }
            });

            $("#dateFormatPreview").click(function () {
                $.ajax({
                    url: '<%: Url.Action("GetGivenFormattedDate","SettingsTemplate") %>',
                    type: 'POST',
                    data: { 'dateFormat': $(".DateFormatClass").val() },
                    beforeSend: function (xhr) {
                    },
                    success: function (data) {
                        if (data !== undefined && data.length > 0) {
                            $("#sampleDateFormatPvw").html('<%: this.GetLocalResourceObject("Preview") %> : ' + data);
                        }
                    },
                    complete: function (data) {
                    }
                });
            });

        });
    </script>
    <script type="text/javascript">
        function SubmitSettingsForm() {
            $('#manageSettingsTemplate').submit();
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%:Url.Content("~/SettingsTemplate/Index")%>" title="Go Back to Settings Template">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: this.GetLocalResourceObject("Title") %>
        </h3>
        <div class="pull-right">
            <a class="btn btn-info" onclick="$('form').submit()" title="<%: this.GetLocalResourceObject("SaveTemplate")%>">
                <i class="fa fa-save"></i>&nbsp;<%: this.GetGlobalResourceObject("General","Save")%>
            </a>
            <a class="btn btn-default" href="<%:Url.Content("~/SettingsTemplate/Index")%>" title="Go Back to Settings Template">
                <%=this.GetGlobalResourceObject("General", "Back")%>
            </a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% using (Html.BeginForm("ManageSettingsTemplates", "SettingsTemplate", FormMethod.Post, new { enctype = "multipart/form-data", @id = "manageSettingsTemplate" }))
           {
        %>
        <section class="panel purple">
            <header class="panel-heading">
                <h4><%:this.GetLocalResourceObject("TemplateDetails") %></h4>
            </header>
            <div class="panel-body">
                <div class="statusMessage">
                    <%
               if (Html.ValidationSummary() != null)
               {
                    %>
                    <div id="errorMsg" class="alert alert-danger">
                        <%: Html.ValidationSummary() %>
                    </div>
                    <%
               }    
                    %>
                </div>
                <div class="form-container">
                    <table class="celloTable details_container_table">
                        <thead>
                            <tr>
                                <th>
                                    <%:this.GetLocalResourceObject("TemplateDetails")%>
                                </th>
                                <th>
                                    <%:this.GetLocalResourceObject("Value")%>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="row4">
                                    <label>
                                        <%:this.GetLocalResourceObject("Name")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                                    </label>
                                </td>
                                <td>
                                    <%: Html.HiddenFor(model => model.Id)%>
                                    <% 
                            
               if (this.Model.Id != null && this.Model.Id != Guid.Empty)
               { %>
                                    <%: Html.Label(this.Model.Name) %>
                                    <%= Html.HiddenFor(model=> model.Name) %>
                                    <% }
               else
               {
                              
                                    %>
                                    <%: Html.TextBox("Name",this.Model.Name, new { @autocomplete = "off" })%>
                                    <%}  %>
                                </td>
                            </tr>
                            <tr>
                                <td class="row4">
                                    <%: Html.Label(this.GetLocalResourceObject("Type").ToString()) %>
                                </td>
                                <td class="row5">
                                    <%
               if (this.Model.Id != null && this.Model.Id != Guid.Empty)
               {
                                    %>
                                    <%= Html.RadioButton("TemplateType", SettingTemplateType.Fixed, new { @disabled = "disabled" })%>
                                    <%:this.GetLocalResourceObject("Fixed")%>
                        &nbsp;
                        <%= Html.RadioButton("TemplateType", SettingTemplateType.Customized, new { @disabled = "disabled" })%>
                                    <%:this.GetLocalResourceObject("Customized")%>
                                    <%: Html.Hidden("TemplateType",this.Model.TemplateType.ToString("f")) %>
                                    <%
               }
               else
               {
                                    %>
                                    <%= Html.RadioButton("TemplateType", SettingTemplateType.Fixed)%>
                                    <%:this.GetLocalResourceObject("Fixed")%>
                        &nbsp;
                        <%= Html.RadioButton("TemplateType", SettingTemplateType.Customized)%>
                                    <%:this.GetLocalResourceObject("Customized")%>
                                    <% } %>
                                </td>
                            </tr>
                            <%
               if (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
               {
                            %>
                            <tr>
                                <td class="row4">
                                    <%: Html.Label(this.GetLocalResourceObject("IsGlobal").ToString())%>
                                </td>
                                <td class="row5">
                                    <%
                            
                   if (this.Model.Id != null && this.Model.Id != Guid.Empty)
                   {
                       if (this.Model.IsGlobal)
                       {
                                    %>
                                    <input type="checkbox" name="IsGlobal" checked="checked" disabled="disabled" />
                                    <%
                       }
                       else
                       {   
                                    %>
                                    <input type="checkbox" name="IsGlobal" disabled="disabled" />
                                    <% } %>
                                    <input type="hidden" name="IsGlobal" value="<%=this.Model.IsGlobal.ToString() %>" />
                                    <%
                   }
                   else
                   {
                                    %>
                                    <%: Html.CheckBox("IsGlobal")%>
                                    <%}%>
                                </td>
                            </tr>
                            <% }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <section class="panel indigo">
            <header class="panel-heading">
                <h4><%: this.GetLocalResourceObject("Attributes")%></h4>
            </header>
            <div class="panel-body">
                <div class="form-contianer" style="padding: 5px;">
                    <div class="text-info">
                        <i class="fa fa-info-circle"></i>&nbsp;<%: this.GetLocalResourceObject("AttributeHint") %>
                    </div>
                    <%
               Dictionary<string, SettingsAttribute> settingsAttributes = (Dictionary<string, SettingsAttribute>)ViewData["SettingsAttribute"];   
                    %>
                    <div style="padding: 5px">
                    </div>
                    <table class="celloTable details_container_table">
                        <thead>
                            <tr>
                                <th>
                                    <%: this.GetLocalResourceObject("Select")%>
                                </th>
                                <th>
                                    <%: this.GetLocalResourceObject("AttributeName")%>
                                </th>
                                <th>
                                    <%: this.GetLocalResourceObject("AttributeValue")%>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
               string value = string.Empty;
               string attributeId = AttributeConstants.WCFSharedKey;
               int attributeOrdinal = 0;
               var status = false;
               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("WCFSharedKey").ToString())%>
                                </td>
                                <td class="row5">
                                    <%=Html.Password("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.TenantAuthenticateSetting;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("TenantAuthenticateSetting").ToString())%>
                                </td>
                                <td class="row5">
                                    <%: Html.TextBox("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.UserPasswordExpiration;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("UserPasswordExpiration").ToString())%>
                                </td>
                                <td class="row5">
                                    <%: Html.TextBox("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.ApplicationConnectionString;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("ApplicationConnectionString").ToString())%>
                                </td>
                                <td class="row5">
                                    <%: Html.TextBox("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                    <span id="Span2" style="margin-left: 10px; color: #666; font-style: italic;">
                                        <%: this.GetLocalResourceObject("ConnectionStringProvider")%>
                                    </span>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.HomeRealm;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("HomeRealm").ToString())%>
                                </td>
                                <td class="row5">
                                    <%: Html.TextBox("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.ShareUsers;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("LinkByOtherTenantUsers").ToString())%>
                                </td>
                                <td class="row5">
                                    <%
               status = string.IsNullOrEmpty(value) ? false : Convert.ToBoolean(value);
                                    %>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].AttributeValue",status) %>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.MaxPasswordAnswerFailureCount;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("MaxPasswordAnswerFailureCount").ToString())%>
                                </td>
                                <td class="row5">
                                    <%: Html.TextBox("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.MaxPasswordFailureCount;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("MaxPasswordFailureCount").ToString())%>
                                </td>
                                <td class="row5">
                                    <%: Html.TextBox("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.UserDetailsConnectionString;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               }
                            %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("UserDetailsConnectionString").ToString())%>
                                </td>
                                <td class="row5">
                                    <%: Html.TextBox("Attributes[" + attributeOrdinal + "].AttributeValue", value, new { @autocomplete = "off" })%>
                                    <span id="Span3" style="margin-left: 10px; color: #666; font-style: italic;">
                                        <%: this.GetLocalResourceObject("ConnectionStringProvider")%>
                                    </span>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++; value = string.Empty; attributeId = AttributeConstants.AutoApprovalTenantCreation;
               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               } %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("AutoApprovalTenantCreation").ToString())%>
                                </td>
                                <td class="row5">
                                    <%
               status = string.IsNullOrEmpty(value) ? false : Convert.ToBoolean(value);
                                    %>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal +"].AttributeValue",status)%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.Theme;
               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               } %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("Theme").ToString())%>
                                </td>
                                <td class="row5">
                                    <%=Html.DropDownList("Attributes[" + attributeOrdinal + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["Themes"], new { style="width:100%;" })%>
                                    <%=Html.CelloValidationMessage("ViewMetaData.Theme", "*")%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.Logo;
               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               } %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("Logo").ToString())%>
                                </td>
                                <td class="row5">
                                    <input id="File1" name="fileuploader" type="file" contenteditable="false" value="<%: value %>" />
                                    <p class="text-info">
                                        <i class="fa fa-info-circle"></i>&nbsp;<%=this.GetLocalResourceObject("info_Logo") %>&nbsp;
                                         <%: this.GetLocalResourceObject("SupportedFormats")%>
                                    </p>
                                    <%= Html.Hidden("Attributes[" + attributeOrdinal + "].AttributeValue", value)%>
                                    <img src='../..<%: ViewData["ImageUrl"]%>' alt="Logo" />
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.DateFormat;
               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               } %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("DateFormat").ToString())%>
                                </td>
                                <td class="row5">
                                    <%=Html.DropDownList("Attributes[" + attributeOrdinal + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["DateFormats"], new { style="width:100%;" })%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.Language;
               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               } %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("Language").ToString())%>
                                </td>
                                <td class="row5">
                                    <%=Html.DropDownList("Attributes[" + attributeOrdinal + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["Languages"], new { style="width:100%;" })%>
                                    <%=Html.CelloValidationMessage("language", "*")%>
                                </td>
                            </tr>
                            <%
               attributeOrdinal++;
               value = string.Empty;
               attributeId = AttributeConstants.DisableProductAnalytics;
               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
               } %>
                            <tr>
                                <td>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal+"].IsSelected", settingsAttributes.ContainsKey(attributeId)?settingsAttributes[attributeId].IsSelected:false)%>
                                </td>
                                <td class="row4">
                                    <%: Html.Hidden("Attributes["+attributeOrdinal+"].AttributeId", attributeId)%>
                                    <%: Html.Label(this.GetLocalResourceObject("DisableProductAnalytics").ToString())%>
                                </td>
                                <td class="row5">
                                    <%
               status = string.IsNullOrEmpty(value) ? false : Convert.ToBoolean(value);
                                    %>
                                    <%: Html.CheckBox("Attributes["+attributeOrdinal +"].AttributeValue",status)%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <% } %>
            </div>
        </section>
        <div class="pull-right">
            <a class="btn btn-info" onclick="$('form').submit()" title="<%: this.GetLocalResourceObject("SaveTemplate")%>">
                <i class="fa fa-save"></i>&nbsp;<%: this.GetGlobalResourceObject("General","Save")%>
            </a>
            <a class="btn btn-default" href="<%:Url.Content("~/SettingsTemplate/Index")%>" title="Go Back to Settings Template">
                <%=this.GetGlobalResourceObject("General", "Back")%>
            </a>
        </div>
    </div>
</asp:Content>
