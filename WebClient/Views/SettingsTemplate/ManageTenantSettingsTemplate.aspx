﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.Model.SettingsManagement.TenantSettingsTemplate>" %>

<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="CelloSaaS.Model.SettingsManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.SettingsManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select').select2();

            $("input[type=text]").attr('maxLength', '200');
            $('input[type="checkbox"]').bind('click', function () {
                if ($(this).is(':checked')) {
                    $(this).val(true);
                } else {
                    $(this).val(false);
                }
            });

            $("#dateFormatPreview").click(function () {
                $.ajax({
                    url: '<%: Url.Action("GetGivenFormattedDate","SettingsTemplate") %>',
                    type: 'POST',
                    data: { 'dateFormat': $(".DateFormatClass").val() },
                    beforeSend: function (xhr) {
                    },
                    success: function (data) {
                        if (data !== undefined && data.length > 0) {
                            $("#sampleDateFormatPvw").html('<%: this.GetLocalResourceObject("Preview") %> : ' + data);
                        }
                    },
                    complete: function (data) {
                    }
                });
            });

        });

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <a href="<%:Url.Content("~/SettingsTemplate/TenantSettingsTemplate")%>" title="<%: this.GetLocalResourceObject("GoButtonTitle") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: this.GetLocalResourceObject("Title") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="statusMessage">
            <%
                if (Html.ValidationMessage("SettingsTemplateSuccess") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateSuccess").ToString()))
                {	
            %>
            <div class="alert alert-success">
                <%: Html.ValidationMessage("SettingsTemplateSuccess")%>
            </div>
            <%
                }

                if (Html.ValidationMessage("SettingsTemplateError") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateError").ToString()))
                {	
            %>
            <div class="alert alert-danger">
                <%: Html.ValidationMessage("SettingsTemplateError")%>
            </div>
            <%
                }
            %>
            <%    
                if (Html.ValidationSummary() != null)
                {
            %>
            <div id="errorMsg" class="alert alert-danger">
                <%: Html.ValidationSummary() %>
            </div>
            <%
                }    
            %>
            <% using (Html.BeginForm("ManageTenantSettingsTemplate", "SettingsTemplate", FormMethod.Post, new { @id = "tenantSettingsTemplateForm", enctype = "multipart/form-data" }))
               {
            %>
        </div>
        <div class="grid simple">
            <div class="grid-title">
                <h4>
                    <%: this.GetLocalResourceObject("Title") %></h4>
            </div>
            <div class="grid-body">
                <table class="celloTable details_container_table">
                    <thead>
                        <tr>
                            <th>
                                <%: this.GetLocalResourceObject("TemplateDetails")%>
                            </th>
                            <th>
                                <%: this.GetLocalResourceObject("Value")%>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="row4">
                                <%: Html.Label(this.GetLocalResourceObject("Name").ToString())%>
                            </td>
                            <td>
                                <%: this.Model.SettingsTemplate.Name %>
                            </td>
                        </tr>
                        <tr>
                            <td class="row4">
                                <%: Html.Label(this.GetLocalResourceObject("Type").ToString())%>
                            </td>
                            <td class="row5">
                                <%: this.Model.SettingsTemplate.TemplateType %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <%: Html.HiddenFor(model=> model.SettingsTemplate.Id) %>
                    <%: Html.HiddenFor(model => model.SettingsTemplate.Name)%>
                    <%: Html.HiddenFor(model=> model.TemplateId) %>
                    <%: Html.HiddenFor(model=> model.SettingsTemplate.TemplateType) %>
                    <%: Html.HiddenFor(model=> model.TenantId) %>
                    <%: Html.Hidden("TemplateType",  this.Model.SettingsTemplate.TemplateType )%>
                </div>
            </div>
        </div>
        <br />
        <div class="grid simple">
            <div class="grid-title">
                <h4>
                    <%: this.GetLocalResourceObject("Attributes") %></h4>
            </div>
            <div class="grid-body">
                <div class="form-container">
                    <%
                   Dictionary<string, SettingsAttribute> settingsAttributes = (Dictionary<string, SettingsAttribute>)ViewData["SettingsAttribute"];

                   var attributeCount = -1;
                    %>
                    <table class="celloTable details_container_table">
                        <thead>
                            <tr>
                                <th>
                                    <%: this.GetLocalResourceObject("AttributeName")%>
                                </th>
                                <th>
                                    <%: this.GetLocalResourceObject("AttributeValue")%>
                                </th>
                            </tr>
                        </thead>
                        <%
                   var templateType = this.Model.SettingsTemplate.TemplateType;

                   string attributeId = AttributeConstants.WCFSharedKey;
                   string value = string.Empty;
                   if (settingsAttributes.ContainsKey(attributeId))
                   {
                       value = settingsAttributes[attributeId].AttributeValue;
                       attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%: attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("WCFSharedKey").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   {
                                       var sharedKey = string.Empty;
                                       for (var i = 0; i < value.Length; i++)
                                       {
                                           sharedKey += "*";
                                       }
                                %>
                                <%: Html.Label(sharedKey)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%:Html.Password("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                   attributeId = AttributeConstants.TenantAuthenticateSetting;
                   value = string.Empty;

                   if (settingsAttributes.ContainsKey(attributeId))
                   {
                       value = settingsAttributes[attributeId].AttributeValue;
                       attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("TenantAuthenticateSetting").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                   attributeId = AttributeConstants.UserPasswordExpiration;
                   value = string.Empty;

                   if (settingsAttributes.ContainsKey(attributeId))
                   {
                       value = settingsAttributes[attributeId].AttributeValue;
                       attributeCount++;

                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("UserPasswordExpiration").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                   attributeId = AttributeConstants.ApplicationConnectionString;
                   value = string.Empty;

                   if (settingsAttributes.ContainsKey(attributeId))
                   {
                       value = settingsAttributes[attributeId].AttributeValue;
                       attributeCount++;

                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%: Html.Label(this.GetLocalResourceObject("ApplicationConnectionString").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                   attributeId = AttributeConstants.HomeRealm;
                   value = string.Empty;

                   if (settingsAttributes.ContainsKey(attributeId))
                   {
                       value = settingsAttributes[attributeId].AttributeValue;
                       attributeCount++;

                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("HomeRealm").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
               attributeId = AttributeConstants.ShareUsers;
               bool attributeValue = false;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   attributeValue = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? false : bool.Parse(settingsAttributes[attributeId].AttributeValue);
                   attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("LinkByOtherTenantUsers").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: attributeValue? Html.Label(this.GetLocalResourceObject("Enabled").ToString()):Html.Label(this.GetLocalResourceObject("Disabled").ToString()) %>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",attributeValue)
                                %>
                                <%}
                                   else
                                   { %>
                                <%=Html.CheckBox("Attributes[" + attributeCount + "].AttributeValue", attributeValue, new { style = "width:20px;" })%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
               attributeId = AttributeConstants.MaxPasswordAnswerFailureCount;
               value = string.Empty;

               if (settingsAttributes.ContainsKey(attributeId))
               {
                   value = settingsAttributes[attributeId].AttributeValue;
                   attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("MaxPasswordAnswerFailureCount").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.MaxPasswordFailureCount;
value = string.Empty;

if (settingsAttributes.ContainsKey(attributeId))
{
    value = settingsAttributes[attributeId].AttributeValue;
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("MaxPasswordFailureCount").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%=Html.CelloTextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.UserDetailsConnectionString;
value = string.Empty;

if (settingsAttributes.ContainsKey(attributeId))
{
    value = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? string.Empty : settingsAttributes[attributeId].AttributeValue;
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId  %>" />
                                <%: Html.Label(this.GetLocalResourceObject("UserDetailsConnectionString").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%=Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%=Html.CelloTextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.AutoApprovalTenantCreation;
attributeValue = false;

if (settingsAttributes.ContainsKey(attributeId))
{
    attributeValue = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? false : bool.Parse(settingsAttributes[attributeId].AttributeValue);
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("AutoApprovalTenantCreation").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: attributeValue?Html.Label("Enabled"):Html.Label("Disabled") %>
                                <%=Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", attributeValue)%>
                                <%}
                                   else
                                   { %>
                                <%=Html.CheckBox("Attributes[" + attributeCount + "].AttributeValue", attributeValue, new { style = "width:20px;" })%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.Theme;
value = string.Empty;
if (settingsAttributes.ContainsKey(attributeId))
{
    value = settingsAttributes[attributeId].AttributeValue;
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("Theme").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount+ "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.DropDownList("Attributes[" + attributeCount + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["Themes"], new { style="width:100%" })%>
                                <%=Html.CelloValidationMessage("ViewMetaData.Theme", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.Logo;
value = string.Empty;

if (settingsAttributes.ContainsKey(attributeId))
{
    value = settingsAttributes[attributeId].AttributeValue;
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%: Html.Label(this.GetLocalResourceObject("Logo").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <img src="<%=this.ResolveClientUrl("../../" + ViewData["ImageUrl"])%>" alt="Logo" />
                                <%: Html.Hidden("Attributes["+ attributeCount + "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <input id="logoPath" name="logoPath" value="ViewMetaData.Logo" type="file" contenteditable="false" />
                                <%= Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <br />
                                <img src="<%=this.ResolveClientUrl("../../" +  ViewData["ImageUrl"])%>" alt="Logo" />
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.DateFormat;
value = string.Empty;

if (settingsAttributes.ContainsKey(attributeId))
{
    value = settingsAttributes[attributeId].AttributeValue;
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%: Html.Label(this.GetLocalResourceObject("DateFormat").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes["+ attributeCount+ "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.DropDownList("Attributes[" + attributeCount + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["DateFormats"], new { style="width:100%" })%>
                                <%=Html.CelloValidationMessage("dateFormat", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.Language;
value = string.Empty;

if (settingsAttributes.ContainsKey(attributeId))
{
    value = settingsAttributes[attributeId].AttributeValue;
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%: Html.Label(this.GetLocalResourceObject("Language").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.DropDownList("Attributes[" + attributeCount + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["Languages"], new { style="width:100%" })%>
                                <%=Html.CelloValidationMessage("language", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
attributeId = AttributeConstants.DisableProductAnalytics;
attributeValue = false;

if (settingsAttributes.ContainsKey(attributeId))
{
    attributeValue = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? false : bool.Parse(settingsAttributes[attributeId].AttributeValue);
    attributeCount++;
                        %>
                        <tr>
                            <td class="row4">
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%: Html.Label(this.GetLocalResourceObject("DisableProductAnalytics").ToString())%>
                            </td>
                            <td class="row5">
                                <% if (templateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: attributeValue?Html.Label("Enabled"):Html.Label("Disabled") %>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",attributeValue.ToString()) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.CheckBox("Attributes[" + attributeCount + "].AttributeValue", attributeValue, new { style = "width:20px;" })%>
                                <%=Html.CelloValidationMessage("EnableProductAnalytics", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                    </table>
                </div>
            </div>
        </div>
        <br />
        <% if (this.Model.SettingsTemplate.TemplateType != SettingTemplateType.Fixed)
           { %>
        <div class="pull-right">
            <a class="btn btn-info" onclick="$('#tenantSettingsTemplateForm').submit()" title="<%: this.GetLocalResourceObject("SaveTemplate")%>">
               <i class="fa fa-save"></i>&nbsp;<%: this.GetGlobalResourceObject("General","Save")%></a>
        </div>
        <% }
               } %>
    </div>
</asp:Content>
