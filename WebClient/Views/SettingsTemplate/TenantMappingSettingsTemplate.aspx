﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $().ready(function () {
            $("#SettingsTemplateList").change(function () {
                alert('<%: this.GetLocalResourceObject("SelectedTemplate")%>' + this.val());
            });
            $("#tenantTemplateGrid").load('/SettingsTemplate/LoadTenantSettingsTemplates/');
        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        <%: this.GetLocalResourceObject("Title")%>
    </h2>
    <br />
    <%: Html.Label(this.GetLocalResourceObject("TenantName").ToString())%>
    <%: ViewData["TenantName"] %>
    <div id="selectorControls">
        <table>
            <tr>
                <td style="width: 300px">
                    <%: this.GetLocalResourceObject("TemplateName")%>
                </td>
                <td>
                    <%= Html.DropDownList("SettingsTemplateList") %>
                </td>
            </tr>
        </table>
    </div>
    <div id="tenantTemplateGrid">
    </div>
    <div id="templateDetails">
    </div>
</asp:Content>
