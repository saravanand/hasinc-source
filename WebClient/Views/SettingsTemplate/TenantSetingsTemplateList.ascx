﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.Model.SettingsManagement.TenantSettingsTemplate>>" %>
<%@ Import Namespace="CelloSaaS.Model.SettingsManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="grid simple">
    <div class="grid-body no-border pdm-0">
        <%
            if (this.Model != null && this.Model.Count() > 0)
            {
                var editAccess = CelloSaaS.ServiceProxies.AccessControlManagement.TenantAccessProxy.CheckTenantAccess(
                        new TenantSettingsTemplate().EntityIdentifier, TenantContext.GetTenantId(
                        new TenantSettingsTemplate().EntityIdentifier), CelloSaaS.Model.FetchType.Edit, TenantContext.GetTenantId(
                        new TenantSettingsTemplate().EntityIdentifier)) || true;
                var deleteAccess = CelloSaaS.ServiceProxies.AccessControlManagement.TenantAccessProxy.CheckTenantAccess(
                        new TenantSettingsTemplate().EntityIdentifier, TenantContext.GetTenantId(
                        new TenantSettingsTemplate().EntityIdentifier), CelloSaaS.Model.FetchType.Delete, TenantContext.GetTenantId(
                        new TenantSettingsTemplate().EntityIdentifier)) || true;

                var viewAccess = CelloSaaS.ServiceProxies.AccessControlManagement.TenantAccessProxy.CheckTenantAccess(
                        new TenantSettingsTemplate().EntityIdentifier, TenantContext.GetTenantId(
                        new TenantSettingsTemplate().EntityIdentifier), CelloSaaS.Model.FetchType.View, TenantContext.GetTenantId(
                        new TenantSettingsTemplate().EntityIdentifier)) || true;

                string tenantId = TenantContext.GetTenantId(new TenantSettingsTemplate().EntityIdentifier);

                Html.Grid(this.Model.OrderBy(x => x.CreatedOn)).Columns(column =>
                {
                    column.For(c => c.SettingsTemplate.Name).Named(this.GetLocalResourceObject("TemplateName").ToString());
                    column.For(c => c.SettingsTemplate.TemplateType).Named(this.GetLocalResourceObject("TemplateType").ToString());

                    column.For(c => c.SettingsTemplate.IsGlobal ? "Yes" : "No").Named(this.GetLocalResourceObject("Global").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign");

                    if (viewAccess && CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.Model.SettingsManagement.TenantSettingsTemplatePrivilegeConstants.ViewTenantTemplate))
                    {
                        column.For(c => "<a class='link' onclick='viewTemplate(\"" + c.SettingsTemplate.Id + "\",\"" + tenantId + "\")'> <i class='fa fa-search'></i></a>")
                            .Named(this.GetGlobalResourceObject("General", "View").ToString())
                            .HeaderAttributes(@class => "noSortCol halign").Attributes(@class => "halign")
                            .DoNotEncode();
                    }
                    if (editAccess && CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.Model.SettingsManagement.TenantSettingsTemplatePrivilegeConstants.EditTenantTemplate))
                    {
                        column.For(c => c.SettingsTemplate.TemplateType.Equals(SettingTemplateType.Fixed)
                            ? "-" : Html.CelloActionLink("Edit", "ManageTenantSettingsTemplate", new { settingTemplateId = c.SettingsTemplate.Id }).Replace("Edit", "<i class='fa fa-pencil'></i>"))
                            .Named(this.GetLocalResourceObject("EditTemplate").ToString())
                            .DoNotEncode()
                            .HeaderAttributes(@class => "noSortCol halign").Attributes(@class => "halign")
                            .Attributes(style => "text-align:center");
                    }
                    if (deleteAccess && CelloSaaS.Library.UserIdentity.HasPrivilege(CelloSaaS.Model.SettingsManagement.TenantSettingsTemplatePrivilegeConstants.DeleteTenantTemplate)
                    && (UserIdentity.LoggedInUserTenantId != ViewData["TenantId"].ToString() || CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId.Equals(ViewData["TenantId"].ToString(), StringComparison.InvariantCultureIgnoreCase))
                        )
                    {
                        column.For(c => "<a class='link' onclick='deleteTemplate(\"" + c.SettingsTemplate.Id + "\")'><i class='fa fa-trash-o'></i></a>")
                            .Named(this.GetGlobalResourceObject("General", "Delete").ToString())
                            .HeaderAttributes(@class => "noSortCol halign").Attributes(@class => "halign")
                            .DoNotEncode();
                    }

                }).Attributes(@class => "celloTable", id => "dataList").Render();
            }
            else
            {
        %>
        <div class="alert alert-info">
            <%: this.GetGlobalResourceObject("General","NoData") %>
        </div>
        <%
        }
        %>
    </div>
</div>
