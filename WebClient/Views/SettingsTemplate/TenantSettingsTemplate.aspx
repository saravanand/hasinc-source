﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Model.SettingsManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
            });
        });

        function viewTemplate(templateId, tenantId) {
            if (templateId == undefined) {
                alert('<%: this.GetLocalResourceObject("ChooseToView")%>');
                return false;
            }

            $(".viewTemplate").html('');

            $.ajax({
                url: '<%: Url.Action("ViewSettingsTemplate","SettingsTemplate") %>',
                type: 'POST',
                data: { 'settingTemplateId': templateId, 'tenantId': tenantId },
                beforeSend: function (xhr) {
                    $('#loading').show();
                },
                success: function (data) {
                    if (data !== undefined && data.length > 0) {
                        $(".viewTemplate").html(data);
                    }
                },
                complete: function (data) {
                    $('#loading').hide();
                }
            });
        }

        function deleteTemplate(templateId) {
            if (templateId == undefined) {
                alert('<%: this.GetLocalResourceObject("ChooseToDelete")%>');
                return false;
            }

            $(".viewTemplate").html('');

            var r = confirm('<%: this.GetLocalResourceObject("DeleteConfirmation")%>');
            if (r == true) {
                window.location = "/SettingsTemplate/DeleteTenantSettingsTemplate?templateId=" + templateId;
            }
        }
    </script>
    <style type="text/css">
        .link {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div id="ajaxMessages">
    </div>
    <%
        var editAccess = CelloSaaS.ServiceProxies.AccessControlManagement.TenantAccessProxy.CheckTenantAccess(
                new TenantSettingsTemplate().EntityIdentifier, TenantContext.GetTenantId(
                new TenantSettingsTemplate().EntityIdentifier), CelloSaaS.Model.FetchType.Edit, TenantContext.GetTenantId(
                new TenantSettingsTemplate().EntityIdentifier)) || true;
        var deleteAccess = CelloSaaS.ServiceProxies.AccessControlManagement.TenantAccessProxy.CheckTenantAccess(
            new TenantSettingsTemplate().EntityIdentifier, TenantContext.GetTenantId(
            new TenantSettingsTemplate().EntityIdentifier), CelloSaaS.Model.FetchType.Delete, TenantContext.GetTenantId(
                new TenantSettingsTemplate().EntityIdentifier)) || true;
    %>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title") %>
        </h3>
        <div class="pull-right">
            <%
                if (string.IsNullOrEmpty(UserIdentity.SessionTenantID))
                {
            %>
            <div class="tenantContextblock" style="float: left">
                <% 
                    ViewData["EnableTenantContext"] = "True";
                    ViewData["DemandedPrivilege"] = TenantSettingsTemplatePrivilegeConstants.ViewTenantTemplate;
                    Html.RenderPartial("TenantContextControl", new TenantSettingsTemplate().EntityIdentifier, ViewData);
                %>
            </div>
            <% } %>
            <% if (editAccess && CelloSaaS.Library.UserIdentity.HasPrivilege(TenantSettingsTemplatePrivilegeConstants.AddTenantTemplate)
                    && (UserIdentity.LoggedInUserTenantId != ViewData["TenantId"].ToString() || CelloSaaS.ServiceContracts.AccessControlManagement.ProductAdminConstants.ProductAdminTenantId.Equals(ViewData["TenantId"].ToString(), StringComparison.InvariantCultureIgnoreCase)))
                {
            %>
            <a class="btn btn-success" style="float: left; margin-left: 25px;" href="<%: Url.Action("AddTenantSettingsTemplate","SettingsTemplate") %>" title="<%: this.GetLocalResourceObject("MapTemplateForTenant")%>">
                <%: this.GetLocalResourceObject("MapTemplate")%></a>
            <%} %>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <div class="statusMessage">
            <%
                if (Html.ValidationMessage("SettingsTemplateSuccess") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateSuccess").ToString()))
                {	
            %>
            <div class="alert alert-success">
                <%: Html.ValidationMessage("SettingsTemplateSuccess")%>
            </div>
            <%
                }

                if (Html.ValidationMessage("SettingsTemplateError") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateError").ToString()))
                {	
            %>
            <div class="alert alert-danger">
                <%: Html.ValidationMessage("SettingsTemplateError")%>
            </div>
            <%
                    }
            %>
        </div>
        <div id="tenantSettingsTemplateDetailsList1">
            <% Html.RenderAction("TenantSetingsTemplateList", "SettingsTemplate"); %>
        </div>
        <div id="EditTemplatePartial">
        </div>
        <div class="viewTemplate">
        </div>
    </div>
</asp:Content>
