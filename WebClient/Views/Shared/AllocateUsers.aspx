﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Workflow.Master" Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowInstance>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container">
        <div class="heading_container">
            <h1>
                <%: this.GetLocalResourceObject("m_AllocateUser") %>
                <i>"<%:ViewData["TaskCode"]%>"</i>
            </h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a href="<%:Url.Action("Index","Workflow") %>" title="<%: this.GetLocalResourceObject("m_backMessage") %>">
                        <span>
                            <%=this.GetGlobalResourceObject("General","Back") %></span></a>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <% using (Html.BeginForm("AllocateUsers", "SampleWorkFlow"))
           {%>
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="error">
            <%=Html.ValidationSummary()%>
        </div>
        <% }
           else
           {
               if (ViewData["AllocateStatus"] != null && (int)ViewData["AllocateStatus"] > 0)
               { %>
        <div class="success">
            <b>
                <%: string.Format(this.GetLocalResourceObject("s_allocationSuccess").ToString(),ViewData["AllocateStatus"].ToString()) %></b>
        </div>
        <% }

               if (this.Model != null && this.Model.Count() > 0)
               { %>
        <div class="grid-part">
            <div id="divGrid">
                <% Html.Grid(this.Model).Columns(column =>
                   {
                       column.For(col => "<input type=\"CheckBox\" name=\"InstanceId\" value=\"" + col.Id + "\" />")
                           .Named("<input type=\"CheckBox\" id=\"selectAll\" name=\"selectAll\"/>")
                           .HeaderAttributes(style => "width:50px;text-align:center", @class => "noSortCol")
                           .Attributes(style => "width:50px;text-align:center;padding-right:20px")
                           .DoNotEncode();
                       column.For(col => col.Id).Named(this.GetLocalResourceObject("lbl_InstanceId").ToString());
                       column.For(col => col.MapId).Named(this.GetLocalResourceObject("lbl_MapId").ToString());
                       column.For(col => col.Status).Named(this.GetLocalResourceObject("lbl_Status").ToString());
                       column.For(col => col.CreatedOn).Named(this.GetLocalResourceObject("lbl_CreatedOn").ToString());
                   }).Attributes(@class => "celloTable", id => "dataList").Render(); %>
            </div>
            <div class="clear">
            </div>
            <div class="box1 btn-holder" style="line-height: 21px;">
                <div class="top-space">
                    <%: this.GetLocalResourceObject("lbl_SelectUser") %>
                    :
                </div>
                <div class="top-space">
                    <% if (ViewData["UserList"] != null)
                       { %>
                    <%=Html.DropDownList("userId", (IEnumerable<SelectListItem>)ViewData["UserList"], new { @class="slct-box1" })%>
                    <% } %>
                </div>
                <div class="button" style="margin: 5px 0px 0px 0px;">
                    <a href="#" onclick="$('form').submit();"><span>
                        <%: this.GetLocalResourceObject("lbl_Allocate") %>
                    </span></a>
                </div>
                <%=Html.Hidden("taskCode",ViewData["TaskCode"]) %>
                <%=Html.Hidden("isHold", ViewData["IsHold"])  %>
            </div>
        </div>
        <% }
               else
               { %>
        <div class="info">
            <%: this.GetLocalResourceObject("e_NoInstanceFound") %>
        </div>
        <% } %>
        <% } %>
        <%} %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            var noSortColumns = $("th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            } 
            
            $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray}]
            });

            $('#selectAll').click(function () {
                var status = $(this).is(':checked');
                $('table#dataList td input[type=Checkbox]').each(function () {
                    if (status)
                        $(this).attr('checked', 'checked');
                    else
                        $(this).attr('checked', null);
                });
            });

            $('input[name=InstanceId]').click(function () {
                var status = $(this).is(':checked');

                if (status) {
                    var selectedCount = $('table#dataList td input[type=Checkbox]:checked').length;
                    var totalCount = $('table#dataList td input[type=Checkbox]').length;
                    if (selectedCount == totalCount) {
                        $('#selectAll').attr('checked', 'checked');
                    }
                }
                else {
                    $('#selectAll').attr('checked', null);
                }
            });
        });
    </script>
</asp:Content>
