﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div id="silverlightControlHost" style="z-index: -1; height: 600px; width: 99.99%;">
    
    <%  
        string wcfUrl = string.Empty;
        if (string.IsNullOrEmpty(CelloSaaS.Library.Helpers.ConfigHelper.BusinessRuleWcfURL))
        {
            Uri uri = HttpContext.Current.Request.Url;
            string host = uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
            wcfUrl = host + "/RuleServices";
        }
        else
        {
            wcfUrl = CelloSaaS.Library.Helpers.ConfigHelper.BusinessRuleWcfURL;
        } %>
    <object data="data:application/x-silverlight-2," type="application/x-silverlight-2"
        width="100%" height="100%">
        <param name="source" value="/Silverlight/CelloSaaS.BusinessRules.UI.xap" />
        <param name="initParams" value="RuleSetCode=<%=ViewData["RuleName"]%>,TenantId=<%=ViewData["TenantId"]%>,ScreenType=<%=ViewData["ScreenType"] %>,WCFServiceURL=<%=wcfUrl %>" />
        <param name="onError" value="onSilverlightError" />
        <param name="background" value="white" />
        <param name="minRuntimeVersion" value="4.0.50826.0" />
        <param name="autoUpgrade" value="true" />
        <param name="windowless" value="true" />
        <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.50826.0" style="text-decoration: none">
            <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight"
                style="border-style: none" />
        </a>
    </object>
    <iframe id="_sl_historyFrame" style="visibility: hidden; height: 0px; width: 0px;
        border: 0px"></iframe>
</div>
