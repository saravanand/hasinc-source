﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container">
        <div class="heading_container">
            <h1>
            </h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a href="<%:Url.Action("Index","Workflow") %>" title="<%: this.GetLocalResourceObject("m_back") %>">
                        <span>
                            <%=this.GetGlobalResourceObject("General","Back") %></span></a>
                </div>
            </div>
        </div>
        <div class="box1">
            <p>
                <%: this.GetLocalResourceObject("m_Success") %>
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
