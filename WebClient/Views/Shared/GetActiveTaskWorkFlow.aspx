﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Workflow.Master" Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowTaskInstance>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container">
        <div class="heading_container">
            <h1>
                <%: this.GetLocalResourceObject("lbl_activeWorkflows") %>
            </h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a href="<%:Url.Action("Index","Workflow") %>" title="<%: this.GetLocalResourceObject("m_backtoWFList") %>">
                        <span>
                            <%=this.GetGlobalResourceObject("General","Back") %></span></a>
                </div>
            </div>
        </div>
        <div class="grid-part">
            <% if (this.Model != null && this.Model.Count() > 0)
               {
                   Html.Grid(this.Model).Columns(column =>
                   {
                       column.For(wfti => wfti.Task.TaskDefinition.TaskCode).Named(this.GetLocalResourceObject("lbl_TaskCode").ToString());
                       column.For(wfti => wfti.Task.TaskDefinition.TaskName).Named(this.GetLocalResourceObject("lbl_TaskName").ToString());
                       column.For(wfti => wfti.CurrentExecutionStatus).Named(this.GetLocalResourceObject("lbl_CurrentStatus").ToString());
                       //column.For(wfti => !wfti.CurrentExecutionStatus.Equals("Completed") ? "<a href='" + (wfti.Task.TaskDefinition as CelloSaaS.WorkFlow.ManualTaskDefinition).Url + "?taskCode=" + wfti.Task.TaskDefinition.TaskCode + "&instanceId=" + wfti.WorkFlowInstanceId + "'>" + (wfti.Task.TaskDefinition as CelloSaaS.WorkFlow.ManualTaskDefinition).Url + "</a>" : "Completed").DoNotEncode().Named(this.GetLocalResourceObject("lbl_Url").ToString());
                   }).Attributes(@class => "celloTable", id => "dataList").Render();
               }
               else
               { %>
            <div class="info">
                <%: this.GetLocalResourceObject("lbl_NoPendingTasks") %></div>
            <% } %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>
</asp:Content>
