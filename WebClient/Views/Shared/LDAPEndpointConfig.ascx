﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%
    string className = "col-md-6";
    if (Model == true)
    {
        className = string.Empty;
    }

    string uriValue = null;
    if (ViewBag.OnPremiseLDAPUri != null)
    {
        uriValue = ViewBag.OnPremiseLDAPUri as string;
    }
%>

<div class="ldapConfig" data-bind="with: LDAPConfig">

    <div class="<%:className %>">
        <div class="form-group">
            <label for="OnPremiseServerUri">On-Premise Server Uri</label>
            <input type="text" class="form-control" placeholder="On-Premise Server Uri [https://stg.identity.cello.com/]" autofocus="" name="OnPremiseServerUri" value="<%:uriValue %>" />
        </div>
    </div>
</div>
