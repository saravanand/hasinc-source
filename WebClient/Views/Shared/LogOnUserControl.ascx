﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    if (Request.IsAuthenticated)
    {
%>
<%= Html.CelloActionLink(this.GetGlobalResourceObject("General", "LogOff").ToString(), "LogOff", "Account", new { area="" }, null)%>
<%
    }
    else
    {
%>
<%= Html.CelloActionLink(this.GetGlobalResourceObject("General", "LogIn").ToString(), "LogOn", "Account") %>
<%
    }
%>
