﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CelloSaaS.Model.LicenseManagement.TenantLicense>>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<% if (Model != null && Model.Count > 0)
   {
       var pricePlans = ViewData["PricePlans"] as Dictionary<Guid, PricePlan>;
%>
<div class="modal fade" id="tenantLicenseHistory" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px; left: -16%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;</button>
                <h4 class="modal-title"><%: this.GetLocalResourceObject("h_SubscriptionHistory") %></h4>
            </div>
            <div class="modal-body">
                <div style="min-height: 250px;">
                    <%
       Html.Grid(Model.OrderByDescending(x => x.ValidityStart)).Columns(col =>
       {
           col.For(x => x.PackageName).Named(this.GetLocalResourceObject("lbl_PackageName").ToString());
           col.For(x => !string.IsNullOrEmpty(x.PricePlanId)
               && pricePlans.ContainsKey(Guid.Parse(x.PricePlanId))
               ? pricePlans[Guid.Parse(x.PricePlanId)].Name
               : "-").Named(this.GetLocalResourceObject("lbl_PricePlan").ToString());
           col.For(x => x.ValidityStart.ToUIDateTimeString()).Named(this.GetLocalResourceObject("lbl_Startdate").ToString());
           col.For(x => x.ValidityEnd.HasValue ? x.ValidityEnd.Value.ToUIDateTimeString() : "--").Named(this.GetLocalResourceObject("lbl_Enddate").ToString());
           col.For(x => x.TrialEndDate.HasValue ? x.TrialEndDate.Value.ToUIDateTimeString() : "--").Named(this.GetLocalResourceObject("lbl_TrialEnddate").ToString());
           col.For(x => (x.ValidityEnd.HasValue && x.ValidityEnd.Value < DateTime.Now) ? "Expired" : "Active")
                        .Named(this.GetLocalResourceObject("lbl_Status").ToString())
                        .Attributes(@class => "halign").HeaderAttributes(@class => "halign")
                        .DoNotEncode();

       }).Attributes(@class => "celloTable").Render();
                    %>
                </div>
            </div>
            <div class="modal-footer">
                <div class="actions">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Close") %> </button>
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>