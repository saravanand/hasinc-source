﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>

<% 
    string tenantId = UserIdentity.TenantID;
    if (!string.IsNullOrEmpty(UserIdentity.SessionTenantID))
    {
        var tenantsDetails = TenantProxy.GetTenantDetailsForShareUsers(new string[] { tenantId });
        var tenantDetails = tenantsDetails != null && tenantsDetails.ContainsKey(tenantId) ? tenantsDetails[tenantId] : new CelloSaaS.Model.TenantManagement.TenantDetails();
%>
<a href="<%: Url.Action("SessionTenant","Home", new {area = ""}) %>" title="<%: this.GetLocalResourceObject("t_ResetSessionTenant") %>">
    <i class="fa fa-exchange"></i>&nbsp;[<%: tenantDetails.TenantName%>]</a>
<%
        }
        else
        {
%>
<a href="<%: Url.Action("SessionTenant","Home", new {area = ""}) %>" title="<%: this.GetLocalResourceObject("t_SetSessionTenant") %>">
    <i class="fa fa-exchange"></i>&nbsp;<%: this.GetLocalResourceObject("lbl_ChangeTenant") %></a>
<% } %>