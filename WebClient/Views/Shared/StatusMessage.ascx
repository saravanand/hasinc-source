﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% if (Html.ValidationMessage("Success") != null)
   { %>
<div class="alert alert-success success">
    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
    <%=Html.ValidationMessage("Success") %>
</div>
<% } %>
<% if (Html.ValidationMessage("Error") != null)
   { %>
<div class="alert alert-danger danger">
    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
    <%=Html.ValidationMessage("Error")%>
</div>
<% } %>
<% if (TempData["Success"] != null)
   { %>
<div class="alert alert-success success">
    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
    <%=TempData["Success"] %>
</div>
<% } %>
<% if (TempData["Error"] != null)
   { %>
<div class="alert alert-danger danger">
    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
    <%=TempData["Error"]%>
</div>
<% } %>