﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<%
    // map tenant templates need this to be true
    if (Convert.ToBoolean(ViewData["EnableTenantContext"] ?? "False") || UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
    {
%>
<label>
    <%=this.GetGlobalResourceObject("General","Tenant") %>:</label>
<select name="chnTenantContext" id="chnTenantContext" style="width:180px;">
</select>
<input type="hidden" name="tenantContextId" id="tenantContextId" value="<%:CelloSaaS.Library.UserIdentity.TenantID %>" />
<script type="text/javascript">
    $(function () {
        var entitId = '<%=this.Model %>';
        var cookieName = 'TenantContextCookie';

        var privilege = '<%= ViewData["DemandedPrivilege"] !=null ? ViewData["DemandedPrivilege"].ToString() : string.Empty%>';
        var postData = { 'entityId': entitId, 'privilege': privilege };

        $.post('<%=Url.Action("GetTenantsForCurrentUser","Home",new{area=""}) %>', postData, function (data) {
            if (data.Error) {
                console.log(data.Error);
                return;
            }

            if (data.length == 0) {
                $("#chnTenantContext").parent().remove();
                return;
            }

            var currentTenantId = getTenantCtxTenantId();
            var tenantData = data;

            for (var i = 0; i < tenantData.length; ++i) {
                var opt = '<option value="' + tenantData[i].TenantId + '"';
                if (tenantData[i].TenantId == currentTenantId) {
                    opt += ' selected';
                }
                opt += '>' + tenantData[i].TenantName + '</option>';
                $(opt).data('id', tenantData[i].TenantId);
                $("#chnTenantContext").append(opt);
            }

            $("#chnTenantContext").select2()
                .on('change', function (e, ui) {
                    var id = $('option:selected', $("#chnTenantContext")).val();
                    var name = $('option:selected', $("#chnTenantContext")).text();
                    setTenantCtxCookie(id, name);
                    window.location.reload();
                });
        }, 'json');

        function getTenantCtxTenantId() {
            if ($.cookie(cookieName)) {
                var entityTenants = $.cookie(cookieName).split('^');
                for (var i = 0; i < entityTenants.length && entityTenants[i]; i++) {
                    var tmp = entityTenants[i].split('~');
                    if (tmp.length == 3 && tmp[0] == entitId) {
                        return tmp[1];
                    }
                }
            }
            return $('#tenantContextId').val();
        }

        function setTenantCtxCookie(id, name) {
            if (!id || !name) return;
            var cookieValue = '';
            if ($.cookie(cookieName)) {
                var found = false;
                var entityTenants = $.cookie(cookieName).split('^');
                for (var i = 0; i < entityTenants.length && entityTenants[i]; i++) {
                    var tmp = entityTenants[i].split('~');
                    if (tmp.length == 3 && tmp[0] == entitId) {
                        tmp[1] = id;
                        tmp[2] = name;
                        found = true;
                    }
                    cookieValue = cookieValue + tmp[0] + '~' + tmp[1] + '~' + tmp[2] + '^';
                }
                if (!found) {
                    cookieValue = cookieValue + entitId + '~' + id + '~' + name + '^';
                }

            } else {
                cookieValue = entitId + '~' + id + '~' + name + '^';
            }

            $.cookie(cookieName, cookieValue, { expires: 1, path: '/', raw: true });
        }
    });
</script>
<%
    }
%>
