﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.SettingsManagement.SettingsTemplate>" %>
<%@ Import Namespace="CelloSaaS.Model.SettingsManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.SettingsManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div>
    <%
        if (ViewData["tenantProvisioning"] != null && !bool.Parse(ViewData["tenantProvisioning"].ToString()))
        {
    %>
    <form action="<%: Url.Action("SaveTenantSettingsTemplatesDetails","SettingsTemplate") %>"
        method="post" id="addTenantTemplate" enctype="multipart/form-data">
        <%
        }
        %>
        <section class="panel indigo">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("TemplateDetails")%></h4>
            </header>
            <div class="panel-body">
                <table class="celloTable">
                    <colgroup>
                        <col style="width: 40%;" />
                        <col style="width: auto;" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th>
                                <%: this.GetLocalResourceObject("TemplateDetails")%>
                            </th>
                            <th>
                                <%: this.GetLocalResourceObject("Value")%>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <%: Html.Label(this.GetLocalResourceObject("Name").ToString())%>
                            </td>
                            <td>
                                <%: this.Model.Name%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%:Html.Label(this.GetLocalResourceObject("Type").ToString())%>
                            </td>
                            <td>
                                <%: this.Model.TemplateType %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <%: Html.HiddenFor(model=> model.Id) %>
                <%: Html.HiddenFor(model => model.Name)%>
                <%: Html.HiddenFor(model=>model.TenantId) %>
                <%: Html.HiddenFor(model=>model.IsGlobal) %>
                <%: Html.HiddenFor(model=>model.TemplateType) %>
            </div>
        </section>
        <section class="panel green">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("Attributes")%>
                </h4>
            </header>
            <div class="panel-body">
                <%
                    Dictionary<string, SettingsAttribute> settingsAttributes = (Dictionary<string, SettingsAttribute>)ViewData["SettingsAttribute"];

                    var attributeCount = -1;
                %>
                <div class="form-container">
                    <table class="celloTable" id="templateAttributes">
                        <colgroup>
                            <col style="width: 40%;" />
                            <col style="width: auto;" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>
                                    <%: this.GetLocalResourceObject("AttributeName")%>
                                </th>
                                <th>
                                    <%: this.GetLocalResourceObject("AttributeValue")%>
                                </th>
                            </tr>
                        </thead>
                        <%
                            string attributeId = AttributeConstants.WCFSharedKey;
                            string value = string.Empty;
                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%: attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("WCFSharedKey").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   {
                                       var sharedKey = string.Empty;
                                       for (var i = 0; i < value.Length; i++)
                                       {
                                           sharedKey += "*";
                                       }
                                %>
                                <%: Html.Label(sharedKey)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%:Html.Password("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.TenantAuthenticateSetting;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("TenantAuthenticateSetting").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.UserPasswordExpiration;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;

                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("UserPasswordExpiration").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.ApplicationConnectionString;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;

                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%:Html.Label(this.GetLocalResourceObject("ApplicationConnectionString").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.HomeRealm;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;

                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("HomeRealm").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.ShareUsers;
                            bool attributeValue = false;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                attributeValue = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? false : bool.Parse(settingsAttributes[attributeId].AttributeValue);
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("LinkByOtherTenantUsers").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: attributeValue ? Html.Label(this.GetLocalResourceObject("Enabled").ToString()) : Html.Label(this.GetLocalResourceObject("Disabled").ToString())%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",attributeValue)
                                %>
                                <%}
                                   else
                                   { %>
                                <%=Html.CheckBox("Attributes[" + attributeCount + "].AttributeValue", attributeValue, new { style = "width:20px;" })%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.MaxPasswordAnswerFailureCount;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("MaxPasswordAnswerFailureCount").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%: Html.TextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.MaxPasswordFailureCount;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("MaxPasswordFailureCount").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%=Html.CelloTextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.UserDetailsConnectionString;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? string.Empty : settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId  %>" />
                                <%:Html.Label(this.GetLocalResourceObject("UserDetailsConnectionString").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%=Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%}
                                   else
                                   { %>
                                <%=Html.CelloTextBox("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.AutoApprovalTenantCreation;
                            attributeValue = false;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                attributeValue = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? false : bool.Parse(settingsAttributes[attributeId].AttributeValue);
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("AutoApprovalTenantCreation").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: attributeValue?Html.Label("Enabled"):Html.Label("Disabled") %>
                                <%=Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", attributeValue)%>
                                <%}
                                   else
                                   { %>
                                <%=Html.CheckBox("Attributes[" + attributeCount + "].AttributeValue", attributeValue, new { style = "width:20px;" })%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.Theme;
                            value = string.Empty;
                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("Theme").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount+ "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.DropDownList("Attributes[" + attributeCount + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["Themes"], new { style="width:100%;" })%>
                                <%=Html.CelloValidationMessage("ViewMetaData.Theme", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.Logo;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%:Html.Label(this.GetLocalResourceObject("Logo").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <img src="<%=this.ResolveClientUrl("~" + ViewData["ImageURL"])%>" alt="Logo" />
                                <%: Html.Hidden("Attributes["+ attributeCount + "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <input id="logoPath" name="logoPath" value="ViewMetaData.Logo" type="file" contenteditable="false" />
                                <%= Html.Hidden("Attributes[" + attributeCount + "].AttributeValue", value)%>
                                <br />
                                <img src="<%=this.ResolveClientUrl("~" +  ViewData["ImageURL"])%>" alt="Logo" />
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.DateFormat;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <%--<%: this.GetLocalResourceObject("DateFormat").ToString()%>--%>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%:Html.Label(this.GetLocalResourceObject("DateFormat").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%:Html.Label(value)%>
                                <%: Html.Hidden("Attributes["+ attributeCount+ "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.DropDownList("Attributes[" + attributeCount + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["DateFormats"], new { style="width:100%;" })%>
                                <%=Html.CelloValidationMessage("dateFormat", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.Language;
                            value = string.Empty;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                value = settingsAttributes[attributeId].AttributeValue;
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <%--<%: this.GetLocalResourceObject("Language").ToString()%>--%>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId%>" />
                                <%:Html.Label(this.GetLocalResourceObject("Language").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: Html.Label(value)%>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",value) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.DropDownList("Attributes[" + attributeCount + "].AttributeValue", (IEnumerable<SelectListItem>)ViewData["Languages"], new { style="width:100%;" })%>
                                <%=Html.CelloValidationMessage("language", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                        <%
                            attributeId = AttributeConstants.DisableProductAnalytics;
                            attributeValue = false;

                            if (settingsAttributes.ContainsKey(attributeId))
                            {
                                attributeValue = string.IsNullOrEmpty(settingsAttributes[attributeId].AttributeValue) ? false : bool.Parse(settingsAttributes[attributeId].AttributeValue);
                                attributeCount++;
                        %>
                        <tr>
                            <td>
                                <input type="hidden" name="Attributes[<%:attributeCount %>].AttributeId" value="<%:attributeId %>" />
                                <%:Html.Label(this.GetLocalResourceObject("DisableProductAnalytics").ToString())%>
                            </td>
                            <td>
                                <% if (this.Model.TemplateType == SettingTemplateType.Fixed)
                                   { %>
                                <%: attributeValue?Html.Label("Enabled"):Html.Label("Disabled") %>
                                <%: Html.Hidden("Attributes[" + attributeCount + "].AttributeValue",attributeValue.ToString()) %>
                                <%}
                                   else
                                   { %>
                                <%=Html.CheckBox("Attributes[" + attributeCount + "].AttributeValue", attributeValue, new { style = "width:20px;" })%>
                                <%=Html.CelloValidationMessage("EnableProductAnalytics", "*")%>
                                <%} %>
                            </td>
                        </tr>
                        <% } %>
                    </table>
                </div>
            </div>
        </section>
        <div class="pull-right">
            <%
                // NOT for Tenant Provisioning
                if (ViewData["tenantProvisioning"] != null && !bool.Parse(ViewData["tenantProvisioning"].ToString()))
                {
            %>
            <a class="btn btn-info" onclick="javascript: $('#addTenantTemplate').submit()">Save</a>
            <% }
                else
                { %>
            <a class="btn btn-info" onclick="AddTemplateDetails()">
                <%=this.GetGlobalResourceObject("General", "Save")%></a>
            <a class="btn btn-default" onclick="IgnoreTemplateDetails()">
                <%=this.GetGlobalResourceObject("General", "Cancel")%></a>
            <% } %>
        </div>
    </form>
</div>
<script type="text/javascript">
    $().ready(function () {
        $("#dateFormatPreview").click(function () {
            $.ajax({
                url: '<%: Url.Action("GetGivenFormattedDate","SettingsTemplate") %>',
                type: 'POST',
                data: { 'dateFormat': $(".DateFormatClass").val() },
                beforeSend: function (xhr) {
                    //$(".loadingContent").show();
                },
                success: function (data) {
                    if (data !== undefined && data.length > 0) {
                        $("#sampleDateFormatPvw").html('<%: this.GetLocalResourceObject("Preview") %> : ' + data);
                    }
                },
                complete: function (data) {
                    //$(".loadingContent").hide();
                }
            });
        });

        $('#addTenantTemplate select').select2();
    });
</script>
