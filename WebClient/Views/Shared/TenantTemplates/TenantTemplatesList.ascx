﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% using (Html.BeginForm("TenantSettingsTemplateDetails", "SettingsTemplate", FormMethod.Post, new { id = "TenantSettingTemplateForm" }))
   {
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4>Choose a template</h4>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="form-container">
                    <div class="form-group">
                        <%: Html.Label(this.GetLocalResourceObject("AvailableTemplates").ToString())%>
                        <%: Html.Hidden("DefaultTemplateId",CelloSaaS.Model.SettingsManagement.TenantSettingTemplateConstants.DefaultTemplateId) %>
                        <%= Html.DropDownList("settingsTemplateId", null, new { onchange = "onTemplateSelected(true)", style="width:100%;" })%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(function () {
        $("#TenantSettingTemplateForm").submit(function () {
            $.post($(this).attr('action'), $(this).serialize(), function (data) {
                $("#templateDetails").html(data);
            }).error(function () {
                $("#templateDetails").html('<div class="alert alert-danger"><%: this.GetLocalResourceObject("TemplateLoadError").ToString() %></div>');
            });
            return false;
        });

        $('#settingsTemplateId').select2();
    });
</script>
<% } %>