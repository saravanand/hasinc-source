﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.SettingsManagement.SettingsTemplate>" %>
<%@ Import Namespace="CelloSaaS.Model.SettingsManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.SettingsManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4><%: this.GetLocalResourceObject("TemplateDetails")%></h4>
    </header>
    <div class="panel-body">
        <%
            if (Html.ValidationMessage("SettingsTemplateSuccess") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateSuccess").ToString()))
            {	
        %>
        <div class="alert alert-success">
            <%: Html.ValidationMessage("SettingsTemplateSuccess")%>
        </div>
        <%
            }

            if (Html.ValidationMessage("SettingsTemplateError") != null && !string.IsNullOrEmpty(Html.ValidationMessage("SettingsTemplateError").ToString()))
            {	
        %>
        <div class="alert alert-danger">
            <%: Html.ValidationMessage("SettingsTemplateError")%>
        </div>
        <%
            }
        %>
        <table class="celloTable">
            <colgroup>
                <col style="width: 40%" />
                <col style="width: auto" />
            </colgroup>
            <thead>
                <tr>
                    <th>
                        <%: this.GetLocalResourceObject("TemplateDetails")%>
                    </th>
                    <th>
                        <%: this.GetLocalResourceObject("Value")%>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <%: Html.Label(this.GetLocalResourceObject("Name").ToString())%>
                    </td>
                    <td>
                        <%
                            if (this.Model != null)
                            {
                        %>
                        <%: this.Model.Name %>
                        <%
                            }%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.Label(this.GetLocalResourceObject("Type").ToString())%>
                    </td>
                    <td class="row5">
                        <%: this.Model.TemplateType %>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.Label(this.GetLocalResourceObject("GlobalTemplate").ToString())%>
                    </td>
                    <td>
                        <%if (this.Model.IsGlobal)
                          { %>
                        <span>
                            <%: this.GetLocalResourceObject("Yes")%>
                        </span>
                        <%}
                          else
                          { %>
                        <span>
                            <%: this.GetLocalResourceObject("No")%>
                        </span>
                        <%} %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</section>
<% if (this.Model != null)
   {
%>
<section class="panel indigo">
    <header class="panel-heading">
        <h4><%: this.GetLocalResourceObject("AttributeDetails")%></h4>
    </header>
    <div class="panel-body">
        <table class="celloTable">
            <colgroup>
                <col style="width: 40%" />
                <col style="width: auto" />
            </colgroup>
            <thead>
                <tr>
                    <th>
                        <%:this.GetLocalResourceObject("AttributeName").ToString()%>
                    </th>
                    <th>
                        <%:this.GetLocalResourceObject("AttributeValue").ToString()%>
                    </th>
                </tr>
            </thead>
            <tbody>
                <% 
               SettingsMetaData settingsMetaData = new SettingsMetaData();
               settingsMetaData = (SettingsMetaData)ViewData["SettingsMetaData"];

               var attrMetaData = from sm in settingsMetaData.AttributeMetaData.Values
                                  select sm;

               foreach (var attribute in attrMetaData.OrderBy(p => p.AttributeSetId))
               {
                   SettingsAttribute attributeData = null;

                   if (this.Model != null && Model.Attributes != null && Model.Attributes.Count > 0)
                   {
                       attributeData = Model.Attributes.Where(p => p.AttributeId.Equals(attribute.AttributeId, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
                   }
                   if (attributeData != null)
                   {
                       string attributeValue = attributeData.AttributeValue;
                       //if (!string.IsNullOrEmpty(attributeData.AttributeValue))
                       //{
                %>
                <tr>
                    <td>
                        <%: Html.Label(this.GetLocalResourceObject(attribute.AttributeId).ToString())%>
                    </td>
                    <td>
                        <%
                           if (attribute.AttributeId.Equals(AttributeConstants.Logo))
                           {
                               var logoPath = ViewData["ImageURL"] + attributeValue;
                        %>
                        <img src='../..<%: logoPath%>' alt="Logo" />
                        <%}
                               else if (attribute.AttributeId.Equals(AttributeConstants.WCFSharedKey))
                               {
                                   var sharedKey = string.Empty;
                                   for (var i = 0; i < attributeValue.Length; i++)
                                   {
                                       sharedKey += "*";
                                   }
                        %>
                        <%: sharedKey%>
                        <%
                               }
                               else if (attribute.AttributeId.Equals(AttributeConstants.DisableProductAnalytics) || attribute.AttributeId.Equals(AttributeConstants.AutoApprovalTenantCreation) || attribute.AttributeId.Equals(AttributeConstants.ShareUsers))
                               {
                                   if (Convert.ToBoolean(attributeValue))
                                   {	
                        %>
                        <%: Html.Label(this.GetLocalResourceObject("Enabled").ToString())%>
                        <%}
                                   else
                                   {%>
                        <%: Html.Label(this.GetLocalResourceObject("Disabled").ToString())%>
                        <%     }
                               }
                               else if(attribute.AttributeId.Equals(AttributeConstants.DateFormat) && !string.IsNullOrEmpty(attributeValue))
                               {%>
                                   <%:string.Format("{1} ({0})", attributeValue, DateTime.Now.ToString(attributeValue))%>
                               <%}
                               else
                               { %>
                        <%:attributeValue%>
                        <%
                               }%>
                    </td>
                    <%
                           //}
                       }
                    %>
                </tr>
                <% } %>
            </tbody>
        </table>
    </div>
</section>
<% } %>