﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.LicenseManagement.TenantLicense>" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>
<%
    var tenantLicense = this.Model;
    int expireDays = tenantLicense != null && tenantLicense.TrialEndDate.HasValue ? (int)(tenantLicense.TrialEndDate.Value - DateTime.Now).TotalDays : -1;
    var packageDetails = ViewData["TrialAlert_PackageDetails"] as PackageDetails;
    if (tenantLicense != null && packageDetails != null
        && tenantLicense.TrialEndDate.HasValue && (expireDays >= 0 || packageDetails.IsEvaluation))
    {
%>
<a class="btnAlert" href="<%=Url.Action("ManageSettings","MySettings", new {area = ""}) %>" title="<%: this.GetLocalResourceObject("t_UpgradeSubscription") %>">
    <% if (expireDays > 0)
       {%>
    <%: this.GetLocalResourceObject("lbl_Trialends") %> <span class="label label-danger animated fadeIn"><%=expireDays %></span> <%: this.GetLocalResourceObject("lbl_days") %>
    <% }
       else if (tenantLicense.TrialEndDate.Value > DateTime.Now)
       { %>
    <% if ((tenantLicense.TrialEndDate.Value - DateTime.Now).TotalMinutes > 60)
       { %>
    <%: this.GetLocalResourceObject("lbl_Trialends") %> <span class="label label-danger animated fadeIn"><%:(int)(tenantLicense.TrialEndDate.Value-DateTime.Now).TotalHours %></span>  <%: this.GetLocalResourceObject("lbl_hours") %>
    <% }
       else
       { %>
    <%: this.GetLocalResourceObject("lbl_Trialends") %>  <span class="label label-danger animated fadeIn"><%:(int)(tenantLicense.TrialEndDate.Value-DateTime.Now).TotalMinutes %></span>  <%: this.GetLocalResourceObject("lbl_minutes") %>
    <% } %>
    <% }
       else if (tenantLicense.TrialEndDate.Value < DateTime.Now && packageDetails.IsEvaluation)
       { %>
    <% if ((int)(DateTime.Now - tenantLicense.TrialEndDate.Value).TotalMinutes > 60)
       { %>
    <%: this.GetLocalResourceObject("lbl_Trialended") %> <span class="label label-danger animated fadeIn"><%:(int)(DateTime.Now -tenantLicense.TrialEndDate.Value).TotalHours%></span>  <%: this.GetLocalResourceObject("lbl_hoursago") %>
    <% }
       else
       { %>
    <%: this.GetLocalResourceObject("lbl_Trialended") %> <span class="label label-danger animated fadeIn"><%:(int)(DateTime.Now -tenantLicense.TrialEndDate.Value).TotalMinutes%></span>  <%: this.GetLocalResourceObject("lbl_minutesago") %>
    <% } %>
    <% }
       else
       { %>
    <%: this.GetLocalResourceObject("lbl_Trialended") %>!
    <% } %>
</a>
<% } %>