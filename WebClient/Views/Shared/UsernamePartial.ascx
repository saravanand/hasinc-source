﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    var tenantSettings = UserIdentity.TenantSettings;
    bool shareUsers = tenantSettings != null && tenantSettings.ContainsKey(CelloSaaS.ServiceContracts.SettingsManagement.AttributeConstants.ShareUsers) && Convert.ToBoolean(tenantSettings[CelloSaaS.ServiceContracts.SettingsManagement.AttributeConstants.ShareUsers]);
%>

<a id="btnMyDetailsdp" class="dropdown-toggle" data-toggle="dropdown" href="#">
    <%=UserIdentity.Name %>
    <i class="fa fa-caret-down"></i>
</a>
<ul class="dropdown-menu animated fadeInLeft" role="menu" aria-labelledby="dropdownMenu">
    <% if (UserIdentity.HasPrivilege(PrivilegeConstants.ViewSelfUser) && UserIdentity.HasPrivilege(PrivilegeConstants.EditSelfUser))
       { %>
    <li><a tabindex="-1" href="<%=Url.Action("UserSettings","MySettings",new{area=""}) %>"><%: this.GetLocalResourceObject("lbl_MyProfile") %></a></li>

    <%
           var tenantSetting = UserIdentity.TenantSettings;
           bool allowChangePassword = false;

           if (tenantSetting != null && tenantSetting.Count > 0 && tenantSetting.ContainsKey(CelloSaaS.ServiceContracts.SettingsManagement.SettingAttributeConstants.IdProviders)
               && tenantSetting[CelloSaaS.ServiceContracts.SettingsManagement.SettingAttributeConstants.IdProviders].Equals(CelloSaaS.Library.Helpers.ConfigHelper.CelloOpenIdProvider, StringComparison.OrdinalIgnoreCase))
           {
               allowChangePassword = true;
           }

           if (!CelloSaaS.Library.Helpers.ConfigHelper.EnableExternalAuthentication)
           {
               allowChangePassword = true;
           }

           if (allowChangePassword)
           { 
    %>
    <li><a tabindex="-1" href="<%=Url.Action("ChangePassword","Account",new{area=""}) %>"><%: this.GetLocalResourceObject("lbl_ ChangePassword") %></a></li>
    <% }
       } %>
    <li><a tabindex="-1" href="<%=Url.Action("MyNotificationDetails","NotificationConfig",new{area=""}) %>"><%: this.GetLocalResourceObject("lbl_MyNotifications") %></a></li>
    <%if (shareUsers)
      { %>
    <li><a tabindex="-1" href="<%=Url.Action("LinkedByOtherTenantsList","TenantUserAssociation",new{area=""}) %>"><%: this.GetLocalResourceObject("lbl_MembershipRequests") %></a></li>
    <% } %>
    <li><a tabindex="-1" href="<%=Url.Action("ActorWorkflowTaskInstance","WorkflowInstance",new{area=""}) %>"><%: this.GetLocalResourceObject("lbl_MyPendingTasks") %></a></li>
    <li class="divider"></li>
    <li><a tabindex="-1" href="<%=Url.Action("LogOff","Account",new{area=""}) %>"><i class="fa fa-sign-out"></i>&nbsp; <%: this.GetLocalResourceObject("lbl_LogOff") %></a></li>
</ul>
