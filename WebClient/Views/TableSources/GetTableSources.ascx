﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%    
    if (ViewData["viewObjectId"] == null)
    {
        ViewData["viewObjectId"] = Guid.NewGuid().ToString();
    }

    string viewObjectId = ViewData["viewObjectId"].ToString();
%>
<div class="tableSourcesForms" id="<%:viewObjectId %>_tableSource">
    <%
        if (ViewData["statusMessage"] != null)
        {
    %>
    <div class="tableSource_Messages">
        <%: ViewData["statusMessage"].ToString() %>
    </div>
    <%
        }
    %>
    <table style="width: 100%;">
        <tr>
            <td>
                <%: Html.Label("Available Table Sources") %>
            </td>
            <td>
                <%: Html.Hidden("viewObjectId", viewObjectId) %>
                <%: Html.DropDownList("TableSourcesList", null, new { @onChange = "setTableSourceId('" + viewObjectId + "')", @class = "TableSourcesList" })%>
            </td>
        </tr>
    </table>
</div>
