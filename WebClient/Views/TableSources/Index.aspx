﻿<%@ Page Title="<%$Resources:PageTitle%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.Reporting.DataSources.Model.TableSource>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container">
        <div class="heading_container">
            <h1>
                <%:this.GetLocalResourceObject("ManageTableSources")%>
            </h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a title="Add New Table Source" href="<%: Url.Action("ManageTableSources","TableSources")%>">
                        <span>
                            <%:this.GetLocalResourceObject("Add")%></span> </a>
                </div>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Success")))
          { %>
        <div class="success">
            <%= Html.CelloValidationMessage("Success", new { @class = "success1" })%>
        </div>
        <%} %>
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
          { %>
        <div class="success">
            <%= Html.CelloValidationMessage("Error", new { @class = "error1" })%>
        </div>
        <%} %>
        <div id="divGrid">
            <%
                if (this.Model != null && this.Model.Count() > 0)
                {
            %>
            <div class="secondary_search_container">
                <div class="secondary_search_holder">
                    <div class="search_holder">
                        <input type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>"
                            id="searchText" name="searchText" />
                    </div>
                    <div class="go_but_grey">
                        <a title="GO" onclick="DoSearch();">
                            <%=this.GetGlobalResourceObject("General","GO") %></a></div>
                </div>
            </div>
            <div class="clear">
                <%
                    string editUrl = this.ResolveClientUrl("../../App_Themes/CelloSkin/edit_icon.gif");
                    string deleteUrl = this.ResolveClientUrl("../../App_Themes/CelloSkin/remove_but.gif");
                    Html.Grid(this.Model).Columns(column =>
                    {
                        column.For(c => c.Name).Named("Name");
                        column.For(c => c.Description).Named("Description");
                        column.For(c => "<a href='/TableSources/ManageTableSources?sourceId=" + c.Identifier + "'><img src='" + editUrl + "' /></a>").Named("Edit").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                        column.For(c => "<a href='/TableSources/DeleteSource?sourceId=" + c.Identifier + "'><img src='" + deleteUrl + "' /></a>").Named("Delete").DoNotEncode().Attributes(style => "text-align:center").HeaderAttributes(@class => "noSortCol");
                    }).Attributes(id => "tableSourcesList", @class => "celloTable").Render();
                %>
            </div>
            <%
                }
                else
                {
            %>
            <div class="info">
                <%:this.GetLocalResourceObject("NoTableSourcesFound")%>
            </div>
            <%
                }       
            %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="<%: this.ResolveClientUrl("../../Scripts/json2.js") %>" type="text/javascript"></script>
    <script src="<%: this.ResolveClientUrl("../../Scripts/managetablesources.js")%>"
        type="text/javascript"></script>
    <script src="<%: this.ResolveClientUrl("../../Scripts/reporting_common.js") %>" type="text/javascript"></script>
    <script type="text/javascript">

        var thisDataTable = null;
        $(document).ready(function () {
            thisDataTable = $('#tableSourcesList').dataTable({
                "bFilter": true,
                "bInfo": true,
                "sPaginationType": "full_numbers",
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray}]
            });
        });


        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            thisDataTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
</asp:Content>
