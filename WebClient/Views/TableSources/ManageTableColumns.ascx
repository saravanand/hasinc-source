﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Reporting.DataSources.Model" %>
<link href="<%:Url.Content("../../Content/ReportBuilder.css")%>" rel="stylesheet"
    type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.js" type="text/javascript"></script>
<%
    if (ViewData["viewObjectId"] == null)
    {
        ViewData["viewObjectId"] = Guid.NewGuid().ToString();
    }

    string reportObjectId = string.Empty;

    if (ViewData["reportObjectId"] != null)
    {
        reportObjectId = ViewData["reportObjectId"].ToString();
    }

    string tableSourceId = string.Empty;

    if (ViewData["tableSourceId"] != null)
    {
        tableSourceId = ViewData["tableSourceId"].ToString();
    }

    string reportObjectViewId = ViewData["viewObjectId"].ToString();

    string reportId = ViewData["reportId"].ToString();

    var tableResultColumns = (Dictionary<string, TableResultColumns>)ViewData["tableResultColumns"];

    AjaxOptions columnSaveOptions = new AjaxOptions
    {
        UpdateTargetId = reportObjectViewId + "_Columns",
        HttpMethod = "POST",
        InsertionMode = InsertionMode.Replace
    };

    using (Ajax.BeginForm("ManageTableColumns", "TableSources", columnSaveOptions))
    {
%>
<div id="<%: reportObjectViewId %>_Columns" class="tableSourceCols">
    <div>
        <%: Html.ValidationSummary() %>
    </div>
    <%: Html.Hidden("reportId",reportId) %>
    <%: Html.Hidden("viewObjectId",reportObjectViewId) %>
    <%
        if (tableResultColumns != null && tableResultColumns.Count > 0)
        {
            int i = 1;
            foreach (var tblResultCol in tableResultColumns)
            {
                string currColGuid = Guid.NewGuid().ToString();                    
    %>
    <div id="<%:tblResultCol.Value.Column %>_<%:i.ToString() %>" class="tabledragbox">
        <h2>
            <%: Html.Hidden("tableColumns.index",currColGuid) %>
            <%: Html.Hidden("tableColumns[" + currColGuid + "].Id", tblResultCol.Value.ColumnsId)%>
            <%
                if (tblResultCol.Value.ReportObjectId == null || string.IsNullOrEmpty(tblResultCol.Value.ReportObjectId))
                {
                    tblResultCol.Value.ReportObjectId = !string.IsNullOrEmpty(reportObjectId) ? reportObjectId : null;
                }
                
            %>
            <%: Html.Hidden("tableColumns["+currColGuid+"].ReportObjectId",tblResultCol.Value.ReportObjectId) %>
            <%
                if (tblResultCol.Value.TableSourceId == null || string.IsNullOrEmpty(tblResultCol.Value.TableSourceId))
                {
                    tblResultCol.Value.TableSourceId = !string.IsNullOrEmpty(tableSourceId) ? tableSourceId : null;
                }
            %>
            <%: Html.Hidden("tableColumns["+currColGuid+"].TableSourceId",tblResultCol.Value.TableSourceId) %>
            <%: Html.Hidden("tableColumns["+currColGuid+"].Ordinal", tblResultCol.Value.Ordinal) %>
            <%: Html.Hidden("tableColumns[" + currColGuid + "].Column", tblResultCol.Value.Column) %>
            [<span class="columnordinal">
                <%: tblResultCol.Value.Ordinal %>
            </span>] <span class="fieldname">
                <%: tblResultCol.Value.Column %></span>:
            <%: Html.TextBox("tableColumns[" + currColGuid + "].DisplayName", tblResultCol.Value.DisplayName, new { @class = "displayname" })%>
        </h2>
    </div>
    <% 
            }
        }
        else
        {%>
    <%: Html.Label("No Source Column Mappings Found") %>
    <%
        } 
    %>
    <input id="Submit1" type="submit" value="Save" />
</div>
<% } %>