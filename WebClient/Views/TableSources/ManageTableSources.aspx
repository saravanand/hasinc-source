﻿<%@ Page Title="<%$Resources:Title%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Reporting.DataSources.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container">
        <div class="heading_container">
            <h1>
                <%:this.GetLocalResourceObject("Title")%>
            </h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a href="<%:Url.Action("Index","TableSources") %>" title="Back"><span><%:this.GetGlobalResourceObject("General", "Back")%></span>
                    </a>
                </div>
            </div>
        </div>
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Success")))
          { %>
        <div class="success">
            <%= Html.CelloValidationMessage("Success", new { @class = "success1" })%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
          { %>
        <div class="error">
            <%= Html.CelloValidationSummary("Please correct the following errors and submit the form again:", new { @class = "exception" })%>
        </div>
        <%} %>
        <%
            TableSource tableSource = new TableSource();

            if (ViewData["tableSource"] != null)
            {
                tableSource = (TableSource)ViewData["tableSource"];
            }

            using (Html.BeginForm("ManageTableSources", "TableSources", FormMethod.Post, new { @id = "tableSourceForm" }))
            {
        %>
        <div style="clear: both">
        </div>
        <table style="width: 100%;" class="celloTable details_container_table">
            <thead>
                <tr>
                    <th colspan="2">
                        <%:this.GetLocalResourceObject("ManageTableSource")%>
                    </th>
                </tr>
            </thead>
            <tr>
                <td class="row4">
                    <%
                string tsIdentifier = tableSource.Id;
                    %>
                    <%: Html.Hidden("Id", tsIdentifier)%>
                    <%: Html.Label("Table Source Name")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                </td>
                <td class="row5">
                    <%: Html.TextBox("Name", tableSource.Name)%>
                    <%: Html.ValidationMessage("EmptyTableSourceName")%>
                    <%: Html.ValidationMessage("LengthySourceName")%>
                </td>
            </tr>
            <tr>
                <td class="row4">
                    <%: Html.Label("Table Source Description")%>
                </td>
                <td class="row5">
                    <%: Html.TextArea("Description", tableSource.Description)%>
                    <%: Html.ValidationMessage("LengthySourceDesc")%>
                </td>
            </tr>
            <tr>
                <td class="row4">
                    <%: Html.Label("Query Source")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                    <%: Html.Hidden("DataSourceId", tableSource.DataSourceId, new { @class = "DataSourceId" })%>
                    <%
                string dataSrcId = null;
                if (tableSource.DataSourceValues != null && !string.IsNullOrEmpty(tableSource.DataSourceValues.SourceContentId))
                {
                    dataSrcId = tableSource.DataSourceValues.SourceContentId;
                }
                    %>
                    <%: Html.Hidden("DataSourceValues.SourceContentId", dataSrcId)%>
                </td>
                <td class="row5">
                    <%
                if (!string.IsNullOrEmpty(dataSrcId))
                {
                    %>
                    <%: Html.DropDownList("dataSources", null, new { @class = "dataSources", @disabled = "disabled", @onchange = "updateTableDataSource()" })%>
                    <%
                }
                else
                {
                    %>
                    <%: Html.DropDownList("dataSources", null, new { @class = "dataSources", @onchange = "updateTableDataSource()" })%>
                    <%: Html.ValidationMessage("InvalidDataSource")%>
                    <% } %>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <%--<input type="image" src="../../App_Themes/CelloSkin/save.gif" title="Save" style="height: 23px;
                            width: 56px" />--%>
                    <div class="green_but">
                        <a onclick="javascript:$('#tableSourceForm').submit()" title="Save Table Source"><span>
                            <%:this.GetGlobalResourceObject("General", "Save")%></span></a>
                    </div>
                </td>
            </tr>
        </table>
        <%
            }
        %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="<%: this.ResolveClientUrl("../../Scripts/managetablesources.js") %>"
        type="text/javascript"></script>
    <style type="text/css">
        .detailsTable tr td
        {
            border-bottom: 1px solid #C3D9FF;
            border-right: 1px solid #C3D9FF;
            border-left: 1px solid #C3D9FF;
        }
    </style>
</asp:Content>
