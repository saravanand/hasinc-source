﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Template.Model.LocaleTemplate>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "AddMasterConfig";
   using (Ajax.BeginForm("AddLocaleTemplate", "Template", new { }, ajaxOption, new { id = "AddLocaleTemplatePage" }))
   {
%>
<section class="panel green">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("AddLocaleTemplate")%></h4>
        <div class="actions pull-right">
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:AddLocaleTemplateDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-sm  btn-default" href="#" onclick="javascript:LocaleTemplateCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>
        <%if (this.Model != null)
           { %>
        <div class="form-container">
            <div class="form-group">
                <label>
                    <%:this.GetLocalResourceObject("LocaleName")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                    <%=Html.HiddenFor(model=>model.TemplateId) %>
                </label>
                <%= Html.DropDownList("LocaleId", ViewData["LocaleId"]!=null?(IEnumerable<SelectListItem>)ViewData["LocaleId"]:null, new { style = "width:100%;", onchange = "GetLocaleName();" })%>
                <%= Html.Hidden("LocaleName")%>
            </div>
            <div class="form-group">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("Template")%>
                </label>
                <%= Html.TextArea("Template", new { @class = "RichTextEditor" })%>
            </div>
        </div>
        <br />
        <div class="pull-right">
            <a class="btn btn-info" href="#" onclick="javascript:AddLocaleTemplateDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="#" onclick="javascript:LocaleTemplateCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
        <%} %>
    </div>
</section>
<script type="text/javascript">
    $('select[name=LocaleId]').select2();
</script>
<% } %>
