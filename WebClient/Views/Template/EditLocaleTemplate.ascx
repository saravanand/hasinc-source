﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Template.Model.LocaleTemplate>" %>
<%@ Import Namespace="CelloSaaS.Template.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    CelloSaaS.Template.Model.LocaleTemplate localeTemplate = (CelloSaaS.Template.Model.LocaleTemplate)ViewData["localeTemplate"];%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "AddMasterConfig";
   using (Ajax.BeginForm("EditLocaleTemplate", "Template", new { }, ajaxOption, new { id = "EditLocaleTemplatePage" }))
   {
%>
<section class="panel indigo">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("EditLocaleTemplate")%></h4>
        <div class="actions pull-right">
            <a class="btn btn-sm btn-info" href="#" onclick="javascript:UpdateLocaleTemplateDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-sm  btn-default" href="#" onclick="javascript:LocaleTemplateCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("ConfigStatusMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("ConfigStatusMessage")%>
        </div>
        <%} %>
        <% else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <%} %>
        <div class="form-container">
            <div class="form-group">
                <label>
                    <%:this.GetLocalResourceObject("LocaleName")%>
                    <%=Html.HiddenFor(TemplateId => localeTemplate.TemplateId)%>
                    <%=Html.HiddenFor(LocaleId => localeTemplate.LocaleId)%>
                    <%=Html.HiddenFor(Id => localeTemplate.Id)%>
                    <%=Html.HiddenFor(LocaleName => localeTemplate.LocaleName)%>
                </label>
                <br />
                <%= localeTemplate.LocaleName%>
            </div>
            <div class="form-group">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("Template")%>
                </label>
                <%= Html.TextArea("Template", localeTemplate.ContentText, new { @class = "RichTextEditor" })%>
            </div>
        </div>
        <br />
        <div class="pull-right">
            <a class="btn btn-info" href="#" onclick="javascript:UpdateLocaleTemplateDetails();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="#" onclick="javascript:LocaleTemplateCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </div>
</section>
<% } %>
