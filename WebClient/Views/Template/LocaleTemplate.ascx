﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Template.Model.LocaleTemplate>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%= Html.Hidden("NotificationTemplateId", ViewData["NotificationTemplateId"])%>
<% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("StatusMessage")))
   { %>
<div class="alert alert-danger">
    <%=Html.CelloValidationSummary()%>
</div>
<%} %>
<% else if (!string.IsNullOrEmpty(Html.CelloValidationMessage("SuccessStatusMessage")))
   { %>
<div class="alert alert-success">
    <%=Html.CelloValidationSummary()%>
</div>
<%} %>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <div class="col-sm-7 m-b-xs">
                <div id="user_filter" class="dataTables_filter">
                    <div class="input-group">
                        <input id="masterTableSearchText" name="masterTableSearchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                        <span class="input-group-btn">
                            <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 m-b-xs pull-right">
                <div class="pull-right">
                    <a class="btn btn-success" href="#" title=" <%=this.GetGlobalResourceObject("General", "Add")%>" onclick="AddLocaleTemplate();">
                        <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Add")%></a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body">
        <% if (ViewData["LocaleTemplateSource"] == null)
           {%>
        <div class="alert alert-info">
            <%:this.GetLocalResourceObject("e_TemplateNotAvailable")%>
        </div>
        <% }
           else
           {
               Html.Grid((IEnumerable<CelloSaaS.Template.Model.LocaleTemplate>)ViewData["LocaleTemplateSource"]).Columns(
             column =>
             {
                 column.For(col => col.LocaleName).Named(this.GetLocalResourceObject("localeName").ToString());
                 column.For(col => col.ContentText.Truncate()).Named(this.GetLocalResourceObject("templateContent").ToString()).Attributes(style => "white-space: -moz-pre-wrap; ");
                 column.For(col => "<a id='Edit' href='#' title='" + string.Format(this.GetLocalResourceObject("edit").ToString(), col.LocaleName) + "' onclick=EditLocaleTemplate('" + col.LocaleId.Trim() + "','" + col.TemplateId.Trim() + "')><i class='fa fa-edit'></i></a>").Named(this.GetLocalResourceObject("editDetails").ToString()).Attributes(@class => "halign", style => "width:100px;").HeaderAttributes(@class => "halign").DoNotEncode();
                 column.For(col => "<a id='Delete' href='#' title='" + string.Format(this.GetLocalResourceObject("delete").ToString(), col.LocaleName) + "' onclick=DeleteLocaleTemplate('" + col.LocaleId.Trim() + "','" + col.TemplateId.Trim() + "')><i class='fa fa-trash-o'></i></a>").Named(this.GetLocalResourceObject("deleteDetails").ToString()).Attributes(@class => "halign", style => "width:120px;").HeaderAttributes(@class => "halign").DoNotEncode();
             }).Attributes(id => "dataList", @class => "celloTable").Render();
           }
        %>
    </div>
</div>
