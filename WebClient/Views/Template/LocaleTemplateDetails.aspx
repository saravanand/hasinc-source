﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Scripts/jquery-te-1.4.0.min.js" type="text/javascript"></script>
    <link href="/Content/jquery-te-1.4.0.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            loadRichTextEditor();
            jQDataTable();
            $('#masterTableSearchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }]
            });
        }

        function DoSearch() {
            var searchText = $('#masterTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert-success,div.alert-error").remove();
            return false;
        }
    </script>
    <script type="text/javascript">
        var addLocaleTemplateDetails = "AddLocaleTemplate";
        var ShowLocaleTemplateActionUrl = "ShowLocaleTemplateDetails";
        var editLocaleTemplateDetails = "EditLocaleTemplate";
        var deleteLocaleTemplateDetails = "RemoveLocaleTemplate";

        function AddLocaleTemplate() {
            var notificationTemplateId = $("#NotificationTemplateId").val();
            $('#statusMessage').fadeOut();
            $('div .success').fadeOut();
            $.get(addLocaleTemplateDetails + "?notificationTemplateId=" + notificationTemplateId, loadAddLocaleTemplate);
        }

        function loadAddLocaleTemplate(data) {
            $(".info").fadeOut();
            $("#AddLocaleTemplate").html(data);
            loadRichTextEditor();
            $("#AddLocaleTemplate").fadeIn();
        }

        function LocaleTemplateCancel() {
            $("#AddLocaleTemplate").fadeOut();
        }

        function AddLocaleTemplateDetails() {
            var url = '<%:Url.Action("AddLocaleTemplateDetails","Template") %>';
            formDetails = $('#AddLocaleTemplatePage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, AddLocaleTemplate_callBack);
        }

        //This method is used to render the data after added the email dispatch details
        function AddLocaleTemplate_callBack(data) {
            if (data == "Success") {
                $("#AddLocaleTemplate").fadeOut();
                loadLocaleTemplateList();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>Locale template details are saved successfully.</div>').fadeIn();
            }
            else {
                $("#AddLocaleTemplate").html(data);
                loadRichTextEditor();
                $("#AddLocaleTemplate").fadeIn();
            }
        }

        function loadLocaleTemplateList() {
            // $.ajaxSettings.cache = false;
            var notificationTemplateId = $("#NotificationTemplateId").val();
            var notificationMasterName = $("#notificationMasterName").text();
            var notificationType = $("#NotificationType").val();
            $.get(ShowLocaleTemplateActionUrl + "?notificationTemplateId=" + notificationTemplateId + "&notificationMasterName=" + notificationMasterName + "&notificationType=" + notificationType, localeTemplateList_callBack);
            $("#AddLocaleTemplate").fadeOut();
        }

        //This method is used to render the locale details in the role List div
        function localeTemplateList_callBack(data) {
            $("#notificationMasterDetails").html(data);
            jQDataTable();
        }

        function EditLocaleTemplate(localeId, templateId) {
            $('#statusMessage').fadeOut();
            $('div .success').fadeOut();
            $.get(editLocaleTemplateDetails + "?localeId=" + localeId + "&templateId=" + templateId, loadAddLocaleTemplate);
        }

        function UpdateLocaleTemplateDetails() {
            var url = '<%:Url.Action("EditLocaleTemplate","Template") %>';
            formDetails = $('#EditLocaleTemplatePage');
            serializedForm = formDetails.serialize();
            $.post(url, serializedForm, AddLocaleTemplate_callBack);
        }

        function DeleteLocaleTemplate(localeId, templateId) {
            $('#statusMessage').fadeOut();
            $('div .success').fadeOut();
            var notificationMasterName = $("#notificationMasterName").text();
            var notificationType = $("#NotificationType").val();
            if (confirm('<%: this.GetLocalResourceObject("askDelete")%>')) {
                $('#EditLocaleTemplatePage').hide();
                $.get(deleteLocaleTemplateDetails + "?localeId=" + localeId + "&templateId=" + templateId + "&notificationMasterName=" + notificationMasterName + "&notificationType=" + notificationType, localeTemplateList_callBack);
            }

        }

        function GetLocaleName() {
            var localeName = $("#LocaleId option:selected").text();
            $("#LocaleName").val(localeName);
        }

        function loadRichTextEditor() {
            $(document).ready(function () {
                $(".RichTextEditor").jqte();
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <a href="TemplateDetails?category=<%=ViewData["category"]%>" title="<%=this.GetGlobalResourceObject("General","Back")%>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%:this.GetLocalResourceObject("manageTemplate")%>
            <span class="semi-bold" id="notificationMasterName"><%=ViewData["NotificationName"]%></span>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div id="statusMessage">
        </div>
        <div id="notificationMasterDetails">
            <% Html.RenderPartial("LocaleTemplate"); %>
        </div>
        <div id="AddLocaleTemplate" style="<%= ViewData["localeTemplate"] != null ? "display: none": ""%>">
            <%if (ViewData["localeTemplate"] != null)
              {%>
            <% Html.RenderPartial("EditLocaleTemplate"); %>
            <% } %>
        </div>
    </div>
</asp:Content>
