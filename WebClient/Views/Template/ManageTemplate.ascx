﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Template.Model.TemplateDetail>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "AddMasterConfig";
   using (Ajax.BeginForm("ManageTemplate", "Template", new { }, ajaxOption, new { id = "ManageTemplatePage" }))
   {
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4>
            <%:this.GetLocalResourceObject("ManageTemplate")%></h4>
    </header>
    <div class="panel-body">
        <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
           { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary()%>
        </div>
        <%} %>
        <div class="form-container">
            <div class="form-group <%=Html.ValidationMessage("TemplateName","*")!=null?"has-error":"" %>">
                <label class="mandatory">
                    <%:this.GetLocalResourceObject("TemplateName")%>
                </label>
                <%= Html.TextBox("TemplateName", Model.TemplateName, new { maxlength = 50 })%>
            </div>
            <div class="form-group">
                <label>
                    <%:this.GetLocalResourceObject("TemplateCategory")%>
                    <%=Html.HiddenFor(model=>model.TemplateId) %>
                </label>
                <%: Html.DropDownList("Category", null, new { style = "width:100%;" })%>
            </div>
            <div class="form-group">
                <label>
                    <%:this.GetLocalResourceObject("TemplateDescription")%>
                </label>
                <%= Html.TextArea("Description", Model.Description, new { maxlength = 200, style = "width:100%;height:80px;" })%>
            </div>
        </div>
        <br />
        <div class="pull-right">
            <a class="btn btn-success" href="#" onclick="javascript:SaveTemplate();" title="<%=this.GetGlobalResourceObject("General","Save") %>">
                <i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %></a>
            <a class="btn btn-default" href="#" onclick="javascript:ManageTemplateCancel();" title="<%=this.GetGlobalResourceObject("General","Cancel") %>">
                <%=this.GetGlobalResourceObject("General","Cancel") %></a>
        </div>
    </div>
</section>
<script type="text/javascript">
    $("select[name=Category]").select2();
</script>
<% } %>
