﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Template.Model.TemplateDetail>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%: Html.Hidden("TemplateCategory",ViewData["TemplateCategory"]) %>
<% var lstTemplates = ViewData["TemplateDetails"] as IEnumerable<CelloSaaS.Template.Model.TemplateDetail>;
%>
<% if (lstTemplates != null && lstTemplates.Count() > 0)
   {
       var templateCategories = new Dictionary<string, string>();
       templateCategories.Add("", "All");
       templateCategories.Add("NotificationTemplate", "NotificationTemplate");
       templateCategories.Add("EventTemplate", "EventTemplate");

       Html.Grid(lstTemplates).Columns(
       column =>
       {
           column.For(col => col.TemplateName.Length > 50 ? col.TemplateName.Substring(0, 50) + "..." : col.TemplateName).Named(this.GetLocalResourceObject("templateName").ToString());
           column.For(col => col.Description.Length > 50 ? col.Description.Substring(0, 50) + "..." : col.Description).Named(this.GetLocalResourceObject("Description").ToString()).Attributes(style => "width:40%;white-space: -moz-pre-wrap !important; ");
           column.For(col => templateCategories.ContainsKey(col.Category) ? templateCategories[col.Category] : col.Category).Named(this.GetLocalResourceObject("Category").ToString());
           column.For(col => "<a id='ManageTemplate' href='LocaleTemplateDetails?templateId=" + col.TemplateId.Trim() + "&category=" + col.Category + "' title='" + string.Format(this.GetLocalResourceObject("manageDetails").ToString(), col.TemplateName) + "'><i class='fa fa-wrench'></i></a>").Named(this.GetLocalResourceObject("manageTemplate").ToString()).Attributes(@class => "halign", style => "width:150px;").HeaderAttributes(@class => "halign").DoNotEncode();
           column.For(col => "<a id='Edit' href='#' title='" + string.Format(this.GetLocalResourceObject("editDetails").ToString(), col.TemplateName) + "' onclick =ManageTemplate('" + col.TemplateId.Trim() + "')><i class='fa fa-pencil'></i></a>").Named(this.GetLocalResourceObject("edit").ToString()).Attributes(@class => "halign", style => "width:100px;").HeaderAttributes(@class => "halign").DoNotEncode();
       }).Attributes(id => "dataList", @class => "celloTable").Render();
   }
   else
   { %>
<div class="alert alert-info">
     <%=this.GetGlobalResourceObject("General", "m_NoData")%>
</div>
<% } %>