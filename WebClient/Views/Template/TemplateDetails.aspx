﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();
           
            $('#masterTableSearchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            filterTable = $('table#dataList').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }]
            });
        }

        function DoSearch() {
            var searchText = $('#masterTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
    <script type="text/javascript">
        var manageTemplateUrl = "ManageTemplate";
        var manageTemplateDetails = '<%:Url.Action("ShowLocaleTemplateDetails") %>';

        function ManageTemplate(templateId) {
            var templateCategory = $("#TemplateCategory").val();
            $('#statusMessage').fadeOut();
            $('div .success').fadeOut();
            $.get(manageTemplateUrl + "?templateId=" + templateId + "&templateCategory=" + templateCategory, loadAddMasterConfig);
        }

        function loadAddMasterConfig(data) {
            $("#divManageTemplate").html(data);
            $("#divManageTemplate").fadeIn();
            $("#TemplateName").focus();
        }

        function SaveTemplate() {
            formDetails = $('#ManageTemplatePage');
            serializedForm = formDetails.serialize();
            $.post(manageTemplateUrl, serializedForm, addMaster_callBack);
        }

        function ManageTemplateDetails(templateId) {
            $.get(manageTemplateDetails + "?templateId=" + templateId, loadAddLocaleTemplate);
        }

        function loadAddLocaleTemplate(data) {
            //            window.location.href = "LocaleTemplateDetails";

        }

        //This method is used to render the data after added the role details
        function addMaster_callBack(data) {
            if (data == "Success") {
                loadMasterList();
                $('#statusMessage').html('<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a><%: this.GetLocalResourceObject("s_SaveTemplate") %></div>').fadeIn();
            }
            else {
                $('#divManageTemplate').html(data);
                $('#divManageTemplate').fadeIn();
            }
        }

        function loadMasterList() {
            $.ajaxSettings.cache = false;
            var templateCategory = $("#TemplateCategory").val();
            $.get("TemplateDetails?category=" + templateCategory, masterList_callBack);
            $("#divManageTemplate").fadeOut();
        }

        //This method is used to render the master details in the role List div
        function masterList_callBack(data) {
            $("#templateDetails").html(data);
            var templateCategoryVal = $('#TemplateCategory').val();
            if (templateCategoryVal == "") {
                templateCategoryVal = "All";
            }
            $('#Category').val(templateCategoryVal);
            jQDataTable();

        }

        function ManageTemplateCancel() {
            $('#divManageTemplate').fadeOut();
        }

        function ChangeCategory() {
            $.ajaxSettings.cache = false;
            var category = $('#Category option:selected').val();
            $.get("TemplateDetails?category=" + category, masterList_callBack);
            $('#divManageTemplate').fadeOut();
        }
    </script>
    <script type="text/javascript">
        $(function () {

            var postData = { 'category': $('TemplateCategory').val() };
            var currentCategoryId = '';

            var querystring = location.search.replace('?', '').split('&');
            // declare object
            var queryObj = {};
            // loop through each name-value pair and populate object
            for (var i = 0; i < querystring.length; i++) {
                // get name and value
                var name = querystring[i].split('=')[0];
                var value = querystring[i].split('=')[1];
                // populate object
                queryObj[name] = value;
            }

            if (querystring.length > 0) {
                currentCategoryId = queryObj['category'];
            }

            $.post('<%=Url.Action("GetCategories","Template") %>', postData, function (data) {
                var tenantData = [];

                if (!data.Error) {
                    tenantData = data;
                }

                for (var i = 0; i < tenantData.length; ++i) {
                    var opt = '<option value="' + tenantData[i].CategoryId + '"';
                    if (tenantData[i].CategoryId == currentCategoryId) {
                        opt += ' selected';
                    }
                    opt += '>' + tenantData[i].CategoryName + '</option>';
                    $(opt).data('id', tenantData[i].CategoryId);
                    $("#chnCategory").append(opt);
                }

                $("#chnCategory").select2()
                    .on('change', function (e, ui) {
                        var id = $('option:selected', $("#chnCategory")).val();
                        var name = $('option:selected', $("#chnCategory")).text();
                        $.get("TemplateDetails?category=" + id, masterList_callBack);;
                    });

            }, 'json');

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="center_container">
        <div class="page-title">
            <h3>
                <%:this.GetLocalResourceObject("manageTemplate")%></h3>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <div id="statusMessage">
        </div>
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input id="masterTableSearchText" name="masterTableSearchText" class="input-sm form-control" type="text" placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 m-b-xs">
                        <div class="pull-right">
                            <label style="font-weight: bold;">
                                <%:this.GetLocalResourceObject("TemplateCategory")%>:</label>
                            <select name="chnCategory" id="chnCategory" style="width: 200px;">
                            </select>
                            <input type="hidden" name="templateCategory" id="templateCategory" value="All" />
                        </div>
                    </div>
                    <div class="col-sm-2 m-b-xs pull-right">
                        <div class="pull-right">
                            <a class="btn btn-success" href="#" title="<%=this.GetGlobalResourceObject("General", "Add")%>" onclick="ManageTemplate('');">
                                <i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General", "Add")%></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <div id="templateDetails">
                    <% Html.RenderPartial("PartialTemplateDetails"); %>
                </div>
            </div>
        </div>
        <div id="divManageTemplate" style="display: none;">
        </div>
    </div>
</asp:Content>
