﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<h2>
    <%: this.GetLocalResourceObject("lbl_AddServiceHeader") %>
</h2>
<hr />
<div class="clear">
</div>
<%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
  { %>
<div class="error">
    <%=Html.CelloValidationSummary()%>
</div>
<% } %>
<% if (ViewData["ServiceList"] == null || ((Dictionary<string, CelloSaaS.Model.LicenseManagement.Service>)ViewData["ServiceList"]).Count == 0)
   { %>
<div class="error">
    <%: this.GetLocalResourceObject("e_ServicesNotAvailable") %>
</div>
<div class="green_but">
    <a href="#" title="<%: this.GetGlobalResourceObject("General","Cancel") %>" onclick="HideServiceDiv();">
        <span>
            <%: this.GetGlobalResourceObject("General","Cancel") %></span> </a>
</div>
<% }
   else
   { %>
<div class="inner_left">
    <div class="inner_hold">
        <label>
            <%:this.GetLocalResourceObject("lbl_ServiceName") %>
            <%:this.GetLocalResourceObject("Mandatory") %>
        </label>
        <%= Html.DropDownList("ServiceId", new SelectList(((Dictionary<string, CelloSaaS.Model.LicenseManagement.Service>)ViewData["ServiceList"]).Values, "ServiceCode", "ServiceName"), new { style = "width:210px;" })%>
    </div>
    <div class="inner_hold">
        <label>
            <%: this.GetLocalResourceObject("lbl_StartPeriod") %>
            <%: this.GetLocalResourceObject("Mandatory") %>
        </label>
        <input type="text" name="startPeriod" id="startPeriod" autocomplete="off" />
    </div>
    <div class="inner_hold">
        <label>
            <%: this.GetLocalResourceObject("lbl_EndPeriod") %>
        </label>
        <input type="text" name="endPeriod" id="endPeriod" autocomplete="off" />
    </div>
    <div class="inner_hold">
        <div class="green_but">
            <a href="#" title="<%: this.GetGlobalResourceObject("General","Save") %>" onclick="AddNewService();">
                <span>
                    <%: this.GetGlobalResourceObject("General","Save") %>
                </span></a>
        </div>
        <div class="green_but">
            <a href="#" title="<%: this.GetGlobalResourceObject("General","Cancel") %>" onclick="HideServiceDiv();">
                <span>
                    <%: this.GetGlobalResourceObject("General","Cancel") %>
                </span></a>
        </div>
    </div>
</div>
<% } %>