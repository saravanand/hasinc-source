﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<h2>
    <%: this.GetLocalResourceObject("lbl_EditServicePeriod")%>
</h2>
<hr />
<div class="clear">
</div>
<%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("Error")))
  { %>
<div class="error">
    <%=Html.CelloValidationSummary()%>
</div>
<% } %>
<% 
    var tenantServiceModel = ViewData["TenantServiceDetails"] as CelloSaaS.Model.LicenseManagement.TenantServiceMapping;
    var serviceDetail = ViewData["Service"] as CelloSaaS.Model.LicenseManagement.Service;
%>
<% if (tenantServiceModel == null || serviceDetail == null)
   { %>
<div class="error">
    <%: this.GetLocalResourceObject("e_ServicePeriodNotAvailable") %>
</div>
<% }
   else
   { %>
<div class="inner_left">
    <div class="inner_hold">
        <label>
            <%: this.GetLocalResourceObject("lbl_ServiceName") %>
        </label>
        <b>
            <%: serviceDetail.ServiceName %></b>
        <input type="hidden" name="ServiceId" value="<%: serviceDetail.ServiceCode %>" />
    </div>
    <div class="inner_hold">
        <label>
            <%: this.GetLocalResourceObject("lbl_StartPeriod") %>
            <%: this.GetLocalResourceObject("Mandatory") %>
        </label>
        <input type="text" name="startPeriod" id="editStartPeriod" value="<%:tenantServiceModel.StartDate.Value.ToShortDateString() %>"
            autocomplete="off" />
    </div>
    <div class="inner_hold">
        <label>
            <%:this.GetLocalResourceObject("lbl_EndPeriod") %>
        </label>
        <% if (tenantServiceModel.EndDate.HasValue)
           {%>
        <input type="text" name="endPeriod" id="editEndPeriod" value="<%:tenantServiceModel.EndDate.Value.ToShortDateString() %>"
            autocomplete="off" />
        <% }
           else
           { %>
        <input type="text" name="endPeriod" id="editEndPeriod" autocomplete="off" />
        <% } %>
    </div>
    <div class="inner_hold">
        <input type="hidden" name="tenantServiceMappingId" value="<%:tenantServiceModel.TenantServiceMappingId %>" />
        <div class="green_but">
            <a href="#" title="<%: this.GetLocalResourceObject("lbl_UpdateServicePeriod") %>"
                onclick="UpdateService('<%:tenantServiceModel.TenantServiceMappingId %>');"><span>
                    <%: this.GetGlobalResourceObject("General","Update") %></span> </a>
        </div>
        <div class="green_but">
            <a href="#" title=" <%: this.GetGlobalResourceObject("General","Cancel") %>" onclick="HideServiceDiv();"><span>
                <%: this.GetGlobalResourceObject("General","Cancel") %>
            </span></a>
        </div>
    </div>
</div>
<% } %>