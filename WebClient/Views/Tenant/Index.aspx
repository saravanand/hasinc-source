﻿<%@ Page Title="Tenant Management" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaSApplication.Models" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var currentTenant = ViewBag.currentTenant as Tenant;
        bool isIsvOrReseller = currentTenant.Types != null && currentTenant.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);
        var allPackages = ViewData["AllPackages"] as IEnumerable<LicensePackage> ?? new List<LicensePackage>();
        var lstPackages = allPackages.Select(x => new SelectListItem
        {
            Text = x.Name,
            Value = x.LicensepackageId
        }).ToList();
        lstPackages.Insert(0, new SelectListItem { Text = "All", Value = "" });

        var lstFilterType = new List<SelectListItem>();
        lstFilterType.Add(new SelectListItem { Text = "All", Value = "All" });
        lstFilterType.Add(new SelectListItem { Text = "New", Value = "IsNew" });
        lstFilterType.Add(new SelectListItem { Text = "Active", Value = "OnlyActive" });
        lstFilterType.Add(new SelectListItem { Text = "Cancelled", Value = "OnlyDeactivated" });

        var currTenantType = currentTenant.Types ?? new List<TenantType>();
        var lstTypes = new List<SelectListItem>();
        lstTypes.Add(new SelectListItem { Text = "All", Value = "" });

        if (currTenantType.Any(x => x.ID == TenantTypeConstants.ISV))
        {
            lstTypes.Add(new SelectListItem { Value = TenantTypeConstants.SMB, Text = "SMB" });
            lstTypes.Add(new SelectListItem { Value = TenantTypeConstants.Enterprise, Text = "Enterprise" });
            lstTypes.Add(new SelectListItem { Value = TenantTypeConstants.Reseller, Text = "Reseller" });

            lstFilterType.Add(new SelectListItem { Text = "Trial", Value = "OnlyTrial" });
        }
        else if (currTenantType.Any(x => x.ID == TenantTypeConstants.Reseller))
        {
            lstTypes.Add(new SelectListItem { Value = TenantTypeConstants.SMB, Text = "SMB" });
            lstTypes.Add(new SelectListItem { Value = TenantTypeConstants.Enterprise, Text = "Enterprise" });

            lstFilterType.Add(new SelectListItem { Text = "Trial", Value = "OnlyTrial" });
        }
        else if (currTenantType.Any(x => x.ID == TenantTypeConstants.Enterprise || x.ID == TenantTypeConstants.SMB))
        {
            lstTypes.Add(new SelectListItem { Value = TenantTypeConstants.OrgUnit, Text = "OrgUnit" });
        }
    %>
    <div id="loading"></div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("h_TenantManagement") %> 
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <% Html.RenderPartial("StatusMessage"); %>
        <section id="tab-container" class="panel">
            <header class="panel-heading bg-success">
                <ul class="nav nav-tabs nav-justified captilize">
                    <li class="active"><a data-toggle="tab" href="#approvedTenantsDiv" title="<%: this.GetLocalResourceObject("t_ViewApprovedTenants") %>"><%: this.GetLocalResourceObject("lbl_ApprovedTenants") %> </a></li>
                    <li><a data-toggle="tab" href="#unapprovedTenantsDiv" title="<%: this.GetLocalResourceObject("t_ViewUnApprovedTenants") %>"><%: this.GetLocalResourceObject("lbl_UnapprovedTenants") %> </a></li>
                    <li><a data-toggle="tab" href="#tenantDensityGraphDiv" title="<%: this.GetLocalResourceObject("t_ViewTenantDensityGraph") %>"><%: this.GetLocalResourceObject("lbl_TenantDensityGraph") %> </a></li>
                </ul>
            </header>
            <div class="panel-body pd-0">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="approvedTenantsDiv" style="padding: 0px;">
                        <div id="tenantdash">
                            <% Html.RenderAction("TenantDashboard"); %>
                        </div>
                        <div class="row-fluid form-container">
                            <div class="panel">
                                <div class="panel-body">
                                    <form id="frmTenantSearch" name="frmTenantSearch">
                                        <table style="width: 100%;">
                                            <colgroup>
                                                <col style="width: 120px;" />
                                                <col style="width: 40%;" />
                                                <col style="width: 120px;" />
                                                <col style="width: 40%;" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: right">
                                                        <label><%=this.GetGlobalResourceObject("General","Status") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("filterType", lstFilterType, new { style="width:100%;" })%></td>

                                                    <td style="text-align: right">
                                                        <label><%: this.GetLocalResourceObject("lbl_Package") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("PackageId",lstPackages, new { style="width:100%;" }) %></td>
                                                </tr>
                                                <% if (isIsvOrReseller)
                                                   { %>
                                                <tr>
                                                    <td style="text-align: right">
                                                        <label><%: this.GetLocalResourceObject("lbl_TenantType") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("TenantType",lstTypes, new { style="width:100%;" }) %></td>

                                                    <td style="text-align: right">
                                                        <%
                                                       var ad = new List<SelectListItem>();
                                                       ad.Add(new SelectListItem { Text = "Doesn't matter", Value = "" });
                                                       ad.Add(new SelectListItem { Text = "True", Value = "True" });
                                                       ad.Add(new SelectListItem { Text = "False", Value = "False" });
                                                       ViewData["HasAutoDebit"] = ad;
                                                        %>
                                                        <label><%: this.GetLocalResourceObject("lbl_AutoDebit") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("HasAutoDebit",null, new { style="width:100%;" }) %></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right">
                                                        <%
                                                       var pa = new List<SelectListItem>();
                                                       pa.Add(new SelectListItem { Text = "Doesn't matter", Value = "" });
                                                       pa.Add(new SelectListItem { Text = "False", Value = "False" });
                                                       pa.Add(new SelectListItem { Text = "True", Value = "True" });

                                                       ViewData["WithoutPaymentAccount"] = pa;
                                                        %>
                                                        <label><%:this.GetLocalResourceObject("lbl_WithoutPaymentAccount") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("WithoutPaymentAccount",null, new { style="width:100%;" })%></td>

                                                    <td style="text-align: right">
                                                        <%
                                                       var sr = new List<SelectListItem>();
                                                       sr.Add(new SelectListItem { Text = "Doesn't matter", Value = "" });
                                                       sr.Add(new SelectListItem { Text = "True", Value = "True" });
                                                       sr.Add(new SelectListItem { Text = "False", Value = "False" });
                                                       ViewData["OnlySelfRegistered"] = sr;
                                                        %>
                                                        <label><%: this.GetLocalResourceObject("lbl_SelfRegistered") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("OnlySelfRegistered",null, new { style="width:100%;" }) %></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align: right">
                                                        <%
                                                       var od = new List<SelectListItem>();
                                                       od.Add(new SelectListItem { Text = "Doesn't matter", Value = "" });
                                                       od.Add(new SelectListItem { Text = "False", Value = "False" });
                                                       od.Add(new SelectListItem { Text = "True", Value = "True" });

                                                       ViewData["HasOverdue"] = od;
                                                        %>
                                                        <label><%:this.GetLocalResourceObject("lbl_HasOverdue") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("HasOverdue", null, new { style="width:100%;" })%></td>

                                                    <td style="text-align: right">
                                                        <%
                                                       var ch = new List<SelectListItem>();
                                                       ch.Add(new SelectListItem { Text = "Doesn't matter", Value = "" });
                                                       ch.Add(new SelectListItem { Text = "True", Value = "True" });
                                                       ch.Add(new SelectListItem { Text = "False", Value = "False" });
                                                       ViewData["HasChildTenants"] = ch;
                                                        %>
                                                        <label><%: this.GetLocalResourceObject("lbl_HasChildTenants") %></label>
                                                    </td>
                                                    <td><%:Html.DropDownList("HasChildTenants",null, new { style="width:100%;" }) %></td>
                                                </tr>
                                                <% } %>
                                                <tr>
                                                    <td style="text-align: right;">
                                                        <label><%: this.GetLocalResourceObject("lbl_TenantName") %></label>
                                                    </td>
                                                    <td>
                                                        <%=Html.TextBox("SearchText",string.Empty, new { @class="search", placeholder=this.GetLocalResourceObject("lbl_Searchtext").ToString()})%>
                                                    </td>
                                                    <td colspan="2">
                                                        <div class="pull-right">
                                                            <button type="submit" class="btn btn-info" id="btnTenantSearch"><i class="fa fa-search"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Search") %></button>
                                                            <button type="reset" class="btn btn-default" id="btnTenantReset"><%=this.GetGlobalResourceObject("General","Reset") %></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="grid simple horizontal red">
                            <div class="grid-title no-border">
                                <div class="row">
                                    <div class="col-md-2 pull-right">
                                        <div class="pull-right" style="margin-top: 3px;">
                                            <%=Html.CelloCreateButton(Url.Action("ManageTenant","Tenant"), "<i class=\"fa fa-plus\"></i>&nbsp;" + this.GetGlobalResourceObject("General","Create").ToString(), null, PrivilegeConstants.AddTenant, null) %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-body no-border" style="padding: 0px;">
                                <div id="divTenantListViewPartial">
                                    <% Html.RenderPartial("TenantListView"); %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="unapprovedTenantsDiv">
                        <%
                            var unapprovedTenants = ViewData["WaitingTenants"] as Dictionary<string, Tenant>;
                            if (unapprovedTenants != null && unapprovedTenants.Count > 0)
                            {
                        %>
                        <% ViewData["ShowActivateColumn"] = UserIdentity.IsInRole(RoleConstants.ProductAdmin); %>
                        <% Html.RenderPartial("PartialTenantGrid", unapprovedTenants.Values.ToList()); %>
                        <%            
                            }
                            else
                            {
                        %>
                        <div class="alert alert-info">
                            <%: this.GetLocalResourceObject("e_NoTenants") %>
                        </div>
                        <% } %>
                    </div>
                    <div class="tab-pane fade" id="tenantDensityGraphDiv">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modalOnlineUsers" tabindex="-1" role="dialog" aria-labelledby="previewExceptionMessage" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="model-title"><%: this.GetLocalResourceObject("lbl_OnlineUsers") %></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close") %></button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="unApprovedTenantView" tabindex="-1" role="dialog" aria-labelledby="unApprovedTenantView" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title"><%=this.GetLocalResourceObject("h_TenantDetails") %></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close") %></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#tenantDensityGraphDiv').load('<%=Url.Action("TenantHierarchyViz", "Dashboard", new { area = "" }) %>');

            $('#TenantType,#PackageId,#filterType').select2();
            $('#HasOnlineUsers,#HasChildTenants,#HasAutoDebit').select2();
            $('#HasOverdue,#WithoutPaymentAccount,#OnlySelfRegistered').select2();

            $('#modalOnlineUsers').modal({ show: false });

            $(document).on('click', '.viewOnlineUsers', function () {
                var tenantid = $(this).data('tenantid');
                $.get('<%=Url.Action("OnlineUsers","Dashboard")%>' + '?tenantId=' + tenantid, function (html) {
                    $('#modalOnlineUsers .modal-body').html(html);
                    $('#modalOnlineUsers').modal('show');
                });
            });

            $(document).on('click', 'a.btnPopoverClose', function () {
                $($(this).data('target')).popover('hide');
                return false;
            });

            $(document).on('click', '#btnTenantReset', function () {
                $('#frmTenantSearch')[0].reset();
                $('#TenantType,#PackageId,#filterType').trigger('change');
                $('#HasOnlineUsers,#HasChildTenants,#HasAutoDebit').trigger('change');
                $('#HasOverdue,#WithoutPaymentAccount,#OnlySelfRegistered').trigger('change');
                $('#btnTenantSearch').trigger('click');
                return false;
            });

            $(document).on('click', '#btnTenantSearch', function () {
                var data = $('#frmTenantSearch').serialize();

                var lt = $('select[name=filterType]').val();

                data = data + '&' + lt + '=true';
                data = data + '&pageSize=' + parseInt($('#pageSize').val());

                $.get('<%=Url.Action("Index")%>', data, function (html) {
                    $('#divTenantListViewPartial').html(html);
                });
                return false;
            });

            $(document).on('change', '#pageSize', function () {
                $('#btnTenantSearch').trigger('click');
                return true;
            });
        });
    </script>
</asp:Content>
