﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="trmtaxes" action="<%=Url.Action("ManageTax") %>" enctype="multipart/form-data"
        method="post">
        <div class="page-title">
            <h3>
                <%: this.GetLocalResourceObject("AddTaxHeader")%>
            </h3>
            <div class="pull-right">
                <a class="btn btn-success" href="#btnSave" id="btnSave" title="<%: this.GetLocalResourceObject("t_Save") %>"><span><i class="fa fa-save"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Save") %> </span></a>
            </div>
        </div>
        <div class="row-fluid pd-25">
            <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TaxSuccessMessage")))
              { %>
            <div class="alert alert-success">
                <%=Html.CelloValidationMessage("TaxSuccessMessage")%>
            </div>
            <% }
              else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
              { %>
            <div class="alert alert-danger">
                <%=Html.CelloValidationSummary(this.GetLocalResourceObject("e_Correcterror").ToString())%>
            </div>
            <% } %>
            <div class="grid simple">
                <div class="grid-body">
                    <div class="row">
                        <% Html.RenderPartial("PartialTaxes");%>
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#taxpanel').on('click', 'a.delPriceSlabs', function (e) {
                e.preventDefault();
                var el = $(this).closest('tr');
                el.animate({ backgroundColor: 'lightcyan' }, 100).fadeOut(200, function () {
                    el.remove();

                    $('.tenantTaxName').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].Name';
                    });

                    $('.tenantTaxDesc').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].Description';
                    });

                    $('.tenantTaxPercentage').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].Percentage';
                    });

                    $('.tenantTaxTaxOrder').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].TaxOrder';
                    });
                });

                return false;
            });


            $('#btnSave').click(function (e) {
                $('form#trmtaxes').submit();
                return false;
            });
        });
    </script>
    <style type="text/css">
        h1, h2, h3, h4, h5, h6 {
            font-weight: bold;
            margin-bottom: 0px !important;
        }

        #taxpanel {
            padding-top: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="topmenu" runat="server">
</asp:Content>
