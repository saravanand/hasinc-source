<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Billing.ServiceProxies" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.LicenseManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="tenantDetailsFrom" action="<%=Url.Action("ManageTenant") %>" enctype="multipart/form-data" role="form"
        method="post">
        <%      
            string tenantId = TenantContext.GetTenantId(new TenantDetails().EntityIdentifier);
            bool isIsvOrReseller = Convert.ToBoolean(ViewData["IsIsvOrReseller"] ?? "False");

            StringBuilder content;
            ColumnProperties columnAttribute;
            CurrentFormMode currentMode = new CurrentFormMode();
            if (ViewData["FormMode"] != null)
            {
                currentMode = (CurrentFormMode)ViewData["FormMode"];
            }

            Dictionary<int, ColumnProperties> columnAttributes = new Dictionary<int, ColumnProperties>();

            columnAttribute = new ColumnProperties();
            columnAttribute.LabelContentProperties = new Dictionary<string, string>() { { "style", "width:120px;text-align:right;" } };
            columnAttribute.FieldContentProperties = new Dictionary<string, string>() { { "style", "width:260px;" } };

            columnAttributes.Add(1, columnAttribute);
            columnAttributes.Add(2, columnAttribute);

            CelloSaaS.View.AlternateRowStyle alternateRowStyle = new CelloSaaS.View.AlternateRowStyle();

            Dictionary<string, string> formAttributes = new Dictionary<string, string>();
            formAttributes.Add("id", "frmAdd");
            formAttributes.Add("class", "details_container_table");
            CelloSaaS.View.DataViewTable frmTable = null;

            Tenant tenant = ViewData["TenantDetails"] as Tenant ?? new Tenant();
            TenantLicense tenantLicense = ViewData["TenantLicense"] as TenantLicense ?? new TenantLicense() { ValidityStart = DateTime.Now.Date };
            List<TenantLicense> licenseHistory = ViewData["LicenseHistory"] as List<TenantLicense>;
        %>
        <div class="page-title">
            <a href="<%=Url.Action("Index") %>" title="<%: this.GetLocalResourceObject("lbl_BacktoTenantList") %>">
                <i class="icon-custom-left"></i>
            </a>
            <h3>
                <%if (currentMode == CurrentFormMode.Insert)
                  { %>
                <%: this.GetLocalResourceObject("AddTenant")%>
                <%}
                  else if (currentMode == CurrentFormMode.Edit)
                  { %>
                <%: this.GetLocalResourceObject("EditTenant") %>
                <% }%>
            </h3>
            <div class="pull-right">
                <% if (ViewBag.AccessDenied != true)
                   { %>
                <%=Html.CelloSubmitButton("<i class=\"fa fa-save\"></i>&nbsp;"+this.GetGlobalResourceObject("General","Save")) %>
                <% if (currentMode == CurrentFormMode.Edit)
                   { %>
                <% if (tenantLicense.ValidityStart < DateTime.Now && !tenantLicense.ValidityEnd.HasValue)
                   { %>
                <a class="btn btn-info" id="btnDeactivate" href="<%=Url.Action("DeActivateTenant", new { tenantId=tenant.Identifier }) %>" title="<%: this.GetLocalResourceObject("t_Deactivatetenantlicense") %>"><%: this.GetLocalResourceObject("lbl_DeactivateLicense") %></a>
                <% }
                   else if (tenantLicense.ValidityEnd.HasValue && tenantLicense.ValidityEnd.Value < DateTime.Now)
                   { %>
                <a class="btn btn-info" id="btnActivate" href="<%=Url.Action("ActivateTenant", new { tenantId=tenant.Identifier })%>" title="<%: this.GetLocalResourceObject("t_ReactivateTenantLicense")%>"><%: this.GetLocalResourceObject("lbl_ReactivateLicense")%></a>
                <% } %>
                <% } %>
                <%=Html.CelloButton(Url.Action("Index","Tenant"), this.GetGlobalResourceObject("General","Cancel")) %>
                <% } %>
            </div>
        </div>
        <div class="row-fluid pd-25">
            <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantSuccessMessage")))
              { %>
            <div class="alert alert-success">
                <%=Html.CelloValidationMessage("TenantSuccessMessage")%>
            </div>
            <% }
              else if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
              { %>
            <div class="alert alert-danger">
                <%=Html.CelloValidationSummary(this.GetLocalResourceObject("e_Correcterror").ToString())%>
            </div>
            <% } %>
            <% if (ViewBag.AccessDenied != true)
               { %>
            <% if (!isIsvOrReseller)
               { %>
            <%=Html.Hidden("Tenant.TenantDetails.DataPartitionId", ViewData["ParentPartitionId"] ?? string.Empty)%>
            <% } %>
            <div id="tenantDetailsDiv" class="form_list">
                <section class="panel purple">
                    <header class="panel-heading">
                        <h4><%: this.GetLocalResourceObject("TenantDetails") %></h4>
                    </header>
                    <div class="panel-body">
                        <div class="blue">
                            <i class="fa fa-exclamation-cirlce"></i>
                            The URL is the application URL which the Tenant will use to access the application
                        </div>
                        <div class="divide-20"></div>
                        <%
                   try
                   {
                       frmTable = new CelloSaaS.View.DataViewTable("TenantDetailsForm", currentMode, formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, tenant, "Tenant", new FormAttributes() { ValidationSummaryMessage = "SummaryMessage" }, tenantId);

                       int appPort = this.Request.Url.Port;
                       if (this.Request.Url.Port == 80 || this.Request.Url.Port == 443)
                           appPort = 0;

                       var tempUri = new UriBuilder
                       {
                           Scheme = this.Request.Url.Scheme,
                           Host = "app." + this.Request.Url.DnsSafeHost
                       };

                       if (appPort > 0)
                           tempUri.Port = appPort;

                       string uri = tempUri.ToString();

                       frmTable.StartInsertSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_CodeString");
                       frmTable.AddFieldLabel("", "Tenant_CodeString", "TenantCode", true);
                       content.Append(Html.Hidden("Tenant.Identifier", tenant.Identifier));
                       content.Append(Html.Hidden("Tenant.TenantDetails.Identifier", tenant.TenantDetails.Identifier));
                       string namehelp = "An unique string to identify your company without spaces / special letters";
                       content.Append(Html.TextBox("Tenant.TenantDetails.TenantCodeString", tenant.TenantDetails.TenantCodeString, new { maxlength = 50, tooltip = namehelp, placeholder = namehelp, required = "required" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.TenantCodeString", "*"));
                       frmTable.AddFieldContent("Tenant_CodeString", content.ToString(), "Tenant.TenantDetails.TenantCodeString");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Name");
                       frmTable.AddFieldLabel("", "Tenant_Name", "TenantName", true);
                       content.Append(Html.TextBox("Tenant.TenantDetails.TenantName", tenant.TenantDetails.TenantName, new { maxlength = 100 ,required = "required"}));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.TenantName", "*"));
                       frmTable.AddFieldContent("Tenant_Name", content.ToString(), "Tenant.TenantDetails.TenantName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Description");
                       frmTable.AddFieldLabel("", "Tenant_Description", "Description");
                       content.Append(Html.TextArea("Tenant.TenantDetails.Description", tenant.TenantDetails.Description, new { onKeyDown = "limitText(this,1000)", onKeyUp = "limitText(this,1000)" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.Description", "*"));
                       frmTable.AddFieldContent("Tenant_Description", content.ToString(), "Tenant.TenantDetails.Description");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Website");
                       frmTable.AddFieldLabel("", "Tenant_Website", "Website");
                       content.Append(Html.TextBox("Tenant.TenantDetails.Website", tenant.TenantDetails.Website, new { maxlength = 255 }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.Website", "*"));
                       frmTable.AddFieldContent("Tenant_Website", content.ToString(), "Tenant.TenantDetails.Website");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_URL");
                       frmTable.AddFieldLabel("", "Tenant_URL", "URL");
                       content.Append(Html.TextBox("Tenant.TenantDetails.URL", tenant.TenantDetails.URL, new { maxlength = 255, placeholder = uri }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.URL", "*"));
                       frmTable.AddFieldContent("Tenant_URL", content.ToString(), "Tenant.TenantDetails.URL");

                       if (isIsvOrReseller)
                       {
                           content = new StringBuilder();
                           frmTable.StartTableCell("Tenant_EnableAutoDebit");
                           frmTable.AddFieldLabel("", "Tenant_EnableAutoDebit", "EnableAutoDebit");
                           content.Append("<br/>");
                           content.Append(Html.CheckBox("Tenant.TenantDetails.EnableAutoDebit", tenant.TenantDetails.EnableAutoDebit));
                           content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.EnableAutoDebit", "*"));
                           frmTable.AddFieldContent("Tenant_EnableAutoDebit", content.ToString());
                       }

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Types");
                       frmTable.AddFieldLabel("", "Tenant_Types", "Tenant Types", true);
                       content.Append(Html.DropDownList("SelectedTenantTypes", (IEnumerable<SelectListItem>)ViewData["Tenant.Types"], new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("Tenant.Types", "*"));
                       frmTable.AddFieldContent("Tenant_Types", content.ToString(), "Tenant.Types");

                       if (isIsvOrReseller)
                       {
                           content = new StringBuilder();
                           frmTable.StartTableCell("Tenant_DataPartitionId");
                           frmTable.AddFieldLabel("", "Tenant_DataPartitionId", "Tenant DataPartition", false);
                           content.Append(Html.DropDownList("Tenant.TenantDetails.DataPartitionId", null, new { style = "width:100%;" }));
                           content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.DataPartitionId", "*"));
                           frmTable.AddFieldContent("Tenant_DataPartitionId", content.ToString(), "Tenant.TenantDetails.DataPartitionId");
                       }

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_CompanySize");
                       frmTable.AddFieldLabel("", "Tenant_CompanySize", "Company Size", false);
                       content.Append(Html.DropDownList("Tenant.TenantDetails.CompanySize", (IEnumerable<SelectListItem>)ViewData["Tenant.CompanySize"], new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.CompanySize", "*"));
                       frmTable.AddFieldContent("Tenant_CompanySize", content.ToString(), "Tenant.TenantDetails.CompanySize");

                       frmTable.EndInsertSection();

                       frmTable.StartEditSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Name");
                       frmTable.AddFieldLabel("", "Tenant_Name", "TenantName", true);
                       content.Append(Html.Hidden("Tenant.Identifier", tenant.Identifier));
                       content.Append(Html.Hidden("Tenant.TenantDetails.Identifier", tenant.TenantDetails.Identifier));
                       content.Append(Html.Hidden("Tenant.TenantDetails.TenantCode", tenant.TenantDetails.TenantCode));
                       content.Append(Html.Hidden("Tenant.TenantDetails.TenantCodeString", tenant.TenantDetails.TenantCodeString));
                       content.Append(Html.TextBox("Tenant.TenantDetails.TenantName", tenant.TenantDetails.TenantName, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.TenantName", "*"));
                       frmTable.AddFieldContent("Tenant_Name", content.ToString(), "Tenant.TenantDetails.TenantName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Description");
                       frmTable.AddFieldLabel("", "Tenant_Description", "Description");
                       content.Append(Html.TextArea("Tenant.TenantDetails.Description", tenant.TenantDetails.Description, new { onKeyDown = "limitText(this,1000)", onKeyUp = "limitText(this,1000)" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.Description", "*"));
                       frmTable.AddFieldContent("Tenant_Description", content.ToString(), "Tenant.TenantDetails.Description");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Website");
                       frmTable.AddFieldLabel("", "Tenant_Website", "Website");
                       content.Append(Html.TextBox("Tenant.TenantDetails.Website", tenant.TenantDetails.Website, new { maxlength = 255 }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.Website", "*"));
                       frmTable.AddFieldContent("Tenant_Website", content.ToString(), "Tenant.TenantDetails.Website");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_URL");
                       frmTable.AddFieldLabel("", "Tenant_URL", "URL");
                       content.Append(Html.TextBox("Tenant.TenantDetails.URL", tenant.TenantDetails.URL, new { maxlength = 255 }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.URL", "*"));
                       frmTable.AddFieldContent("Tenant_URL", content.ToString(), "Tenant.TenantDetails.URL");

                       if (isIsvOrReseller)
                       {
                           content = new StringBuilder();
                           frmTable.StartTableCell("Tenant_EnableAutoDebit");
                           frmTable.AddFieldLabel("", "Tenant_EnableAutoDebit", "EnableAutoDebit");
                           content.Append("<br/>");
                           content.Append(Html.CheckBox("Tenant.TenantDetails.EnableAutoDebit", tenant.TenantDetails.EnableAutoDebit));
                           content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.EnableAutoDebit", "*"));
                           frmTable.AddFieldContent("Tenant_EnableAutoDebit", content.ToString());
                       }

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_Types");
                       frmTable.AddFieldLabel("", "Tenant_Types", "Tenant Types", true);
                       content.Append(Html.DropDownList("SelectedTenantTypes", (IEnumerable<SelectListItem>)ViewData["Tenant.Types"], new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("Tenant.Types", "*"));
                       frmTable.AddFieldContent("Tenant_Types", content.ToString(), "Tenant.Types");

                       if (isIsvOrReseller)
                       {
                           frmTable.StartTableCell("Tenant_DataPartitionId");
                           frmTable.AddFieldLabel("", "Tenant_DataPartitionId", "Data Partition", false);
                           frmTable.AddFieldContent("Tenant_DataPartitionId", "<br/>" + (ViewData["DataPartitionName"] != null ? ViewData["DataPartitionName"].ToString() : "NA"));
                       }

                       content = new StringBuilder();
                       frmTable.StartTableCell("Tenant_CompanySize");
                       frmTable.AddFieldLabel("", "Tenant_CompanySize", "Company Size", false);
                       content.Append(Html.DropDownList("Tenant.TenantDetails.CompanySize", (IEnumerable<SelectListItem>)ViewData["Tenant.CompanySize"], new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantDetails.CompanySize", "*"));
                       frmTable.AddFieldContent("Tenant_CompanySize", content.ToString(), "Tenant.TenantDetails.CompanySize");

                       frmTable.EndEditSection();

                       string table = frmTable.Render();
                       Response.Write(table);
                   }
                   catch
                   {
                   }
                        %>
                    </div>
                </section>
            </div>
            <div id="billingAddressDiv" class="form_list">
                <section class="panel green">
                    <header class="panel-heading">
                        <h4><%: this.GetLocalResourceObject("BillingAddress") %></h4>
                    </header>
                    <div class="panel-body">
                        <div class="blue">
                            <i class="fa fa-exclamation-cirlce"></i>
                            <%: this.GetLocalResourceObject("AddressInfo")%>
                        </div>
                        <div style="padding-top: 3px; width: 100%;">
                            &nbsp;
                        </div>
                        <%
                   try
                   {
                       frmTable = new CelloSaaS.View.DataViewTable("TenantAddressDetailsForm", currentMode, formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, tenant.Address, "Tenant.Address", tenantId);

                       frmTable.StartInsertSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Address1");
                       frmTable.AddFieldLabel("", "Address_Address1", "Address1", true);
                       content.Append(Html.Hidden("Tenant.Address.Identifier", tenant.Address.Identifier));
                       content.Append(Html.Hidden("Tenant.Address.AddressId", tenant.Address.AddressId));

                       content.Append(Html.TextBox("Tenant.Address.Address1", tenant.Address.Address1, new { maxlength = 1000 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.Address1", "*"));
                       frmTable.AddFieldContent("Address_Address1", content.ToString(), "Tenant.Address.Address1");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_City");
                       frmTable.AddFieldLabel("", "Address_City", "City", true);
                       content.Append(Html.TextBox("Tenant.Address.City", tenant.Address.City, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.City", "*"));
                       frmTable.AddFieldContent("Address_City", content.ToString(), "Tenant.Address.City");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_State");
                       frmTable.AddFieldLabel("", "Address_State", "State", true);
                       content.Append(Html.TextBox("Tenant.Address.State", tenant.Address.State, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.State", "*"));
                       frmTable.AddFieldContent("Address_State", content.ToString(), "Tenant.Address.State");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Country");
                       frmTable.AddFieldLabel("", "Address_Country", "Country", true);
                       content.Append(Html.DropDownList("Tenant.Address.CountryId", null, new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.CountryId", "*"));
                       frmTable.AddFieldContent("Address_Country", content.ToString(), "Tenant.Address.CountryId");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_PostalCode");
                       frmTable.AddFieldLabel("", "Address_PostalCode", "Zipcode", true);
                       content.Append(Html.TextBox("Tenant.Address.PostalCode", tenant.Address.PostalCode, new { maxlength = 15 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.PostalCode", "*"));
                       frmTable.AddFieldContent("Address_PostalCode", content.ToString(), "Tenant.Address.PostalCode");

                       frmTable.EndInsertSection();

                       frmTable.StartEditSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Address1");
                       frmTable.AddFieldLabel("", "Address_Address1", "Address1", true);
                       content.Append(Html.Hidden("Tenant.Address.Identifier", tenant.Address.Identifier));
                       content.Append(Html.Hidden("Tenant.Address.AddressId", tenant.Address.AddressId));

                       content.Append(Html.TextBox("Tenant.Address.Address1", tenant.Address.Address1, new { maxlength = 1000 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.Address1", "*"));
                       frmTable.AddFieldContent("Address_Address1", content.ToString(), "Tenant.Address.Address1");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_City");
                       frmTable.AddFieldLabel("", "Address_City", "City", true);
                       content.Append(Html.TextBox("Tenant.Address.City", tenant.Address.City, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.City", "*"));
                       frmTable.AddFieldContent("Address_City", content.ToString(), "Tenant.Address.City");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_State");
                       frmTable.AddFieldLabel("", "Address_State", "State", true);
                       content.Append(Html.TextBox("Tenant.Address.State", tenant.Address.State, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.State", "*"));
                       frmTable.AddFieldContent("Address_State", content.ToString(), "Tenant.Address.State");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Country");
                       frmTable.AddFieldLabel("", "Address_Country", "Country", true);
                       content.Append(Html.DropDownList("Tenant.Address.CountryId", null, new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.CountryId", "*"));
                       frmTable.AddFieldContent("Address_Country", content.ToString(), "Tenant.Address.CountryId");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_PostalCode");
                       frmTable.AddFieldLabel("", "Address_PostalCode", "Zipcode", true);
                       content.Append(Html.TextBox("Tenant.Address.PostalCode", tenant.Address.PostalCode, new { maxlength = 15 }));
                       content.Append(Html.CelloValidationMessage("Tenant.Address.PostalCode", "*"));
                       frmTable.AddFieldContent("Address_PostalCode", content.ToString(), "Tenant.Address.PostalCode");

                       frmTable.EndEditSection();

                       string table = frmTable.Render();
                       Response.Write(table);
                   }
                   catch
                   {

                   }
                        %>
                    </div>
                </section>
            </div>
            <div id="contactDetailsDiv" class="form_list">
                <section class="panel indigo">
                    <header class="panel-heading">
                        <h4><%: this.GetLocalResourceObject("ContactDetails") %></h4>
                    </header>
                    <div class="panel-body">
                        <div class="blue">
                            <i class="fa fa-exclamation-cirlce"></i>
                            <%: this.GetLocalResourceObject("ContactDetailsInfo")%>
                        </div>
                        <div style="padding-top: 3px; width: 100%;">
                            &nbsp;
                        </div>
                        <%
                   try
                   {
                       frmTable = new CelloSaaS.View.DataViewTable("TenantContactDetailsForm", currentMode, formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, tenant.ContactDetail, "Tenant.ContactDetail", tenantId);

                       frmTable.StartInsertSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_FirstName");
                       frmTable.AddFieldLabel("", "Contact_FirstName", "FirstName", true);
                       content.Append(Html.Hidden("Tenant.ContactDetail.Identifier", tenant.ContactDetail.Identifier));
                       content.Append(Html.Hidden("Tenant.ContactDetail.ContactId", tenant.ContactDetail.ContactId));

                       content.Append(Html.TextBox("Tenant.ContactDetail.FirstName", tenant.ContactDetail.FirstName, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.FirstName", "*"));
                       frmTable.AddFieldContent("Contact_FirstName", content.ToString(), "Tenant.ContactDetail.FirstName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_LastName");
                       frmTable.AddFieldLabel("", "Contact_LastName", "LastName", true);
                       content.Append(Html.TextBox("Tenant.ContactDetail.LastName", tenant.ContactDetail.LastName, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.LastName", "*"));
                       frmTable.AddFieldContent("Contact_LastName", content.ToString(), "Tenant.ContactDetail.LastName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_Phone");
                       frmTable.AddFieldLabel("", "Contact_Phone", "Phone", true);
                       content.Append(Html.TextBox("Tenant.ContactDetail.Phone", tenant.ContactDetail.Phone, new { maxlength = 50, tooltip = Resources.TenantResource.m_samplePhone, placeholder = Resources.TenantResource.m_samplePhone }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.Phone", "*"));
                       frmTable.AddFieldContent("Contact_Phone", content.ToString(), "Tenant.ContactDetail.Phone");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_Fax");
                       frmTable.AddFieldLabel("", "Contact_Fax", "Fax", false);
                       content.Append(Html.TextBox("Tenant.ContactDetail.Fax", tenant.ContactDetail.Fax, new { maxlength = 50 }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.Fax", "*"));
                       frmTable.AddFieldContent("Contact_Fax", content.ToString(), "Tenant.ContactDetail.Fax");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_Email");
                       frmTable.AddFieldLabel("", "Contact_Email", "Contact Email", true);
                       content.Append(Html.TextBox("Tenant.ContactDetail.Email", tenant.ContactDetail.Email, new { maxlength = 100, tooltip = Resources.TenantResource.m_sampleEmail, placeholder = Resources.TenantResource.m_sampleEmail }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.Email", "*"));
                       frmTable.AddFieldContent("Contact_Email", content.ToString(), "Tenant.ContactDetail.Email");

                       frmTable.EndInsertSection();

                       frmTable.StartEditSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_FirstName");
                       frmTable.AddFieldLabel("", "Contact_FirstName", "FirstName", true);
                       content.Append(Html.Hidden("Tenant.ContactDetail.Identifier", tenant.ContactDetail.Identifier));
                       content.Append(Html.Hidden("Tenant.ContactDetail.ContactId", tenant.ContactDetail.ContactId));

                       content.Append(Html.TextBox("Tenant.ContactDetail.FirstName", tenant.ContactDetail.FirstName, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.FirstName", "*"));
                       frmTable.AddFieldContent("Contact_FirstName", content.ToString(), "Tenant.ContactDetail.FirstName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_LastName");
                       frmTable.AddFieldLabel("", "Contact_LastName", "LastName", true);
                       content.Append(Html.TextBox("Tenant.ContactDetail.LastName", tenant.ContactDetail.LastName, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.LastName", "*"));
                       frmTable.AddFieldContent("Contact_LastName", content.ToString(), "Tenant.ContactDetail.LastName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_Phone");
                       frmTable.AddFieldLabel("", "Contact_Phone", "Phone", true);
                       content.Append(Html.TextBox("Tenant.ContactDetail.Phone", tenant.ContactDetail.Phone, new { maxlength = 50, tooltip = Resources.TenantResource.m_samplePhone, placeholder = Resources.TenantResource.m_samplePhone }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.Phone", "*"));
                       frmTable.AddFieldContent("Contact_Phone", content.ToString(), "Tenant.ContactDetail.Phone");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_Fax");
                       frmTable.AddFieldLabel("", "Contact_Fax", "Fax", false);
                       content.Append(Html.TextBox("Tenant.ContactDetail.Fax", tenant.ContactDetail.Fax, new { maxlength = 50 }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.Fax", "*"));
                       frmTable.AddFieldContent("Contact_Fax", content.ToString(), "Tenant.ContactDetail.Fax");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Contact_Email");
                       frmTable.AddFieldLabel("", "Contact_Email", "Contact Email", true);
                       content.Append(Html.TextBox("Tenant.ContactDetail.Email", tenant.ContactDetail.Email, new { maxlength = 100, tooltip = Resources.TenantResource.m_sampleEmail, placeholder = Resources.TenantResource.m_sampleEmail }));
                       content.Append(Html.CelloValidationMessage("Tenant.ContactDetail.Email", "*"));
                       frmTable.AddFieldContent("Contact_Email", content.ToString(), "Tenant.ContactDetail.Email");

                       frmTable.EndEditSection();
                       string table = frmTable.Render();
                       Response.Write(table);
                   }
                   catch
                   {
                   }
                        %>
                    </div>
                </section>
            </div>
            <% if (currentMode == CurrentFormMode.Insert)
               { %>
            <div id="companyUserDetailsDiv" class="form_list">
                <section class="panel blue">
                    <header class="panel-heading">
                        <h4><%: this.GetLocalResourceObject("TenantAdminUserDetails") %></h4>
                    </header>
                    <div class="panel-body">
                        <%
                   try
                   {
                       frmTable = new CelloSaaS.View.DataViewTable("TenantUserDetailsForm", currentMode, formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, tenant.TenantAdminUserdetail, "Tenant.Userdetails", tenantId);

                       frmTable.StartInsertSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("User_FirstName");
                       frmTable.AddFieldLabel("", "User_FirstName", "FirstName", true);
                       content.Append(Html.Hidden("Tenant.TenantAdminUserdetail.Identifier", tenant.TenantAdminUserdetail.Identifier));
                       content.Append(Html.Hidden("Tenant.TenantAdminUserdetail.UserId", tenant.TenantAdminUserdetail.UserId));

                       content.Append(Html.TextBox("Tenant.TenantAdminUserdetail.FirstName", tenant.TenantAdminUserdetail.FirstName, new { maxlength = 100, required = "required" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminUserdetail.FirstName", "*"));
                       frmTable.AddFieldContent("User_FirstName", content.ToString(), "Tenant.TenantAdminUserdetail.FirstName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("User_LastName");
                       frmTable.AddFieldLabel("", "User_LastName", "LastName", true);
                       content.Append(Html.TextBox("Tenant.TenantAdminUserdetail.LastName", tenant.TenantAdminUserdetail.LastName, new { maxlength = 100, required = "required" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminUserdetail.LastName", "*"));
                       frmTable.AddFieldContent("User_LastName", content.ToString(), "Tenant.TenantAdminUserdetail.LastName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("User_UserName");
                       frmTable.AddFieldLabel("", "User_UserName", "UserName", true);
                       content.Append(Html.Hidden("Tenant.TenantAdminMembershipdetail.TenantCode", tenant.TenantAdminMembershipdetail.TenantCode));

                       string tooltip = "Provide this value as either the external login username / email or phone number";
                       content.Append(Html.TextBox("Tenant.TenantAdminMembershipdetail.UserName", tenant.TenantAdminMembershipdetail.UserName, new { maxlength = 255, title = tooltip, placeholder = tooltip,required = "required" }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminMembershipdetail.UserName", "*"));
                       frmTable.AddFieldContent("User_UserName", content.ToString(), "Tenant.TenantAdminMembershipdetail.UserName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("MembershipDetails_EmailId");
                       frmTable.AddFieldLabel("", "MembershipDetails_EmailId", "Tenant Admin Email");
                       content.Append(Html.TextBox("Tenant.TenantAdminMembershipdetail.EmailId", tenant.TenantAdminMembershipdetail.EmailId, new { maxlength = 100, tooltip = Resources.TenantResource.m_sampleEmail, placeholder = Resources.TenantResource.m_sampleEmail }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminMembershipdetail.EmailId", "*"));
                       frmTable.AddFieldContent("MembershipDetails_EmailId", content.ToString(), "Tenant.TenantAdminUserdetail.EmailId");

                       frmTable.EndInsertSection();

                       frmTable.StartEditSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("User_FirstName");
                       frmTable.AddFieldLabel("", "User_FirstName", "FirstName", true);
                       content.Append(Html.Hidden("Tenant.TenantAdminUserdetail.Identifier", tenant.TenantAdminUserdetail.Identifier));
                       content.Append(Html.Hidden("Tenant.TenantAdminUserdetail.UserId", tenant.TenantAdminUserdetail.UserId));
                       content.Append(Html.Hidden("Tenant.TenantAdminUserdetail.AddressId", tenant.TenantAdminUserdetail.AddressId));
                       content.Append(Html.Hidden("Tenant.TenantAdminUserdetail.MembershipId", tenant.TenantAdminUserdetail.MembershipId));

                       content.Append(Html.TextBox("Tenant.TenantAdminUserdetail.FirstName", tenant.TenantAdminUserdetail.FirstName, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminUserdetail.FirstName", "*"));
                       frmTable.AddFieldContent("User_FirstName", content.ToString(), "Tenant.TenantAdminUserdetail.FirstName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("User_LastName");
                       frmTable.AddFieldLabel("", "User_LastName", "LastName", true);
                       content.Append(Html.TextBox("Tenant.TenantAdminUserdetail.LastName", tenant.TenantAdminUserdetail.LastName, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminUserdetail.LastName", "*"));
                       frmTable.AddFieldContent("User_LastName", content.ToString(), "Tenant.TenantAdminUserdetail.LastName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("User_UserName");
                       frmTable.AddFieldLabel("", "User_UserName", "UserName", true);
                       content.Append(Html.Hidden("Tenant.TenantAdminMembershipdetail.Identifier", tenant.TenantAdminMembershipdetail.Identifier));
                       content.Append(Html.Hidden("Tenant.TenantAdminMembershipdetail.TenantCode", tenant.TenantAdminMembershipdetail.TenantCode));
                       content.Append(Html.TextBox("Tenant.TenantAdminMembershipdetail.UserName", tenant.TenantAdminMembershipdetail.UserName, new { maxlength = 255 }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminMembershipdetail.UserName", "*"));
                       frmTable.AddFieldContent("User_UserName", content.ToString(), "Tenant.TenantAdminMembershipdetail.UserName");

                       content = new StringBuilder();
                       frmTable.StartTableCell("MembershipDetails_EmailId");
                       frmTable.AddFieldLabel("", "MembershipDetails_EmailId", "Tenant Admin Email");
                       content.Append(Html.TextBox("Tenant.TenantAdminMembershipdetail.EmailId", tenant.TenantAdminMembershipdetail.EmailId, new { maxlength = 100, tooltip = Resources.TenantResource.m_sampleEmail, placeholder = Resources.TenantResource.m_sampleEmail }));
                       content.Append(Html.CelloValidationMessage("Tenant.TenantAdminMembershipdetail.EmailId", "*"));
                       frmTable.AddFieldContent("MembershipDetails_EmailId", content.ToString(), "Tenant.TenantAdminUserdetail.EmailId");

                       frmTable.EndEditSection();

                       string table = frmTable.Render();
                       Response.Write(table);
                   }
                   catch
                   {

                   }
                        %>
                    </div>
                </section>
            </div>
            <% } %>
            <div id="packageDetails" class="form_list">
                <section class="panel orange">
                    <header class="panel-heading">
                        <h4><%: this.GetLocalResourceObject("lbl_PackageSettings") %>
                            <% if (licenseHistory != null && licenseHistory.Count > 0)
                               {
                            %>
                            <small><a href="#" id="viewSubscriptionHistory" title="<%: this.GetLocalResourceObject("t_ViewSubscriptionHistory") %>"><i
                                class="fa fa-search"></i>&nbsp; <%: this.GetLocalResourceObject("lbl_ViewHistory") %></a></small>
                            <% } %>
                        </h4>
                    </header>
                    <div class="panel-body">
                        <div class="form-container">
                            <div class="col-md-6">
                                <div class="form-group <%:Html.ValidationMessage("TenantLicense.PackageId", "*") != null ? " has-error" : "" %>">
                                    <label id="Label_Package" class="mandatory">
                                        <%: this.GetLocalResourceObject("Package")%>
                                    </label>
                                    <%
                               var licPackages = ViewData["LicensePackages"] as List<LicensePackage>;
                               var licensePackageList = licPackages != null ? new SelectList(licPackages, "LicensepackageId", "Name", tenantLicense.PackageId) : new SelectList(new string[0]);
                                    %>
                                    <%: Html.Hidden("TenantLicense.TenantLicenseCode", tenantLicense.TenantLicenseCode)%>
                                    <%: Html.DropDownList("TenantLicense.PackageId", licensePackageList, this.GetLocalResourceObject("lbl_ChoosePackage").ToString(), new { style="width:100%;" })%>
                                    <%=Html.CelloValidationMessage("TenantLicense.PackageId", "*")%>
                                </div>
                            </div>
                            <% bool showPricePlan = isIsvOrReseller && UserIdentity.HasPrivilege(CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.ViewPricePlan); %>
                            <% if (showPricePlan)
                               { %>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label id="Label_PricePlan"><%: this.GetLocalResourceObject("lbl_PricePlan")%></label>
                                    <%
                                   var pricePlanList = ViewData["PricePlanList"] as IEnumerable<SelectListItem> ?? new List<SelectListItem>() { new SelectListItem { Text = this.GetLocalResourceObject("lbl_ChoosePlan").ToString(), Value = "" } };
                                    %>
                                    <%: Html.DropDownList("TenantLicense.PricePlanId", pricePlanList, new { style="width:100%;" })%>
                                    <%=Html.CelloValidationMessage("TenantLicense.PackageId", "*")%>
                                    <%: Html.Hidden("hidden_PricePlanId", tenantLicense.PricePlanId) %>
                                </div>
                            </div>
                            <% } %>
                            <div class="col-md-6">
                                <div class="form-group <%:Html.ValidationMessage("TenantLicense.ValidityStart", "*") != null ? " has-error" : "" %>">
                                    <label for="TenantLicense_ValidityStart" class="mandatory">
                                        <%: this.GetLocalResourceObject("lbl_SubscriptionStartDate")%></label>
                                    <% if (currentMode == CurrentFormMode.Edit)
                                       { %>
                                    <%: Html.TextBox("TenantLicense.ValidityStart", tenantLicense.ValidityStart.ToUIDateString(), new { @readonly="readonly" })%>
                                    <% }
                                       else
                                       {%>
                                    <%: Html.TextBox("TenantLicense.ValidityStart", DateTime.Now.ToUIDateString(), new { @class = "datetime", @readonly="readonly" })%>
                                    <% } %>
                                    <%=Html.CelloValidationMessage("TenantLicense.ValidityStart", "*")%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="TenantLicense_ValidityEnd"><%: this.GetLocalResourceObject("lbl_SubscriptionEndDate")%></label>
                                    <%: Html.TextBox("TenantLicense.ValidityEnd", tenantLicense.ValidityEnd.HasValue ? tenantLicense.ValidityEnd.Value.ToUIDateString() : string.Empty, new { @class = "datetime" })%>
                                    <%=Html.CelloValidationMessage("TenantLicense.ValidityEnd", "*")%>
                                </div>
                            </div>
                            <% if (showPricePlan)
                               { %>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="TenantLicense_TrialEndDate"><%: this.GetLocalResourceObject("lbl_TrialEndDate")%></label>
                                    <%: Html.TextBox("TenantLicense.TrialEndDate", tenantLicense.TrialEndDate.HasValue ? tenantLicense.TrialEndDate.Value.ToUIDateString() : string.Empty, new { @class = "datetime" })%>
                                    <%=Html.CelloValidationMessage("TenantLicense.TrialEndDate", "*")%>
                                </div>
                            </div>
                            <% } %>
                        </div>
                    </div>
                </section>
            </div>
            <%
                   if (currentMode == CurrentFormMode.Insert && CelloSaaS.Library.Helpers.ConfigHelper.EnableExternalAuthentication)
                   {
            %>
            <div class="row-fluid">
                <section class="panel red">
                    <header class="panel-heading">
                        <h4>Authorization Providers</h4>
                    </header>
                    <div class="panel-body">

                        <div class='col-md-6'>
                            <div class='form-group'>
                                <% if (ViewBag.TenantIdProvider != null)
                                   {
                                %>
                                <label for="TenantIdProvider">Identity Provider</label>
                                <%: Html.DropDownList("TenantIdProvider") %>
                                <%
                                   }
                                %>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                <%
                       if (ViewBag.UserAuthenticationType != null)
                       {
                                %>
                                <label for="UserAuthenticationType">Authentication Types</label>
                                <%: Html.DropDownList("UserAuthenticationType") %>
                                <%
                       }
                                %>
                            </div>
                        </div>

                        <div class="ldapconfig" style="display: none">
                            <% Html.RenderPartial("LDAPEndpointConfig", false); %>
                        </div>
                    </div>
                </section>
            </div>
            <% }
                   else
                   {
            %>
            <div class="row-fluid">
                <section class="panel red">
                    <header class="panel-heading">
                        <h4>Authorization Providers</h4>
                    </header>
                    <div class="panel-body">
                        <div class='col-md-6'>
                            <div class='form-group'>
                                <label>Identity Provider</label>
                                <label><%:ViewBag.Provider %></label>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                <label>Authentication Type</label>
                                <label><%:ViewBag.AuthenticationType %></label>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <%
                   }
                   if (isIsvOrReseller)
                   { %>
            <div id="taxRate" class="form_list">
                <section class="panel purple">
                    <header class="panel-heading">
                        <h4>
                            <%: this.GetLocalResourceObject("lbl_TaxRate") %>
                        </h4>
                    </header>
                    <div class="panel-body">
                        <% Html.RenderPartial("PartialTaxes");%>
                    </div>
                </section>
            </div>
            <% } %>
            <div class="pull-right">
                <% if (ViewBag.AccessDenied != true)
                   { %>
                <%=Html.CelloSubmitButton("<i class=\"fa fa-save\"></i>&nbsp;"+this.GetGlobalResourceObject("General","Save")) %>
                <%=Html.CelloButton(Url.Action("Index","Tenant"), this.GetGlobalResourceObject("General","Cancel")) %>
                <% } %>
            </div>
        </div>
        <% } %>
    </form>
    <% Html.RenderPartial("PartialLicenseHistory", licenseHistory); %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select').select2({ width: "100%" });

            $('input.datetime').datepicker({ format: '<%= CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>' });

            $('#btnActivate').click(function () {
                if (confirm('<%: this.GetLocalResourceObject("q_ReActivate")%>')) {
                    return true;
                }
                return false;
            });

            $('#btnDeactivate').click(function () {
                if (confirm('<%: this.GetLocalResourceObject("q_Deactivate")%>')) {
                    return true;
                }
                return false;
            });

            $('#taxpanel').on('click', 'a.delPriceSlabs', function (e) {
                e.preventDefault();
                var el = $(this).closest('tr');
                el.animate({ backgroundColor: 'lightcyan' }, 100).fadeOut(200, function () {
                    el.remove();

                    $('.tenantTaxName').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].Name';
                    });

                    $('.tenantTaxDesc').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].Description';
                    });

                    $('.tenantTaxPercentage').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].Percentage';
                    });

                    $('.tenantTaxTaxOrder').each(function (index) {
                        this.name = 'tenantTaxRate[' + index + '].TaxOrder';
                    });
                });

                return false;
            });

            $('#tenantLicenseHistory').modal({ show: false });
            $('#viewSubscriptionHistory').click(function (e) {
                e.preventDefault();
                $('#tenantLicenseHistory').modal('show');
                return false;
            });

            $("select[name='TenantLicense.PackageId']").change(function () {
                var packageId = $(this).val();
                var url = '/Tenant/GetPricePlans';

                var pid = $('#hidden_PricePlanId').val();

                $("select[name='TenantLicense.PricePlanId']").empty();
                $("select[name='TenantLicense.PricePlanId']").trigger('change');

                $.post(url, { packageId: packageId }, function (data) {
                    var selectListHtml = '';
                    if (data && !data.Error) {
                        for (i = 0; i < data.length; i++) {
                            selectListHtml += '<option value="' + data[i].Value + '"';
                            if (pid && pid.length > 0 && data[i].Value == pid) {
                                selectListHtml += ' selected="selected" ';
                            }
                            selectListHtml += '>' + data[i].Text + '</option>';
                        }

                        $("select[name='TenantLicense.PricePlanId']").html(selectListHtml);
                    }

                    $("select[name='TenantLicense.PricePlanId']").trigger('change');
                }, 'json');
            });

            $("select[name='TenantLicense.PackageId']").trigger('change');
        });
    </script>
    <script type="text/javascript">

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            }
        }

        var authProviders = JSON.parse('<%=ViewBag.Providers ?? "{}"%>');
        var authTypes = JSON.parse('<%=ViewBag.AuthTypes ?? "{}"%>');

        $(function () {

            var ddlIdProviders = $("#TenantIdProvider");
            var ddlAuthTypes = $("#UserAuthenticationType");

            updateAuthTypes($(ddlIdProviders).val());

            $(ddlIdProviders).change(function () {
                var value = $(this).val();
                $(ddlAuthTypes).val('');
                $(ddlAuthTypes).html('');
                $(ddlAuthTypes).select2({ width: "100%" });
                updateAuthTypes(value);
            });

            function updateAuthTypes(currentProvider) {
                for (var i = 0; i < authTypes.length; i++) {
                    if (authTypes[i].ProviderId == currentProvider) {
                        if (i == 0) {
                            $(ddlAuthTypes).append($('<option></option>').val(authTypes[i].AuthTypeId).text(authTypes[i].AuthTypeName));
                        }
                        else {
                            $(ddlAuthTypes).append($('<option></option>').val(authTypes[i].AuthTypeId).text(authTypes[i].AuthTypeName));
                        }
                    }
                }

                if ($(ddlIdProviders).val() == "221480d5-4ae4-4c9d-9c1d-025716ff33b0") {
                    $('.ldapconfig').show();
                }
                else {
                    $('.ldapconfig').hide();
                }

                $(ddlAuthTypes).select2({ width: "100%" });
            }
        });
    </script>
</asp:Content>
