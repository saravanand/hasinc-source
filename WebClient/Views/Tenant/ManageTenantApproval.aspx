﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        function initializeDataTables() {
            filterTable = $('#dataList').dataTable({
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "iDisplayLength": 10,
                "aaSorting": [[3, "desc"]],
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1, -2] }]
            });
        }

        $(document).ready(function () {
            initializeDataTables();

            $(document).on('keypress','#searchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });


        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }

        function ApproveTenant(tenantId) {
            $.post('<%=Url.Action("ApproveTenant")%>', { tenantId: tenantId }, approve_callBack);
            return false;
        }
        function RejectTenant(tenantId) {
            $.post('<%=Url.Action("RejectTenant")%>', { tenantId: tenantId }, approve_callBack);
            return false;
        }
        function approve_callBack(data) {
            $("#partialPageId").html(data);
            initializeDataTables();
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("lbl_ManageApproval")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="grid simple">
            <div class="grid-title">
                <div class="row">
                    <div class="col-sm-5 m-b-xs">
                        <div id="user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" onclick="DoSearch();" type="button"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-body">
                <div class="grid-part" id="partialPageId">
                    <% Html.RenderPartial("PartialManageTenantApproval"); %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
