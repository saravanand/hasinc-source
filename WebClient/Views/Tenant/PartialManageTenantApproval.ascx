﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<% Html.RenderPartial("StatusMessage"); %>
<% if (ViewData["TenantList"] != null && ((IEnumerable<TenantDetails>)ViewData["TenantList"]).Count() > 0)
   {
       Html.Grid((IEnumerable<TenantDetails>)ViewData["TenantList"]).Columns(
        column =>
        {
            column.For(col => col.TenantCodeString).Named(this.GetLocalResourceObject("lbl_TenantCode").ToString());
            column.For(col => col.TenantName).Named(this.GetLocalResourceObject("lbl_TenantName").ToString());
            column.For(col => col.ParentTenant.TenantName).Named(this.GetLocalResourceObject("lbl_ParentTenantName").ToString());
            column.For(col => col.ApprovalStatus).Named(this.GetLocalResourceObject("lbl_ApprovalStatus").ToString());
            column.For(col => (!col.ApprovalStatus.Equals(TenantApprovalStatus.WAITINGFORAPPROVAL) ? (col.ApprovalStatus.Equals(TenantApprovalStatus.APPROVED) ? "-" : "<a id='approve' href='#' title='Approve \"" + col.TenantName + "\"' onclick =ApproveTenant('" + col.TenantCode + "')>ApproveStatus</a>".Replace("ApproveStatus", "<i class='fa fa-check-circle'></i>")) : "<a id='approve' href='#' title='Approve \"" + col.TenantName + "\"' onclick =ApproveTenant('" + col.TenantCode + "')>ApproveStatus</a>".Replace("ApproveStatus", "<i class='fa fa-check-circle'></i>")))
                .Named(this.GetLocalResourceObject("Approve").ToString())
                .HeaderAttributes(style => "text-align:center;")
                .Attributes(style => "text-align:center;").DoNotEncode();
            column.For(col => (!col.ApprovalStatus.Equals(TenantApprovalStatus.WAITINGFORAPPROVAL) ? (col.ApprovalStatus.Equals(TenantApprovalStatus.APPROVED) ? "-" : "") : "<a id='reject' href='#' title='Reject \"" + col.TenantName + "\"' onclick =RejectTenant('" + col.TenantCode + "')>RejectStatus</a>".Replace("RejectStatus", "<i class='fa fa-times-circle'></i>")))
                .Named(this.GetLocalResourceObject("Reject").ToString())
                .HeaderAttributes(style => "text-align:center;")
                .Attributes(style => "text-align:center;").DoNotEncode();
        }).Attributes(id => "dataList", @class => "celloTable").Render(); 
%>
<%
   }
   else
   {%>
<div class="alert alert-info">
    <%: this.GetGlobalResourceObject("General","m_NoData") %>
</div>
<%}%>
