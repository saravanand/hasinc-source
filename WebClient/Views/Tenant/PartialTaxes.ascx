﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    var taxRate = ViewData["TaxRate"] as TaxRate ?? new TaxRate();                
%>
<div id="taxpanel" class="form-container">
    <div class="form-group">
        <label id="Label_description"><%: this.GetLocalResourceObject("description")%></label>
        <div>
            <%=Html.Hidden("TaxRate.TaxRateId", taxRate.TaxRateId)%>
            <%=Html.Hidden("TaxRate.TenantId", taxRate.TenantId)%>
            <%=Html.Hidden("hdnVendorContacts")%>
            <%: Html.TextArea("TaxRate.Description", taxRate.Description, new { @class="form-control", maxlength = 500, style = "min-height:50px;" })%>
        </div>
    </div>
     <div class="pull-right">
        <a href="#" class="btnAddTenantTax btn btn-info" title="<%: this.GetLocalResourceObject("t_AddNewTenantTaxRate") %>"><i class="fa fa-plus"></i>&nbsp;<%=this.GetGlobalResourceObject("General","Add") %></a>
    </div>
    <table class="celloTable" id="tenantTaxTable">
        <thead>
            <tr>
                <th><%: this.GetLocalResourceObject("name")%>
                </th>
                <th><%: this.GetLocalResourceObject("description")%>
                </th>
                <th><%: this.GetLocalResourceObject("percentage")%>
                </th>
                <th><%: this.GetLocalResourceObject("order")%>
                </th>
                <th style="width: 30px;"></th>
            </tr>
        </thead>
        <tbody>
            <% if (taxRate.TenantTaxRates != null)
               {
                   int cnt = -1;
                   foreach (var ttr in taxRate.TenantTaxRates.OrderBy(x => x.TaxOrder))
                   {
                       cnt += 1;
            %>
            <tr>
                <td>
                    <input type="text" name="tenantTaxRate[<%= cnt %>].Name" value="<%=ttr.Name %>" style="width: 300px" maxlength="150" class="tenantTaxName" required="required"/>
                </td>
                <td>
                    <input type="text" name="tenantTaxRate[<%= cnt%>].Description" value="<%=ttr.Description %>" style="width: 300px" maxlength="300" class="tenantTaxDesc" />
                </td>
                <td style="width: 100px">
                    <div class="input-group col-xs-2">
                        <input type="text" name="tenantTaxRate[<%=cnt%>].Percentage" value="<%=ttr.Percentage %>" style="width: 50px; text-align: right;" maxlength="5" class="tenantTaxPercentage" required="required"/>
                        <span class="input-group-addon">%</span>
                    </div>
                </td>
                <td style="width: 50px">
                    <input type="text" name="tenantTaxRate[<%=cnt%>].TaxOrder" value="<%=ttr.TaxOrder %>" style="width: 50px; text-align: right;" maxlength="3" class="tenantTaxTaxOrder"  required="required"/>
                </td>
                <td class="halign">
                    <a href="#" class="delPriceSlabs"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <%
                   }
               } 
            %>
        </tbody>
    </table>
   
</div>
<script type="text/javascript">
    $(function () {
        $('a.btnAddTenantTax').click(function (e) {

            var cnt = $('table#tenantTaxTable input.tenantTaxName').length;
            var trHtml = '<tr>';
            trHtml += '<td> <input type="text" name="tenantTaxRate[' + cnt + '].Name" placeholder="Tax name" style="width:300px" maxlength="150" class="tenantTaxName"/></td>';
            trHtml += '<td> <input type="text" name="tenantTaxRate[' + cnt + '].Description" placeholder="description" style="width:300px" maxlength="300"/></td>';
            trHtml += '<td> <div class="input-group col-xs-2"><input type="text" name="tenantTaxRate[' + cnt + '].Percentage" value="0" style="width: 50px" maxlength="5"/><span class="input-group-addon">%</span></div></td>';
            trHtml += '<td> <input type="text" name="tenantTaxRate[' + cnt + '].TaxOrder" value="0" style="width: 50px" maxlength="3"/></td>';
            trHtml += '<td class="halign"><a href="#" class="delPriceSlabs"><i class="fa fa-trash-o"></i></a></td>';
            trHtml += '</tr>';
            $('table#tenantTaxTable tbody').append(trHtml);
            return false;
        });
    });
</script>

