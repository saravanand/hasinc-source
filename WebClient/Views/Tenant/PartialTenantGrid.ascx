﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CelloSaaS.Model.TenantManagement.Tenant>>" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
    var dateForamt = CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat();
    dateForamt = dateForamt.Contains("yyyy") ? dateForamt.Replace("yyyy", "yy") : dateForamt.Replace("yy", "y");
%>
<div class="grid-part">
    <%  if (Model != null && Model.Count > 0)
        { %>
    <%
            bool showActivateColumn = Convert.ToBoolean(ViewData["ShowActivateColumn"] ?? "False");
            string tenantId = TenantContext.GetTenantId(new TenantDetails().EntityIdentifier);

            new CelloSaaS.View.Controls.CelloGrid.CelloGrid<Tenant>(this.Model, this.Html.ViewContext.HttpContext.Response.Output, this.Html.ViewContext, "TenantDetailsGrid", tenantId).CelloColumns(
                   column =>
                   {
                       //column.ForColumn(col => col.TenantDetails.TenantCodeString).SetFieldIdentifier("Tenant_CodeString").Named("~" + this.GetLocalResourceObject("TenantCodeString").ToString() + "~").Attributes(style => "min-width:80px;");
                       column.ForColumn(col => string.Format("<a href='#' title='" + this.GetLocalResourceObject("t_Clicktoviewfulldetails") + "' class='view-unapproved-tenant' data-tenant='{0}'>{1}</a>", Newtonsoft.Json.JsonConvert.SerializeObject(col), col.TenantDetails.TenantName.Truncate()))
                           .SetFieldIdentifier("Tenant_Name")
                           .Named("~" + this.GetGlobalResourceObject("General", "Name") + "~")
                           .Attributes(style => "min-width:100px;").DoNotEncode();
                       //column.ForColumn(col => col.TenantDetails.Description.Truncate()).SetFieldIdentifier("Tenant_Description").Named("~" + this.GetGlobalResourceObject("General", "Description") + "~").Attributes(style => "min-width:120px;");
                       column.ForColumn(col => col.ContactDetail == null ? "-" : col.ContactDetail.FirstName).Named(this.GetLocalResourceObject("lbl_FirsName").ToString());
                       column.ForColumn(col => col.ContactDetail == null ? "-" : col.ContactDetail.LastName).Named(this.GetLocalResourceObject("lbl_LastName").ToString());
                       column.ForColumn(col => col.ContactDetail == null ? "-" : col.ContactDetail.Phone).Named(this.GetLocalResourceObject("lbl_Phone").ToString());
                       column.ForColumn(col => col.ContactDetail == null ? "-" : col.ContactDetail.Email).Named(this.GetLocalResourceObject("lbl_Email").ToString());
                       //column.ForColumn(col => col.TenantDetails.Website).SetFieldIdentifier("Tenant_Website").Named("~" + this.GetGlobalResourceObject("General", "Website") + "~").Attributes(style => "min-width:110px;").DoNotEncode();
                       //column.ForColumn(col => col.TenantDetails.URL).SetFieldIdentifier("Tenant_URL").Named("~" + this.GetGlobalResourceObject("General", "URL") + "~").Attributes(style => "min-width:100px;").DoNotEncode();
                       column.ForColumn(col => col.TenantDetails.CreatedOn.ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat())).Named(this.GetLocalResourceObject("lbl_RegisteredOn").ToString());
                       //column.ForColumn(col => col.TenantDetails.ApprovalStatus).SetFieldIdentifier("Tenant_ApprovalStatus").Named("~" + this.GetLocalResourceObject("ApprovalStatus") + "~");

                       if (showActivateColumn)
                       {
                           column.ForColumn(col =>
                               col.TenantDetails.ApprovalStatus == TenantApprovalStatus.WAITINGFORAPPROVAL ?
                                 string.Format("<a href='{0}' title='" + this.GetLocalResourceObject("t_Clicktoactivatethistenant") + "'><i class='fa fa-check-circle'></i></a>", Url.Action("ActivateSelfRegisteredTenant", new { tenantId = col.TenantDetails.TenantCode }))
                                 : "-")
                                .Named(this.GetLocalResourceObject("lbl_Activate").ToString())
                                .Attributes(@class => "halign", style => "min-width:30px;")
                                .HeaderAttributes(@class => "noSortCol halign")
                                .DoNotEncode();

                           column.ForColumn(col =>
                               col.TenantDetails.ApprovalStatus == TenantApprovalStatus.WAITINGFORAPPROVAL ?
                                string.Format("<a href='{0}' onclick='return confirm(\"'" + this.GetLocalResourceObject("t_Areyousuretorejectthistenant") + "'\");' title='" + this.GetLocalResourceObject("t_ClicktoRejectthistenant") + "'><i class='fa fa-ban'></i></a>", Url.Action("RejectTenant", new { tenantId = col.TenantDetails.TenantCode, redirect = true }))
                                : "-")
                                .Named(this.GetLocalResourceObject("lbl_Reject").ToString())
                                .Attributes(@class => "halign", style => "min-width:30px;")
                                .HeaderAttributes(@class => "noSortCol halign")
                                .DoNotEncode();
                       }
                   }).Attributes(id => "nonApproved", @class => "celloTable").Render();                                                         
    %>
    <%}
        else
        {%>
    <div class="alert alert-info">
        <%: this.GetLocalResourceObject("NoTenantAvailable")%>
    </div>
    <%}%>
</div>
<script type="text/template" id="tenantTemplate">
    <table class="table">
        <tbody>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_Code")%></label></td>
                <td>${TenantDetails.TenantCodeString}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_TenantName") %></label>
                </td>

                <td>${TenantDetails.TenantName}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_CompanySize") %></label></td>
                <td>${TenantDetails.CompanySize || "NA"}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_Website") %></label>
                </td>
                <td>${TenantDetails.Website || "NA"}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_LoginURL") %></label>
                </td>
                <td>${TenantDetails.URL || "NA"}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_FirsName") %></label>
                </td>
                <td>${ContactDetail.FirstName || "NA"}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_LastName") %></label>
                </td>
                <td>${ContactDetail.LastName || "NA"}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_Email") %></label>
                </td>
                <td>${ContactDetail.Email || "NA"}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_Phone") %></label>
                </td>
                <td>${ContactDetail.Phone || "NA"}</td>
            </tr>
            <tr>
                <td>
                    <label><%:this.GetLocalResourceObject("lbl_RegisteredOn") %></label>
                </td>
                <td>${TenantDetails.CreatedOn}</td>
            </tr>
            <tr>
                <td>
                    <label><%=this.GetGlobalResourceObject("General","Status") %></label>
                </td>
                <td>${TenantDetails.ApprovalStatus}</td>
            </tr>
        </tbody>
    </table>
</script>
<script src="/Scripts/jquery.tmpl.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#unApprovedTenantView").modal({ show: false });
        $("table#nonApproved .view-unapproved-tenant").click(function () {
            var tenant = $(this).data('tenant');
            var date = $.datepicker.formatDate('<%:dateForamt%>', new Date(tenant.TenantDetails.CreatedOn));
            tenant.TenantDetails.CreatedOn = date;
            if (tenant.ContactDetail == null)
                tenant.ContactDetail = {};
            $("#unApprovedTenantView .modal-body").empty();
            $.tmpl($("#tenantTemplate").html(), tenant).appendTo("#unApprovedTenantView .modal-body");
            $("#unApprovedTenantView").modal("show");
        });
    });
</script>
