﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaSApplication.Models" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%    
    var currentTenant = ViewBag.currentTenant as Tenant;
    bool isIsvOrReseller = currentTenant.Types != null && currentTenant.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);

    var lstTenants = ViewData["TenantDetails"] as IEnumerable<TenantViewModel> ?? new List<TenantViewModel>();
    var allChildTenants = ViewBag.AllChildTenants as Dictionary<string, TenantDetails>;
    var totalUsersList = ViewBag.TotalUsers as Dictionary<string, Dictionary<int, int>>;
    var totalOnlineUsersList = ViewBag.TotalOnlineUsers as Dictionary<string, int>;
    var notActivatedTenants = ViewData["WaitingTenants"] as Dictionary<string, Tenant> ?? new Dictionary<string, Tenant>();
    var selectedyear = (int?)ViewData["year"] ?? DateTime.Now.Year;
    var yearStart = DateTime.Parse("01/01/" + selectedyear, System.Globalization.CultureInfo.InvariantCulture);

    int maxCountInMonth = 0;
    var tmp = new List<dynamic>();
    IEnumerable<TenantViewModel> newTenants = null;
    IEnumerable<TenantViewModel> activeTenants = null;
    IEnumerable<TenantViewModel> trialTenants = null;
    IEnumerable<TenantViewModel> deactivateTenants = null;

    var totalUsers = totalUsersList == null ? 0 : totalUsersList.SelectMany(x => x.Value).Where(x => x.Key == 1).Sum(x => x.Value);
    var totalOnlineUsers = totalOnlineUsersList == null ? 0 : totalOnlineUsersList.Sum(x => x.Value);
    var totalChildTenants = allChildTenants == null ? 0 : allChildTenants.Count();

    trialTenants = lstTenants.Where(x => x.License != null && x.License.TrialEndDate.HasValue && x.License.TrialEndDate.Value > DateTime.Now);
    activeTenants = lstTenants.Where(x => x.License != null && (!x.License.ValidityEnd.HasValue || x.License.ValidityEnd.Value > DateTime.Now));
    newTenants = lstTenants.Where(x => x.TenantDetails.CreatedOn.Year == selectedyear);
    int payingTenants = activeTenants.Where(x => x.PaymentAccount != null && (x.License != null && (!x.License.TrialEndDate.HasValue || x.License.TrialEndDate.Value < DateTime.Now))).Count();
    deactivateTenants = lstTenants.Where(x => x.License != null && x.License.ValidityEnd.HasValue && x.License.ValidityEnd.Value > yearStart && x.License.ValidityEnd.Value < DateTime.Now);

    foreach (var m in Enumerable.Range(1, 12))
    {
        string monthName = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(m);
        int newcount = newTenants.Where(x => x.TenantDetails.CreatedOn.Month == m).Count();

        if (maxCountInMonth < newcount)
            maxCountInMonth = newcount;

        var item = new
        {
            category = monthName,
            value = newcount
        };
        tmp.Add(item);
    }

    var lstYears = new SelectList(Enumerable.Range(DateTime.Now.Year - 15, 16).Reverse(), selectedyear);
    string chartData = Newtonsoft.Json.JsonConvert.SerializeObject(tmp, Newtonsoft.Json.Formatting.None); 
%>
<section class="panel indigo" id="tenantDashbordDiv">
    <header class="panel-heading">
        <h4><%: this.GetLocalResourceObject("t_TenantStatistics") %></h4>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="tiles purple">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_ ActiveTenants") %>
                        </div>
                        <div class="heading">
                            <%:activeTenants.Count() %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles red">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_ NotActivated") %>
                        </div>
                        <div class="heading">
                            <%:notActivatedTenants.Count() %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles green">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_New") %> - <span class="semi-bold"><%:selectedyear %></span>
                        </div>
                        <div class="heading">
                            <%:newTenants.Count() %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles orange">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_Cancelled") %> - <span class="semi-bold"><%:selectedyear %></span>
                        </div>
                        <div class="heading">
                            <%:deactivateTenants.Count() %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <% if (isIsvOrReseller)
               { %>
            <div class="col-md-3">
                <div class="tiles lt-red">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_Paying") %>
                        </div>
                        <div class="heading">
                            <%:payingTenants %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles indigo">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_Trial") %>
                        </div>
                        <div class="heading">
                            <%:trialTenants.Count() %>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
            <% if (totalChildTenants > 0)
               { %>
            <div class="col-md-3">
                <div class="tiles blue">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_TotalTenants") %>
                        </div>
                        <div class="heading">
                            <%:totalChildTenants %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="tiles lt-green" title="<%: this.GetLocalResourceObject("t_TotalUsers") %>">
                    <div class="tiles-body">
                        <div class="tiles-title captilize">
                            <%: this.GetLocalResourceObject("lbl_TotalUsers") %>
                        </div>
                        <div class="heading">
                            <%:totalUsers %>/<%:totalOnlineUsers %>                            
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('#tenant_year').select2();
        $('#tenant_year').change(function () {
            var year = parseInt($(this).val());
            var data = { 'year': year };
            $('#tenantdash').load('<%:Url.Action("TenantDashboard")%>', data);
            return false;
        });
    });
</script>
