<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="MvcContrib.UI.Grid.ActionSyntax" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable;
        $(document).ready(function () {
            var noSortColumns = $("table#TenantDetails th.noSortCol");
            var noSortIndexArray = new Array();
            if (noSortColumns.length > 0) {
                noSortColumns.each(function () {
                    var indexVal = noSortColumns.parent("tr").children().index(this);
                    noSortIndexArray.push(indexVal);
                });
            }

            approvedTenantfilterTable = $('table#TenantDetails').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray}]
            });

            nonApprovedTenantFilterTable = $('table#nonApproved').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1]}]
            });

            $('#approvedTenantTableSearchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    SearchApprovedTenants();
                    e.preventDefault();
                }
            });

            $('#waitingTenantTableSearchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    SearchWaitingTenants();
                    e.preventDefault();
                }
            });
        });

        function DoSearch(id, selectedTable) {
            var searchText = $('#' + id).val();
            if (searchText == 'Search') {
                searchText = '';
            }
            selectedTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }

        function SearchApprovedTenants() {
            DoSearch('approvedTenantTableSearchText', approvedTenantfilterTable);
        }

        function SearchWaitingTenants() {
            DoSearch('waitingTenantTableSearchText', nonApprovedTenantFilterTable);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="heading_container">
        <h1>
            <%: this.GetLocalResourceObject("lbl_ApprovedList") %>
        </h1>
        <div class="inner_hold">
            <% var editAccess = true || AccessControlProxy.CheckTenantAccessWithTenantScope(CelloSaaS.Library.UserIdentity.UserId, PrivilegeConstants.UpdateTenant, TenantContext.GetTenantId(new TenantDetails().EntityIdentifier), new TenantDetails().EntityIdentifier, CelloSaaS.Model.FetchType.Edit, TenantContext.GetTenantId(new TenantDetails().EntityIdentifier));%>
            <% if (CelloSaaS.Library.UserIdentity.HasPrivilege(PrivilegeConstants.AddTenant) && editAccess)
               {  %>
            <div class="button">
                <a href="<%: Url.Action("ManageTenant","Tenant") %>" title="<%: this.GetLocalResourceObject("lbl_AddNewTenant") %>">
                    <span> <i class="fa fa-plus"></i>
                        <%=this.GetGlobalResourceObject("General","Add") %></span></a></div>
            <% } %>
        </div>
    </div>
    <div class="clear">
    </div>
    <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantSuccessMessage")))
      { %>
    <div class="success">
        <%=Html.CelloValidationMessage("TenantSuccessMessage")%>
    </div>
    <%} %>
    <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantErrorMessage")))
       { %>
    <div class="error">
        <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
    </div>
    <%} %>
    <div class="clear">
    </div>
    <%
        if (ViewData["TenantDetails"] != null && ((List<TenantDetails>)ViewData["TenantDetails"]).Count > 0)
        {
    %>
    <div class="secondary_search_container">
        <div class="search_holder">
            <input type="text" id="approvedTenantTableSearchText" name="approvedTenantTableSearchText"
                placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
        </div>
        <div class="go_but_grey">
            <a href="#" onclick="SearchApprovedTenants();" title="<%=this.GetGlobalResourceObject("General","GO") %>">
                <%=this.GetGlobalResourceObject("General","GO") %></a></div>
    </div>
    <%
        }
            
    %>
    <div class="clear">
    </div>
    <% ViewData["editAccess"] = true; %>
    <% ViewData["cssClass"] = "TenantDetails"; %>
    <% Html.RenderPartial("PartialTenantGrid", ViewData["TenantDetails"], ViewData); %>
    <div class="clear">
    </div>
    <br />
    <div class="heading_container">
        <h1>
            <%: this.GetLocalResourceObject("lbl_WaitingList") %>
        </h1>
    </div>
    <%
        if (ViewData["WaitingTenants"] != null && ((List<TenantDetails>)ViewData["WaitingTenants"]).Count > 0)
        {
    %>
    <div class="secondary_search_container">
        <div class="search_holder">
            <input type="text" id="waitingTenantTableSearchText" name="waitingTenantTableSearchText"
                placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
        </div>
        <div class="go_but_grey">
            <a href="#" onclick="SearchWaitingTenants();" title="<%=this.GetGlobalResourceObject("General","GO") %>">
                <%=this.GetGlobalResourceObject("General", "GO")%></a></div>
    </div>
    <div class="clear">
    </div>
    <% ViewData["cssClass"] = "nonApproved"; %>
    <% ViewData["editAccess"] = false; %>
    <% ViewData["ShowActivateColumn"] = UserIdentity.IsInRole(RoleConstants.ProductAdmin); %>
    <% Html.RenderPartial("PartialTenantGrid", ViewData["WaitingTenants"], ViewData); %>
    <%            
        }
        else
        {
    %>
    <div class="clear">
    </div>
    <div class="info">
        <%: this.GetLocalResourceObject("NoTenantAvailable")%>
    </div>
    <% } %>
    <div class="clear">
    </div>
</asp:Content>
