﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<%@ Import Namespace="CelloSaaSApplication.Models" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    var searchCondition = ViewData["searchCondition"] as TenantViewSearch ?? new TenantViewSearch();
    var currentTenant = ViewData["currentTenant"] as Tenant;
    bool showBillStats = currentTenant.Types != null && currentTenant.Types.Any(x => x.ID == TenantTypeConstants.ISV || x.ID == TenantTypeConstants.Reseller);
    string dtFormat = CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat();
%>
<div id="tl-container">
    <%
        var lstTenants = ViewData["TenantDetails"] as IEnumerable<TenantViewModel>;
        var totalCount = Convert.ToInt16(ViewData["TotalCount"] ?? "0");
    %>
    <% if (totalCount == 0)
       { %>
    <div class="alert alert-info">
        <%: this.GetLocalResourceObject("e_NoTenants") %>
    </div>
    <% }
       else if (lstTenants != null && lstTenants.Count() > 0)
       {
    %>
    <% foreach (var item in lstTenants)
       {
           var lstUsages = item.UsageDetails;
    %>
    <div id="tenant<%:item.TenantDetails.TenantCode%>" style="position: relative;" class="row <%=(item.License.ValidityEnd.HasValue && item.License.ValidityEnd.Value <= DateTime.Now) ? "list-item red-border" : "list-item" %>">
        <%--<div class="lbox" style="display: table-cell; width: 165px; vertical-align: middle;">
            <div style="margin: 0 auto;">
                <%=Html.Logo(item.TenantDetails.TenantCode) %>
            </div>
        </div>--%>
        <div class="col-md-4">
            <h2 class="semi-bold no-margin">
                <a title=" <%: this.GetLocalResourceObject("t_ClicktoEdit") %>" href="<%=Url.Action("ManageTenant",new{ tenantId = item.TenantDetails.TenantCode }) %>"><%:item.TenantDetails.TenantName %></a>
                <%=item.TenantDetails.IsSelfRegistered ? "<small><i title='"+this.GetLocalResourceObject("t_Selfregisteredtenant")+"' class='fa fa-cloud'></i></small>" : "" %>
                <%=item.TenantDetails.EnableAutoDebit ? "<small><i title='"+this.GetLocalResourceObject("t_Autobilldebitenabled")+"' class='fa fa-credit-card'></i></small>" : "" %>
                <%=item.PackageDetails.IsEvaluation ? "<small><i title='"+this.GetLocalResourceObject("t_InEvaluationperiod")+"' class='fa fa-leaf'></i></small>" : "" %>
                <%=(item.PaymentAccount !=null && item.PaymentAccount.StoreKey.Split('/').Length == 2) ? string.Format("<img src='{0}' style='width:24px;margin-bottom: -5px;' title='{1}' alt=''/>", (item.PaymentAccount.AccountType == PaymentAccountType.PayPal ? Url.Content("~/Content/images/paypal.png") : Url.Content("~/Content/images/visa.png")), (item.PaymentAccount.AccountType == PaymentAccountType.PayPal ? "PayPal account" : "Credit card account")) : "" %>
            </h2>
            <p>
                <%: this.GetLocalResourceObject("m_MemberSince") %> <span class="badge" title="Created on: <%:item.TenantDetails.CreatedOn.ToUIDateTimeString() %>">
                    <%
           string mstr = string.Empty;
           var mdiff = (DateTime.Now - item.TenantDetails.CreatedOn);
           int tdays = (int)mdiff.TotalDays;
           if (tdays == 0)
           {
               if (mdiff.TotalMinutes < 60)
               {
                   mstr = (int)mdiff.TotalMinutes + this.GetLocalResourceObject("lbl_minutes").ToString();
               }
               else
               {
                   mstr = (int)mdiff.TotalHours + this.GetLocalResourceObject("lbl_hours").ToString();
               }
           }
           else if (tdays < DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month))
           {
               mstr = tdays + this.GetLocalResourceObject("lbl_days").ToString();
           }
           else if (tdays < 365)
           {
               mstr = (int)(tdays / 30) + this.GetLocalResourceObject("lbl_months").ToString();
           }
           else
           {
               mstr = (int)(tdays / 365) + this.GetLocalResourceObject("lbl_years").ToString();
           }
                    %>
                    <%:mstr %>
                </span>
            </p>
            <p>
                <i class="fa fa-link" title="<%: this.GetLocalResourceObject("t_TenantLoginURL") %>"></i>
                <%=!string.IsNullOrEmpty(item.TenantDetails.URL) ? string.Format(System.Globalization.CultureInfo.InvariantCulture,"<a href=\"{0}\" title=\"Open this link in new tab\"  target=\"_blank\">{0}</a>", item.TenantDetails.URL) : this.GetLocalResourceObject("lbl_NA").ToString() %>
            </p>
            <%--<div class="l-item">
                <span>Website: <a title="Click to visit tenants website!" href="<%:string.IsNullOrEmpty(item.TenantDetails.Website) ? "#" : item.TenantDetails.Website %>"><%:string.IsNullOrEmpty(item.TenantDetails.Website) ? "NA" : item.TenantDetails.Website %></a></span>
            </div>--%>
            <p>
                <i class="fa fa-envelope" title="<%: this.GetLocalResourceObject("t_Contactemail") %>"></i>
                <%=item.TenantContactDetail != null && !string.IsNullOrEmpty(item.TenantContactDetail.Email) ? string.Format(System.Globalization.CultureInfo.InvariantCulture,"<a href=\"mailto:{0}\" title=\"Click to open Email application\">{0}</a>", item.TenantContactDetail.Email): this.GetLocalResourceObject("lbl_NA").ToString()%>
            </p>
            <p>
                <i class="fa fa-phone" title="<%: this.GetLocalResourceObject("t_Contactphone") %>"></i>
                <%:item.TenantContactDetail != null && !string.IsNullOrEmpty(item.TenantContactDetail.Phone) ? item.TenantContactDetail.Phone : this.GetLocalResourceObject("lbl_NA").ToString()%>
            </p>
            <p>
                <span title="<%: this.GetLocalResourceObject("t_TenantType") %>">
                    <i class="icon icon-sitemap"></i>
                    <%:item.Types !=null && item.Types.Count() > 0 ? item.Types.First().Name : this.GetLocalResourceObject("lbl_NA").ToString() %>
                </span>
                <span title="<%: this.GetLocalResourceObject("t_Datapartitionname") %>" style="margin-left: 10px;">
                    <i class="fa fa-hdd"></i>
                    <%:!string.IsNullOrEmpty(item.DataPartitionName) ? item.DataPartitionName: this.GetLocalResourceObject("lbl_NA").ToString() %>
                </span>
            </p>
        </div>
        <div class="col-md-3" style="margin-top: 35px;">
            <div class="tiles blue added-margin">
                <div class="tiles-body">
                    <div class="tiles-title"><%:item.License.PackageName %><span class="semi-bold"><%=item.Plan != null ? "&nbsp;-&nbsp;" + item.Plan.Name: string.Empty %></span></div>
                    <div class="heading" title="<%:item.Plan!=null ? item.Plan.BillFrequency.ToString() : "" %>">
                        <% var planPrice = item.Plan != null ? item.Plan.Price : 0; %>
                        <%=Html.GetCurrencySymbol() %><span class="animate-number" data-animation-duration="1200" data-value="<%=planPrice %>"><%=planPrice %></span>
                    </div>
                    <div class="description" style="display: block;">
                        <div class="row">
                            <div class="pull-left" title="Validity start date">
                                <%:item.License.ValidityStart.ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat()) %>
                            </div>
                            <div class="pull-right">
                                <% if (item.License.TrialEndDate.HasValue && item.License.TrialEndDate.Value >= DateTime.Now)
                                   { %>
                                <span title="<%: this.GetLocalResourceObject("t_Trialwillendon") %> <%:item.License.TrialEndDate.Value.ToUIDateTimeString() %>"><%:item.License.TrialEndDate.Value.ToString(dtFormat) %></span>
                                <% }
                                   else if (item.License.ValidityEnd.HasValue && item.License.ValidityEnd.Value >= DateTime.Now)
                                   { %>
                                <span title="<%: this.GetLocalResourceObject("t_Subscriptionwillend") %> <%:item.License.ValidityEnd.Value.ToUIDateTimeString() %>"><%:item.License.ValidityEnd.Value.ToString(dtFormat) %></span>
                                <% }
                                   else if (item.License.ValidityEnd.HasValue)
                                   { %>
                                <span title="<%: this.GetLocalResourceObject("t_Subscriptionended") %> <%:item.License.ValidityEnd.Value.ToUIDateTimeString() %>"><%:item.License.ValidityEnd.Value.ToString(dtFormat) %></span>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% if (lstUsages != null && lstUsages.Count > 0)
               { %>
            <div style="text-align: right;">
                <a href="#" data-tenantid="<%:item.TenantDetails.TenantCode%>" onclick="return false;"
                    id="btnViewUsages<%:item.TenantDetails.TenantCode%>" data-placement="right" data-html="true"
                    title="<%: this.GetLocalResourceObject("lbl_TenantType") %> Usage values<div style='position: absolute;top: 9px;right: 10px;'><a data-target='#btnViewUsages<%:item.TenantDetails.TenantCode%>' href='#' class='btnPopoverClose'><i class='fa fa-times'></i></a></div>"><%: this.GetLocalResourceObject("lbl_ViewUsages") %></a>
                <% if (lstUsages.Any(x => x.Item5))
                   { %>
                <div class="usage-alert-div" style="display: inline-block;">
                    <a class="red" id="btnUsageAlertInfo<%:item.TenantDetails.TenantCode%>" href="#" title="<%: this.GetLocalResourceObject("t_UsageAlert") %>">
                        <i class="fa fa-info-circle"></i>
                    </a>
                </div>
                <% } %>
                <div id="divUsageLimitInfo<%:item.TenantDetails.TenantCode%>" style="display: none;">
                    <% foreach (var usage in lstUsages)
                       {
                           int maxPer = (int)(((usage.Item4 - usage.Item3) / usage.Item4) * 100);
                           int usedPer = 100 - maxPer;
                    %>
                    <div style="line-height: 25px;">
                        <span class="<%:usage.Item5 ? "red" : "" %>">
                            <%:usage.Item1 %></span>
                        <% if (usage.Item4 == 0.0)
                           { %>
                        <div style="display: inline-block; float: right; text-align: right;"><%:usage.Item3 %></div>
                        <% }
                           else
                           { %>
                        <div class="progress" title="<%: this.GetLocalResourceObject("m_Maxlimit") %> <%:usage.Item4 %><%: this.GetLocalResourceObject("m_used") %> <%:usage.Item3 %>">
                            <div class="bar" style="width: <%:usedPer %>%"><%:usedPer %>%</div>
                        </div>
                        <% } %>
                    </div>
                    <% } %>
                </div>
            </div>
            <% } %>
        </div>
        <div class="col-md-5 pull-right" style="margin-top: 35px;">
            <div class="pull-right">
                <ul class="drow">
                    <li>
                        <span class="info-title"><%: this.GetLocalResourceObject("lbl_TotalUsers") %></span>
                        <div class="number"><%:item.TotalUsers %></div>
                    </li>
                    <li class="viewOnlineUsers" data-tenantid="<%=item.TenantDetails.TenantCode %>" title="Click to view online users!">
                        <span class="info-title"><%: this.GetLocalResourceObject("lbl_OnlineUsers") %></span>
                        <div class="number"><%:item.OnlineUsers %></div>
                    </li>
                    <li>
                        <span class="info-title"><%: this.GetLocalResourceObject("lbl_ChildTenants") %></span>
                        <div class="number"><%:item.TotalChildTenants%></div>
                    </li>
                </ul>
                <% if (showBillStats)
                   { %>
                <ul class="drow no-border-bottom">
                    <li>
                        <span class="info-title"><%: this.GetLocalResourceObject("lbl_TotalBills") %></span>
                        <div class="number">
                            <a href="<%=Url.Action("ManageChildBills", "Billing", new { viewTenantId=item.TenantDetails.TenantCode })%>"><%:item.BillStatistics.TotalBills %></a>
                        </div>
                    </li>
                    <li>
                        <span class="info-title"><%: this.GetLocalResourceObject("lbl_Revenue") %> </span>
                        <div class="number" title="<%:item.BillStatistics.TotalAmount.ToBillingCurrency("C") %>">
                            <a href="<%=Url.Action("ManageChildBills", "Billing", new { viewTenantId=item.TenantDetails.TenantCode })%>"><%:item.BillStatistics.TotalAmount.ToBillingCurrency("C0") %></a>
                        </div>
                    </li>
                    <li title="<%: this.GetLocalResourceObject("lbl_TotalOverdude") %>">
                        <span class="info-title"><%: this.GetLocalResourceObject("lbl_Overdue") %></span>
                        <div title="<%:item.OverallOverdue.ToBillingCurrency("C") %>" class="number red">
                            <a href="<%=Url.Action("ManageChildBills", "Billing", new { viewTenantId=item.TenantDetails.TenantCode })%>"><%:item.OverallOverdue.ToBillingCurrency("C0") %></a>
                        </div>
                    </li>
                </ul>
                <% } %>
            </div>
        </div>
    </div>
    <% } %>
    <% }
       else
       { %>
    <div class="alert alert-info">
        <%: this.GetLocalResourceObject("e_NoTenantsSearch") %>
    </div>
    <% } %>
</div>
<% 
    if (lstTenants != null && lstTenants.Count() > 0)
    {
        var routeValues = new
        {
            SearchText = searchCondition.SearchText,
            Sort = searchCondition.Sort,
            CreatedYear = searchCondition.CreatedYear,
            IsNew = searchCondition.IsNew,
            OnlyActive = searchCondition.OnlyActive,
            OnlyTrial = searchCondition.OnlyTrial,
            OnlyDeactivated = searchCondition.OnlyDeactivated,
            OnlyPaying = searchCondition.OnlyPaying,
            OnlySelfRegistered = searchCondition.OnlySelfRegistered,
            HasAutoDebit = searchCondition.HasAutoDebit,
            HasOverdue = searchCondition.HasOverdue,
            HasOnlineUsers = searchCondition.HasOnlineUsers,
            HasUsageAlert = searchCondition.HasUsageAlert,
            HasChildTenants = searchCondition.HasChildTenants,
            WithoutPaymentAccount = searchCondition.WithoutPaymentAccount,
            TenantType = searchCondition.TenantType,
            PackageId = searchCondition.PackageId
        };

        Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divTenantListViewPartial", LoadingElementId = "loading", LoadingElementDuration = 500 },
            routeValues)
            .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
            .SetPageNumber(searchCondition.PageNo)
            .SetPageSize(searchCondition.PageSize)
            .SetTotalCount(totalCount)
            .Render();
    } 
%>
<script type="text/javascript">
    $(function () {
        $('#divTenantListViewPartial a[id^=btnViewUsages]').popover({
            placement: 'right',
            trigger: 'click',
            html: true,
            content: function () {
                return $('#divUsageLimitInfo' + $(this).data('tenantid')).html();
            }
        });
    });
</script>
