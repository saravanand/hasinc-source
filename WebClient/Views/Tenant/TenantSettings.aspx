<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("TenantSettings", "Tenant", FormMethod.Post, new { enctype = "multipart/form-data", id = "TenantSettingId" }))
       {
    %>
    <div class="center_container">
        <div class="heading_container">
            <h1>
                <%: this.GetLocalResourceObject("lbl_ManageTenantSettings") %>
            </h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a href="<%:Url.Content("~/Home/Index") %>" title="<%=this.GetGlobalResourceObject("General","Back") %>">
                        <span>
                            <%=this.GetGlobalResourceObject("General","Back") %></span></a>
                </div>                
            </div>
        </div>
        <div class="clear">
        </div>
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="error">
            <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <%} %>
        <%if (ViewData["Success"] != null)
          { %>
        <div class="success">
            <%=ViewData["Success"]%>
        </div>
        <%} %>
        <div class="form_list">
            <table class="celloTable details_container_table">
                <thead>
                    <tr>
                        <th>
                            <%: this.GetLocalResourceObject("lbl_Attribute") %>
                        </th>
                        <th>
                            <%: this.GetLocalResourceObject("lbl_Value")%>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 130px; text-align: right;">
                            <%: this.GetLocalResourceObject("Theme").ToString() %>
                        </td>
                        <td>
                            <%=Html.DropDownList("ViewMetaData.Theme", (IEnumerable<SelectListItem>)ViewData["Themes"])%>
                            <%=Html.CelloValidationMessage("ViewMetaData.Theme", "*")%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; text-align: right;">
                            <%: this.GetLocalResourceObject("Logo").ToString() %>
                        </td>
                        <td>
                            <input id="logoPath" name="logoPath" value="ViewMetaData.Logo" type="file" contenteditable="false" />
                            <%= Html.Hidden("SavedImagePath", ViewData["ImageURL"]) %>
                            <br />
                            <img src="<%=this.ResolveClientUrl("../../"+this.ViewData["ImageURL"].ToString())%>"
                                alt="Logo" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; text-align: right;">
                            <%: this.GetLocalResourceObject("DateFormat").ToString()%>
                        </td>
                        <td>
                            <%=Html.CelloTextBox("dateFormat", ViewData["dateFormat"])%>
                            <%=Html.CelloValidationMessage("dateFormat", "*")%>
                            <span id="dateFormatPreview" style="margin-left: 10px; color: #666; font-style: italic;">
                                <%: this.GetLocalResourceObject("lbl_Preview")%>:
                                <%=DateTime.Now.ToString(ViewData["dateFormat"].ToString()) %></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; text-align: right;">
                            <%: this.GetLocalResourceObject("Language")%>
                        </td>
                        <td>
                            <%=Html.DropDownList("language", (IEnumerable<SelectListItem>)ViewData["Languages"])%>
                            <%=Html.CelloValidationMessage("language", "*")%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; text-align: right;">
                            <%: this.GetLocalResourceObject("lbl_ProductAnalytics")%>
                        </td>
                        <td>
                            <%=Html.CheckBox("EnableProductAnalytics", bool.Parse(ViewData["EnableProductAnalytics"].ToString()), new { style="width:20px;" })%>
                            <%=Html.CelloValidationMessage("EnableProductAnalytics", "*")%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; text-align: right;">
                            <%: this.GetLocalResourceObject("ExpirationDays").ToString()%>
                        </td>
                        <td>
                            <%=Html.CelloTextBox("ExpiryDays", ViewData["ExpiryDays"])%>
                            <%=Html.CelloValidationMessage("ExpiryDays", "*")%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 130px; text-align: right;">
                            <%: this.GetLocalResourceObject("WCFSecretKey").ToString() %>
                        </td>
                        <td>
                            <%=Html.Password("WCFSecretKey", ViewData["WCFSecretKey"])%>
                            <%=Html.Hidden("HiddenWCFSecretKey", ViewData["HiddenWCFSecretKey"]) %>
                            <%=Html.CelloValidationMessage("WCFSecretKey", "*")%>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="inner_hold">
            <div class="green_but">
                <a href="#" title="<%=this.GetGlobalResourceObject("General","Save") %>" onclick="SubmitSettingsForm();">
                    <span>
                        <%=this.GetGlobalResourceObject("General","Save") %></span></a>
            </div>
        </div>
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#TenantSettingId").validate({
                rules: {
                    logoPath: {
                        accept: true
                    },
                    dateFormat: {
                        required: true
                    },
                    ExpiryDays:
                    { required: true },
                    WCFSecretKey:
                     { required: true }
                },
                messages: {
                    logoPath: {
                        accept: '<%: this.GetLocalResourceObject("e_ImageFormat")%>'
                    },
                    dateFormat: {
                        required:  '<%: this.GetLocalResourceObject("e_InvalidDateFormat")%>'
                    },
                    ExpiryDays:
                    { required: '<%: this.GetLocalResourceObject("e_InvalidExpiryValue")%>' },
                    WCFSecretKey:
                    { required: '<%: this.GetLocalResourceObject("e_InvalidSecretKey")%>'  }
                }
            });
        });

        function SubmitSettingsForm() {
            $('#TenantSettingId').submit();
        }
    </script>
    <style type="text/css">
        label.error
        {
            border: 0px;
            background: transparent;
            color: Red;
        }
    </style>
</asp:Content>
