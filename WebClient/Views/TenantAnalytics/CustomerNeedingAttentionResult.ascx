﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.ProductAnalytics" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    var CustomerList = ViewData["CustomerList"] as List<ProductAnalyticsTrial> ?? new List<ProductAnalyticsTrial>();

    if (CustomerList.Count() > 0)
    {
        var searchCondition = ViewData["SearchCondition"] as ProductAnalyticsTrialSearchCondition ?? new ProductAnalyticsTrialSearchCondition();
        int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
        int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;
        int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
        var visitFreq = ViewData["ApplicationFreq"] != null ? ViewData["ApplicationFreq"] as List<SelectListItem> : new List<SelectListItem>();

        Html.Grid<ProductAnalyticsTrial>(CustomerList).Columns(column =>
        {
            column.For(x => x.Name).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_Name").ToString()).HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.TrialFromDate.HasValue ? x.TrialFromDate.Value.ToShortDateString() : "").Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_TrialFromDate1").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center");
            column.For(x => x.LastActivity.HasValue ? x.LastActivity.Value.ToShortDateString() : "").Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_LastActivity").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.TotalHits).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_TotalHits").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.AverageHits.ToString("#.##")).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_AverageHits").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.UserCounts).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_UserCounts").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => (x.TrialDays < 0 ? 0 : x.TrialDays)).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_TrialDays1").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.UsedDays).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_UsedDays").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.visitFrequency).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_visitFrequency").ToString()).HeaderAttributes(@style => "text-align:center"); ;
        }).Attributes(id => "tblCustomerResult", @class => "celloTable").Render();

        if (CustomerList.Count() > 0)
        {
            var rval = new { SinceLastActivity = searchCondition.SinceLastActivity, VisitFrequently = searchCondition.VisitFrequently, SortString = searchCondition.SortString, SortDirection = searchCondition.SortDirection };
            Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "DivCustomerDetail", LoadingElementId = "loading", LoadingElementDuration = 500 }, rval)
                .SetTotalCount(totalCount).SetPageNumber(pageNumber).SetPageSize(pageSize)
                .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
                .Render();
        }
    }
    else
    {
%>
<div class="alert alert-info">
    <%:this.GetGlobalResourceObject("TenantAnalyticsResources","i_noData")%>
</div>
<% } 
%>

