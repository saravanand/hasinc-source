﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.ProductAnalytics" %>
<form class="form-inline" id="frmCustomerSearch" name="frmCustomerSearch" method="post" action="<%=Url.Action("CustomerNeedAttention") %>" role="form">
   <div class="content-box">
        <div class="form-group">
           <%=this.GetGlobalResourceObject("TenantAnalyticsResources","lbl_SinceLastActivity") %>
           <%= Html.TextBox("Customer.SinceLastActivity", "", new { @class = "form-control",maxlength = 50, style = "width:50px" })%>
        </div>
        <div class="form-group">
             <%=this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_visitFrequency").ToString() %>
            <%= Html.DropDownList("Customer.VisitFrequently", (IEnumerable<SelectListItem>)ViewData["ApplicationFreq"], new { @class = "form-control",style = "width:120px;" })%>
        </div>
       <div class="pull-right">
            <a class="btn btn-info" href="#" id="customerSearch" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%></a>
            <a class="btn btn-default" href="/TenantAnalytics/Index" id="A1" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                <%:this.GetGlobalResourceObject("General", "Reset")%></a>
        </div>
    </div>
</form>
