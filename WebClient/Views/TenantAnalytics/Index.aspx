﻿<%@ Page Title="<%$ Resources:t_TenantAnalytics %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("hotTrial")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel red">
            <header class="panel-heading">
                <h4>
                    <%:this.GetLocalResourceObject("hotTrial")%>
                </h4>
            </header>
            <div class="panel-body">
                <% Html.RenderAction("HotTrialSearch"); %>
                <div id="DivHotTrialDetail">
                    <% Html.RenderAction("HotTrial"); %>
                </div>
            </div>
        </section>
        <section class="panel indigo">
            <header class="panel-heading">
                <h4>
                    <%:this.GetLocalResourceObject("trialsNeedAttention")%>
                </h4>
            </header>
            <div class="panel-body">
                <% Html.RenderAction("TrialsNeedingAttentionSearch"); %>
                <div id="DivTrialAttentionDetail">
                    <% Html.RenderAction("TrialNeedAttention"); %>
                </div>
            </div>
        </section>
        <section class="panel blue">
            <header class="panel-heading">
                <h4>
                    <%:this.GetLocalResourceObject("customerNeedAttention")%>
                </h4>
            </header>
            <div class="panel-body">
                <% Html.RenderAction("CustomerNeedingAttentionSearch"); %>
                <div id="DivCustomerDetail">
                    <% Html.RenderAction("CustomerNeedAttention"); %>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#HotTrial_TrialFromDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#TrialNeedAtt_TrialFromDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#HotTrial_TrialToDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#TrialNeedAtt_TrialToDate').datepicker({
                format: "<%=CelloSaaS.Library.Helpers.DateTimeHelper.JQueryDateFormat() %>"
            });

            $('#SinceLastActivity').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });


            $("#trialSearch").click(function () {
                var fromDate = new Date($('#HotTrial_TrialFromDate').val());
                var toDate = new Date($('#HotTrial_TrialToDate').val());
                if (fromDate && toDate) {
                    if (fromDate > toDate) {
                        alert(' <%:this.GetLocalResourceObject("e_DateAlert")%>');
                        return false;
                    }
                }

                var postdata = $('#frmTrialSearch').serialize();
                postdata += '&pageSize=' + $('#DivHotTrialDetail').find('select[name=pageSize]').val()
                $.post('/TenantAnalytics/HotTrial', postdata, function (data) {
                    $('#DivHotTrialDetail').hide().html(data).fadeIn();
                });
                return false;
            });

            $("#trialNeedSearch").click(function () {
                var fromDate = new Date($('#TrialNeedAtt_TrialFromDate').val());
                var toDate = new Date($('#TrialNeedAtt_TrialToDate').val());
                if (fromDate && toDate) {
                    if (fromDate > toDate) {
                        alert(' <%:this.GetLocalResourceObject("e_DateAlert")%>');
                        return false;
                    }
                }

                var postdata = $('#frmTrialAttentionSearch').serialize();
                postdata += '&pageSize=' + $('#DivTrialAttentionDetail').find('select[name=pageSize]').val()
                $.post('/TenantAnalytics/TrialNeedAttention', postdata, function (data) {
                    $('#DivTrialAttentionDetail').hide().html(data).fadeIn();
                });
                return false;

            });

            $("#customerSearch").click(function () {
                var postdata = $('#frmCustomerSearch').serialize();
                postdata += '&pageSize=' + $('#DivCustomerDetail').find('select[name=pageSize]').val()
                $.post('/TenantAnalytics/CustomerNeedAttention', postdata, function (data) {
                    $('#DivCustomerDetail').hide().html(data).fadeIn();
                });
                return false;
            });

            $(document).on('change','select[name=pageSize]', function () {
                if ($(this).parent().parent().parent().parent().attr('id') == 'DivHotTrialDetail') {
                    var postdata = $('#frmTrialSearch').serialize();
                    postdata += '&pageSize=' + $(this).val();

                    $.post('/TenantAnalytics/HotTrial', postdata, function (data) {
                        $('#DivHotTrialDetail').hide().html(data).fadeIn();
                    });
                }
                else if ($(this).parent().parent().parent().parent().attr('id') == 'DivTrialAttentionDetail') {
                    var postdata = $('#frmTrialAttentionSearch').serialize();
                    postdata += '&pageSize=' + $(this).val();

                    $.post('/TenantAnalytics/TrialNeedAttention', postdata, function (data) {
                        $('#DivTrialAttentionDetail').hide().html(data).fadeIn();
                    });
                }
                else if ($(this).parent().parent().parent().parent().attr('id') == 'DivCustomerDetail') {
                    var postdata = $('#frmCustomerSearch').serialize();
                    postdata += '&pageSize=' + $(this).val();

                    $.post('/TenantAnalytics/CustomerNeedAttention', postdata, function (data) {
                        $('#DivCustomerDetail').hide().html(data).fadeIn();
                    });
                }
            });
        });
    </script>
</asp:Content>

