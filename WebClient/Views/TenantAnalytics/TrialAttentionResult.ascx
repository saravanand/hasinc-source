﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.ProductAnalytics" %>
<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    var TrialList = ViewData["TrialList"] as List<ProductAnalyticsTrial> ?? new List<ProductAnalyticsTrial>();

    if (TrialList.Count() > 0)
    {
        var searchCondition = ViewData["SearchCondition"] as ProductAnalyticsTrialSearchCondition ?? new ProductAnalyticsTrialSearchCondition();
        int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
        int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;
        int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
        var visitFreq = ViewData["ApplicationFreq"] != null ? ViewData["ApplicationFreq"] as List<SelectListItem> : new List<SelectListItem>();

        var ajaxOpt = new AjaxOptions { UpdateTargetId = "DivTrialAttentionDetail", LoadingElementId = "loading", LoadingElementDuration = 300 };

        var routeValues = new
        {
            TrialFromDate = searchCondition.TrialFromDate,
            TrialToDate = searchCondition.TrialToDate,
            SinceLastActivity = searchCondition.SinceLastActivity,
            VisitFrequently = searchCondition.VisitFrequently,
            SortString = searchCondition.SortString,
            SortDirection = searchCondition.SortDirection
        };

        Html.Grid<ProductAnalyticsTrial>(TrialList).Columns(column =>
        {
            column.For(x => x.Name).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_Name").ToString()).HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.TrialFromDate.HasValue ? x.TrialFromDate.Value.ToShortDateString() : "").Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_TrialFromDate").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center");
            column.For(x => x.TrialToDate.HasValue ? ((x.TrialToDate.Value == Convert.ToDateTime("1/1/1900")) ? "-" : x.TrialToDate.Value.ToShortDateString()) : "").Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_TrialToDate").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.LastActivity.HasValue ? x.LastActivity.Value.ToShortDateString() : "").Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_LastActivity").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.TotalHits).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_TotalHits").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.AverageHits.ToString("#.##")).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_AverageHits").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.UserCounts).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_UserCounts").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => (x.TrialDays < 0 ? 0 : x.TrialDays)).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_TrialDays").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.UsedDays).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_UsedDays").ToString()).Attributes(@style => "text-align:right").HeaderAttributes(@style => "text-align:center"); ;
            column.For(x => x.visitFrequency).Named(this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_visitFrequency").ToString()).HeaderAttributes(@style => "text-align:center"); ;
        }).Attributes(id => "tblHotTrialSearch", @class => "celloTable").Render();

        Ajax.CelloPager(ajaxOpt, routeValues).SetTotalCount(totalCount).SetPageNumber(pageNumber).SetPageSize(pageSize)
         .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
         .Render();
    }
    else
    {
%>
<div class="alert alert-info">
   <%:this.GetGlobalResourceObject("TenantAnalyticsResources","i_noData")%>
</div>
<% } 
%>

