﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.ProductAnalytics" %>
<form class="form-inline" id="frmTrialAttentionSearch" name="frmTrialAttentionSearch" method="post" action="<%=Url.Action("TrialNeedAttention") %>" role="form">
    <div class="content-box">
        <div class="form-group">
            <%=this.GetGlobalResourceObject("TenantAnalyticsResources","lbl_HTrialFromDate") %>
            <%=Html.TextBox("TrialNeedAtt.TrialFromDate", DateTime.Today.AddMonths(-2).ToString(CelloSaaS.Library.Helpers.DateTimeHelper.GetDateFormat()), new { @class = "form-control datetime", style="width:100px" })%>
        </div>
        <div class="form-group">
            <%=this.GetGlobalResourceObject("TenantAnalyticsResources","lbl_HTrialToDate") %>
            <%=Html.TextBox("TrialNeedAtt.TrialToDate", "", new { @class = "form-control datetime", style="width:100px" })%>
        </div>
        <div class="form-group">
            <%=this.GetGlobalResourceObject("TenantAnalyticsResources","lbl_HasSinceLastActivity") %>
            <%= Html.TextBox("TrialNeedAtt.SinceLastActivity", "", new { @class = "form-control", maxlength = 50, style = "width:50px" })%>
        </div>
        <div class="form-group">
            <%=this.GetGlobalResourceObject("TenantAnalyticsResources", "lbl_visitFrequency").ToString() %>
            <%= Html.DropDownList("TrialNeedAtt.VisitFrequently", (IEnumerable<SelectListItem>)ViewData["ApplicationFreq"], new { @class = "form-control", style = "width:90px;" })%>
        </div>
        <div class="pull-right">
            <a class="btn btn-info" href="#" id="trialNeedSearch" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                <i class="fa fa-search"></i>&nbsp;<%:this.GetGlobalResourceObject("General", "Search")%></a>
            <a class="btn btn-default" href="/TenantAnalytics/Index" id="A1" title="<%:this.GetGlobalResourceObject("General","Reset") %>">
                <%:this.GetGlobalResourceObject("General", "Reset")%></a>
        </div>
    </div>
</form>
