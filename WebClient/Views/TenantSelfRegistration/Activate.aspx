﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SelfRegisterSite.Master"
     Inherits="System.Web.Mvc.ViewPage"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12 col-np">
        <div class="register-box-plain">
            <% if (ViewData["ErrorMessage"] != null && !string.IsNullOrEmpty(ViewData["ErrorMessage"].ToString()))
               {%>
            <div class="alert alert-danger">
                <%= ViewData["ErrorMessage"].ToString() %>
            </div>
            <%}
               else if (ViewData["SuccessMessage"] != null && !string.IsNullOrEmpty(ViewData["SuccessMessage"].ToString()))
               {%>
            <section class="panel green no-border">
                <header class="panel-heading">
                    <h4><%:this.GetLocalResourceObject("h_ActivationSuccess")%> </h4>
                </header>
                <div class="panel-body">
                    <span class="text-success">
                        <%=ViewData["SuccessMessage"].ToString()%>
                    </span>
                    <br />
                    <div class="text-center">
                        <a class="btn btn-primary btn-cons" href="/"><%:this.GetGlobalResourceObject("General", "Continue")%></a>
                    </div>
                </div>
            </section>
            <%} %>
        </div>
    </div>
</asp:Content>
