﻿<%@ Page Title="<%$ Resources:t_RegisterSuccess %>" Language="C#" MasterPageFile="~/Views/Shared/SelfRegisterSite.Master"
     Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12 col-np">
        <div class="register-box-plain">
            <section class="panel green no-border">
                <header class="panel-heading">
                    <h4><i class="fa fa-thumbs-up"></i>&nbsp;<%:this.GetLocalResourceObject("m_registering")%></h4>
                </header>
                <div class="panel-body">
                    <p style="font-size: 16px;"><%:this.GetLocalResourceObject("m_CheckMail")%></p>
                    <br />
                    <br />
                    <div class="text-center">
                        <a href="/" class="btn btn-success"><i class="fa fa-arrow-circle-left"></i>&nbsp;<%:this.GetLocalResourceObject("lbl_Back")%></a>
                    </div>
                </div>
            </section>
        </div>
    </div>
</asp:Content>
