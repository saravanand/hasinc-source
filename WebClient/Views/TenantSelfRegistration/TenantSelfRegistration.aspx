﻿<%@ Page Title="<%$ Resources:Register %>" Language="C#" MasterPageFile="~/Views/Shared/SelfRegisterSite.Master"
    Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="CelloSaaS.Billing.Model" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.LicenseManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <% if (!string.IsNullOrEmpty(ViewBag.PackageError))
                     { %>
    <div style="margin-top:15%;">
    <div align="center" class="row" ><div class="alert alert-danger">
                         <%=ViewBag.PackageError%>
                         </div></div>
   <div style="margin-left:auto;margin-right:auto;width:100px">
        <a href="/Home"  class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> Back to Login</a>
 </div></div>
                    <%}else{%>
    <div class="col-md-12 col-np">
        <div class="register-box-plain">
            <%
                var tenant = ViewData["Tenant"] as Tenant;
                var availablePackages = ViewBag.AvailablePackages as List<LicensePackage>;
                var packagePricePlans = ViewBag.PackagePricePlans as Dictionary<string, List<PricePlan>> ?? new Dictionary<string, List<PricePlan>>();
            %>
            <form id="tenantDetailsFrom" action="<%=Url.Action("TenantSelfRegistration") %>"
                enctype="multipart/form-data" method="post" role="form">
               
                       
                 
                    
                    
                <div class="row">
                    <% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
                       { %>
                    <div class="alert alert-danger">
                        <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
                        
                    </div>
                    <% } %>
                    <div class="col-md-7">
                        <div class="grid simple horizontal red">
                            <div class="grid-title">
                                <h4><%: this.GetLocalResourceObject("PackageSettings")%></h4>
                            </div>
                            <div class="grid-body">
                                <div class="form-container">
                                    <div class="row">
                                        <div class="form-group">
                                            <%: Html.Label(this.GetLocalResourceObject("lbl_BillingCycle").ToString())%>
                                            <%
                                                string temp = this.Request.Form["billingFrequency"] ?? BillFrequency.Monthly.ToString();
                                                var currentFreq = (BillFrequency)Enum.Parse(typeof(BillFrequency), temp);
                                            %>
                                            <select id="billingFrequency" name="billingFrequency" style="width: 100%;">
                                                <option <%=currentFreq == BillFrequency.Monthly ? "selected=\"selected\"" : "" %>><%: BillFrequency.Monthly %></option>
                                                <option <%=currentFreq == BillFrequency.Quarterly ? "selected=\"selected\"" : "" %>><%: BillFrequency.Quarterly %></option>
                                                <option <%=currentFreq == BillFrequency.Semiannual ? "selected=\"selected\"" : "" %>><%: BillFrequency.Semiannual %></option>
                                                <option <%=currentFreq == BillFrequency.Annual ? "selected=\"selected\"" : "" %>><%: BillFrequency.Annual %></option>
                                            </select>
                                        </div>
                                        <%=Html.Hidden("PackageId") %>
                                        <%=Html.Hidden("PricePlanId") %>
                                    </div>
                                    <div class="row">
                                        <% if (availablePackages != null && availablePackages.Count > 0)
                                           {
                                               var defaultPlans = CelloSaaS.ServiceProxies.Configuration.PickupListProxy.GetAllPickupListValues(CelloSaaSApplication.Models.Constants.DefaultPricePlanPickupListId, UserIdentity.TenantID) ?? new List<CelloSaaS.Model.Configuration.PickupListValue>();
                                               var colors = new string[] { "purple", "green", "orange", "indigo", "lt-green" };
                                               int cnt = 0;
                                               string selectedPricePlanId = ViewData["PricePlanId"] != null ? ViewData["PricePlanId"].ToString() : string.Empty;
                                               Dictionary<string, Dictionary<string, List<PricePlan>>> pricePlanDetails = new Dictionary<string, Dictionary<string, List<PricePlan>>>();
                                               Dictionary<string, List<PricePlan>> pricePlans = new Dictionary<string, List<PricePlan>>();
                                               foreach (var item in availablePackages.OrderBy(x => x.Name))
                                               {
                                                   var plans = packagePricePlans.ContainsKey(item.LicensepackageId) ? packagePricePlans[item.LicensepackageId] : null;
                                                   if (plans == null || plans.Count == 0) return;

                                                   foreach (var plan in plans.OrderBy(x => x.CreatedOn))
                                                   {
                                                       if (!pricePlanDetails.ContainsKey(plan.BillFrequency.ToString()))
                                                       {
                                                           pricePlans = new Dictionary<string, List<PricePlan>>();
                                                           pricePlans.Add(item.LicensepackageId, new List<PricePlan> { plan });
                                                           pricePlanDetails.Add(plan.BillFrequency.ToString(), pricePlans);
                                                       }
                                                       else
                                                       {
                                                           if (!pricePlanDetails[plan.BillFrequency.ToString()].ContainsKey(item.LicensepackageId))
                                                           {
                                                               pricePlanDetails[plan.BillFrequency.ToString()].Add(item.LicensepackageId, new List<PricePlan> { plan });
                                                           }
                                                           else
                                                           {
                                                               pricePlanDetails[plan.BillFrequency.ToString()][item.LicensepackageId].Add(plan);
                                                           }
                                                       }
                                                   }
                                               }

                                               foreach (var currentPricePlanDetails in pricePlanDetails.Values)
                                               {
                                                   cnt = 0;
                                                   if (currentPricePlanDetails == null || currentPricePlanDetails.Count == 0)
                                                       continue;

                                                   foreach (var val in currentPricePlanDetails)
                                                   {
                                                       var plan = val.Value.FirstOrDefault();
                                                       var item = availablePackages.Where(x => x.LicensepackageId == val.Key).FirstOrDefault();
                                                       if (cnt >= colors.Length) cnt = 0;
                                                       string color = colors[cnt];
                                                       if (!string.IsNullOrEmpty(selectedPricePlanId) && selectedPricePlanId.Equals(plan.Id.ToString(), StringComparison.OrdinalIgnoreCase))
                                                       {
                                                           color += " selected";
                                                       }
                                                       cnt++;
                                        %>
                                        <div class="col-md-4" style="padding: 15px;">
                                            <div class="plan tiles <%: color %> <%:plan.BillFrequency.ToString() %>" id="<%: item.LicensepackageId %>" data-planid="<%=plan.Id %>" onclick="showAsSelected('<%: item.LicensepackageId %>', '<%:plan.Id %>')">
                                                <div class="tiles-body">
                                                    <div class="tiles-title">
                                                        <%= item.Name %> - <%:plan.BillFrequency.ToString() %>
                                                    </div>
                                                    <div class="heading planPrice" data-id="<%=plan.Id %>">
                                                        <% if (plan.Price > 0)
                                                           { %>
                                                        <span class="animate-number" data-animation-duration="1200" data-value="<%:plan.Price %>">
                                                            <%
                                                               if (plan.Price % 1 == 0) // no decimals
                                                               {
                                                            %>
                                                            <%:plan.Price.ToBillingCurrency("C0") %>
                                                            <%
                                                               }
                                                               else
                                                               {
                                                            %>
                                                            <%:plan.Price.ToBillingCurrency("C2") %><%
                                                                   
                                                               }
                                                            %>
                                                        </span>
                                                        <% } %>
                                                    </div>
                                                    <div class="description">
                                                        <%= HttpUtility.HtmlDecode(string.IsNullOrWhiteSpace(plan.Description) ? item.Description : plan.Description) %>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%                                          
                                                       if (cnt == 0 || cnt == 3)
                                                       {%>
                                    </div>
                                    <div class="row">
                                        <%}
                                                   }
                                               }
                                           }
                                        %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="grid simple horizontal purple">
                            <div class="grid-title">
                                <h4><%:this.GetLocalResourceObject("h_SignUp")%>
                                </h4>
                            </div>
                            <div class="grid-body">
                                <div class="form-container">
                                    <div class="form-group <%=Html.ValidationMessage("Tenant.TenantDetails.TenantCodeString","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_TenantDetails_TenantCodeString">
                                            <%:this.GetLocalResourceObject("lbl_CompanyCodeString")%>
                                        </label>
                                        <i class="fa fa-cloud"></i>
                                        <%: Html.TextBox("Tenant.TenantDetails.TenantCodeString", tenant.TenantDetails.TenantCodeString, new { placeholder="abc" })%>
                                        <span class="help-block">
                                            <span class="text-info fa fa-info-circle"></span>
                                            <%:this.GetLocalResourceObject("m_helpTenantCode")%>
                                        </span>
                                    </div>
                                    <div class="form-group <%=Html.ValidationMessage("Tenant.TenantDetails.TenantName","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_TenantDetails_TenantName"><%:this.GetLocalResourceObject("lbl_CompanyName")%></label>
                                        <i class="fa fa-heart"></i>
                                        <%: Html.TextBox("Tenant.TenantDetails.TenantName", tenant.TenantDetails.TenantName, new { placeholder="ABC Company" })%>
                                    </div>
                                    <%-- <div class="form-group <%=Html.ValidationMessage("Tenant.TenantDetails.CompanySize","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_TenantDetails_CompanySize"> <%:this.GetLocalResourceObject("lbl_CompanySize")%></label>
                                        <%: Html.DropDownList("Tenant.TenantDetails.CompanySize", (IEnumerable<SelectListItem>)ViewData["CompanySize"], new { style="width:100%;"  })%>
                                    </div>--%>
                                    <div class="form-group <%=Html.ValidationMessage("Tenant.ContactDetail.FirstName","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_ContactDetail_FirstName"><%:this.GetLocalResourceObject("lbl_FirstName")%></label>
                                        <i class="fa fa-font"></i>
                                        <%: Html.TextBox("Tenant.ContactDetail.FirstName", tenant.ContactDetail.FirstName, new { placeholder="John" })%>
                                    </div>
                                    <div class="form-group <%=Html.ValidationMessage("Tenant.ContactDetail.LastName","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_ContactDetail_LastName"><%:this.GetLocalResourceObject("lbl_LastName")%></label>
                                        <i class="fa fa-font"></i>
                                        <%: Html.TextBox("Tenant.ContactDetail.LastName", tenant.ContactDetail.LastName, new { placeholder="Smith" })%>
                                    </div>
                                    <div class="form-group <%=Html.ValidationMessage("Tenant.ContactDetail.Email","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_ContactDetail_Email"><%:this.GetLocalResourceObject("lbl_Email")%></label>
                                        <i class="fa fa-envelope"></i>
                                        <%: Html.TextBox("Tenant.ContactDetail.Email", tenant.ContactDetail.Email, new { placeholder="john.smith@example.com" })%>
                                        <span class="help-block">
                                            <span class="text-info fa fa-info-circle"></span>
                                            <%:this.GetLocalResourceObject("m_Activationdetails")%>
                                        </span>
                                    </div>
                                    <% if (CelloSaaS.Library.Helpers.ConfigHelper.EnableUserNameInTenantRegistration)
                                       { %>
                                    <div class="form-group <%=Html.ValidationMessage("Tenant.TenantAdminMembershipdetail.UserName","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_TenantAdminMembershipdetail_UserName"><%:this.GetLocalResourceObject("lbl_userName")%></label>
                                        <i class="fa fa-font"></i>
                                        <%: Html.TextBox("Tenant.TenantAdminMembershipdetail.UserName", tenant.ContactDetail.Email, new { placeholder="john.smith" })%>
                                        <%-- <span class="help-block">
                                            <span class="text-info fa fa-info-circle"></span>
                                            <%:this.GetLocalResourceObject("m_Activationdetails")%>
                                        </span>--%>
                                    </div>
                                    <% } %>
                                    <div class="form-group <%=Html.ValidationMessage("Tenant.ContactDetail.Phone","*") !=null ? "has-error" : "" %>">
                                        <label for="Tenant_ContactDetail_Phone"><%:this.GetLocalResourceObject("lbl_Phone")%></label>
                                        <i class="fa fa-phone"></i>
                                        <%: Html.TextBox("Tenant.ContactDetail.Phone", tenant.ContactDetail.Phone, new { placeholder="+1-234-567-8901" })%>
                                        <span class="help-block"><%:this.GetLocalResourceObject("m_egphoneNo")%></span>
                                    </div>
                                    <div class='form-group'>
                                        <% if (ViewBag.TenantIdProvider != null)
                                           {
                                        %>
                                        <label for="TenantIdProvider">Identity Provider</label><br />
                                        <select name="TenantIdProvider" id="TenantIdProvider"></select>
                                        <%
                                           }
                                        %>
                                    </div>
                                    <div class='form-group'>
                                        <%
                                            if (ViewBag.UserAuthenticationType != null)
                                            {
                                        %>
                                        <label for="UserAuthenticationType">Authentication Types</label>
                                        <br />
                                        <select name="UserAuthenticationType" id="UserAuthenticationType"></select>
                                        <%
                                            }
                                        %>
                                    </div>
                                    <div class="ldapconfig" style="display: none">
                                        <% Html.RenderPartial("LDAPEndpointConfig", true); %>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success btn-block" onclick="SubmitTenantDetailsForm();">
                                            <%: this.GetLocalResourceObject("Register")%></button>
                                    </div>
                                    <div class="line line-dashed"></div>
                                    <p class="text-muted text-center">
                                        <small><%:this.GetLocalResourceObject("m_Alreadyhaveanaccount")%></small>
                                    </p>
                                    <a class="btn btn-white btn-block" href="/"><%:this.GetLocalResourceObject("lbl_Signin")%></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/knockout-3.1.0.js"></script>
    <script type="text/javascript">

        var authProviders = JSON.parse('<%=ViewBag.Providers ?? "{}"%>');
        var authTypes = JSON.parse('<%=ViewBag.AuthTypes ?? "{}"%>');

        $(function () {

            var ddlIdProviders = $("#TenantIdProvider");
            var ddlAuthTypes = $("#UserAuthenticationType");

            for (var i = 0; i < authProviders.length; i++) {
                $(ddlIdProviders).append($('<option></option>').val(authProviders[i].ProviderId).text(authProviders[i].Name));
            }

            updateAuthTypes($(ddlIdProviders).val());

            $(ddlIdProviders).change(function () {
                var value = $(this).val();
                $(ddlAuthTypes).val('');
                $(ddlAuthTypes).html('');
                updateAuthTypes(value);
            });

            function updateAuthTypes(currentProvider) {
                $(ddlAuthTypes).html('');

                for (var i = 0; i < authTypes.length; i++) {
                    if (authTypes[i].ProviderId == currentProvider) {
                        $(ddlAuthTypes).append($('<option></option>').val(authTypes[i].AuthTypeId).text(authTypes[i].AuthTypeName));
                    }
                }

                if ($(ddlIdProviders).val() == "221480d5-4ae4-4c9d-9c1d-025716ff33b0") {
                    $('.ldapconfig').show();
                }
                else {
                    $('.ldapconfig').hide();
                }
                $(ddlAuthTypes).select2({ width: "100%" });
            }
            $(ddlIdProviders).val('');
            $(ddlAuthTypes).val('');
            $(ddlAuthTypes).select2({ width: "100%" });
        });

        function showAsSelected(selectedItemId, planId) {
            $('.plan.selected').removeClass("selected");
            $('.plan').attr("title", "<%:this.GetLocalResourceObject("t_ChoosePlan")%>");
            $("#PackageId").val(selectedItemId);
            $('#PricePlanId').val(planId);
            var el = $('.plan[data-planid="' + planId + '"]');
            el.addClass("selected");
            el.attr("title", "<%:this.GetLocalResourceObject("t_PlanSelected")%>");
        }

        function SubmitTenantDetailsForm() {
            $('#tenantDetailsFrom').submit();
        }

        $(function () {
            $('select').select2({ width: "100%" });
            var planid = $("#PricePlanId").val();
            if (planid) {
                $('.plan.selected').removeClass('selected');
                var el = $('.plan[data-planid="' + planid + '"]');
                el.addClass("selected");
                el.attr("title", "<%:this.GetLocalResourceObject("t_PlanSelected")%>");
            }

            $('select#billingFrequency').change(function () {
                var val = $(this).val();
                $('.plan.selected').removeClass('selected');
                $("#PackageId").val('');
                $('#PricePlanId').val('');
                $('.plan').parent().hide();
                $('.plan.' + val).parent().show();
                return true;
            });

            var ferquencyVal = $('select#billingFrequency').val();
            $('.plan').parent().hide();
            $('.plan.' + ferquencyVal).parent().show();
            //$('select#billingFrequency').trigger('change');
        });
    </script>
</asp:Content>
