﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.UserManagement.LinkedTenantUsers>" %>
<%  if (ViewData["sharedUsers"] != null && bool.Parse(ViewData["sharedUsers"].ToString()) && ViewData["LinkedTenants"] != null && ((List<CelloSaaS.Model.UserManagement.LinkedTenantUsers>)ViewData["LinkedTenants"]).Count() > 0)
    { %>
<div class="grid-title">
    <div class="row">
        <div class="col-sm-5 m-b-xs">
            <div id="user_filter" class="dataTables_filter">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="tenantTableSearchText" name="tenantTableSearchText"
                        placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" onclick="DoUserSearch();" type="button"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<%     
        Html.Grid((IEnumerable<CelloSaaS.Model.UserManagement.LinkedTenantUsers>)ViewData["LinkedTenants"]).Columns(
               column =>
               {
                   column.For(col => col.TenantDetails.TenantCodeString).Named(this.GetGlobalResourceObject("General", "Tenant") + " " + this.GetLocalResourceObject("lbl_Code").ToString());
                   column.For(col => col.TenantDetails.TenantName).Named(this.GetGlobalResourceObject("General", "Tenant") + " " + this.GetGlobalResourceObject("General", "Name"));
                   column.For(col => !string.IsNullOrEmpty(col.TenantDetails.Description) && col.TenantDetails.Description.Length > 25 ? Regex.Match(col.TenantDetails.Description, @"(\w+[\s.]*){1,10}").Value + "..." : col.TenantDetails.Description).Named(this.GetGlobalResourceObject("General", "Tenant") + " " + this.GetGlobalResourceObject("General", "Description"));
                   column.For(col => "<a style='color:#0000FF; text-decoration:underline' href='" + col.TenantDetails.Website + "'>" + col.TenantDetails.Website + "</a>").Named(this.GetGlobalResourceObject("General", "Tenant") + " " + this.GetGlobalResourceObject("General", "Website")).DoNotEncode();
                   column.For(col => !string.IsNullOrEmpty(col.Comments) && col.Comments.Length > 25 ? Regex.Match(col.Comments, @"(\w+[\s.]*){1,10}").Value + ".." : col.Comments).Named(this.GetLocalResourceObject("lbl_Comments").ToString());

                   column.For(col => col.RequestStatus == CelloSaaS.Model.UserManagement.RequestStatus.WaitingForTenantApproval
                       ? this.GetLocalResourceObject("lbl_WaitingforApproval").ToString()
                       : col.RequestStatus.ToString()).Named(this.GetLocalResourceObject("lbl_RequestStatus").ToString()).DoNotEncode();

                   column.For(col => col.RequestStatus != CelloSaaS.Model.UserManagement.RequestStatus.Open
                       && col.RequestStatus != CelloSaaS.Model.UserManagement.RequestStatus.Rejected
                       ? "-"
                       : "<a id='Manage' href='#' title='Add \"" + col.TenantDetails.TenantName + "\" Details' onclick=addTenant('" + col.TenantId + "','" + col.Id + "','" + col.RequestStatus.ToString() + "') > <i class='fa fa-wrench'></i></a>")
                       .Named(this.GetGlobalResourceObject("General", "Action").ToString()).DoNotEncode();
               }).Attributes(id => "dataListLinkedByOtherTenants", @class => "celloTable").Render();                                                         
%>
<%}
    else if (ViewData["sharedUsers"] != null && bool.Parse(ViewData["sharedUsers"].ToString()))
    {%>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("e_noTenant") %>
</div>
<%}%>
