﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container" id="container">
        <div class="page-title">
            <h3>
                <%: this.GetLocalResourceObject("lbl_RequestTenants") %>
            </h3>
        </div>
        <div class="row-fluid pd-25">
            <div class="text-info">
                <i class="fa fa-info-circle"></i>&nbsp;<%: this.GetLocalResourceObject("info_request") %>
            </div>
            <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantSuccessMessage")))
              { %>
            <div class="alert alert-success">
                <%=Html.CelloValidationMessage("TenantSuccessMessage")%>
            </div>
            <%} %>
            <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantErrorMessage")))
               { %>
            <div class="alert alert-danger">
                <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
            </div>
            <%} %>
            <div id="LinkedByOtherTenant" class="simple grid horizontal">
                <div class="grid-body">
                    <%Html.RenderPartial("LinkedbyOtherTenants"); %>
                </div>
            </div>
            <div class="page-title pd-0">
                <h3>
                    <%: this.GetLocalResourceObject("lbl_ApproveTenants") %>
                </h3>
            </div>
            <div class="text-info">
                <i class="fa fa-info-circle"></i>&nbsp;<%: this.GetLocalResourceObject("info_approve") %>
            </div>
            <div id="RequestedByTeanants" class="simple grid horizontal">
                <div class="grid-body">
                    <%Html.RenderPartial("RequestByTenant"); %>
                </div>
            </div>
            <div id="RequestPopup" title="<%: this.GetLocalResourceObject("msg_AddRequest") %>"
                class="addRequestPopup" style="display: none;">
                <table class="celloTable details_container_table">
                    <thead>
                        <tr>
                            <th>
                                <%: Html.Label(this.GetLocalResourceObject("lbl_Comments").ToString()) %>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <%=Html.Hidden("TenantId", ViewData["TenantId"])%>
                                <%=Html.Hidden("linkedId")%>
                                <%=Html.Hidden("status")%>
                                <%= Html.TextArea("Comments", new { style = "width:350px;", @placeHolder="Provide Comments to the Approver" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="green_but">
                                    <a href="#" title="<%=this.GetLocalResourceObject("lbl_SendRequest")%>" onclick="sendRequest();">
                                        <span>
                                            <%=this.GetLocalResourceObject("lbl_SendRequest")%>
                                        </span></a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style='display: none'>
                    <i class='fa fa-times-circle'></i>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var filterTable = null;
        var filterTable1 = null;
        $(function () {
            jQDataTable();
            jQDataTableTenant();
            $('#searchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
            $('#searchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoUserSearch();
                    e.preventDefault();
                }
            });
        });

        function addPopup() {
            $(".addRequestPopup").dialog({
                modal: true,
                height: 180,
                width: 400
            });
        }

        function jQDataTableTenant() {
            filterTable = $('table#dataListRequestByTenant').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "aaSorting": [[2, "desc"]],
                "bInfo": true,
                "bAutoWidth": false
            });
        }

        function jQDataTable() {
            filterTable1 = $('table#dataListLinkedByOtherTenants').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true,
                "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": [-1] }]
            });
        }

        function DoSearch() {
            var searchText = $('#tenantTableUserSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }

        function DoUserSearch() {
            var searchText = $('#tenantTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable1.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }

        var addRequestUrl = '<%: Url.Action("RequestTenant","TenantUserAssociation") %>';
        var respondRequestUrl = '<%: Url.Action("TenantUserAssociation","ApproveTenantRequest") %>';
        var status = "Ignore";
        function addRequest(tenantId) {
            $.get(addRequestUrl + "?tenantId=" + tenantId, loadLinkedTenantDetails);
        }
        function loadLinkedTenantDetails(data) {
            $("#LinkedByOtherTenant").html(data);
            jQDataTable();
        }

        function loadApprove(data) {
            $("#RequestedByTeanat").html(data);
            jQDataTable();
        }

        function addTenant(tenantId, Id, status) {
            $("input[id=TenantId]").val(tenantId)
            $("input[id=linkedId]").val(Id)
            $("input[id=status]").val(status)
            addPopup();
            $(".addRequestPopup").dialog("open");
            return false;
        }

        function sendRequest() {
            var comments = $("#Comments").val();
            comments = $.trim(comments.toString());
            var tenantId = $("#TenantId").val();
            var linkedId = $("#linkedId").val();
            var status = $("#status").val();
            if (comments == null || comments.toString() == "") {
                alert("Comment is Mandatory");
                return false;
            }
            else {
                $.ajax({
                    url: '<%: Url.Action("RequestTenant","TenantUserAssociation") %>',
                    data: { 'comments': comments, 'tenantId': tenantId, 'linkedId': linkedId, 'status': status },
                    type: "POST",
                    success: function (data) {
                        window.location.reload();
                    }
                });

            }
        }

        function approveRequest(linkedTenantUsersId, userId) {
            var status = "Approved";
            $.ajax({
                url: '<%: Url.Action("RespondRequest","TenantUserAssociation") %>',
                data: {
                    'linkedTenantUsersId': linkedTenantUsersId,
                    'status': status,
                    'userId': userId
                },
                type: "POST",
                success: function (data) {
                    loadLinkedTenantDetails(data);
                }
            });
        }

        function rejectRequest(linkedTenantUsersId, userId) {
            if (confirm('<%: this.GetLocalResourceObject("m_rejectRequest") %>')) {
                var status = "Rejected";
                $.ajax({
                    url: '<%: Url.Action("RespondRequest","TenantUserAssociation") %>',
                    data:
                    {
                        'linkedTenantUsersId': linkedTenantUsersId,
                        'status': status,
                        'userId': userId
                    },
                    type: "POST",
                    success: function (data) {
                        loadLinkedTenantDetails(data);
                    }
                });
            }
        }

        function loadLinkedTenantDetails(data) {
            $("#RequestedByTeanants").html(data);
            jQDataTableTenant();
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>