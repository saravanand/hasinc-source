﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.UserManagement.LinkedTenantUsers>" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantSuccessMessage")))
  { %>
<div class="alert alert-success">
    <%=Html.CelloValidationMessage("TenantSuccessMessage")%>
</div>
<%} %>
<%  if (ViewData["RequstByUser"] != null && ((List<LinkedTenantUsers>)ViewData["RequstByUser"]).Count() > 0)
    { %>
<%      
        Html.Grid((IEnumerable<LinkedTenantUsers>)ViewData["RequstByUser"]).Columns(
        column =>
        {
            column.For(col => col.UserDetails.MembershipDetails.UserName).Named(this.GetLocalResourceObject("lbl_UserName").ToString());
            column.For(col => col.UserDetails.User.FirstName.Length > 25 ? col.UserDetails.User.FirstName.Substring(0, 25) + "..." : col.UserDetails.User.FirstName).Named(this.GetLocalResourceObject("lbl_FirstName").ToString());
            column.For(col => col.Comments.Length > 25 ? Regex.Match(col.Comments, @"(\w+[\s.]*){1,10}").Value + ".." : col.Comments).Named(this.GetLocalResourceObject("lbl_Comments").ToString());
            column.For(col => "<a style='color:#0000FF; text-decoration:underline' href='mailto:" + col.UserDetails.MembershipDetails.EmailId + "' >" + col.UserDetails.MembershipDetails.EmailId + "</a>").Named(this.GetLocalResourceObject("lbl_UserMail").ToString()).DoNotEncode();

            column.For(col => col.RequestStatus == RequestStatus.WaitingForTenantApproval
                        ? this.GetLocalResourceObject("m_waitingApproval").ToString()
                        : col.RequestStatus.ToString()).Named(this.GetLocalResourceObject("lbl_RequestStatus").ToString());

            column.For(col => col.RequestStatus.Equals(RequestStatus.WaitingForTenantApproval) && !col.RequestStatus.Equals(RequestStatus.Approved) && !col.RequestStatus.Equals(RequestStatus.Rejected)
                        ? "<a id='Approve' style='color: green;' href='#' title='" + this.GetLocalResourceObject("lbl_Approve").ToString() + " " + col.UserDetails.MembershipDetails.UserName + " Request' onclick =approveRequest('" + col.Id + "','" + col.UserId + "','" + col.RequestedBy + "')> <i class='fa fa-check-circle'></i></a>"
                        : "-")
                        .Named(this.GetLocalResourceObject("lbl_Approve").ToString()).DoNotEncode();

            column.For(col => col.RequestStatus.Equals(RequestStatus.WaitingForTenantApproval) || col.RequestStatus.Equals(RequestStatus.Approved) && !col.RequestStatus.Equals(RequestStatus.Rejected)
                        ? "<a id='Reject' style='color: red;' href='#' title='" + this.GetLocalResourceObject("lbl_Reject").ToString() + " " + col.UserDetails.MembershipDetails.UserName + " Request' onclick =rejectRequest('" + col.Id + "','" + col.UserId + "','" + col.RequestedBy + "')> <i class='fa fa-times-circle'></a>"
                        : "-")
                        .Named(this.GetLocalResourceObject("lbl_Reject").ToString()).DoNotEncode();

        }).Attributes(id => "dataListOtherTenantUsers", cellpadding => "0", cellspacing => "1", @class => "clear celloTable").Render();
                                                         
%>
<%}
    else
    {%>
<div class="info">
    <%: this.GetLocalResourceObject("m_NoUserRequest") %>
</div>
<%}%>
