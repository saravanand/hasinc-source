﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var respondRequestUrl = '<%: Url.Action("ApproveTenantRequest","TenantUserAssociation") %>';
        var respondUserRequestUrl = '<%: Url.Action("RespondRequest","TenantUserAssociation") %>';
        var status = '<%: this.GetLocalResourceObject("m_Ignore") %>';
        var approvedStatusMessage = '<%: this.GetLocalResourceObject("m_Approved") %>';
        var rejectMessage = '<%: this.GetLocalResourceObject("m_rejectRequest") %>';
        var rejectedStatus = '<%: this.GetLocalResourceObject("m_Rejected") %>';
        var dialogtitle = '<%: this.GetLocalResourceObject("lbl_MapRoles") %>';
        var approvalError = '<%: this.GetLocalResourceObject("lbl_errorMsg") %>';
    </script>
    <script type="text/javascript" src="<%: Url.Content("../../Scripts/othertenantuserslist.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("../../Scripts/helpers.js") %>"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("lbl_ApproveUser") %>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <section class="panel red">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("lbl_ApproveUser") %>
                </h4>
            </header>
            <div class="panel-body">
                <div id="statusMessage">
                </div>
                <div class="blue">
                    <i class="fa fa-info-circle"></i>
                    <%: this.GetLocalResourceObject("info_ApproveRequest") %>
                </div>

                <%  if (ViewData["RequstByUser"] != null && ((List<CelloSaaS.Model.UserManagement.LinkedTenantUsers>)ViewData["RequstByUser"]).Count() > 0)
                    { %>
                <div class="grid simple">
                    <div class="grid-title">
                        <div class="row">
                            <div class="col-sm-5 m-b-xs">
                                <div id="user_filter" class="dataTables_filter">
                                    <div class="input-group">
                                        <input type="text" class="input-sm form-control" id="tenantUserTableSearchText" name="tenantUserTableSearchText"
                                            placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white" onclick="DoUserSearch();" type="button"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantSuccessMessage")))
                      { %>
                    <div class="success">
                        <%=Html.CelloValidationMessage("TenantSuccessMessage")%>
                    </div>
                    <%} %>
                    <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantErrorMessage")))
                       { %>
                    <div class="error">
                        <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
                    </div>
                    <%} %>

                    <div class="grid-body">
                        <div id="OtherTenantUser">
                            <%Html.RenderPartial("OtherTenantUsers"); %>
                        </div>
                    </div>
                </div>
                <%}
                    else
                    { %>
                <div class="alert alert-info">
                    <%:this.GetGlobalResourceObject("UserResource","e_NoData")%>
                </div>
                <%}                    
                %>
            </div>

        </section>
        <section class="panel blue">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("lbl_Requestedusers") %>
                </h4>
            </header>
            <div class="panel-body">
                <div class="blue">
                    <i class="fa fa-info-circle"></i>
                    <%: this.GetLocalResourceObject("info_Requests") %>
                </div>


                <%  if (ViewData["RequstByAdmin"] != null && ((List<CelloSaaS.Model.UserManagement.LinkedTenantUsers>)ViewData["RequstByAdmin"]).Count() > 0)
                    { %>
                <div class="grid simple">
                    <div class="grid-title">
                        <div class="row">
                            <div class="col-sm-5 m-b-xs">
                                <div id="Div1" class="dataTables_filter">
                                    <div class="input-group">
                                        <input type="text" class="input-sm form-control" id="tenantTableSearchText" name="tenantTableSearchText"
                                            placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-white" onclick="DoSearch();" type="button"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-body">
                        <div id="RequestedByTeanat">
                            <%Html.RenderPartial("RequestByAdmin"); %>
                        </div>
                    </div>
                </div>
                <%}
                    else
                    { %>
                <div class="alert alert-info">
                    <%:this.GetGlobalResourceObject("UserResource","e_NoData")%>
                </div>
                <%} %>
            </div>
        </section>
    </div>
</asp:Content>
