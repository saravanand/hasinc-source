﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.UserManagement.LinkedTenantUsers>" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%  if (ViewData["RequstByAdmin"] != null && ((List<LinkedTenantUsers>)ViewData["RequstByAdmin"]).Count() > 0)
    { %>
<%
        string strEditImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/edit_icon.gif");

        Html.Grid((IEnumerable<LinkedTenantUsers>)ViewData["RequstByAdmin"]).Columns(
         column =>
         {
             column.For(col => col.UserDetails.MembershipDetails.UserName).Named(this.GetLocalResourceObject("lbl_UserName").ToString());
             column.For(col => col.UserDetails.User.FirstName).Named(this.GetLocalResourceObject("m_FirstName").ToString());
             column.For(col => "<a style='color:#0000FF; text-decoration:underline' href='mailto:" + col.UserDetails.MembershipDetails.EmailId + "' >" + col.UserDetails.MembershipDetails.EmailId + "</a>").Named(this.GetLocalResourceObject("lbl_UserMail").ToString()).DoNotEncode();
             column.For(col => col.Comments.Length > 25 ? Regex.Match(col.Comments, @"(\w+[\s.]*){1,10}").Value + "..." : col.Comments).Named(this.GetLocalResourceObject("m_Comments").ToString());
             column.For(col => col.RequestStatus == RequestStatus.WaitingForUserApproval
                 ? this.GetLocalResourceObject("m_waitingApproval")
                 : col.RequestStatus.ToString())
                 .Named(this.GetLocalResourceObject("m_RequestStatus").ToString()).DoNotEncode();

         }).Attributes(id => "dataListRequestByAdmin", cellpadding => "0", cellspacing => "1", @class => "clear celloTable").Render();                                                         
%>
<%}
    else
    {%>
<div class="info">
    <%: this.GetLocalResourceObject("m_NoRequests") %>
</div>
<%}%>
