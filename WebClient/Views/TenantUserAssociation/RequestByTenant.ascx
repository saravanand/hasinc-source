﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.Model.UserManagement.LinkedTenantUsers>" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<% 
    Html.EnableUnobtrusiveJavaScript(false);
    Html.EnableClientValidation(false);
%>

<%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantSuccessMessage")))
  { %>
<div class="alert alert-success">
    <%=Html.CelloValidationMessage("TenantSuccessMessage")%>
</div>
<%} %>
<%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("TenantErrorMessage")))
  { %>
<div class="alert alert-danger">
    <%=Html.CelloValidationMessage("TenantErrorMessage")%>
</div>
<%} %>
<%  if (ViewData["RequstByAdmin"] != null && ((List<LinkedTenantUsers>)ViewData["RequstByAdmin"]).Count() > 0)
    { %>
<div class="grid-title">
    <div class="row">
        <div class="col-sm-5 m-b-xs">
            <div id="user_filter" class="dataTables_filter">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="tenantTableUserSearchText" name="tenantTableUserSearchText"
                        placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" onclick="DoSearch();" type="button"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<%
        Html.Grid((IEnumerable<LinkedTenantUsers>)ViewData["RequstByAdmin"]).Columns(
          column =>
          {
              column.For(col => col.TenantDetails.TenantName).Named(this.GetGlobalResourceObject("General", "Tenant") + " " + this.GetGlobalResourceObject("General", "Name"));
              column.For(col => col.Comments.Length > 25 ? Regex.Match(col.Comments, @"(\w+[\s.]*){1,10}").Value + ".." : col.Comments).Named(this.GetLocalResourceObject("lbl_Comments").ToString());

              column.For(col => col.RequestStatus.Equals(RequestStatus.WaitingForUserApproval)
                  ? this.GetLocalResourceObject("m_waitingUserApproval")
                  : col.RequestStatus.ToString()).Named(this.GetGlobalResourceObject("General", "Status").ToString());

              column.For(col => col.RequestStatus.Equals(RequestStatus.WaitingForUserApproval) && !col.RequestStatus.Equals(RequestStatus.Approved)
                  ? "<a id='Approve' style='color: green;font-weight:bold;' href='#' title='" + string.Format(this.GetLocalResourceObject("t_AddTenantDetails").ToString(), col.TenantDetails.TenantName) + "' onclick=approveRequest('" + col.Id + "','" + col.UserId + "')><i class='fa fa-check-circle'></i></a>"
                  : "-").Named(this.GetGlobalResourceObject("General", "Approve").ToString()).DoNotEncode();

              column.For(col => (col.RequestStatus.Equals(RequestStatus.WaitingForUserApproval) || col.RequestStatus.Equals(RequestStatus.Approved))

                  ? "<a id='Reject'style='color: red;font-weight:bold;' href='#' title='" + string.Format(this.GetLocalResourceObject("t_RejectTenantDetails").ToString(), col.TenantDetails.TenantName) + "' onclick=rejectRequest('" + col.Id + "','" + col.UserId + "') ><i class='fa fa-times-circle'></i></a>"
                  : "-").Named(this.GetGlobalResourceObject("General", "Reject").ToString()).DoNotEncode();
          }).Attributes(id => "dataListRequestByTenant", @class => "celloTable").Render();
                                                         
%>
<%}
    else if (ViewData["sharedUsers"] != null && bool.Parse(ViewData["sharedUsers"].ToString()))
    {
%>
<div class="alert alert-info">
    <%: this.GetLocalResourceObject("m_NoRequest") %>
</div>
<%}%>
