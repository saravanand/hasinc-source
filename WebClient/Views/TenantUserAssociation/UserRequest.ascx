﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    if (Html.ValidationSummary() != null && !string.IsNullOrEmpty(Html.ValidationSummary().ToString()))
    {
%>
<%: Html.Hidden("IsErrorView", true) %>
<div>
    <%: Html.ValidationSummary()%>
</div>
<%
    }
    else
    {
%>
<%: Html.Hidden("IsErrorView", false) %>
<div class="modal fade" id="userRequestModel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><%: this.GetLocalResourceObject("lbl_dialogtitle") %></h4>
            </div>
            <div class="modal-body">
                <div class="requestUser">
                    <table class="celloTable details_container_table">
                        <tbody>
                            <tr>
                                <td style="width: 130px;">
                                    <%: this.GetLocalResourceObject("lbl_UserName")%>
                                    <%=Html.Hidden("RequestUserId", ViewData["UserId"])%>
                                    <%=Html.Hidden("RequestStatus", ViewData["ReqStatus"])%>
                                    <%=Html.Hidden("RequestlinkId", ViewData["linkedId"])%>
                                </td>
                                <td style="padding: 5px;">
                                    <%= Html.Display("UserName", ViewData["UserName"].ToString(), new { maxlength = 50, style = "width:200px;" })%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 130px;">
                                    <%: this.GetLocalResourceObject("lbl_Comments")%>
                                </td>
                                <td style="padding: 5px;">
                                    <%= Html.TextArea("Comments", new { style = "width:300px;", @placeholder = this.GetLocalResourceObject("m_PlaceHolder") })%>
                                </td>
                            </tr>
                            <%
        if (ViewData["RolesList"] != null && ((List<CelloSaaS.Model.AccessControlManagement.Role>)ViewData["RolesList"]).Count > 0)
        {
            var rolesList = ((List<CelloSaaS.Model.AccessControlManagement.Role>)ViewData["RolesList"]);
                            %>
                            <tr>
                                <td colspan="2">
                                    <b>
                                        <%: this.GetLocalResourceObject("lbl_AvailableRoles")%></b>
                                </td>
                            </tr>
                            <%
            foreach (var role in rolesList)
            {                       
                        
                            %>
                            <tr>
                                <td>
                                    <%: role.RoleName%>
                                </td>
                                <td>
                                    <input class="_userroles" type="checkbox" name="<%: role.RoleName %>" id="<%: role.RoleId %>" />
                                </td>
                            </tr>
                            <%
            }
        }
                            %>
                        </tbody>                       
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <a class="btn btn-success" href="#" title="<%:this.GetGlobalResourceObject("General", "Link")%>" onclick="linkUser();">
                        <i class="fa fa-link"></i>
                        <%=this.GetGlobalResourceObject("General", "Link")%>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<% } %>