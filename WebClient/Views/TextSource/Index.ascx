﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<style type="text/css">
    .tableSourcesForms form
    {
        display: inline;
    }
</style>
<%    
    if (ViewData["viewObjectId"] == null)
    {
        ViewData["viewObjectId"] = Guid.NewGuid().ToString();
    }

    string viewObjectId = ViewData["viewObjectId"].ToString();
%>
<div class="tableSourcesForms" id="<%:viewObjectId %>">
    <%
        if (ViewData["statusMessage"] != null)
        {
    %>
    <div class="textSource_Messages">
        <%: ViewData["statusMessage"].ToString() %>
    </div>
    <%
        }
        
    %>
    <table style="width: 100%;">
        <tr>
            <td>
                <%: Html.Label("Available Text Sources") %>
            </td>
            <td>
                <%: Html.Hidden("formMode") %>
                <%--<%: Html.Hidden("viewObjectId", viewObjectId) %>--%>
                <input type="hidden" class="viewObjectId" name="viewObjectId" value="<%: viewObjectId %>" />
                <%: Html.DropDownList("textSourceId",null, new {@onChange="setTxtSourceId('"+viewObjectId+"')" ,@class="textSourceId"})%>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <%
                    AjaxOptions addAjaxOptions = new AjaxOptions();
                    addAjaxOptions.HttpMethod = "Get";
                    addAjaxOptions.InsertionMode = InsertionMode.Replace;
                    addAjaxOptions.UpdateTargetId = viewObjectId;
                %>
                <%
                    using (Ajax.BeginForm("ManageTextSource", "TextSource", addAjaxOptions, new { @class = "addTextSourceForm" }))
                    { 
                %>
                <%: Html.Hidden("viewObjectId", viewObjectId)%>
                <input type="image" src="../../App_Themes/CelloSkin/new.png" name="add New" alt="Add New Table Source" />
                <%}     %>
                <%
                    AjaxOptions editAjaxOptions = new AjaxOptions
                   {
                       HttpMethod = "GET",
                       InsertionMode = InsertionMode.Replace,
                       UpdateTargetId = viewObjectId
                   };
                    using (Ajax.BeginForm("ManageTextSource", "TextSource", editAjaxOptions, new { @class = "editTextSourceForm", @style = "display:none" }))
                    { 
                %>
                <%: Html.Hidden("editTextSourceId", null, new { @class = "txtSrcid" })%>
                <%--<%: Html.Hidden("viewObjectId", viewObjectId) %>--%>
                <input type="hidden" class="viewObjectId" name="viewObjectId" value="<%: viewObjectId %>" />
                <input type="image" src="../../App_Themes/CelloSkin/edit.png" name="edit" alt="Edit Text Source" />
                <%}     %>
                <%
                    AjaxOptions deleteAjaxOptions = new AjaxOptions
                    {
                        HttpMethod = "POST",
                        InsertionMode = InsertionMode.Replace,
                        UpdateTargetId = viewObjectId
                    };
                    using (Ajax.BeginForm("DeleteSource", "TextSource", deleteAjaxOptions, new { @class = "deleteTextSourceForm", @style = "display:none" }))
                    { 
                %>
                <%--<%: Html.Hidden("viewObjectId", viewObjectId) %>--%>
                <input type="hidden" class="viewObjectId" name="viewObjectId" value="<%: viewObjectId %>" />
                <%: Html.Hidden("deleteTextSourceId", null, new { @class = "txtSrcid" })%>
                <input type="image" src="../../App_Themes/CelloSkin/delete.png" name="delete" alt="Delete Text Source" />
                <%}     %>
            </td>
        </tr>
    </table>
</div>
