﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%@ Import Namespace="CelloSaaS.Reporting.DataSources.Model" %>
<%
    if (ViewData["viewObjectId"] == null)
    {
        ViewData["viewObjectId"] = Guid.NewGuid().ToString();
    }

    string viewObjectId = ViewData["viewObjectId"].ToString();

    AjaxOptions ajaxOptions = new AjaxOptions
    {
        HttpMethod = "POST",
        InsertionMode = InsertionMode.Replace,
        UpdateTargetId = viewObjectId
    };

    using (Ajax.BeginForm("ManageTextSource", "TextSource", ajaxOptions))
    {
        TextSource txtSource = new TextSource();

        if (ViewData["textSource"] != null)
        {
            txtSource = (TextSource)ViewData["textSource"];
        }
%>
<div id="<%:viewObjectId %>">
    <table style="width: 100%;">
        <tr>
            <td>
                <%: Html.Label("Text Source Name")%>
            </td>
            <td>
                <%: Html.Hidden("viewObjectId",viewObjectId) %>
                <%: Html.Hidden("Id", txtSource.Id)%>
                <%: Html.TextBox("Name", txtSource.Name)%>
            </td>
        </tr>
        <tr>
            <td>
                <%: Html.Label("Text Source Content")%>
            </td>
            <td>
                <%:Html.TextArea("Content", txtSource.Content, new {@rows="5", @style = "width:300px" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <input type="image" src="../../App_Themes/CelloSkin/save.gif" value="Save" />
            </td>
        </tr>
    </table>
</div>
<% } %>