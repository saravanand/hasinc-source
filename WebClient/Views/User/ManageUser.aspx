<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('select').select2({ width: "100%" });
        });
        var commentMandatoryMessage = '<%: this.GetLocalResourceObject("m_CommentMandatory") %>';
        var requestLink = '<%: Url.Action("RequestUser","TenantUserAssociation") %>';
        var otherTenantUserListLink = '<%: Url.Action("OtherTenantUsersList","TenantUserAssociation") %>';
        var linkException = '<%: this.GetLocalResourceObject("lbl_UserAddException") %>';
        var dialogTitle = '<%: this.GetLocalResourceObject("lbl_dialogtitle") %>';
        var validateUrl = '<%: Url.Action("ValidateUserMail","User") %>';
        var minRole = '<%: this.GetLocalResourceObject("lbl_minRole") %>';
        var linkErrorMsg = '<%: this.GetLocalResourceObject("e_ErrorLinking") %>';
    </script>
    <script type="text/javascript" src="/Scripts/manageuser.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("ManageUser", "User", FormMethod.Post, new { id = "userDetailsFrom", role = "form" }))
       { %>
    <% 
           string sortDirection = (string)ViewData["SortDirection"];
           string sortString = (string)ViewData["SortString"];
           int pageNumber = (int)ViewData["PageNumber"];
    %>
    <%= Html.Hidden("PageNumber", pageNumber)%>
    <%= Html.Hidden("SortString", sortString)%>
    <%= Html.Hidden("SortDirection", sortDirection)%>
    <%        
           StringBuilder content;
           ColumnProperties columnAttribute;
           CelloSaaS.Model.UserManagement.UserDetails userDetails = null;
           CurrentFormMode currentMode = new CurrentFormMode();
           if (ViewData["FormMode"] != null)
               currentMode = (CurrentFormMode)ViewData["FormMode"];

           Dictionary<int, ColumnProperties> columnAttributes = new Dictionary<int, ColumnProperties>();

           columnAttribute = new ColumnProperties();
           columnAttribute.LabelContentProperties = new Dictionary<string, string>() { { "style", "width:250px;text-align:right;" } };
           columnAttribute.FieldContentProperties = new Dictionary<string, string>();

           columnAttributes.Add(1, columnAttribute);

           columnAttribute = new ColumnProperties();
           columnAttribute.LabelContentProperties = new Dictionary<string, string>() { { "style", "width:150px;text-align:right;" } };
           columnAttribute.FieldContentProperties = new Dictionary<string, string>();
           columnAttributes.Add(2, columnAttribute);

           CelloSaaS.View.AlternateRowStyle alternateRowStyle = new CelloSaaS.View.AlternateRowStyle();

           Dictionary<string, string> formAttributes = new Dictionary<string, string>();
           formAttributes.Add("id", "frmAdd");
           formAttributes.Add("class", "details_container_table");
           CelloSaaS.View.DataViewTable frmTable = null;
           if (ViewData["UserDetails"] as UserDetails == null)
               userDetails = new UserDetails();
           else
               userDetails = ViewData["UserDetails"] as UserDetails;          
    %>
    <div class="page-title">
        <a href="<%=Url.Action("UserList") %>?page=<%= pageNumber %>&sortString=<%= sortString %>&sortDirection=<%= sortDirection %>" title="  <%: this.GetLocalResourceObject("t_GoBack")%>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%if (currentMode == CurrentFormMode.Insert)
              { %>
            <%: this.GetLocalResourceObject("AddUserHeader")%>
            <%}
              else if (currentMode == CurrentFormMode.Edit)
              { %>
            <%: this.GetLocalResourceObject("EditUserHeader")%>
            <%}%>
        </h3>
        <div class="pull-right">
            <%=Html.CelloSubmitButton("<i class=\"fa fa-save\"></i>&nbsp;" + this.GetGlobalResourceObject("General","Save")) %>
            <%=Html.CelloButton(Url.Action("UserList","User",new{page=pageNumber,sortString=sortString,sortDirection=sortDirection}), this.GetGlobalResourceObject("General","Cancel")) %>
        </div>
    </div>
    <div class="pd-25">
        <div class="userMessage">
        </div>
        <% if (Html.ValidationSummary() != null && string.IsNullOrEmpty(Html.CelloValidationMessage("AccessError")))
           {
               if (!string.IsNullOrEmpty(Html.CelloValidationMessage("DuplicateEmailUserErrorMessage")) && ViewData["IsLinkable"] != null && bool.Parse(ViewData["IsLinkable"].ToString()))
               {
        %>
        <div class="alert alert-danger">
            <a onclick="ValidateUserEmail()" style="cursor: pointer !important" class="link">
                <%= Html.CelloValidationMessage("DuplicateEmailUserErrorMessage") %>
            </a>
        </div>
        <% 
               }
               else
               { 
        %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary(this.GetGlobalResourceObject("ErrorMessage","ErrorTitle").ToString())%>
        </div>
        <% } %>
        <% } %>
        <% if (ViewBag.AccessDenied != true)
           { %>
        <div class="row-fluid">
            <section class="panel purple">
                <header class="panel-heading">
                    <h4><%: this.GetLocalResourceObject("UserDetails")%></h4>
                </header>
                <div class="panel-body">
                    <%
               frmTable = new CelloSaaS.View.DataViewTable("UserDetailsForm", currentMode, formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, userDetails, "UserDetails", UserIdentity.TenantID);

               frmTable.StartInsertSection();

               content = new StringBuilder();
               frmTable.StartTableCell("User_FirstName");
               frmTable.AddFieldLabel("", "User_FirstName", "FirstName", true);
               content.Append(Html.Hidden("UserDetails.Identifier", userDetails.Identifier));
               content.Append(Html.Hidden("UserDetails.User.Identifier", userDetails.User.Identifier));
               content.Append(Html.Hidden("UserDetails.MembershipDetails.TenantCode", userDetails.MembershipDetails.TenantCode));

               content.Append(Html.TextBox("UserDetails.User.FirstName", userDetails.User.FirstName, new { maxlength = 100,required="required" }));
               content.Append(Html.CelloValidationMessage("UserDetails.User.FirstName", "*"));
               frmTable.AddFieldContent("User_FirstName", content.ToString(), "UserDetails.User.FirstName");

               content = new StringBuilder();
               frmTable.StartTableCell("User_LastName");
               frmTable.AddFieldLabel("", "User_LastName", "LastName", true);
               content.Append(Html.TextBox("UserDetails.User.LastName", userDetails.User.LastName, new { maxlength = 100 ,required="required"}));
               content.Append(Html.CelloValidationMessage("UserDetails.User.LastName", "*"));
               frmTable.AddFieldContent("User_LastName", content.ToString(), "UserDetails.User.LastName");


               content = new StringBuilder();
               frmTable.StartTableCell("User_UserName");
               frmTable.AddFieldLabel("", "User_UserName", "Username", true);
               content.Append(Html.Hidden("UserDetails.MembershipDetails.TenantCode", userDetails.MembershipDetails.TenantCode));
               content.Append(Html.TextBox("UserDetails.MembershipDetails.UserName", userDetails.MembershipDetails.UserName, new { maxlength = 150 ,required="required"}));
               content.Append(Html.CelloValidationMessage("UserDetails.MembershipDetails.UserName", "*"));
               frmTable.AddFieldContent("User_UserName", content.ToString(), "UserDetails.MembershipDetails.UserName");

               content = new StringBuilder();
               frmTable.StartTableCell("MembershipDetails_EmailId");
               frmTable.AddFieldLabel("", "MembershipDetails_EmailId", "Email");
               content.Append(Html.TextBox("UserDetails.MembershipDetails.EmailId", userDetails.MembershipDetails.EmailId, new { maxlength = 255, tooltip = Resources.TenantResource.m_sampleEmail, placeholder = Resources.TenantResource.m_sampleEmail }));
               content.Append(Html.CelloValidationMessage("UserDetails.MembershipDetails.EmailId", "*"));
               frmTable.AddFieldContent("MembershipDetails_EmailId", content.ToString(), "UserDetails.MembershipDetails.EmailId");

               frmTable.EndInsertSection();

               frmTable.StartEditSection();
               content = new StringBuilder();
               frmTable.StartTableCell("User_FirstName");
               frmTable.AddFieldLabel("", "User_FirstName", "FirstName", true);
               content.Append(Html.Hidden("UserDetails.Identifier", userDetails.Identifier));
               content.Append(Html.Hidden("UserDetails.User.Identifier", userDetails.User.Identifier));
               content.Append(Html.Hidden("UserDetails.User.UserId", userDetails.User.UserId));
               content.Append(Html.Hidden("UserDetails.User.MembershipId", userDetails.User.MembershipId));
               content.Append(Html.Hidden("UserDetails.User.AddressId", userDetails.User.AddressId));
               content.Append(Html.Hidden("UserDetails.MembershipDetails.TenantCode", userDetails.MembershipDetails.TenantCode));

               content.Append(Html.TextBox("UserDetails.User.FirstName", userDetails.User.FirstName, new { maxlength = 100 ,required="required"}));
               content.Append(Html.CelloValidationMessage("UserDetails.User.FirstName", "*"));
               frmTable.AddFieldContent("User_FirstName", content.ToString(), "UserDetails.User.FirstName");

               content = new StringBuilder();
               frmTable.StartTableCell("User_LastName");
               frmTable.AddFieldLabel("", "User_LastName", "LastName", true);
               content.Append(Html.TextBox("UserDetails.User.LastName", userDetails.User.LastName, new { maxlength = 100 ,required="required"}));
               content.Append(Html.CelloValidationMessage("UserDetails.User.LastName", "*"));
               frmTable.AddFieldContent("User_LastName", content.ToString(), "UserDetails.User.LastName");

               content = new StringBuilder();
               frmTable.StartTableCell("User_UserName");
               frmTable.AddFieldLabel("", "User_UserName", "Username", true);
               content.Append(Html.Hidden("UserDetails.MembershipDetails.Identifier", userDetails.MembershipDetails.Identifier));
               content.Append(Html.Hidden("UserDetails.MembershipDetails.TenantCode", userDetails.MembershipDetails.TenantCode));
               content.Append(Html.TextBox("UserDetails.MembershipDetails.UserName", userDetails.MembershipDetails.UserName, new { maxlength = 150 ,required="required"}));
               content.Append(Html.CelloValidationMessage("UserDetails.MembershipDetails.UserName", "*"));
               frmTable.AddFieldContent("User_UserName", content.ToString(), "UserDetails.MembershipDetails.UserName");

               content = new StringBuilder();
               frmTable.StartTableCell("MembershipDetails_EmailId");
               frmTable.AddFieldLabel("", "MembershipDetails_EmailId", "Email");
               content.Append(Html.TextBox("UserDetails.MembershipDetails.EmailId", userDetails.MembershipDetails.EmailId, new { maxlength = 255 , tooltip = Resources.TenantResource.m_sampleEmail, placeholder = Resources.TenantResource.m_sampleEmail}));
               content.Append(Html.CelloValidationMessage("UserDetails.MembershipDetails.EmailId", "*"));
               frmTable.AddFieldContent("MembershipDetails_EmailId", content.ToString(), "UserDetails.MembershipDetails.EmailId");

               frmTable.EndEditSection();

               Response.Write(frmTable.Render());
                    %>
                </div>
            </section>
            <section class="panel green">
                <header class="panel-heading">
                    <h4><%: this.GetLocalResourceObject("AddresDetails") %></h4>
                </header>
                <div class="panel-body">
                    <% if (userDetails.Address == null) userDetails.Address = new CelloSaaS.Model.TenantManagement.Address();
                       frmTable = new CelloSaaS.View.DataViewTable("UserAddressDetailsForm", currentMode, formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, userDetails.Address, "UserDetails.Address", UserIdentity.TenantID);

                       frmTable.StartInsertSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Address1");
                       frmTable.AddFieldLabel("", "Address_Address1", "Address1", true);
                       content.Append(Html.Hidden("UserDetails.Address.AddressId", userDetails.Address.AddressId));
                       content.Append(Html.Hidden("UserDetails.Address.Identifier", userDetails.Address.Identifier));

                       content.Append(Html.TextBox("UserDetails.Address.Address1", userDetails.Address.Address1, new { maxlength = 1000 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.Address1", "*"));
                       frmTable.AddFieldContent("Address_Address1", content.ToString(), "UserDetails.Address.Address1");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_City");
                       frmTable.AddFieldLabel("", "Address_City", "City", true);
                       content.Append(Html.TextBox("UserDetails.Address.City", userDetails.Address.City, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.City", "*"));
                       frmTable.AddFieldContent("Address_City", content.ToString(), "UserDetails.Address.City");


                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_State");
                       frmTable.AddFieldLabel("", "Address_State", "State", true);
                       content.Append(Html.TextBox("UserDetails.Address.State", userDetails.Address.State, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.State", "*"));
                       frmTable.AddFieldContent("Address_State", content.ToString(), "UserDetails.Address.State");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Country");
                       frmTable.AddFieldLabel("", "Address_Country", "Country", true);
                       content.Append(Html.DropDownList("UserDetails.Address.CountryId", null, new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.CountryId", "*"));
                       frmTable.AddFieldContent("Address_Country", content.ToString(), "UserDetails.Address.CountryId");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_PostalCode");
                       frmTable.AddFieldLabel("", "Address_PostalCode", "Zip code", true);
                       content.Append(Html.TextBox("UserDetails.Address.PostalCode", userDetails.Address.PostalCode, new { maxlength = 15 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.PostalCode", "*"));
                       frmTable.AddFieldContent("Address_PostalCode", content.ToString(), "UserDetails.Address.PostalCode");

                       frmTable.EndInsertSection();

                       frmTable.StartEditSection();

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Address1");
                       frmTable.AddFieldLabel("", "Address_Address1", "Address1", true);
                       content.Append(Html.Hidden("UserDetails.Address.AddressId", userDetails.Address.AddressId));
                       content.Append(Html.Hidden("UserDetails.Address.Identifier", userDetails.Address.Identifier));
                       content.Append(Html.TextBox("UserDetails.Address.Address1", userDetails.Address.Address1, new { maxlength = 1000 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.Address1", "*"));
                       frmTable.AddFieldContent("Address_Address1", content.ToString(), "UserDetails.Address.Address1");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_City");
                       frmTable.AddFieldLabel("", "Address_City", "City", true);
                       content.Append(Html.TextBox("UserDetails.Address.City", userDetails.Address.City, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.City", "*"));
                       frmTable.AddFieldContent("Address_City", content.ToString(), "UserDetails.Address.City");


                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_State");
                       frmTable.AddFieldLabel("", "Address_State", "State", true);
                       content.Append(Html.TextBox("UserDetails.Address.State", userDetails.Address.State, new { maxlength = 100 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.State", "*"));
                       frmTable.AddFieldContent("Address_State", content.ToString(), "UserDetails.Address.State");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_Country");
                       frmTable.AddFieldLabel("", "Address_Country", "Country", true);
                       content.Append(Html.DropDownList("UserDetails.Address.CountryId", null, new { style = "width:100%;" }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.CountryId", "*"));
                       frmTable.AddFieldContent("Address_Country", content.ToString(), "UserDetails.Address.CountryId");

                       content = new StringBuilder();
                       frmTable.StartTableCell("Address_PostalCode");
                       frmTable.AddFieldLabel("", "Address_PostalCode", "Zip code", true);
                       content.Append(Html.TextBox("UserDetails.Address.PostalCode", userDetails.Address.PostalCode, new { maxlength = 15 }));
                       content.Append(Html.CelloValidationMessage("UserDetails.Address.PostalCode", "*"));
                       frmTable.AddFieldContent("Address_PostalCode", content.ToString(), "UserDetails.Address.PostalCode");

                       frmTable.EndEditSection();

                       Response.Write(frmTable.Render());
                    %>
                </div>
            </section>
        </div>
        <%
               if (CelloSaaS.Library.Helpers.ConfigHelper.EnableExternalAuthentication)
               {
                   if (currentMode == CurrentFormMode.Insert)
                   { %>
                <div class="row-fluid">
            <section class="panel blue">
                <header class="panel-heading">
                    <h4>Authorization Providers</h4>
                </header>
                <div class="panel-body">
                    <%
              if (ViewBag.UserAuthenticationType != null)
              {
                    %>
                    <%: Html.Label("Provider")%>
                    <%: Html.Hidden("TenantIdProvider", ViewBag.UserAuthType as string)%>
                    <%
                  if (currentMode == CurrentFormMode.Insert)
                  {
                    %>    <%: Html.DropDownList("UserAuthenticationType")%>
                    <%}
                  else
                  {
                      if (ViewBag.UserAuthType != null)
                      {
                    %>
                    <%: Html.Label(ViewBag.UserAuthType as string) %> <%
                      }
                      else
                      {
                    %>  <%: Html.DropDownList("UserAuthenticationType")%><%
                                   }
                  }
                    %>
                    <%
              }
                    %>
                </div>
            </section>
        </div>
        <%
          }
        %>
        <%if (currentMode == CurrentFormMode.Edit || currentMode == CurrentFormMode.View)
          { %>
        <div class="row-fluid">
            <section class="panel blue">
                <header class="panel-heading">
                    <h4>Authorization Providers</h4>
                </header>
                <div class="panel-body">
                    <div class="form-container">
                        <div class="col-md-6">
                            <%
              if (ViewBag.TenantIdProvider != null)
              {
                            %>
                            <%: Html.Label("Provider")%>
                            <%: Html.Label(ViewBag.TenantIdProvider as string) %>
                            <%
              }
                            %>
                        </div>
                    </div>
                    <div class="form-container">
                        <div class="col-md-6">
                            <%
              if (ViewBag.LoginProvider != null)
              {
                            %>
                            <%: Html.Label("Authentication Type")%>
                            <%: Html.Label(ViewBag.TenantIdProvider as string) %>
                            <%
              }
                            %>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <%
          }
            }
        %>
        <div class="row-fluid">
            <div class="form-actions">
                <div class="pull-right">
                    <%=Html.CelloSubmitButton("<i class=\"fa fa-save\"></i>&nbsp;" + this.GetGlobalResourceObject("General","Save")) %>
                    <%=Html.CelloButton(Url.Action("UserList","User",new{page=pageNumber,sortString=sortString,sortDirection=sortDirection}), this.GetGlobalResourceObject("General","Cancel")) %>
                </div>
            </div>
        </div>
        <% } %>
    </div>
    <% } %>
    <div class="addUserPopup" title='<%: this.GetLocalResourceObject("m_UserAlreadyExists") %>'>
        <div class="userRequestForm">
        </div>
    </div>
</asp:Content>
