﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $(document).on('change', '#chkSelectAll', function () {
                var chk = $(this).is(':checked');
                $('input[type=checkbox][name=tenantIds]:not(:disabled)').each(function () {
                    $(this).prop('checked', chk);
                });
            });
        });

        function ShowTenantList(roleId) {
            var url = '<%:Url.Action("GetUserRoleTenantMappingList") %>';
            var userId = $('input[type=hidden][name=userId]').val();
            var tenantId = $('input[type=hidden][name=tenantId]').val();

            HideAjaxMessage();
            $('#serviceListDiv').hide();
            $('#addRoleDiv').hide();

            $.post(url, { roleId: roleId, userId: userId, tenantId: tenantId }, function (data) {
                $('#TenantListDiv').html(data);
                $('#TenantListDiv').show();
            });
        }

        function SaveTenantMappings(roleId) {
            var url = '<%:Url.Action("UpdateUserRoleTenantMapping") %>';
            var userId = $('input[type=hidden][name=userId]').val();

            var tenantIds = new Array();
            $('input[type=checkbox][name=tenantIds]').each(function () {
                if ($(this).is(':checked')) {
                    tenantIds.push($(this).val());
                }
            });

            // should not allow the mapping to be saved when no tenants are chosen
            if (tenantIds.length < 1) {

                $("#ajaxStatusMessage .success").hide();

                if ($("#ajaxStatusMessage").length > 0 && $("#ajaxStatusMessage").is(":visible")) {
                    if ($("#ajaxStatusMessage .error").length > 0) {
                        $("#ajaxStatusMessage .error").html("<%: this.GetLocalResourceObject("e_RoleMapping") %>");
                    }
                    else {
                        $("#ajaxStatusMessage").append("<div class='alert alert-danger'><%: this.GetLocalResourceObject("e_RoleMapping") %></div>");
                    }
                }
                else if ($("#ajaxStatusMessage").length > 0 && !$("#ajaxStatusMessage").is(":visible")) {
                    if ($("#ajaxStatusMessage .error").length > 0) {
                        $("#ajaxStatusMessage .error").html("<%: this.GetLocalResourceObject("e_RoleMapping") %>");
                    }
                    else {
                        $("#ajaxStatusMessage").append("<div class='alert alert-danger'><%: this.GetLocalResourceObject("e_RoleMapping") %></div>");
                    }
                    $("#ajaxStatusMessage").show();
                }
            return false;
        }

        $('input[type=hidden][name=hiddenTenantIds]').each(function () {
            tenantIds.push($(this).val());
        });

        $.ajax({
            url: url,
            data: JSON.stringify
            ({
                userId: userId,
                roleId: roleId,
                tenantIds: tenantIds
            }),
            dataType: 'json',
            contentType: 'application/json',
            type: 'POST',
            success: function (data) {
                if (data) {
                    if (data.Error == undefined) {
                        ShowAjaxStatusMessage(data.Success, true);
                    } else {
                        ShowAjaxStatusMessage(data.Error, false);
                    }
                } else {
                    ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("e_TenantMapping") %>', false);
            }
            },
            error: function (xhr) {
                ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("e_TenantMapping") %>', false);
            }
        });

            $('#TenantListDiv').hide();
        }

        function SaveServiceMappings() {
            var url = '<%:Url.Action("UpdateUserServiceMapping") %>';
            var userId = $('input[type=hidden][name=userId]').val();
            var serviceIds = new Array();

            $('input[type=checkbox][name=userRoleService]').each(function () {
                if ($(this).is(':checked')) {
                    serviceIds.push($(this).val());
                }
            });

            $.ajax({
                url: url,
                data: JSON.stringify({
                    userId: userId,
                    serviceIds: serviceIds
                }),
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    if (data.Error == undefined) {
                        //success
                        ReloadRoleDetailsList(function () {
                            ShowAjaxStatusMessage(data.Success, true);
                        });
                    } else {
                        //failure
                        ShowAjaxStatusMessage(data.Error, false);
                    }
                }
            });
            $('#serviceListDiv').hide();
        }

        function RemoveRoleFromUser(roleId) {
            var url = '<%:Url.Action("RemoveRoleFromUser") %>';
            var userId = $('input[type=hidden][name=userId]').val();

            $.post(url, { userId: userId, roleId: roleId }, function (data) {
                if (data.Error == undefined) {
                    //success
                    ReloadRoleDetailsList(function () {
                        ShowAjaxStatusMessage(data.Success, true);
                    });
                } else {
                    //failure
                    ShowAjaxStatusMessage(data.Error, false);
                }
            }, 'json');
        }

        function Cancel() {
            $('#TenantListDiv').hide();
            $('#serviceListDiv').hide();
        }

        function ShowAddUserRoleForm() {
            HideAjaxMessage();
            $('#addRoleDiv').show();
            $('#TenantListDiv').hide();
            $('#serviceListDiv').hide();
        }

        function HideAddUserRoleForm() {
            $('#addRoleDiv').hide();
        }

        function AddRoleToUser() {
            var url = '<%:Url.Action("AddRoleToUser") %>';
                var userId = $('input[type=hidden][name=userId]').val();
                var roleIds = new Array();
                var removeableElements = new Array();

                $('input[type=checkbox][name=chkRoleIds]:checked').each(function () {
                    roleIds.push($(this).val());
                    removeableElements.push($(this).parent('div'));
                });

                if (roleIds.length == 0) {
                    ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("e_MinRole") %>', false);
                    return false;
                }

                $.ajax({
                    url: url,
                    data: JSON.stringify({
                        userId: userId,
                        roleIds: roleIds
                    }),
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data) {
                        if (data.Error == undefined) {
                            //success
                            ReloadRoleDetailsList(function () {
                                ShowAjaxStatusMessage(data.Success, true);
                            });
                            for (var div in removeableElements) {
                                $(div).remove();
                            }
                            $('#addRoleDiv').hide();
                        } else {
                            //failure
                            ShowAjaxStatusMessage(data.Error, false);
                        }
                    }
                });

                return false;
            }

            function ReloadRoleDetailsList(callback) {
                var url = '<%:Url.Action("ManageUserRole") %>';
                var userId = $('input[type=hidden][name=userId]').val();
                var tenantId = $('input[type=hidden][name=tenantId]').val();

                $('#serviceListDiv').hide();
                $('#TenantListDiv').hide();

                $('#roleDetailsList').load(url, { userId: userId, tenantId: tenantId }, function () {
                    callback();
                    var roleId = $('select[name=RoleList]').val();
                    if (roleId == '-1') {
                        $('input[type=button][name=addRoleToUser]').attr('disabled', 'disabled');
                    }
                });
            }

            function ShowAjaxStatusMessage(message, success) {
                var div = $('#ajaxStatusMessage');
                div.hide();

                var html = '';
                if (success) {
                    html += '<div class="alert alert-success">';
                } else {
                    html += '<div class="alert alert-danger">';
                }
                html += message;
                html += '</div>';

                div.html(html);
                div.show();
            }

            function HideAjaxMessage() {
                var div = $('#ajaxStatusMessage');
                div.hide();
            }

            function ShowServiceList() {
                HideAjaxMessage();
                $('#addRoleDiv').hide();
                $('#TenantListDiv').hide();
                $('#serviceListDiv').show();
            }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <%
        string sortDirection = Request.QueryString["sortDirection"];
        string sortString = Request.QueryString["sortString"];
        int pageNumber = 0;

        if (!string.IsNullOrEmpty(Request.QueryString["pageNumber"]))
        {
            Int32.TryParse(Request.QueryString["pageNumber"], out pageNumber);
        }
    %>
    <% using (Html.BeginForm("ManageUserRole", "User", FormMethod.Post, new { id = "manageRoleTenantForm" }))
       {%>
    <div class="page-title">
        <a href="<%=Url.Action("UserList") %>?page=<%= pageNumber %>&sortString=<%= sortString %>&sortDirection=<%= sortDirection %>" title="<%: this.GetLocalResourceObject("t_GoBack") %>">
            <i class="icon-custom-left"></i>
        </a>
        <h3>
            <%: this.GetLocalResourceObject("m_ManageUserRoles") %>
        </h3>
    </div>
    <div class="pd-25">
        <div id="ajaxStatusMessage"></div>
        <div id="roleDetailsList">
            <% Html.RenderPartial("UserRoleTenantDetails"); %>
        </div>
    </div>
    <% } %>
</asp:Content>
