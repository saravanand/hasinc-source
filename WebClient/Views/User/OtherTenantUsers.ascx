﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%  if (ViewData["OtherTenantUser"] != null && ((List<CelloSaaS.Model.UserManagement.LinkedTenantUsers>)ViewData["OtherTenantUser"]).Count() > 0)
    { %>
<%
        string strEditImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/edit_icon.gif");

        Html.Grid((IEnumerable<CelloSaaS.Model.UserManagement.LinkedTenantUsers>)ViewData["OtherTenantUser"]).Columns(
        column =>
        {
            column.For(col => col.UserDetails.User.FirstName.Length > 25 ? col.UserDetails.User.FirstName.Substring(0, 25) + "..." : col.UserDetails.User.FirstName).Named(this.GetLocalResourceObject("lbl_FirstName").ToString());
            column.For(col => col.UserDetails.User.LastName.Length > 25 ? col.UserDetails.User.LastName.Substring(0, 25) + "..." : col.UserDetails.User.LastName).Named(this.GetLocalResourceObject("lbl_LastName").ToString());
            column.For(col => col.UserDetails.MembershipDetails.UserName.Length > 25 ? col.UserDetails.MembershipDetails.UserName.Substring(0, 25) + "..." : col.UserDetails.MembershipDetails.UserName).Named(this.GetLocalResourceObject("lbl_UserName").ToString());
            column.For(col => col.UserDetails.MembershipDetails.EmailId).Named(this.GetLocalResourceObject("lbl_EmailId").ToString());

        }).Attributes(id => "dataListOtherTenantUsers", cellpadding => "0", cellspacing => "1", @class => "celloTable").Render();                                                         
%>
<%}
    else
    {%>
<div class="info">
    <%: this.GetLocalResourceObject("m_nodata") %>
</div>
<%}%>
