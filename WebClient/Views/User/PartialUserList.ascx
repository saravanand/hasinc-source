<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.UserManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% 
    string tenantId = CelloSaaS.Library.UserIdentity.TenantID;
    Dictionary<string, string> tenantDetails = ViewData["tenantDetails"] as Dictionary<string, string>;
    var searchFilter = ViewData["searchFilter"] != null ? ViewData["searchFilter"].ToString() : string.Empty;
%>
<div class="span12">
    <div class="grid simple">
        <div class="grid-title">
            <div class="row">
                <div class="col-md-12">
                    <select name="userSearchFilter" style="padding: 5px; width: 110px; display: inline;" class="form-control">
                        <option value="Username" <%: searchFilter== "Username" ? "selected=selected" : "" %>>Username
                        </option>
                        <option value="Email" <%:searchFilter == "Email" ? "selected=selected" : "" %>>Email
                        </option>
                        <option value="Both" <%:searchFilter == "Both" ? "selected=selected" : "" %>>Both
                        </option>
                    </select>
                    <div id="user_filter" class="dataTables_filter" style="display: inline-block;">
                        <input id="userSearchText" name="userSearchText" class="form-control" type="text" style="width: 250px; display: inline;"
                            value="<%:ViewData["searchString"] %>"
                            placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                        <button class="btn btn-sm btn-white" style="display: inline-block;" type="button" onclick="SearchUser();"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                    </div>
                    <div class="btn-group btn-group-sm pull-right">
                        <%=Html.CelloButton("#", this.GetLocalResourceObject("lbl_Lock"), new Dictionary<string,object> { { "class", "btn btn-warning" } }, null, "CanLockUser", "ManageUserAccountsLock(true);")%>
                        <%=Html.CelloButton("#", this.GetLocalResourceObject("lbl_LockAll"), new Dictionary<string,object> { { "class", "btn btn-danger" } }, null, "CanUnLockUser","LockAllUser('"+tenantId+"');") %>
                        <%=Html.CelloButton("#", this.GetLocalResourceObject("lbl_Un-Lock"), new Dictionary<string,object> { { "class", "btn btn-primary" } }, null, "CanUnLockUser", "ManageUserAccountsLock(false);") %>
                        <%=Html.CelloButton("#", this.GetLocalResourceObject("lbl_Un-LockAll"), new Dictionary<string,object> { { "class", "btn btn-danger" } }, null, "CanUnLockUser","UnLockAllUser('"+tenantId+"');") %>
                        <%=Html.CelloButton("#", this.GetLocalResourceObject("lbl_ResetPassword"), new Dictionary<string,object> { { "class", "btn btn-warning" } }, null, "CanResetPINUser","ResetPINforUserAccounts();") %>
                        <%=Html.CelloButton("#", this.GetLocalResourceObject("lbl_ForcePasswordReset"), new Dictionary<string,object> { { "class", "btn btn-info" } }, null, "CanForcePassword","ForcePasswordReset(true);") %>
                        <%=Html.CelloButton("#", "<i class='fa fa-check'></i>"+this.GetLocalResourceObject("lbl_ApproveUserAccounts"), new Dictionary<string,object> { { "class", "btn btn-info" } }, null, "CanApproveUser","ApproveUserAccounts();") %>
                        <%=Html.CelloButton("#", "<i class=\"fa fa-bookmark\"></i>&nbsp;Invite", new Dictionary<string,object> { { "class", "btn btn-info" } }, null, "UserInvite","InviteUsers();") %>
                        <%=Html.CelloCreateButton(Url.Action("ManageUser","User"), ("<i class=\"fa fa-plus\"></i> "+this.GetGlobalResourceObject("General","Create")), null, null, "CreateUser") %>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-body">
            <%
                string sortDirection = (string)ViewData["SortDirection"];
                string sortString = (string)ViewData["SortString"];
                int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
                int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
                int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;

                var ajaxOpt = new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementId = "loading", LoadingElementDuration = 500 };
            %>
            <div class="celloTableWrapper">
                <%
                    if (ViewData["UserDetails"] != null && ((IEnumerable<UserDetails>)ViewData["UserDetails"]).Count() > 0)
                    {
                        Html.CelloGrid<UserDetails>((IEnumerable<UserDetails>)ViewData["UserDetails"], "UserDetailsGrid", tenantId).CelloColumns(
                        column =>
                        {
                            column.ForColumn(col => (UserIdentity.UserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)
                            || ProductAdminConstants.ProductAdminUserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)
                            || !col.MembershipDetails.TenantCode.Equals(tenantId, StringComparison.OrdinalIgnoreCase)) ? string.Empty : "<input type=\"checkbox\" name=\"chk_" + col.User.UserId + "\"/>")
                                .SetFieldIdentifier("User_Select")
                                .Named("<input type=\"checkbox\" name=\"selectAllUserCheckbox\"/>")
                                .HeaderAttributes(@class => "check_box halign noSortCol", style => "")
                                .Attributes(@class => "check_box halign")
                                .DoNotEncode();

                            column.ForColumn(col => col.User.FirstName).SetFieldIdentifier("User_FirstName").Named(Ajax.ActionLink("~First~", "UserList", "User", new
                            {
                                page = pageNumber,
                                pageSize = pageSize,
                                sortString = UserDetailsSortStringConstants.FirstName,
                                sortDirection = sortString.Equals(UserDetailsSortStringConstants.FirstName) && !string.IsNullOrEmpty(sortDirection) && sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending) ? CelloSaaS.Model.SortExpressionConstants.Decending : string.Empty
                            },
                            ajaxOpt).ToHtmlString()).HeaderAttributes(new
                            Hash(@class => (sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.FirstName) ? ((string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"))).DoNotEncode();

                            column.ForColumn(col => (tenantDetails != null && tenantDetails.Count > 0) ? tenantDetails[col.MembershipDetails.TenantCode] : string.Empty).SetFieldIdentifier("User_TenantCodeString").Named("User_TenantCodeString");

                            column.ForColumn(col => col.User.LastName).SetFieldIdentifier("User_LastName").Named(Ajax.ActionLink("~Last~", "UserList", "User", new
                            {
                                page = pageNumber,
                                pageSize = pageSize,
                                sortString = CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.LastName,
                                sortDirection = sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.LastName) && !string.IsNullOrEmpty(sortDirection) && sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending) ? CelloSaaS.Model.SortExpressionConstants.Decending : string.Empty
                            },
                            ajaxOpt).ToHtmlString()).HeaderAttributes(new
                            Hash(@class => (sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.LastName) ? ((string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"))).DoNotEncode();
                            column.ForColumn(col => col.MembershipDetails.UserName).SetFieldIdentifier("User_UserName").Named(Ajax.ActionLink("~User~", "UserList", "User", new
                            {
                                page = pageNumber,
                                pageSize = pageSize,
                                sortString = CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.UserName,
                                sortDirection = sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.UserName) && !string.IsNullOrEmpty(sortDirection) && sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending) ? CelloSaaS.Model.SortExpressionConstants.Decending : string.Empty
                            },
                            new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementId = "loading", LoadingElementDuration = 500 }).ToHtmlString()).HeaderAttributes(new
                            Hash(@class => (sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.UserName) ? ((string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"))).DoNotEncode();
                            column.ForColumn(col => col.MembershipDetails.EmailId).SetFieldIdentifier("User_EmailId").Named(Ajax.ActionLink("~Email~", "UserList", "User", new
                            {
                                page = pageNumber,
                                pageSize = pageSize,
                                sortString = UserDetailsSortStringConstants.EmailId,
                                sortDirection = sortString.Equals(UserDetailsSortStringConstants.EmailId) && !string.IsNullOrEmpty(sortDirection) && sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending) ? CelloSaaS.Model.SortExpressionConstants.Decending : string.Empty
                            },
                            new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementId = "loading", LoadingElementDuration = 500 }).ToHtmlString()).HeaderAttributes(new
                            Hash(@class => (sortString.Equals(UserDetailsSortStringConstants.EmailId) ? ((string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting"))).DoNotEncode();

                            column.ForColumn(x => (!string.IsNullOrEmpty(x.User.User_Description) && x.User.User_Description.Length > 30) ? x.User.User_Description.Substring(0, 30) + "..." : x.User.User_Description)
                                .SetFieldIdentifier("User_Description").Named("~Description~");

                            if (ViewData["CanEditUser"] != null && (bool)ViewData["CanEditUser"] == true)
                            {
                                //Edit
                                column.ForColumn(col => (col.User.Status == false || (ProductAdminConstants.ProductAdminUserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)
                                    || !col.MembershipDetails.TenantCode.Equals(tenantId, StringComparison.OrdinalIgnoreCase) && !CelloSaaS.Library.UserIdentity.IsInRole(RoleConstants.ProductAdmin)))
                                    ? string.Empty
                                    : Html.CelloActionLink("Edit User", "ManageUser", new { userId = col.User.UserId, pageNumber = pageNumber, sortString = sortString, sortDirection = sortDirection }, new { title = this.GetGlobalResourceObject("General", "Edit") + " '" + col.User.FirstName + "'" }).Replace("Edit User", "<i class='fa fa-pencil'></i>")).SetFieldIdentifierAndPermissions("", CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.UpdateUser).Named(this.GetGlobalResourceObject("General", "Edit").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                            }

                            if (ViewData["CanDeleteUser"] != null && (bool)ViewData["CanDeleteUser"] == true)
                            {
                                //Deactivate
                                column.ForColumn(col => (CelloSaaS.Library.UserIdentity.UserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)
                                || !col.MembershipDetails.TenantCode.Equals(tenantId, StringComparison.OrdinalIgnoreCase) || ProductAdminConstants.ProductAdminUserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase) || col.User.Status == false) ? string.Empty : "<a href=\"#\" title=\"" + this.GetLocalResourceObject("lbl_Deactivate") + " '" + col.User.FirstName + "'\" onclick=\"DeactivateUser('" + col.User.UserId + "','" + col.User.FirstName + "')\" ><i class='fa fa-times-circle'></i></a>").SetFieldIdentifierAndPermissions("", CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.DeleteUser).Named(this.GetLocalResourceObject("lbl_Deactivate").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                            }

                            if (ViewData["CanDeleteUser"] != null && (bool)ViewData["CanDeleteUser"] == true)
                            {
                                //Activate
                                column.ForColumn(col => (CelloSaaS.Library.UserIdentity.UserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)
                            || !col.MembershipDetails.TenantCode.Equals(tenantId, StringComparison.OrdinalIgnoreCase) || ProductAdminConstants.ProductAdminUserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase) || col.User.Status == true) ? string.Empty : "<a href=\"#\" title=\"" + this.GetLocalResourceObject("lbl_Activate") + " '" + col.User.FirstName + "' \" onclick=\"ActivateUser('" + col.User.UserId + "','" + col.User.FirstName + "')\" ><i class='fa fa-plus-circle'></i></a>").SetFieldIdentifierAndPermissions("", CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.DeleteUser).Named(this.GetLocalResourceObject("lbl_Activate").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                            }

                            if (false && ViewData["CanDeleteUser"] != null && (bool)ViewData["CanDeleteUser"] == true)
                            {
                                //Disassociate
                                column.ForColumn(col => (CelloSaaS.Library.UserIdentity.UserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)
                            || !col.MembershipDetails.TenantCode.Equals(tenantId, StringComparison.OrdinalIgnoreCase) || ProductAdminConstants.ProductAdminUserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)) ? string.Empty : "<a href=\"#\" title=\"" + this.GetLocalResourceObject("lbl_Activate") + " '" + col.User.FirstName + "' \" onclick=\"DisAssociateUser('" + col.User.UserId + "')\" ><i class='fa fa-times-circle'></i></a>").SetFieldIdentifierAndPermissions("", CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.DeleteUser).Named("DisAssociate").HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                            }

                            if (ViewData["CanManageRoles"] != null && (bool)ViewData["CanManageRoles"] == true)
                            {
                                //Manage Role
                                column.ForColumn(col => col.User.Status == false ? string.Empty : Html.CelloActionLink("~Manage Roles~", "ManageUserRole", new { userId = col.User.UserId, pageNumber = pageNumber, sortString = sortString, sortDirection = sortDirection }, new { title = this.GetLocalResourceObject("lbl_ManageRoles") + " for '" + col.User.FirstName + "'" }).Replace("~Manage Roles~", "<i class='fa fa-wrench'></i>")).Named(this.GetLocalResourceObject("lbl_ManageRoles").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                            }
                            column.ForColumn(col => col.MembershipDetails.IsLockedOut ? "<i class='fa fa-lock'></i>" : string.Empty).SetFieldIdentifier("User_IsLocked").Named(Ajax.ActionLink("~IsLocked~", "UserList", "User",
                            new
                            {
                                page = pageNumber,
                                pageSize = pageSize,
                                sortString = CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.IsLocked,
                                sortDirection = sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.IsLocked) && !string.IsNullOrEmpty(sortDirection) && sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending) ? CelloSaaS.Model.SortExpressionConstants.Decending : string.Empty
                            },
                            ajaxOpt).ToHtmlString()).Attributes(@class => "halign").DoNotEncode().HeaderAttributes(new
                            Hash(@class => "halign " + (sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.IsLocked) ? ((string.IsNullOrEmpty(sortDirection) || sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending)) ? "sorting_asc" : "sorting_desc") : "sorting")));

                            if (ViewData["CanApproveUser"] != null && (bool)ViewData["CanApproveUser"])
                            {
                                //Approve Users
                                column.ForColumn(col => (CelloSaaS.Library.UserIdentity.UserId.Equals(col.User.UserId, StringComparison.OrdinalIgnoreCase)
                                || !col.MembershipDetails.TenantCode.Equals(tenantId, StringComparison.OrdinalIgnoreCase)
                                || col.MembershipDetails.IsApproved)
                                    ? string.Empty
                                    : "<a href=\"#\" title=\"" + this.GetLocalResourceObject("lbl_Approve") + " '" + col.User.FirstName + "'\" onclick=\"ApproveUsers('" + col.User.UserId + "')\" ><i class='fa fa-check'></i></a>")
                                    .SetFieldIdentifierAndPermissions("", CelloSaaS.ServiceContracts.AccessControlManagement.PrivilegeConstants.ApproveUser)
                                    .Named(Ajax.ActionLink("Approve Users", "UserList", "User",
                            new
                            {
                                page = pageNumber,
                                pageSize = pageSize,
                                sortString = CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.IsApproved,
                                sortDirection = sortString.Equals(CelloSaaS.Model.UserManagement.UserDetailsSortStringConstants.IsApproved) && !string.IsNullOrEmpty(sortDirection) && sortDirection.Equals(CelloSaaS.Model.SortExpressionConstants.Ascending) ? CelloSaaS.Model.SortExpressionConstants.Decending : string.Empty
                            },
                            ajaxOpt).ToHtmlString()).Attributes(@class => "halign").DoNotEncode();


                                //this.GetLocalResourceObject("lbl_Approve").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
                            }

                        }).Attributes(cellspacing => "1", cellpadding => "0", width => "100%", @class => "celloTable").Render();

                    }
                    else
                    {%>
                <div class="alert alert-info">
                    <%: this.GetLocalResourceObject("NoRecordFound") %>
                </div>
                <%}%>
            </div>
            <%  if (ViewData["UserDetails"] != null && ((IEnumerable<CelloSaaS.Model.UserManagement.UserDetails>)ViewData["UserDetails"]).Count() > 0)
                {
                    Ajax.CelloPager(ajaxOpt)
                        .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
                        .Render();
                } %>
        </div>
    </div>
</div>
