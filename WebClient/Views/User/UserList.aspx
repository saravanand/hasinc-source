<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage" %>

<%@ Import Namespace="MvcContrib.Pagination" %>
<%@ Import Namespace="CelloSaaS.View" %>
<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<asp:Content ID="content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();

            $(document).on('keypress', '#userSearchText', function (e) {
                if (e.keyCode == 13) {
                    SearchUser();
                    e.preventDefault();
                }
            });

            $(document).on('keypress', '#otherTenantUserTableSearchText', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var sortString = $('input[type=hidden][name=pager_sortString]').val();
                var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();
                var searchString = $('input[type=text][name=userSearchText]').val();
                var searchFilter = $('select[name=userSearchFilter]').val();
                var tenantId = $('#tenantContextId').val();

                var queryString = '?pageSize=' + $(this).val();
                queryString += '&sortString=' + (sortString || '');
                queryString += '&sortDirection=' + (sortDirection || '');
                queryString += '&searchString=' + searchString;
                queryString += '&searchFilter=' + searchFilter;
                queryString += '&tenantId=' + tenantId;

                $.post('<%=Url.Action("UserList")%>' + queryString, null, function (data) {
                    $('#divGrid').html(data);
                });

                $('div.alert').remove();
            });

            $(document).on('click', 'input[type=checkbox][name=selectAllUserCheckbox]', function () {
                var check = false;
                if ($(this).is(':checked')) {
                    check = true;
                }
                $('input[type=checkbox][name^="chk_"]', '#divGrid').each(function () {
                    $(this).prop('checked', check == true ? 'checked' : null);
                })
            });
        });

        function jQDataTable() {
            filterTable = $('table#dataListOtherTenantUsers').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "bRetrieve": true,
                "bStateSave": true
            });
        }

        function DoSearch() {
            var searchText = $('#otherTenantUserTableSearchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.alert").remove();
            return false;
        }

        function GetSelectedUserIds() {
            var userIds = [];
            $('input[type=checkbox][name^="chk_"]:checked', '#divGrid').each(function () {
                var id = $(this).attr('name');
                userIds.push(id.substring(4, id.length));
            });

            return userIds;
        }

        function ManageUserAccountsLock(lock) {
            var userIds = GetSelectedUserIds();
            var tenantId = $('#tenantContextId').val();

            if (userIds.length <= 0) {
                ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("m_MinUserSelection") %>', false);
                return;
            }

            var unLockUrl = '<%= Url.Action("UnLockUserAccounts","User")%>';
            var lockUrl = '<%= Url.Action("LockUserAccounts","User")%>';
            var url = unLockUrl;

            if (lock) { // lock user accounts
                url = lockUrl;
            }

            PostAndLoadUserList(url, { userIds: userIds, tenantId: tenantId });
        }

        function ForcePasswordReset() {
            var userIds = GetSelectedUserIds();
            var tenantId = $('#tenantContextId').val();

            if (userIds.length <= 0) {
                ShowAjaxStatusMessage('Please select at-least one user', false);
                return;
            }

            var url = '<%= Url.Action("ForcePasswordReset","User")%>';
            PostAndLoadUserList(url, { userIds: userIds, tenantId: tenantId });
        }

        function LockAllUser(tenantId) {
            if (confirm('<%: string.Format(this.GetLocalResourceObject("m_lockPrompt").ToString(),this.GetLocalResourceObject("m_LockAll")) %>')) {
                PostAndLoadUserList('<%:Url.Action("LockAllUserAccounts","User") %>', { tenantId: tenantId });
            }
        }

        function UnLockAllUser(tenantId) {
            if (confirm('<%: string.Format(this.GetLocalResourceObject("m_lockPrompt").ToString(),this.GetLocalResourceObject("m_UnLockAll")) %>')) {
                PostAndLoadUserList('<%:Url.Action("UnLockAllUserAccounts","User") %>', { tenantId: tenantId });
            }
        }

        function ResetPINforUserAccounts() {

            var userIds = GetSelectedUserIds();
            var tenantId = $('#tenantContextId').val();

            if (userIds.length <= 0) {
                ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("e_SelectMinUser") %>', false);
                return;
            }

            if (confirm('<%: string.Format(this.GetLocalResourceObject("prompt").ToString(),this.GetLocalResourceObject("m_ResetPasswordPrompt")) %>')) {
                var url = '<%= Url.Action("ResetPINforUserAccounts","User")%>';

                PostAndLoadUserList(url, { userIds: userIds, tenantId: tenantId });
            }
        }

        function ApproveUserAccounts() {

            var userIds = GetSelectedUserIds();
            var tenantId = $('#tenantContextId').val();

            if (userIds.length <= 0) {
                ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("e_SelectMinUser") %>', false);
                return;
            }

            if (confirm('<%: string.Format(this.GetLocalResourceObject("prompt").ToString(),this.GetLocalResourceObject("Invite"),this.GetLocalResourceObject("m_billingInfo")) %>')) {
                var url = '<%= Url.Action("ApproveUserAccounts","User")%>';

                PostAndLoadUserList(url, { userIds: userIds, tenantId: tenantId });
            }
        }

        function InviteUsers() {

            var userIds = GetSelectedUserIds();
            var tenantId = $('#tenantContextId').val();

            if (userIds.length <= 0) {
                ShowAjaxStatusMessage('<%: this.GetLocalResourceObject("e_SelectMinUser") %>', false);
                return;
            }

            if (confirm('<%: string.Format(this.GetLocalResourceObject("prompt").ToString(),this.GetLocalResourceObject("Approve"),this.GetLocalResourceObject("m_billingInfo")) %>')) {
                var url = '<%= Url.Action("InviteUsers","User")%>';

                PostAndLoadUserList(url, { userIds: userIds, tenantId: tenantId });
            }
        }

        function DeactivateUser(userId) {
            if (userId == '' || !confirm('<%: string.Format(this.GetLocalResourceObject("prompt").ToString(),this.GetLocalResourceObject("Deactivate")) %>')) return;
            var tenantId = $('#tenantContextId').val();

            var url = '<%= Url.Action("DeleteUser","User")%>';

            PostAndLoadUserList(url, { userId: userId, tenantId: tenantId });
        }

        function ApproveUsers(userId) {
            if (userId == '' || !confirm('<%: string.Format(this.GetLocalResourceObject("prompt").ToString(),this.GetLocalResourceObject("Approve")) %>')) return;
            var tenantId = $('#tenantContextId').val();

            var url = '<%= Url.Action("ApproveUserAccounts","User")%>';

            PostAndLoadUserList(url, { userIds: userId, tenantId: tenantId });
        }

        function ActivateUser(userId) {
            if (userId == '' || !confirm('<%: string.Format(this.GetLocalResourceObject("prompt").ToString(),this.GetLocalResourceObject("Activate")) %>')) return;
            var tenantId = $('#tenantContextId').val();

            var url = '<%= Url.Action("ActivateUser","User")%>';

            PostAndLoadUserList(url, { userId: userId, tenantId: tenantId });
        }

        function DisAssociateUser(userId) {
            if (userId == '' || !confirm('<%: string.Format(this.GetLocalResourceObject("prompt").ToString(),this.GetLocalResourceObject("Activate")) %>')) return;
            var tenantId = $('#tenantContextId').val();

            var url = '<%= Url.Action("DisAssociateUser","User")%>';

            PostAndLoadUserList(url, { userId: userId, tenantId: tenantId });
        }

        function getQS() {
            var pageSize = $('input[type=hidden][name=pager_pageSize]').val();
            var pageIndex = $('input[type=hidden][name=pager_pageNumber]').val();
            var sortString = $('input[type=hidden][name=pager_sortString]').val();
            var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();
            var tenantId = $('#tenantContextId').val();

            var queryString = '?page=' + (pageIndex || 1);
            queryString += '&pageSize=' + (pageSize || 10);
            queryString += '&sortString=' + (sortString || '');
            queryString += '&sortDirection=' + (sortDirection || '');
            queryString += '&tenantId=' + tenantId;

            return queryString;
        }

        function PostAndLoadUserList(url, postdata) {
            var userListUrl = '<%= Url.Action("UserList","User")%>';
            var queryString = getQS();
            $.ajax({
                url: url,
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(postdata),
                success: function (data) {
                    if (data.error == undefined) {
                        // success
                        $('#divGrid').load(userListUrl + queryString, function () {
                        });
                        ShowAjaxStatusMessage(data.Status, true);
                    } else {
                        //error in updating
                        ShowAjaxStatusMessage(data.error, false);
                    }
                }
            });
        }

        function ShowAjaxStatusMessage(message, success) {
            $('div.success,div.error').remove();
            var div = $('#ajaxStatusMessage');
            var html = '';
            if (success) {
                html += '<div class="alert alert-success">';
            } else {
                html += '<div class="alert alert-danger">';
            }
            html += message;
            html += '</div>';

            div.html(html);
            div.show();
        }

        function SearchUser() {
            var pageSize = $('input[type=hidden][name=pager_pageSize]').val();
            var sortString = $('input[type=hidden][name=pager_sortString]').val();
            var sortDirection = $('input[type=hidden][name=pager_sortDirection]').val();
            var tenantId = $('#tenantContextId').val();

            var queryString = '?pageSize=' + (pageSize || 10);
            queryString += '&sortString=' + (sortString || '');
            queryString += '&sortDirection=' + (sortDirection || '');
            queryString += '&tenantId=' + tenantId;

            var searchString = $('input[type=text][name=userSearchText]').val();
            var searchFilter = $('select[name=userSearchFilter]').val();
            if (searchString != '') searchString = searchString.trim();

            var postdata = { searchString: searchString, searchFilter: searchFilter };
            var url = '<%:Url.Action("UserList","User") %>';

            $.post(url + queryString, postdata, function (data) {
                $('#divGrid').html(data);
            });
            $('div.alert').remove();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("lbl_UserManagement")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <%if (!string.IsNullOrEmpty(Html.CelloValidationMessage("UserSuccessMessage")))
          { %>
        <div class="alert alert-success">
            <%=Html.ValidationMessage("UserSuccessMessage")%>
        </div>
        <%} %>
        <% if (!string.IsNullOrEmpty(Html.CelloValidationMessage("UserErrorMessage")))
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationMessage("UserErrorMessage")%>
        </div>
        <%}
           else
           { %>
        <div id="ajaxStatusMessage">
        </div>
        <div id="divGrid">
            <% Html.RenderPartial("PartialUserList");  %>
        </div>
        <% } %>
        <%if (ViewData["IsLinked"] == "True" && false)
          { %>
        <div class="grid simple horizontal red">
            <div class="row">
                <div class="grid-title">
                    <div class="col-sm-3 m-b-xs">
                        <h4>
                            <%: this.GetLocalResourceObject("lbl_OtherTenantUsers") %>
                        </h4>
                    </div>
                    <div class="col-sm-3 m-b-xs">
                        <div id="other_user_filter" class="dataTables_filter">
                            <div class="input-group">
                                <input id="otherTenantUserTableSearchText" name="tenantTableSearchText" class="input-sm form-control" type="text"
                                    placeholder="<%: this.GetGlobalResourceObject("General","Search") %>" />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%: this.GetGlobalResourceObject("General","GO") %>!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divOtherTenantUsers" class="grid-body">
                <% Html.RenderPartial("OtherTenantUsers");  %>
            </div>
        </div>
        <%} %>
    </div>
</asp:Content>
