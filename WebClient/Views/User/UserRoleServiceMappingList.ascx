﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="heading_container">
    <h1>
        <%: this.GetLocalResourceObject("lbl_EditServiceMapping") %>
    </h1>
</div>
<table class="celloTable" id="serviceListTable">
    <thead>
        <tr>
            <th>
                <%: this.GetLocalResourceObject("lbl_Services") %>
            </th>
        </tr>
    </thead>
    <tbody>
        <% if (ViewData["ServiceList"] != null
       && ((List<CelloSaaS.Model.LicenseManagement.Service>)ViewData["ServiceList"]).Count > 0)
           { %>
        <tr>
            <td style="padding: 5px;">
                <% foreach (var item in (List<CelloSaaS.Model.LicenseManagement.Service>)ViewData["ServiceList"])
                   {
                %>
                <div style="width: 120px; float: left; margin: 5px;">
                    <% if (ViewData["RoleServices"] != null & ((string[])ViewData["RoleServices"]).Contains(item.ServiceCode))
                       { %>
                    <input type="checkbox" name="userRoleService" value="<%=item.ServiceCode %>" style="margin: 0px;
                        padding: 0px; width: 25px;" checked="checked" />
                    <%=item.ServiceName%>
                    <% }
                       else
                       { %>
                    <input type="checkbox" name="userRoleService" value="<%=item.ServiceCode %>" style="margin: 0px;
                        padding: 0px; width: 25px;" />
                    <%=item.ServiceName%>
                    <% } %>
                </div>
                <% } %>
            </td>
        </tr>
        <% }
           else
           { %>
        <tr>
            <td style="padding: 5px;">
                <div class="info">
                    <%: this.GetLocalResourceObject("e_NoServices") %>
                </div>
            </td>
        </tr>
        <% } %>
    </tbody>
</table>
<div class="inner_hold" style="float: right; margin-top: 10px;">
    <div class="green_but">
        <a href="#" onclick="SaveServiceMappings();"><span>
            <%=this.GetGlobalResourceObject("General","Save") %></span></a>
    </div>
    <div class="green_but">
        <a href="#" onclick="Cancel();"><span>
            <%=this.GetGlobalResourceObject("General","Cancel") %></span></a></div>
</div>
