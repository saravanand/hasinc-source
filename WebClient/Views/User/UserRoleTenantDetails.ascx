﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="row-fluid">
    <div class="content-box">
        <div class="row">
            <div class="col-sm-3">
                <label>
                    <%: this.GetLocalResourceObject("lbl_UserName") %>:</label>
                <b>
                    <%: ViewData["UserName"]%></b>
                <input type="hidden" name="userId" value="<%:ViewData["UserId"] %>" />
            </div>
            <div class="col-sm-3">
                <label><%: this.GetLocalResourceObject("lbl_TenantName") %>:</label>
                <b>
                    <%: ViewData["TenantName"]%></b>
                <input type="hidden" name="tenantId" value="<%:ViewData["TenantId"]  %>" />
            </div>
        </div>
    </div>
    <div class="grid simple">
        <div class="grid-title">
            <div class="row">
                <div class="col-sm-2">
                    <h4><%: this.GetLocalResourceObject("t_UserRoles") %></h4>
                </div>
                <div class="col-sm-3 pull-right">
                    <div class="pull-right">
                        <%=Html.CelloCreateButton("#", "<i class=\"fa fa-plus\"></i> " + this.GetGlobalResourceObject("General","Add"), null, null, "CandAddUserRole", "ShowAddUserRoleForm()") %>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-body">
            <table class="celloTable" id="userRoleTable">
                <thead>
                    <tr>
                        <th>
                            <%: this.GetLocalResourceObject("lbl_RoleName") %>
                        </th>
                        <th>
                            <%: this.GetGlobalResourceObject("General", "Description")%>
                        </th>
                        <% if (ViewData["CanManageUserRoleTenant"] != null && (bool)ViewData["CanManageUserRoleTenant"])
                           { %>
                        <th style="text-align: center; width: 180px;">
                            <%: this.GetLocalResourceObject("lbl_ManageTenants") %>
                        </th>
                        <% } %>
                        <% bool loadServices = false;
                           if (ViewData["UserRoleList"] != null
                              && ((List<Role>)ViewData["UserRoleList"]).Where(r => r.RoleId.Equals(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ServiceAdmin)).Count() > 0
                                  && CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
                           {
                               loadServices = true;
                        %>
                        <th style="text-align: center; width: 150px;">
                            <%: this.GetLocalResourceObject("lbl_ManageServices") %>
                        </th>
                        <% } %>
                        <th style="text-align: center; width: 75px;">
                            <%: this.GetLocalResourceObject("lbl_Remove") %>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <%  if (ViewData["UserRoleList"] == null || ((List<Role>)ViewData["UserRoleList"]).Count == 0)
                        {
                    %>
                    <tr>
                        <td colspan="5">
                            <div class="info">
                                <%: this.GetLocalResourceObject("m_NoRoles") %>
                            </div>
                        </td>
                    </tr>
                    <%
                        }
                        else
                        {
                            foreach (var item in (List<Role>)ViewData["UserRoleList"])
                            { %>
                    <tr>
                        <td>
                            <label>
                                <%: item.RoleName %></label>
                        </td>
                        <td>
                            <%:item.Description %>
                        </td>
                        <% if (ViewData["CanManageUserRoleTenant"] != null && (bool)ViewData["CanManageUserRoleTenant"] == true)
                           { %>
                        <td style="text-align: center">
                            <a href="#" onclick="ShowTenantList('<%:item.RoleId %>');" title="<%: this.GetLocalResourceObject("m_ManageMappings") %>">
                                <i class='fa fa-pencil'></i>&nbsp;</a>
                        </td>
                        <% } %>
                        <% if (loadServices)
                           { %>
                        <td style="text-align: center">
                            <% if (item.RoleId.Equals(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ServiceAdmin)
                       && CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
                               {
                            %>
                            <a href="#" onclick="ShowServiceList();" title="<%: this.GetLocalResourceObject("m_ServiceMappings") %>">
                                <i class='fa fa-pencil'></i>&nbsp;</a>
                            <% } %>
                        </td>
                        <% } %>
                        <td style="text-align: center">
                            <a href="#" onclick="RemoveRoleFromUser('<%:item.RoleId %>');" title="<%: this.GetLocalResourceObject("m_RemoveRole") %>">
                                <i class='fa fa-times-circle'></i>&nbsp;</a>
                        </td>
                    </tr>
                    <%  } %>
                    <% }%>
                </tbody>
            </table>
        </div>
    </div>
    <div id="addRoleDiv" style="display: none;">
        <% if (ViewData["RoleList"] != null && ((List<Role>)ViewData["RoleList"]).Count > 0)
           { %>
        <div class="grid simple horizontal green">
            <div class="grid-title">
                <h4><%: this.GetLocalResourceObject("lbl_AddUserRole") %></h4>
            </div>
            <div class="grid-body">
                <div class="row">
                    <% foreach (Role role in (List<Role>)ViewData["RoleList"])
                       { %>
                    <div class="col-md-2">
                        <input type="checkbox" name="chkRoleIds" value="<%:role.RoleId %>" />
                        <b>
                            <%:role.RoleName%></b><br />
                        <%: this.GetGlobalResourceObject("General","Description") %>: <i>
                            <%:role.Description%></i>
                    </div>
                    <% }%>
                </div>
            </div>
            <div class="grid-footer">
                <div class="row">
                    <div class="col-md-3 col-md-offset-9">
                        <div class="pull-right">
                            <%=Html.CelloButton("#", "<i class='fa fa-save'></i>&nbsp;" + this.GetGlobalResourceObject("General","Save"), new Dictionary<string,object>() { {"class","btn btn-success"}}, null,null,"AddRoleToUser();") %>
                            <%=Html.CelloButton("#",this.GetGlobalResourceObject("General","Cancel"),null,null,null,"HideAddUserRoleForm();") %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <% }
           else
           { %>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("m_NoRoles") %>
        </div>
        <% } %>
    </div>
    <% if (loadServices)
       { %>
    <div id="serviceListDiv" style="display: none;">
        <% Html.RenderPartial("UserRoleServiceMappingList"); %>
    </div>
    <% } %>
    <div id="TenantListDiv">
    </div>
</div>
