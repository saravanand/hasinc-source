﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.TenantManagement" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% Role role = ViewData["RoleDetial"] as Role; %>
<input type="hidden" name="editRoleId" value="<%:role.RoleId %>" />
<div class="grid simple horizontal red">
    <div class="grid-title">
        <h4><%: this.GetLocalResourceObject("lbl_EditMapping") %> -
                    <span class="semi-bold"><%:role.RoleName%></span>
        </h4>
        <div class="actions pull-right">
            <input type="checkbox" id="chkSelectAll" />&nbsp;Select All
        </div>
    </div>
    <div class="grid-body">
        <div class="row">
            <div class="scrollable">
                <% var tenantList = ViewData["TenantList"] as Dictionary<TenantDetails, bool>;
                   var userid = ViewData["UserId"] != null ? ViewData["UserId"].ToString() : null;
                   if (tenantList != null && tenantList.Count > 0)
                   {%>
                <% foreach (var item in tenantList)
                   {
                       string disabled = "";
                       if (!string.IsNullOrEmpty(userid)
                           && userid.Equals(ProductAdminConstants.ProductAdminUserId, StringComparison.OrdinalIgnoreCase)
                           && item.Key.TenantCode.Equals(ProductAdminConstants.ProductAdminTenantId, StringComparison.OrdinalIgnoreCase))
                       {
                           disabled = "disabled=\"disabled\"";
                       }
                %>
                <div class="col-sm-2">
                    <% if (item.Value)
                       { %>
                    <input type="checkbox" style="width: 20px" checked="checked" name="tenantIds" value="<%:item.Key.TenantCode%>" <%=disabled %> />
                    <%:item.Key.TenantName%>
                    <% }
                       else
                       { %>
                    <input type="checkbox" style="width: 20px" name="tenantIds" value="<%:item.Key.TenantCode%>" />
                    <%:item.Key.TenantName%>
                    <% } %>
                </div>
                <% } %>
                <%  
                   }
                   else
                   { %>
                <div class="alert alert-warning">
                    <%: this.GetLocalResourceObject("m_NoTenants") %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <div class="grid-footer">
        <div class="row">
            <div class="col-md-3 col-md-offset-9">
                <div class="pull-right">
                    <%=Html.CelloButton("#","<i class='fa fa-save'></i>&nbsp;" + this.GetGlobalResourceObject("General","Save"), new Dictionary<string,object>() { {"class","btn btn-success"}}, null,null,"SaveTenantMappings('"+ role.RoleId +"');") %>
                    <%=Html.CelloButton("#",this.GetGlobalResourceObject("General","Cancel"),null,null,null,"Cancel();") %>
                </div>
            </div>
        </div>
    </div>
</div>
<% if (ViewData["HiddenTenantIds"] != null && (ViewData["HiddenTenantIds"] as List<string>).Count > 0)
   { %>
<div>
    <% foreach (string item in ViewData["HiddenTenantIds"] as List<string>)
       { %>
    <input type="hidden" name="hiddenTenantIds" value="<%:item %>" />
    <% } %>
</div>
<% } %>
<script>
    $(function () {
        if ($('input[type=checkbox][name=tenantIds]:not(:disabled)').length == $('input[type=checkbox][name=tenantIds]:not(:disabled):checked').length) {
            $('#chkSelectAll').prop('checked', true);
        }
    });
</script>