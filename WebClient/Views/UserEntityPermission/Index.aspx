﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #PrivilegeContainer {
            width: 300px;
        }

            #PrivilegeContainer > div {
                padding: 2px 2px 2px 2px;
            }

        .otherPrivileges {
            width: 90px;
            float: left;
        }
    </style>
    <script type="text/javascript">
        $('#AddUserEntityPermissionBtn').hide();

        function addPrivilegeButtonClick() {
            var privilege = $("#addPrivilegeTextbox").val();
            var privilegeclass = privilege.split(' ').join('');
            if (privilege != undefined && privilege != "") {
                var innerhtml = '<div class="otherPrivileges ' + privilegeclass + '"><input checked="checked" onclick="privilegeClick(\'' + privilegeclass + '\')" type="checkbox" class="' + privilegeclass + '"/> <span class="' + privilegeclass + '">' + privilege + '</span><input class="' + privilegeclass + '" type="hidden" name="Privileges" value="' + privilege + '" /></div>';
                $('#PrivilegeContainer').append(innerhtml);
                $("#addPrivilegeTextbox").val('');
            }
        }

        function populatePermissions() {
            var userId = $("#UserList option:selected").val();
            var roleId = $("#RoleList option:selected").val();
            var entityId = $('#EntityList option:selected').val();
            var referenceId = $('#ReferenceList option:selected').val();
            $.ajax({
                url: '/UserEntityPermission/Load',
                type: 'POST',
                data: { 'userId': userId, 'entityId': entityId, 'referenceId': referenceId, 'roleId': roleId },
                beforeSend: function (xhr) {
                    $('#loading').show();
                },
                success: function (data) {
                    $("#UserEntityPermissionId").val(data.userEntityPermissionId);

                    if (data.viewPermission) {
                        $("#ViewPermission").attr('checked', true);
                    } else {
                        $("#ViewPermission").attr('checked', false);
                    }

                    if (data.addPermission) {
                        $("#AddPermission").attr('checked', true);
                    } else {
                        $("#AddPermission").attr('checked', false);
                    }

                    if (data.editPermission) {
                        $("#EditPermission").attr('checked', true);
                    } else {
                        $("#EditPermission").attr('checked', false);
                    }

                    if (data.deletePermission) {
                        $("#DeletePermission").attr('checked', true);
                    } else {
                        $("#DeletePermission").attr('checked', false);
                    }

                    if (data.userEntityPermissionId != "") {
                        $('#buttonSave').html('Update');
                    } else {
                        $('#buttonSave').html('Add');
                    }
                    if (data.UserEntityOtherPrivileges && data.UserEntityOtherPrivileges.length > 0) {
                        $('#PrivilegeContainer').html('');
                        $.each(data.UserEntityOtherPrivileges, function (index, item) {
                            var Privilege = item.Privilege;
                            Privilege = Privilege.split(' ').join('');
                            var innerhtml = '<div class="otherPrivileges ' + Privilege + '"><input checked="checked" onclick="privilegeClick(\'' + Privilege + '\')" type="checkbox" class="' + Privilege + '"/> <span class="' + Privilege + '">' + item.Privilege + '</span><input class="' + Privilege + '" type="hidden" name="Privileges" value="' + Privilege + '" /></div>';
                            $('#PrivilegeContainer').append(innerhtml);
                        });
                    } else {
                        $('#PrivilegeContainer').html('');
                    }

                    if (data.entityId != "" && data.referenceId != "") {
                        $('#AddUserEntityPermissionBtn').fadeIn();
                    } else {
                        $('#AddUserEntityPermissionBtn').fadeOut();
                    }

                },
                complete: function (data) {
                    $('#loading').hide();
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("ManageUserEntityPermission")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <%
            Html.EnableUnobtrusiveJavaScript(true);

            if (Html.ValidationMessage("UserEntityPermissionSuccess") != null)
            {
        %>
        <div class="alert alert-success">
            <%: Html.ValidationMessage("UserEntityPermissionSuccess")%>
        </div>
        <%
        }
        %>
        <%
            if (ViewData["UserIdentifier"] != null)
            {
        %>
        <%: Html.Hidden("UserIdentifier", ViewData["UserIdentifier"].ToString())%>
        <% } %>
        <%
            if (Html.ValidationMessage("UserEntityPermissionErrorMessage") != null)
            {
        %>
        <div id="errorMsg" class="alert alert-danger">
            <%: Html.ValidationMessage("UserEntityPermissionErrorMessage")%>
        </div>
        <% }%>
        <div id="UserEntityPermissionFormDiv">
            <% Html.RenderAction("UserEntityPermissionForm", "UserEntityPermission"); %>
        </div>
        <div id="entityPermissionDetails">
            <% Html.RenderAction("UserEntityPermissionDetails", "UserEntityPermission", new { userId = ViewData["UserList"], entityId = ViewData["EntityList"], referenceId = ViewData["ReferenceList"], roleId = ViewData["RoleList"] }); %>
        </div>
    </div>
</asp:Content>
