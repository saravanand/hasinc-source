﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="CelloSaaS.View.CelloViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        <%: this.GetLocalResourceObject("Title")%></h2>
    <%
        if (Html.ValidationSummary() != null)
        {
    %>
    <div class="success">
        <%: Html.ValidationMessage("UserEntityPermissionSuccess")%>
    </div>
    <%
            }
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="topmenu" runat="server">
</asp:Content>
