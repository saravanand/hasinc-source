﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%
    string referenceId = (string)ViewData["referenceId"];
    string userId = (string)ViewData["UserIdentifier"];
    string roleId = (string)ViewData["RoleIdentifier"];
    string entityId = (string)ViewData["EntityId"];
    string sortDirection = (string)ViewData["SortDirection"];
    string sortString = (string)ViewData["SortString"];
    int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
    int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
    int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;
%>
<div class="grid simple horizontal indigo">
    <div class="grid-title">
        <h4>
            <%: this.GetLocalResourceObject("Title")%>
        </h4>
    </div>
    <div class="grid-body">
        <%
            Dictionary<string, CelloSaaS.Model.UserManagement.UserDetails> userList = new Dictionary<string, CelloSaaS.Model.UserManagement.UserDetails>();
            Dictionary<string, string> roleList = new Dictionary<string, string>();
            Dictionary<string, string> referenceList = new Dictionary<string, string>();

            if (ViewData["UserDetailsList"] != null)
            {
                userList = (Dictionary<string, CelloSaaS.Model.UserManagement.UserDetails>)ViewData["UserDetailsList"];
            }
            if (ViewData["RoleDetailsList"] != null)
            {
                roleList = (Dictionary<string, string>)ViewData["RoleDetailsList"];
            }
            if (ViewData["ReferenceListDetails"] != null)
            {
                referenceList = (Dictionary<string, string>)ViewData["ReferenceListDetails"];
            }
            Dictionary<string, CelloSaaS.Model.AccessControlManagement.UserEntityPermission> userEntityPermissionDetails = (Dictionary<string, CelloSaaS.Model.AccessControlManagement.UserEntityPermission>)ViewData["UserEntityPermission"];

            if (userEntityPermissionDetails != null && userEntityPermissionDetails.Values.Count > 0)
            {
                Html.Grid(userEntityPermissionDetails.Values).Columns(column =>
                                                  {
                                                      column.For(c => !string.IsNullOrEmpty(c.UserId) ? userList.ContainsKey(c.UserId) ? userList[c.UserId].User.FirstName : c.UserId : string.Empty).Named(this.GetLocalResourceObject("UserId").ToString());
                                                      column.For(c => !string.IsNullOrEmpty(c.RoleId) ? roleList.ContainsKey(c.RoleId) ? roleList[c.RoleId] : string.Empty : string.Empty).Named(this.GetLocalResourceObject("RoleId").ToString());
                                                      column.For(c => c.EntityId).Named(this.GetLocalResourceObject("EntityId").ToString());
                                                      column.For(c => c.ReferenceId != null && referenceList.ContainsKey(c.ReferenceId) ? referenceList[c.ReferenceId] : c.ReferenceId).Named(this.GetLocalResourceObject("ReferenceId").ToString());
                                                      column.For(c => c.ViewPermission).Named(this.GetLocalResourceObject("View").ToString());
                                                      column.For(c => c.AddPermission).Named(this.GetLocalResourceObject("Add").ToString());
                                                      column.For(c => c.EditPermission).Named(this.GetLocalResourceObject("Edit").ToString());
                                                      column.For(c => c.DeletePermission).Named(this.GetLocalResourceObject("Delete").ToString());
                                                      column.For(c => c.UserEntityOtherPrivileges != null ? string.Join(", ", c.UserEntityOtherPrivileges.Select(privilege => privilege.Privilege).ToList()) : "-").Named(this.GetLocalResourceObject("OtherPrivileges").ToString());
                                                      column.For(col => Ajax.ActionLink(this.GetLocalResourceObject("EditPermission").ToString(), "UserEntityPermissionForm", new { userId = col.UserId, roleId = col.RoleId, entityId = col.EntityId, referenceId = col.ReferenceId }, new AjaxOptions { HttpMethod = "Get", InsertionMode = InsertionMode.Replace, UpdateTargetId = "UserEntityPermissionFormDiv", OnSuccess = "populatePermissions" }).ToHtmlString()
                                                          .Replace(this.GetLocalResourceObject("EditPermission").ToString(), "<i class='fa fa-pencil'></i>"))
                                                          .HeaderAttributes(@class => "noSortCol")
                                                          .Named(this.GetLocalResourceObject("Edit").ToString()).Attributes(@class => "halign").DoNotEncode();
                                                  }).Attributes(@class => "celloTable", id => "dataList").Render();
        %>
        <%
                Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "entityPermissionDetails", LoadingElementDuration = 500, LoadingElementId = "loading" }, new
                                                {
                                                    userId = userId,
                                                    entityId = entityId,
                                                    referenceId = referenceId,
                                                    roleId = roleId,
                                                })
       .SetPageNumber(pageNumber).SetTotalCount(totalCount).SetPageSize(pageSize).SetSortValues(sortString, sortDirection)
       .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
       .Render();
        %>
        <%
            }
            else
            {
                       
        %>
        <div class="alert alert-info">
            <%: this.GetGlobalResourceObject("General","NoData") %>
        </div>
        <%
                }
        %>
    </div>
</div>
