﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<section class="panel purple">
    <header class="panel-heading">
        <h4>Manage UEP</h4>
    </header>
    <% using (Ajax.BeginForm("UserEntityPermissionForm", null, new AjaxOptions { HttpMethod = "Post", InsertionMode = InsertionMode.Replace, UpdateTargetId = "UserEntityPermissionFormDiv", OnSuccess = "populatePermissions" }, new { @Id = "UserEntityPermissionForm" }))
       { %>
    <div class="panel-body">
        <div class="form-container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <%: Html.Label(this.GetLocalResourceObject("Entity").ToString())%>
                        <%= Html.DropDownList("EntityList", null, new { style="width:100%;", @onchange = "EntityListOnchange()" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <%: Html.Label(this.GetLocalResourceObject("EntityReference").ToString())%>
                        <%= Html.DropDownList("ReferenceList", null, new { style="width:100%;",@onchange = "ReferenceListOnChange()" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <%: Html.Label(this.GetLocalResourceObject("User").ToString())%>
                        <%: Html.Hidden("UserEntityPermissionId", ViewData["UserEntityPermissionId"])%>
                        <%= Html.DropDownList("UserList", null, new {style="width:100%;", @onchange = "UserListOnChange()" })%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <%: Html.Label(this.GetLocalResourceObject("Role").ToString())%>
                        <%= Html.DropDownList("RoleList", null, new { style="width:100%;",@onchange = "RoleListOnchange()" })%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <%: this.GetLocalResourceObject("Permissions") %>
                        <% if (Convert.ToBoolean(ViewData["ViewPermission"]))
                           {%>
                        <input type="checkbox" id="ViewPermission" name="ViewPermission" checked="checked" />
                        <%}
                           else
                           {%>
                        <input type="checkbox" id="Checkbox1" name="ViewPermission" />
                        <%} %>
                        <label for="ViewPermission">
                            <%: this.GetLocalResourceObject("View").ToString() %></label>

                        <% if (Convert.ToBoolean(ViewData["AddPermission"]))
                           {%>
                        <input type="checkbox" id="AddPermission" name="AddPermission" checked="checked" />
                        <%}
                           else
                           {%>
                        <input type="checkbox" id="Checkbox2" name="AddPermission" />
                        <%} %>
                        <label for="AddPermission">
                            <%: this.GetLocalResourceObject("Add").ToString() %></label>

                        <% if (Convert.ToBoolean(ViewData["EditPermission"]))
                           {%>
                        <input type="checkbox" id="EditPermission" name="EditPermission" checked="checked" />
                        <%}
                           else
                           {%>
                        <input type="checkbox" id="Checkbox3" name="EditPermission" />
                        <%} %>
                        <label for="EditPermission">
                            <%: this.GetLocalResourceObject("Edit").ToString() %></label>

                        <% if (Convert.ToBoolean(ViewData["DeletePermission"]))
                           {%>
                        <input type="checkbox" id="DeletePermission" name="DeletePermission" checked="checked" />
                        <%}
                           else
                           {%>
                        <input type="checkbox" id="Checkbox4" name="DeletePermission" />
                        <%} %>
                        <label for="DeletePermission">
                            <%: this.GetLocalResourceObject("Delete").ToString() %></label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div id="OtherPrivilegeContainer">
                            <div class="privilegeTextboxContainer">
                                <div class="form-group">
                                    <label>
                                        <%: this.GetLocalResourceObject("OtherPrivileges").ToString() %></label>
                                    <div class="input-group">
                                        <input type="text" id="addPrivilegeTextbox" />
                                        <span class="input-group-btn">
                                            <a class="btn btn-info" id="addPrivilegeButton" onclick="addPrivilegeButtonClick()">
                                                <i class="fa fa-plus"></i>&nbsp;<%: this.GetLocalResourceObject("Add").ToString() %>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div id="PrivilegeContainer">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="AddUserEntityPermissionBtn">
                <a class="btn btn-info" id="AddUserEntityPermission" onclick="SubmitForm()">
                    <i class="fa fa-plus"></i>&nbsp;<%: this.GetLocalResourceObject("Add").ToString() %>
                </a>
            </div>
        </div>
    </div>
    <% } %>
</section>
<script type="text/javascript">
    function SubmitForm() {
        $.post($("#UserEntityPermissionForm").attr('action'), $("#UserEntityPermissionForm").serialize(), function (data) {
            $("#UserEntityPermissionFormDiv").html(data);
        }).error(function () {
            $("#UserEntityPermissionFormDiv").html('<div class="error"><%: this.GetLocalResourceObject("Error").ToString() %></div>');
        });
        return false;
    }

    populatePermissions();
    populateGrid();
    function privilegeClick(className) {
        if (!$('input:checkbox.' + className).is(":checked")) {
            $('input:hidden.' + className).val('');
        } else {
            $('input:hidden.' + className).val($('span.' + className).html());
        }
    }

    function UserListOnChange() {
        $("#RoleList").val("");
        if (!$("#EntityList option:selected").val() == "") {
            populatePermissions($("#UserList option:selected").val(), $("#EntityList option:selected").val(), $("#ReferenceList option:selected").val(), "");
        }
        populateGrid();
    }

    function RoleListOnchange() {
        $("#UserList").val("");
        if (!$("#RoleList option:selected").val() == "" && !$("#EntityList option:selected").val() == "" && !$("#ReferenceList option:selected").val() == "") {
            populatePermissions($("#UserList option:selected").val(), $("#EntityList option:selected").val(), $("#ReferenceList option:selected").val(), $("#RoleList option:selected").val());
        }
        populateGrid();
    }

    function EntityListOnchange() {
        var userId = $("#UserList option:selected").val();
        var roleId = $("#RoleList option:selected").val();
        var entityId = $('#EntityList option:selected').val();
        var referenceId = "";
        $("#ReferenceList").val('');
        $.ajax({
            url: '/UserEntityPermission/GetUserReferenceDetails',
            type: 'POST',
            data: { 'entityId': entityId },
            beforeSend: function (xhr) {
                $('#loading').show();
            },
            success: function (data) {
                $('#ReferenceList').empty();

                if (data || data == null) {
                    $('#ReferenceList').append('<option value="">' + "Select Reference" + '</option>');
                    $.each(data, function (primaryKey, displyName) { $('#ReferenceList').append('<option value="' + primaryKey + '">' + displyName + '</option>'); });
                } else {
                    $('#ReferenceList').append('<option value="">' + "No Reference found" + '</option>');
                }
            },
            complete: function (data) {
                $('#loading').hide();
            }
        });

        if (entityId == "") {
            userId = "";
            roleId = "";
            referenceId = "";
        }
        populatePermissions(userId, entityId, referenceId, roleId);
        populateGrid();
    }

    function ReferenceListOnChange() {
        var userId = $("#UserList option:selected").val();
        var roleId = $("#RoleList option:selected").val();
        var entityId = $('#EntityList option:selected').val();
        var referenceId = '';

        if (!entityId == "") {
            populatePermissions(userId, entityId, referenceId, roleId);
        }
    }

    function setPermissionsFalse() {
        $("#ViewPermission").attr('checked', false);
        $("#AddPermission").attr('checked', false);
        $("#EditPermission").attr('checked', false);
        $("#DeletePermission").attr('checked', false);
    }

    function populateGrid() {
        var userId = $("#UserList option:selected").val();
        var roleId = $("#RoleList option:selected").val();
        var entityId = $('#EntityList option:selected').val();
        var referenceId = '';

        if ($('#EntityList option:selected').val() == "") {
            userId = "";
            roleId = "";
            entityId = "";
        }

        $.ajax({
            url: '/UserEntityPermission/UserEntityPermissionDetails',
            type: 'POST',
            data: { 'userId': userId, 'entityId': entityId, 'referenceId': referenceId, 'roleId': roleId },
            beforeSend: function (xhr) { },
            success: function (data) {
                $('#entityPermissionDetails').empty();
                $('#entityPermissionDetails').hide().html(data).slideDown();
            },
            complete: function (data) { }
        });

        if ((userId != "" || roleId != "" || (userId == "" || roleId == "")) && entityId != "" && referenceId != "") {
            $('#AddUserEntityPermissionBtn').fadeIn();
        } else {
            $('#AddUserEntityPermissionBtn').fadeOut();
        }
    }
</script>
