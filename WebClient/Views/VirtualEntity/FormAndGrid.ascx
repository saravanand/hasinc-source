﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="CelloSaaS.Model.VirtualEntityManagement" %>
<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% if (TempData["Info"] != null)
   { %>
<div class="alert alert-info">
    <%:TempData["Info"]%></div>
<% TempData["Info"] = null;
   }%>
<% if (TempData["Success"] != null)
   { %>
<div class="alert alert-success">
    <%:TempData["Success"] %></div>
<% } %>
<% if (TempData["Error"] != null)
   { %>
<div class="alert alert-danger">
    <%:TempData["Error"] %>
    <%TempData["Error"] = string.Empty; %>
</div>
<% } %>
<% if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
   { %>
<div class="alert alert-danger">
    <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
</div>
<%}%>
<div id="divForm">
    <%  if ((ViewData["EntityId"] != null && !string.IsNullOrEmpty(ViewData["EntityId"].ToString()) && ViewData["TenantId"] != null && !string.IsNullOrEmpty(ViewData["TenantId"].ToString())))
        {
            if (ViewData["FormMode"] == null)
            {
                ViewData["FormMode"] = CurrentFormMode.Insert;
            }
            CurrentFormMode currentMode = new CurrentFormMode();
            currentMode = (CurrentFormMode)ViewData["FormMode"];
            IAccessControlService accessControlService = (IAccessControlService)ServiceLocator.GetServiceImplementation(typeof(IAccessControlService));
            if (currentMode == CurrentFormMode.Insert)
            {
                VirtualEntity virtualEntity = new VirtualEntity(ViewData["EntityId"].ToString());
                virtualEntity.TenantId = ViewData["TenantId"].ToString();
                virtualEntity.Identifier = string.Empty;
                virtualEntity.ExtendedRow = new ExtendedEntityRow();
                ViewData["VirtualEntity"] = virtualEntity;
                if (accessControlService.CheckPrivilege(EntityPrivilegePrefix.AddPrefix + ViewData["EntityId"].ToString()))
                {
                    Html.RenderPartial("ManageVirtualEntityData", (VirtualEntity)ViewData["VirtualEntity"], ViewData);
                }
            }
            else if (currentMode == CurrentFormMode.Edit)
            {
                if (accessControlService.CheckPrivilege(EntityPrivilegePrefix.EditPrefix + ViewData["EntityId"].ToString()) || !string.IsNullOrEmpty(Html.CelloValidationSummary()))
                {
                    Html.RenderPartial("ManageVirtualEntityData", (VirtualEntity)ViewData["VirtualEntity"], ViewData);
                }
            }
        }
    %>
</div>
<div id="divGrid">
    <% Html.RenderPartial("VEGrid", ViewData["VirtualEntities"], ViewData); %>
</div>
