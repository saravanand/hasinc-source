﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VirtualEntity>" %>
<%@ Import Namespace="CelloSaaS.Library" %>
<%@ Import Namespace="CelloSaaS.Model.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.Model.VirtualEntityManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.DataManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.ViewManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% AjaxOptions ajaxOption = new AjaxOptions();
   ajaxOption.UpdateTargetId = "divGridForm";
   ajaxOption.LoadingElementId = "loading";
   using (Ajax.BeginForm("Manage", "VirtualEntity", new { }, ajaxOption, new { id = "SaveVirtualEntityPage" }))
   {
       if (this.Model != null && ViewData["FormDataViewId"] != null && !string.IsNullOrEmpty(ViewData["FormDataViewId"].ToString())
     && ViewData["FormDataViewName"] != null && !string.IsNullOrEmpty(ViewData["FormDataViewName"].ToString()))
       {
           bool canAdd = false;
           bool canEdit = false;
           var fieldMetaData = ViewMetaDataProxy.GetDataViewMetaData(UserIdentity.TenantID, ViewData["FormDataViewId"].ToString(), this.Model.EntityIdentifier);
           if (fieldMetaData != null && fieldMetaData.AvailableFields != null && fieldMetaData.AvailableFields.Count > 0 && fieldMetaData.AvailableFields.Any(x => x.Value.IsVisible))
           {
               CurrentFormMode currentMode = new CurrentFormMode();
               if (ViewData["FormMode"] != null)
               {
                   currentMode = (CurrentFormMode)ViewData["FormMode"];
               }

               var columnAttribute = new ColumnProperties();
               columnAttribute.LabelContentProperties = new Dictionary<string, string>();
               columnAttribute.FieldContentProperties = new Dictionary<string, string>();

               Dictionary<int, ColumnProperties> columnAttributes = new Dictionary<int, ColumnProperties>();
               columnAttributes.Add(1, columnAttribute);
               columnAttributes.Add(2, columnAttribute);

               CelloSaaS.View.AlternateRowStyle alternateRowStyle = new CelloSaaS.View.AlternateRowStyle();

               Dictionary<string, string> formAttributes = new Dictionary<string, string>();
               formAttributes.Add("id", "frmAdd");
               CelloSaaS.View.DataViewTable frmTable = null;

               if (ViewData["FormDataViewId"] != null && !string.IsNullOrEmpty(ViewData["FormDataViewId"].ToString())
                  && ViewData["FormDataViewName"] != null && !string.IsNullOrEmpty(ViewData["FormDataViewName"].ToString()))
               {
                   canAdd = UserIdentity.HasPrivilege(EntityPrivilegePrefix.AddPrefix + ViewData["EntityId"].ToString());
                   canEdit = (UserIdentity.HasPrivilege(EntityPrivilegePrefix.EditPrefix + ViewData["EntityId"].ToString()) || !string.IsNullOrEmpty(Html.CelloValidationSummary()));
%>
<section class="panel indigo" id="virtualEntityFormDiv">
    <header class="panel-heading">
        <h4>
            <%:   ViewData["FormDataViewName"]%>
        </h4>
    </header>
    <div class="panel-body">
        <div class="form-container">
            <%= Html.Hidden("EntityIdentifier", this.Model.EntityIdentifier)%>
            <%if (ViewData["Identifier"] != null && !string.IsNullOrEmpty(ViewData["Identifier"].ToString()))
              {%>
            <%= Html.Hidden("Identifier", ViewData["Identifier"])%>
            <%}
              else
              {%>
            <%= Html.Hidden("Identifier", this.Model.Identifier)%>
            <%} %>
            <%
                   try
                   {
                       frmTable = new CelloSaaS.View.DataViewTable(ViewData["FormDataViewId"].ToString(), currentMode,
                       formAttributes, this.Html, 2, columnAttributes, alternateRowStyle, this.Model, this.Model.EntityIdentifier,
                       new FormAttributes() { ValidationSummaryMessage = "SummaryMessage" }, this.Model.TenantId);
                       string table = frmTable.Render();
                       Response.Write(table);
                   }
                   catch { } 
            %>
            <div class="pull-right">
                <a class="btn btn-info" href="#" title="<%: this.GetLocalResourceObject("lbl_SaveVirtualEntityDetails") %>"
                    onclick="SubmitVirtualEntityDetails();">
                    <%=this.GetGlobalResourceObject("General","Save") %>
                </a>
                <a class="btn btn-default" href="#" onclick="Clear();" title="<%: this.GetLocalResourceObject("lbl_BacktoVirtualEntityList") %>">
                    <%=this.GetGlobalResourceObject("General","Cancel") %>
                </a>
            </div>
        </div>
    </div>
</section>
<%
               }
%>

<% 
           }
           else
           { 
%>
<div class="alert alert-info">
    <%: this.GetGlobalResourceObject("VirtualEntityResource", "m_NoExtnData")%>
</div>
<% 
           }
       }
   } 
%>
<script type="text/javascript">
    $(function () {
        $('#virtualEntityFormDiv select').each(function () {
            $(this).attr('style', 'width:100%;');
        });
        $('#virtualEntityFormDiv select').select2();
    });
</script>
