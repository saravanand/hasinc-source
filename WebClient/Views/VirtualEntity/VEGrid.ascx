﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<VirtualEntity>>" %>
<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="CelloSaaS.Model.VirtualEntityManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceContracts.AccessControlManagement" %>
<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<%@ Import Namespace="CelloSaaS.ServiceProxies.ViewManagement" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<%  if (ViewData["EntityId"] != null && !string.IsNullOrEmpty(ViewData["EntityId"].ToString()) && ViewData["TenantId"] != null && !string.IsNullOrEmpty(ViewData["TenantId"].ToString()) && ViewData["GridDataViewId"] != null && !string.IsNullOrEmpty(ViewData["GridDataViewId"].ToString()))
    {
        var fieldMetaData = ViewMetaDataProxy.GetDataViewMetaData(UserIdentity.TenantID, ViewData["GridDataViewId"].ToString(), ViewData["EntityId"].ToString());
        if (fieldMetaData != null && fieldMetaData.AvailableFields != null && fieldMetaData.AvailableFields.Count > 0 && fieldMetaData.AvailableFields.Any(x => x.Value.IsVisible))
        {

            if (ViewData["VirtualEntities"] != null && ((IEnumerable<VirtualEntity>)ViewData["VirtualEntities"]).Count() > 0)
            {
                bool canDelete = false;
                bool canEdit = false;
                if (UserIdentity.HasPrivilege(EntityPrivilegePrefix.DeletePrefix + ViewData["EntityId"].ToString()))
                {
                    canDelete = true;
                }

                if (UserIdentity.HasPrivilege(EntityPrivilegePrefix.EditPrefix + ViewData["EntityId"].ToString()))
                {
                    canEdit = true;
                }
                   
%>
<div class="grid simple">
    <div class="grid-title">
        <div class="row">
            <div class="col-md-6">
                <h4>
                    <%:ViewData["GridDataViewName"]%>
                </h4>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-body">
        <%    string DataViewId = ViewData["GridDataViewId"].ToString();
              Html.CelloGrid<VirtualEntity>((IEnumerable<VirtualEntity>)ViewData["VirtualEntities"], DataViewId, ViewData["TenantId"].ToString()).CelloColumns(
             column =>
             {
                 if (canEdit)
                 {
                     column.ForColumn(col => ("<a href='#' id='Manage' onclick =ManageVirtualEntityData('" + col.EntityIdentifier + "','" + col.Identifier + "','" + DataViewId + "') ><i class='fa fa-pencil'></i></a>"))
                           .Named(this.GetGlobalResourceObject("General", "Edit").ToString())
                           .HeaderAttributes(@class => "halign noSortCol", style => "width:50px;")
                           .Attributes(@class => "halign")
                           .DoNotEncode();
                 }

                 if (canDelete)
                 {
                     column.ForColumn(col => ("<a href='#' id='Delete' onclick=DeleteVirtualEntityData('" + col.EntityIdentifier + "','" + col.Identifier + "','" + col.TenantId + "','" + DataViewId + "') ><i class='fa fa-trash-o'></i></a>"))
                            .Named(this.GetGlobalResourceObject("General", "Delete").ToString())
                            .HeaderAttributes(@class => "halign noSortCol", style => "width:50px;")
                            .Attributes(@class => "halign")
                            .DoNotEncode();
                 }
             }).Attributes(id => "dataList", @class => "celloTable").Render();
               }
               else
               {%>
        <div class="alert alert-info">
            <%: this.GetGlobalResourceObject("General", "m_NoData")%>
        </div>
        <%
               }
           }
       }
        %>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        jQDataTable();

        $('#searchText').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                DoSearch();
                e.preventDefault();
            }
        });
    });
    function jQDataTable() {
        var noSortColumns = $("th.noSortCol");
        var noSortIndexArray = new Array();
        if (noSortColumns.length > 0) {
            noSortColumns.each(function () {
                var indexVal = noSortColumns.parent("tr").children().index(this);
                noSortIndexArray.push(indexVal);
            });
        }

        filterTable = $('table#dataList').dataTable({
            "iDisplayLength": 10,
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bRetrieve": true,
            "bStateSave": true,
            "aoColumnDefs": [{ "bSearchable": false, "bSortable": false, "aTargets": noSortIndexArray }]
        });
    }

    function DoSearch() {
        var searchText = $('#searchText').val();
        if (searchText == 'Search') {
            searchText = '';
        }
        filterTable.fnFilter(searchText);
        return false;
    }
</script>
