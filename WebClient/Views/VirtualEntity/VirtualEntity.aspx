﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Title="<%$Resources:VirtualEntityManagement%>"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<%@ Import Namespace="CelloSaaS.Model.DataManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">   
    <div id="loading">
    </div>
    <div class="row-fluid pd-25" id="divGridForm" style="margin-top:20px;">
        <%  Html.RenderPartial("FormAndGrid");%>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/jscript">
        function SubmitVirtualEntityDetails() {
            var postdata = $('form#SaveVirtualEntityPage').serialize();
            $.post('<%=Url.Action("Manage") %>', postdata, function (data) {
                $('#divGridForm').html(data);
                jQDataTable();
            });
        }

        function DeleteVirtualEntityData(entityId, referenceId, tenantId, dataViewId) {
            if (confirm("<%=this.GetLocalResourceObject("m_DeleteData") %>")) {
                $.ajax({
                    url: '/VirtualEntity/DeleteVirtualEntityData',
                    type: 'POST',
                    data: { 'entityId': entityId, 'referenceId': referenceId, 'tenantId': tenantId, 'dataViewId': dataViewId },
                    beforeSend: function (xhr) {
                        $('#loading').show();
                    },
                    success: function (data) {
                        $('#divGridForm').html(data);
                        jQDataTable();
                    },
                    complete: function (data) {
                        $('#loading').hide();
                    }
                });
            }
        }

        function ManageVirtualEntityData(entityId, referenceId, dataViewId) {
            $.ajax({
                url: '/VirtualEntity/ManageVirtualEntityData',
                type: 'POST',
                data: { 'entityId': entityId, 'referenceId': referenceId, 'dataViewId': dataViewId },
                beforeSend: function (xhr) {
                    $('#loading').show();
                },
                success: function (data) {
                    $('#divGridForm').html(data);
                    jQDataTable();
                },
                complete: function (data) {
                    $('#loading').hide();
                }
            });
        }

        function Clear() {
            $('#divGridForm').empty();
            location.reload();
        }

        function GetVirtualEntityDataViews(entityId, entityName) {
            $.ajax({
                url: '/VirtualEntity/GetVirtualEntityDataViews',
                type: 'POST',
                data: { 'entityId': entityId, 'entityName': entityName },
                beforeSend: function (xhr) {
                    $('#loading').show();
                },

                success: function (data) {
                    if (data || data == null) {
                        $('#divGridForm').html(data);
                        jQDataTable();
                    }
                },
                complete: function (data) {
                    $('#loading').hide();
                }
            });
        }
    </script>
</asp:Content>
