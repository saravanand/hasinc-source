﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowModel>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #searchOptions {
            font-size: 11px;
        }

        .Started, .Routed, .Executed {
            background-color: #FFD698 !important;
        }

        .Completed {
            background-color: #E0F9D4 !important;
        }

        .Errored {
            background-color: #F66666 !important;
        }

        #loadingDiv {
            visibility: hidden;
        }

        .ldRefreshStep {
            height: inherit;
            width: inherit;
            background-color: #F8F7F4;
        }

        .ddlWidth select {
            font-size: 11px;
            width: 175px;
        }

        .txtWidth input[name=MapId] {
            width: 175px;
            font-size: 11px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#wfDashboard').submit(function () {
                $('#divResults').fadeOut().empty();
                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    $('#divResults').hide().html(data).slideDown('normal');
                }).error(function () {
                    var data = '<div class="error"><%:this.GetLocalResourceObject("SearchError")  %></div>';
                    $('#divResults').hide().html(data).slideDown('normal');
                });
                return false;
            });

            $('a.axLink').on('click', function () {
                var url = $(this).attr('href');
                var $tdelm = $(this).parent('td');
                $tdelm.append('<div class="ldRefreshStep"><img src="../../App_Themes/CelloSkin/loading.gif" /></div>');
                $.post(url, null, function (data) {
                    var result = JSON.parse(data);
                    if (result.Error !== undefined) {
                        alert(result.Error);
                    }
                    if (result.Success !== undefined) {
                        alert(result.Success);
                    }
                    $('.ldRefreshStep', $tdelm).remove();
                });
                $('.ldRefreshStep', $tdelm).remove();
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                           
                var postdata = "?" + $('#wfDashboard').serialize();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();               
                
                $.post('<%=Url.Action("Index","WFDashboard") %>' + postdata, null, function (data) {
                    $('#divResults').html(data);
                });
            });

        });

        function showLoading() { $('#loading').show(); }
        function hideLoading() { $('#loading').hide(); }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("Title")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <% int count = ViewData["Workflows"] != null ? (int)ViewData["Workflows"] : 0;
           if (count > 0)
           {
               if (!string.IsNullOrEmpty(Html.CelloValidationMessage("WorkFlowDashBoardError")))
               { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationMessage("WorkFlowDashBoardError")%>
        </div>
        <% } %>
        <div class="secondary_search_container content-box" style="position: relative;">
            <% using (Html.BeginForm("Index", "WFDashboard", FormMethod.Post, new { @id = "wfDashboard", @class = "form-inline" }))
               {
            %>
            <div class="form-group">
                <%:this.GetLocalResourceObject("WorkflowName")%><%=this.GetGlobalResourceObject("General", "Mandatory")%>
                <%= Html.DropDownList("WorkflowName", (IEnumerable<SelectListItem>)ViewData["WorkflowName"], "---Select Workflow---", new { style="width:220px;" })%>
            </div>
            <div class="form-group">
                <%:this.GetLocalResourceObject("Status")%>:
            <%= Html.DropDownList("WfInstanceStatus")%>
            </div>
            <div class="form-group">
                <%:this.GetLocalResourceObject("MapId")%>:
            <%= Html.TextBox("MapId")%>
            </div>
            <div class="form-group">
                <a class="btn btn-info" href="#" onclick="$('form').submit();return false;" title="<%:this.GetGlobalResourceObject("General","Search") %>">
                    <%:this.GetGlobalResourceObject("General", "Search")%></a>
            </div>
            <% } %>
        </div>
        <div id="divResults">
        </div>
        <%}
           else
           { %>
        <div class="alert alert-info">
            <%: this.GetLocalResourceObject("NoWorkflowsFound")%>
        </div>
        <%} %>
    </div>
</asp:Content>
