﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowInstance>>" %>
<%@ Import Namespace="CelloSaaS.WorkFlow" %>
<%@ Import Namespace="CelloSaaS.WorkFlow.Model" %>
<% 
    Html.EnableUnobtrusiveJavaScript(false);
    Html.EnableClientValidation(false);
%>
<div class="grid simple">
    <div class="grid-body pd-0">
        <%
            string WorkflowName = ViewData["WorkflowName"] != null ? ViewData["WorkflowName"].ToString() : null;
            string WfInstanceStatus = ViewData["WfInstanceStatus"] != null ? ViewData["WfInstanceStatus"].ToString() : string.Empty;
            string MapId = ViewData["MapId"] != null ? ViewData["MapId"].ToString() : string.Empty;
            int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
            int totalCount = ViewData["PageNumber"] != null ? (int)ViewData["TotalCount"] : 0;
            int pageSize = ViewData["PageNumber"] != null ? (int)ViewData["PageSize"] : 0;

            if (!string.IsNullOrEmpty(Html.CelloValidationSummary()))
            { %>
        <div class="alert alert-danger">
            <%=Html.CelloValidationSummary(this.GetGlobalResourceObject("ErrorMessage", "ErrorTitle").ToString())%>
        </div>
        <% }
            else
            { %>
        <% if (this.Model != null && this.Model.Count() > 0)
           { %>
        <div style="width: auto; overflow: auto;">
            <table id="wfDashboardResult" class="celloTable">
                <thead>
                    <tr>
                        <th><%= this.GetLocalResourceObject("Date")%>
                        </th>
                        <th>
                            <%= this.GetLocalResourceObject("MapId")%>
                        </th>
                        <%
               int columnCount = this.Model.Max(x => x.WorkflowDefinition.WorkflowItemDefinitions.Count(y => y is TaskDefinition));

               for (int i = 1; i <= columnCount; i++)
               {
                        %>
                        <th style="min-width: 50px;">
                            <%=  GetLocalResourceObject("Step") + " " + i.ToString()%>
                        </th>
                        <%
               }
                        %>
                    </tr>
                </thead>
                <tbody>
                    <% 
               foreach (var wfInstance in this.Model.OrderByDescending(x => x.CreatedOn))
               { 
                    %>
                    <tr>
                        <td style="width: 150px;">
                            <%= wfInstance.CreatedOn.ToUIDateTimeString()%>
                        </td>
                        <td style="width: 150px;">
                            <%= wfInstance.MapId  %>
                        </td>
                        <% 
                   var abc = wfInstance.WorkflowDefinition.WorkflowItemDefinitions.Where(x => x is CelloSaaS.WorkFlow.Model.TaskDefinition);
                   IEnumerable<TaskDefinition> def = from a in abc select ((TaskDefinition)a);
                   var ghi = def.OrderBy(p => p.Ordinal);

                   foreach (var taskDef in ghi)
                   {
                       string statusCssClass = string.Empty;

                       if (wfInstance.StartedTasks != null)
                       {
                           var temp = wfInstance.StartedTasks.Where(X => X.TaskDefinition.TaskId == taskDef.TaskId);
                           statusCssClass = temp.Count() > 0 ? temp.First().CurrentExecutionStatus : string.Empty;
                       }

                       if (wfInstance.RoutedTasks != null && string.IsNullOrEmpty(statusCssClass))
                       {
                           var temp = wfInstance.RoutedTasks.Where(X => X.TaskDefinition.TaskId == taskDef.TaskId);
                           statusCssClass = temp.Count() > 0 ? temp.First().CurrentExecutionStatus : string.Empty;
                       }

                       if (wfInstance.CompletedTasks != null && string.IsNullOrEmpty(statusCssClass))
                       {
                           var temp = wfInstance.CompletedTasks.Where(X => X.TaskDefinition.TaskId == taskDef.TaskId);
                           statusCssClass = temp.Count() > 0 ? temp.First().CurrentExecutionStatus : string.Empty;
                       }

                       if (wfInstance.ActiveTasks != null && string.IsNullOrEmpty(statusCssClass))
                       {
                           var temp = wfInstance.ActiveTasks.Where(X => X.TaskDefinition.TaskId == taskDef.TaskId);
                           statusCssClass = temp.Count() > 0 ? temp.First().CurrentExecutionStatus : string.Empty;
                       }
                        %>
                        <td class="<%= statusCssClass %>">
                            <%= taskDef.TaskName%>
                            <% if (statusCssClass == "Errored")
                               {
                                   var taskRunDetails = wfInstance.ActiveTasks.Where(x => x.Task.TaskId.Equals(taskDef.TaskId)).FirstOrDefault();
                                   if (taskRunDetails != null && taskRunDetails.Id != null && taskRunDetails.Id != Guid.Empty)
                                   {%>
                            <a class="thickbox" title="<%:this.GetLocalResourceObject("t_ExceptionDetails")%>" href="ShowException?weight=200&height=300&taskRunId=<%= taskRunDetails.Id.ToString() %>">
                                <i class="fa fa-search"></i>
                            </a>
                            <%                       }
                                   //AJax Call Link/Image %>
                            <a class="axLink" href="<%=Url.Action("RestartStep", new { wfId = wfInstance.WorkflowDefinition.WfId,wfInstanceId=wfInstance.Id,taskCode=taskDef.TaskCode })%>"
                                title="<%:this.GetLocalResourceObject("ClicktoReStartthisStep")%>">
                                <i class="fa fa-refresh"></i>
                            </a>
                            <% } %>
                        </td>
                        <% } %>
                    </tr>
                    <% } %>
                </tbody>
            </table>
            <%
               Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divResults", LoadingElementDuration = 500, LoadingElementId = "loading" }, new
                                                                {
                                                                    WorkflowName,
                                                                    WfInstanceStatus,
                                                                    MapId
                                                                })
                       .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
                       .Render();
            %>
        </div>
        <br />
        <br />
        <table class="celloTable">
            <thead>
                <tr>
                    <th>
                        <%:this.GetLocalResourceObject("Legend")%>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="border: 0px; width: 80px;">
                                    <span style="color: Red; font-size: 12px;">*&nbsp;</span>
                                    <%:this.GetLocalResourceObject("Mandatory")%>
                                </td>
                                <td style="border: 0px; width: 100px;">
                                    <div style="width: 15px; height: 15px; margin-right: 5px; float: left;" class="Completed">
                                    </div>
                                    <%:this.GetLocalResourceObject("Completed")%>
                                </td>
                                <td style="border: 0px; width: 100px;">
                                    <div style="width: 15px; height: 15px; margin-right: 5px; float: left;" class="Started">
                                    </div>
                                    <%:this.GetLocalResourceObject("Progress")%>
                                </td>
                                <td style="border: 0px; width: 100px;">
                                    <div style="width: 15px; height: 15px; margin-right: 5px; float: left;" class="Errored">
                                    </div>
                                    <%:this.GetLocalResourceObject("Errored")%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <% }
           else
           { %>
        <div class="alert alert-info">
            <%=this.GetGlobalResourceObject("General", "NoData")%>
        </div>
        <% }
            } %>
    </div>
</div>
