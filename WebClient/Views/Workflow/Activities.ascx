﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CelloSaaS.WorkFlow.Model.ActivityDetails>>" %>
<%@ Import Namespace="CelloSaaS.WorkFlow.Model" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="row">
    <%if (Model != null || Model.Count > 0)
      {
          foreach (ActivityDetails activity in Model)
          {%>
    <div class="col-md-12 form-container">
        <h4>
            <%:activity.Name%></h4>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("l_Description")%>
            </label>
            <p>
                <%:activity.Description%>
            </p>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("l_TypeName")%>
            </label>
            <p>
                <%:activity.TypeName%>
            </p>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("l_AssemblyName")%>
            </label>
            <p>
                <%:activity.AssemblyName%>
            </p>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("l_Outcomes")%>
            </label>
            <p>
                <% if (activity.Outcomes != null)
                   { %>
                <%: string.Join(",", activity.Outcomes)%>
                <% }
                   else
                   {%>
            -
            <%}%>
            </p>
        </div>
        <div class="form-group">
            <label>
                <%:this.GetLocalResourceObject("l_Properties")%>
            </label>
            <p>
                <% if (activity.KeyValues != null)
                   { %>
                <%: string.Join(",", activity.KeyValues.ToList().Select(x => x.Split(':')[0].ToString()).ToList())%>
                <% }
                   else
                   {%>
            -
            <%}%>
            </p>
        </div>
        <hr />
    </div>
    <% } %>
    <% }
      else
      { %>
    <div class="alert alert-info">
        <%:this.GetLocalResourceObject("e_ActivityNotAvailable")%>
    </div>
    <% } %>
</div>
