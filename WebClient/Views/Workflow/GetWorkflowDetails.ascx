﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div id="wfFrmDiv">
    <form id="editwfform" name="editwfform" action="<%=Url.Action("SaveWorkflow") %>">
        <% Html.RenderPartial("ManageWorkflow"); %>
    </form>
</div>
<a class="btn btn-info" href="#" id="btnSave" title="<%:this.GetGlobalResourceObject("General","Save") %>">
    <%:this.GetGlobalResourceObject("General","Save") %></a>
<script type="text/javascript">
    $(function () {
        $('#btnSave').click(function () {
            var wfName = $('form#editwfform input[name=Name]').val();

            if (window.xsleditor) {
                window.xsleditor.save();
            }

            var postdata = $('form#editwfform').serialize();

            $.post($('form#editwfform').attr('action'), postdata, function (data) {
                if (data && data.Success) {
                    $("#workflowListPanel").jstree('rename_node', null, wfName);
                    ShowSuccess('Workflow details updated successfully!');
                } else {
                    $('#wfFrmDiv').html(data);
                }
            });

            return false;
        });
    });
</script>
