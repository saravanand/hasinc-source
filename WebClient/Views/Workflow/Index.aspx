﻿<%@ Page Title="<%$ Resources:Title %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%:this.GetLocalResourceObject("ManageWorkflow")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="row">
            <div class="col-md-3">
                <div id="workflowActionPanel" class="grid simple">
                    <div class="grid-title">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="workflowTreeSearchbox" name="workflowTreeSearchbox" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();"><%=this.GetGlobalResourceObject("General","GO") %>!</button>
                            </span>
                        </div>
                    </div>
                    <div class="grid-body" style="padding: 10px 0; overflow-x: auto; min-height: 300px;">
                        <div id="workflowListPanel">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 pd-0">
                <div id="statusMessage">
                </div>
                <section id="rightPanel" class="panel">
                    <header class="panel-heading bg-info">
                        <ul class="nav nav-tabs nav-justified captilize">
                            <li class="active"><a data-toggle="tab" href="#workflowConfigurePanel" title="<%:this.GetLocalResourceObject("WorkflowConfiguration")%>">
                                <%:this.GetLocalResourceObject("WorkflowDetails")%></a></li>
                            <li><a data-toggle="tab" href="#workflowDesignerPanel" title="<%:this.GetLocalResourceObject("WorkflowDesignConfiguration")%>">
                                <%:this.GetLocalResourceObject("Design")%></a></li>
                            <li><a data-toggle="tab" href="#workflowActivityPanel" title="<%:this.GetLocalResourceObject("Workflow Activities")%>">
                                <%:this.GetLocalResourceObject("Activities")%></a></li>
                        </ul>
                    </header>
                    <div class="panel-body pd-0">
                        <div class="tab-content" style="overflow: visible;">
                            <div id="workflowConfigurePanel" class="tab-pane fade in active">
                                <div class="alert alert-info">
                                    <%:this.GetLocalResourceObject("e_SelectWorkflow")%>
                                </div>
                            </div>
                            <div id="workflowDesignerPanel" class="tab-pane fade">
                            </div>
                            <div id="workflowActivityPanel" class="tab-pane fade">
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="modal fade" id="AddWorkflow" tabindex="-1" role="dialog" aria-labelledby="AddWorkflow" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 700px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title"><%:this.GetLocalResourceObject("WorkflowDetails")%></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><%=this.GetGlobalResourceObject("General","Close") %></button>
                    <button type="button" id="btnSaveWorkflow" class="btn btn-primary"><%=this.GetGlobalResourceObject("General","Save") %></button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        li.addrule ins, li.deleterule ins {
            background-position: center bottom !important;
        }

        .stackbox {
            border: 1px solid #CCCCCC;
            margin: 10px auto;
            padding: 3px 5px;
            position: relative;
            width: 100%;
            display: inline-block;
        }

            .stackbox h4 {
                color: #5F92A7;
                margin-bottom: 0.5em;
                text-decoration: underline;
            }

            .stackbox label {
                clear: left;
                float: left;
                margin-right: 10px;
            }

            .stackbox > span {
                float: left;
            }

            .stackbox .actionlinks {
                position: absolute;
                right: 5px;
                top: 3px;
                width: auto;
            }
    </style>
    <script type="text/javascript" src="/Scripts/jquery.jstree.js"></script>
    <script type="text/javascript">
        function reload() {
            window.location.reload(true);
        }

        function DoSearch() {
            $("#workflowListPanel").jstree("deselect_all");
            var val = $('#workflowTreeSearchbox').val();

            if (val && val != '') {
                $("#workflowListPanel").jstree('search', val);
            } else {
                $("#workflowListPanel").jstree('clear_search');
            }

            return false;
        }

        function LoadWorkflowDetails(workflowId, filterId) {
            $.get('<%=Url.Action("Index","WorkflowDesigner") %>', { 'workflowId': workflowId, 'filterId': filterId }, function (html) {
                if (html) {
                    $("#workflowDesignerPanel").html(html);
                }
            });
        }

        $(function () {
            $('#workflowTreeSearchbox').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });

            var selectNodes = [];
            var selectNodeId = '<%= (ViewData["SelectWorkflowId"] == null ? "" : ViewData["SelectWorkflowId"] )%>';

            if (selectNodeId && selectNodeId != '') {
                selectNodes.push(selectNodeId);
            }

            LoadWorkflowTreeView();

            function LoadWorkflowTreeView() {
                $("#workflowListPanel")
                .bind("loaded.jstree", function (event, data) {
                    $("#workflowListPanel").jstree("open_all", -1, true);
                    if (selectNodeId && selectNodeId != '' && $('#' + selectNodeId).length > 0) {
                        setTimeout(function () {
                            $("#workflowListPanel").jstree("deselect_all");
                            $("#workflowListPanel").jstree("select_node", $('#' + selectNodeId), true);
                        }, 500);
                    }
                })
                .bind("select_node.jstree", function (event, data) {
                    if (!data) return;
                    var el = data.rslt.obj;

                    if ((el.hasClass('category') || el.hasClass('root'))) {
                        // don't do anything
                    } else {
                        //load the rule config page
                        var categoryName = data.inst._get_parent(el).attr('id');
                        var workflowId = el.attr('id');
                        if (!workflowId)
                            return;

                        $.get('<%=Url.Action("GetWorkflowDetails","Workflow") %>', { 'wfId': workflowId }, function (html) {
                            if (html) {
                                $("#workflowConfigurePanel").html(html);
                                window.xsleditor = CodeMirror.fromTextArea(document.getElementById("XmlTransform"), {
                                    mode: { name: "xml", alignCDATA: true },
                                    lineNumbers: true,
                                    lineWrapping: true,
                                    autoCloseTags: true
                                });
                                window.xsleditor.on("cursorActivity", function () {
                                    window.xsleditor.matchHighlight("CodeMirror-matchhighlight");
                                });
                            }
                        });

                        $.get('<%=Url.Action("Index","WorkflowDesigner") %>', { 'workflowId': workflowId, 'filterId': '' }, function (html) {
                            if (html) {
                                $("#workflowDesignerPanel").html(html);
                            }
                        });

                        $.get('<%=Url.Action("Activities","Workflow") %>', { 'workflowId': workflowId }, function (html) {
                            if (html) {
                                $("#workflowActivityPanel").html(html);
                            }
                        });
                    }
                })
	            .jstree({
	                core: {
	                    /* core options go here */
	                    "animation": 100,
	                    "initially_select": selectNodes
	                },
	                plugins: ["themes", "json_data", "search", "ui", "crrm", "contextmenu", "cookies"],
	                "search": {
	                    "show_only_matches": true
	                },
	                "ui": {
	                    "select_limit": 1
	                },
	                "json_data": {
	                    "ajax": {
	                        "url": '<%=Url.Action("GetAllWorkflow","Workflow") %>',
	                        "data": function (n) {
	                            return {
	                                "id": n.attr ? n.attr("id").replace("node_", "") : 1
	                            }
	                        }
	                    }
	                },
	                "contextmenu": {
	                    "select_node": true,
	                    "items": getContextMenu
	                },
	                "types": {
	                    "valid_children": ["root"],
	                    "type_attr": "rel",
	                    "types": {
	                        "root": {
	                            "valid_children": ["category"],
	                            "max_depth": -1,
	                            "hover_node": false,
	                            "select_node": function () { return false; }
	                        },
	                        "category": {
	                            "valid_children": ["default"],
	                            "max_depth": -1,
	                            "hover_node": false,
	                            "select_node": function () { return false; }
	                        },
	                        "default": {
	                            "valid_children": ["default"]
	                        }
	                    }
	                }
	            });
            }

            function CreateNewWorkflow(inst, el) {
                $('#AddWorkflow .modal-body').load('<%:Url.Action("ManageWorkflow") %>', function () {
                    var categoryName = el.attr('id');
                    $('input[name=CategoryName]').val(el.hasClass('root') ? '' : categoryName);
                    $("#AddWorkflow").modal('show');
                    window.xsleditor = CodeMirror.fromTextArea(document.getElementById("XmlTransformAdd"), {
                        mode: { name: "xml", alignCDATA: true },
                        lineNumbers: true,
                        lineWrapping: true,
                        autoCloseTags: true
                    });

                    window.xsleditor.on("cursorActivity", function () {
                        window.xsleditor.matchHighlight("CodeMirror-matchhighlight");
                    });
                });
            }

            function getContextMenu(el) {
                if (el.hasClass('category') || el.hasClass('root')) {
                    return {
                        "addworkflow": {
                            "label": '<%: this.GetLocalResourceObject("AddWorkflow")%>',
                            "action": function (obj) { CreateNewWorkflow(this, obj); },
                            "_disabled": false,
                            "_class": "addworkflow",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": "/Content/images/plus-ico.png"
                        }
                    };
            }
        }

            $("#AddWorkflow").modal({ show: false });

            $('#btnSaveWorkflow').click(function () {
                if (window.xsleditor) {
                    window.xsleditor.save();
                }
                var postdata = $('form#addworkflowform').serialize();

                $.post($('form#addworkflowform').attr('action'), postdata, function (data) {
                    if (data && data.Success) {
                        $("#workflowListPanel").jstree('destroy');
                        LoadWorkflowTreeView();
                        $("#AddWorkflow").modal('hide');
                    } else {
                        $("#AddWorkflow .modal-body").html(data);
                        window.xsleditor = CodeMirror.fromTextArea(document.getElementById("XmlTransformAdd"), {
                            mode: { name: "xml", alignCDATA: true },
                            lineNumbers: true,
                            lineWrapping: true,
                            autoCloseTags: true
                        });

                        window.xsleditor.on("cursorActivity", function () {
                            window.xsleditor.matchHighlight("CodeMirror-matchhighlight");
                        });
                    }
                });
            });
        });
    </script>
    <script src="<%: Url.Content("~/bundles/codemirrorscripts") %>" type="text/javascript"></script>
    <link rel="Stylesheet" href="/Content/codemirror.css" />
</asp:Content>
