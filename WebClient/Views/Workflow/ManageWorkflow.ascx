﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CelloSaaS.WorkFlow.Model.WorkflowModel>" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<div class="form-container">
    <form id="addworkflowform" name="addworkflowform" action="<%=Url.Action("SaveWorkflow") %>">
        <% Html.RenderPartial("StatusMessage"); %>
        <div class="form-group <%=Html.ValidationMessage("Name","") != null ? "has-error" : "" %>">
            <label class="mandatory"><%:this.GetLocalResourceObject("l_WorkflowName")%></label>
            <%: Html.TextBox("Name")  %>
            <%: Html.Hidden("Id")  %>
            <%: Html.Hidden("TenantId") %>
        </div>
        <div class="form-group <%=Html.ValidationMessage("Description","") != null ? "has-error" : "" %>">
            <label class="mandatory"><%:this.GetLocalResourceObject("l_Description")%></label>
            <%: Html.TextArea("Description")  %>
        </div>
        <div class="form-group">
            <label><%:this.GetLocalResourceObject("l_WorkflowInputFinderType")%></label>
            <%: Html.TextBox("WFInputFinderType")%>
        </div>
        <div class="form-group">
            <label><%:this.GetLocalResourceObject("l_Category")%></label>
            <%: Html.TextBox("CategoryName")%>
        </div>
        <div class="form-group">
            <label><%:this.GetLocalResourceObject("l_XMLTransform")%></label>
            <% if (Model != null && Model.Id != null && Model.Id != Guid.Empty)
               { %>
            <%: Html.TextArea("XmlTransform", Model.WFXmlTransform)%>
            <%}
               else
               { %>
            <%: Html.TextArea("XmlTransformAdd", Model.WFXmlTransform)%>
            <%} %>
        </div>
        <% if (ViewData["IsGlobal"] != null && (bool)ViewData["IsGlobal"])
           {
               if (this.Model.Id == Guid.Empty)
               {%>

        <div class="form-group">
            <label><%:this.GetLocalResourceObject("l_IsGlobal")%></label>
            <%: Html.CheckBox("IsGlobal")%>
        </div>
        <%
                       }
                       else
                       {
        %>
        <%: Html.Hidden("IsGlobal", (bool)ViewData["IsGlobal"])%>

        <%
                       }
                   }
        %>
    </form>
</div>
