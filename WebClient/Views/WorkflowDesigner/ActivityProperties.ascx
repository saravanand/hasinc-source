﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CelloSaaS.WorkFlow.Model.ActivityProperty>>" %>
<%@ Import Namespace="CelloSaaS.WorkFlow.Model" %>
<%@ Import Namespace="CelloSaaS.WorkFlow.Activities" %>
<%
    HtmlHelper.ClientValidationEnabled = false;
    HtmlHelper.UnobtrusiveJavaScriptEnabled = false;
%>
<% if (Model != null && Model.Count > 0)
   { %>
<table id="ActivityProperties" class="celloTable">
    <thead>
        <tr>
            <th>
                <%: this.GetLocalResourceObject("Property") %>
            </th>
            <%--<th>
                <%: this.GetLocalResourceObject("Source") %>
            </th>--%>
            <th>
                <%: this.GetLocalResourceObject("Value")%>
            </th>
        </tr>
    </thead>
    <% foreach (ActivityProperty property in Model)
       {
           string propertyName = property.Name.Replace(" ", "");%>
    <tr class='<%: propertyName %>'>
        <td>
            <%= property.Name %>
            <%: Html.Hidden("Name_" + propertyName, property.Name, new {@class="Name" })%>
            <%: Html.Hidden("DataType_" + propertyName, property.DataType, new { @class = "DataType" })%>
        </td>
        <%-- <td>
            <%: Html.DropDownList("SourceType_" + propertyName, new SelectList((IEnumerable)ViewData["SourceType"], "Text", "Text", property.SourceType), "--- Select Source Type---", new { @class = "SourceType" })%>
        </td>--%>
        <td>
            <% if (property.Name.Equals(NotificationActivityConstants.NotificationName, StringComparison.OrdinalIgnoreCase))
               {%>
            <%:Html.DropDownList(NotificationActivityConstants.NotificationName, null, new { style="width:95%;",@class="Value" })%>
            <a href="#" onclick="ManageNotification()" title="<%:this.GetLocalResourceObject("t_ManageWorkflow")%>">
                <i class="fa fa-wrench"></i>
            </a>
            <%}
               else
               {
                   if (property.DataType.Equals("TextArea"))
                   {%>
            <%: Html.TextArea("Value_" + propertyName, property.Value,new {@class="Value", width = "100%", cols="50", rows="15", style="width:400px" }) %>
            <%}
                   else
                   {%>
            <%: Html.TextBox("Value_" + propertyName, property.Value, new { @class = "Value", width = "100%" })%>
            <%}
               }%>
        </td>
    </tr>
    <%} %>
</table>
<%}
   else
   { %>
<%: this.GetLocalResourceObject("NoPropertiesAvailable")%>
<%} %>
<script type="text/javascript">
    $(function () {
        $('select').select2();
    });
    function ManageNotification() {
        var url = '/NotificationConfig/NotificationMasterDetails';
        window.open(url);
    }
</script>
