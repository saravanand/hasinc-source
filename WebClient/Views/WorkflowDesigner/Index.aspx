﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Workflow.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowDefinition>>" %>

<%@ Import Namespace="CelloSaaS.Model.Configuration" %>
<%@ Import Namespace="MvcContrib.UI.Grid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var WFID = '<%=ViewData["WorkflowId"]%>';
        var FILTERID = '<%=ViewData["FilterId"]%>';

        var publishWF = function (workflowDefinitionId) {
            var psdata = { workflowDefinitionId: workflowDefinitionId };
            $.post("/WorkflowDesigner/Publish", psdata, function (data) {
                if (data.Error) {
                    ShowErrorMsg(data.Error);
                } else {
                    saveTempMsg(data.Status);
                    window.location = "/WorkflowDesigner/Index?workflowId=" + WFID + '&filterId=' + FILTERID;
                }
            }, 'json');
            return false;
        }

        var copyWF = function (workflowDefinitionId) {
            var psdata = { workflowDefinitionId: workflowDefinitionId, workflowId: WFID, filterId: FILTERID };
            $.post("/WorkflowDesigner/CreateCopy", psdata, function (data) {
                if (data.Error) {
                    ShowErrorMsg(data.Error);
                } else {
                    saveTempMsg(data.Status);
                    window.location = "/WorkflowDesigner/Index?workflowId=" + WFID + '&filterId=' + FILTERID;
                }
            }, 'json');
            return false;
        }

        var deleteWFDef = function (workflowDefinitionId, obj) {
            if (confirm('<%: this.GetLocalResourceObject("m_DeleteDesign")%>')) {
                var psdata = { workflowDefinitionId: workflowDefinitionId, workflowId: WFID, filterId: FILTERID };
                $.post("/WorkflowDesigner/DeleteWorkflow", psdata, function (data) {
                    if (data.Error) {
                        ShowErrorMsg(data.Error);
                    } else {
                        var $tbl = $(obj).closest('table');
                        var countTrs = $('tr', $tbl).length;

                        if (countTrs <= 2) {
                            var $div = $tbl.closest('div');
                            $tbl.remove();
                            $div.append('<div class="clear"></div><div class="info"><%: this.GetGlobalResourceObject("General","NoData") %></div>');
                        } else {
                            $(obj).closest('tr').remove();
                        }
                        ShowSuccessMsg(data.Success);
                    }
                }, 'json');
            }
            return false;
        };

        var SaveToDB = function (workflowDefinitionId) {
            var psdata = { workflowDefinitionId: workflowDefinitionId, workflowId: WFID, filterId: FILTERID };
            $.post("/WorkflowDesigner/SaveWorkflowDefinition", psdata, function (data) {
                if (data.Error) {
                    ShowErrorMsg(data.Error);
                } else {
                    saveTempMsg(data.Success);
                    window.location = "/WorkflowDesigner/Index?workflowId=" + WFID + '&filterId=' + FILTERID;
                }
            }, 'json');
            return false;
        };

        function saveTempMsg(msg) {
            var date = new Date();
            date.setTime(date.getTime() + (5 * 60 * 1000));
            $.cookie("tempMsg", msg, { expires: date });
        }

        $(function () {
            var msg = $.cookie("tempMsg");

            if (msg) {
                ShowSuccessMsg(msg);
                $.cookie("tempMsg", '');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (ViewData["WorkflowModel"] == null || Html.ValidationSummary() != null)
       { %>
    <div class="alert alert-danger">
        <%=Html.ValidationSummary()%>
    </div>
    <% }
       else
       { %>
    <% 
           bool CanEdit = (bool)ViewData["CanEdit"];
           bool CanAdd = (bool)ViewData["CanAdd"];
           bool CanDelete = (bool)ViewData["CanDelete"];
           bool CanPublish = (bool)ViewData["CanPublish"];

           string filterId = string.Empty;
           if (ViewData["FilterId"] != null && !string.IsNullOrEmpty(ViewData["FilterId"].ToString()))
           {
               filterId = ViewData["FilterId"].ToString();
           }
           var wfModel = (CelloSaaS.WorkFlow.Model.WorkflowModel)ViewData["WorkflowModel"];
    %>
    <h3>
        <%: this.GetLocalResourceObject("Manage")%>
                '<%=wfModel.Name %>'</h3>
    <div class="pull-right">
        <% if (ViewData["DefaultWfDef"] != null && CanAdd)
           { %>
        <a class="btn btn-success" href="<%:Url.Action("CreateNewDesign","WorkflowDesigner", new { workflowId = wfModel.Id, isDefault=false, filterId=filterId}) %>"
            title="<%: this.GetLocalResourceObject("CreateNewWFDesign")%>"><span>
                <%: this.GetLocalResourceObject("CreateNew")%></span></a>
        <% }
           else if (CanAdd
           && CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
           { %>
        <a class="btn btn-success" title="<%: this.GetLocalResourceObject("CreateNewDefaultDesign")%>" href="<%= this.Url.Action("CreateNewDesign", new { workflowId = wfModel.Id,isDefault=true })%>">
            <%: this.GetLocalResourceObject("CreateNew")%>
        </a>
        <% } %>
        <a class="btn btn-default" href="<%:Url.Action("Index","Workflow") %>" title="<%: this.GetLocalResourceObject("BackToList")%>">
            <%=this.GetGlobalResourceObject("General","Back") %></a>
    </div>
    <div class="content-box">
        <p>
            <b>
                <%: this.GetLocalResourceObject("WorkflowName")%>: </b>
            <%=wfModel.Name %>
        </p>
        <p>
            <b>
                <%: this.GetLocalResourceObject("Description")%>: </b>
            <%=wfModel.Description%>
        </p>
        <p>
            <b>
                <%: this.GetLocalResourceObject("ActiveVersion")%>: </b>
            <%=wfModel.ActiveVersion%>
        </p>
        <%if (!string.IsNullOrEmpty(filterId))
          {%>
        <p>
            <b>
                <%: this.GetLocalResourceObject("FilterId")%>:</b>
            <%=filterId%>
        </p>
        <% } %>
    </div>
    <div id="statusMessage">
    </div>
    <section id="defaultWFGrid" class="panel purple">
        <header class="panel-heading">
            <h4>
                <%: this.GetLocalResourceObject("DefaultWorkflowDesign")%></h4>
        </header>
        <div class="panel-body">
            <% if (ViewData["DefaultWfDef"] != null)
               { %>
            <%     
                   string manageStr = this.GetGlobalResourceObject("General", "View").ToString();
                   if (CelloSaaS.Library.UserIdentity.IsInRole(CelloSaaS.ServiceContracts.AccessControlManagement.RoleConstants.ProductAdmin))
                   {
                       manageStr = this.GetGlobalResourceObject("General", "Edit").ToString();
                   }

                   Html.Grid(new List<CelloSaaS.WorkFlow.Model.WorkflowDefinition>() { (CelloSaaS.WorkFlow.Model.WorkflowDefinition)ViewData["DefaultWfDef"] }).Columns(column =>
                   {
                       column.For(wf => wf.Version).Named(this.GetLocalResourceObject("Version").ToString());
                       column.For(wf => wf.Status.ToString()).Named(this.GetLocalResourceObject("PublishStatus").ToString()).DoNotEncode();
                       column.For(wf => wf.CreatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("CreatedOn").ToString());
                       column.For(wf => wf.UpdatedOn == DateTime.MinValue ? "--" : wf.UpdatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("UpdatedOn").ToString());
                       column.For(wf => Html.ActionLink(manageStr, "ManageDesign", new { workflowId = wfModel.Id, workflowDefinitionId = wf.DefinitionId }).ToHtmlString().Replace(manageStr, "<i class='fa fa-search'></i>"))
                           .Named(manageStr)
                           .Attributes(@class => "halign")
                           .HeaderAttributes(@class => "halign")
                           .DoNotEncode();
                       column.For(wf => "<a href='#' onclick=\"javascript:copyWF('" + wf.DefinitionId + "');\"><i class='fa fa-copy'></i></a>")
                           .Named(this.GetLocalResourceObject("Copy").ToString())
                           .Attributes(@class => "halign")
                           .HeaderAttributes(@class => "halign")
                           .DoNotEncode();
                   }).Attributes(id => "dataList", @class => "celloTable").Render();
               }
               else
               { %>
            <div class="alert alert-info">
                <%: this.GetLocalResourceObject("No_DefaultWF")%>
            </div>
            <% } %>
        </div>
    </section>
    <% if (ViewData["Source"].ToString() == ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING)
       { %>
    <section class="panel indigo">
        <header class="panel-heading">
            <h4>
                <%: this.GetLocalResourceObject("ParentWFDesign")%></h4>
        </header>
        <div class="panel-body">
            <% if (ViewData["ActiveParentWfDef"] != null)
               {
                   Html.Grid(new List<CelloSaaS.WorkFlow.Model.WorkflowDefinition>() { (CelloSaaS.WorkFlow.Model.WorkflowDefinition)ViewData["ActiveParentWfDef"] }).Columns(column =>
                   {
                       if (string.IsNullOrEmpty(filterId))
                       {
                           column.For(wf => wf.FilterId ?? "---").Named(this.GetLocalResourceObject("FilterId").ToString());
                       }
                       column.For(wf => wf.Version).Named(this.GetLocalResourceObject("Version").ToString());
                       column.For(wf => wf.CreatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("CreatedOn").ToString());
                       column.For(wf => wf.UpdatedOn == DateTime.MinValue ? "--" : wf.UpdatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("UpdatedOn").ToString());
                       column.For(wf => "<span style='color:green;font-weight:bold;'>" + this.GetLocalResourceObject("Active").ToString() + "</span>")
                           .Named(this.GetLocalResourceObject("Status").ToString())
                           .Attributes(@class => "halign")
                           .HeaderAttributes(@class => "halign")
                           .DoNotEncode();
                       column.For(wf => Html.ActionLink("~View~", "ManageDesign", new { workflowId = wfModel.Id, workflowDefinitionId = wf.DefinitionId, filterId = ViewData["FilterId"] }).ToHtmlString().Replace("~View~", "<i class='fa fa-search'></i>"))
                           .Named(this.GetGlobalResourceObject("General", "View").ToString())
                           .Attributes(@class => "halign")
                           .HeaderAttributes(@class => "halign")
                           .DoNotEncode();
                       column.For(wf => "<a href='#' onclick=\"javascript:copyWF('" + wf.DefinitionId + "');\"><i class='fa fa-copy'></i></a>")
                           .Named(this.GetLocalResourceObject("Copy").ToString())
                           .Attributes(@class => "halign")
                           .HeaderAttributes(@class => "halign")
                           .DoNotEncode();
                   }).Attributes(id => "dataList", cellpadding => "0", cellspacing => "1", @class => "celloTable").Render();
               }
               else
               { %>
            <div class="alert alert-info">
                <%: this.GetGlobalResourceObject("General","NoData") %>
            </div>
            <% } %>
        </div>
    </section>
    <% } %>
    <div class="grid-part" style="margin-top: 20px;">
        <% if (ViewData["Source"].ToString() == ConfigurationSettingValueConstant.LOOKUP_PARENT_SETTING)
           { %>
        <div class="page-title">
            <h3>
                <%: this.GetLocalResourceObject("OwnWFDesign") %></h3>
        </div>
        <% } %>
        <section class="panel green">
            <header class="panel-heading">
                <h4>
                    <%: this.GetLocalResourceObject("PublishedWFDesigns")%></h4>
            </header>
            <div class="panel-body">
                <% if (this.Model != null && this.Model.Where(x => x.Status == CelloSaaS.WorkFlow.Model.WorkflowDefinitionStatus.Published).Count() > 0)
                   {
                       Html.Grid(this.Model.Where(x => x.Status == CelloSaaS.WorkFlow.Model.WorkflowDefinitionStatus.Published)).Columns(column =>
                       {
                           if (string.IsNullOrEmpty(filterId))
                           {
                               column.For(wf => wf.FilterId ?? "---").Named(this.GetLocalResourceObject("FilterId").ToString());
                           }
                           column.For(wf => wf.Version).Named(this.GetLocalResourceObject("Version").ToString());
                           column.For(wf => wf.CreatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("CreatedOn").ToString());
                           column.For(wf => wf.UpdatedOn == DateTime.MinValue ? "--" : wf.UpdatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("UpdatedOn").ToString());
                           column.For(wf => wfModel.ActiveVersion == wf.Version ? "<span style='color:green;font-weight:bold;'>" + this.GetLocalResourceObject("Active").ToString() + "</span>" : "---")
                                 .Named(this.GetLocalResourceObject("Status").ToString())
                               .Attributes(@class => "halign")
                               .HeaderAttributes(@class => "halign")
                               .DoNotEncode();
                           column.For(wf => Html.ActionLink("~View~", "ManageDesign", new { workflowId = wfModel.Id, workflowDefinitionId = wf.DefinitionId, filterId = ViewData["FilterId"] }).ToHtmlString().Replace("~View~", "<i class='fa fa-search'></i>"))
                               .Named(this.GetGlobalResourceObject("General", "View").ToString())
                               .Attributes(@class => "halign")
                               .HeaderAttributes(@class => "halign")
                               .DoNotEncode();
                           if (CanAdd)
                           {
                               column.For(wf => "<a href='#' onclick=\"javascript:copyWF('" + wf.DefinitionId + "');\"><i class='fa fa-copy'></i></a>")
                                   .Named(this.GetLocalResourceObject("Copy").ToString())
                                   .Attributes(@class => "halign")
                                   .HeaderAttributes(@class => "halign")
                                   .DoNotEncode();
                           }
                       }).Attributes(id => "dataList", @class => "celloTable").Render();
                   }
                   else
                   { %>
                <div class="alert alert-info">
                    <%: this.GetGlobalResourceObject("General","NoData") %>
                </div>
                <% } %>
            </div>
        </section>
    </div>
    <section class="panel blue">
        <header class="panel-heading">
            <h4>
                <%: this.GetLocalResourceObject("UnPublishedWFDesigns")%></h4>
        </header>
        <div class="panel-body">
            <% if (this.Model != null && this.Model.Where(x => x.Status == CelloSaaS.WorkFlow.Model.WorkflowDefinitionStatus.Draft).Count() > 0)
               {
                   Html.Grid(this.Model.Where(x => x.Status == CelloSaaS.WorkFlow.Model.WorkflowDefinitionStatus.Draft)).Columns(column =>
                   {
                       if (string.IsNullOrEmpty(filterId))
                       {
                           column.For(wf => wf.FilterId ?? "---").Named(this.GetLocalResourceObject("FilterId").ToString());
                       }
                       column.For(wf => wf.Version).Named(this.GetLocalResourceObject("Version").ToString());
                       column.For(wf => wf.CreatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("CreatedOn").ToString());
                       column.For(wf => wf.UpdatedOn == DateTime.MinValue ? "--" : wf.UpdatedOn.ToString(CelloSaaS.Library.Helpers.ConfigHelper.ConfiguredDateTimeFormat)).Named(this.GetLocalResourceObject("UpdatedOn").ToString());
                       if (CanPublish)
                       {
                           column.For(wf => "<a href='#' style='color:red;text-decoration:underline;' title='" + this.GetLocalResourceObject("PublihDesign").ToString() + "' onclick=\"javascript:publishWF('" + wf.DefinitionId.ToString() + "');\">" + this.GetLocalResourceObject("Publish").ToString() + "</a>")
                               .Named(this.GetLocalResourceObject("Action").ToString())
                               .Attributes(@class => "halign")
                               .HeaderAttributes(@class => "halign")
                               .DoNotEncode();
                       }
                       if (CanEdit)
                       {
                           column.For(wf => Html.ActionLink("~Edit~", "ManageDesign", new { workflowId = wfModel.Id, workflowDefinitionId = wf.DefinitionId, filterId = ViewData["FilterId"] }).ToHtmlString().Replace("~Edit~", "<i class='fa fa-search'></i>"))
                               .Named(this.GetGlobalResourceObject("General", "Edit").ToString())
                               .Attributes(@class => "halign")
                               .HeaderAttributes(@class => "halign")
                               .DoNotEncode();
                       }
                       if (CanDelete)
                       {
                           column.For(wf => "<a href='#' onclick=\"javascript:deleteWFDef('" + wf.DefinitionId + "',this);\"><i class='fa fa-copy'></i></a>")
                               .Named(this.GetGlobalResourceObject("General", "Delete").ToString())
                               .Attributes(@class => "halign")
                               .HeaderAttributes(@class => "halign")
                               .DoNotEncode();
                       }
                   }).Attributes(id => "dataList", @class => "celloTable").Render();
               }
               else
               { %>
            <div class="alert alert-info">
                <%: this.GetGlobalResourceObject("General","NoData") %>
            </div>
            <% } %>
        </div>
    </section>
    <% } %>
</asp:Content>
