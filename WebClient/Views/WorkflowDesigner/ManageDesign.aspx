﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Workflow.Master"
    Inherits="CelloSaaS.View.CelloViewPage<CelloSaaS.WorkFlow.Model.WorkflowDefinition>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/Content/workflowstyle.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading"></div>
    <div class="page-title">
        <% 
            var wfModel = (CelloSaaS.WorkFlow.Model.WorkflowModel)ViewData["WorkflowModel"];
            bool canEdit = (bool)ViewData["CanEdit"];
        %>
        <a href="<%:Url.Action("Index","Workflow", new { filterId=ViewData["FilterId"] }) %>"
            title="<%: this.GetLocalResourceObject("BackToList") %>">
            <i class="icon-custom-left"></i></a>
        <h3>
            <%:this.GetLocalResourceObject("Title")  %></h3>
        <div class="pull-right">
            <% if (canEdit)
               { %>
            <a class="btn btn-info" id="saveTemp" href="<%:Url.Action("SaveWorkflowDefinition","WorkflowDesigner", new { workflowId = ViewData["WorkflowId"], workflowDefinitionId = this.Model.DefinitionId.ToString(),filterId=ViewData["FilterId"] }) %>"
                title="<%: this.GetLocalResourceObject("SaveTitle") %>">
                <%=this.GetGlobalResourceObject("General","Save") %></a>
            <% } %>
            <a class="btn btn-default" href="<%:Url.Action("Index","Workflow", new { filterId=ViewData["FilterId"] }) %>"
                title="<%: this.GetLocalResourceObject("BackToList") %>">
                <%=this.GetGlobalResourceObject("General","Back") %></a>
        </div>
    </div>
    <div class="row-fluid pd-25">
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="alert alert-danger">
            <%=Html.ValidationSummary()%>
        </div>
        <% }
           else
           { %>
        <div class="content-box">
            <p>
                <b>
                    <%:this.GetLocalResourceObject("WorkflowName")%>:</b>
                <%=wfModel.Name %>
            </p>
            <p>
                <b>
                    <%:this.GetLocalResourceObject("Description")%>:</b>
                <%=wfModel.Description%>
            </p>
            <p>
                <b>
                    <%:this.GetLocalResourceObject("ActiveVersion")%>: </b>
                <%=wfModel.ActiveVersion%>
            </p>
            <p>
                <b>
                    <%:this.GetLocalResourceObject("ThisVersion")%>:</b>
                <% if (this.Model.Version != 0)
                   { %>
                <%:this.Model.Version%>
                <% }
                   else if (this.Model.Version == 0)
                   { %>
                <%:this.GetLocalResourceObject("TemporaryDesign")%>
                <% } %>
                <%if (ViewData["FilterId"] != null)
                  {%>
                <p>
                    <b>
                        <%:this.GetLocalResourceObject("FilterId")%>:</b>
                    <%=ViewData["FilterId"]%>
                </p>
                <% } %>
            </p>
        </div>
        <div id="statusMessage">
        </div>
        <div class="row">
            <% if (canEdit)
               { %>
            <div class="col-md-2">
                <div class="accordion">
                    <h3>
                        <a href="#">
                            <%:this.GetLocalResourceObject("Tools")%></a></h3>
                    <div>
                        <div class="min-window Start gradient" style="line-height: 38px;" id="Start" title="<%:this.GetLocalResourceObject("m_DargStart")%>">
                            <%:this.GetLocalResourceObject("Start")%>
                        </div>
                        <div class="min-window Manual gradient" id="Manual" title="<%:this.GetLocalResourceObject("m_DargManual")%>">
                            <%:this.GetLocalResourceObject("Manual")%>
                        </div>
                        <div class="min-window Auto gradient" id="Auto" title="<%:this.GetLocalResourceObject("m_DragAuto")%>">
                            <%:this.GetLocalResourceObject("Auto")%>
                        </div>
                        <div class="min-window Router gradient" id="Router" title="<%:this.GetLocalResourceObject("m_DragRouter")%>">
                            <span class="edit"><%:this.GetLocalResourceObject("Router")%></span>
                        </div>
                        <div class="min-window  IfElseRouter gradient" id="IfElseRouter" title="<%:this.GetLocalResourceObject("m_DargIfElse")%>">
                            <span class="edit"><%:this.GetLocalResourceObject("IfElseRouter")%></span>
                        </div>
                        <div class="min-window Close gradient" style="line-height: 38px;" id="Close" title="<%:this.GetLocalResourceObject("m_DragEnd")%>">
                            End
                        </div>
                    </div>
                    <h3>
                        <a href="#">
                            <%:this.GetLocalResourceObject("Activities")%></a></h3>
                    <div>
                        <% if (ViewData["Activities"] != null)
                           {
                               foreach (var item in (List<CelloSaaS.WorkFlow.Model.ActivityDetails>)ViewData["Activities"])
                               {%>
                        <div class="activity-window" id="<%=item.Id %>" title="Description: <%=item.Description %>, Outcomes: <%=item.Outcomes == null ? "---" : string.Join(", ",item.Outcomes)%>">
                            <%=item.Name %>
                        </div>
                        <%      
                               }
                           } %>
                    </div>
                </div>
            </div>
            <% } %>
            <section class="col-md-10 pdm-0">
                <div class="grid horizontal simple">
                    <div id="wfdesignParentDiv" class="grid-body pdm-0">
                        <div id="wfDesignCanvas" data-celloworkflow="true" data-edit="<%=canEdit.ToString().ToLowerInvariant() %>"
                            data-wf-id="<%=ViewData["WorkflowId"]%>"
                            data-wf-def-id="<%=Model.DefinitionId%>"
                            data-filter-id="<%=ViewData["FilterId"]%>" class="droppable" style="height: 820px; width: 100%; overflow: auto;">
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <script type="text/javascript" src="/Scripts/jquery.jsPlumb-1.5.5-min.js"></script>
        <script type="text/javascript" src="/Scripts/jquery.multi-open-accordion-1.5.3.min.js"></script>
        <script type="text/javascript" src="/Scripts/celloworkflow.js"></script>
        <script type="text/javascript">
            $(function () {
                var canEditDesign = true;

                if ($('#wfDesignCanvas').data('edit')) {
                    $(".accordion").multiOpenAccordion({ active: [0, 1] });
                } else {
                    canEditDesign = false;
                    $("input[type=text]").attr("disabled", "disabled");
                    $('#wfdesignParentDiv').css('width', '100%');
                    $('#wfdesignParentDiv').css('margin-left', '0px');
                }

                $('#wfDesignCanvas').resizable();

                $(document).on('click', ".defineTask", function () {
                    var stepObj = $(this).parent();
                    var activityElement = stepObj.find(".activity-w");

                    $.post("/WorkflowDesigner/GetActivityPropertyDetails",
                    { 'wfId': $('#wfDesignCanvas').data('wf-id'), 'wfDefId': $('#wfDesignCanvas').data('wf-def-id'), 'activityId': activityElement.data('activityId'), 'taskDefId': stepObj.attr('id') },
                    function (html) {
                        $("#taskDefModal #activityProperties").html(html);

                        $('input[name=taskDefinitionId]').val(stepObj.attr('id'));
                        $('input[name=taskName]').val($('span.edit', stepObj).text());
                        $('input[name=taskCode]').val(stepObj.data('taskCode'));
                        $('input[name=Ordinal]').val(stepObj.data('Ordinal'));

                        if (stepObj.data('type') != 'Manual') {
                            $('#manualTabItem').css('display', 'none');
                            $('#manualActorDetailsDiv').css('display', 'none');
                        }
                        else if (stepObj.data('type') == 'Manual') {
                            $('#manualTabItem').css('display', 'list-item');
                            $('#manualActorDetailsDiv').css('display', '');

                            var roleNames = stepObj.data('roleNames');
                            if (roleNames == typeof (Array)) {
                                roleNames = roleNames.join(',');
                            }

                            var roleIds = stepObj.data('roleIds');
                            if (roleIds == typeof (Array)) {
                                roleIds = roleIds.join(',');
                            }

                            var userNames = stepObj.data('userNames');
                            if (userNames == typeof (Array)) {
                                userNames = userNames.join(',');
                            }

                            $('input[name=roleNames]').val(roleNames);
                            $('input[name=roleIds]').val(roleIds);
                            $('input[name=userNames]').val(userNames);
                            $('input[name=url]').val(stepObj.data('stepUrl'));
                        }

                        if (stepObj.data('type') == 'Router') {
                            $('#stepConditionItem').css('display', 'none');
                            //$('#stepConditions').css('display', 'none');
                        } else {
                            $('#stepConditionItem').css('display', 'list-item');
                            //$('#stepConditions').css('display', 'block');
                        }

                        FillConditions(stepObj);

                        $("#taskDefModal").modal('show');
                    });

                });

                function FillConditions($taskElement) {
                    var values = $taskElement.data('StartConditions');
                    $('#startCondition').html('');
                    AddCondToUI(values, $('#startCondition'));

                    values = $taskElement.data('ExpireConditions');
                    $('#expireCondition').html('');
                    AddCondToUI(values, $('#expireCondition'));

                    values = $taskElement.data('CompleteConditions');
                    $('#completeCondition').html('');
                    AddCondToUI(values, $('#completeCondition'));

                    values = $taskElement.data('SkipConditions');
                    $('#skipCondition').html('');
                    AddCondToUI(values, $('#skipCondition'));

                    values = $taskElement.data('OverrideConditions');
                    $('#overrideCondition').html('');
                    AddCondToUI(values, $('#overrideCondition'));
                }

                function AddCondToUI(values, $id) {
                    if (values != null) {
                        for (var i = 0; i < values.length; ++i) {
                            var conditionType = values[i].substring(0, values[i].indexOf(':'));
                            var expression = values[i].substring(values[i].indexOf(':') + 1, values[i].length);
                            AddCodeCondition($id, expression, conditionType);
                        }
                    }
                }

                function SaveTaskDefinition() {
                    var id = $('input[name=taskDefinitionId]').val();
                    var taskName = $('input[name=taskName]').val();
                    var taskCode = $('input[name=taskCode]').val();
                    var ordinal = $('input[name=Ordinal]').val();
                    var taskType = $('#' + id).data('type');

                    var roleIds = $('input[name=roleIds]').val();
                    var roleNames = $('input[name=roleNames]').val();
                    var userNames = $('input[name=userNames]').val();
                    var stepUrl = $('input[name=url]').val();

                    var startConditions = [];
                    var expireConditions = [];
                    var completeConditions = [];
                    var skipConditions = [];
                    var overrideConditions = [];

                    $('#startCondition input[type=text]').each(function () {
                        startConditions.push($(this).attr('name') + ':' + $(this).val());
                    });

                    $('#expireCondition input[type=text]').each(function () {
                        expireConditions.push($(this).attr('name') + ':' + $(this).val());
                    });

                    $('#completeCondition input[type=text]').each(function () {
                        completeConditions.push($(this).attr('name') + ':' + $(this).val());
                    });

                    $('#skipCondition input[type=text]').each(function () {
                        skipConditions.push($(this).attr('name') + ':' + $(this).val());
                    });

                    $('#overrideCondition input[type=text]').each(function () {
                        overrideConditions.push($(this).attr('name') + ':' + $(this).val());
                    });


                    var activityProperty = [];
                    $('#ActivityProperties tbody tr').each(function () {
                        var classAttribute = $(this).attr('class');
                        var name = $('.' + classAttribute + ' .Name').val();
                        var dataType = $('.' + classAttribute + ' .DataType').val();
                        var sourceType = $('.' + classAttribute + ' .SourceType :selected').val();
                        var value;
                        if (classAttribute == "NotificationName") {
                            value = $('.NotificationName .Value option:selected').val()
                        }
                        else {
                            value = $('.' + classAttribute + ' .Value').val();
                        }
                        activityProperty.push({ 'Name': name, 'DataType': dataType, 'SourceType': sourceType, 'Value': value });
                    });

                    var psdata = {
                        workflowDefinitionId: $('#wfDesignCanvas').data('wf-def-id'),
                        TaskId: id,
                        TaskType: taskType,
                        TaskName: taskName,
                        TaskCode: taskCode,
                        Ordinal: ordinal,
                        Roles: roleIds,
                        RoleNames: roleNames,
                        UserNames: userNames,
                        Url: stepUrl,
                        StartConditions: startConditions,
                        ExpireConditions: expireConditions,
                        CompleteConditions: completeConditions,
                        SkipConditions: skipConditions,
                        OverrideConditions: overrideConditions,
                        ActivityProperties: activityProperty
                    };

                    $.ajax({
                        url: '/TaskDefinition/UpdateTaskDefinition',
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(psdata),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data.Error) {
                                alert(data.Error);
                            } else {
                                var $taskElement = $('#' + id);
                                $taskElement.data('taskCode', taskCode);
                                $('span.edit', $taskElement).text(taskName);
                                $('div.ordinal', $taskElement).text(ordinal);

                                // manual
                                if (taskType == 'Manual') {
                                    $taskElement.data('roleNames', roleNames);
                                    $taskElement.data('roleIds', roleIds);
                                    $taskElement.data('userNames', userNames);
                                    $taskElement.data('stepUrl', stepUrl);
                                }

                                $taskElement.data('StartConditions', startConditions);
                                $taskElement.data('ExpireConditions', expireConditions);
                                $taskElement.data('CompleteConditions', completeConditions);
                                $taskElement.data('SkipConditions', skipConditions);
                                $taskElement.data('OverrideConditions', overrideConditions);
                            }
                        }
                    });
                }

                function SaveRouterDefinition() {
                    var id = $('input[name=routerId]').val();
                    var routerName = $('input[name=routerName]').val();

                    var routerconditions = [];
                    $('#routerconditions input[type=text],#routerconditions select').each(function () {
                        routerconditions.push($(this).attr('name') + ':' + $(this).val());
                    });

                    var psdata = {
                        workflowDefinitionId: $('#wfDesignCanvas').data('wf-def-id'),
                        routerId: id,
                        routerName: routerName,
                        conditionExpressions: routerconditions
                    };

                    $.ajax({
                        url: '/WorkflowDesigner/UpdateRouterDetails',
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(psdata),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data.Error) {
                                alert(data.Error);
                            } else {
                                $('#' + id).data('routerName', routerName);
                                $('#' + id).data('routerconditions', routerconditions);

                                $('#' + id + ' span.edit').text(routerName);
                            }
                        }
                    });
                    $(this).dialog("close");
                }

                $(document).on('click', ".defineRouter", function () {
                    var robj = $(this).parent();
                    $('input[name=routerId]').val(robj.attr('id'));
                    $('input[name=routerName]').val(robj.data('routerName'));

                    var values = robj.data('routerconditions');
                    $('#routerconditions').empty();
                    AddCondToUI(values, $('#routerconditions'));

                    $("#routerDefModal").modal('show');
                });


                $(document).on('click', '#addCondition', function () {
                    var $curTab = $('#stepConditions div.tab-pane.active:first');
                    var conditionType = $('select[name=conditionType] option:selected').val();
                    AddCodeCondition($curTab, '', conditionType);
                    return false;
                });

                $(document).on('click', '#addroutercondition', function () {
                    var $curTab = $('#routerconditions');
                    var conditionType = $('select[name=routerconditionType] option:selected').val();
                    AddCodeCondition($curTab, '', conditionType);
                    return false;
                });

                function AddCodeCondition($curTab, value, conditionType) {
                    var html = '<div class="form-group">'
                    html += '<label>' + conditionType + '</label>';
                    html += '<input type="text" name="' + conditionType + '"';
                    if (!canEditDesign)
                        html += ' disabled="disabled" ';
                    html += ' style="width: 80%; display: inline; margin-right: 10px;" value=\'' + value + '\'/>';
                    if (canEditDesign)
                        html += '<a href="#" class="deleteCondExpression"><i class="fa fa-times"></i></a></div>';
                    $curTab.append(html);
                }

                $(document).on('click', '#stepConditions .deleteCondExpression,#routerconditions .deleteCondExpression', function () {
                    $(this).parent().remove();
                    return false;
                });

                $('#saveTemp').click(function () {
                    SaveTempWFDefinition(function (msg, success) {
                        if (success)
                            ShowSuccessMsg(msg);
                        else
                            ShowErrorMsg(msg);
                    });
                    return false;
                });

                function SaveTempWFDefinition(callback) {
                    var uiModel = [];

                    $('#wfDesignCanvas .w').each(function (n) {
                        uiModel[n] =
                        {
                            Id: $(this).attr('id'),
                            OffsetTop: $(this).position().top,
                            OffsetLeft: $(this).position().left,
                            Width: $(this).width(),
                            Height: $(this).height()
                        };
                    });

                    if (uiModel.length == 0) {
                        callback('Cannot save! Design is empty.', false);
                        return;
                    }

                    $.ajax({
                        url: '/WorkflowDesigner/SaveWorkflowUIModel',
                        method: 'POST',
                        dataType: 'json',
                        data: JSON.stringify({ workflowId: $('#wfDesignCanvas').data('wf-id'), workflowDefinitionId: $('#wfDesignCanvas').data('wf-def-id'), uiModel: uiModel }),
                        contentType: 'application/json; charset=utf-8',
                        complete: function () {
                        },
                        success: function (data) {
                            if (data.Success) {
                                if (callback)
                                    callback(data.Success, true);
                            } else {
                                if (callback)
                                    callback(data.Error, false);
                            }
                        }
                    });
                }

                $("#taskDefModal").modal({ show: false });
                $("#routerDefModal").modal({ show: false });

                $('#btnSaveTaskDef').click(function () {
                    SaveTaskDefinition();
                    $("#taskDefModal").modal('hide');
                });

                $('#btnCancelTaskDef').click(function () {
                    $("#taskDefModal").modal('hide');
                    $('input[name=taskDefinitionId]').val('');
                    $('input[name=taskName]').val('');
                    $('input[name=taskCode]').val('');
                    $('input[name=roleNames]').val('');
                    $('input[name=roleIds]').val('');
                    $('input[name=userNames]').val('');
                    $('input[name=url]').val('');
                });

                $('#btnSaveRouterDef').click(function () {
                    SaveRouterDefinition();
                    $("#routerDefModal").modal('hide');
                });

                $('#btnCancelRouterDef').click(function () {
                    $("#routerDefModal").modal('hide');
                    $('input[name=routerId]').val('');
                    $('input[name=routerName]').val('');
                    $('#routerconditions').empty();
                });
            });
        </script>
        <% } %>
    </div>
    <div class="modal fade" id="taskDefModal" tabindex="-1" role="dialog" aria-labelledby="task-definition" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px; left: -16%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">Task Definition</h4>
                </div>
                <div class="modal-body">
                    <section id="task-def-content" class="panel">
                        <header class="panel-heading bg-blue">
                            <ul class="nav nav-tabs captilize">
                                <li class="active">
                                    <a data-toggle="tab" href="#taskDetailsDiv"><%:this.GetLocalResourceObject("TaskDetails")%></a>
                                </li>
                                <li id="manualTabItem">
                                    <a data-toggle="tab" href="#manualActorDetailsDiv"><%:this.GetLocalResourceObject("ActorDetails")%></a>
                                </li>
                                <li id="stepConditionItem">
                                    <a data-toggle="tab" href="#stepConditions"><%:this.GetLocalResourceObject("Conditions")%></a>
                                </li>
                                <li id="properties">
                                    <a data-toggle="tab" href="#activityProperties"><%:this.GetLocalResourceObject("Properties")%></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#helpDiv"><%:this.GetLocalResourceObject("OutcomesAndStatus")%></a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body pd-0 form-container">
                            <div class="tab-content">
                                <div id="taskDetailsDiv" class="tab-pane fade in active">
                                    <div class="form-group">
                                        <label for="taskCode">
                                            <%:this.GetLocalResourceObject("TaskCode")%></label>
                                        <input type="text" name="taskCode" />
                                    </div>
                                    <div class="form-group">
                                        <label for="taskName"><%:this.GetLocalResourceObject("TaskName")%></label>
                                        <input type="text" name="taskName" value="" />
                                        <input type="hidden" name="taskDefinitionId" />
                                    </div>
                                    <div class="form-group">
                                        <label for="ordinal"><%:this.GetLocalResourceObject("Ordinal")%></label>
                                        <input type="text" name="Ordinal" value="" />
                                    </div>
                                </div>
                                <div id="manualActorDetailsDiv" class="tab-pane fade">
                                    <div class="form-group">
                                        <label for="roleIds">
                                            <%:this.GetLocalResourceObject("Roles")%>
                                        </label>
                                        <input type="text" name="roleIds" />
                                        <i><%:this.GetLocalResourceObject("RolesTips")%></i>
                                    </div>
                                    <div class="form-group">
                                        <label for="roleNames">
                                            <%:this.GetLocalResourceObject("RoleNames")%>
                                        </label>
                                        <input type="text" name="roleNames" />
                                        <i><%:this.GetLocalResourceObject("RoleNamesTips")%></i>
                                    </div>
                                    <div class="form-group">
                                        <label for="userNames">
                                            <%:this.GetLocalResourceObject("UserNames")%>
                                        </label>
                                        <input type="text" name="userNames" />
                                        <i><%:this.GetLocalResourceObject("UserNamesTips")%></i>
                                    </div>
                                    <div class="form-group">
                                        <label for="url"><%:this.GetLocalResourceObject("Url")%></label>
                                        <input type="text" name="url" />
                                    </div>
                                </div>
                                <div id="activityProperties" class="tab-pane fade">
                                </div>
                                <div id="helpDiv" class="tab-pane fade">
                                    <section style="overflow: auto; height: 300px;">
                                        <h4>
                                            <%:this.GetLocalResourceObject("StepExecutionStatus")%>:</h4>
                                        <p>
                                            <%:this.GetLocalResourceObject("StepExecutionStatusInfo")%>.
                                        </p>
                                        <span><%:this.GetLocalResourceObject("Example")%>: <b>E:<i>TaskCode</i>.<i>Completed</i></b></span>
                                        <p>
                                            <%:this.GetLocalResourceObject("lbl_PosibleStep")%>:
                                        </p>
                                        <ul style="list-style-type: disc; margin-left: 30px; margin-bottom: 20px;">
                                            <li>Started</li>
                                            <li>Skipped</li>
                                            <li>Routed</li>
                                            <li>Errored</li>
                                            <li>Executed</li>
                                            <li>Expired</li>
                                            <li>Completed</li>
                                        </ul>
                                        <h4>
                                            <%:this.GetLocalResourceObject("ActivityOutcomeDetails")%>:</h4>
                                        <p>
                                            <%:this.GetLocalResourceObject("ActivityInfo")%>
                                        </p>
                                        <span><%:this.GetLocalResourceObject("Example")%>: <b>O:<i>TaskCode</i>.<i>Approved</i></b></span>
                                        <br />
                                        <table class="celloTable">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <%:this.GetLocalResourceObject("ActivityName")%>
                                                    </th>
                                                    <th>
                                                        <%:this.GetLocalResourceObject("PossibleOutcomes")%>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% if (ViewData["Activities"] != null)
                                                   {
                                                       foreach (var item in (List<CelloSaaS.WorkFlow.Model.ActivityDetails>)ViewData["Activities"])
                                                       {%>
                                                <tr>
                                                    <td>
                                                        <%:item.Name %>
                                                    </td>
                                                    <td>
                                                        <%: item.Outcomes == null ? "---" : string.Join(", ", item.Outcomes)%>
                                                    </td>
                                                </tr>
                                                <%      
                                                       }
                                                   } %>
                                            </tbody>
                                        </table>
                                    </section>
                                </div>
                                <div id="stepConditions" class="tab-pane fade">
                                    <section class="panel">
                                        <header class="panel-heading bg-success">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#startCondition">
                                                        <%:this.GetLocalResourceObject("Start")%></a></li>
                                                <li>
                                                    <a data-toggle="tab" href="#expireCondition">
                                                        <%:this.GetLocalResourceObject("Expire")%></a></li>
                                                <li>
                                                    <a data-toggle="tab" href="#skipCondition">
                                                        <%:this.GetLocalResourceObject("Skip")%></a></li>
                                                <li>
                                                    <a data-toggle="tab" href="#completeCondition">
                                                        <%:this.GetLocalResourceObject("Complete")%></a></li>
                                            </ul>
                                        </header>
                                        <div class="tab-content panel-body pd-0">
                                            <% if (canEdit)
                                               { %>
                                            <div class="form-group col-md-12" style="margin-top: 10px;">
                                                <select name="conditionType">
                                                    <option value="TaskExpressionCondition">
                                                        <%:this.GetLocalResourceObject("TaskConditionExpression")%></option>
                                                    <option value="CodeExpressionCondition">
                                                        <%:this.GetLocalResourceObject("CodeConditionExpression")%></option>
                                                    <option value="BusinessRuleExpressionCondition">
                                                        <%:this.GetLocalResourceObject("BusinessRuleConditionExpression")%></option>
                                                </select>
                                                <a class="btn btn-info" href="#" id="addCondition" title="<%:this.GetLocalResourceObject("AddConditionToolTip")%>"><%:this.GetLocalResourceObject("AddCondition")%></a>
                                            </div>
                                            <% } %>
                                            <div id="startCondition" class="tab-pane fade in active">
                                            </div>
                                            <div id="expireCondition" class="tab-pane fade">
                                            </div>
                                            <div id="skipCondition" class="tab-pane fade">
                                            </div>
                                            <div id="completeCondition" class="tab-pane fade">
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button type="button" id="btnSaveTaskDef" class="btn btn-primary" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Save") %> </button>
                        <button type="button" id="btnCancelTaskDef" class="btn btn-default" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Close") %> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="routerDefModal" tabindex="-1" role="dialog" aria-labelledby="router-definition" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;</button>
                    <h4 class="modal-title">Router Definition</h4>
                </div>
                <div class="modal-body">
                    <section id="router-def-content" class="panel">
                        <header class="panel-heading bg-blue">
                            <ul class="nav nav-tabs captilize">
                                <li class="active">
                                    <a data-toggle="tab" href="#routerDetails"><%:this.GetLocalResourceObject("RouterDetails")%></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#rConditions"><%:this.GetLocalResourceObject("Conditions")%></a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body pd-0 form-container">
                            <div class="tab-content">
                                <div id="routerDetails" class="tab-pane fade in active">
                                    <div class="form-group">
                                        <label for="routerName">
                                            <%:this.GetLocalResourceObject("RouterName")%>:</label>
                                        <input type="text" name="routerName" />
                                    </div>
                                </div>
                                <div id="rConditions" class="tab-pane fade">
                                    <% if (canEdit)
                                       { %>
                                    <div class="form-group">
                                        <select name="routerconditionType">
                                            <option value="TaskExpressionCondition">
                                                <%:this.GetLocalResourceObject("TaskConditionExpression")%></option>
                                            <option value="CodeExpressionCondition">
                                                <%:this.GetLocalResourceObject("CodeConditionExpression")%></option>
                                            <option value="BusinessRuleExpressionCondition">
                                                <%:this.GetLocalResourceObject("BusinessRuleConditionExpression")%></option>
                                        </select>
                                        <a href="#" class="btn btn-info" id="addroutercondition" title="<%:this.GetLocalResourceObject("AddConditionToolTip")%>">
                                            <%:this.GetLocalResourceObject("AddCondition")%></a>
                                    </div>
                                    <% } %>
                                    <div id="routerconditions">
                                    </div>
                                    <input type="hidden" name="routerId" />
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="modal-footer">
                    <div class="actions">
                        <button type="button" id="btnSaveRouterDef" class="btn btn-primary" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Save") %> </button>
                        <button type="button" id="btnCancelRouterDef" class="btn btn-default" data-dismiss="modal"><%: this.GetGlobalResourceObject("General","Close") %> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
