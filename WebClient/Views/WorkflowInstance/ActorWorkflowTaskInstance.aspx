﻿<%@ Page Title="<%$Resources:PageTitle%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowTaskInstance>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title") %></h3>
    </div>
    <div class="row-fluid pd-25">
        <div class="simple grid horizontal purple">
            <% if (Model != null && Model.Count() > 0)
               {
            %>
            <div class="grid-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control" id="searchText" name="searchText" placeholder="<%=this.GetGlobalResourceObject("General","Search") %>" />
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-white" type="button" onclick="DoSearch();">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
            <div class="grid-body">
                <% Html.RenderPartial("StatusMessage"); %>
                <%  
                    if (Model != null && Model.Count() > 0)
                    {
                        string srcViewImage = Url.Content("~/App_Themes/CelloSkin/view_icon.gif");
                        Html.Grid(this.Model).Columns(column =>
                    {
                        column.For(c => c.StartTime.HasValue ? (c.StartTime.Value.ToUIDateTimeString()) : string.Empty).Named(this.GetLocalResourceObject("CreatedDate").ToString());
                        column.For(c => c.MapId).Named(this.GetLocalResourceObject("MapId").ToString());
                        column.For(c => c.TaskDefinition.TaskCode).Named(this.GetLocalResourceObject("TaskCode").ToString());
                        column.For(c => c.TaskDefinition.TaskName).Named(this.GetLocalResourceObject("TaskName").ToString());
                        column.For(c => c.CurrentExecutionStatus).Named(this.GetLocalResourceObject("Status").ToString()).Attributes(@class => "halign").HeaderAttributes(@class => "halign");
                        column.For(c => c.CurrentExecutionStatus == "Completed" ? "<a style='text-decoration:underline;' href='ExecuteTask?wfInstanceId=" + c.WorkFlowInstanceId.ToString() + "&taskCode=" + c.TaskDefinition.TaskCode + "&status=" + c.CurrentExecutionStatus + "' ><i class='fa fa-eye'></i></a>" : (c.TaskDefinition is CelloSaaS.WorkFlow.Model.ManualTaskDefinition)
                                        ? string.IsNullOrEmpty((c.TaskDefinition as CelloSaaS.WorkFlow.Model.ManualTaskDefinition).Url)
                                        ? "<a style='text-decoration:underline;' href='ExecuteTask?wfInstanceId=" + c.WorkFlowInstanceId.ToString() + "&taskCode=" + c.TaskDefinition.TaskCode + "&status=" + c.CurrentExecutionStatus + "' >Execute</a>" :
                                        "<a style='text-decoration:underline;' href=" + (c.TaskDefinition as CelloSaaS.WorkFlow.Model.ManualTaskDefinition).Url + " >Execute</a> "
                                        : "---").Named("Action").DoNotEncode().Attributes(@class => "halign").HeaderAttributes(@class => "halign");
                    }).Attributes(id => "wfTaskInstanceTable", @class => "celloTable").Render();
                    }
                    else
                    {%>
                <div class="alert alert-info">
                    <%: this.GetLocalResourceObject("NoTasksAvail") %>
                </div>
                <%}%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var filterTable = null;

        $(function () {
            jQDataTable();

            $('#searchText').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    DoSearch();
                    e.preventDefault();
                }
            });
        });

        function jQDataTable() {
            filterTable = $('table#wfTaskInstanceTable').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false,
                "aoColumns": [
                      { "asSorting": ["desc", "asc"], "bSortable": true },
                      { "asSorting": ["desc", "asc"], "bSortable": true },
                      { "asSorting": ["desc", "asc"], "bSortable": true },
                      { "asSorting": ["desc", "asc"], "bSortable": true },
                      { "asSorting": null, "bSortable": false },
                      { "asSorting": null, "bSortable": false }
                ]
            });
        }

        function DoSearch() {
            var searchText = $('#searchText').val();
            if (searchText == 'Search') {
                searchText = '';
            }
            filterTable.fnFilter(searchText);
            $("div.success,div.error").remove();
            return false;
        }
    </script>
</asp:Content>
