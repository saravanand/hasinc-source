﻿<%@ Page Title="<%$Resources:PageTitle%>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% 
        string status = ViewBag.status;
        string taskName = ViewBag.taskName;
        CelloSaaS.WorkFlow.Model.ActivityDetails activityDetails = ViewBag.Activity;
        int count = 0;%>
    <div class="row-fluid pd-25">
        <div class="page-title">
            <a href="<%=Url.Action("ActorWorkflowTaskInstance") %>" title="Go Back!"><i class="icon-custom-left"></i></a>
            <h3>
                <%: taskName+" " %>
                <%: this.GetLocalResourceObject("TaskDetails")%></h3>
        </div>

        <div class="clear">
        </div>
        <% if (ViewData["TaskDetails"] != null && ((Dictionary<string, string>)ViewData["TaskDetails"]).Count > 0)
           {%>
        <div class="grid simple">
            <div class="grid-body">
                <table class="celloTable">
                    <tbody>
                        <% foreach (KeyValuePair<string, string> attribute in (Dictionary<string, string>)ViewData["TaskDetails"])
                           { %>
                        <tr class="<%: count%2==0?"even":"odd" %>">
                            <td style="width: 250px">
                                <%: attribute.Key %>
                            </td>
                            <td>
                                <%: attribute.Value %>
                            </td>
                        </tr>
                        <%
                           count++;
                       }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
        <%}
           string wfInputString = ViewData["InputDetails"] != null ? ViewData["InputDetails"].ToString() : string.Empty;
           if (!string.IsNullOrEmpty(wfInputString))
           {%>
        <%= wfInputString %>
        <%} %>
        <div class="clear">
        </div>
        <div class="grid simple">
            <div class="grid-title">
                <h4>Status and Outcome Details</h4>
            </div>
            <div class="grid-body">
                <table class="celloTable">
                    <tbody>
                        <tr>
                            <td style="width: 250px">
                                <label>
                                    <%: this.GetLocalResourceObject("TaskStatus")%>
                                </label>
                            </td>
                            <td>
                                <%: status%>
                            </td>
                        </tr>
                        <tr>
                            <% if (status.Equals(CelloSaaS.WorkFlow.ServiceContracts.WorkFlowExecutionStatus.Completed))
                               {%>
                            <td style="width: 250px">
                                <label>
                                    <%: this.GetLocalResourceObject("Outcome")%>
                                </label>
                            </td>
                            <td>
                                <%: ViewData["Outcome"].ToString()%>
                            </td>
                            <%   }
                               else
                               { %>
                            <td style="width: 250px">
                                <label>
                                    <%: this.GetLocalResourceObject("Outcome")%>
                                </label>
                            </td>
                            <td>
                                <%if (activityDetails != null && activityDetails.Outcomes != null && activityDetails.Outcomes.Count() > 0)
                                  {
                                      var selectList = new SelectList((IEnumerable)activityDetails.Outcomes);
                                      using (Html.BeginForm("ExecuteTask", "WorkflowInstance"))
                                      {%>
                                <%: Html.Hidden("wfInstanceId")%>
                                <%: Html.Hidden("taskCode")%>
                                <%: Html.Hidden("status")%>
                                <%: Html.DropDownList("outcome", selectList, "---- Select Outcome ---", new { style = "float:left" })%>
                                <%}
                              }
                                  else
                                  {%>
                                <%: this.GetLocalResourceObject("NotAvailable")%>
                                <% }
                           }
                                %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <% if (!status.Equals(CelloSaaS.WorkFlow.ServiceContracts.WorkFlowExecutionStatus.Completed) && activityDetails != null && activityDetails.Outcomes != null && activityDetails.Outcomes.Count() > 0)
                   {%>
                <div class="form-actions">
                    <a href="#" class="btn btn-info" title="Execute" onclick="$('form').submit()"><span><%: this.GetLocalResourceObject("Execute")%></span></a>
                </div>
                <%} %>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
