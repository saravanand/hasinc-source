﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowInstance>>" %>
<% 
    Html.EnableUnobtrusiveJavaScript(false);
    Html.EnableClientValidation(false);
%>
<%
    string workflowId = ViewData["WorkflowId"].ToString();
    string filterId = ViewData["FilterId"] != null ? ViewData["FilterId"].ToString() : string.Empty;
    string sortDirection = (string)ViewData["SortDirection"];
    string sortString = (string)ViewData["SortString"];
    int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
    int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
    int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;
%>
<% 
    string strManageImageUrl = ResolveClientUrl("/App_Themes/CelloSkin/manage.gif");
    Html.Grid(this.Model.OrderByDescending(x => x.CreatedOn)).Columns(column =>
    {
        column.For(wf => wf.MapId).Named(this.GetLocalResourceObject("MapId").ToString());
        column.For(wf => wf.WorkflowDefinition.Version).Named(this.GetLocalResourceObject("DesignVersion").ToString()).Attributes(@class => "halign").HeaderAttributes(@class => "halign");
        column.For(wf => wf.Status).Named(this.GetLocalResourceObject("Status").ToString());
        column.For(wf => wf.CreatedOn).Named(this.GetLocalResourceObject("CreatedOn").ToString());
        column.For(wf => Html.ActionLink("~View~", "ViewWorkflowTaskInstances",
                     new
                     {
                         workflowId = ViewData["WorkflowId"],
                         filterId = ViewData["FilterId"],
                         workflowInstanceId = wf.Id,
                         redirectToPage = pageNumber
                     }, null)
                     .ToHtmlString()
                     .Replace("~View~", "<img alt='" + this.GetLocalResourceObject("ViewStepsDetails").ToString() + "' src='" + strManageImageUrl + "'/>"))
                     .Named(this.GetLocalResourceObject("ViewStepsDetails").ToString()).HeaderAttributes(@class => "halign").Attributes(@class => "halign").DoNotEncode();
    }).Attributes(@class => "celloTable", id => "dataList").Render();
%>
<%
    Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divGrid", LoadingElementDuration = 500, LoadingElementId = "loading" }, new
                                                        {
                                                            workflowId,
                                                            filterId,
                                                        })
               .SetPageNumber(pageNumber).SetTotalCount(totalCount).SetPageSize(pageSize)
               .SetSortValues(sortString, sortDirection)
               .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
               .Render();
%>
