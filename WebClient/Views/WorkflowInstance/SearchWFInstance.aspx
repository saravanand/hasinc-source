﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowInstance>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .exStatus tr td {
            padding-right: 10px;
        }

        .labelColumn {
            text-align: right;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#wfSearchForm').submit(function () {
                $('#divResults').fadeOut().empty();
                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    $('#divResults').html(data).fadeIn();
                }).error(function () {
                    var data = '<div class="error"><%:this.GetLocalResourceObject("SearchError")  %></div>';
                    $('#divResults').html(data).fadeIn();
                });

                return false;
            });

            $(document).on('change', 'select[name=pageSize]', function () {               
                var postdata = "?" + $('#wfSearchForm').serialize();               
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                $.post('<%=Url.Action("SearchWFInstance","WorkflowInstance") %>' + postdata, null, function (data) {
                    $('#divResults').html(data);
                });
              });
        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div id="errorMsg">
        </div>
        <% using (Html.BeginForm("SearchWFInstance", "WorkflowInstance", FormMethod.Post, new { id = "wfSearchForm", @class = "form-inline" }))
           {
        %>
        <div class="content-box">
            <div class="form-group">
                <%: this.GetLocalResourceObject("WFInstanceStatus")%>
                <%= Html.DropDownList("WfInstanceStatus")%>
            </div>
            <div class="form-group">
                <%: this.GetLocalResourceObject("WFInstanceId")%>
                <% 
               string wfInstanceId = null;
               if (ViewData["WFInstanceId"] != wfInstanceId)
               {
                   wfInstanceId = ViewData["WFInstanceId"].ToString();
               } %>
                <%= Html.TextBox("WFInstanceId", wfInstanceId)%>
            </div>
            <div class="form-group">
                <%: this.GetLocalResourceObject("MapId")%>
                <% 
               string mapId = null;
               if (ViewData["MapId"] != mapId)
               {
                   mapId = ViewData["MapId"].ToString();
               } %>
                <%= Html.TextBox("MapId", mapId, new { placeholder=this.GetLocalResourceObject("MapIdHint") })%>
            </div>
            <div class="form-group">
                <%: this.GetLocalResourceObject("WorkflowName")%>
                <%= Html.DropDownList("WFName")%>
            </div>
            <div class="form-group">              
                <a class="btn btn-info" href="#" title="Search..." onclick="$('form').submit();">
                    <%: this.GetGlobalResourceObject("General","Search") %></a>
            </div>
        </div>
        <% } %>
        <div id="divResults">
        </div>
    </div>
    <% if ((bool)ViewData["LoadSearch"])
       { %>
    <script type='text/javascript'>
        $(function () {
            $('#divResults').fadeOut().empty();
            $.post($(this).attr('action'), $(this).serialize(), function (data) {
                $('#divResults').html(data).fadeIn();
            }).error(function () {
                var data = '<div class=\'error\'>SearchError</div>';
                $('#divResults').html(data).fadeIn();
            });

            return false;

        });
    </script>
    <% } %>
</asp:Content>
