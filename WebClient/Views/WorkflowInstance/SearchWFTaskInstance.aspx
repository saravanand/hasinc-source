﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowTaskInstance>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .exStatus tr td {
            padding-right: 10px;
        }

        .labelColumn {
            text-align: right;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#wfTaskInstanceSearchForm').submit(function () {
                $('#divResults').fadeOut().empty();
                $.post($(this).attr('action'), $(this).serialize(), function (data) {
                    $('#divResults').html(data).fadeIn();
                }).error(function () {
                    var data = '<div class="error"><%:this.GetLocalResourceObject("SearchError")  %></div>';
                    $('#divResults').html(data).fadeIn();
                });

                return false;
            });

            $(document).on('change', 'select[name=pageSize]', function () {
                var postdata = "?" + $('#wfTaskInstanceSearchForm').serialize();
                postdata += '&pageSize=' + $('select[name=pageSize]').val();
                $.post('<%=Url.Action("SearchWFTaskInstance","WorkflowInstance") %>' + postdata, null, function (data) {
                    $('#divResults').html(data);
                });
              });

            // Current Execution Status All checkbox event
            $("#TaskCurrentExecutionStatusAll").click(function () {
                if ($("#TaskCurrentExecutionStatusAll").is(":checked")) {
                    $(".TaskCurrentExecutionStatus").attr("checked", "checked");
                    $("#TaskCurrentExecutionStatus").val('');
                } else {
                    $(".TaskCurrentExecutionStatus").removeAttr("checked");
                    $("#TaskCurrentExecutionStatus").val('');
                }
            });
            $(".TaskCurrentExecutionStatus").click(function () {
                var countLength = $(".TaskCurrentExecutionStatus:checked").length;
                var countTotalLength = $(".TaskCurrentExecutionStatus").length;
                if (countLength == countTotalLength) {
                    $("#TaskCurrentExecutionStatusAll").attr("checked", "checked");
                } else {
                    $("#TaskCurrentExecutionStatusAll").removeAttr("checked");
                }
            });
            // Getting selected Current Execution Status checkbox details
            $("#searchWFTI").click(function () {
                var selectedOptions = [];
                $(".TaskCurrentExecutionStatus").each(function () {
                    if ($(this).is(":checked")) {
                        selectedOptions.push($(this).attr("id"));
                    }
                });
                $("#TaskCurrentExecutionStatus").val(selectedOptions);
                $('#wfTaskInstanceSearchForm').submit();
                return true;
            });
            //Default Current Execution Status checkbox settings            
            $("#TaskCurrentExecutionStatusAll").attr("checked", true).trigger("click");
            $("#TaskCurrentExecutionStatusAll").attr("checked", "checked");

        });

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loading">
    </div>
    <div class="page-title">
        <h3>
            <%: this.GetLocalResourceObject("Title")%>
        </h3>
    </div>
    <div class="row-fluid pd-25">
        <div id="errorMsg">
        </div>
        <% using (Html.BeginForm("SearchWFTaskInstance", "WorkflowInstance", FormMethod.Post, new { id = "wfTaskInstanceSearchForm", @class="form-inline" }))
           {
        %>
        <div class="content-box">
            <table class="">
                <tr>
                    <td class="labelColumn">
                        <%: this.GetLocalResourceObject("WFName")%>
                    </td>
                    <td class="row5">
                        <%= Html.DropDownList("WFName")%>
                    </td>
                    <td class="row4 labelColumn">
                        <%: this.GetLocalResourceObject("TaskCode")%>
                    </td>
                    <td class="row5">
                        <% string wfInstanceId = null;
                           if (ViewData["TaskCode"] != wfInstanceId)
                           {
                               wfInstanceId = ViewData["TaskCode"].ToString();
                           } %>
                        <%= Html.TextBox("TaskCode", wfInstanceId)%>
                        <br />
                        <span style="font-size: 10px;">
                            <%: this.GetLocalResourceObject("TaskCodeHint")%></span>
                    </td>
                </tr>
                <tr>
                    <td class="row4 labelColumn">
                        <%: this.GetLocalResourceObject("MapId")%>
                    </td>
                    <td class="row5">
                        <%string mapId = null;
                          if (ViewData["MapId"] != mapId)
                          {
                              mapId = ViewData["MapId"].ToString();
                          } %>
                        <%= Html.TextBox("MapId", mapId)%>
                        <br />
                        <span style="font-size: 10px;">
                            <%: this.GetLocalResourceObject("MapIdHint")%></span>
                    </td>
                    <td class="labelColumn">
                        <%: this.GetLocalResourceObject("ActorId")%>
                    </td>
                    <td>
                        <% string actorId = null;
                           if (ViewData["ActorId"] != wfInstanceId)
                           {
                               actorId = ViewData["ActorId"].ToString();
                           } %>
                        <%= Html.TextBox("ActorId", actorId)%>
                        <br />
                        <span style="font-size: 10px;">
                            <%: this.GetLocalResourceObject("ActorIdHint")%></span>
                    </td>
                </tr>
                <tr>
                    <td class="row4 labelColumn">
                        <%: this.GetLocalResourceObject("WFTaskInstanceStatus") %>
                    </td>
                    <td class="row5" colspan="2">
                        <table class="exStatus">
                            <tr valign="bottom">
                                <td>
                                    <% bool isChecked = false;
                                       if (ViewData["TaskCurrentExecutionStatusAll"] != null)
                                       {
                                           isChecked = Convert.ToBoolean(ViewData["TaskCurrentExecutionStatusAll"].ToString());
                                       } 
                                    %>
                                    <%= Html.CheckBox("TaskCurrentExecutionStatusAll", isChecked)%>
                                    <%: this.GetLocalResourceObject("All")%>
                                </td>
                                <td>
                                    <% isChecked = false;
                                       if (ViewData["TaskCurrentExecutionStatusRouted"] != null)
                                       {
                                           isChecked = Convert.ToBoolean(ViewData["TaskCurrentExecutionStatusRouted"].ToString());
                                       } 
                                    %>
                                    <%= Html.CheckBox("Routed", isChecked, new { @class = "TaskCurrentExecutionStatus" })%>
                                    <%: this.GetLocalResourceObject("Routed")%>
                                </td>
                                <td>
                                    <% isChecked = false;
                                       if (ViewData["TaskCurrentExecutionStatusExecuted"] != null)
                                       {
                                           isChecked = Convert.ToBoolean(ViewData["TaskCurrentExecutionStatusExecuted"].ToString());
                                       } 
                                    %>
                                    <%= Html.CheckBox("Executed", isChecked, new { @class = "TaskCurrentExecutionStatus" })%>
                                    <%: this.GetLocalResourceObject("Executed")%>
                                </td>
                                <td>
                                    <% isChecked = false;
                                       if (ViewData["TaskCurrentExecutionStatusErrored"] != null)
                                       {
                                           isChecked = Convert.ToBoolean(ViewData["TaskCurrentExecutionStatusErrored"].ToString());
                                       } 
                                    %>
                                    <%= Html.CheckBox("Errored", isChecked, new { @class = "TaskCurrentExecutionStatus" })%>
                                    <%: this.GetLocalResourceObject("Errored")%>
                                </td>
                                <td>
                                    <% isChecked = false;
                                       if (ViewData["TaskCurrentExecutionStatusSkipped"] != null)
                                       {
                                           isChecked = Convert.ToBoolean(ViewData["TaskCurrentExecutionStatusSkipped"].ToString());
                                       } 
                                    %>
                                    <%= Html.CheckBox("Skipped", isChecked, new { @class = "TaskCurrentExecutionStatus" })%>
                                    <%: this.GetLocalResourceObject("Skipped")%>
                                </td>
                                <td>
                                    <% isChecked = false;
                                       if (ViewData["TaskCurrentExecutionStatusStarted"] != null)
                                       {
                                           isChecked = Convert.ToBoolean(ViewData["TaskCurrentExecutionStatusStarted"].ToString());
                                       } 
                                    %>
                                    <%= Html.CheckBox("Started", isChecked, new { @class = "TaskCurrentExecutionStatus" })%>
                                    <%: this.GetLocalResourceObject("Started")%>
                                </td>
                                <td>
                                    <% isChecked = false;
                                       if (ViewData["TaskCurrentExecutionStatusCompleted"] != null)
                                       {
                                           isChecked = Convert.ToBoolean(ViewData["TaskCurrentExecutionStatusCompleted"].ToString());
                                       } 
                                    %>
                                    <%= Html.CheckBox("Completed", isChecked, new { @class = "TaskCurrentExecutionStatus" })%>
                                    <%: this.GetLocalResourceObject("Completed")%>
                                    <%= Html.Hidden("TaskCurrentExecutionStatus", ViewData["TaskCurrentExecutionStatus"])%>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="row4"></td>
                    <td class="row5"></td>
                </tr>
                <tr>
                    <td class="row4"></td>
                    <td class="row5"></td>
                    <td class="row4"></td>
                    <td class="row5">
                        <a class="btn btn-info" id="searchWFTI" href="#" title="Search...">
                            <%: this.GetGlobalResourceObject("General","Search") %></a>
                    </td>
                </tr>
            </table>
        </div>
        <% } %>
        <div id="divResults">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="topmenu" runat="server">
</asp:Content>
