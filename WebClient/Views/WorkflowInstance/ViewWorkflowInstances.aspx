﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Workflow.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowInstance>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="center_container">
        <div class="heading_container">
            <h1>
                <%: this.GetLocalResourceObject("Title") %></h1>
            <div class="inner_hold">
                <div class="green_but">
                    <a href="javascript:history.go(-1)">
                        <%=this.GetGlobalResourceObject("General","Back") %></a>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <% if (Html.ValidationSummary() != null)
           { %>
        <div class="error">
            <%=Html.ValidationSummary()%>
        </div>
        <% }
           else
           { %>
        <div class="grid-part">
            <% if (this.Model != null && this.Model.Count() > 0)
               { %>
            <div class="box1">
                <p>
                    <label>
                        <%: this.GetLocalResourceObject("WorkflowName")%>:</label>
                    <span>
                        <%:this.Model.FirstOrDefault().WorkflowDefinition.WfModel.Name %></span></p>
                <p>
                    <label>
                        <%: this.GetLocalResourceObject("Description")%>:</label>
                    <span>
                        <%:this.Model.FirstOrDefault().WorkflowDefinition.WfModel.Description%></span></p>
            </div>
            <div id="divGrid">
                <% Html.RenderPartial("PartialWfInstances", this.Model); %>
            </div>
            <% }
               else
               { %>
            <div class="info">
                <%: this.GetLocalResourceObject("NoRecord")%>
            </div>
            <% } %>
        </div>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
