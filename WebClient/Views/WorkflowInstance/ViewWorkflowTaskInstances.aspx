﻿<%@ Page Title="<%$ Resources:PageTitle %>" Language="C#" MasterPageFile="~/Views/Shared/Workflow.Master"
    Inherits="CelloSaaS.View.CelloViewPage<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowTaskInstance>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row-fluid pd-25">
        <div class="page-title">
            <%                
                var searchParameter = new CelloSaaS.WorkFlow.Model.WfSearchParameter();
                searchParameter.WFInstanceStatus = ViewData["WFIStatus"].ToString();
                searchParameter.WFInstanceId = string.IsNullOrEmpty(ViewData["WFInstId"].ToString()) ? string.Empty : ViewData["WFInstId"].ToString();
                searchParameter.MapId = string.IsNullOrEmpty(ViewData["MapId"].ToString()) ? string.Empty : ViewData["MapId"].ToString();
                searchParameter.WFName = string.IsNullOrEmpty(ViewData["WfName"].ToString()) ? string.Empty : ViewData["WfName"].ToString();
                string pageNumber = ViewData["PageNumber"].ToString();
                string pageSize = ViewData["PageSize"].ToString();
            %>
            <a href="<%:Url.Action("SearchWFInstance","WorkflowInstance",new {
                        WFInstanceStatus = searchParameter.WFInstanceStatus, 
                        WFInstanceId = searchParameter.WFInstanceId,
                        MapId = searchParameter.MapId,
                        WFName = searchParameter.WFName,
                        page = pageNumber,                        
                        pageSize = pageSize,
                        LoadSearch = true
                        }) %>"
                title="Go Back!"><i class="icon-custom-left"></i></a>
            <h3>

                <%: this.GetLocalResourceObject("Title") %></h3>
        </div>
        <div class="clear">
        </div>
        <div class="grid simple">
            <div class="grid-body">
                <% if (Html.ValidationSummary() != null)
                   { %>
                <div class="error">
                    <%=Html.ValidationSummary()%>
                </div>
                <% }
                   else
                   { %>
                <div id="divGrid">
                    <%                  
                       if (this.Model != null && this.Model.Count() > 0)
                       {
                           Html.Grid(this.Model.OrderBy(x => x.TaskDefinition.Ordinal)).Columns(column =>
                           {
                               column.For(col => col.TaskDefinition.TaskName).Named(this.GetLocalResourceObject("TaskName").ToString());
                               column.For(col => col.TaskDefinition.TaskCode).Named(this.GetLocalResourceObject("TaskCode").ToString());
                               column.For(col => col.CurrentExecutionStatus).Named(this.GetLocalResourceObject("CurrentStatus").ToString());
                               column.For(col => string.Join(", ", col.ExecutionStatus)).Named(this.GetLocalResourceObject("ExecutionStatus").ToString());
                               column.For(col => string.Join(", ", col.OutComes)).Named(this.GetLocalResourceObject("Outcomes").ToString());
                               column.For(col => col.StartTime).Named(this.GetLocalResourceObject("StartTime").ToString());
                               column.For(col => string.IsNullOrEmpty(col.ActorId) ? "---" : col.ActorId).Named(this.GetLocalResourceObject("Actor").ToString());
                           }).Attributes(@class => "celloTable", id => "dataList").Render();
                       }
                       else
                       { %>
                    <div class="info">
                        <%: this.GetLocalResourceObject("NoRecord") %>
                    </div>
                    <% } %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
