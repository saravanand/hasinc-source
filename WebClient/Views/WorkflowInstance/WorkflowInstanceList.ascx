﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowInstance>>" %>
<% 
    Html.EnableUnobtrusiveJavaScript(false);
    Html.EnableClientValidation(false);
%>
<div class="grid simple">
    <div class="grid-body pdm-0 no-border">
        <%
            int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
            int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
            int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;

            string wfSearchInstId = string.IsNullOrEmpty(ViewData["WFInstId"].ToString()) ? string.Empty : ViewData["WFInstId"].ToString();
            string WFInstanceStatus = string.IsNullOrEmpty(ViewData["WFIStatus"].ToString()) ? string.Empty : ViewData["WFIStatus"].ToString();
            string mapId = string.IsNullOrEmpty(ViewData["MapId"].ToString()) ? string.Empty : ViewData["MapId"].ToString();
            string wFName = string.IsNullOrEmpty(ViewData["WfName"].ToString()) ? string.Empty : ViewData["WfName"].ToString();

            if (this.Model != null && this.Model.Count() > 0)
            {
                Html.Grid(this.Model).Columns(column =>
                    {
                        column.For(c => c.WorkflowDefinition.WfModel.Name).Named(this.GetLocalResourceObject("WorkflowName").ToString());
                        column.For(c => c.Id).Named(this.GetLocalResourceObject("WorkflowInstanceId").ToString());
                        column.For(c => c.MapId).Named(this.GetLocalResourceObject("MapId").ToString());
                        column.For(c => c.Status).Named(this.GetLocalResourceObject("CurrentExecutionStatus").ToString());
                        column.For(c => Html.ActionLink("~View~", "ViewWorkflowTaskInstances",
                            new
                            {
                                workflowId = c.WorkflowDefinition.WfId,
                                workflowInstanceId = c.Id,
                                redirectTopage = pageNumber,
                                wfSearchInstId = wfSearchInstId,
                                wfInstanceStatus = WFInstanceStatus,
                                mapId = mapId,
                                wFName = wFName,
                                pageNumber = pageNumber,
                                pageSize = pageSize

                            }).ToString()
                                .Replace("~View~", "<i class='fa fa-search'></i>"))
                            .Named(this.GetLocalResourceObject("ViewTasks").ToString()).DoNotEncode().Attributes(style => "text-align:center");
                    }).Attributes(id => "wfInstance", @class => "celloTable").Render();
        %>
        <%
        Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divResults", LoadingElementDuration = 500, LoadingElementId = "loading" }, new
                                                        {
                                                            WFName = ViewData["WfName"],
                                                            WFInstanceStatus = ViewData["WFIStatus"],
                                                            WFInstanceId = ViewData["WFInstId"],
                                                            MapId = ViewData["MapID"]
                                                        })
               .SetPageNumber(pageNumber).SetTotalCount(totalCount).SetPageSize(pageSize)
               .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
               .Render();
        %>
        <% }
    else
    { %>
        <div class="alert alert-info">
            <%: this.GetGlobalResourceObject("General","NoData") %>
        </div>
        <% } %>
    </div>
</div>
