﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<CelloSaaS.WorkFlow.Model.WorkflowTaskInstance>>" %>
<% 
    Html.EnableUnobtrusiveJavaScript(false);
    Html.EnableClientValidation(false);
%>
<div class="grid simple">
    <div class="grid-body pdm-0 no-border">
        <%
            int pageNumber = ViewData["PageNumber"] != null ? (int)ViewData["PageNumber"] : 0;
            int totalCount = ViewData["TotalCount"] != null ? (int)ViewData["TotalCount"] : 0;
            int pageSize = ViewData["PageSize"] != null ? (int)ViewData["PageSize"] : 0;
            if (this.Model != null && this.Model.Count() > 0)
            {
                string strViewImageUrl = ResolveClientUrl("../../App_Themes/CelloSkin/view.gif");
                var wfName = (Dictionary<string, string>)ViewData["workflowModels"];
                Html.Grid(this.Model).Columns(column =>
                    {
                        column.For(c => wfName[c.WFId.ToString()]).Named(this.GetLocalResourceObject("WorkflowName").ToString());
                        column.For(c => c.MapId).Named(this.GetLocalResourceObject("MapId").ToString());
                        column.For(c => c.ActorId).Named(this.GetLocalResourceObject("ActorId").ToString());
                        column.For(c => c.TaskDefinition.TaskCode).Named(this.GetLocalResourceObject("TaskCode").ToString());
                        column.For(c => c.CurrentExecutionStatus).Named(this.GetLocalResourceObject("CurrentExecutionStatus").ToString());
                    }).Attributes(id => "wfTaskInstance", @class => "celloTable").Render();
        %>
        <%
        Ajax.CelloPager(new AjaxOptions { UpdateTargetId = "divResults", LoadingElementDuration = 500, LoadingElementId = "loading" }, new
                                                        {
                                                            WFName = ViewData["WfName"],
                                                            TaskCode = ViewData["WFTaskCode"],
                                                            ActorId = ViewData["WFActorID"],
                                                            MapId = ViewData["WFMapId"],
                                                            TaskCurrentExecutionStatus = ViewData["WfTaskCurrentExecutionStatus"]
                                                        })
               .SetPageNumber(pageNumber).SetTotalCount(totalCount).SetPageSize(pageSize)
               .SetMenuStrings(this.GetGlobalResourceObject("General", "Pager_sLengthMenu").ToString(), this.GetGlobalResourceObject("General", "Pager_sInfo").ToString())
               .Render();
        %>
        <% }
    else
    { %>
        <div class="alert alert-info">
            <%: this.GetGlobalResourceObject("General","NoData") %>
        </div>
        <% } %>
    </div>
</div>
