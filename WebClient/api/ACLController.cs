﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.View.WebApi;
using CelloSaaS.RestApiSDK;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for managing user login and user security.
    /// </summary>
    public class ACLController : CelloApiController
    {
        private IAccessControlService service;
        private IRoleService roleService;
        private IPrivilegeService privilegeService;
        private IDataAccessService dataAccessService;
        private IUserDetailsService userService;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public ACLController()
        {
            this.service = ServiceLocator.Resolve<IAccessControlService>();
            this.roleService = ServiceLocator.Resolve<IRoleService>();
            this.userService = ServiceLocator.Resolve<IUserDetailsService>();
            this.privilegeService = ServiceLocator.Resolve<IPrivilegeService>();
            this.dataAccessService = ServiceLocator.Resolve<IDataAccessService>();
        }

        /// <summary>
        /// This method is used for validates the given api key and returns the re-usable token to make subsequent REST requests.
        /// Token valid for 20 minutes.
        /// </summary>
        /// <returns>token string.</returns>
        [HttpPost]
        public string login()
        {
            // Validate user.
            if (this.userService.ValidateWCFUser(UserIdentity.Name, UserIdentity.WCFSharedKey, this.TenantId.ToString()))
            {
                return ApiTokenHelper.GetToken(this.TenantId.ToString(), UserIdentity.Name);
            }

            return null;
        }

        /// <summary>
        /// This method is used to validates the user if valid then returns true otherwise false.
        /// </summary>
        /// <param name="entity">company code, username, password parameters.</param>
        /// <returns><c>True</c>, if the user is valid, <c>false</c> otherwise.</returns>
        [HttpPost]
        public bool validate_user(LogOnDTO entity)
        {
            Guard.Null("LogOnDetails", entity);

            return this.userService.ValidateUser(entity.username, entity.password, entity.company_code);
        }

        /// <summary>
        /// This method is used to checks if the logged in user has the given privileges.
        /// </summary>
        /// <param name="privileges_ids">unique identifier of privileges (mandatory).</param>
        /// <returns>dictionary of privilege with corresponding permission.</returns>
        [HttpPost]
        public Dictionary<string, bool> check_privileges([Required]string[] privileges_ids)
        {
            if (privileges_ids == null || privileges_ids.Length == 0) return null;

            var perm = this.service.CheckPrivileges(privileges_ids);

            var result = new Dictionary<string, bool>();
            for (int i = 0; i < privileges_ids.Length; ++i)
            {
                if (result.ContainsKey(privileges_ids[i]))
                {
                    result[privileges_ids[i]] = perm[i];
                }
                else
                {
                    result.Add(privileges_ids[i], perm[i]);
                }
            }
            return result;
        }

        /// <summary>
        /// This method is used to retrieves the roles for the given user.
        /// </summary>
        /// <param name="user_id">user unique identifier (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <returns>collection of role identifier.</returns>
        [HttpGet]
        public IEnumerable<string> get_user_roles([Required]Guid user_id, Guid? tenant_id = null)
        {
            Guard.NullGuid("user_id", user_id, true);

            if (!tenant_id.HasValue || tenant_id.Value == Guid.Empty) tenant_id = this.TenantId;
            // Get user roles for tenant
            return this.roleService.GetUserRolesForTenant(user_id.ToString(), tenant_id.Value.ToString());

        }

        /// <summary>
        /// This method is used to retrieves the privileges for the given user.
        /// </summary>
        /// <param name="user_id">user unique identifier (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <returns>collection of privilege.</returns>
        [HttpGet]
        public IEnumerable<string> get_user_privileges([Required]Guid user_id, Guid? tenant_id = null)
        {
            Guard.NullGuid("user_id", user_id, true);

            if (!tenant_id.HasValue || tenant_id.Value == Guid.Empty) tenant_id = this.TenantId;
            // Get user roles for tenant
            string[] roleIds = this.roleService.GetUserRolesForTenant(user_id.ToString(), tenant_id.Value.ToString());

            if (roleIds != null && roleIds.Count() > 0)
            {
                // Get privilege names for tenant
                var privileges = this.privilegeService.GetPrivilegeNames(tenant_id.Value.ToString(), roleIds);
                return privileges;
            }

            return null;
        }

        /// <summary>
        /// This method is used to checks if the given user has the privilege against the given tenant.
        /// </summary>
        /// <param name="user_id">user unique identifier (mandatory).</param>
        /// <param name="privilege">privilege unique identifier (mandatory). (e.g.: View_Employee).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <returns><c>True</c>, if the given user has the privilege against the given tenant is valid, <c>false</c> otherwise.</returns>
        [HttpGet]
        public bool check_tenant_access_privilege([Required]Guid user_id, [Required]string privilege, [Required]Guid tenant_id)
        {
            Guard.NullGuid("user_id", user_id, true);
            Guard.NullGuid("tenant_id", tenant_id, true);

            var access = this.service.CheckTenantAccessPrivilege(user_id.ToString(), privilege, tenant_id.ToString());
            return access;
        }

        /// <summary>
        /// This method is used to checks if the given URI has the privilege against the given tenant.
        /// </summary>
        /// <param name="resource">UI Path (mandatory)</param>
        /// <param name="resource_type">either URI or BusinessLogic (mandatory).</param>
        /// <returns><c>True</c>, if the given URI has the privilege against the given tenant is valid, <c>false</c> otherwise.</returns>
        [HttpGet]
        public bool check_access_for_resource([Required]string resource, [Required] ResourceTypeConstants resource_type)
        {
            if (ResourceTypeConstants.UI.Equals(resource_type))
            {
                return UISecurityManager.CheckPermission(resource);
            }
            else if (ResourceTypeConstants.BL.Equals(resource_type))
            {
                return BLSecurityManager.CheckPermission(null);
            }
            else
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
        }

        /// <summary>
        /// This method is used to get active roles created by the given tenant.
        /// </summary>
        /// <param name="tenant_id">tenant unique identifier (mandatory).</param>
        /// <returns>collection role details.</returns>
        [HttpGet]
        public IEnumerable<RoleDTO> get_tenant_role_details([Required]Guid tenant_id)
        {
            Guard.NullGuid("tenant_id", tenant_id, true);

            var result = this.roleService.GetTenantRoleDetails(tenant_id.ToString());
            return result != null && result.Count > 0 ? result.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to get all roles (including both active and inactive role) created by the given tenant.
        /// </summary>
        /// <param name="tenant_id">tenant unique identifier (mandatory).</param>
        /// <returns>collection of active and inactive role details.</returns>
        [HttpGet]
        public IEnumerable<RoleDTO> get_all_tenant_role_details([Required]Guid tenant_id)
        {
            Guard.NullGuid("tenant_id", tenant_id, true);

            var result = this.roleService.GetAllTenantRoleDetails(tenant_id.ToString());
            return result != null && result.Count > 0 ? result.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to get active global role details.
        /// </summary>
        /// <returns>collection of global role details.</returns>
        [HttpGet]
        public IEnumerable<RoleDTO> get_global_role_details()
        {
            var result = this.roleService.GetGlobalRoleDetails();
            return result != null && result.Count > 0 ? result.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to get all global(including both active and inactive role) role details.
        /// </summary>
        /// <returns>collection of active and inactive global role details.</returns>
        [HttpGet]
        public IEnumerable<RoleDTO> get_all_global_role_details()
        {
            var result = this.roleService.GetAllGlobalRoleDetails();
            return result != null && result.Count > 0 ? result.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to get the user roles for a specified tenant.
        /// </summary>
        /// <param name="user_id">unique identifier of user (mandatory).</param>
        /// <param name="stratified_tenant_id">unique identifier of the stratified tenant (mandatory).</param>
        /// <param name="role_assigning_tenant_id">unique identifier of the role assigning tenant identifier (mandatory).</param>
        /// <returns>collection of user roles for specified tenant.</returns>
        [HttpGet]
        public IEnumerable<string> get_user_roles_for_tenant([Required]Guid user_id, [Required]Guid stratified_tenant_id, [Required]Guid role_assigning_tenant_id)
        {
            Guard.NullGuid("User_Id", user_id, true);
            Guard.NullGuid("stratified_tenant_id", stratified_tenant_id, true);
            Guard.NullGuid("role_assigning_tenant_id", role_assigning_tenant_id, true);
            var roleIds = this.roleService.GetUserRolesForTenant(user_id.ToString(), stratified_tenant_id.ToString(), role_assigning_tenant_id.ToString());
            return roleIds != null && roleIds.Count() > 0 ? roleIds : null; ;
        }

        /// <summary>
        /// This method is used to get the global roles by tenant identifier.
        /// </summary>
        /// <param name="tenant_id">unique identifier of the tenant (mandatory).</param>
        /// <returns>collection of role details.</returns>
        [HttpGet]
        public IEnumerable<RoleDTO> get_global_roles_by_tenant([Required]Guid tenant_id)
        {
            Guard.NullGuid("tenant_id", tenant_id, true);

            var roleDetails = this.roleService.GetGlobalRolesByTenant(tenant_id.ToString());
            return roleDetails != null && roleDetails.Count > 0 ? roleDetails.ToViewModel() : null; ;
        }

        /// <summary>
        /// This method is used to remove the given user role tenant mapping that has been assigned by the role assigning tenant.
        /// </summary>
        /// <param name="userRoles">user role details (user_id,tenant_id and role_ids are mandatory).</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        [HttpPost]
        public HttpResponseMessage delete_user_roles([Required] UserRolesDTO userRoles)
        {
            Guard.NullGuid("user_id", userRoles.user_id, true);
            Guard.NullOrEmptyEnumerable("role_ids", userRoles.role_ids, true);
            Guard.NullGuid("tenant_id", userRoles.tenant_id, true);

            this.roleService.DeleteUserRoleAssignedByTenant(userRoles.user_id.ToString(), userRoles.role_ids, userRoles.tenant_id.ToString());

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// This method is used to add the collection of roles to the given user.
        /// </summary>
        /// <param name="userRoles">user role details (user_id,tenant_id and role_ids are mandatory).</param>
        [HttpPut]
        public void put_user_to_roles([Required]UserRolesDTO userRoles)
        {
            Guard.NullGuid("User_Id", userRoles.user_id, true);
            Guard.NullOrEmptyEnumerable("Role_Ids", userRoles.role_ids, true);
            Guard.NullGuid("tenant_id", userRoles.tenant_id, true);

            this.roleService.AddUserToRoles(userRoles.user_id.ToString(), userRoles.role_ids, userRoles.tenant_id.ToString());
        }

        /// <summary>
        /// This method is used to add role to users.
        /// </summary>
        /// <param name="userRoles">user role details (user_ids,tenant_id and role_id are mandatory).</param>
        [HttpPut]
        public void put_users_to_role([Required]UserRolesDTO userRoles)
        {
            Guard.NullOrEmpty("role_id", userRoles.role_id, true);
            Guard.NullOrEmptyEnumerable("User_Ids", userRoles.user_ids, true);
            Guard.NullGuid("tenant_id", userRoles.tenant_id, true);

            this.roleService.AddUsersToRole(userRoles.user_ids.Select(c => c.ToString()).ToArray(), userRoles.role_id, userRoles.tenant_id.ToString());
        }

        /// <summary>
        /// This method is used to update user roles.
        /// New role Identifier for user then insert role to user.
        /// Previous user role is not available in the current list then remove role from user.
        /// </summary>
        /// <param name="userRoles">user role details (user_id,tenant_id and role_ids are mandatory).</param>
        [HttpPost]
        public void post_update_user_roles_implicit([Required]UserRolesDTO userRoles)
        {
            Guard.NullGuid("user_id", userRoles.user_id, true);
            Guard.NullOrEmptyEnumerable("role_ids", userRoles.role_ids, true);
            Guard.NullGuid("tenant_id", userRoles.tenant_id, true);

            this.roleService.UpdateUserRoles(userRoles.user_id.ToString(), userRoles.role_ids, userRoles.tenant_id.ToString());
        }

        /// <summary>
        /// This method is used to update the user roles.
        /// deleted roles are removed.
        /// </summary>
        /// <param name="updateUserRoles">user role details (user_id,role_ids and delete_role_ids are mandatory).</param>
        [HttpPost]
        public void post_update_user_roles_explicit([Required]UpdateUserRolesDTO updateUserRoles)
        {
            Guard.NullGuid("User_Id", updateUserRoles.user_id, true);

            Guard.NullOrEmptyEnumerable("role_ids", updateUserRoles.role_ids, true);
            Guard.NullOrEmptyEnumerable("delete_role_ids", updateUserRoles.delete_role_ids, true);

            if (!updateUserRoles.tenant_id.HasValue || updateUserRoles.tenant_id.Value.Equals(Guid.Empty)) updateUserRoles.tenant_id = this.TenantId;
            this.roleService.UpdateUserRoles(updateUserRoles.user_id.ToString(), updateUserRoles.role_ids, updateUserRoles.delete_role_ids, updateUserRoles.tenant_id.ToString());
        }

        /// <summary>
        /// This method is used to apply the entity data scope for the given entity and reference identifiers and retunes the json response.
        /// </summary>
        /// <param name="entity_id">entity identifier.</param>
        /// <param name="reference_ids">array of reference identifiers.</param>
        /// <param name="dataview_id">data view identifier.</param>
        /// <returns>JSON formatted string.</returns>
        [HttpPost]
        public string apply_datascope([Required]string entity_id, [Required]string[] reference_ids, [Required]string dataview_id)
        {
            return this.dataAccessService.GetDataSecurityJson(this.TenantId.ToString(), entity_id, null, reference_ids, dataview_id);
        }
    }

}

