﻿using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceContracts;
using CelloSaaS.Library;
using CelloSaaS.RestApiSDK;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for providing the billing related details.
    /// </summary>
    public class BillingController : CelloApiController
    {
        private IBillingService service;
        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public BillingController()
        {
            this.service = ServiceLocator.Resolve<IBillingService>();
        }

        /// <summary>
        /// This method is used to search the invoices (current invoices are not searched!).
        /// </summary>
        /// <param name="condition">search condition.</param>
        /// <returns>search result.</returns>
        [HttpPost]
        public CelloSaaS.RestApiSDK.InvoiceSearchResultDTO search_invoice(CelloSaaS.RestApiSDK.InvoiceSearchConditionDTO condition)
        {
            if (condition == null)
                condition = new InvoiceSearchConditionDTO();

            if (condition.PageSize <= 0) condition.PageSize = 100;
            if (condition.PageNo < 0) condition.PageNo = 1;

            var searchCondtion = new InvoiceSearchCondition
            {
                TenantIds = condition.TenantIds,
                InvoiceIds = condition.InvoiceIds,

                FromInvoiceDate = condition.FromInvoiceDate,
                ToInvoiceDate = condition.ToInvoiceDate,
                StartDate = condition.StartDate,
                EndDate = condition.EndDate,
                DueDate = condition.DueDate,
                PageSize = condition.PageSize,
                PageNo = condition.PageNo,
                SortField = condition.SortField,
                SortDirection = condition.SortDirection,

            };
            if (condition.InvoiceStatus != null)
            {
                searchCondtion.InvoiceStatus = (InvoiceStatus)condition.InvoiceStatus;
            }
            if (condition.BillFrequency != null)
            {
                searchCondtion.BillFrequency = (BillFrequency)condition.BillFrequency;
            }



            var result = this.service.SearchInvoices(searchCondtion);
            if (result != null && result.Items != null && result.Items.Count > 0)
            {
                return new CelloSaaS.RestApiSDK.InvoiceSearchResultDTO
                {
                    TotalCount = result.TotalCount,
                    Items = result.Items.Values.ToViewModel()
                };
            }
            return null;
        }

        /// <summary>
        /// This method is used to returns the invoice details based on invoice primary and tenant identifier.
        /// </summary>
        /// <param name="id">invoice primary key(mandatory).</param>
        /// <param name="tenantId">tenant identifier(mandatory).</param>
        /// <returns>returns the invoice entity if found else null.</returns>
        public InvoiceDTO get_invoice_byId([Required]Guid invoice_id, [Required]Guid tenantId)
        {
            var result = this.service.GetInvoiceById(invoice_id, tenantId);
            if (result == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return result.ToViewModel();
        }

        /// <summary>
        /// This method is used to returns the tenant wise bill statistics.
        /// </summary>
        /// <param name="condition">search condition.</param>
        /// <returns>returns the bill statistics.</returns>
        [HttpPost]
        public Dictionary<Guid, BillStatisticsDTO> get_bill_statistics(BillStatisticsSearchConditionDTO condition)
        {
            if (condition == null)
                condition = new BillStatisticsSearchConditionDTO();
            var result = this.service.GetBillStatistics(new BillStatisticsSearchCondition
            {
                TenantIds = condition.TenantIds,
                OnlyOverdue = condition.OnlyOverdue,
                MinOverdueAmount = condition.MinOverdueAmount,
                MaxOverdueAmount = condition.MaxOverdueAmount,
                MinTotalAmount = condition.MinTotalAmount,
                MaxTotalAmount = condition.MaxTotalAmount,
                FromDate = condition.FromDate,
                ToDate = condition.ToDate,
                TopTenants = condition.TopTenants
            });
            if (result != null && result.Count > 0)
            {
                return result.ToViewModel();
            }
            return null;
        }

        /// <summary>
        /// This method is used to get total bill revenues for the given date range.
        /// </summary>
        /// <param name="fromDate">from date.</param>
        /// <param name="toDate">to date.</param>
        /// <param name="childTenantId"> child tenant identifier (optional).</param>
        /// <returns>date, (total bills, amount, paid).</returns>
        public Dictionary<DateTime, Tuple<int, double, double, double>> get_revenues([Required]DateTime? fromDate, [Required]DateTime? toDate, Guid childTenantId)
        {
            var result = this.service.GetRevenues(fromDate, toDate, childTenantId.ToString());
            if (result == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return result;
        }
    }

}