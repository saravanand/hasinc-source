﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace WebApplication.api
{
    /// <summary>
    /// CelloSaaS Api controller base class
    /// </summary>
    [Authorize]
    public abstract class CelloApiController : ApiController
    {
        private Guid? _tenantId, _userId;

        protected Guid TenantId
        {
            get
            {
                if (!_tenantId.HasValue)
                    if (CelloSaaS.Library.UserIdentity.TenantID != null)
                        _tenantId = Guid.Parse(CelloSaaS.Library.UserIdentity.TenantID);
                    else
                        _tenantId = Guid.Empty;

                return _tenantId.Value;
            }
        }

        protected Guid UserId
        {
            get
            {
                if (!_userId.HasValue)
                    if (CelloSaaS.Library.UserIdentity.UserId != null)
                        _userId = Guid.Parse(CelloSaaS.Library.UserIdentity.UserId);
                    else
                        _userId = Guid.Empty;

                return _userId.Value;
            }
        }
    }
}
