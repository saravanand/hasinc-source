﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.PartitionManagement.ServiceContracts;
using CelloSaaS.ServiceContracts.TenantManagement;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the data partition metadata.
    /// </summary>
    public class DataPartitionController : CelloApiController
    {
        IDataPartitionService service;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public DataPartitionController()
        {
            this.service = ServiceLocator.Resolve<IDataPartitionService>();
        }

        /// <summary>
        /// This method is used to get the default partition identifier.
        /// </summary>
        /// <returns>default data partition identifier (GUID).</returns>
        [HttpGet]
        public Guid get_default_partition_id()
        {
            return this.service.GetDefaultPartitionId();
        }

        /// <summary>
        /// This method is used to sets the given partition identifier as default.
        /// </summary>
        /// <param name="partition_id">partition identifier(mandatory).</param>
        /// <returns>HttpResponseMessage (OK).</returns>
        [HttpPut]
        public HttpResponseMessage set_default_partition_id([Required]Guid partition_id)
        {
            Guard.NullGuid("partition_id", partition_id, true);
            this.service.SetDataPartitionDefault(partition_id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// This method is used to get the connection string for the given module.
        /// </summary>
        /// <param name="module_code">module code (connection string name) (required).</param>
        /// <param name="partition_id">partition identifier (optional). If not given then taken form current authenticated tenant partition identifier.</param>
        /// <returns>specified connection string based on the parameters.</returns>
        [HttpGet]
        public string get_connection_string([Required]string module_code, Guid? partition_id = null)
        {
            partition_id = GetPartitionId(partition_id);

            if (partition_id.HasValue)
            {
                return this.service.GetConnectionString(partition_id.Value, module_code);
            }

            return null;
        }

        /// <summary>
        /// This method is used to get all the connection strings.
        /// </summary>
        /// <param name="partition_id">partition identifier (optional). If not given then taken form current authenticated tenant partition identifier.</param>
        /// <returns>collection of all connection string.</returns>
        [HttpGet]
        public IEnumerable<CelloSaaS.RestApiSDK.DataPartitionConnDTO> get_all_connection_strings(Guid? partition_id = null)
        {
            partition_id = GetPartitionId(partition_id);

            if (partition_id.HasValue)
            {
                var part = this.service.GetDataPartitionById(partition_id.Value);
                if (part != null && part.DataPartitionMappings != null && part.DataPartitionMappings.Count > 0)
                {
                    var res = part.DataPartitionMappings.Select(x => new CelloSaaS.RestApiSDK.DataPartitionConnDTO
                    {
                        module_code = x.DataModule.DataModuleCode,
                        connection_string = x.DataServer.ConnectionString
                    });
                    return res;
                }
            }

            return null;
        }

        #region Private

        private Guid? GetPartitionId(Guid? partition_id)
        {
            if (!partition_id.HasValue)
            {
                var tenantService = ServiceLocator.Resolve<ITenantService>();
                var tenant = tenantService.GetTenantDetailsByTenantId(this.TenantId.ToString());
                Guid tmp = Guid.Empty;

                if (tenant != null && tenant.TenantDetails != null
                    && !string.IsNullOrEmpty(tenant.TenantDetails.DataPartitionId)
                    && Guid.TryParse(tenant.TenantDetails.DataPartitionId, out tmp) && tmp != Guid.Empty)
                {
                    partition_id = tmp;
                }
            }

            return partition_id;
        }

        #endregion
    }
}
