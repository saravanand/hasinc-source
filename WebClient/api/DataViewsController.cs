﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.ViewManagement;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the data view metadata.
    /// </summary>
    public class DataViewsController : CelloApiController
    {
        private IViewMetaDataService service;
        private IAccessControlService aclService;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public DataViewsController()
        {
            this.service = ServiceLocator.Resolve<IViewMetaDataService>();
            this.aclService = ServiceLocator.Resolve<IAccessControlService>();
        }

        /// <summary>
        /// This method is used to get all data views available to this tenant subscriptions.
        /// </summary>
        /// <param name="tenant_id">tenant identifier (optional).</param>
        /// <returns>collection of data views.</returns>
        public IEnumerable<CelloSaaS.RestApiSDK.DataViewDTO> get_all_dataviews(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;

            var data = this.service.GetDataViews(tenant_id.ToString());

            if (data != null)
            {
                var lst = new List<CelloSaaS.RestApiSDK.DataViewDTO>();

                foreach (var id in data.Select(x => x.DataViewID))
                {
                    var metadata = this.service.GetDataViewMetaData(tenant_id.ToString(), id, null);
                    lst.Add(metadata.ToViewModel(tenant_id.Value));
                }

                return lst;
            }

            return null;
        }

        /// <summary>
        /// This method is used to get the data view metadata details for the given identifier.
        /// </summary>
        /// <param name="dataview_id">data view identifier (e.g.: EmployeeGrid).</param>
        /// <param name="tenant_id">tenant identifier (optional).</param>
        /// <returns>specified data view details.</returns>
        public CelloSaaS.RestApiSDK.DataViewDTO get_dataview([Required]string dataview_id, Guid? tenant_id = null)
        {
            Guard.NullOrEmpty("dataview_id", dataview_id);
            if (!tenant_id.HasValue) tenant_id = this.TenantId;

            var data = this.service.GetDataViewMetaData(tenant_id.ToString(), dataview_id, null);

            if (data == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid dataview id was  passed!") });
            }

            // check field permissions if available
            /*if (data != null && data.AvailableFields != null)
            {
                var ids = data.AvailableFields.Where(x => x.Value.IsVisible).Select(x => "View_" + x.Key).ToArray();

                if (ids.Length > 0)
                {
                    var perms = this.aclService.CheckPrivileges(ids);

                    for (int i = 0; i < ids.Length; ++i)
                    {
                        var key = ids[i].Substring(5, ids[i].Length - 5);
                        data.AvailableFields[key].IsVisible = perms[i];
                    }
                }
            }*/

            return data.ToViewModel(tenant_id.Value);
        }
    }
}
