﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.ServiceContracts.DataManagement;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the entity metadata.
    /// </summary>
    public class EntityMetadatasController : CelloApiController
    {
        private IDataManagementService service;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public EntityMetadatasController()
        {
            this.service = ServiceLocator.Resolve<IDataManagementService>();
        }

        /// <summary>
        /// This method is used to get all entity metadata available for this tenant subscription.
        /// </summary>
        /// <param name="tenant_id">tenant identifier (optional).</param>
        /// <returns>collection of all entity metadata.</returns>
        public IEnumerable<CelloSaaS.RestApiSDK.EntityMetadataDTO> get_all_entity_metadatas(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;

            var data = this.service.GetAllEntityMetaData(tenant_id.ToString());

            return data == null ? null : data.Values.ToViewModel(tenant_id.Value);
        }

        /// <summary>
        /// This method is used to get entity metadata for the given entity identifier.
        /// </summary>
        /// <param name="entity_id">entity identifier (e.g. Employee).</param>
        /// <param name="tenant_id">tenant identifier (optional).</param>
        /// <returns>specified entity metadata based on the entity identifier.</returns>
        public CelloSaaS.RestApiSDK.EntityMetadataDTO get_entity_metadata([Required]string entity_id, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;

            var data = this.service.GetEntityMetaData(entity_id, tenant_id.ToString());

            if (data == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid entity id was  passed!") });
            }

            return data.ToViewModel(tenant_id.Value);
        }

        /// <summary>
        /// This method is used to retrieves the custom field data for the given entity and reference identifier.
        /// </summary>
        /// <param name="ref_id">reference identifier.</param>
        /// <param name="entity_id">entity identifier.</param>
        /// <param name="tenant_id">tenant identifier (optional).</param>
        /// <returns>specified custom data based on the entity and reference identifier.</returns>
        public CelloSaaS.RestApiSDK.CustomDataDTO get_custom_data([Required]string ref_id, [Required]string entity_id, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;

            var data = this.service.GetExtendedEntityRow(entity_id, tenant_id.ToString(), ref_id.ToString());

            if (data == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid details are passed!") });
            }

            var vm = new CelloSaaS.RestApiSDK.CustomDataDTO
            {
                entity_id = entity_id,
                ref_id = ref_id.ToString(),
                tenant_id = tenant_id
            };

            if (data != null && data.ExtendedEntityColumnValues != null)
            {
                vm.data = data.ExtendedEntityColumnValues.Select(x => new CelloSaaS.RestApiSDK.FieldDataDTO
                {
                    field_id = x.Key,
                    value = x.Value.Value
                }).ToArray();
            }

            return vm;
        }

        /// <summary>
        /// This method is used to saves the given custom field data.
        /// </summary>
        /// <param name="data">custom field data details.</param>
        /// <returns>HttpResponseMessage (Created).</returns>
        public HttpResponseMessage put_custom_data([Required]CelloSaaS.RestApiSDK.CustomDataDTO data)
        {
            Guard.Null("CustomDataDTO", data);
            if (!data.tenant_id.HasValue) data.tenant_id = this.TenantId;

            var row = new ExtendedEntityRow
            {
                EntityIdentifier = data.entity_id,
                TenantId = data.tenant_id.ToString(),
                ReferenceId = data.ref_id.ToString(),
                ExtendedEntityColumnValues = new Dictionary<string, ExtendedEntityColumn>()
            };

            if (data.data != null)
            {
                foreach (var item in data.data)
                {
                    row.ExtendedEntityColumnValues.Add(item.field_id, new ExtendedEntityColumn
                    {
                        EntityFieldIdentifier = item.field_id,
                        Id = item.field_id,
                        Value = item.value
                    });
                }
            }

            this.service.SaveExtendedEntityRow(data.entity_id, data.tenant_id.ToString(), row, data.ref_id.ToString());

            return Request.CreateResponse(HttpStatusCode.Created, data);
        }

        /// <summary>
        /// This method is used to deletes the custom field data for the given reference identifier of the entity.
        /// </summary>
        /// <param name="ref_id">reference identifier.</param>
        /// <param name="entity_id">entity identifier.</param>
        /// <param name="tenant_id">tenant identifier (optional).</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        public HttpResponseMessage delete_custom_data([Required]string ref_id, [Required]string entity_id, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;

            var data = this.service.GetExtendedEntityRow(entity_id, tenant_id.ToString(), ref_id.ToString());

            if (data == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid details are passed!") });
            }

            this.service.DeleteExtendedEntityRow(entity_id, tenant_id.ToString(), ref_id.ToString());

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}
