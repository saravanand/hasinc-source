﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.EventScheduler.Model;
using CelloSaaS.EventScheduler.ServiceContracts;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.RestApiSDK;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the event system details such as raising events and fetch/searching event audit details.
    /// </summary>
    public class EventsController : CelloApiController
    {
        private IEventAuditService auditService;
        private IEventRegister registerService;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public EventsController(/*IEventRegister registerService*/)
        {
            this.auditService = ServiceLocator.Resolve<IEventAuditService>();
            this.registerService = ServiceLocator.Resolve<IEventRegister>();
        }
        /// <summary>
        /// This method is used to searches the event audit data from the given parameters.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <returns>collection of event details.</returns>
        [HttpPost]
        public IEnumerable<CelloSaaS.RestApiSDK.EventDTO> get_all_events(CelloSaaS.RestApiSDK.EventAduitSearchConditionDTO condition)
        {

            if (condition == null)
                condition = new EventAduitSearchConditionDTO();

            if (condition.pageSize <= 0) condition.pageSize = 100;
            if (condition.pageNo < 0) condition.pageNo = 1;

            var allevents = this.auditService.GetEventAuditDetails(new EventAuditSearchCondition
            {
                TenantId = this.TenantId.ToString(),
                FromLogTime = condition.fromLogTime,
                ToLogTime = condition.toLogTime,
                SortString = EventSortStringConstants.EventAuditCreatedOn,
                SortExpression = SortExpressionConstants.Decending,
                RecordStart = ((condition.pageNo - 1) * condition.pageSize) + 1,
                RecordEnd = (condition.pageNo * condition.pageSize)
            });
            return allevents != null && allevents.Result != null ? allevents.Result.Values.ToViewModel() : null;          
        }

        /// <summary>
        /// This method is used to retrieve the event audit details for the given event identifier.
        /// </summary>
        /// <param name="event_id">event audit identifier (mandatory).</param>
        /// <param name="tenant_id">tenant identifier (optional).</param>
        /// <returns>specified event details based on the event identifier.</returns>
        [HttpGet]
        public CelloSaaS.RestApiSDK.EventDTO get_event([Required]Guid event_id, Guid? tenant_id = null)
        {
            Guard.NullGuid("event_id",event_id, true);
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            var entity = this.auditService.GetEventAuditById(event_id.ToString(), tenant_id.Value.ToString());

            return entity != null ? entity.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to raises a new event.
        /// </summary>
        /// <param name="model">event details.</param>
        /// <returns>HttpResponseMessage (Created).</returns>
        [HttpPost]
        public HttpResponseMessage register_event(CelloSaaS.RestApiSDK.EventDTO model)
        {
            Guard.NullGuid("event_id", model.event_id, true);
            Guard.NullGuid("user_id",model.user_id, true);
            if (!model.tenant_id.HasValue||model.tenant_id.Value.Equals(Guid.Empty)) model.tenant_id = this.TenantId;

            var entity = model.ToBusinessEntity();

            this.registerService.RegisterEvent(entity);           
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }
    }
}
