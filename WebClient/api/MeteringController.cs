﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.UserManagement;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling metering.
    /// </summary>
    public class MeteringController : CelloApiController
    {
        IMeteringService service;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public MeteringController()
        {
            this.service = ServiceLocator.Resolve<IMeteringService>();
        }

        /// <summary>
        /// This method is used to retrieves the metering amount for the given usage code.
        /// </summary>
        /// <param name="usage_code">usage code.</param>
        /// <returns>used amount for the given usage.</returns>
        [HttpGet]
        public double get_meter_amount([Required]string usage_code)
        {
            Guard.NullOrEmpty("usage_code", usage_code);
            return this.service.GetMeterUsage(this.TenantId.ToString(), usage_code);
        }

        /// <summary>
        /// This method is used to increments the metering count by the given amount for the given usage.
        /// </summary>
        /// <param name="usage_code">usage code.</param>
        /// <param name="amount">amount.</param>
        /// <returns><c>True</c>, if the metering count is incremented successfully, <c>false</c> otherwise.</returns>
        [HttpPost]
        public bool increment([Required]string usage_code, [Required]double amount)
        {
            Guard.NullOrEmpty("usage_code", usage_code);            
            try
            {
                return this.service.IncrementMeterUsage(this.TenantId.ToString(), usage_code, amount);
            }
            catch (LicenseLimitException ex)
            {
                ExceptionService.HandleException(ex);
                return false;
            }
        }

        /// <summary>
        /// This method is used to decrements the metering count by the given amount for the given usage.
        /// </summary>
        /// <param name="usage_code">usage code.</param>
        /// <param name="amount">amount.</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        [HttpDelete]
        public HttpResponseMessage decrement([Required]string usage_code, [Required]double amount)
        {
            Guard.NullOrEmpty("usage_code", usage_code);
            this.service.DecrementMeterUsage(this.TenantId.ToString(), usage_code, amount);
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// This method is used to searches the metering audits.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <returns>search result.</returns>
        [HttpPost]
        public CelloSaaS.RestApiSDK.MeteringSearchResultDTO search(CelloSaaS.RestApiSDK.MeteringSearchConditionDTO condition)
        {
            if (condition == null) condition = new CelloSaaS.RestApiSDK.MeteringSearchConditionDTO();

            if (!condition.tenant_id.HasValue) condition.tenant_id = this.TenantId;
            if (condition.page <= 0) condition.page = 1;
            if (condition.page_size <= 0) condition.page_size = 100;

            var meteringSearchCondition = new MeteringSearchCondition
            {
                TenantId = condition.tenant_id.ToString(),
                UsageCode = condition.usage_code,
                PageNumber = condition.page,
                PageSize = condition.page_size,
                SetTotalCount = true
            };

            if (condition.user_id.HasValue) meteringSearchCondition.LoggedBy = condition.user_id.ToString();
            if (condition.start_date.HasValue) meteringSearchCondition.StartDate = condition.start_date.Value;
            if (condition.end_date.HasValue) meteringSearchCondition.EndDate = condition.end_date.Value;

            var search_result = this.service.SearchMeteringLog(meteringSearchCondition);

            var res = new CelloSaaS.RestApiSDK.MeteringSearchResultDTO
            {
                result = search_result.Result.Values.ToViewModel(),
                total_count = search_result.TotalCount,
                condition = condition
            };

            return res;
        }

        /// <summary>
        /// This method is used to searches the usage audits.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <returns>search result.</returns>
        [HttpPost]
        public CelloSaaS.RestApiSDK.UsageSearchResultDTO search_usage(CelloSaaS.RestApiSDK.UsageSearchConditionDTO condition)
        {
            if (condition == null) condition = new CelloSaaS.RestApiSDK.UsageSearchConditionDTO();

            if (!condition.tenant_id.HasValue) condition.tenant_id = this.TenantId;

            var searchCondition = new UsageSearchCondition
            {
                TenantId = condition.tenant_id.ToString(),
                UsageCodes = condition.usage_codes,
                ModuleCodes = condition.module_codes
            };

            var search_result = this.service.SearchUsageDetails(searchCondition);

            var res = new CelloSaaS.RestApiSDK.UsageSearchResultDTO
            {
                result = search_result.Result.ToViewModel(),
                total_count = search_result.TotalCount,
                condition = condition
            };

            return res;
        }
    }
}
