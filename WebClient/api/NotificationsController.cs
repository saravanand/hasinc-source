﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.Notification.Model;
using CelloSaaS.Notification.Model.Content;
using CelloSaaS.Notification.Model.Dispatch;
using CelloSaaS.Notification.NotificationManager;
using CelloSaaS.Notification.ServiceContracts;
using CelloSaaS.RestApiSDK;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling  email sending and searching audit details.
    /// </summary>
    public class NotificationsController : CelloApiController
    {
        private INotificationService service;
        private INotificationAuditService auditService;

        /// <summary>
        /// This constructor is used to initialize required services.
        /// </summary>
        public NotificationsController()
        {
            this.service = ServiceLocator.Resolve<INotificationService>();
            this.auditService = ServiceLocator.Resolve<INotificationAuditService>();
        }

        /// <summary>
        /// This method is used for searching the email audits.
        /// </summary>
        /// <param name="condition">search conditions.</param>
        /// <returns>search result.</returns>
        [HttpPost]
        public CelloSaaS.RestApiSDK.EmailAuditSearchResultDTO search_email_audits(CelloSaaS.RestApiSDK.EmailAuditSearchConditionDTO condition)
        {
            if (condition == null)
                condition = new EmailAuditSearchConditionDTO();

            if (!condition.tenant_id.HasValue) condition.tenant_id = this.TenantId;

            if (condition.page_no <= 0) condition.page_no = 1;
            if (condition.page_size <= 0) condition.page_size = 100;

            var searchCondition = new NotificationAuditSearchCondition
            {
                DestinationType = NotificationDestinationType.Email,
                EndDate = condition.end_date,
                StartDate = condition.start_date,
                UserId = condition.user_id.HasValue ? condition.user_id.Value.ToString() : null,
                TenantId = condition.tenant_id.Value.ToString(),
                TrackId = condition.track_id,
                NotificationId = condition.notification_id,
                NotificationName = condition.notification_name,
                PageSize = condition.page_size,
                PageNumber = condition.page_no,
                SetTotalCount = true
            };

            var audits = this.auditService.SearchNotificationAuditDetails(searchCondition);

            var result = new CelloSaaS.RestApiSDK.EmailAuditSearchResultDTO
            {
                result = audits.Result.ToViewModel(),
                search_condition = condition,
                total_count = audits.TotalCount
            };

            return result;
        }

        /// <summary>
        /// This method is used to sends email.
        /// </summary>
        /// <param name="data">email details.</param>
        /// <returns>HttpResponseMessage (Accepted).</returns>
        [HttpPost]
        public HttpResponseMessage send_email(CelloSaaS.RestApiSDK.EmailDTO data)
        {
            if (!data.tenant_id.HasValue) data.tenant_id = this.TenantId;

            ValidateEmailAddress(data.to, "to");
            ValidateEmailAddress(data.cc, "cc");
            ValidateEmailAddress(data.bcc, "bcc");

            NotificationDetails details = null;

            if (data.notification_id.HasValue)
            {
                details = this.service.GetNotificationDetails(data.notification_id.Value.ToString(), data.tenant_id.ToString());
            }
            else if (!string.IsNullOrEmpty(data.notification_name))
            {
                details = this.service.GetNotificationDetailsByName(data.notification_name, data.tenant_id.ToString());
            }
            else
            {
            }

            if (data == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("Notification configuration for the given id/name not found"),
                    ReasonPhrase = "Notification configuration not found"
                });
            }

            var notificationDestination = (EmailDestination)details.NotificationDestination;
            var textMessageContent = (TextMessageContent)details.NotificationContent;

            textMessageContent.Placeholder = new NotificationPlaceholder { Keys = data.placeholders };
            notificationDestination.RecipientAddress = data.to;
            notificationDestination.SecondaryRecipientAddress = string.IsNullOrEmpty(data.cc) ? null : data.cc.Split(';');
            notificationDestination.Bcc = string.IsNullOrEmpty(data.bcc) ? null : data.bcc.Split(';');

            if (!string.IsNullOrEmpty(data.from))
            {
                notificationDestination.SenderAddress = data.from;
            }

            notificationDestination.TrackId = data.track_id;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    this.service.ProcessAndSendNotification(details.NotificationId, data.tenant_id.ToString(), textMessageContent, notificationDestination);
                }
                catch (NotificationFailedException ex)
                {
                    ExceptionService.HandleException(ex, null);
                }
            });

            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        /// <summary>
        /// This method is used to validates the email address.
        /// </summary>
        /// <param name="address">email address.</param>
        /// <param name="field">field.</param>
        private static void ValidateEmailAddress(string address, string field)
        {
            if (!string.IsNullOrEmpty(address))
            {
                if (!address.Contains(';'))
                {
                    if (!Util.ValidateEmailId(address))
                    {
                        throw new ArgumentException(string.Format("Invalid email address in {0} field", field));
                    }
                }
                else
                {
                    foreach (var id in address.Split(';'))
                    {
                        if (!Util.ValidateEmailId(id))
                        {
                            throw new ArgumentException(string.Format("Invalid email address in {0} field", field));
                        }
                    }
                }
            }
        }
    }
}
