﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Billing.ServiceContracts;
using CelloSaaS.Library;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the packages.
    /// </summary>
    public class PackagesController : CelloApiController
    {
        private ILicenseService service;
        private IBillingPlanService planService;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public PackagesController()
        {
            this.service = ServiceLocator.Resolve<ILicenseService>();
            this.planService = ServiceLocator.Resolve<IBillingPlanService>();
        }

        /// <summary>
        /// This method used to get the all the packages by tenant identifier.
        /// </summary>
        /// <param name="tenant_id">tenant unique identifier.</param>
        /// <returns>collection of all packages.</returns>
        public IEnumerable<CelloSaaS.RestApiSDK.PackageDTO> get_all_packages(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;
            //Get the all packages by tenant Id.
            var result = this.service.GetAllPackageDetails(tenant_id.ToString());

            var lst = new List<CelloSaaS.RestApiSDK.PackageDTO>();

            if (result != null)
            {
                foreach (var item in result.Values)
                {
                    var vm = item.ToViewModel();

                    //Get the all price plan by tenant Id and package id.
                    var plans = this.planService.GetPricePlansByPackageId(Guid.Parse(item.PackageId), tenant_id.Value);

                    if (plans != null)
                    {
                        vm.plans = plans.Values.ToViewModel().ToArray();
                    }
                    lst.Add(vm);
                }
            }

            return lst;
        }

        /// <summary>
        /// This method is used to get the package details by package identifier.
        /// </summary>
        /// <param name="package_id">package identifier(mandatory).</param>
        /// <returns>package details.</returns>
        public CelloSaaS.RestApiSDK.PackageDTO get_package([Required]Guid package_id)
        {
            //Get the package details by package identifier.
            var entity = this.service.GetPackageDetailsByPackageId(package_id.ToString());

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            var vm = entity.ToViewModel();

            //Get the price plan details by package identifier and tenant identifier.
            var plans = this.planService.GetPricePlansByPackageId(Guid.Parse(entity.PackageId), Guid.Parse(entity.TenantId));

            if (plans != null)
            {
                vm.plans = plans.Values.ToViewModel().ToArray();
            }

            return vm;
        }
    }
}
