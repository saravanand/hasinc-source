﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.ServiceContracts.Configuration;
using CelloSaaS.RestApiSDK;

namespace WebApplication.api
{
    /// <summary>
    /// This endpoint is responsible for managing the pickup list and its values.
    /// </summary>
    public class PickuplistsController : CelloApiController
    {
        IPickupListService service;
        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public PickuplistsController()
        {
            this.service = ServiceLocator.Resolve<IPickupListService>();
        }

        /// <summary>
        /// This method is used to get all pickup list.
        /// </summary>
        /// <returns>collection of pickup list.</returns>
        [HttpGet]
        public IEnumerable<PickuplistDTO> get_pickup_lists()
        {
            // Get pickup lists for tenant
            var lst = this.service.GetAllPickupLists(this.TenantId.ToString());

            var vm = lst.ToViewModel();

            if (vm != null)
            {
                foreach (var item in vm)
                {
                    var values = this.service.GetPickupListValues(item.id.ToString(), this.TenantId.ToString());
                    item.values = values.ToViewModel();
                }
            }

            return vm;
        }

        /// <summary>
        /// This method is used to get the pickup list value by pickup list identifier.
        /// </summary>
        /// <param name="pickuplist_id">pickup list identifier.</param>
        /// <returns>pickup list details.</returns>
        [HttpGet]
        public PickuplistDTO get_pickup_list([Required]Guid pickuplist_id)
        {
            // Get pickup list details by pickup list identifier.
            var entity = this.service.GetPickupListDetails(pickuplist_id.ToString());

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("Invalid Pickup list id is passed.!"),
                });
            }

            var values = this.service.GetPickupListValues(entity.Id, this.TenantId.ToString());

            var vm = entity.ToViewModel();
            vm.values = values.ToViewModel();
            return vm;
        }

    }
}
