﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.ProductAnalytics;
using CelloSaaS.ServiceContracts.ProductAnalytics;
using CelloSaaS.RestApiSDK;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the product analytics.
    /// </summary>
    public class ProductAnalyticsController : CelloApiController
    {
        private IProductAnalyticsService service;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public ProductAnalyticsController(/*IProductAnalyticsService service*/)
        {
            this.service = ServiceLocator.Resolve<IProductAnalyticsService>();
        }
        /// <summary>
        /// This method is used to gets the product analytics.
        /// </summary>
        /// <param name="condition">condition.</param>
        /// <returns>collection of product analytics.</returns>
        [HttpPost]
        public IEnumerable<CelloSaaS.RestApiSDK.ProductAnalyticsDTO> get_all_product_analytics(ProductAnalyticsSearchConditionDTO condition)
        {
            Guard.Null("condition", condition, true);
            var results = this.service.GetProductAnalyticsLog(new ProductAnalyticsSearchCondition
            {
                TenantId = condition.tenant_id.ToString(),
                FromDate = condition.from_date,
                ToDate = condition.to_date,
                UserId = condition.user_id.ToString(),
                HasError = condition.has_error,
                ResponseTime = Convert.ToInt32(condition.avg_response_time),
                SortString = "ProductAnalytics_TimeStamp",
                SortDirection = SortExpressionConstants.Decending,
                Offset = ((condition.page_no - 1) * condition.page_size) + 1,
                PageSize = condition.page_size
            });

            return results != null ? results.Values.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to add the product analytics.
        /// </summary>
        /// <param name="vm">required data to add product analytics.</param>
        /// <returns>HttpResponseMessage (Created).</returns>
        public HttpResponseMessage post_product_analytics([Required]CelloSaaS.RestApiSDK.ProductAnalyticsDTO vm)
        {
            Guard.Null("vm", vm, true);
            if (!vm.tenant_id.HasValue) vm.tenant_id = this.TenantId; 

            var entity = vm.ToBusinessEntity();
            entity.CreatedOn = DateTime.Now;
            entity.UserId = entity.CreatedBy = this.UserId.ToString();

            this.service.AddProductAnalytics(entity);
            vm.id = Guid.Parse(entity.Id);
            var response = Request.CreateResponse<CelloSaaS.RestApiSDK.ProductAnalyticsDTO>(HttpStatusCode.Created, vm);
            return response;
        }
    }
}
