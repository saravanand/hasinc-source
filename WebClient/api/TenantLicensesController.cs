﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using System.ComponentModel.DataAnnotations;
namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the tenant license.
    /// </summary>
    public class TenantLicensesController : CelloApiController
    {
        private ILicenseService service;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public TenantLicensesController()
        {
            this.service = ServiceLocator.Resolve<ILicenseService>();
        }

        /// <summary>
        /// This method is used to get tenant license history details.
        /// </summary>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>collection of tenant license details.</returns>
        public IEnumerable<CelloSaaS.RestApiSDK.TenantLicenseDTO> get_all_tenant_licenses(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;
            var result = this.service.GetTenantLicenseHistory(tenant_id.ToString());
            return result.ToViewModel();
        }

        /// <summary>
        /// This method is used to get the tenant license modules, usages, features and privileges.
        /// </summary>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>tenant license details.</returns>
        public CelloSaaS.RestApiSDK.TenantLicenseDTO get_tenant_license(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;
            var entity = this.service.GetTenantLicense(tenant_id.ToString());

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return entity.ToViewModel();
        }

        /// <summary>
        /// This method is used to update tenant license.
        /// </summary>
        /// <param name="vm">required data to update tenant license.</param>
        public void put_tenant_license(CelloSaaS.RestApiSDK.TenantLicenseDTO vm)
        {
            if (!vm.tenant_id.HasValue) vm.tenant_id = this.TenantId;

            var old_entity = this.service.GetTenantLicense(vm.tenant_id.ToString());

            if (old_entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            TenantLicense entity = vm.ToBusinessEntity(old_entity);
            this.service.UpdateTenantLicense(entity);
        }

        /// <summary>
        /// This method is used to deactivate tenant subscription.
        /// </summary>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        public HttpResponseMessage delete_tenant_license(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;
            this.service.DeactivateTenantLicense(tenant_id.ToString());
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// This method is used to activate tenant subscription.
        /// </summary>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        [HttpGet]
        public HttpResponseMessage activate_tenant_license(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;
            this.service.ActivateTenantLicense(tenant_id.ToString());
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// This method is used to validate tenant have license on accessing date.
        /// </summary>
        /// <param name="tenant_id">tenant identifier(mandatory).</param>
        /// <returns><c>True</c>, if the tenant license is valid, <c>false</c> otherwise.</returns>
        [HttpGet]
        public bool validate_tenant_license(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue)
            tenant_id = this.TenantId;
            var result=this.service.ValidateTenantLicense(tenant_id.ToString());         
            return result;
        }
        /// <summary>
        /// This method is used to get all the feature details for each modules.
        /// </summary>
        /// <param name="module_ids">array of module identifier.</param>
        /// <returns>collection of features.</returns>
        [HttpPost]
        public Dictionary<string, Dictionary<string, CelloSaaS.RestApiSDK.FeatureDTO>> get_feature_by_moduleIds([Required]string[] module_ids)
        {
            var entity = this.service.GetFeatureByModuleIds(module_ids);
            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return entity.ToViewModel();            
        }

        /// <summary>
        /// This method is used to get usage details based on module identfiers.
        /// </summary>
        /// <param name="module_ids">module identifiers.</param>
        /// <returns>colelction of usage details.</returns>     
        [HttpPost]
        public Dictionary<string, Dictionary<string, CelloSaaS.RestApiSDK.UsageDTO>> get_usage_by_moduleIds([Required]string[] module_ids)
        {
            var entity = this.service.GetUsageByModuleIds(module_ids);
            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return entity.ToViewModel();
        }
    }
}