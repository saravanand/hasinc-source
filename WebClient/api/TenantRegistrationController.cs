﻿using CelloSaaS.Billing.Model;
using CelloSaaS.Billing.ServiceContracts;
using CelloSaaS.Library;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.RestApiSDK;
using CelloSaaS.ServiceContracts.Configuration;
using CelloSaaS.ServiceContracts.LicenseManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Billing.ServiceProxies;
namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for add a tenant (tenant registration).
    /// </summary>
    public class TenantRegistrationController : CelloApiController
    {
        private IPickupListService pickuplistservice;
        private IBillingPlanService planService;
        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public TenantRegistrationController()
        {
            this.pickuplistservice= ServiceLocator.Resolve<IPickupListService>();
            this.planService = ServiceLocator.Resolve<IBillingPlanService>();
        }
        
        /// <summary>
        /// This method is used to add a tenant.
        /// </summary>
        /// <param name="tenantRegistrationModel">tenant registration model(mandatory).</param>
        /// <returns>HttpResponseMessage (Created) along with tenant identifier.</returns>
        public HttpResponseMessage put_tenant_self_registration([Required]TenantRegistrationDTO tenantRegistrationModel)
        {
            var tenantId = Register(tenantRegistrationModel);
            var response = Request.CreateResponse(HttpStatusCode.Created, Guid.Parse(tenantId));
            return response;
        }

        /// <summary>
        /// This method is used to check whether the price plan map code is Valid against the given package and tenant identifier.
        /// </summary>
        /// <param name="planmap_code">price plan map code.</param>
        /// <param name="planmap_pickuplist_id">price plan map code pickup list identifier.</param>
        /// <param name="package_id">package identifier.</param>
        /// <param name="parenttenant_id">parent tenant identifier.</param>
        /// <returns>
        /// <c>True</c>, if the plan map code is valid, <c>false</c> otherwise.
        /// </returns>
        [HttpGet]
        public bool is_priceplan_mapcode_Valid([Required]Guid planmap_code, [Required]Guid planmap_pickuplist_id, [Required]Guid package_id, [Required]Guid parenttenant_id)
        {
            Guard.NullGuid("planmap_code", planmap_code,true);
            Guard.NullGuid("planmap_pickuplist_id", planmap_pickuplist_id, true);
            Guard.NullGuid("package_id", package_id, true);
            Guard.NullGuid("parenttenant_id", parenttenant_id, true);
            try
            {
                return IsPricePlanMapCodeValid(planmap_code, planmap_pickuplist_id, package_id, parenttenant_id);
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex);
                return false;
            }
        }

        /// <summary>
        /// This method is used to get the packages for self registration based on the billing frequency.
        /// </summary>
        /// <param name="frequency">billing frequency.</param>
        /// <param name="default_priceplan_pickuplist_id">default price plan identifier.</param>
        /// <returns>collection of license packages.</returns>
        [HttpGet]
        public IEnumerable<CelloSaaS.RestApiSDK.PackageDTO> packages_for_self_registration([Required]BillingFrequency frequency, Guid? default_priceplan_pickuplist_id)
        {

            Guard.Null("frequency", frequency);

            var entity = GetPackagesForSelfRegistration();

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            var lst = new List<CelloSaaS.RestApiSDK.PackageDTO>();

            BillFrequency billingFrequency = (BillFrequency)Enum.Parse(typeof(BillFrequency), frequency.ToString());
            List<PickupListValue> defaultPricePlanValues = null;
            if (default_priceplan_pickuplist_id.HasValue && default_priceplan_pickuplist_id.Value != Guid.Empty)
            {
                defaultPricePlanValues = pickuplistservice.GetPickupListValues(default_priceplan_pickuplist_id.ToString(), this.TenantId.ToString());
            }

            foreach (var item in entity)
            {
                //Get the all price plan by tenant Id and package id.
                var plans = planService.GetPricePlansByPackageId(Guid.Parse(item.LicensepackageId), this.TenantId);

                if (plans != null)
                {
                    string defaultPricePlanId = null;
                    if (defaultPricePlanValues != null && defaultPricePlanValues.Count > 0 && defaultPricePlanValues.Any(c => c.ItemName.Equals(item.LicensepackageId)))
                    {
                        defaultPricePlanId = defaultPricePlanValues.Where(c => c.ItemName.Equals(item.LicensepackageId)).Select(c => c.ItemId).FirstOrDefault();
                    }

                    PricePlan defaultPricePlan = null;
                    if (!string.IsNullOrWhiteSpace(defaultPricePlanId))
                    {
                        defaultPricePlan = plans.Values.Where(c => c.Id.Equals(defaultPricePlanId) && c.BillFrequency.Equals(billingFrequency)).Select(c => c).FirstOrDefault(); 
                    }

                    if (defaultPricePlan == null)
                    {
                        defaultPricePlan = plans.Values.Where(c => c.BillFrequency.Equals(billingFrequency)).OrderBy(c => c.CreatedOn).FirstOrDefault();
                    }

                    if (defaultPricePlan != null)
                    {
                        var vm = item.ToViewModel();
                        vm.plans = new PricePlanDTO[] { defaultPricePlan.ToViewModel() };
                        lst.Add(vm);
                    }                  
                }
            }
            return lst;
        }

        #region Private Members

        /// <summary>
        /// Gets the Extended Rows
        /// </summary>
        /// <param name="tenantRegistrationModel"></param>
        /// <returns></returns>
        private static ExtendedEntityRow GetExtendedRows(TenantRegistrationDTO tenantRegistrationModel)
        {
            if (tenantRegistrationModel.additional_fields == null || tenantRegistrationModel.additional_fields.Count < 1)
            {
                return null;
            }

            var extendedRow = new ExtendedEntityRow
            {
                EntityIdentifier = new TenantDetails().EntityIdentifier,
                ExtendedEntityColumnValues = new Dictionary<string, ExtendedEntityColumn>(),
                TenantId = UserIdentity.TenantID
            };

            foreach (var field in tenantRegistrationModel.additional_fields)
            {
                string fieldId = field.Key;

                //check for extn prefix, if not found, add that to the field
                if (!field.Key.StartsWith("Extn_", StringComparison.OrdinalIgnoreCase))
                {
                    fieldId = "Extn_" + field.Key;
                }

                if (extendedRow.ExtendedEntityColumnValues.ContainsKey(fieldId))
                {
                    continue;
                }

                extendedRow.ExtendedEntityColumnValues.Add(fieldId,
                    new ExtendedEntityColumn
                    {
                        EntityFieldIdentifier = fieldId,
                        Value = field.Value
                    });
            }
            return extendedRow;
        }

        /// <summary>
        /// Gets the TenantLicense From Model
        /// </summary>
        /// <param name="tenantRegistrationModel"></param>
        /// <returns></returns>
        private static TenantLicense GetTenantLicenseFromModel(TenantRegistrationDTO tenantRegistrationModel)
        {
            return new TenantLicense
            {
                PackageId = tenantRegistrationModel.package_id.ToString(),
                PricePlanId = tenantRegistrationModel.priceplan_id.ToString(),
                //ValidityStart = tenantRegistrationModel.RegistrationDate
                ValidityStart = CelloSaaS.Library.Helpers.DateTimeHelper.GetDatabaseDateTime()
            };
        }

        /// <summary>
        /// Gets the TenantDetails From the tenant registration Model
        /// </summary>
        /// <param name="tenantRegistrationModel"></param>
        /// <returns></returns>
        private static Tenant GetTenantDetailFromModel(TenantRegistrationDTO tenantRegistrationModel)
        {
            var tenant = new Tenant
            {
                //Types = tenantRegistrationModel.TenantType,
                ContactDetail = new ContactDetails
                {
                    FirstName = tenantRegistrationModel.first_name,
                    LastName = tenantRegistrationModel.last_name,
                    Email = tenantRegistrationModel.email_id,
                    Phone = tenantRegistrationModel.contact_number,
                    Status = true,
                    CreatedBy = UserIdentity.UserId,
                    CreatedOn = tenantRegistrationModel.registration_date
                },
                TenantAdminMembershipdetail = new MembershipDetails
                {
                    EmailId = tenantRegistrationModel.email_id,
                    IsFirstTimeUser = true,
                    UserName = string.IsNullOrEmpty(tenantRegistrationModel.user_name)
                                ? tenantRegistrationModel.email_id
                                : tenantRegistrationModel.user_name,
                    Password = tenantRegistrationModel.password,
                    Status = true,
                    CreationDate = tenantRegistrationModel.registration_date
                },
                TenantAdminUserdetail = new User
                {
                    FirstName = tenantRegistrationModel.first_name,
                    LastName = tenantRegistrationModel.last_name,
                    CreatedBy = UserIdentity.UserId,
                    CreatedOn = tenantRegistrationModel.registration_date,
                    Status = true
                },
                TenantDetails = new TenantDetails
                {
                    ApprovalStatus = tenantRegistrationModel.approval_status == null ? TenantApprovalStatus.WAITINGFORAPPROVAL : tenantRegistrationModel.approval_status.ToString(),
                    CompanySize = null,
                    CreatedBy = UserIdentity.UserId,
                    CreatedOn = tenantRegistrationModel.registration_date,
                    Description = null,
                    IsSelfRegistered = true,
                    EnableAutoDebit = true,
                    IsProductAdmin = false,
                    ParentTenantId = UserIdentity.TenantID,
                    Status = true,
                    TenantName = tenantRegistrationModel.friendly_name,
                    URL = tenantRegistrationModel.url,
                    Website = tenantRegistrationModel.website,
                    TenantCodeString = tenantRegistrationModel.friendly_code,
                },
                Address = null
            };
            if (tenantRegistrationModel.data_partition_id != Guid.Empty)
            {
                tenant.TenantDetails.DataPartitionId = GetDefaultDataPartitionId();
            }
            else
            {
                tenant.TenantDetails.DataPartitionId = tenantRegistrationModel.data_partition_id.ToString();
            }

            return tenant;
        }

        /// <summary>
        /// Returns the default data partition configured
        /// </summary>
        /// <returns></returns>
        private static string GetDefaultDataPartitionId()
        {
            try
            {
                var dataPartition = CelloSaaS.PartitionManagement.ServiceProxies.DataPartitionProxy.GetDefaultPartitionId();
                return dataPartition == Guid.Empty ? null : dataPartition.ToString();
            }
            catch (Exception ex)
            {
                ExceptionService.HandleException(ex);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tenantRegistrationModel"></param>
        /// <param name="parentTenantId"></param>
        /// <returns></returns>
        private static string GetPricePlanIdByMapCode(CelloSaaS.RestApiSDK.TenantRegistrationDTO tenantRegistrationModel, string parentTenantId)
        {
            if (string.IsNullOrEmpty(tenantRegistrationModel.plan_mapcode) || string.IsNullOrEmpty(tenantRegistrationModel.planmap_pickuplist_id))
            {
                return null;
            }
            // get the pickup list values for the discount code
            List<PickupListValue> lstPickupListValues = ServiceLocator.Resolve<IPickupListService>().GetAllPickupListValues(tenantRegistrationModel.planmap_pickuplist_id, parentTenantId);

            // No discount code mapping is made, the discount code is still invalid
            if (lstPickupListValues == null || lstPickupListValues.Count < 1)
            {
                return null;
            }

            IEnumerable<PickupListValue> matches = lstPickupListValues.Where(lpv => lpv.ItemName.Equals(tenantRegistrationModel.plan_mapcode, StringComparison.OrdinalIgnoreCase));

            // priceplan Id, if there is a match, False otherwise
            string pricePlanId = (matches != null && matches.Count() > 0) ? matches.FirstOrDefault().ItemId : null;

            if (string.IsNullOrEmpty(pricePlanId))
            {

                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The provided plan map code is not found in the System"),
                });
            }

            var pricePlans = ServiceLocator.Resolve<IBillingPlanService>().GetPricePlansByPackageId(tenantRegistrationModel.package_id, Guid.Parse(parentTenantId));

            // invalid price plan mapping, return the discount code too as invalid
            // check if there is a match for any id in the discounted price plan
            if (pricePlans == null || pricePlans.Count < 1)
            {
                return null;
            }

            List<Guid> discountedPricePlans = pricePlanId.Contains(',')
                                                ? pricePlanId.Split(',').Select(ppl => Guid.Parse(ppl)).ToList()
                                                : new List<Guid> { Guid.Parse(pricePlanId) };

            if (discountedPricePlans == null || discountedPricePlans.Count < 1)
            {

                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The plan map code is not mapped with the chosen package"),
                });
            }

            Guid matchedPricePlans = pricePlans.Keys.ToList().Where(ppl => discountedPricePlans.Contains(ppl)).FirstOrDefault();

            if (matchedPricePlans == Guid.Empty)
            {

                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The plan map code is not mapped with the chosen package"),
                });
            }

	    if (!string.IsNullOrEmpty(pricePlanId) && !pricePlans.Any(pp => pp.Value.Id.Equals(Guid.Parse(pricePlanId))))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("The price plan id is not valid for this package"),
                });
            }

            pricePlanId = (matchedPricePlans != Guid.Empty) ? matchedPricePlans.ToString() : null;

            // there is a valid priceplan for this license package
            return pricePlanId;
        }


        /// <summary>
        /// To Register a tenant
        /// </summary>
        /// <param name="tenantRegistrationModel">Tenant Registration Model</param>
        /// <returns>Tenant Identifier for the registered tenant</returns>
        private string Register(CelloSaaS.RestApiSDK.TenantRegistrationDTO tenantRegistrationModel)
        {
            Guard.Null("TenantRegistrationDTO", tenantRegistrationModel);
            Guard.NullGuid("TenantRegistrationDTO.package_id", tenantRegistrationModel.package_id, true);
            Guard.NullOrEmpty("tenantRegistrationModel.user_name", tenantRegistrationModel.user_name, true);
            Guard.NullOrEmpty("tenantRegistrationModel.last_name", tenantRegistrationModel.last_name, true);
            Guard.NullOrEmpty("tenantRegistrationModel.contact_number", tenantRegistrationModel.contact_number, true);
            Guard.NullOrEmpty("tenantRegistrationModel.user_name", tenantRegistrationModel.user_name, true);

            Tenant tenant = GetTenantDetailFromModel(tenantRegistrationModel);

            var tenantService = ServiceLocator.Resolve<CelloSaaS.ServiceContracts.TenantManagement.ITenantService>();

            var parentTenant = tenantService.GetTenantDetailsByTenantId(tenant.TenantDetails.ParentTenantId);

            if (parentTenant != null && parentTenant.TenantDetails != null)
            {
                tenant.TenantDetails.ParentTenant = parentTenant.TenantDetails;
            }
            else
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("Unable to fetch the parent tenant details"),
                    });
            }

            string priceplanId = GetPricePlanIdByMapCode(tenantRegistrationModel, tenant.TenantDetails.ParentTenantId);

            // Convert into tenant license
            TenantLicense tenantLicense = GetTenantLicenseFromModel(tenantRegistrationModel);

            if (!string.IsNullOrEmpty(priceplanId))
            {
                tenantLicense.PricePlanId = priceplanId;
            }

            // Get extended Rows
            ExtendedEntityRow extendedRow = GetExtendedRows(tenantRegistrationModel);
            tenant.TenantDetails.ExtendedRow = extendedRow;

            string tenantId = tenantService.ProvisionTenant(tenant, tenantLicense);

            if (!ConfigHelper.ApplyGlobalBillSettingForPrepaidMode && !string.IsNullOrEmpty(tenantId)
                && !string.IsNullOrEmpty(tenantLicense.PricePlanId))
            {
                var gTenantId = Guid.Parse(tenantId);
                var gParentTenantId = Guid.Parse(tenant.TenantDetails.ParentTenantId);
                var pricePlan = ServiceLocator.Resolve<IBillingPlanService>().GetPricePlan(Guid.Parse(tenantLicense.PricePlanId), gParentTenantId);
                if (pricePlan == null)
                {
                    throw new Exception ("Invalid price plan passed!.");
                }
                var frequency = pricePlan.BillFrequency;
                var globalBillSetting = ServiceLocator.Resolve<IBillingService>().GetBillingSetting(frequency, gParentTenantId);

                if (globalBillSetting.Mode == BillCycleMode.Prepaid)
                {
                    var startDay = tenantLicense.TrialEndDate.HasValue ? tenantLicense.TrialEndDate.Value.Day : tenantLicense.ValidityStart.Day;

                    var billSetting = new BillingSetting
                    {
                        Id = Guid.Empty,
                        TenantId = gTenantId,
                        Mode = globalBillSetting.Mode,
                        BillFrequency = frequency,
                        ChargeDay = startDay + (globalBillSetting.ChargeDay - globalBillSetting.StartDay),
                        StartDay = startDay,
                    };
                    ServiceLocator.Resolve<IBillingService>().AddBillingSetting(billSetting);
                }
            }
            return tenantId;
        }

        /// <summary>
        /// Checks whether the PricePlanMapCode is Valid against the given package and tenantid
        /// </summary>
        /// <param name="planMapCode">The Price Plan Map Code</param>
        /// <param name="planMapPickupListId">Price Plan Map Code pickuplist id</param>
        /// <param name="packageId">The License Package Id</param>
        /// <param name="parentTenantId">The Parent tenant Identifier</param>
        /// <returns>
        /// <c>True</c>, if the plan map code is valid, <c>false</c> otherwise
        /// </returns>
        private bool IsPricePlanMapCodeValid(Guid planMapCode, Guid planMapPickupListId, Guid packageId, Guid parentTenantId)
        {
            Guard.NullGuid("planMapCode", planMapCode);
            Guard.NullGuid("planMapPickupListId", planMapPickupListId, true);
            Guard.NullGuid("packageId", packageId, true);
            Guard.NullGuid("parentTenantId", parentTenantId, true);
            // get the pickup list values for the discount code
            var lstPickupListValues = ServiceLocator.Resolve<IPickupListService>().GetAllPickupListValues(planMapPickupListId.ToString(), parentTenantId.ToString());

            // No discount code mapping is made, the discount code is still invalid
            if (lstPickupListValues == null || lstPickupListValues.Count < 1)
            {
                return false;
            }

            IEnumerable<PickupListValue> matches = lstPickupListValues.Where(lpv => lpv.ItemName.Equals(planMapCode.ToString(), StringComparison.OrdinalIgnoreCase));

            // priceplan Id, if there is a match, False otherwise
            string pricePlanId = (matches != null && matches.Count() > 0) ? matches.FirstOrDefault().ItemId : null;

            if (string.IsNullOrEmpty(pricePlanId))
            {
                //return null;
                //throw new PlanMapCodeMismatchException("The provided plan map code is not found in the System");
                return false;
            }

            // get the priceplans per package
            var pricePlans = ServiceLocator.Resolve<IBillingPlanService>().GetPricePlansByPackageId(packageId, parentTenantId);

            // invalid price plan mapping, return the discount code too as invalid
            // check if there is a match for any id in the discounted price plan
            if (pricePlans == null || pricePlans.Count < 1)
            {
                return false;
            }

            List<Guid> discountedPricePlans = pricePlanId.Contains(',')
                                                ? pricePlanId.Split(',').Select(ppl => Guid.Parse(ppl)).ToList()
                                                : new List<Guid> { Guid.Parse(pricePlanId) };


            Guid matchedPricePlans = pricePlans.Keys.ToList().Where(ppl => discountedPricePlans.Contains(ppl)).FirstOrDefault();

            if (matchedPricePlans == Guid.Empty)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get the packages for Self-Registration based on the billing frequency
        /// </summary>
        /// <param name="defaultPricePlanPickupListId">The default price plan identifier</param>
        /// <param name="frequency">The billing frequency</param>
        /// <returns>List of License Packages</returns>
        private List<LicensePackage> GetPackagesForSelfRegistration()
        {
            string parentTenantId = UserIdentity.TenantID;
            List<LicensePackage> availablePackages = ServiceLocator.Resolve<ILicenseService>().GetAllLicensePackage(parentTenantId);

            var pickuplistService = ServiceLocator.Resolve<IPickupListService>();

            var lstPickupListValues = pickuplistService.GetAllPickupListValues(CelloSaaS.Model.PickupListConstants.SelfRegistrationPackagePickupList, parentTenantId.ToString()) ?? new List<PickupListValue>();

            var pickupIds = lstPickupListValues.Where(x => x.Status == true).Select(x => x.ItemId);
            return availablePackages.Where(x => pickupIds.Contains(x.LicensepackageId)).ToList(); ;

        }

        #endregion
    }
}
