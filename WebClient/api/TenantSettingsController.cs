﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.ServiceContracts.SettingsManagement;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the tenant settings.
    /// </summary>
    public class TenantSettingsController : CelloApiController
    {
        private ITenantSettingsService service;
        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public TenantSettingsController()
        {
            //this.service = service;
            this.service = ServiceLocator.Resolve<ITenantSettingsService>();
        }

        /// <summary>
        /// This method is used to get settings of a tenant.
        /// </summary>
        /// <param name="tenant_id">tenant identifier (mandatory).</param>
        /// <returns>collection of tenant setting details.</returns>      
        public Dictionary<string, string> get_tenant_setting(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;
            var entity = this.service.GetTenantSettings(tenant_id.ToString());

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return entity.Setting.Attributes;
        }
        
        /// <summary>
        /// This method is used to get the tenant settings based on the tenant identifier and attribute set identifier.
        /// </summary>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <param name="attributeSet_id">attribute set identifier.</param>
        /// <returns>collection of tenant settings</returns>
        public Dictionary<string, string> get_tenant_setting(Guid tenant_id, string attributeSet_id)
        {
            var entity = this.service.GetTenantSettings(tenant_id.ToString(), attributeSet_id);
            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return entity.Setting.Attributes;
        }

        /// <summary>
        /// This method is used to update the tenant settings for a tenant.
        /// Check if tenant settings exist for a given tenant identifier and attribute set identifier. 
        /// If so update else insert tenant setting details.
        /// </summary>
        /// <param name="attributes">list of attributes.</param>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        public HttpResponseMessage put_tenant_setting([FromBody]Dictionary<string, string> attributes, [FromUri]Guid? tenant_id = null)
        {
            if (attributes == null || attributes.Count == 0)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Send at-least one attribute");
            }

            if (!tenant_id.HasValue) tenant_id = this.TenantId;
            var entity = this.service.GetTenantSettings(tenant_id.ToString());

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            foreach (var item in attributes)
            {
                if (entity.Setting.Attributes.ContainsKey(item.Key))
                {
                    entity.Setting.Attributes[item.Key] = item.Value;
                }
                else
                {
                    entity.Setting.Attributes.Add(item.Key, item.Value);
                }
            }

            this.service.UpdateTenantSettings(entity);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// This method is used to delete tenant settings for given tenant and attribute Identifier.
        /// </summary>
        /// <param name="attribute_id">attribute set identifier (mandatory).</param>
        /// <param name="tenant_id">tenant identifier (mandatory).</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        public HttpResponseMessage delete_tenant_setting(string attribute_id = null, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue) tenant_id = this.TenantId;

            if (!string.IsNullOrEmpty(attribute_id))
            {
                this.service.DeleteTenantSettingsByAttribute(tenant_id.ToString(), attribute_id);
            }
            else
            {
                this.service.PermanentDeleteTenantSettings(tenant_id.ToString());
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
