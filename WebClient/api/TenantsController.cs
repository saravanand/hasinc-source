﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Billing.ServiceContracts;
using CelloSaaS.Library;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.ServiceContracts.MasterData;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.RestApiSDK;
using System.Globalization;

namespace WebApplication.api
{
    /// <summary>
    /// These endpoints are responsible for handling the tenant management system.
    /// </summary>
    public class TenantsController : CelloApiController
    {
        private ITenantService service;
        private ITenantRelationService relationService;
        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public TenantsController()
        {
            this.service = ServiceLocator.Resolve<ITenantService>();
            this.relationService = ServiceLocator.Resolve<ITenantRelationService>();
        }

        /// <summary>
        /// This method is used to tenant details by search condition.
        /// </summary>
        /// <param name="condition">search condition.</param>
        /// <returns>tenant search details.</returns>
        [HttpPost]
        public CelloSaaS.RestApiSDK.TenantSearchResultDTO search_tenants(TenantSearchConditionDTO condition)
        {
            if (!condition.parent_id.HasValue) condition.parent_id = this.TenantId;

            if (condition.page_size <= 0) condition.page_size = 100;
            if (condition.page_no <= 0) condition.page_no = 1;

            var result = this.service.SearchTenantDetails(new TenantDetailsSearchCondition
            {
                ParentTenantId = condition.parent_id.Value,
                TenantIds = condition.tenant_ids,
                ContactEmail = condition.email,
                ContactFirstName = condition.first_name,
                ContactLastName = condition.last_name,
                ContactPhone = condition.phone,
                WebSite = condition.website,
                Url = condition.login_url,
                DataPartitionId = condition.data_partition_id.HasValue ? condition.data_partition_id.Value : Guid.Empty,
                EnableAutoDebit = condition.enable_auto_debit,
                HasChildTenants = condition.has_child_tenants,
                HasOnlineUsers = condition.has_online_users,
                IsSelfRegistered = condition.is_self_registered,
                WithOutPaymentAccount = condition.without_payment_account,
                PageNo = condition.page_no,
                PageSize = condition.page_size
            });

            if (result != null && result.Items != null && result.Items.Count > 0)
            {
                return new CelloSaaS.RestApiSDK.TenantSearchResultDTO
                {
                    condition = condition,
                    total_count = result.TotalCount,
                    result = result.Items.Values.ToViewModel()
                };
            }

            return null;
        }

        /// <summary>
        /// This method is used to get the tenant details by tenant identifier.
        /// </summary>
        /// <param name="tenant_id">tenant unique identifier (mandatory).</param>
        /// <returns>tenant details</returns>
        public CelloSaaS.RestApiSDK.TenantDTO get_tenant([Required]Guid tenant_id)
        {
            Guard.NullGuid("tenant_id", tenant_id, true);

            //Get the tenant details by tenant id.
            var entity = this.service.GetTenantDetailsByTenantId(tenant_id.ToString());

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return entity.ToViewModel();
        }

        /// <summary>
        /// This method is used to get the tenant details based on the tenant url.
        /// </summary>
        /// <param name="login_url">tenant url (mandatory).</param>
        /// <returns>tenant details.</returns>     
        public CelloSaaS.RestApiSDK.TenantDTO get_tenant_by_url([Required]Uri login_url)
        {
            Tenant entity = null;
            var tmp = this.service.GetTenantDetailsByURL(login_url.ToString());

            if (tmp != null)
            {
                entity = this.service.GetTenantDetailsByTenantId(tmp.TenantCode);
            }

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            return entity.ToViewModel();
        }

        /// <summary>
        /// This method is used to get the tenant details for the tenant code string.
        /// </summary>
        /// <param name="friendly_code"> unique code string of the tenant (mandatory).</param>
        /// <returns>tenant details.</returns>
        public CelloSaaS.RestApiSDK.TenantDTO get_tenant_details_by_friendly_code([Required]string friendly_code)
        {
            Tenant entity = null;
            var tmp = this.service.GetTenantDetailsByTenantCodeString(friendly_code);
            if (tmp != null)
            {
                entity = this.service.GetTenantDetailsByTenantId(tmp.TenantCode);
            }

            if (entity == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return entity.ToViewModel();
        }

        /// <summary>
        /// This method is used to get the tenant identifier from the tenant code string.
        /// </summary>
        /// <param name="friendly_code"> unique code string of the tenant (mandatory).</param>
        /// <returns> unique identifier of the tenant.</returns>
        public Guid get_tenantId_from_friendly_code([Required]string friendly_code)
        {
            var tenantId = this.service.GetTenantIdFromTenantCode(friendly_code);

            if (string.IsNullOrWhiteSpace(tenantId))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid friendly_code is passed!.") });
            }
            return Guid.Parse(tenantId);
        }

        /// <summary>
        /// This method is used to get the collection of tenant who are in trail.
        /// </summary>
        /// <returns>collection of trail tenant details.</returns>
        public Dictionary<string, TenantDTO> get_tenants_in_Trail()
        {
            var tenantDetails = this.service.GetTenantsInTrial();

            return tenantDetails != null && tenantDetails.Count > 0 ? tenantDetails.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to get the tenant details.
        /// </summary>
        /// <returns>collection of tenant details.</returns>
        public IEnumerable<TenantDTO> get_tenant_info()
        {
            var tenantDetails = this.service.GetTenantInfoDetails();
            return tenantDetails != null && tenantDetails.Count > 0 ? tenantDetails.ToViewModel() : null;
        }

        /// <summary>
        ///This method is used to get the stratified tenant by user identifier and tenant code.
        /// </summary>
        /// <param name="user_id">unique Identifier of the user (mandatory).</param>
        /// <param name="tenant_id">unique Identifier of the tenant (mandatory).</param>
        /// <returns>
        /// collection of stratified tenant details.
        /// </returns>
        public Dictionary<string, TenantDTO> get_stratified_tenant_by_user([Required]Guid user_id, [Required]Guid tenant_id)
        {
            Guard.NullGuid("userId", user_id, true);
            Guard.NullGuid("tenant_id", tenant_id, true);

            var stratifiedTenantDetails = this.service.GetStratifiedTenantsByUser(user_id.ToString(), tenant_id.ToString());

            return stratifiedTenantDetails != null && stratifiedTenantDetails.Count > 0 ? stratifiedTenantDetails.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to delete the user role and tenant mapping.
        /// Calling this API will invoke a delete of the selected role to the primary tenant of this user.
        /// and also to the child tenants or stratified tenants to this user.
        /// </summary>
        /// <param name="user_id">The user unique identifier (mandatory).</param>
        /// <param name="role_id">The role identifier (mandatory).</param>
        /// <param name="tenant_id">The tenant unique identifier (mandatory).</param> 
        public HttpResponseMessage delete_user_roles_and_tenant_mapping([Required]Guid user_id, [Required]string role_id, [Required]Guid tenant_id)
        {
            Guard.NullGuid("user_id", user_id, true);
            Guard.NullOrEmpty("role_id", role_id, true);
            Guard.NullGuid("tenant_id", tenant_id, true);

            //if (!ServiceLocator.Resolve<CelloSaaS.ServiceContracts.UserManagement.ITenantUserAssociationService>().CheckLinkExists(user_id.ToString(), tenant_id.ToString(), CelloSaaS.Model.UserManagement.RequestStatus.Approved))
            //    return new HttpResponseMessage(HttpStatusCode.Unauthorized);

            this.service.DeleteUserRoleAndTenantMapping(user_id.ToString(), role_id, tenant_id.ToString());
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }

}
