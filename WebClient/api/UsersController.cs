﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CelloSaaS.Library;
using CelloSaaS.Model;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.ServiceContracts.MasterData;
using CelloSaaS.ServiceContracts.UserManagement;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Text;
using System.Transactions;
using System.Web.Security;
using CelloSaaS.DAL;
using CelloSaaS.DAL.TenantManagement;
using CelloSaaS.DAL.UserManagement;
using CelloSaaS.Library;
using CelloSaaS.Library.DataAccessLayer;
using CelloSaaS.Library.Encryption;
using CelloSaaS.Library.Helpers;
using CelloSaaS.Model;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.SettingsManagement;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.Notification.Model.Content;
using CelloSaaS.Notification.Model.Dispatch;
using CelloSaaS.Notification.NotificationManager;
using CelloSaaS.Notification.NotificationManager.Content;
using CelloSaaS.ServiceContracts.AccessControlManagement;
using CelloSaaS.ServiceContracts.LicenseManagement;
using CelloSaaS.ServiceContracts.Providers;
using CelloSaaS.ServiceContracts.SettingsManagement;
using CelloSaaS.ServiceContracts.TenantManagement;
using CelloSaaS.ServiceContracts.UserManagement;
using CelloSaaS.Services.SettingsManagement;
using CelloSaaS.EventScheduler.EventPublishingEngine;
using CelloSaaS.ServiceContracts.MasterData;
using System.Threading.Tasks;
using CelloSaaS.RestApiSDK;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApplication.api
{
    public class JsonResponse<T> : IHttpActionResult
    {
        private T _input;

        public JsonResponse(T input)
        {
            _input = input;
        }
        public async Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new ObjectContent<T>(_input, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            return resp;
        }
    }

    /// <summary>
    /// These endpoints are responsible for handling the user information.
    /// </summary>
    public class UsersController : CelloApiController
    {
        private IUserDetailsService service;

        /// <summary>
        /// This constructor is used to initialize the required services.
        /// </summary>
        public UsersController()
        {
            //this.service = service;
            this.service = ServiceLocator.Resolve<IUserDetailsService>();
            
        }

        /// <summary>
        /// This method is used to retrieves all the users for this tenant.
        /// </summary>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <returns>collection of user details.</returns>
        public  IEnumerable<UserDetailsDTO> get_all_users(Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            var result = this.service.GetUserDetailsByTenantId(tenant_id.ToString());
            return result != null ? result.Values.ToViewModel() : null;
        }

        /// <summary>
        /// This method is used to retrieve the given user information.
        /// </summary>
        /// <param name="user_id">user unique identifier (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <returns>user details.</returns>        
        public IHttpActionResult get_user([Required]Guid user_id, Guid? tenant_id = null)
        {
            Guard.NullGuid("UserId", user_id, true);
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            UserDetails user = this.service.GetUserDetailsByUserId(user_id.ToString(), tenant_id.ToString());

            if (user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid user id was passed!") });
            }
            return new JsonResponse<UserDetailsDTO>(user.ToViewModel());
        }

        /// <summary>
        /// This method is used to retrieve the user information by username.
        /// </summary>
        /// <param name="user_name">user name of the user (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <returns>user details</returns>
        public UserDetailsDTO get_user_by_user_name([Required]string user_name, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            UserDetails user = this.service.GetUserDetailsByName(user_name, tenant_id.ToString());

            if (user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid user name was passed!") });
            }

            return user.ToViewModel();
        }

        /// <summary>
        /// This method is used to activates the given user.
        /// </summary>
        /// <param name="user_id">user unique identifier (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        [HttpGet]
        public void activate_user([Required]Guid user_id, Guid? tenant_id = null)
        {
            Guard.NullGuid("UserId", user_id, true);

            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;
            this.service.ActivateUser(user_id.ToString());
        }

        /// <summary>
        /// This method is used to update the first time log on.
        /// </summary>
        /// <param name="user_id">the user unique identifier (mandatory).</param>
        /// <param name="password">string of character containing  user password (mandatory).</param>
        /// <param name="security_question">user security question (mandatory).</param>
        /// <param name="security_answer">answer of the security question (mandatory).</param>
        /// <param name="first_logOn">indicates user is first time log on user or not.</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        [HttpPost]
        public void update_first_time_logon([Required]Guid user_id, [Required]string password, [Required]string security_question, [Required]string security_answer, [Required]bool first_logOn, Guid? tenant_id = null)
        {
            Guard.NullGuid("UserId", user_id, true);

            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            UserDetails user = this.service.GetUserDetailsByUserId(user_id.ToString(), tenant_id.ToString());

            if (user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid user identifier was passed!") });
            }

            this.service.UpdateFirstTimeLogOn(user.MembershipDetails.MembershipId, password, security_question, security_answer, first_logOn, tenant_id.Value.ToString());
        }

        /// <summary>
        /// This method is used to validate the passed security answer.
        /// </summary>
        /// <param name="friendly_code">unique code string of the tenant (mandatory).</param>
        /// <param name="user_name">string of character containing  user name (mandatory).</param>
        /// <param name="answer">answer of the security question (mandatory).</param>
        /// <returns><c>true</c>, if the security answer is valid, <c>false</c> otherwise.</returns>
        [HttpGet]
        public bool validate_security_answer([Required]string friendly_code, [Required]string user_name, [Required]string answer)
        {
            return this.service.ValidateSecurityAnswer(friendly_code, user_name, answer);
        }

        /// <summary>
        /// This method is used to change the existing password of the tenant.
        /// </summary>
        /// <param name="user_name">string of character containing  user name (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <param name="old_password">string of character containing old password of the user.</param>
        /// <param name="new_password">string of character containing new password of the user.</param>
        [HttpGet]
        public void change_password([Required]string user_name, [Required]string old_password, [Required]string new_password, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            this.service.ChangePassword(user_name, tenant_id.Value.ToString(), old_password, new_password);
        }

        /// <summary>
        /// This method is used to send mail to user with his/her password.
        /// </summary>
        /// <param name="user_name">string of character containing  user name (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        [HttpGet]
        public void forgot_password([Required]string user_name, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            this.service.ForgotPassword(user_name, tenant_id.Value.ToString());
        }

        /// <summary>
        /// This method is used to update the user password.
        /// </summary>
        /// <param name="user_id">user unique identifier (mandatory).</param>
        /// <param name="password">string of character containing new  password of the user (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        [HttpPost]
        public void update_user_password([Required]Guid user_id, [Required]string password, Guid? tenant_id = null)
        {
            Guard.NullGuid("user_id", user_id, true);

            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            UserDetails user = this.service.GetUserDetailsByUserId(user_id.ToString(), tenant_id.ToString());
            if (user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid user identifier was passed!") });
            }
            this.service.UpdateFirstTimeLogOn(user.MembershipDetails.MembershipId, password, string.Empty, string.Empty, false, tenant_id.ToString());
        }

        /// <summary>
        /// Updates the password expiry status daily and sets the expiry status for each user in the user password expiration table.
        /// </summary>
        [HttpGet]
        public void update_password_expiry()
        {
            this.service.UpdatePasswordExpirationStatus();
        }

        /// <summary>
        /// This method is used to reset the password.
        /// </summary>
        /// <param name="user_name">string of character containing  user name (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        [HttpPost]
        public void reset_password([Required]string user_name, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            this.service.ResetPassword(user_name, tenant_id.Value.ToString());
        }

        /// <summary>
        /// This method is used to reset the password with the password question and password answer.
        /// </summary>
        /// <param name="user_name">string of character containing  user name (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <param name="password_question">user password security  question (mandatory).</param>
        /// <param name="password_answer">answer of the password security question (mandatory).</param>
        [HttpPost]
        public void reset_password([Required]string user_name, [Required]string password_question, [Required]string password_answer, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            this.service.ResetPassword(user_name, tenant_id.Value.ToString(), password_question, password_answer);
        }

        /// <summary>
        ///  This method is used to reset the password  for specified users  with the password.
        /// </summary>
        /// <param name="resetPassword">user identifiers,password and tenant identifier are mandatory.</param>
        [HttpPost]
        public void reset_password([Required]ResetPasswordDTO resetPassword)
        {
            Guard.Null("resetPassword", resetPassword);
            Guard.NullOrEmptyEnumerable("user_ids", resetPassword.user_ids, true);
            Guard.NullGuid("tenant_id", resetPassword.tenant_id, true);
            this.service.ResetPassword(resetPassword.user_ids.Select(c => c.ToString()).ToArray(), resetPassword.password, resetPassword.tenant_id.ToString());
        }

        /// <summary>
        /// This method is used to unlock the specified user accounts.
        /// </summary>
        /// <param name="user_ids">user unique identifiers (mandatory).</param>
        [HttpPost]
        public void unlock_user_accounts([Required]Guid[] user_ids)
        {
            Guard.NullOrEmptyEnumerable("user_ids", user_ids, true);

            this.service.UnLockUserAccounts(user_ids.Select(c => c.ToString()).ToArray());
        }

        /// <summary>
        /// This method is used to lock the specified user accounts.
        /// </summary>
        /// <param name="user_ids">user unique identifiers (mandatory).</param>
        [HttpPost]
        public void lock_user_accounts(Guid[] user_ids)
        {
            Guard.NullOrEmptyEnumerable("user_ids", user_ids, true);

            this.service.LockUserAccounts(user_ids.Select(c => c.ToString()).ToArray());
        }

        /// <summary>
        /// This method is used to lock all the existing user accounts.
        /// </summary>
        /// <param name="tenant_id">tenant unique identifiers (mandatory).</param>
        [HttpPost]
        public void lock_all_user_accounts([Required]Guid tenant_id)
        {
            Guard.NullGuid("tenant_id", tenant_id, true);
            this.service.LockAllUserAccounts(tenant_id.ToString());
        }

        /// <summary>
        /// This method is used to unlock all the existing user accounts.
        /// </summary>
        /// <param name="tenant_id">tenant unique identifiers (mandatory).</param>
        [HttpPost]
        public void unlock_all_user_accounts([Required]Guid tenant_id)
        {
            Guard.NullGuid("tenant_id", tenant_id, true);

            this.service.UnLockAllUserAccounts(tenant_id.ToString());
        }

        /// <summary>
        /// This method is used to reset the password forcedly.
        /// </summary>
        /// <param name="user_ids">user unique identifiers (mandatory).</param>
        [HttpPost]
        public void force_user_password_reset([Required]Guid[] user_ids)
        {
            Guard.NullOrEmptyEnumerable("user_ids", user_ids, true);

            this.service.ForceUserPasswordReset(user_ids.Select(c => c.ToString()).ToArray());
        }
        /// <summary>
        /// This method used to creates a new user.
        /// If password was not set, system will create the password.
        /// </summary>
        /// <param name="user">user details.</param>
        /// <returns>HttpResponseMessage (Created) along with the user identifier.</returns>
        public HttpResponseMessage put_user([Required]UserDetailsDTO user)
        {
            Guard.Null("user", user);
            if (!user.tenant_id.HasValue || user.tenant_id.Value.Equals(Guid.Empty)) user.tenant_id = this.TenantId;

            // convert the user details dto to user details.
            var entity = user.ToBusinessEntity();

            entity.MembershipDetails.Password = user.password;

            entity.MembershipDetails.TenantCode = entity.User.TenantId = user.tenant_id.ToString();

            if (entity.Address != null && !string.IsNullOrEmpty(entity.Address.CountryName))
            {
                var countryService = ServiceLocator.Resolve<ICountryService>();
                var countries = countryService.GetAllCountry();
                entity.Address.CountryId = countries.Where(x => x.Name.Equals(entity.Address.CountryName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().CountryId;
            }

            // add the user to system
            var id = this.service.AddUser(entity);

            var response = Request.CreateResponse<Guid>(HttpStatusCode.Created, Guid.Parse(id));

            return response;
        }

        /// <summary>
        /// This method used to updates the existing user.
        /// </summary>
        /// <param name="user">user details to update.</param>
        public void post_user([Required]UserDetailsDTO user)
        {
            Guard.Null("user", user);
            Guid userId = user.id;


            if (!user.tenant_id.HasValue || user.tenant_id.Value.Equals(Guid.Empty)) user.tenant_id = this.TenantId;

            UserDetails old_user = this.service.GetUserDetailsByUserId(userId.ToString(), user.tenant_id.ToString());

            if (old_user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }

            var entity = user.ToBusinessEntity(old_user);

            if (entity.Address != null && !string.IsNullOrEmpty(entity.Address.CountryName))
            {
                var countryService = ServiceLocator.Resolve<ICountryService>();
                var countries = countryService.GetAllCountry();
                entity.Address.CountryId = countries.Where(x => x.Name.Equals(entity.Address.CountryName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().CountryId;
            }

            this.service.UpdateUser(entity);
        }

        /// <summary>
        /// This method is used to deactivate the given user.
        /// </summary>
        /// <param name="user_id">user unique identifier (mandatory).</param>
        /// <param name="tenant_id">tenant unique identifier (optional).</param>
        /// <returns>HttpResponseMessage (NoContent).</returns>
        public HttpResponseMessage delete_User([Required]Guid user_id, Guid? tenant_id = null)
        {
            if (!tenant_id.HasValue || tenant_id.Value.Equals(Guid.Empty)) tenant_id = this.TenantId;

            UserDetails old_user = this.service.GetUserDetailsByUserId(user_id.ToString(), tenant_id.ToString());

            if (old_user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound) { Content = new StringContent("Invalid user id was passed!") });
            }

            this.service.DeleteUser(user_id.ToString());
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// This method is used to searches the user details.
        /// </summary>
        /// <param name="condition">search condition.</param>
        /// <returns>search result.</returns>
        public UserDetailsSearchResultDTO Search_User(UserDetailsSearchConditionDTO condition)
        {
            if (condition == null)
                condition = new UserDetailsSearchConditionDTO();

            if (!condition.tenant_id.HasValue) condition.tenant_id = this.TenantId;

            if (condition.page_no <= 0) condition.page_no = 1;
            if (condition.page_size <= 0) condition.page_size = 100;



            var searchCondition = new UserDetailsSearchCondition
            {
                TenantId = condition.tenant_id.ToString(),
                RecordStart = ((condition.page_no - 1) * condition.page_size) + 1,
                RecordEnd = (condition.page_no * condition.page_size),

                Status = condition.status.HasValue ? condition.status.Value : false,
                SetTotalCount = true
            };

            switch (condition.sort_direction)
            {
                case SortExpression.Ascending:
                    searchCondition.SortExpression = SortExpressionConstants.Ascending;
                    break;
                default:
                case SortExpression.Decending:
                    searchCondition.SortExpression = SortExpressionConstants.Decending;
                    break;
            }

            switch (condition.sort_string)
            {
                case UserDetailsSortString.user_name:
                    searchCondition.SortString = UserDetailsSortStringConstants.UserName;
                    break;
                case UserDetailsSortString.email:
                    searchCondition.SortString = UserDetailsSortStringConstants.EmailId;
                    break;
                case UserDetailsSortString.last_name:
                    searchCondition.SortString = UserDetailsSortStringConstants.LastName;
                    break;
                case UserDetailsSortString.is_locked:
                    searchCondition.SortString = UserDetailsSortStringConstants.IsLocked;
                    break;
                case UserDetailsSortString.user_id:
                    searchCondition.SortString = UserDetailsSortStringConstants.UserId;
                    break;
                default:
                case UserDetailsSortString.first_name:
                    searchCondition.SortString = UserDetailsSortStringConstants.FirstName;
                    break;
            }
            if (!string.IsNullOrEmpty(condition.search_value))
            {
                switch (condition.search_field)
                {
                    case UserDetailsSearchField.user_name:
                        searchCondition.UserName = condition.search_value;
                        break;
                    case UserDetailsSearchField.email:
                        searchCondition.EmailId = condition.search_value;
                        break;
                    default:
                    case UserDetailsSearchField.both:
                        searchCondition.SearchValue = condition.search_value;
                        break;
                }
            }

            var searchParameters = new CelloSaaS.Model.SearchParameters
            {
                searchCondition = searchCondition
            };

            var result = this.service.SearchUserDetails(searchParameters);

            int total_count = 0, search_count = 0;

            var cntlst = this.service.GetUserCountByTenantId(condition.tenant_id.ToString());

            if (cntlst != null)
            {
                total_count = cntlst.Sum(x => x.Value);
            }

            if (searchParameters.SearchSummary != null)
            {
                search_count = searchParameters.SearchSummary.TotalCount;
            }

            var lst = result != null ? result.Values.ToViewModel() : null;

            var search_result = new UserDetailsSearchResultDTO
            {
                result = lst,
                total_count = total_count,
                search_count = search_count,
                search_condition = condition
            };

            return search_result;
        }

    }
}
