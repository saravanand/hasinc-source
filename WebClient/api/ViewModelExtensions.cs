﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CelloSaaS.Billing.Model;
using CelloSaaS.Model.AccessControlManagement;
using CelloSaaS.Model.Configuration;
using CelloSaaS.Model.DataManagement;
using CelloSaaS.Model.LicenseManagement;
using CelloSaaS.Model.ProductAnalytics;
using CelloSaaS.Model.TenantManagement;
using CelloSaaS.Model.UserManagement;
using CelloSaaS.Model.ViewManagement;
using CelloSaaS.Notification.Model;
using CelloSaaS.Notification.Model.Content;
using CelloSaaS.Notification.Model.Dispatch;
using CelloSaaS.RestApiSDK;

namespace WebApplication.api
{
    public static class ViewModelExtensions
    {
        #region Custom Fields
        /// <summary>
        /// This method is used to fill the custom fields details.
        /// </summary>
        /// <param name="vm">BaseDTO model.</param>
        /// <param name="entity">entity model.</param>
        public static void FillCustomFields(this BaseDTO vm, CelloSaaS.Model.BaseEntity entity)
        {
            if (vm == null || entity == null || entity.ExtendedRow == null || entity.ExtendedRow.ExtendedEntityColumnValues == null || entity.ExtendedRow.ExtendedEntityColumnValues.Count == 0) return;

            foreach (var item in entity.ExtendedRow.ExtendedEntityColumnValues)
            {
                // field id , value
                vm.Properties.Add(item.Key, item.Value.Value);
            }
        }

        /// <summary>
        /// This method is used to fill the custom fields details.
        /// </summary>
        /// <param name="vm">BaseDTO model.</param>
        /// <param name="entity">entity model.</param>
        public static void FillExtendedFields(this CelloSaaS.Model.BaseEntity entity, BaseDTO vm)
        {
            if (vm == null || entity == null || vm.Properties == null || vm.Properties.Count < 1) return;

            entity.ExtendedRow = new ExtendedEntityRow();
            entity.ExtendedRow.ExtendedEntityColumnValues = new Dictionary<string, ExtendedEntityColumn>();
            foreach (var item in vm.Properties)
            {
                var extendedEntityColumn = new ExtendedEntityColumn()
                    {
                        EntityFieldIdentifier = item.Key,
                        Value = item.Value != null && !string.IsNullOrWhiteSpace(item.Value.ToString()) ? item.Value.ToString() : null
                    };
                // field id , value
                entity.ExtendedRow.ExtendedEntityColumnValues.Add
                    (item.Key,
                   extendedEntityColumn
                    );
            }
        }


        #endregion

        #region User

        /// <summary>
        /// This method is used to convert UserDetailsDTO to UserDetails model.
        /// </summary>
        /// <param name="vm">UserDetailsDTO model</param>
        /// <param name="entity">UserDetails model</param>
        /// <returns>UserDetails model</returns>
        public static UserDetails ToBusinessEntity(this UserDetailsDTO vm, UserDetails entity = null)
        {
            if (vm == null) return null;
            if (entity == null) entity = new UserDetails();

            entity.Identifier = vm.id.ToString();
            entity.User.UserId = vm.id.ToString();
            entity.User.FirstName = vm.first_name;
            entity.User.LastName = vm.last_name;
            entity.User.User_Description = vm.description;
            entity.MembershipDetails.UserName = vm.username;
            entity.MembershipDetails.EmailId = vm.email;
            entity.Address = vm.address.ToBusinessEntity();

            if (!string.IsNullOrEmpty(vm.password))
            {
                entity.MembershipDetails.Password = vm.password;
            }
            entity.FillExtendedFields(vm);
            return entity;
        }

        /// <summary>
        /// This method is used to convert the UserDetails to UserDetailsDTO model.
        /// </summary>
        /// <param name="entity">UserDetails model</param>
        /// <returns>UserDetailsDTO model</returns>
        public static UserDetailsDTO ToViewModel(this UserDetails entity)
        {
            if (entity == null) return null;
            var vm = new UserDetailsDTO
            {
                id = Guid.Parse(entity.User.UserId),
                tenant_id = Guid.Parse(entity.User.TenantId),
                first_name = entity.User.FirstName,
                last_name = entity.User.LastName,
                email = entity.MembershipDetails.EmailId,
                username = entity.MembershipDetails.UserName,
                last_activity = entity.MembershipDetails.LastActivityDate,
                last_locked = entity.MembershipDetails.LastLockedOutDate,
                is_first_time_user = entity.MembershipDetails.IsFirstTimeUser,
                is_locked = entity.MembershipDetails.IsLockedOut,
                description = entity.User.User_Description,
                last_logon = entity.MembershipDetails.LastLogonDate,
                status = entity.MembershipDetails.Status.Value,
                created_by = Guid.Parse(entity.User.CreatedBy),
                created_on = entity.User.CreatedOn,
                address = entity.Address.ToViewModel()
            };

            if (entity.User.UpdatedOn != DateTime.MinValue)
            {
                vm.updated_on = entity.User.UpdatedOn;
            }

            if (!string.IsNullOrEmpty(entity.User.UpdatedBy))
            {
                vm.updated_by = Guid.Parse(entity.User.UpdatedBy);
            }

            vm.FillCustomFields(entity);

            return vm;
        }

        /// <summary>
        /// This method is used to convert collection of UserDetailsDTO to collection UserDetails model.
        /// </summary>
        /// <param name="entity">collection of user details</param>
        /// <returns>collection of UserDetailsDTO.</returns>
        public static IEnumerable<UserDetailsDTO> ToViewModel(this IEnumerable<UserDetails> entity)
        {
            if (entity == null) return null;
            var lst = new List<UserDetailsDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        /// <summary>
        ///  This method is used to convert the AddressDTO to Address model.
        /// </summary>
        /// <param name="vm">AddressDTO model</param>
        /// <returns>Address model</returns>
        public static Address ToBusinessEntity(this AddressDTO vm)
        {
            if (vm == null) return null;
            var entity = new Address();
            entity.AddressId = vm.id == Guid.Empty ? null : vm.id.ToString();
            entity.Address1 = vm.address1;
            entity.Address2 = vm.address2;
            entity.City = vm.city;
            entity.CountryName = vm.country;
            entity.State = vm.state;
            entity.PostalCode = vm.zipcode;

            entity.FillExtendedFields(vm);

            return entity;
        }

        /// <summary>
        /// This method is used to convert the Address  to AddressDTO model.
        /// </summary>
        /// <param name="entity">Address model</param>
        /// <returns>AddressDTO model</returns>
        public static AddressDTO ToViewModel(this Address entity)
        {
            if (entity == null || string.IsNullOrEmpty(entity.AddressId)) return null;
            var vm = new AddressDTO
            {
                id = !string.IsNullOrEmpty(entity.AddressId) ? Guid.Parse(entity.AddressId) : Guid.Empty,
                address1 = entity.Address1,
                address2 = entity.Address2,
                zipcode = entity.PostalCode,
                country = entity.CountryName,
                state = entity.State,
                city = entity.City,
                created_on = entity.CreatedOn,
                updated_on = entity.UpdatedOn,
                status = entity.Status.HasValue ? entity.Status.Value : true
            };

            if (!string.IsNullOrEmpty(entity.CreatedBy))
            {
                vm.updated_by = Guid.Parse(entity.CreatedBy);
            }

            if (!string.IsNullOrEmpty(entity.UpdatedBy))
            {
                vm.updated_by = Guid.Parse(entity.UpdatedBy);
            }

            vm.FillCustomFields(entity);

            return vm;
        }

        /// <summary>
        /// This method is used to convert the ContactDetailsDTO to ContactDetails model.
        /// </summary>
        /// <param name="vm">ContactDetailsDTO model</param>
        /// <returns>ContactDetails model</returns>
        public static ContactDetails ToBusinessEntity(this ContactDetailsDTO vm)
        {
            if (vm == null) return null;
            var entity = new ContactDetails
            {
                ContactId = vm.id == Guid.Empty ? null : vm.id.ToString(),
                FirstName = vm.first_name,
                LastName = vm.last_name,
                Email = vm.email,
                Phone = vm.phone,
                Fax = vm.fax,
                Status = true
            };
            return entity;
        }

        /// <summary>
        /// This method is used to convert the ContactDetails model  to ContactDetailsDTO.
        /// </summary>
        /// <param name="entity">ContactDetails model</param>
        /// <returns>ContactDetailsDTO model</returns>
        public static ContactDetailsDTO ToViewModel(this ContactDetails entity)
        {
            if (entity == null || string.IsNullOrEmpty(entity.ContactId)) return null;

            var vm = new ContactDetailsDTO
            {
                first_name = entity.FirstName,
                last_name = entity.LastName,
                email = entity.Email,
                phone = entity.Phone,
                fax = entity.Fax,
            };

            if (!string.IsNullOrEmpty(entity.ContactId)) vm.id = Guid.Parse(entity.ContactId);
            vm.FillCustomFields(entity);
            return vm;
        }

        #endregion User

        #region Billing
        /// <summary>
        /// This method is used to convert collection of Invoice model to collection of InvoiceDTO model.
        /// </summary>
        /// <param name="entity">collection of Invoice model.</param>
        /// <returns>collection of InvoiceDTO model.</returns>
        public static IEnumerable<InvoiceDTO> ToViewModel(this IEnumerable<Invoice> entity)
        {
            if (entity == null) return null;
            var lst = new List<InvoiceDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        /// <summary>
        /// This method is used to convert Invoice model to InvoiceDTO model.
        /// </summary>
        /// <param name="entity">Invoice model.</param>
        /// <returns>InvoiceDTO model.</returns>
        public static InvoiceDTO ToViewModel(this Invoice entity)
        {
            if (entity == null) return null;
            var vm = new InvoiceDTO
            {
                Id = entity.Id,
                TenantId = entity.TenantId,
                ParentTenantId = entity.ParentTenantId,
                PackageId = entity.PackageId,
                PricePlanId = Guid.Parse(entity.PricePlanId.ToString()),
                InvoiceType = (InvoiceTypeDTO)entity.InvoiceType,
                InvoiceTypeValue = entity.InvoiceTypeValue,
                InvoiceNo = entity.InvoiceNo,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                NextInvoiceDate = entity.NextInvoiceDate,
                DueDate = entity.DueDate,
                InvoiceStatusValue = entity.InvoiceStatusValue,
                InvoiceStatus = (InvoiceStatusDTO)entity.InvoiceStatus,
                BillFrequencyValue = entity.BillFrequencyValue,
                BillFrequency = (BillingFrequency)entity.BillFrequency,
                PendingBalance = entity.PendingBalance,
                Memo = entity.Memo,
                LineItems = entity.LineItems.ToViewModel(),
                Payments = entity.Payments.ToViewModel(),
                CreatedOn = entity.CreatedOn,
                CreatedBy = entity.CreatedBy,
                UpdatedOn = entity.UpdatedOn
            };

            return vm;
        }

        /// <summary>
        /// This method is used convert collection InvoiceLineItem to collection of InvoiceLineItemDTO.
        /// </summary>
        /// <param name="entity">collection of InvoiceLineItem model.</param>
        /// <returns>collection of InvoiceLineItemDTO model.</returns>
        public static ICollection<InvoiceLineItemDTO> ToViewModel(this ICollection<InvoiceLineItem> entity)
        {
            if (entity == null) return null;
            var lst = new List<InvoiceLineItemDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        /// <summary>
        /// This method is used to convert collection of InvoicePayment model to collection of InvoicePaymentDTO model.
        /// </summary>
        /// <param name="entity">collection of InvoicePayment model.</param>
        /// <returns>collection of InvoicePaymentDTO model.</returns>
        public static ICollection<InvoicePaymentDTO> ToViewModel(this ICollection<InvoicePayment> entity)
        {
            if (entity == null) return null;
            var lst = new List<InvoicePaymentDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        /// <summary>
        /// This is used for the purpose of convert collection of InvoiceLineItem model to collection of InvoiceLineItemDTO model.
        /// </summary>
        /// <param name="entity">collection of InvoiceLineItem model.</param>
        /// <returns>collection of InvoiceLineItemDTO model.</returns>
        public static InvoiceLineItemDTO ToViewModel(this InvoiceLineItem entity)
        {
            if (entity == null) return null;
            var vm = new InvoiceLineItemDTO
            {
                Id = entity.Id,
                InvoiceId = entity.InvoiceId,
                TenantId = entity.TenantId,
                ParentTenantId = entity.ParentTenantId,
                Name = entity.Name,
                Description = entity.Description,
                Type = entity.Description,
                Usage = entity.Usage,
                Amount = entity.Amount,
                IsUserAdded = entity.IsUserAdded
            };
            return vm;
        }

        /// <summary>
        /// This method is used to convert InvoicePayment model to InvoicePaymentDTO model.
        /// </summary>
        /// <param name="entity">InvoicePayment model.</param>
        /// <returns>InvoicePaymentDTO model.</returns>
        public static InvoicePaymentDTO ToViewModel(this InvoicePayment entity)
        {
            if (entity == null) return null;
            var vm = new InvoicePaymentDTO
            {
                Id = entity.Id,
                InvoiceId = entity.InvoiceId,
                TenantId = entity.TenantId,
                ParentTenantId = entity.ParentTenantId,
                PaymentModeValue = entity.PaymentModeValue,
                PaymentMode = (PaymentModeDTO)entity.PaymentMode,
                PaidAmount = entity.PaidAmount,
                PaymentDate = entity.PaymentDate,
                CreatedBy = entity.CreatedBy,
                Notes = entity.Notes
            };
            return vm;
        }

        /// <summary>
        /// This method is used to convert collection of BillStatistics model to BillStatisticsDTO model.
        /// </summary>
        /// <param name="entity">collection of BillStatistics model.</param>
        /// <returns>collection of BillStatisticsDTO model.</returns>
        public static Dictionary<Guid, BillStatisticsDTO> ToViewModel(this Dictionary<Guid, BillStatistics> entity)
        {
            var billStatisticsDTO = new Dictionary<Guid, BillStatisticsDTO>();
            foreach (KeyValuePair<Guid, BillStatistics> item in entity)
            {
                billStatisticsDTO.Add(item.Key, item.Value.ToViewModel());
            }
            return billStatisticsDTO;
        }

        /// <summary>
        /// This method is used to convert BillStatistics model to BillStatisticsDTO model.
        /// </summary>
        /// <param name="entity">BillStatistics model.</param>
        /// <returns>BillStatisticsDTO model.</returns>
        public static BillStatisticsDTO ToViewModel(this BillStatistics entity)
        {
            if (entity == null) return null;
            var vm = new BillStatisticsDTO
            {
                TenantId = entity.TenantId,
                TotalBills = entity.TotalBills,
                TotalAmount = entity.TotalAmount,
                TotalPaid = entity.TotalPaid,
                TotalTax = entity.TotalTax,
                //Overdue=entity.Overdue
            };
            return vm;
        }

        #endregion

        #region Events
        /// <summary>
        /// This method isued to convert EventDTO model to Event model.
        /// </summary>
        /// <param name="vm">EventDTO model.</param>
        /// <param name="entity">Event model.</param>
        /// <returns>Event model.</returns>
        public static CelloSaaS.EventScheduler.EventPublishingEngine.Event ToBusinessEntity(this EventDTO vm, CelloSaaS.EventScheduler.EventPublishingEngine.Event entity = null)
        {
            if (vm == null) return null;
            if (entity == null) entity = new CelloSaaS.EventScheduler.EventPublishingEngine.Event();

            entity.Identifier = vm.id.ToString();
            entity.TenantId = vm.tenant_id.ToString();
            entity.UserId = vm.user_id.ToString();
            entity.EventId = vm.event_id.ToString();
            entity.SubjectId = vm.subject_id;
            entity.SubjectType = vm.subject_type;
            entity.SubjectXmlValue = vm.subject_xml_value;
            entity.TargetId = vm.target_id;
            entity.TargetType = vm.target_type;
            entity.TargetXmlValue = vm.target_xml_value;
            entity.ContextId = vm.context_id;
            entity.ContextType = vm.context_type;
            entity.ContextXmlValue = vm.context_xml_value;
            entity.EventParameterXml = vm.parameter_xml;
            entity.Priority = (CelloSaaS.EventScheduler.EventPublishingEngine.EventPriority)Enum.Parse(typeof(CelloSaaS.EventScheduler.EventPublishingEngine.EventPriority), vm.priority.ToString());

            return entity;
        }

        /// <summary>
        /// This method is used to Event model to EventDTO model.
        /// </summary>
        /// <param name="entity">Event model.</param>
        /// <returns>EventDTO model.</returns>
        public static EventDTO ToViewModel(this CelloSaaS.EventScheduler.EventPublishingEngine.Event entity)
        {
            if (entity == null) return null;
            var vm = new EventDTO
            {
                id = Guid.Parse(entity.Identifier),
                event_id = Guid.Parse(entity.EventId),
                tenant_id = Guid.Parse(entity.TenantId),
                event_name = entity.EventName,
                event_description = entity.EventDescription,
                parameter_xml = entity.EventParameterXml,
                template_id = entity.TemplateId,
                transformed_description = entity.TransformedDescription,
                user_id = Guid.Parse(entity.UserId),
                created_by = Guid.Parse(entity.CreatedBy),
                created_on = entity.CreatedOn,
                subject_id = entity.SubjectId,
                subject_type = entity.SubjectType,
                subject_xml_value = entity.SubjectXmlValue,
                context_id = entity.ContextId,
                context_type = entity.ContextType,
                context_xml_value = entity.ContextXmlValue,
                target_id = entity.TargetId,
                target_type = entity.TargetType,
                target_xml_value = entity.TargetXmlValue,
                status = entity.Status,
                priority = (EventPriorityDTO)Enum.Parse(typeof(EventPriorityDTO), entity.Priority.ToString()),
                event_job_execution_status = (EventStatusDTO)Enum.Parse(typeof(EventStatusDTO), entity.EventStatus.ToString())
            };

            return vm;
        }

        /// <summary>
        /// This method is used to convert collection of Event model to collection of EventDTO model.
        /// </summary>
        /// <param name="entity">collection of Event model.</param>
        /// <returns>collection of EventDTO model.</returns>
        public static IEnumerable<EventDTO> ToViewModel(this IEnumerable<CelloSaaS.EventScheduler.EventPublishingEngine.Event> entity)
        {
            if (entity == null) return null;
            var lst = new List<EventDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }
        #endregion Events

        #region ProductAnalytics
        /// <summary>
        /// This model is used to convert ProductAnalyticsDTO model to ProductAnalytics model.
        /// </summary>
        /// <param name="vm">ProductAnalyticsDTO model.</param>
        /// <param name="entity">ProductAnalytics model.</param>
        /// <returns>ProductAnalytics model.</returns>
        public static ProductAnalytics ToBusinessEntity(this ProductAnalyticsDTO vm, ProductAnalytics entity = null)
        {
            if (vm == null) return null;
            if (entity == null) entity = new ProductAnalytics();

            entity.Id = entity.Identifier = vm.id.ToString();
            entity.TenantId = vm.tenant_id.ToString();
            entity.UserId = vm.user_id.ToString();
            entity.IPAddress = vm.ip_address;
            entity.MachineName = vm.machine_name;
            entity.RequestMethod = vm.request_method;
            entity.RequestMode = vm.request_mode;
            entity.ResponseLength = vm.response_length;
            entity.ResponseStatusCode = vm.response_status_code;
            entity.SessionId = vm.session_id;
            entity.TimeStamp = vm.time_stamp;
            entity.ActionName = vm.action_name;
            entity.BrowserDetails = vm.browser_details;
            entity.ControllerName = vm.controller_name;
            entity.CurrentUrl = vm.current_url.ToString();
            entity.ExceptionDetails = vm.exception_details;
            entity.Status = true;

            return entity;
        }

        /// <summary>
        /// This method is used to convert ProductAnalytics model to ProductAnalyticsDTO model.
        /// </summary>
        /// <param name="entity">ProductAnalytics model.</param>
        /// <returns>ProductAnalyticsDTO model.</returns>
        public static ProductAnalyticsDTO ToViewModel(this ProductAnalytics entity)
        {
            if (entity == null) return null;
            var vm = new ProductAnalyticsDTO
            {
                id = Guid.Parse(entity.Id),
                user_id = Guid.Parse(entity.UserId),
                tenant_id = Guid.Parse(entity.TenantId),
                user_name = entity.UserName,
                tenant_name = entity.TenantName,
                time_stamp = entity.TimeStamp,
                session_id = entity.SessionId,
                response_status_description = entity.ResponseDescription,
                request_method = entity.RequestMethod,
                request_mode = entity.RequestMode,
                response_length = entity.ResponseLength,
                response_status_code = entity.ResponseStatusCode.HasValue ? entity.ResponseStatusCode.Value : 200,
                machine_name = entity.MachineName,
                action_name = entity.ActionName,
                controller_name = entity.ControllerName,
                created_by = Guid.Parse(entity.CreatedBy),
                created_on = entity.CreatedOn,
                current_url = new Uri(HttpUtility.UrlDecode(entity.CurrentUrl)),
                exception_details = entity.ExceptionDetails,
                ip_address = entity.IPAddress,
                status = entity.Status
            };

            return vm;
        }

        /// <summary>
        /// This method is used to convert ProductAnalyticsSearchCondition model to ProductAnalyticsSearchConditionDTO model.
        /// </summary>
        /// <param name="entity">ProductAnalyticsSearchCondition model.</param>
        /// <returns>ProductAnalyticsSearchConditionDTO model.</returns>
        public static ProductAnalyticsSearchConditionDTO ToViewModel(this ProductAnalyticsSearchCondition entity)
        {
            if (entity == null) return null;
            var vm = new ProductAnalyticsSearchConditionDTO
            {
             tenant_id=Guid.Parse(entity.TenantId),
             user_id =Guid.Parse(entity.UserId),
             user_name =entity.UserName,
             controller_name =entity.ControllerName,
             action_name =entity.ActionName,
             from_date=entity.FromDate,
             to_date  =entity.ToDate,
             offset=entity.Offset,
             response_time =entity.ResponseTime,
             response_length =entity.ResponseLength,
             has_error =entity.HasError,
             page_size =entity.PageSize,
             sort_string  =entity.SortString,
             sort_direction  =entity.SortDirection,
             user_ip  =entity.UserIP,
             total_count  =entity.TotalCount,
             avg_response_time  =entity.AvgResponseTime,
             avg_page_size  =entity.AvgPageSize,
             session_id = entity.SessionId
            };
            return vm;
        }

        /// <summary>
        /// This method is used to convert collection of ProductAnalytics model to collection of ProductAnalyticsDTO model.
        /// </summary>
        /// <param name="entity">collection of ProductAnalytics model.</param>
        /// <returns>collection of ProductAnalyticsDTO model.</returns>
        public static IEnumerable<ProductAnalyticsDTO> ToViewModel(this IEnumerable<ProductAnalytics> entity)
        {
            if (entity == null) return null;
            var lst = new List<ProductAnalyticsDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        #endregion ProductAnalytics

        #region Tenant
        /// <summary>
        /// This method is used to convert TenantDTO model to Tanant model.
        /// </summary>
        /// <param name="vm">TenantDTO model.</param>
        /// <param name="entity">Tenant model.</param>
        /// <returns>Tenant model.</returns>
        public static Tenant ToBusinessEntity(this TenantDTO vm, Tenant entity = null)
        {
            if (vm == null) return null;
            if (entity == null) entity = new Tenant();

            entity.TenantDetails = new TenantDetails()
            {
                TenantCode = vm.id == Guid.Empty ? null : vm.id.ToString(),
                TenantCodeString = vm.friendly_code,
                TenantName = vm.name,
                Description = vm.description,
                DataPartitionId = vm.data_partition_id.HasValue ? vm.data_partition_id.ToString() : null,
                EnableAutoDebit = vm.enable_auto_debit,
                IsSelfRegistered = vm.is_self_registered,
                ParentTenantId = vm.parent_id.ToString(),
                Status = vm.status,
                URL = vm.login_url != null ? vm.login_url.ToString() : null,
                Website = vm.website != null ? vm.website.ToString() : null
            };

            entity.Address = vm.address.ToBusinessEntity();
            entity.ContactDetail = vm.contact.ToBusinessEntity();

            if (entity.Address != null) entity.TenantDetails.AddressID = entity.Address.AddressId;
            if (entity.ContactDetail != null) entity.TenantDetails.ContactId = entity.ContactDetail.ContactId;

            if (!string.IsNullOrEmpty(vm.type))
            {
                entity.Types = new List<TenantType>();
                entity.Types.Add(new TenantType { Name = vm.type });
            }

            return entity;
        }

        /// <summary>
        /// This method is used to convert Tenant model to TenantDTO model.
        /// </summary>
        /// <param name="entity">Tenant model.</param>
        /// <returns>TenantDTO model.</returns>
        public static TenantDTO ToViewModel(this Tenant entity)
        {
            if (entity == null) return null;
            var vm = new TenantDTO
            {
                id = Guid.Parse(entity.TenantDetails.TenantCode),
                parent_id = Guid.Parse(entity.TenantDetails.ParentTenantId),
                friendly_code = entity.TenantDetails.TenantCodeString,
                name = entity.TenantDetails.TenantName,
                description = entity.TenantDetails.Description,
                enable_auto_debit = entity.TenantDetails.EnableAutoDebit,
                is_self_registered = entity.TenantDetails.IsSelfRegistered,
                login_url = !string.IsNullOrEmpty(entity.TenantDetails.URL) ? new Uri(entity.TenantDetails.URL) : null,
                website = !string.IsNullOrEmpty(entity.TenantDetails.Website) ? new Uri(entity.TenantDetails.Website) : null,
                created_by = Guid.Parse(entity.TenantDetails.CreatedBy),
                created_on = entity.TenantDetails.CreatedOn,
                status = entity.TenantDetails.Status.Value,
                approval_status = entity.TenantDetails.ApprovalStatus,
                company_size = entity.TenantDetails.CompanySize
            };

            if (!string.IsNullOrEmpty(entity.TenantDetails.DataPartitionId)) vm.data_partition_id = Guid.Parse(entity.TenantDetails.DataPartitionId);
            if (!string.IsNullOrEmpty(entity.TenantDetails.UpdatedBy)) vm.updated_by = Guid.Parse(entity.TenantDetails.UpdatedBy);
            if (entity.TenantDetails.UpdatedOn != DateTime.MinValue) vm.updated_on = entity.TenantDetails.UpdatedOn;
            if (entity.Types != null && entity.Types.Count > 0) vm.type = entity.Types.FirstOrDefault().Name;

            vm.address = entity.Address.ToViewModel();
            vm.contact = entity.ContactDetail.ToViewModel();
            vm.FillCustomFields(entity);
            return vm;
        }

        /// <summary>
        /// This method is used to convert TenantDetails model to TenantDTO model.
        /// </summary>
        /// <param name="entity">TenantDetails model.</param>
        /// <returns>TenantDTO model.</returns>
        public static TenantDTO ToViewModel(this TenantDetails entity)
        {
            if (entity == null) return null;
            var vm = new TenantDTO
            {
                id = Guid.Parse(entity.TenantCode),
                parent_id = Guid.Parse(entity.ParentTenantId),
                friendly_code = entity.TenantCodeString,
                name = entity.TenantName,
                description = entity.Description,
                enable_auto_debit = entity.EnableAutoDebit,
                is_self_registered = entity.IsSelfRegistered,
                login_url = !string.IsNullOrEmpty(entity.URL) ? new Uri(entity.URL) : null,
                website = !string.IsNullOrEmpty(entity.Website) ? new Uri(entity.Website) : null,
                created_by = Guid.Parse(entity.CreatedBy),
                created_on = entity.CreatedOn,
                status = entity.Status.Value,
                approval_status = entity.ApprovalStatus,
                company_size = entity.CompanySize
            };
            return vm;
        }

        /// <summary>
        /// This method is used to convert collection of Tenant model to collection of TenantDTO model.
        /// </summary>
        /// <param name="entity">collection Tenant model.</param>
        /// <returns>collection of TenantDTO model.</returns>
        public static IEnumerable<TenantDTO> ToViewModel(this IEnumerable<Tenant> entity)
        {
            if (entity == null) return null;
            var lst = new List<TenantDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        /// <summary>
        /// This method is used to convert collection TenantDetails model to collection TenantDTO model.
        /// </summary>
        /// <param name="entity">collection of TenantDetails model.</param>
        /// <returns>collection of TenantDTO mode.</returns>
        public static Dictionary<string, TenantDTO> ToViewModel(this Dictionary<string, TenantDetails> entity)
        {
            var tenantDTO = new Dictionary<string, TenantDTO>();
            foreach (KeyValuePair<string, TenantDetails> item in entity)
            {
                tenantDTO.Add(item.Key, item.Value.ToViewModel());
            }
            return tenantDTO;
        }

        /// <summary>
        /// This method is used to convert collection of TenantDetails model to collection of TenantDTO model.
        /// </summary>
        /// <param name="entity">collection of TenantDetails model.</param>
        /// <returns>collection of TenantDTO model.</returns>
        public static Dictionary<string, Dictionary<string, TenantDTO>> ToViewModel(this Dictionary<string, Dictionary<string, TenantDetails>> entity)
        {
            var tenantDTO = new Dictionary<string, Dictionary<string, TenantDTO>>();
            foreach (KeyValuePair<string, Dictionary<string, TenantDetails>> item in entity)
            {
                tenantDTO.Add(item.Key, item.Value.ToViewModel());
            }
            return tenantDTO;
        }



        #endregion Tenant

        #region TenantLicense
        /// <summary>
        /// This method is used to convert TenantLicenseDTO model to TenantLicense model.
        /// </summary>
        /// <param name="vm">TenantLicenseDTO model.</param>
        /// <param name="entity">TenantLicense model.</param>
        /// <returns>TenantLicense model.</returns>
        public static TenantLicense ToBusinessEntity(this TenantLicenseDTO vm, TenantLicense entity = null)
        {
            if (vm == null) return null;
            if (entity == null) entity = new TenantLicense();

            entity.TenantLicenseCode = vm.id == Guid.Empty ? null : vm.id.ToString();
            entity.TenantId = vm.tenant_id.ToString();
            entity.PackageId = vm.package_id.ToString();
            entity.PricePlanId = vm.priceplan_id.HasValue ? vm.priceplan_id.ToString() : null;
            entity.ValidityStart = vm.validity_start;
            entity.ValidityEnd = vm.validity_end;
            entity.TrialEndDate = vm.trial_end_date;

            return entity;
        }

        /// <summary>
        /// This method is used to convert TenantLicense model to TenantLicenseDTO model.
        /// </summary>
        /// <param name="entity">TenantLicense model.</param>
        /// <returns>TenantLicenseDTO model.</returns>
        public static TenantLicenseDTO ToViewModel(this TenantLicense entity)
        {
            if (entity == null) return null;
            var vm = new TenantLicenseDTO
            {
                id = Guid.Parse(entity.TenantLicenseCode),
                tenant_id = Guid.Parse(entity.TenantId),
                package_id = Guid.Parse(entity.PackageId),
                package_name = entity.PackageName,
                validity_end = entity.ValidityEnd,
                validity_start = entity.ValidityStart,
                trial_end_date = entity.TrialEndDate
            };

            if (!string.IsNullOrEmpty(entity.PricePlanId)) vm.priceplan_id = Guid.Parse(entity.PricePlanId);

            if (entity.LicenseModuleDetails != null)
            {
                vm.modules = entity.LicenseModuleDetails.Values.Select(x => x.ToViewModel()).ToArray();
            }

            if (entity.LicenseFeatureDetails != null)
            {
                vm.features = entity.LicenseFeatureDetails.Values.Select(x => x.ToViewModel()).ToArray();
            }

            if (entity.LicenseUsageDetails != null)
            {
                vm.usages = entity.LicenseUsageDetails.Values.Select(x => x.ToViewModel()).ToArray();
            }

            if (entity.LicenseServiceDetails != null)
            {
                vm.services = entity.LicenseServiceDetails.Values.Select(x => x.ToViewModel()).ToArray();
            }

            if (entity.LicensePrivilegeDetails != null)
            {
                vm.privileges = entity.LicensePrivilegeDetails.Values.Select(x => x.ToViewModel()).ToArray();
            }

            return vm;
        }

        /// <summary>
        /// This method is used to convert Module model to ModuleDTO model.
        /// </summary>
        /// <param name="item">Module model.</param>
        /// <returns>ModuleDTO model.</returns>
        public static ModuleDTO ToViewModel(this Module item)
        {
            return new ModuleDTO
            {
                code = item.ModuleCode,
                name = item.ModuleName,
                service_code = item.ServiceCode,
                features = item.FeatureDetails == null ? null : item.FeatureDetails.Values.Select(x => x.ToViewModel()).ToArray(),
                usages = item.UsageDetails == null ? null : item.UsageDetails.Values.Select(x => x.ToViewModel()).ToArray()
            };
        }

        /// <summary>
        /// This method is used to convert Features model to FeatureDTO model.
        /// </summary>
        /// <param name="item">Feature model.</param>
        /// <returns>FeatureDTO model.</returns>
        public static FeatureDTO ToViewModel(this Feature item)
        {
            return new FeatureDTO
            {
                code = item.FeatureCode,
                name = item.FeatureName,
                module_code = item.FeatureModuleCode
            };
        }

        /// <summary>
        /// This method is used to convert Service model to ServiceDTO model.
        /// </summary>
        /// <param name="item">Service model.</param>
        /// <returns>ServiceDTO model.</returns>
        public static ServiceDTO ToViewModel(this Service item)
        {
            return new ServiceDTO
            {
                code = item.ServiceCode,
                name = item.ServiceName,
                description = item.ServiceDescription,
                features = item.FeatureDetails == null ? null : item.FeatureDetails.Values.Select(x => x.ToViewModel()).ToArray(),
                modules = item.ModuleDetails == null ? null : item.ModuleDetails.Values.Select(x => x.ToViewModel()).ToArray()
            };
        }

        /// <summary>
        /// This method is used to convert Usage model to UsageDTO model.
        /// </summary>
        /// <param name="item">Usage model.</param>
        /// <returns>UsageDTO model.</returns>
        public static UsageDTO ToViewModel(this Usage item)
        {
            return new UsageDTO
            {
                code = item.UsageCode,
                name = item.UsageName,
                type_code = item.UsageTypeCode,
                type_name = item.UsageTypeName,
                threshold = item.Threshold,
                module_code = item.UsageModuleCode,
                min_capacity = item.MinimumCapacityUsage.HasValue ? item.MinimumCapacityUsage.Value : 0,
                max_capacity = item.MaximumCapacityUsage.HasValue ? item.MaximumCapacityUsage.Value : 0,
                can_generate_invoice = item.CanGenerateInvoice,
                allow_over_usage = item.AllowOverUsage.HasValue ? item.AllowOverUsage.Value : false
            };
        }

        /// <summary>
        /// This method is used to convert Privilege model to PrivilegeDTO model.
        /// </summary>
        /// <param name="item">Privilege model.</param>
        /// <returns>PrivilegeDTO model.</returns>
        public static PrivilegeDTO ToViewModel(this Privilege item)
        {
            return new PrivilegeDTO
            {
                code = item.Id,
                name = item.Name,
                description = item.Description,
            };
        }

        /// <summary>
        /// This method is used to convert collection of TenantLicense model to collection TenantLicenseDTO model.
        /// </summary>
        /// <param name="entity">collection of TenantLicense model.</param>
        /// <returns>collection of TenantLicenseDTO model.</returns>
        public static IEnumerable<TenantLicenseDTO> ToViewModel(this IEnumerable<TenantLicense> entity)
        {
            if (entity == null) return null;
            var lst = new List<TenantLicenseDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        /// <summary>
        /// This method is used to convert the collection of Feature model to collection of FeatureDTO model.
        /// </summary>
        /// <param name="entity">collection of Feature model.</param>
        /// <returns>collection of FeatureDTO model.</returns>
        public static Dictionary<string, FeatureDTO> ToViewModel(this Dictionary<string, Feature> entity)
        {
            var featureDTO = new Dictionary<string, FeatureDTO>();
            foreach (KeyValuePair<string, Feature> item in entity)
            {
                featureDTO.Add(item.Key, item.Value.ToViewModel());
            }
            return featureDTO;
        }

        /// <summary>
        /// This method is used to convert collection of Feature model to collection of FeatureDTO model.
        /// </summary>
        /// <param name="entity">collection of Feature model.</param>
        /// <returns>collection of FeatureDTO model.</returns>
        public static Dictionary<string, Dictionary<string, FeatureDTO>> ToViewModel(this Dictionary<string, Dictionary<string, Feature>> entity)
        {
            var featureDTO = new Dictionary<string, Dictionary<string, FeatureDTO>>();
            foreach (KeyValuePair<string, Dictionary<string, Feature>> item in entity)
            {
                featureDTO.Add(item.Key, item.Value.ToViewModel());
            }
            return featureDTO;
        }

        /// <summary>
        /// This method is used to convert collection of Usage model to collection of UsgaeDTO model.
        /// </summary>
        /// <param name="entity">collection of Usage model.</param>
        /// <returns>collection of UsgaeDTO model.</returns>
        public static Dictionary<string, UsageDTO> ToViewModel(this Dictionary<string, Usage> entity)
        {
            var usageDTO = new Dictionary<string, UsageDTO>();
            foreach (KeyValuePair<string, Usage> item in entity)
            {
                usageDTO.Add(item.Key, item.Value.ToViewModel());
            }
            return usageDTO;
        }

        /// <summary>
        /// This method is used to convert the collection of Usage model to collection of UsageDTO model.
        /// </summary>
        /// <param name="entity">collection of Usage model.</param>
        /// <returns>collection of UsgaeDTO model.</returns>
        public static Dictionary<string, Dictionary<string, UsageDTO>> ToViewModel(this Dictionary<string, Dictionary<string, Usage>> entity)
        {
            var usageDTO = new Dictionary<string, Dictionary<string, UsageDTO>>();
            foreach (KeyValuePair<string, Dictionary<string, Usage>> item in entity)
            {
                usageDTO.Add(item.Key, item.Value.ToViewModel());
            }
            return usageDTO;
        }


        #endregion TenantLicense

        #region Package

        /// <summary>
        /// This method is used to convert the PackageDetails entity to PackageDTO model.
        /// </summary>
        /// <param name="entity">PackageDetails model.</param>
        /// <returns>PackageDTO model.</returns>
        public static PackageDTO ToViewModel(this PackageDetails entity)
        {
            if (entity == null) return null;

            var vm = new PackageDTO
            {
                id = Guid.Parse(entity.PackageId),
                tenant_id = Guid.Parse(entity.TenantId),
                name = entity.PackageName,
                description = entity.PackageDescription,
                trial_days = entity.ExpiryDays,
            };

            if (entity.PacakageModuleDetails != null) vm.modules = entity.PacakageModuleDetails.Values.Select(x => x.ToViewModel()).ToArray();
            if (entity.PacakgeFeatureDetails != null) vm.features = entity.PacakgeFeatureDetails.Values.Select(x => x.ToViewModel()).ToArray();
            if (entity.PackageUsageDetails != null) vm.usages = entity.PackageUsageDetails.Values.Select(x => x.ToViewModel()).ToArray();

            return vm;
        }

        /// <summary>
        /// This method used to convert the collection of PackageDetails entity to list of PackageDTO model.
        /// </summary>
        /// <param name="entity">collection of PackageDetails model.</param>
        /// <returns>collection of PackageDTO model.</returns>
        public static IEnumerable<PackageDTO> ToViewModel(this IEnumerable<LicensePackage> entity)
        {
            if (entity == null || entity.Count() == 0) return null;

            var lst = entity.Select(x => x.ToViewModel()).ToList();
            return lst;
        }

        /// <summary>
        /// This method is used to convert LicensePackage model to PackageDTO model.
        /// </summary>
        /// <param name="entity">LicensePackage model.</param>
        /// <returns>PackageDTO model.</returns>
        public static PackageDTO ToViewModel(this LicensePackage entity)
        {
            if (entity == null) return null;
            var vm = new PackageDTO
            {
                id = Guid.Parse(entity.LicensepackageId),
                tenant_id = Guid.Parse(entity.TenantId),
                name = entity.Name,
                description = entity.Description,
                trial_days = entity.ExpiryDays,
                
            };
            return vm;
        }

        /// <summary>
        /// This method is used to convert the PackageModule entity to ModuleDTO model.
        /// </summary>
        /// <param name="entity">PackageModule model.</param>
        /// <returns>ModuleDTO model.</returns>
        public static ModuleDTO ToViewModel(this PackageModule item)
        {
            if (item == null) return null;
            return new ModuleDTO
            {
                code = item.ModuleCode,
                name = item.ModuleName,
                //description = item.ModuleName,
                service_code = item.ServiceId,
                features = item.FeatureDetails == null ? null : item.FeatureDetails.Values.Select(x => x.ToViewModel()).ToArray(),
                usages = item.UsageDetails == null ? null : item.UsageDetails.Values.Select(x => x.ToViewModel()).ToArray()
            };
        }

        /// <summary>
        /// This method is used to convert the PackageFeature entity to FeatureDTO model.
        /// </summary>
        /// <param name="entity">PackageFeature model.</param>
        /// <returns>FeatureDTO model.</returns>
        public static FeatureDTO ToViewModel(this PackageFeature item)
        {
            if (item == null) return null;
            return new FeatureDTO
            {
                code = item.FeatureId,
                name = item.FeatureName,
                module_code = item.ModuleId
            };
        }

        /// <summary>
        /// This method is used to convert the PackageUsage entity to UsageDTO model.
        /// </summary>
        /// <param name="entity">PackageUsage model.</param>
        /// <returns>UsageDTO model.</returns>
        public static UsageDTO ToViewModel(this PackageUsage item)
        {
            if (item == null) return null;
            return new UsageDTO
            {
                code = item.UsageCode,
                name = item.UsageName,
                type_code = item.UsageTypeCode,
                type_name = item.UsageTypeName,
                threshold = item.UsageThreshold,
                module_code = item.ModuleId,
                min_capacity = item.MinimumCapacity.HasValue ? item.MinimumCapacity.Value : 0,
                max_capacity = item.MaximumCapacity.HasValue ? item.MaximumCapacity.Value : 0,
                can_generate_invoice = item.CanGenerateInvoice,
                allow_over_usage = item.AllowOverUsage.HasValue ? item.AllowOverUsage.Value : false
            };
        }
        /// <summary>
        /// This method is used to convert PricePlan model to PricePlanDTO model.
        /// </summary>
        /// <param name="entity">PricePaln model.</param>
        /// <returns>PricePlanDTO model.</returns>
        public static PricePlanDTO ToViewModel(this PricePlan entity)
        {
            if (entity == null) return null;
            var vm = new PricePlanDTO
            {
                id = entity.Id,
                tenant_id = entity.TenantId,
                name = entity.Name,
                description = entity.Description,
                frequency = (BillingFrequency)Enum.Parse(typeof(BillingFrequency), entity.BillFrequency.ToString(), true),
                base_price = entity.Price
            };

            if (entity.LineItems != null)
            {
                var prices = new List<PriceTableDTO>();
                foreach (var item in entity.LineItems)
                {
                    var val = new PriceTableDTO
                    {
                        id = item.Id,
                        name = item.Name,
                        description = item.Description,
                        slab_variable = item.PriceTable.SlabMeterVariable,
                        actedon_variable = item.PriceTable.ActedOnMeterVariable,
                        factor_type = (CelloSaaS.RestApiSDK.FactorType)Enum.Parse(typeof(CelloSaaS.RestApiSDK.FactorType), item.PriceTable.FactorType.ToString(), true),
                        calculation_type = (CalculationType)Enum.Parse(typeof(CalculationType), item.PriceTable.CalculationType.ToString(), true),
                    };

                    if (item.PriceTable.PriceSlabs != null)
                    {
                        val.slabs = item.PriceTable.PriceSlabs.Select(s => new SlabDTO
                            {
                                id = s.Id,
                                start = s.StartValue,
                                end = s.EndValue,
                                factor = s.Factor
                            }).ToArray();
                    }

                    prices.Add(val);
                }
                vm.items = prices.ToArray();
            }

            return vm;
        }
        /// <summary>
        /// This method is used to convert collection of PricePlan model to collection of PricePlanDTo model.
        /// </summary>
        /// <param name="entity">collection of PricePlan model.</param>
        /// <returns>collection of PricePlanDTO model.</returns>
        public static IEnumerable<PricePlanDTO> ToViewModel(this IEnumerable<PricePlan> entity)
        {
            var lst = new List<PricePlanDTO>();
            if (entity == null) return lst;
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        #endregion Package

        #region Email
        /// <summary>
        /// This method is used to convert NotificationAudit model to EmailAuditDTO model.
        /// </summary>
        /// <param name="entity">NotificationAudit model.</param>
        /// <returns>EmailAuditDTO model.</returns>
        public static EmailAuditDTO ToViewModel(this NotificationAudit entity)
        {
            var cnt = entity.NotificationContent as TextMessageContent;
            var dest = entity.NotificationDestination as EmailDestination;

            return new EmailAuditDTO
            {
                id = Guid.Parse(entity.Id),
                tenant_id = Guid.Parse(entity.TenantId),
                notification_id = Guid.Parse(entity.NotificationId),
                notification_name = entity.NotificationDetails.NotificationName,
                notification_category = entity.NotificationDetails.Category,
                notification_description = entity.NotificationDetails.Description,
                track_id = entity.NotificationDestination.TrackId,
                to = dest.RecipientAddress,
                from = dest.SenderAddress,
                cc = dest.SecondaryRecipientAddress != null ? string.Join(";", dest.SecondaryRecipientAddress) : null,
                bcc = dest.Bcc != null ? string.Join(";", dest.Bcc) : null,
                subject = cnt.Subject,
                content = cnt.ProcessedContent,
                created_by = Guid.Parse(entity.CreatedBy),
                created_on = entity.CreatedOn,
                status = entity.Status.HasValue ? entity.Status.Value : true
            };
        }

        /// <summary>
        /// This method is used to convert collection of NotificationAudit model to collection of EmailAuditDTO model.
        /// </summary>
        /// <param name="entity">collection of NotificationAudit model.</param>
        /// <returns>collection of EmailAuditDTO model.</returns>
        public static IEnumerable<EmailAuditDTO> ToViewModel(this IEnumerable<NotificationAudit> entity)
        {
            if (entity == null) return null;
            var lst = new List<EmailAuditDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel());
            }
            return lst;
        }

        #endregion

        #region Entity metadata

        /// <summary>
        /// This method is used to convert the EntityMetadataDTO model to  EntityMetaData model.
        /// </summary>
        /// <param name="vm">EntityMetadataDTO model.</param>
        /// <param name="entity">EntityMetaData model.</param>
        /// <returns>EntityMetaData model.</returns>
        public static EntityMetaData ToBusinessEntity(this EntityMetadataDTO vm, EntityMetaData entity = null)
        {
            if (vm == null) return null;
            if (entity == null) entity = new EntityMetaData();

            return entity;
        }

        /// <summary>
        /// This method is used to convert the EntityMetaData model to  EntityMetadataDTO model.
        /// </summary>
        /// <param name="entity">EntityMetaData model.</param>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>EntityMetadataDTO model.</returns>
        public static EntityMetadataDTO ToViewModel(this EntityMetaData entity, Guid tenant_id)
        {
            if (entity == null) return null;
            var vm = new EntityMetadataDTO
            {
                tenant_id = string.IsNullOrEmpty(entity.TenantId) ? tenant_id : Guid.Parse(entity.TenantId),
                entity_id = entity.EntityIdentifier,
                entity_name = entity.EntityName,
                entity_schema = entity.EntitySchema,
                display_column_name = entity.DisplayColumnName,
                entity_schema_table_connection_string_name = entity.EntitySchemaTableConnectionStringName,
                extended_schema_table_name = entity.ExtendedSchemaTableName,
                extn_schema_table_connection_name = entity.ExtnSchemaTableConnectionName,
                is_extensible = entity.IsExtensible,
                primary_key = entity.PrimaryKeyName,
                referenced_schema = entity.ReferencedSchema,
                schema_table_name = entity.SchemaTableName,
                tenant_id_column_name = entity.TenantIdColumnName,
                features = entity.EntityFeatures != null ? entity.EntityFeatures.ToArray() : null,
                privileges = entity.EntityPrivileges != null ? entity.EntityPrivileges.ToArray() : null
            };

            if (entity.FieldsMetaData != null)
            {
                vm.fields = entity.FieldsMetaData.Values.Select(x => x.ToViewModel(tenant_id)).ToArray();
            }

            return vm;
        }

        /// <summary>
        /// This method is used to convert the EntityFieldMetaData model to  EntityFieldMetadataDTO model.
        /// </summary>
        /// <param name="entity">EntityFieldMetaDatam model.</param>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>EntityFieldMetadataDTO model.</returns>
        public static EntityFieldMetadataDTO ToViewModel(this EntityFieldMetaData entity, Guid tenant_id)
        {
            if (entity == null) return null;
            var vm = new EntityFieldMetadataDTO
            {
                field_id = entity.EntityFieldIdentifier,
                field_name = entity.Name,
                entity_id = entity.EntityId,
                tenant_id = string.IsNullOrEmpty(entity.TenantId) ? tenant_id : Guid.Parse(entity.TenantId),
                activated_date = entity.ActivatedDate,
                deactivated_date = entity.DeactivatedDate,
                extn_columndetails_id = entity.ExtnColumnDetailsId,
                is_unique = entity.IsUnique,
                length = entity.Length,
                mapped_column_name = entity.MappedColumnName,
                pickup_list_id = entity.PickupListId,
                reference_column_name = entity.ReferenceColumnName,
                reference_entity_id = entity.ReferenceEntityId,
                reference_table_name = entity.ReferenceTableName,
                type_id = entity.TypeID,
                validation_regex = entity.ValidationRegEx
            };

            if (entity.entityExtnColumnDetails != null)
            {
                vm.column_details = new EntityExtendedColumnDetailsDTO
                {
                    id = entity.entityExtnColumnDetails.Id,
                    column_name = entity.entityExtnColumnDetails.ColumnName
                };
            }

            return vm;
        }

        /// <summary>
        ///  This method is used to convert the collection of  EntityMetaData model to  collection of EntityMetadataDTO model.
        /// </summary>
        /// <param name="entity">collection of  EntityMetaData model.</param>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>collection of EntityMetadataDTO model.</returns>
        public static IEnumerable<EntityMetadataDTO> ToViewModel(this IEnumerable<EntityMetaData> entity, Guid tenant_id)
        {
            if (entity == null) return null;
            var lst = new List<EntityMetadataDTO>();
            foreach (var item in entity)
            {
                lst.Add(item.ToViewModel(tenant_id));
            }
            return lst;
        }

        #endregion

        #region Dataview

        /// <summary>
        /// This method is used to convert the DataViewMetaData model to  DataViewDTO model.
        /// </summary>
        /// <param name="entity">DataViewMetaData model.</param>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <returns>DataViewDTO model.</returns>
        public static DataViewDTO ToViewModel(this DataViewMetaData entity, Guid tenant_id)
        {
            if (entity == null) return null;

            var vm = new DataViewDTO
            {
                dataview_id = entity.DataView.DataViewID,
                description = entity.DataView.Description,
                id = entity.DataView.ID,
                entity_id = entity.DataView.MainEntity,
                name = entity.DataView.Name,
                tenant_id = string.IsNullOrEmpty(entity.TenantId) ? tenant_id : Guid.Parse(entity.TenantId),
            };

            if (entity.AvailableFields != null)
            {
                vm.fields = entity.AvailableFields.Values.Select(x => x.ToViewModel(tenant_id, vm.entity_id)).ToArray();
            }

            return vm;
        }

        /// <summary>
        /// This method is used to convert the DataViewFieldMetaData model to  DataViewFieldDTO model.
        /// </summary>
        /// <param name="entity">DataViewFieldMetaData model.</param>
        /// <param name="tenant_id">tenant identifier.</param>
        /// <param name="entity_id">entity identifier.</param>
        /// <returns>DataViewFieldDTO model.</returns>
        public static DataViewFieldDTO ToViewModel(this DataViewFieldMetaData entity, Guid tenant_id, string entity_id)
        {
            if (entity == null) return null;

            var vm = new DataViewFieldDTO
            {
                id = entity.ID,
                field_id = entity.FieldID,
                entity_id = entity_id,
                tenant_id = string.IsNullOrEmpty(entity.TenantID) ? tenant_id : Guid.Parse(entity.TenantID),
                field_metadata = entity.EntityFieldMetaData.ToViewModel(tenant_id),
                description = entity.Description,
                display_name = entity.DisplayName,
                field_type = entity.FieldType.ToString(),
                is_editable = entity.IsEditable,
                is_extended_field = entity.IsExtendedField,
                is_fixed = entity.IsFixed,
                is_mandatory = entity.IsMandatory,
                is_visible = entity.IsVisible,
                max_length = entity.MaxLength,
                ordinal = entity.Ordinal,
                pickup_list_id = entity.PickupListId,
                property_name = entity.PropertyName,
                regex = entity.RegularExpression,
                regex_error_message = entity.RegularExpressionErrorMessage,
                required_field_error_message = entity.RequiredFieldErrorMessage,
                viewdata_name = entity.ViewDataName,
                is_hidden = entity.IsHidden,
                default_value = entity.DefaultValue

            };

            if (string.IsNullOrEmpty(vm.field_id) && vm.field_metadata != null) vm.field_id = vm.field_metadata.field_id;

            return vm;
        }

        #endregion

        #region Pickuplist

        /// <summary>
        /// This method is used to convert the PickupList entity to PickuplistDTO model.
        /// </summary>
        /// <param name="entity">PickupList model.</param>
        /// <returns>PickuplistDTO model.</returns>
        public static PickuplistDTO ToViewModel(this PickupList entity)
        {
            if (entity == null) return null;
            var vm = new PickuplistDTO
            {
                id = Guid.Parse(entity.Id),
                name = entity.Name,
            };

            if (!string.IsNullOrEmpty(entity.TenantCode)) vm.tenant_id = Guid.Parse(entity.TenantCode);

            return vm;
        }

        /// <summary>
        /// This method is used to convert the PickupListValue  entity to PickuplistValueDTO model.
        /// </summary>
        /// <param name="entity">PickupListValue model.</param>
        /// <returns>PickuplistValueDTO model.</returns>
        public static PickuplistValueDTO ToViewModel(this PickupListValue entity)
        {
            if (entity == null) return null;
            var vm = new PickuplistValueDTO
            {
                id = Guid.Parse(entity.Id),
                name = entity.ItemName,
                value = entity.ItemId
            };
            return vm;
        }

        /// <summary>
        /// This method is used to convert the collection of PickupList entity to collection of PickuplistDTO model.
        /// </summary>
        /// <param name="entity">collection of PickupList model.</param>
        /// <returns>collection of PickuplistDTO model.</returns>
        public static IEnumerable<PickuplistDTO> ToViewModel(this IEnumerable<PickupList> entity)
        {
            if (entity == null || entity.Count() == 0) return null;
            var lst = entity.Select(x => x.ToViewModel()).ToList();
            return lst;
        }

        /// <summary>
        /// This method is used to convert the collection of PickupListValue  entity to collection of PickuplistValueDTO model.
        /// </summary>
        /// <param name="entity">collection of PickupListValue model.</param>
        /// <returns>collection of PickuplistValueDTO model.</returns>
        public static IEnumerable<PickuplistValueDTO> ToViewModel(this IEnumerable<PickupListValue> entity)
        {
            if (entity == null || entity.Count() == 0) return null;
            var lst = entity.Select(x => x.ToViewModel()).ToList();
            return lst;
        }

        #endregion

        #region Metering
        /// <summary>
        /// This method is used to convert MeteringLog model to MeteringLogDTO model.
        /// </summary>
        /// <param name="entity">MeteringLog model.</param>
        /// <returns>MeteringLogDTO model.</returns>
        public static MeteringLogDTO ToViewModel(this MeteringLog entity)
        {
            if (entity == null) return null;

            var vm = new MeteringLogDTO
            {
                id = Guid.Parse(entity.Id),
                tenant_id = Guid.Parse(entity.TenantId),
                amount = entity.Amount,
                usage_code = entity.UsageCode,
                user_id = Guid.Parse(entity.LoggedBy),
                logged_date = entity.LoggedDate
            };

            return vm;
        }

        /// <summary>
        /// This method is used to convert collection of MeteringLog model to collection of MeteringLogDTO model.
        /// </summary>
        /// <param name="entities">collection of MeteringLog model.</param>
        /// <returns>collection of MeteringLogDTO model.</returns>
        public static IEnumerable<MeteringLogDTO> ToViewModel(this IEnumerable<MeteringLog> entities)
        {
            if (entities == null || entities.Count() == 0) return null;
            return entities.Select(x => x.ToViewModel());
        }

        /// <summary>
        /// This method is used to convert UsageDetail model to UsageDetailDTO model.
        /// </summary>
        /// <param name="entity">UsageDetail model.</param>
        /// <returns>UsageDetailDTO model.</returns>
        public static UsageDetailDTO ToViewModel(this UsageDetail entity)
        {
            if (entity == null) return null;

            var vm = new UsageDetailDTO
            {
                tenant_id = Guid.Parse(entity.TenantId),
                amount = entity.Amount,
                usage_code = entity.UsageCode,
                can_generate_invoice = entity.CanGenerateInvoice,
                module_code = entity.ModuleCode,
                threshold = entity.Threshold,
                usage_name = entity.UsageName,
                usage_type = entity.UsageType
            };

            return vm;
        }

        /// <summary>
        /// This method is used to convert collection of UsageDetail model to collection of UsageDetailDTO model.
        /// </summary>
        /// <param name="entities">collection of UsageDetail model.</param>
        /// <returns>collection of UsageDetailDTO model.</returns>
        public static IEnumerable<UsageDetailDTO> ToViewModel(this IEnumerable<UsageDetail> entities)
        {
            if (entities == null || entities.Count() == 0) return null;
            return entities.Select(x => x.ToViewModel());
        }

        #endregion Metering

        #region Role

        /// <summary>
        ///  This method is used to convert the Role model to  RoleDTO model.
        /// </summary>
        /// <param name="entity">Role model.</param>
        /// <returns>RoleDTO model.</returns>
        public static RoleDTO ToViewModel(this Role entity)
        {
            if (entity == null) return null;
            var vm = new RoleDTO
            {
                id = entity.RoleId,
                name = entity.RoleName,
                description = entity.Description,
                is_global = entity.IsGlobal,

            };

            if (!string.IsNullOrEmpty(entity.TenantId))
            {
                vm.tenant_id = Guid.Parse(entity.TenantId);
            }
            return vm;
        }

        /// <summary>
        /// This method is used to convert the collection of  Role model to collection of RoleDTO model.
        /// </summary>
        /// <param name="entity">collection of  Role model.</param>
        /// <returns>collection of RoleDTO model.</returns>
        public static IEnumerable<RoleDTO> ToViewModel(this IEnumerable<Role> entity)
        {
            if (entity == null || entity.Count() == 0) return null;
            var lst = entity.Select(x => x.ToViewModel()).ToList();
            return lst;
        }
        #endregion
    }
}